/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.DataAccess
import org.osate.aadl2.DataSubcomponent
import org.osate.aadl2.Feature
import org.osate.ba.aadlba.BehaviorVariable
import org.osate.ba.aadlba.DataComponentReference
import org.osate.ba.aadlba.DataHolder
import org.osate.ba.aadlba.DataSubcomponentHolder
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.VariableDeclaration
import org.eclipse.ui.console.MessageConsoleStream

class BehaviorReferencedElementGeneration {
	new(){
		
	}
	
	def static RecordFieldDeclaration genReferencedElement(Model model, DataComponentReference target, DataHolder data, MessageConsoleStream _out) {

			return model.eAllContents.toList.filter(RecordFieldDeclaration).filter[rfd|
				rfd.name ==
					getReferencedElementContext(target.data.get(target.data.indexOf(data) - 1), _out) + '_' + RTFiacreCore.id(data.element as DataSubcomponent)].get(0)			
		}
	
	def static  genReferencedElement(ProcessDeclaration declaration, DataSubcomponent dsc, MessageConsoleStream _out) {
			_out.println("referencedelement "+RTFiacreCore.id(dsc))
			return declaration.eAllContents.toList.filter(VariableDeclaration).filter[vd|
						vd.name ==RTFiacreCore.id(dsc)].get(0)
		}
	def static  genReferencedElement(ProcessDeclaration declaration, DataAccess dsc, MessageConsoleStream _out) {
			_out.println("referencedelement "+RTFiacreCore.id(dsc))
			return declaration.eAllContents.toList.filter(ParameterDeclaration).filter[vd|
						vd.name ==RTFiacreCore.id(dsc)].get(0)
		}
	def static  genReferencedElement(ProcessDeclaration declaration, BehaviorVariable bv, MessageConsoleStream _out) {
			_out.println("referencedelement "+RTFiacreCore.id(bv))
			return declaration.eAllContents.toList.filter(VariableDeclaration).filter[vd|
						vd.name ==RTFiacreCore.id(bv)].get(0)
		}
	def static  genReferencedElement(ProcessDeclaration declaration, Feature p, MessageConsoleStream _out) {
			return declaration.eAllContents.toList.filter(ParameterDeclaration).filter[vd|
						vd.name ==RTFiacreCore.id(p)].get(0)
		}
	def static  genReferencedPort(ProcessDeclaration declaration, Feature p, MessageConsoleStream _out) {
		val found = declaration.eAllContents.toList.filter(PortDeclaration).filter[vd|vd.name ==RTFiacreCore.id(p)]
			if (found.size > 0) { 
				return found.get(0)
			} else {
				return null
			}
		}	
	def static  genReferencedFreshElement(ProcessDeclaration declaration, Feature p, MessageConsoleStream _out) {
			return declaration.eAllContents.toList.filter(ParameterDeclaration).filter[vd|
						vd.name ==RTFiacreCore.fresh_id(RTFiacreCore.id(p))].get(0)
		}
		
				
	def static String getReferencedElementContext(DataHolder holder, MessageConsoleStream _out) {
		getContext(((holder as DataSubcomponentHolder).element as DataSubcomponent), _out)
	}

	def static String getContext(DataSubcomponent dsc, MessageConsoleStream _out) {
		RTFiacreCore.uid(dsc.classifier, _out)
	}
}