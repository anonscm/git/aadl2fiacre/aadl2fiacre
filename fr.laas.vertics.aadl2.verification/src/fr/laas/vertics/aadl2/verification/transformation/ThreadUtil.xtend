/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.IntegerLiteral

class ThreadUtil {
	new(){}
	
	def static boolean hasDispatchOffset(ComponentInstance instance){
		AADLCore.hasPropertyValueForKey(instance.ownedPropertyAssociations,kOffset())
	}
	
	def static long getDispatchOffset(ComponentInstance instance) {
		val IntegerLiteral deadline = AADLCore.getPropertyValueForKey(instance.ownedPropertyAssociations,kOffset()).ownedValue as IntegerLiteral
			val long factor = (deadline.unit.factor as IntegerLiteral).value
			return (deadline.value * factor)
			
		}	
	 
	def final static String kOffset() {
		"Dispatch_Offset"
	}
		 
	def final static String kDeadline() {
		"Deadline"
	}
	def final static String kPeriod() {
		"Period"
	}
	def final static String kPriority() {
		"Priority"
	}
	
	def static boolean hasThreadDeadline(ComponentInstance instance){
		if (AADLCore.hasPropertyValueForKey(instance.ownedPropertyAssociations,kDeadline())
			&& AADLCore.hasPropertyValueForKey(instance.ownedPropertyAssociations,kPeriod())){
			! (getThreadPeriod(instance) == getThreadDeadline(instance)) 
		}
		else{
			false
			}
	}
	def static long getThreadPeriod(ComponentInstance instance){
			val IntegerLiteral intlit = AADLCore.getPropertyValueForKey(instance.ownedPropertyAssociations,kPeriod()).ownedValue as IntegerLiteral
			val long factor = (intlit.unit.factor as IntegerLiteral).value
			return (intlit.value * factor)
	}
	def static long getThreadDeadline(ComponentInstance instance) {
		val IntegerLiteral deadline = AADLCore.getPropertyValueForKey(instance.ownedPropertyAssociations,kDeadline()).ownedValue as IntegerLiteral
			val long factor = (deadline.unit.factor as IntegerLiteral).value
			return (deadline.value * factor)
			
		}
}