/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import java.util.ArrayList
import org.eclipse.emf.common.util.EList
import org.osate.aadl2.ConnectionTiming
import org.osate.aadl2.EnumerationLiteral
import org.osate.aadl2.NamedValue
import org.osate.aadl2.PropertyAssociation
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.FeatureInstance
import org.osate.aadl2.instance.SystemInstance
import fr.irit.fiacre.etoile.xtext.fiacre.Block
import fr.irit.fiacre.etoile.xtext.fiacre.BooleanType
import fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import fr.irit.fiacre.etoile.xtext.fiacre.IdentifierExpression
import fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.IntegerType
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ReferenceExpression
import fr.irit.fiacre.etoile.xtext.fiacre.ReferencedType
import fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.TypeInstance
import org.eclipse.ui.console.MessageConsoleStream

class ConnectionProcessInstantiation {
	new() {
	}

	def static ArrayList<Block> genDataConnections(SystemInstance instance, Model model,
		ComponentDeclaration declaration, Model library, FiacreFactory factory, MessageConsoleStream _out) {
		val ArrayList<Block> blocks = new ArrayList<Block>()
		for (cnxi : SystemInstanceUtil.getDataPortConnectionInstances(instance)) {
			_out.println("Connexion " + cnxi)
			_out.println("timing" + getTimingProtocol(cnxi.ownedPropertyAssociations) + "z")
			_out.println("timing" + ConnectionTiming.IMMEDIATE + "z")
			_out.println(("immediate" == "immediate").toString())
			_out.println((getTimingProtocol(cnxi.ownedPropertyAssociations) == ConnectionTiming.IMMEDIATE.getName()).toString())
			_out.println(ConnectionTiming.IMMEDIATE.class.getName())
			blocks.add(
				genConnectionDataPort(cnxi.source as FeatureInstance, cnxi.destination as FeatureInstance,
					getTimingProtocol(cnxi.ownedPropertyAssociations) == ConnectionTiming.IMMEDIATE.getName(), model,
					declaration, library, factory, _out))

		}
		_out.println("blocks" + blocks) //+ ((blocks.get(0) as InstanceDeclaration).instance.component as ProcessDeclaration).name)
		blocks
	}

	def static InstanceDeclaration genConnectionDataPort(
		FeatureInstance sourcePort, 
		FeatureInstance destinationPort,
		boolean immediate, 
		Model model, 
		ComponentDeclaration declaration, 
		Model library, 
		FiacreFactory factory, 
		MessageConsoleStream _out
	) {
		
		_out.println("immediate " + immediate)
		val ComponentInstance source_ci = Utils.eContainer(sourcePort, ComponentInstance) as ComponentInstance
		val ComponentInstance destination_ci = Utils.eContainer(destinationPort, ComponentInstance) as ComponentInstance
		if (ComponentInstanceUtil.isPeriodic(Utils.eContainer(destinationPort, ComponentInstance) as ComponentInstance)) {
			if (ComponentInstanceUtil.isPeriodic(source_ci)) {
				if (immediate) {
					val InstanceDeclaration id = factory.createInstanceDeclaration
					val fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance compoi = factory.createComponentInstance
					id.instance = compoi
					compoi.component = library.declarations.filter(ProcessDeclaration).filter[process|
						process.name == "immediate_connection"].get(0)

					// type
					val TypeInstance tii = factory.createTypeInstance
					compoi.generics.add(tii)
					// if then else for each type
					if (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).type instanceof ReferencedType) {
						val ReferencedType reftt = factory.createReferencedType
						tii.type = reftt
						reftt.type = (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).
							type as ReferencedType).type
					} else {
						if (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).type instanceof IntegerType) {
							val IntegerType inttype = factory.createIntegerType
							tii.type = inttype
						} else {
							if (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).type instanceof BooleanType) {
								val BooleanType booltype = factory.createBooleanType
								tii.type = booltype
							}

						}
					}
					//ports
					val IdentifierExpression ide_src_d = factory.createIdentifierExpression
					compoi.ports.add(ide_src_d)
					ide_src_d.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
					lpd.name == RTFiacreCore.processUid(source_ci) + "_d"].get(0)
					
					val IdentifierExpression ide_src_c = factory.createIdentifierExpression
					compoi.ports.add(ide_src_c)
					ide_src_c.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
					lpd.name == RTFiacreCore.processUid(source_ci) + "_c"].get(0)
					
					val IdentifierExpression ide_dst_d = factory.createIdentifierExpression
					compoi.ports.add(ide_dst_d)
					ide_dst_d.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
					lpd.name == RTFiacreCore.processUid(destination_ci) + "_d"].get(0)
					
					val IdentifierExpression ide_dst_e = factory.createIdentifierExpression
					compoi.ports.add(ide_dst_e)
					ide_dst_e.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
					lpd.name == RTFiacreCore.processUid(destination_ci) + "_e"].get(0)
					
					// parameters
					val ReferenceExpression ref_exp_src_port = factory.createReferenceExpression
					compoi.parameters.add(ref_exp_src_port)
					ref_exp_src_port.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
					vardec.name == RTFiacreCore.processUid(source_ci) +"_"+ RTFiacreCore.id(sourcePort)].get(0)
					
					val ReferenceExpression ref_exp_dst_port = factory.createReferenceExpression
					compoi.parameters.add(ref_exp_dst_port)
					ref_exp_dst_port.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
					vardec.name == RTFiacreCore.processUid(destination_ci) +"_"+ RTFiacreCore.id(destinationPort)].get(0)
					
					val ReferenceExpression ref_exp_dst_fresh_port = factory.createReferenceExpression
					compoi.parameters.add(ref_exp_dst_fresh_port)
					ref_exp_dst_fresh_port.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
					vardec.name == RTFiacreCore.processUid(destination_ci) +"_"+ RTFiacreCore.fresh_id(RTFiacreCore.id(destinationPort))].get(0)
					id
				} /* immediate */ else {
					val InstanceDeclaration id = factory.createInstanceDeclaration
					val fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance compoi = factory.createComponentInstance
					id.instance = compoi
					compoi.component = library.declarations.filter(ProcessDeclaration).filter[process|
						process.name == "delayed_connection"].get(0)
						
					// type
					val TypeInstance tii = factory.createTypeInstance
					compoi.generics.add(tii)
					// if then else for each type
					if (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).type instanceof ReferencedType) {
						val ReferencedType reftt = factory.createReferencedType
						tii.type = reftt
						reftt.type = (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).
							type as ReferencedType).type
					} else {
						if (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).type instanceof IntegerType) {
							val IntegerType inttype = factory.createIntegerType
							tii.type = inttype
						} else {
							if (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).type instanceof BooleanType) {
								val BooleanType booltype = factory.createBooleanType
								tii.type = booltype
							}

						}
					}
					//ports
					val IdentifierExpression ide_src_dl = factory.createIdentifierExpression
					compoi.ports.add(ide_src_dl)
					ide_src_dl.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
					lpd.name == RTFiacreCore.processUid(source_ci) + "_dl"].get(0)
					
					val IdentifierExpression ide_dst_d = factory.createIdentifierExpression
					compoi.ports.add(ide_dst_d)
					ide_dst_d.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
					lpd.name == RTFiacreCore.processUid(destination_ci) + "_d"].get(0)
					
					// parameters
					val ReferenceExpression ref_exp_src_port = factory.createReferenceExpression
					compoi.parameters.add(ref_exp_src_port)
					ref_exp_src_port.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
					vardec.name == RTFiacreCore.processUid(source_ci) +"_"+ RTFiacreCore.id(sourcePort)].get(0)
					
					val ReferenceExpression ref_exp_dst_port = factory.createReferenceExpression
					compoi.parameters.add(ref_exp_dst_port)
					ref_exp_dst_port.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
					vardec.name == RTFiacreCore.processUid(destination_ci) +"_"+ RTFiacreCore.id(destinationPort)].get(0)
					
					val ReferenceExpression ref_exp_dst_fresh_port = factory.createReferenceExpression
					compoi.parameters.add(ref_exp_dst_fresh_port)
					ref_exp_dst_fresh_port.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
					vardec.name == RTFiacreCore.processUid(destination_ci) +"_"+ RTFiacreCore.fresh_id(RTFiacreCore.id(destinationPort))].get(0)
						
					id
				}
			} /* source is not Periodic */ else {
				if (ComponentInstanceUtil.isSporadic(source_ci)) {
					val InstanceDeclaration id = factory.createInstanceDeclaration
					val fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance compoi = factory.createComponentInstance
					id.instance = compoi
					compoi.component = library.declarations.filter(ProcessDeclaration).filter[process|
						process.name == "per_data_connection"].get(0)
					
					// type
					val TypeInstance tii = factory.createTypeInstance
					compoi.generics.add(tii)
					// if then else for each type
					if (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).type instanceof ReferencedType) {
						val ReferencedType reftt = factory.createReferencedType
						tii.type = reftt
						reftt.type = (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).
							type as ReferencedType).type
					} else {
						if (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).type instanceof IntegerType) {
							val IntegerType inttype = factory.createIntegerType
							tii.type = inttype
						} else {
							if (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).type instanceof BooleanType) {
								val BooleanType booltype = factory.createBooleanType
								tii.type = booltype
							}

						}
					}
					//ports
					val IdentifierExpression ide_src_c = factory.createIdentifierExpression
					compoi.ports.add(ide_src_c)
					ide_src_c.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
					lpd.name == RTFiacreCore.processUid(source_ci) + "_c"].get(0)
					
					val IdentifierExpression ide_dst_d = factory.createIdentifierExpression
					compoi.ports.add(ide_dst_d)
					ide_dst_d.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
					lpd.name == RTFiacreCore.processUid(destination_ci) + "_d"].get(0)
					
					// parameters
					val ReferenceExpression ref_exp_src_port = factory.createReferenceExpression
					compoi.parameters.add(ref_exp_src_port)
					ref_exp_src_port.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
					vardec.name == RTFiacreCore.processUid(source_ci) +"_"+ RTFiacreCore.id(sourcePort)].get(0)
					
					val ReferenceExpression ref_exp_dst_port = factory.createReferenceExpression
					compoi.parameters.add(ref_exp_dst_port)
					ref_exp_dst_port.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
					vardec.name == RTFiacreCore.processUid(destination_ci) +"_"+ RTFiacreCore.id(destinationPort)].get(0)
					
					val ReferenceExpression ref_exp_dst_fresh_port = factory.createReferenceExpression
					compoi.parameters.add(ref_exp_dst_fresh_port)
					ref_exp_dst_fresh_port.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
					vardec.name == RTFiacreCore.processUid(destination_ci) +"_"+ RTFiacreCore.fresh_id(RTFiacreCore.id(destinationPort))].get(0)
					id
				} /* source is neither Sporadic nor Periodic */ else {
					throw new UnsupportedOperationException(
						"error unsupported dispatch_procol for source component" + source_ci.name)
				}
			}
		} /* destination is not Periodic */ else {
			if (ComponentInstanceUtil.isSporadic(
				Utils.eContainer(destinationPort, ComponentInstance) as ComponentInstance)) {
				val InstanceDeclaration id = factory.createInstanceDeclaration
				val fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance compoi = factory.createComponentInstance
				id.instance = compoi
				compoi.component = library.declarations.filter(ProcessDeclaration).filter[process|
					process.name == "spo_data_connection"].get(0)
				
				// type
					val TypeInstance tii = factory.createTypeInstance
					compoi.generics.add(tii)
					// if then else for each type
					if (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).type instanceof ReferencedType) {
						val ReferencedType reftt = factory.createReferencedType
						tii.type = reftt
						reftt.type = (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).
							type as ReferencedType).type
					} else {
						if (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).type instanceof IntegerType) {
							val IntegerType inttype = factory.createIntegerType
							tii.type = inttype
						} else {
							if (DataGeneration.genPortDeclarationType(destinationPort.feature, factory, model, _out).type instanceof BooleanType) {
								val BooleanType booltype = factory.createBooleanType
								tii.type = booltype
							}

						}
					}
					
					val TypeInstance ti = factory.createTypeInstance
					compoi.generics.add(ti)
					val ReferencedType reft = factory.createReferencedType
					ti.type = reft
					reft.type = model.declarations.filter(TypeDeclaration).filter[dec|
					dec.name == RTFiacreCore.unionTypeUid(destination_ci)].get(0)
					
					//ports
					val IdentifierExpression ide_src_c = factory.createIdentifierExpression
					compoi.ports.add(ide_src_c)
					ide_src_c.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
					lpd.name == RTFiacreCore.processUid(source_ci) + "_c"].get(0)
					
					val IdentifierExpression ide_dst_d = factory.createIdentifierExpression
					compoi.ports.add(ide_dst_d)
					ide_dst_d.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
					lpd.name == RTFiacreCore.processUid(destination_ci) + "_d"].get(0)
					
					// parameters
					val ReferenceExpression ref_exp_src_port = factory.createReferenceExpression
					compoi.parameters.add(ref_exp_src_port)
					ref_exp_src_port.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
					vardec.name == RTFiacreCore.processUid(source_ci) +"_"+ RTFiacreCore.id(sourcePort)].get(0)
					
					val ReferenceExpression ref_exp_dst_port = factory.createReferenceExpression
					compoi.parameters.add(ref_exp_dst_port)
					ref_exp_dst_port.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
					vardec.name == RTFiacreCore.processUid(destination_ci) +"_"+ RTFiacreCore.id(destinationPort)].get(0)
					
					val ReferenceExpression ref_exp_dst_fresh_port = factory.createReferenceExpression
					compoi.parameters.add(ref_exp_dst_fresh_port)
					ref_exp_dst_fresh_port.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
					vardec.name == RTFiacreCore.processUid(destination_ci) +"_"+ RTFiacreCore.fresh_id(RTFiacreCore.id(destinationPort))].get(0)
					
				id
			} /* destination is not sporadic and source is sporadic */ else {
				throw new UnsupportedOperationException(
					"error unsupported dispatch_procol for destination component " +
						(Utils.eContainer(destinationPort, ComponentInstance) as ComponentInstance).name)
			}
		}
	}

	def static String getTimingProtocol(EList<PropertyAssociation> list) {
		((AADLCore.getPropertyValueForKey(list, kTiming()).ownedValue as NamedValue).namedValue as EnumerationLiteral).
			name
	}

	def final static String kTiming() {
		"Timing"
	}

}
