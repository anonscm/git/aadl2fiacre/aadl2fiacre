/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.instance.SystemInstance
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import org.osate.aadl2.instance.ComponentInstance
import fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.UnionType
import org.osate.aadl2.Port
import org.osate.aadl2.instance.FeatureInstance
import fr.irit.fiacre.etoile.xtext.fiacre.UnionTags
import fr.irit.fiacre.etoile.xtext.fiacre.UnionTagDeclaration
import org.osate.ba.aadlba.BehaviorAnnex
import org.osate.ba.aadlba.DispatchCondition
import org.osate.ba.aadlba.CompletionRelativeTimeout
import java.util.Set
import java.util.HashSet
import java.util.ArrayList
import org.eclipse.ui.console.MessageConsoleStream

class UnionGeneration {
	
	new(){}
	// ToDo : NullPointerException on Producer Consumer
	def static Set<String> getTimeoutConstructors (ComponentInstance ci){
		val BehaviorAnnex ba = ComponentInstanceUtil.getBehaviorAnnex(ci)
		if (ba !== null) {
			if (ba.transitions !== null) {
				ba.transitions.map[t|t.condition].filter(DispatchCondition).map[dc|dc.dispatchTriggerCondition]
			    .filter(CompletionRelativeTimeout).map(t|BehaviorExpressionGeneration.genExpression(t))
			    .map(exp|RTFiacreCore.constructorUid(exp,ci)).toSet
			} else {
				throw new IllegalStateException( "BA transitions should not be null in " + ci.name )
			}
		} else {
			throw new IllegalStateException( "BA should not be null in " + ci.category.literal + " " + ci.name )
			}
	}
	
	def static Set<ArrayList<String>> getTimeoutConstructorsWithValues (ComponentInstance ci){
		val BehaviorAnnex ba = ComponentInstanceUtil.getBehaviorAnnex(ci)
		val Iterable<String> ite_exp = ba.transitions.map[t|t.condition].filter(DispatchCondition).map[dc|dc.dispatchTriggerCondition]
			    .filter(CompletionRelativeTimeout).map(t|BehaviorExpressionGeneration.genExpression(t))
		val Set<ArrayList<String>> set_exp = new HashSet<ArrayList<String>>()
		for (exp : ite_exp){
			val ArrayList<String> aexp = new ArrayList<String>()
			aexp.add(RTFiacreCore.constructorUid(exp,ci))
			aexp.add(exp)
			set_exp.add(aexp)
		}
		
		return set_exp
	}
	def static void generateUnionTypes(SystemInstance instance, Model m, FiacreFactory factory, MessageConsoleStream _out)	{
		for (ComponentInstance compo_ins: UnionGeneration.getComponentsGeneratingUnionType(instance, _out)){
			UnionGeneration.genUnionType(compo_ins, m, factory, _out)
		}
	}

	
	def static Iterable <ComponentInstance> getComponentsGeneratingUnionType(SystemInstance instance, MessageConsoleStream _out) {
		SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out).filter
		[t|ComponentInstanceUtil.isSporadic(t) 
			&& ComponentInstanceUtil.getPortsFeatureInstances(t).exists
			   [f|PortUtil.isEventOrEventDataPort(f) && PortUtil.isInputPort(f)]
		]
	}
		
	def static genUnionType(ComponentInstance compo_ins, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		val TypeDeclaration type_dec = factory.createTypeDeclaration 
		type_dec.name = RTFiacreCore.unionTypeUid(compo_ins)
		val UnionType ut = factory.createUnionType
		type_dec.value = ut
		model.declarations.add(type_dec)
		for (FeatureInstance p : PortUtil.incoming(PortUtil.withEventKind(ComponentInstanceUtil.getPortsFeatureInstances(compo_ins))).sortBy[category==2] ){ 
			val UnionTags utgs = factory.createUnionTags
			ut.tags.add(utgs)
			val UnionTagDeclaration utd = factory.createUnionTagDeclaration
			utgs.tags.add(utd)
			utd.name = RTFiacreCore.constructorUid(p, compo_ins)
			_out.println("test p.feature "+p.feature)
				
			_out.println("test type "+DataGeneration.genConstructorType(p.feature as Port, factory, model, _out))
			
			if (DataGeneration.genConstructorType(p.feature as Port, factory, model, _out) !== null)
			
			{	
				utgs.type =  DataGeneration.genConstructorType(p.feature as Port, factory, model, _out)
				
			}
		
		}
		for (String s : getTimeoutConstructors(compo_ins)){
			val UnionTags utgs = factory.createUnionTags
			ut.tags.add(utgs)
			val UnionTagDeclaration utd = factory.createUnionTagDeclaration
			utgs.tags.add(utd)
			utd.name = s
		}
		
		
		
	}
	
	def static getTimeoutConstructor(ArrayList<String> strings) {
		strings.get(0)
	}
	
	def static getTimeoutValue(ArrayList<String> strings) {
		strings.get(1)
	}
	 
} 