/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.instance.SystemInstance
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration
import org.eclipse.ui.console.MessageConsoleStream

class AADLTransformer {
	private SystemInstance instance;
	private Model model;
	private Model library;
	private FiacreFactory factory
	private MessageConsoleStream out
	new(SystemInstance _instance, Model _model, Model _library, MessageConsoleStream _out) {
		instance = _instance
		model = _model
		library = _library
		factory = FiacreFactory::eINSTANCE
		out = _out
	}

	def void transform() {

		// generate header: the import section
		RTFiacreGeneration.genHeader( model, factory, out)
		out.println("Fiacre header generated.")
		out.flush
		// generate types 
		RTFiacreGeneration.genTypes( instance, model, factory, out) 
		out.println("Fiacre type generated.")
 		out.flush
		//UnionGeneration.generateUnionTypes(s, m, factory)
//		out.println("list--2---"+SystemInstanceUtil.getProcesses(s))
//		out.println(s.componentImplementation.eContainer.class)
//		out.println("---------------------------------------------------")
//		out.println("list--3---"+SystemInstanceUtil.getThreads(s))
//		out.println("---------------------------------------------------")
//		out.println("list--4---"+SystemInstanceUtil.getComponentsGeneratingProcesses(s))
//		out.println("UnionGeneration---------------------------------------------------")
//		out.println(UnionGeneration.getComponentsGeneratingUnionType(s))
		
		RTFiacreGeneration.generateAllProcesses( instance, model, factory, out) 
		out.println("Fiacre processes generated.")
		out.flush
		RTFiacreGeneration.genComponent( instance, model, library, factory, out)
		out.println("Fiacre components generated.")
		out.flush
		out.println("Fiacre code generation successful.")
		out.flush
//		val ThreadImplementation th = (s.componentImplementation.eContainer as PublicPackageSection).ownedClassifiers.filter[name == "thApplis.others"].get(0) as ThreadImplementation
//		out.println("thread implementation------"+ th)
//		val DefaultAnnexSubclause das = th.ownedAnnexSubclauses.get(0) as DefaultAnnexSubclause
//		val BehaviorAnnex ba = das.parsedAnnexSubclause as BehaviorAnnex
//		out.println (ba.states)
		out.println("library"+library.declarations.filter(TypeDeclaration).get(0).name)
	}
}
