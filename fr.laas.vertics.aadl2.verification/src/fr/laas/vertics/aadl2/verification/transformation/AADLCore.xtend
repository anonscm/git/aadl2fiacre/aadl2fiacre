/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.eclipse.emf.common.util.EList
import org.osate.aadl2.PropertyAssociation
import org.osate.aadl2.ModalPropertyValue
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.SystemInstance

class AADLCore {
	new(){}
	
	def static ModalPropertyValue getPropertyValueForKey(EList<PropertyAssociation> list, String string) {
		val resultats = list.filter[pa|pa.property.name == string]
		if (! resultats.empty) {
			resultats.get(0).ownedValues.get(0)	
		} else {
			throw new IllegalArgumentException("The property List " + list +" does not contain key " + string)
		}
	} 
	
	def static boolean hasPropertyValueForKey (EList<PropertyAssociation> lpa, String key) {
		if (lpa.size== 0 || key =="")
			false
		else
			lpa.exists[pa|pa.property.name==key]
	}
	
	def static SystemInstance getSystemInstance(ComponentInstance instance) {
		instance.systemInstance
	}

}