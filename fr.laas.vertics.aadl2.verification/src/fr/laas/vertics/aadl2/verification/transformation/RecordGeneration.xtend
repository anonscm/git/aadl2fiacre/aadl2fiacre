/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.instance.SystemInstance
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import org.osate.aadl2.DataImplementation
import fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration
import org.osate.aadl2.Classifier
import fr.irit.fiacre.etoile.xtext.fiacre.RecordType
import org.osate.aadl2.DataSubcomponent
import fr.irit.fiacre.etoile.xtext.fiacre.RecordFields
import fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldDeclaration
import org.osate.aadl2.DataClassifier
import org.eclipse.ui.console.MessageConsoleStream

class RecordGeneration {
	new(){}
	def static void generateRecordType(SystemInstance instance, Model m, FiacreFactory factory, MessageConsoleStream _out) {
		for (DataImplementation data_impl : DataGeneration.getDataImplementations(instance)) {
			m.declarations.add(createRecordType(data_impl, m, factory, _out))
		}
	}
	
	def static TypeDeclaration createRecordType (DataImplementation data_impl, Model m, FiacreFactory factory, MessageConsoleStream _out){
		val TypeDeclaration type_decl = factory.createTypeDeclaration

			//val Classifier cl = d as Classifier
			_out.println(data_impl.name)
			type_decl.name = RTFiacreCore.uid(data_impl as Classifier, _out)
			_out.println(type_decl.name)
			val RecordType record_type = factory.createRecordType
			type_decl.value = record_type
 
			for (DataSubcomponent data_sub_compo : data_impl.ownedDataSubcomponents) {
				val RecordFields f1 = factory.createRecordFields
				record_type.fields.add(f1)
				val RecordFieldDeclaration rfd = factory.createRecordFieldDeclaration
				f1.fields.add(rfd)
				rfd.name = RTFiacreCore.uid(data_impl as Classifier, _out)+'_'+RTFiacreCore.id(data_sub_compo)
				
				//val DataType dt = data_sub_compo.allClassifier as DataType
				_out.println("data_sub_compo-----"+data_sub_compo.dataSubcomponentType)
				//f1.type = factory.createIntegerType; 
				f1.type = DataGeneration.genType(data_sub_compo.dataSubcomponentType as DataClassifier, factory, m, _out) 
			}
			type_decl
		}
	
	
}