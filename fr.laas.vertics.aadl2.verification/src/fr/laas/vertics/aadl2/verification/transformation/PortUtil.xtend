/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.eclipse.emf.common.util.EList
import org.osate.aadl2.DirectedFeature
import org.osate.aadl2.DirectionType
import org.osate.aadl2.EnumerationLiteral
import org.osate.aadl2.EventDataPort
import org.osate.aadl2.EventPort
import org.osate.aadl2.IntegerLiteral
import org.osate.aadl2.NamedValue
import org.osate.aadl2.Port
import org.osate.aadl2.PropertyAssociation
import org.osate.aadl2.instance.FeatureInstance

class PortUtil {
	new (){}
	
	// TODO to delete
	def static boolean isEventOrEventDataPort (FeatureInstance f){
		f.category.value==1 ||f.category.value==2
	}
	def static boolean isEventOrEventDataPort (Port p){
		p instanceof EventPort || p instanceof EventDataPort
	}
	// TODO to delete
	def static isInputPort(FeatureInstance f) {
		f.direction != DirectionType.OUT
	}
	def static isInputPort(Port p) {
		p.direction != DirectionType.OUT
	}
	//TODO to delete
	def static Iterable<FeatureInstance> withEventKind(Iterable<FeatureInstance> ports) {
		ports.filter[category.value==1 ||category.value==2]
	}
	
	def static Iterable<Port> withEventKindPorts(Iterable<Port> ports) { 
		ports.filter[p|p instanceof EventPort || p instanceof EventDataPort]
	}
	
	def static Iterable<Port> incomingPorts(Iterable<Port> ports) {
		ports.filter[p| ! ((p as DirectedFeature).isOut)]
	}
	
	def static Iterable<Port> outgoingPorts(Iterable<Port> ports) {
		ports.filter[p| ! ((p as DirectedFeature).isIn)]
	}
	def static Iterable<FeatureInstance> incoming(Iterable<FeatureInstance> ports) {
		ports.filter[direction != DirectionType.OUT]
	}
	def static Iterable<FeatureInstance> outgoing(Iterable<FeatureInstance> ports) {
		ports.filter[direction != DirectionType.IN]
	}
	
	//
	
	def static String getDequeueProtocolWithDefaultValue(EList<PropertyAssociation> pa) {
		if (hasDequeueProtocol(pa) && getQueueSizeWithDefaultValue(pa) > 1 ) 
			getDequeueProtocol(pa).name
		else
			vOneItem()
	}
		def static EnumerationLiteral getDequeueProtocol(EList<PropertyAssociation> list) {
		(AADLCore.getPropertyValueForKey(list,kDequeue_Protocol()).ownedValue as NamedValue).namedValue as EnumerationLiteral
	}
	
	def static boolean hasDequeueProtocol (EList<PropertyAssociation> lpa){
		AADLCore.hasPropertyValueForKey(lpa, kDequeue_Protocol())
	}
	//////
	def static String getOverflowProtocolWithDefaultValue(EList<PropertyAssociation> pa) {
		if (hasOverflowHandlingProtocol(pa)){
			getOverflowHandlingProtocol(pa).name
		}
		else{
			vOverflowDropOldest()
		}
	}
	
	def final static String vOverflowDropOldest() {
		"DropOldest"
	}
	def final static String vOverflowError(){
		"Error"
	}
	
	def static EnumerationLiteral getOverflowHandlingProtocol(EList<PropertyAssociation> list) {
		(AADLCore.getPropertyValueForKey(list,kOverflowHandlingProtocol()).ownedValue as NamedValue).namedValue as EnumerationLiteral
	}
	
	def static boolean hasOverflowHandlingProtocol(EList<PropertyAssociation> list) {
		AADLCore.hasPropertyValueForKey(list,kOverflowHandlingProtocol())
	}
	/////
	def static Integer getQueueSizeWithDefaultValue (EList<PropertyAssociation> lpa){
		if (hasQueueSize(lpa))
			getQueueSize(lpa)
		else
			1
	}
	
	def static Integer getQueueSize(EList<PropertyAssociation> list) {
		(AADLCore.getPropertyValueForKey(list,kQueueSize()).ownedValue as IntegerLiteral).value.intValue
	}
	
	def static hasQueueSize(EList<PropertyAssociation> lpa) {
		AADLCore.hasPropertyValueForKey(lpa, kQueueSize())
	}
	
	def static hasUrgency(EList<PropertyAssociation> lpa) {
		AADLCore.hasPropertyValueForKey(lpa, kUrgency())
	}
	def static IntegerLiteral getUrgency(EList<PropertyAssociation> list) {
		(AADLCore.getPropertyValueForKey(list,kUrgency()).ownedValue as IntegerLiteral)
	}
	
	def static Integer getUrgencyWithDefaultValue (EList<PropertyAssociation> lpa){
		if (hasUrgency(lpa))
			getUrgency(lpa).value.intValue
		else
			0
	}
	def static boolean isAllItems (Port p){
		return getDequeueProtocolWithDefaultValue(p.ownedPropertyAssociations)== vAllItem()	
	}
	
	def final static String kQueueSize() {
		"Queue_Size"
	}
	
	def final static String vOneItem() {
		"OneItem"
	}
	
	def final static String vAllItem() {
		"AllItems"
	}
	def final static String kDequeue_Protocol() {
		"Dequeue_Protocol"
	}
	def final static String kUrgency() {
		"Urgency"
	}
	def final static String kOverflowHandlingProtocol(){
		"Overflow_Handling_Protocol"
	}
	
	def static vOverflowDropNewest() {
		"DropNewest"
	}
	
}