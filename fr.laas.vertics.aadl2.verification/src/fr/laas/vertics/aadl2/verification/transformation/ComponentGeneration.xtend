/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.instance.SystemInstance
import fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import org.eclipse.ui.console.MessageConsoleStream

class ComponentGeneration {
	new(){}
	
	def static genMainComponent(SystemInstance instance, Model model, Model library, FiacreFactory factory, MessageConsoleStream _out) {
		//create the main component, add it in the fiacre model and specify it as the root declaration 
		val ComponentDeclaration cd = factory.createComponentDeclaration
		cd.name = RTFiacreCore.id(instance)
		model.declarations.add(cd)
		model.root = cd
	//	_out.println("instancessize"+SystemInstanceUtil.getAllComponentsInstances(instance).size)
	//	_out.println("getAllConnectionInstances"+instance.getAllConnectionInstances)
		VariableGeneration.genVariableDefinitions(instance, model, cd, factory, _out)
		PortGeneration.genPortDeclarations(instance, model, cd, factory, _out) 
		PriorityGeneration.genComponentPriority(instance, model, cd, factory, _out) 
		ParBlockGeneration.genComponentParBlock(instance,model, cd, library, factory, _out)
	}
	
}