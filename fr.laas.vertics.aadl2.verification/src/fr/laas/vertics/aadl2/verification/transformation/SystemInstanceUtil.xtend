package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.instance.ComponentInstance
import java.util.ArrayList
import org.osate.aadl2.instance.SystemInstance
import org.osate.aadl2.ComponentCategory
import org.eclipse.emf.common.util.EList
import java.util.List
import org.osate.aadl2.instance.ConnectionKind
import org.osate.aadl2.instance.FeatureInstance
import org.osate.aadl2.impl.DataPortImpl
import org.eclipse.ui.console.MessageConsoleStream

/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
class SystemInstanceUtil {
	new(){}
	
	//Returns the set of component instances that are translated to an RT-Fiacre process
	def static List<ComponentInstance> getComponentsGeneratingProcesses (SystemInstance instance, MessageConsoleStream _out){
			val ArrayList <ComponentInstance> componentsGeneratingProcesses = new ArrayList <ComponentInstance>()
			componentsGeneratingProcesses.addAll(SystemInstanceUtil.getThreads(instance, _out))
			componentsGeneratingProcesses.addAll(SystemInstanceUtil.getDevices(instance, _out))
			componentsGeneratingProcesses.toSet.sortBy[name]
	}
	
	def static getDataPortConnectionInstances(SystemInstance instance){
		instance.allConnectionInstances().filter[cnxi|cnxi.kind == ConnectionKind.PORT_CONNECTION]
		.filter[pcnxi|((pcnxi.destination) as FeatureInstance).feature.class.equals(DataPortImpl)].toSet
	}
	def static ArrayList <ComponentInstance> getProcesses(SystemInstance instance, MessageConsoleStream _out){
		
		val ArrayList <ComponentInstance> al = new ArrayList <ComponentInstance>()
		for (ComponentInstance ci : instance.componentInstances){
			al.addAll(ComponentInstanceUtil.getComponentProcesses(ci))
		}
		return al
	}
	
	def static ArrayList <ComponentInstance> getThreads	(SystemInstance instance, MessageConsoleStream _out){
		//all processes in a system instance
		val ArrayList <ComponentInstance> ci = SystemInstanceUtil.getProcesses(instance, _out)
		
		//all component instances in different processes
		val ArrayList <ComponentInstance> all_comp =  new ArrayList <ComponentInstance>()
		
		_out.print("getThreads: ")
		for (ComponentInstance compo : ci){
			all_comp.addAll(compo.componentInstances)
		}
		
		// first subset : threads in all processes
		val ArrayList <ComponentInstance> compo_thread =  new ArrayList <ComponentInstance>()
		compo_thread.addAll(all_comp.filter[category == ComponentCategory.THREAD])
		
		// all thread groups in different processes 
		val ArrayList <ComponentInstance> compo_group_thread =  new ArrayList <ComponentInstance>()
		compo_group_thread.addAll(all_comp.filter[category == ComponentCategory.THREAD_GROUP])
		
		
		val ArrayList <ComponentInstance> compo_group_thread_thread =  new ArrayList <ComponentInstance>()
		for (ComponentInstance compo_instance : compo_group_thread){
			compo_group_thread_thread.addAll(compo_instance.componentInstances.filter[category == ComponentCategory.THREAD])
		}
		val ArrayList <ComponentInstance> result = new ArrayList <ComponentInstance>()
		result.addAll(compo_thread)
		result.addAll(compo_group_thread_thread)
		return result
		
	}
	
	def static ArrayList <ComponentInstance> getDevices	(SystemInstance instance, MessageConsoleStream _out){
		//all processes in a system instance
		val EList <ComponentInstance> ci = instance.componentInstances
		
		//all component instances in different processes
		val ArrayList <ComponentInstance> all_devices =  new ArrayList <ComponentInstance>()
		all_devices.addAll(ci.filter[category == ComponentCategory.DEVICE])
		return all_devices	
	}	
	
}