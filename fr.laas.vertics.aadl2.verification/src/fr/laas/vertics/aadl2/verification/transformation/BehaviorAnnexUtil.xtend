/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.ba.aadlba.BehaviorAnnex
import org.osate.ba.aadlba.BehaviorState
import java.util.ArrayList
import java.util.Set

class BehaviorAnnexUtil {
	new(){}
	
	
	def static Set<BehaviorState> getDispatchStates(BehaviorAnnex annex) {
		val ArrayList<BehaviorState> array_bs = new ArrayList<BehaviorState> ()
		for (BehaviorState bs : getInitialState(annex) ){
			array_bs.add(bs)
		}
		for (BehaviorState bs : getCompleteStates(annex) ){
			array_bs.add(bs)
		}
		return array_bs.toSet
	}
	
	def static Iterable<BehaviorState> getCompleteStates(BehaviorAnnex annex) {
		annex.states.filter[s|s.complete==true]
	}
	
	def static Iterable<BehaviorState> getInitialState(BehaviorAnnex annex) {
		annex.states.filter[s|s.initial==true]
	}
	
	def static boolean isDispatchState(BehaviorState state) {
		state.isComplete || state.initial 
	}
	
}