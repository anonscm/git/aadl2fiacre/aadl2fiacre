/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import java.util.ArrayList
import org.osate.aadl2.instance.ComponentInstance
import org.osate.ba.aadlba.AssignmentAction
import org.osate.ba.aadlba.BehaviorAction
import org.osate.ba.aadlba.BehaviorVariableHolder
import org.osate.ba.aadlba.DataAccessHolder
import org.osate.ba.aadlba.DataComponentReference
import org.osate.ba.aadlba.DataPortHolder
import org.osate.ba.aadlba.DataSubcomponentHolder
import org.osate.ba.aadlba.ElseStatement
import org.osate.ba.aadlba.EventDataPortHolder
import org.osate.ba.aadlba.IfStatement
import org.osate.ba.aadlba.PortSendAction
import org.osate.ba.aadlba.TimedAction
import org.osate.ba.aadlba.impl.ElseStatementImpl
import fr.irit.fiacre.etoile.xtext.fiacre.AssignStatement
import fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement
import fr.irit.fiacre.etoile.xtext.fiacre.ExtendedConditionalStatement
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.NaturalLowerBound
import fr.irit.fiacre.etoile.xtext.fiacre.NaturalUpperBound
import fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.QueueExpression
import fr.irit.fiacre.etoile.xtext.fiacre.SendStatement
import fr.irit.fiacre.etoile.xtext.fiacre.Statement
import fr.irit.fiacre.etoile.xtext.fiacre.WaitStatement
import org.eclipse.ui.console.MessageConsoleStream
import org.osate.ba.declarative.CommAction
import org.osate.ba.aadlba.PortDequeueAction
import org.osate.ba.aadlba.PortFreezeAction
import org.osate.ba.aadlba.SharedDataAction
import org.osate.ba.aadlba.SubprogramCallAction
import org.osate.ba.aadlba.BehaviorActionBlock
import org.osate.ba.aadlba.LoopStatement
import org.osate.ba.aadlba.Target
import org.osate.ba.aadlba.DataAccessPrototypeHolder
import org.osate.ba.aadlba.EventPortHolder
import org.osate.ba.aadlba.FeaturePrototypeHolder
import org.osate.ba.aadlba.ParameterHolder
import org.osate.ba.aadlba.PortPrototypeHolder
import org.osate.ba.declarative.Reference
import org.osate.ba.aadlba.StructUnionElementHolder
import fr.irit.fiacre.etoile.xtext.fiacre.ReceiveStatement

class BehaviorActionGeneration {
	new() {
	}

	def static ArrayList<Statement> genAction(BehaviorAction action, ProcessDeclaration declaration,
		ComponentInstance instance, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		_out.println('Generating action statement begins.')
		val ArrayList<Statement> astmt = new ArrayList<Statement>()
		switch action {
			
			// Computation line 38-40
			TimedAction: {
				_out.println('Timed action: ' + (action as TimedAction))
				val WaitStatement ws = factory.createWaitStatement

				// TimeInterval  line 19-21
				val NaturalLowerBound nlb = factory.createNaturalLowerBound
				ws.left = nlb
				nlb.left = true
				nlb.value = Integer.parseInt(
					BehaviorExpressionGeneration.genExpression((action as TimedAction).lowerTime))

				//
				val NaturalUpperBound nub = factory.createNaturalUpperBound
				ws.right = nub
				nub.right = true
				if ((action as TimedAction).upperTime != null) {
					nub.value = Integer.parseInt(
						BehaviorExpressionGeneration.genExpression((action as TimedAction).upperTime))
				} else {
					nub.value = Integer.parseInt(
						BehaviorExpressionGeneration.genExpression((action as TimedAction).lowerTime))
				}
				astmt.add(ws)
			}
			
			// Assignment line 23-32
			AssignmentAction: {
				_out.print('Assignment action: ' + (action as AssignmentAction))

				//BehaviorExpressionGeneration.genExpression((action as AssignmentAction).valueExpression, declaration, instance, model, factory)
				val AssignStatement ass = factory.createAssignStatement
				astmt.add(ass)
				val Target target = (action as AssignmentAction).target
				switch target {
					
					DataSubcomponentHolder: {
						_out.println(' Data subcomponent holder ' + (target as DataSubcomponentHolder))
						ass.patterns.add(BehaviorPatternGeneration.genPattern(declaration, model, factory,
								(action as AssignmentAction).target as DataSubcomponentHolder, _out))
						ass.values.add(BehaviorExpressionGeneration.genExpression((action as AssignmentAction).valueExpression,
								declaration, instance, model, factory, _out))
					}
					
					DataComponentReference: {
						_out.println(' Data component reference ' + (target as DataComponentReference))
						// le dernier paramètre (data.size -1), c'est le dernier reference dans l'ensemble de data
						// qui va être le container dans le modèle fiacre
						// ce genre de chose est dû à aux manières dont la grammaire fiacre et la grammaire ba ont été définies 
						ass.patterns.add(
							BehaviorPatternGeneration.genPattern(declaration, model, factory,
								(action as AssignmentAction).target as DataComponentReference,
								((action as AssignmentAction).target as DataComponentReference).data.get(
									((action as AssignmentAction).target as DataComponentReference).data.size - 1), _out))
						ass.values.add(
							BehaviorExpressionGeneration.genExpression((action as AssignmentAction).valueExpression,
								declaration, instance, model, factory, _out))
					}
					
					DataPortHolder: {
						_out.println(' Data port holder ' + (target as DataPortHolder))
						ass.patterns.add(BehaviorPatternGeneration.genPattern(declaration, model, factory,
								(action as AssignmentAction).target as DataPortHolder, _out))
						val QueueExpression qe = factory.createQueueExpression
						ass.values.add(qe)
						qe.values.add(
							BehaviorExpressionGeneration.genExpression((action as AssignmentAction).valueExpression,declaration, instance, model, factory, _out))
					}
					EventDataPortHolder: {
						_out.println(' Event data port holder ' + (target as EventDataPortHolder))
						ass.patterns.add(BehaviorPatternGeneration.genPattern(declaration, model, factory,
								(action as AssignmentAction).target as EventDataPortHolder, _out))
						val QueueExpression qe = factory.createQueueExpression
						ass.values.add(qe)
						qe.values.add(
							BehaviorExpressionGeneration.genExpression((action as AssignmentAction).valueExpression,
								declaration, instance, model, factory, _out))
					}
					// behaviorvariableholder
					// dataaccessholder
					BehaviorVariableHolder: {
						_out.println(' Behavior variable holder ' + (target as BehaviorVariableHolder))
						ass.patterns.add(BehaviorPatternGeneration.genPattern(declaration, model, factory,
								(action as AssignmentAction).target as BehaviorVariableHolder, _out))
						ass.values.add(BehaviorExpressionGeneration.genExpression((action as AssignmentAction).valueExpression,
								declaration, instance, model, factory, _out))
						
					}
					DataAccessHolder : {
						_out.println(' Data access holder ' + (target as DataAccessHolder))
						ass.patterns.add(BehaviorPatternGeneration.genPattern(declaration, model, factory,
								(action as AssignmentAction).target as DataAccessHolder, _out))
						ass.values.add(BehaviorExpressionGeneration.genExpression((action as AssignmentAction).valueExpression,
								declaration, instance, model, factory, _out))
					}
					DataAccessPrototypeHolder : {
						// TODO: Implement this statement
						throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle DataAccessPrototypeHolder.")
						}
					EventPortHolder : {
						// TODO: Implement this statement
						throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle EventPortHolder.")
						}
					FeaturePrototypeHolder : {
						// TODO: Implement this statement
						throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle FeaturePrototypeHolder.")
						}
					ParameterHolder : {
						// TODO: Implement this statement
						throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle ParameterHolder.")
						}
					PortPrototypeHolder : {
						// TODO: Implement this statement
						throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle PortPrototypeHolder.")
						}
					Reference : {
						// TODO: Implement this statement
						throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle Reference.")
						}
					StructUnionElementHolder : {
						// TODO: Implement this statement
						throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle StructUnionElementHolder.")
						}
					default:{
						throw new ForbiddenConstructException(target,"The AADL2 verifier does not currently handle this construct.")
					}
				}
			}
			
			// IfThenElse line 46-53
			IfStatement:{
				_out.println('If statement ' + (action as IfStatement))
			 	val ArrayList<ElseStatement> elses = new  ArrayList<ElseStatement>()
				println("get" +getAllIfStatement((action as IfStatement),elses))
				println("else" + elses)
				// le conditional statement
	  			val ConditionalStatement conds = factory.createConditionalStatement
				astmt.add(conds)
				conds.conditions.add(BehaviorExpressionGeneration.genExpression((action as IfStatement).logicalValueExpression,declaration, instance, model, factory, _out))
				// the then statement
				conds.then = AutomatonGeneration.createFiacreSequenceStatement(AutomatonGeneration.genActionsStatements((action as IfStatement).behaviorActions, declaration,instance, model, factory, _out), factory)
				for (elseifstmt : elses.filter[esleif| esleif instanceof IfStatement].filter[ifs|(ifs as IfStatement).elif]){
					println("elseif" + elseifstmt)
					val ExtendedConditionalStatement excs = factory.createExtendedConditionalStatement
					conds.elseif.add(excs)
					excs.condition = BehaviorExpressionGeneration.genExpression((elseifstmt as IfStatement).logicalValueExpression,declaration, instance, model, factory, _out)
					excs.then = AutomatonGeneration.createFiacreSequenceStatement(AutomatonGeneration.genActionsStatements((elseifstmt as IfStatement).behaviorActions, declaration,instance, model, factory, _out), factory)
				}
				if(elses.exists[esleif| esleif.class == ElseStatementImpl])
				{
					conds.^else = AutomatonGeneration.createFiacreSequenceStatement(AutomatonGeneration.genActionsStatements((elses.get(elses.size-1) as ElseStatement).behaviorActions, declaration,instance, model, factory, _out), factory)		
				}	
			}
			PortSendAction:{
				_out.println('Port send action ' + (action as PortSendAction))
				if((action as PortSendAction).valueExpression == null)
				{
					astmt.add(BehaviorPatternGeneration.genPortPattern(declaration, model, factory, (action as PortSendAction).port, _out))	
				}
				else{
					val SendStatement ss = factory.createSendStatement
					astmt.add(ss)
					ss.port = BehaviorPatternGeneration.genPortPattern(declaration, model, factory, (action as PortSendAction).port, _out)
					ss.values.add(BehaviorExpressionGeneration.genExpression((action as PortSendAction).valueExpression, declaration, instance, model, factory, _out))
					
					val AssignStatement ass = factory.createAssignStatement
					astmt.add(ass)
					ass.patterns.add(BehaviorPatternGeneration.genPattern(declaration, model, factory, (action as PortSendAction).port, _out))
					val QueueExpression qe = factory.createQueueExpression
					ass.values.add(qe)
				}
			}
			
			PortDequeueAction:{
				_out.println('Port dequeue action ' + (action as PortDequeueAction))
				if ((action as PortDequeueAction).target === null)
				{
					astmt.add(BehaviorPatternGeneration.genPortPattern(declaration, model, factory, (action as PortDequeueAction).port, _out))	
				}
				else{
					println("receive port"+(action as PortDequeueAction).port.element.name)
					val ReceiveStatement ss = factory.createReceiveStatement
					astmt.add(ss)
					ss.port = BehaviorPatternGeneration.genPortPattern(declaration, model, factory, (action as PortDequeueAction).port, _out)
//					ss.values.add(BehaviorExpressionGeneration.genExpression((action as PortDequeueAction).target, declaration, instance, model, factory, _out))
				}
			}

//			PortDequeueAction : {
// TODO: Implement this statement
//				throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle PortDequeueAction at " + action.locationReference.toString + ".")
//			} 
			PortFreezeAction : {
				// TODO: Implement this statement
				throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle PortFreezeAction at " + action.locationReference.toString + ".")
			} 
			SharedDataAction : {
				// TODO: Implement this statement
				throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle SharedDataAction at " + action.locationReference.toString + ".")
			} 
			SubprogramCallAction : {
				// TODO: Implement this statement
				throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle SubprogramCallAction at " + action.locationReference.toString + ".")
			}
			BehaviorActionBlock : {
				// TODO: Implement this statement
				throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle BehaviorActionBlock at " + action.locationReference.toString + ".")
			}
			ElseStatement : {
				// TODO: Implement this statement
				throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle ElseStatement at " + action.locationReference.toString + ".")
			}
			LoopStatement : {
				// TODO: Implement this statement
				throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle LoopStatement at " + action.locationReference.toString + ".")
			}
			CommAction : {
				// TODO: Implement this statement
				throw new ForbiddenConstructException(action,"The AADL2 verifier does not handle CommAction: " + action.qualifiedName + ".")
			}

			default:{
				throw new ForbiddenConstructException(action,"The AADL2 verifier does not currently handle the construct at " + action.locationReference.toString + ".")
			}
		}
		_out.println('Generating action statement ends.')
		return astmt
	}
	
	def static ArrayList<ElseStatement> getAllIfStatement(ElseStatement statement, ArrayList<ElseStatement> statements) {
		
		if (statement instanceof IfStatement){
			statements.add(statement)
			if(statement.elseStatement != null){
				getAllIfStatement((statement as IfStatement).elseStatement, statements)
			}
		}
		else{
			statements.add(statement)
			statements
		}
	}

}
