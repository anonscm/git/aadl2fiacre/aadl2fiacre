/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import java.util.ArrayList
import org.osate.aadl2.EventDataPort
import org.osate.aadl2.EventPort
import org.osate.aadl2.Feature
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.SystemInstance
import fr.irit.fiacre.etoile.xtext.fiacre.Block
import fr.irit.fiacre.etoile.xtext.fiacre.BooleanType
import fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.CompositeBlock
import fr.irit.fiacre.etoile.xtext.fiacre.Composition
import fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance
import fr.irit.fiacre.etoile.xtext.fiacre.ExplicitArrayExpression
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import fr.irit.fiacre.etoile.xtext.fiacre.IdentifierExpression
import fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.IntegerType
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.NaturalLiteral
import fr.irit.fiacre.etoile.xtext.fiacre.PortSet
import fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ReferenceExpression
import fr.irit.fiacre.etoile.xtext.fiacre.ReferencedType
import fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.TypeInstance
import fr.irit.fiacre.etoile.xtext.fiacre.UnionTagDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance
import org.eclipse.ui.console.MessageConsoleStream

class InputPortProcessInstantiation {
	new() {
	}

	def static ArrayList<Block> genInputEventPortProcessControllers(SystemInstance instance, Model model,
		ComponentDeclaration declaration, Model library, FiacreFactory factory, MessageConsoleStream _out) {
		val ArrayList<Block> bl = new ArrayList<Block>()
		for (ci : SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out)) {
			bl.addAll(genInputPortProcessControllerInstantiation(ci, instance, model, declaration, library, factory, _out))
		}
		bl
	}

	def static ArrayList<Block> genInputPortProcessControllerInstantiation(ComponentInstance ci, SystemInstance instance,
		Model model, ComponentDeclaration declaration, Model library, FiacreFactory factory, MessageConsoleStream _out) {
		val CompositeBlock cb = factory.createCompositeBlock
		if (ComponentInstanceUtil.isSporadic(ci)) {
			val Composition compo = factory.createComposition
			cb.composition = compo
			val PortSet ps = factory.createPortSet
			compo.global = ps
			ps.allPorts = true
			_out.println("sporadic input" + ComponentInstanceUtil.getIncomingPortFeatureInstances(ci,_out))
			val ArrayList<Block> cb_b = new ArrayList<Block>()
			_out.println("raoua"+ComponentInstanceUtil.getIncomingPortFeatureInstances(ci,_out).map[fi|fi.feature.getQualifiedName()])
			_out.println("raoua2"+ComponentInstanceUtil.getPortConnectionInstances(ci,_out))
			for (fi : ComponentInstanceUtil.getIncomingPortFeatureInstances(ci,_out)) {
				compo.blocks.add(
					genInputPortProcessController(fi.feature, ci, instance, model, declaration, library, factory, _out))
			}
			for (ss : UnionGeneration.getTimeoutConstructorsWithValues(ci)) {
				compo.blocks.add(
					genTimeoutProcessControllerInstantiation(ss, ci, instance, model, declaration, library, factory, _out))
			}
			cb_b.add(cb)
			cb_b
		} else {
			if (ComponentInstanceUtil.isPeriodic(ci)) {
				_out.println("periodic input" + ComponentInstanceUtil.getIncomingPortFeatureInstances(ci,_out))
				val ArrayList<Block> ins_decls = new ArrayList<Block>()
				for (fi : ComponentInstanceUtil.getIncomingPortFeatureInstances(ci,_out)) {
					ins_decls.add(
						genInputPortProcessController(fi.feature, ci, instance, model, declaration, library, factory, _out))
				}
				ins_decls
			}
		}

	}

	def static InstanceDeclaration genTimeoutProcessControllerInstantiation(ArrayList<String> strings,
		ComponentInstance ci, SystemInstance instance, Model model, ComponentDeclaration declaration, Model library,
		FiacreFactory factory, MessageConsoleStream _out) {
		val InstanceDeclaration id = factory.createInstanceDeclaration
		val fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance compoi = factory.createComponentInstance
		id.instance = compoi
		compoi.component = library.declarations.filter(ProcessDeclaration).filter[process|process.name == "timeout"].
			get(0)

		// generics
		// type
		val TypeInstance ti = factory.createTypeInstance
		compoi.generics.add(ti)
		val ReferencedType reft = factory.createReferencedType
		ti.type = reft
		reft.type = model.declarations.filter(TypeDeclaration).filter[dec|
			dec.name == RTFiacreCore.unionTypeUid(ci)].get(0)

		// constr0
		val UnionTagInstance uti = factory.createUnionTagInstance
		compoi.generics.add(uti)

		uti.constr0 = true

		uti.head = model.eAllContents.toList.filter(UnionTagDeclaration).filter[utd|
			utd.name == UnionGeneration.getTimeoutConstructor(strings)].get(0)

		val ConstantInstance consi = factory.createConstantInstance
		compoi.generics.add(consi)
		val NaturalLiteral natl = factory.createNaturalLiteral
		consi.value = natl
		natl.value = Integer.parseInt(UnionGeneration.getTimeoutValue(strings))

		// ports
		// ports
		val IdentifierExpression ide_c = factory.createIdentifierExpression
		compoi.ports.add(ide_c)
		ide_c.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
			lpd.name == RTFiacreCore.processUid(ci) + "_c"].get(0)

		val IdentifierExpression ide_d = factory.createIdentifierExpression
		compoi.ports.add(ide_d)
		ide_d.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
			lpd.name == RTFiacreCore.processUid(ci) + "_d"].get(0)
		id
	}

	def static InstanceDeclaration genInputPortProcessController(Feature f, ComponentInstance ci,
		SystemInstance instance, Model model, ComponentDeclaration declaration, Model library, FiacreFactory factory, MessageConsoleStream _out) {
		if (f instanceof EventPort) {
			if (ComponentInstanceUtil.isSporadic(ci)) {
				_out.println("genInput")
				_out.println("spo_ieport_" + genDequeueMode(f) + "_" + genOverflowMode(f))
				val InstanceDeclaration id = factory.createInstanceDeclaration
				val fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance compoi = factory.createComponentInstance
				id.instance = compoi
				compoi.component = library.declarations.filter(ProcessDeclaration).filter[process|
					process.name == "spo_ieport_" + genDequeueMode(f) + "_" + genOverflowMode(f)].get(0)

				// generics
				// type
				val TypeInstance ti = factory.createTypeInstance
				compoi.generics.add(ti)
				val ReferencedType reft = factory.createReferencedType
				ti.type = reft
				reft.type = model.declarations.filter(TypeDeclaration).filter[dec|
					dec.name == RTFiacreCore.unionTypeUid(ci)].get(0)

				// const
				val ConstantInstance consi = factory.createConstantInstance
				compoi.generics.add(consi)
				val NaturalLiteral natl = factory.createNaturalLiteral
				consi.value = natl
				natl.value = PortUtil.getQueueSizeWithDefaultValue(f.ownedPropertyAssociations)

				// constr 
				_out.println("listcontents" + model.eAllContents.toList.filter(UnionTagDeclaration))
				val UnionTagInstance uti = factory.createUnionTagInstance
				compoi.generics.add(uti)
				if (PortUtil.getDequeueProtocolWithDefaultValue(f.ownedPropertyAssociations) == PortUtil.vOneItem ||
					PortUtil.getQueueSizeWithDefaultValue(f.ownedPropertyAssociations) == 1) {
					uti.constr0 = true
				} else {
					uti.constr1 = true
				}
				uti.head = model.eAllContents.toList.filter(UnionTagDeclaration).filter[utd|
					utd.name == RTFiacreCore.constructorUid(f, ci)].get(0)

				// ports
				val IdentifierExpression ide_c = factory.createIdentifierExpression
				compoi.ports.add(ide_c)
				ide_c.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
					lpd.name == RTFiacreCore.processUid(ci) + "_c"].get(0)

				// TODO vérifier ce separator [] ligne 66 
				for (fi : ComponentInstanceUtil.getPortSourcesForDestination(ci, f, _out).sortBy[name]) {
					val IdentifierExpression ide_fi = factory.createIdentifierExpression
					compoi.ports.add(ide_fi)
					ide_fi.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
						lpd.name == RTFiacreCore.processUid(fi) + "_" + RTFiacreCore.id(fi.feature)].get(0)
				}
				val IdentifierExpression ide_d = factory.createIdentifierExpression
				compoi.ports.add(ide_d)
				ide_d.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
					lpd.name == RTFiacreCore.processUid(ci) + "_d"].get(0)

				// parameters
				val ReferenceExpression refexpd = factory.createReferenceExpression
				compoi.parameters.add(refexpd)
				refexpd.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
					vardec.name == RTFiacreCore.processUid(ci) + "_d"].get(0)
				id
			} else {
				if (ComponentInstanceUtil.isPeriodic(ci)) {
					_out.println("genInput")
					_out.println("per_ieport_" + genDequeueMode(f) + "_" + genOverflowMode(f))
					val InstanceDeclaration id = factory.createInstanceDeclaration
					val fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance compoi = factory.createComponentInstance
					id.instance = compoi
					compoi.component = library.declarations.filter(ProcessDeclaration).filter[process|
						process.name == "per_ieport_" + genDequeueMode(f) + "_" + genOverflowMode(f)].get(0)

					// const
					val ConstantInstance consi = factory.createConstantInstance
					compoi.generics.add(consi)
					val NaturalLiteral natl = factory.createNaturalLiteral
					consi.value = natl
					natl.value = PortUtil.getQueueSizeWithDefaultValue(f.ownedPropertyAssociations)

					// ports
					// TODO vérifier ce separator [] ligne 106 
					for (fi : ComponentInstanceUtil.getPortSourcesForDestination(ci, f, _out).sortBy[name]) {
						val IdentifierExpression ide_fi = factory.createIdentifierExpression
						compoi.ports.add(ide_fi)
						ide_fi.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
							lpd.name == RTFiacreCore.processUid(fi) + "_" + RTFiacreCore.id(fi.feature)].get(0)
					}
					val IdentifierExpression ide_d = factory.createIdentifierExpression
					compoi.ports.add(ide_d)
					ide_d.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
						lpd.name == RTFiacreCore.processUid(ci) + "_d"].get(0)

					// parameters
					val ReferenceExpression refexpd = factory.createReferenceExpression
					compoi.parameters.add(refexpd)
					refexpd.declaration = declaration.variables.map[vars|vars.variables].flatten.filter[vardec|
						vardec.name == RTFiacreCore.processUid(ci) + "_" + RTFiacreCore.id(f)].get(0)

					id
				} else {
					throw new UnsupportedOperationException("unsupported compnent mode")
				}
			}
		} else {
			if (f instanceof EventDataPort) {
				if (ComponentInstanceUtil.isSporadic(ci)) {
					_out.println("genInput")
					_out.println("spo_iedport_" + genDequeueMode(f))
					val InstanceDeclaration id = factory.createInstanceDeclaration
					val fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance compoi = factory.createComponentInstance
					id.instance = compoi
					compoi.component = library.declarations.filter(ProcessDeclaration).filter[process|
						process.name == "spo_iedport_" + genDequeueMode(f)].get(0)

					// generics
					// type
					val TypeInstance ti = factory.createTypeInstance
					compoi.generics.add(ti)
					val ReferencedType reft = factory.createReferencedType
					ti.type = reft
					reft.type = model.declarations.filter(TypeDeclaration).filter[dec|
						dec.name == RTFiacreCore.unionTypeUid(ci)].get(0)

					//
					val TypeInstance tii = factory.createTypeInstance
					compoi.generics.add(tii)

					// if then else for each type
					if (DataGeneration.genPortDeclarationType(f, factory, model, _out).type instanceof ReferencedType) {
						val ReferencedType reftt = factory.createReferencedType
						tii.type = reftt
						reftt.type = (DataGeneration.genPortDeclarationType(f, factory, model, _out).type as ReferencedType).
							type
					} else {
						if (DataGeneration.genPortDeclarationType(f, factory, model, _out).type instanceof IntegerType) {
							val IntegerType inttype = factory.createIntegerType
							tii.type = inttype
						} else {
							if (DataGeneration.genPortDeclarationType(f, factory, model, _out).type instanceof BooleanType) {
								val BooleanType booltype = factory.createBooleanType
								tii.type = booltype
							}

						}
					}
					_out.println("DataGen" + DataGeneration.genPortDeclarationType(f, factory, model, _out).type)

					// const
					val ConstantInstance consi = factory.createConstantInstance
					compoi.generics.add(consi)
					val NaturalLiteral natl = factory.createNaturalLiteral
					consi.value = natl
					natl.value = PortUtil.getQueueSizeWithDefaultValue(f.ownedPropertyAssociations)

					// const
					val ConstantInstance consii = factory.createConstantInstance
					compoi.generics.add(consii)
					val NaturalLiteral natll = factory.createNaturalLiteral
					consii.value = natll
					natll.value = ComponentInstanceUtil.getPortSourcesForDestination(ci, f, _out).size

					// constr
					val UnionTagInstance uti = factory.createUnionTagInstance
					compoi.generics.add(uti)
					uti.constr1 = true
					uti.head = model.eAllContents.toList.filter(UnionTagDeclaration).filter[utd|
						utd.name == RTFiacreCore.constructorUid(f, ci)].get(0)

					// ports
					// _c
					val IdentifierExpression ide_c = factory.createIdentifierExpression
					compoi.ports.add(ide_c)
					ide_c.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
						lpd.name == RTFiacreCore.processUid(ci) + "_c"].get(0)

					// array ports
					if (ComponentInstanceUtil.getPortSourcesForDestination(ci, f, _out).size > 0) {
						val ExplicitArrayExpression eax = factory.createExplicitArrayExpression
						compoi.ports.add(eax)
						for (fi : ComponentInstanceUtil.getPortSourcesForDestination(ci, f, _out).sortBy[name]) {
							val IdentifierExpression id_fi = factory.createIdentifierExpression
							eax.values.add(id_fi)
							id_fi.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
								lpd.name == RTFiacreCore.processUid(fi) + "_" + RTFiacreCore.id(fi.feature)].get(0)
						}
						val ExplicitArrayExpression eaxx = factory.createExplicitArrayExpression
						compoi.ports.add(eaxx)
						for (cii : ComponentInstanceUtil.getPortSourcesForDestination(ci, f, _out).sortBy[name].map[a|
							Utils.eContainer(a, ComponentInstance)]) {
							val IdentifierExpression id_cii = factory.createIdentifierExpression
							eaxx.values.add(id_cii)
							id_cii.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
								lpd.name == RTFiacreCore.processUid(cii as ComponentInstance) + "_c"].get(0)

						}

						// parameters array : it is here because the condition is the same
						val ExplicitArrayExpression veax = factory.createExplicitArrayExpression
						compoi.parameters.add(veax)
						for (fi : ComponentInstanceUtil.getPortSourcesForDestination(ci, f, _out).sortBy[name]) {
							val ReferenceExpression v_id_fi = factory.createReferenceExpression
							veax.values.add(v_id_fi)
							v_id_fi.declaration = declaration.variables.map[psd|psd.variables].flatten.filter[lpd|
								lpd.name == RTFiacreCore.processUid(fi) + "_" + RTFiacreCore.id(fi.feature)].get(0)
						}
					}

					// _d
					val IdentifierExpression ide_d = factory.createIdentifierExpression
					compoi.ports.add(ide_d)
					ide_d.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
						lpd.name == RTFiacreCore.processUid(ci) + "_d"].get(0)

					// parameters
					val ReferenceExpression v_ide = factory.createReferenceExpression
					compoi.parameters.add(v_ide)
					v_ide.declaration = declaration.variables.map[psd|psd.variables].flatten.filter[lpd|
						lpd.name == RTFiacreCore.processUid(ci) + "_d"].get(0)

					val IdentifierExpression lib_id_exp = factory.createIdentifierExpression
					compoi.parameters.add(lib_id_exp)
					lib_id_exp.declaration = library.eAllContents.toList.filter(UnionTagDeclaration).filter[utd|
						utd.name ==
							genOverflowProtocol(
								PortUtil.getOverflowProtocolWithDefaultValue(f.ownedPropertyAssociations))].get(0)

					id
				} else {
					if (ComponentInstanceUtil.isPeriodic(ci)) {
						_out.println("genInput")
						_out.println("per_iedport_" + genDequeueMode(f))
						val InstanceDeclaration id = factory.createInstanceDeclaration
						val fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance compoi = factory.createComponentInstance
						id.instance = compoi
						compoi.component = library.declarations.filter(ProcessDeclaration).filter[process|
							process.name == "per_iedport_" + genDequeueMode(f)].get(0)

						// generics
						// type
						val TypeInstance tii = factory.createTypeInstance
						compoi.generics.add(tii)

						// if then else for each type
						if (DataGeneration.genPortDeclarationType(f, factory, model, _out).type instanceof ReferencedType) {
							val ReferencedType reftt = factory.createReferencedType
							tii.type = reftt
							reftt.type = (DataGeneration.genPortDeclarationType(f, factory, model, _out).type as ReferencedType).
								type
						} else {
							if (DataGeneration.genPortDeclarationType(f, factory, model, _out).type instanceof IntegerType) {
								val IntegerType inttype = factory.createIntegerType
								tii.type = inttype
							} else {
								if (DataGeneration.genPortDeclarationType(f, factory, model, _out).type instanceof BooleanType) {
									val BooleanType booltype = factory.createBooleanType
									tii.type = booltype
								}

							}
						}
						_out.println("DataGen" + DataGeneration.genPortDeclarationType(f, factory, model, _out).type)

						// const
						val ConstantInstance consi = factory.createConstantInstance
						compoi.generics.add(consi)
						val NaturalLiteral natl = factory.createNaturalLiteral
						consi.value = natl
						natl.value = PortUtil.getQueueSizeWithDefaultValue(f.ownedPropertyAssociations)

						// const
						val ConstantInstance consii = factory.createConstantInstance
						compoi.generics.add(consii)
						val NaturalLiteral natll = factory.createNaturalLiteral
						consii.value = natll
						natll.value = ComponentInstanceUtil.getPortSourcesForDestination(ci, f, _out).size

						// ports
						if (ComponentInstanceUtil.getPortSourcesForDestination(ci, f, _out).size > 0) {
							val ExplicitArrayExpression eax = factory.createExplicitArrayExpression
							compoi.ports.add(eax)
							for (fi : ComponentInstanceUtil.getPortSourcesForDestination(ci, f, _out).sortBy[name]) {
								val IdentifierExpression id_fi = factory.createIdentifierExpression
								eax.values.add(id_fi)
								id_fi.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
									lpd.name == RTFiacreCore.processUid(fi) + "_" + RTFiacreCore.id(fi.feature)].get(0)
							}
							val ExplicitArrayExpression eaxx = factory.createExplicitArrayExpression
							compoi.ports.add(eaxx)
							for (cii : ComponentInstanceUtil.getPortSourcesForDestination(ci, f, _out).sortBy[name].map[a|
								Utils.eContainer(a, ComponentInstance)]) {
								val IdentifierExpression id_cii = factory.createIdentifierExpression
								eaxx.values.add(id_cii)
								id_cii.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
									lpd.name == RTFiacreCore.processUid(cii as ComponentInstance) + "_c"].get(0)

							}

							// parameters array : it is here because the condition is the same
							val ExplicitArrayExpression veax = factory.createExplicitArrayExpression
							compoi.parameters.add(veax)
							for (fi : ComponentInstanceUtil.getPortSourcesForDestination(ci, f, _out).sortBy[name]) {
								val ReferenceExpression v_id_fi = factory.createReferenceExpression
								veax.values.add(v_id_fi)
								v_id_fi.declaration = declaration.variables.map[psd|psd.variables].flatten.filter[lpd|
									lpd.name == RTFiacreCore.processUid(fi) + "_" + RTFiacreCore.id(fi.feature)].get(0)
							}
						}

						// _d
						val IdentifierExpression ide_d = factory.createIdentifierExpression
						compoi.ports.add(ide_d)
						ide_d.declaration = declaration.localPorts.map[psd|psd.ports].flatten.filter[lpd|
							lpd.name == RTFiacreCore.processUid(ci) + "_d"].get(0)

						val ReferenceExpression v_ide = factory.createReferenceExpression
						compoi.parameters.add(v_ide)
						v_ide.declaration = declaration.variables.map[psd|psd.variables].flatten.filter[lpd|
							lpd.name == RTFiacreCore.processUid(ci) + "_" + RTFiacreCore.id(f)].get(0)

						val IdentifierExpression lib_id_exp = factory.createIdentifierExpression
						compoi.parameters.add(lib_id_exp)
						lib_id_exp.declaration = library.eAllContents.toList.filter(UnionTagDeclaration).filter[utd|
							utd.name ==
								genOverflowProtocol(
									PortUtil.getOverflowProtocolWithDefaultValue(f.ownedPropertyAssociations))].get(0)
						id
					} else {
						throw new UnsupportedOperationException("unsupported component mode")
					}
				}
			} else {
				throw new UnsupportedOperationException("not supported port type")
			}
		}
	}

	def static String genOverflowMode(Feature f) {
		if (f instanceof EventPort) {
			if (PortUtil.getOverflowProtocolWithDefaultValue(f.ownedPropertyAssociations) != PortUtil.vOverflowError()) {
				"noovf"
			} else {
				"ovf"
			}
		} else {
			throw new UnsupportedOperationException("FIXME genOverflowMode() for")
		}
	}

	def static String genDequeueMode(Feature port) {
		if (port instanceof EventPort || port instanceof EventDataPort) {
			if (PortUtil.getDequeueProtocolWithDefaultValue(port.ownedPropertyAssociations) == PortUtil.vOneItem ||
				PortUtil.getQueueSizeWithDefaultValue(port.ownedPropertyAssociations) == 1) {
				"one"
			} else {
				"all"
			}
		} else {
			throw new UnsupportedOperationException("FIXME genDequeueMode() for")
		}

	}

	def static String genOverflowProtocol(String s) {
		if (s == PortUtil.vOverflowDropNewest) {
			"DropNewest"
		} else {
			if (s == PortUtil.vOverflowDropOldest) {
				"DropOldest"
			} else {
				if (s == PortUtil.vOverflowError) {
					"Overflow"
				} else {
					throw new UnsupportedOperationException("ERROR unknow overflow protocol " + s)
				}

			}
		}
	}

}
