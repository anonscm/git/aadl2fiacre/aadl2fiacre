/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.Access
import org.osate.aadl2.AccessType
import org.osate.aadl2.DataAccess
import org.osate.aadl2.DataPort
import org.osate.aadl2.DataSubcomponent
import org.osate.aadl2.Feature
import org.osate.aadl2.Port
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.ConnectionInstanceEnd
import org.osate.aadl2.instance.ConnectionKind
import org.osate.aadl2.instance.FeatureInstance
import org.osate.aadl2.instance.SystemInstance
import fr.irit.fiacre.etoile.xtext.fiacre.BooleanType
import fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.ReferencedType
import fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.VariableDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration
import org.eclipse.ui.console.MessageConsoleStream

class VariableGeneration {
	new(){}
	def static boolean hasPorts (ComponentInstance ci){
		! (ProcessGeneration.getProcessArguments(ci).filter[fi|!(fi.feature instanceof DataAccess)].empty)
	}
	def static  genVariableDefinitions (SystemInstance instance, Model model,ComponentDeclaration cd, FiacreFactory factory, MessageConsoleStream _out) {
		if ( !(instance.getAllConnectionInstances().filter[kind == ConnectionKind.ACCESS_CONNECTION].empty) || 
			(SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out).exists[ci|ComponentInstanceUtil.isSporadic(ci) || (! ProcessGeneration.getProcessArguments(ci).empty)])
		)
		{	//dataaccess
		// TODO ça marche pas avec provided
			_out.println("true variable")
			for (ConnectionInstanceEnd cie : instance.getAllConnectionInstances().filter[kind == ConnectionKind.ACCESS_CONNECTION].map[coni|coni.source].toSet){ 
				_out.println("cie" + cie)
				val VariablesDeclaration vsd = factory.createVariablesDeclaration 
				cd.variables.add(vsd)
				vsd.type = DataGeneration.genType((cie as ComponentInstance).subcomponent as DataSubcomponent, factory, model, _out)
				//
				val VariableDeclaration vd = factory.createVariableDeclaration
				vsd.variables.add(vd)
				vd.name = RTFiacreCore.dataAccessUid((cie as ComponentInstance))	
			}				
			// variables
			for (ComponentInstance ci : SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out).filter[ci|hasPorts(ci)]){
				genVariableDefinition(ci, model, cd, factory, _out)
			}
		}	
	}
	
	def static genVariableDefinition(ComponentInstance instance, Model model, ComponentDeclaration cd, FiacreFactory factory, MessageConsoleStream _out) {
		_out.println("genVariableDefinition")
		if (ComponentInstanceUtil.isSporadic(instance)){
			val VariablesDeclaration vsd = factory.createVariablesDeclaration 
			cd.variables.add(vsd)	
			val ReferencedType refty = factory.createReferencedType
			vsd.type = refty
			refty.type = model.declarations.filter(TypeDeclaration).filter[td|td.name== RTFiacreCore.unionTypeUid(instance)].get(0)
			val VariableDeclaration vd = factory.createVariableDeclaration
			vsd.variables.add(vd)
			vd.name = RTFiacreCore.processUid(instance)+"_d"
			//
			genProcessVariableDefinitions(instance, model, cd, factory, _out)
		}
		else{
			if (ComponentInstanceUtil.isPeriodic(instance)){
					genProcessVariableDefinitions(instance, model, cd, factory, _out)			
			}	
		}
			
	}
	
	def static genProcessVariableDefinitions(ComponentInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		for (FeatureInstance fi : ProcessGeneration.getProcessArguments(instance).filter[fei|!(fei.feature instanceof DataAccess)])	{
			genVariableDefinition(fi.feature, RTFiacreCore.processUid(instance), model, declaration, factory, _out)
		}
	}
	def static genVariableDefinition (Feature f, String s, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out){
		_out.println ("genVD "+ s + f.class.toString)
		// TODO ça n'a pas de sens 
		// il y a un prb avec provides à voir
		// parce que dans le filter on supprime les DataAccess 
		if(f instanceof DataAccess && (f as Access).kind == AccessType.PROVIDES){ 
		
			
			_out.println ("class:da")
			val VariablesDeclaration vsd = factory.createVariablesDeclaration 
			declaration.variables.add(vsd)	
			vsd.type = DataGeneration.genSharedVariableType(f, model, factory, _out)
			
			val VariableDeclaration vd = factory.createVariableDeclaration
			vsd.variables.add(vd)
			vd.name = s+ "_" + RTFiacreCore.id(f)
		}
		else{
			if(f instanceof DataPort && PortUtil.isInputPort(f as Port)){
				_out.println ("class: in dp"+ " in "+(f as Port).in + " out "+(f as Port).out)
				
				val VariablesDeclaration vsd = factory.createVariablesDeclaration 
				declaration.variables.add(vsd)
				vsd.type = DataGeneration.genSharedVariableType(f, model, factory, _out)
			
				val VariableDeclaration vd = factory.createVariableDeclaration
				vsd.variables.add(vd)
				vd.name = s+ "_" + RTFiacreCore.id(f)
				
				
				val VariablesDeclaration vsd2 = factory.createVariablesDeclaration 
				declaration.variables.add(vsd2)	
				val BooleanType bt2 = factory.createBooleanType
				vsd2.type = bt2
			
				val VariableDeclaration vd2 = factory.createVariableDeclaration
				vsd2.variables.add(vd2)
				vd2.name = s+ "_" + RTFiacreCore.fresh_id(RTFiacreCore.id(f))
			}
			else{
				if(f instanceof Port){
					_out.println ("class:p" + " in "+(f as Port).in + " out "+(f as Port).out)
					val VariablesDeclaration vsd = factory.createVariablesDeclaration 
					declaration.variables.add(vsd)	
					vsd.type = DataGeneration.genSharedVariableType(f, model, factory, _out)
			
					val VariableDeclaration vd = factory.createVariableDeclaration
					vsd.variables.add(vd)
					vd.name = s+ "_" + RTFiacreCore.id(f)
				}
				else{
					if(f instanceof Feature){
						_out.println ("class:f")
					}
				}
			}
		}
	}
	
}