/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import fr.irit.fiacre.etoile.xtext.fiacre.Type
import org.osate.aadl2.DataType
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import org.osate.aadl2.DataClassifier
import org.osate.aadl2.DataImplementation
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ReferencedType
import org.osate.aadl2.instance.SystemInstance
import org.osate.aadl2.AadlPackage
import org.osate.aadl2.Port
import org.osate.aadl2.EventPort
import org.osate.aadl2.EventDataPort
import fr.irit.fiacre.etoile.xtext.fiacre.QueueType
import fr.irit.fiacre.etoile.xtext.fiacre.NaturalLiteral
import fr.irit.fiacre.etoile.xtext.fiacre.RangeType
import org.osate.aadl2.Feature
import org.osate.aadl2.DataPort
import fr.irit.fiacre.etoile.xtext.fiacre.ChannelType
import org.osate.aadl2.DataAccess
import org.osate.aadl2.DataSubcomponent
import org.eclipse.ui.console.MessageConsoleStream

class DataGeneration {
	new() {
	}

	def static Iterable<DataImplementation> getDataImplementations(SystemInstance instance) {

		val AadlPackage ap = instance.componentImplementation.eContainer.eContainer as AadlPackage
		return ap.ownedPublicSection.ownedClassifiers.filter(DataImplementation)
	}

	def static Type genType(DataSubcomponent dsc, FiacreFactory f, Model m, MessageConsoleStream _out) {
		genType(dsc.classifier as DataClassifier,f, m, _out)
	}
	
	/* TODO: ajouter un parametre MessageConsoleStream ds ces methodes */
	def static Type genType(DataClassifier dt, FiacreFactory f, Model m, MessageConsoleStream _out) {
		if (dt instanceof DataType)
			DataGeneration.genType(dt as DataType, f)
		else
			DataGeneration.genType(dt as DataImplementation, f, m, _out)
	}

	def static Type genType(DataType dt, FiacreFactory f) {
		if (dt.getQualifiedName() == "Base_Types::Integer") {
			//_out.println(dt)
			f.createIntegerType
		} else
			f.createBooleanType
	}

	def static Type genType(DataImplementation dt, FiacreFactory f, Model m, MessageConsoleStream _out) {
		val TypeDeclaration td = m.declarations.filter(TypeDeclaration).filter[name == RTFiacreCore.uid(dt,_out)].get(0)
		val ReferencedType ref_type = f.createReferencedType
		ref_type.type = td
		return ref_type
	}

	//Generates for a given port the RTFiacre constructor type argument usually used in union type definitions.
	def static Type genConstructorType(Port p, FiacreFactory factory, Model m, MessageConsoleStream _out) {
		//_out.println("entry point")
		if (p instanceof EventDataPort) {
			//_out.println("entry point if ")
			if (PortUtil.getDequeueProtocolWithDefaultValue(p.ownedPropertyAssociations) == PortUtil.vOneItem()) {
				genType(p.dataFeatureClassifier as DataClassifier, factory, m, _out)

			} else {
				if (PortUtil.getDequeueProtocolWithDefaultValue(p.ownedPropertyAssociations) == PortUtil.vAllItem()) {
					val QueueType qt = factory.createQueueType()
					val NaturalLiteral nl = factory.createNaturalLiteral
					nl.value = PortUtil.getQueueSizeWithDefaultValue(p.ownedPropertyAssociations)
					qt.size = nl
					qt.type = genType(p.dataFeatureClassifier as DataType, factory)
					return qt
				}

			}

		} else {
			//_out.println("entry point else ")
			if (p instanceof EventPort) {
				if (PortUtil.getDequeueProtocolWithDefaultValue(p.ownedPropertyAssociations) == PortUtil.vOneItem()) {
					//_out.println("testtttt---- event port one item")
					return null
				} else {
					if (PortUtil.getDequeueProtocolWithDefaultValue(p.ownedPropertyAssociations) == PortUtil.vAllItem()) {
						//_out.println("testtttt---- event port all item")
						val RangeType rt = factory.createRangeType
						val NaturalLiteral min = factory.createNaturalLiteral
						val NaturalLiteral max = factory.createNaturalLiteral
						max.value = PortUtil.getQueueSizeWithDefaultValue(p.ownedPropertyAssociations)
						min.value = 0
						rt.minimum = min
						rt.maximum = max
						return rt
					}
				}

			} else if (p instanceof Port) {
				//_out.println("return null")
				return null
			}

		}
	}

	def static ChannelType genPortDeclarationType(Feature feature, FiacreFactory factory, Model model, MessageConsoleStream _out) {
		if (feature instanceof EventPort) {
			val ChannelType cht = factory.createChannelType
			cht.in = true
			cht.out = true
			return cht
		} else {
			if (feature instanceof EventDataPort) {
				val ChannelType cht1 = factory.createChannelType
				cht1.type = genType(feature.dataFeatureClassifier as DataClassifier, factory, model, _out)
				cht1.in = true
				cht1.out = true
				return cht1
			} else {
				if (feature instanceof DataPort) {
					val ChannelType cht2 = factory.createChannelType
					cht2.type = genType(feature.dataFeatureClassifier as DataClassifier, factory, model, _out)
					cht2.in = true
					cht2.out = true
					return cht2
				} else {
					if (feature instanceof Feature) {
						throw new UnsupportedOperationException(
							"ERROR genPortDeclarationType " + feature.name + "  was not of type Port but was " +
								feature.class.name)
					}
				}
			}
		}
	}

	def static Type genSharedVariableType(Feature feature, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		if (feature instanceof EventPort) {
			if (PortUtil.getDequeueProtocolWithDefaultValue(feature.ownedPropertyAssociations) == PortUtil.vAllItem()) {
				//("testtttt---- event port all item")
				val RangeType rt = factory.createRangeType
				val NaturalLiteral min = factory.createNaturalLiteral
				val NaturalLiteral max = factory.createNaturalLiteral
				max.value = PortUtil.getQueueSizeWithDefaultValue(feature.ownedPropertyAssociations)
				min.value = 0
				rt.minimum = min
				rt.maximum = max
				return rt
			} else {
				//_out.println("testtttt---- event port all item")
				val RangeType rt = factory.createRangeType
				val NaturalLiteral min = factory.createNaturalLiteral
				val NaturalLiteral max = factory.createNaturalLiteral
				max.value = 1
				min.value = 0
				rt.minimum = min
				rt.maximum = max
				return rt
			}
		} else {
			if (feature instanceof EventDataPort) {
				if (PortUtil.getDequeueProtocolWithDefaultValue(feature.ownedPropertyAssociations) ==
					PortUtil.vOneItem()) {
					val QueueType qt = factory.createQueueType()
					val NaturalLiteral nl = factory.createNaturalLiteral
					nl.value = 1
					qt.size = nl
					qt.type = genType(feature.dataFeatureClassifier as DataClassifier, factory, model, _out)
					return qt

				} else {
					if (PortUtil.getDequeueProtocolWithDefaultValue(feature.ownedPropertyAssociations) ==
						PortUtil.vAllItem()) {
						val QueueType qt = factory.createQueueType()
						val NaturalLiteral nl = factory.createNaturalLiteral
						nl.value = PortUtil.getQueueSizeWithDefaultValue(feature.ownedPropertyAssociations)
						qt.size = nl
						qt.type = genType(feature.dataFeatureClassifier as DataClassifier, factory, model, _out)
						return qt
					}

				}
			} else {
				if (feature instanceof DataPort) {
					if (PortUtil.isInputPort(feature)) {
						genType(feature.dataFeatureClassifier as DataClassifier, factory, model, _out)
					} else {
						if (PortUtil.getDequeueProtocolWithDefaultValue(feature.ownedPropertyAssociations) ==
							PortUtil.vOneItem()) {
							val QueueType qt = factory.createQueueType()
							val NaturalLiteral nl = factory.createNaturalLiteral
							nl.value = 1
							qt.size = nl
							qt.type = genType(feature.dataFeatureClassifier as DataClassifier, factory, model, _out)
							return qt

						} else {
							if (PortUtil.getDequeueProtocolWithDefaultValue(feature.ownedPropertyAssociations) ==
								PortUtil.vAllItem()) {
								val QueueType qt = factory.createQueueType()
								val NaturalLiteral nl = factory.createNaturalLiteral
								nl.value = PortUtil.getQueueSizeWithDefaultValue(feature.ownedPropertyAssociations)
								qt.size = nl
								qt.type = genType(feature.dataFeatureClassifier as DataClassifier, factory, model, _out)
								return qt
							}

						}
					}
				} else {
					if (feature instanceof DataAccess) {
						genType(feature.dataFeatureClassifier as DataClassifier, factory, model, _out)

					//_out.println ("genSharedVariableType DataAccess")	
					} else {
						if (feature instanceof Feature) {
							throw new UnsupportedOperationException(
								"ERROR genPortDeclarationType " + feature.name + "  was not of type Port but was " +
									feature.class.name)
						}
					}
				}

			}
		}
	}

}
