/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import java.util.ArrayList
import org.osate.aadl2.Access
import org.osate.aadl2.AccessType
import org.osate.aadl2.DataAccess
import org.osate.aadl2.DataPort
import org.osate.aadl2.Feature
import org.osate.aadl2.Port
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.SystemInstance
import fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance
import fr.irit.fiacre.etoile.xtext.fiacre.ExplicitArrayExpression
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import fr.irit.fiacre.etoile.xtext.fiacre.IdentifierExpression
import fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.NaturalLiteral
import fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ReferenceExpression
import fr.irit.fiacre.etoile.xtext.fiacre.ReferencedType
import fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.TypeInstance
import org.eclipse.ui.console.MessageConsoleStream

class ProcessInstantiationGeneration {
	new(){
		      
	}
	
	def static InstanceDeclaration genSchedulerInstantiation(SystemInstance instance, Model model, ComponentDeclaration declaration, Model library, FiacreFactory factory, MessageConsoleStream _out) {
		val InstanceDeclaration id = factory.createInstanceDeclaration		
		
		//
		val fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance ci = factory.createComponentInstance
		id.instance = ci
		
		ci.component = library.declarations.filter(ProcessDeclaration).filter[process|process.name=="scheduler"].get(0)
		
		val ConstantInstance const = factory.createConstantInstance
		val NaturalLiteral nb = factory.createNaturalLiteral
		const.value = nb
		nb.value = SystemInstanceUtil.getThreads(instance, _out).size
		ci.generics.add(const)
		val ExplicitArrayExpression e_ports = factory.createExplicitArrayExpression
		for(componentInstance : SystemInstanceUtil.getThreads(instance, _out)){
			val IdentifierExpression port = factory.createIdentifierExpression
			port.declaration = declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(componentInstance)+"_e"].get(0)
			e_ports.values.add(port)
		}
		ci.ports.add(e_ports)
		val ExplicitArrayExpression c_ports = factory.createExplicitArrayExpression
		for(componentInstance : SystemInstanceUtil.getThreads(instance, _out)){
			val IdentifierExpression port = factory.createIdentifierExpression
			port.declaration = declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(componentInstance)+"_c"].get(0)
			c_ports.values.add(port)
		}
		ci.ports.add(c_ports)	
		return id
	}
	
	def static InstanceDeclaration genProcessControllerInstantiation(ComponentInstance aadl_ci, SystemInstance instance, Model model, ComponentDeclaration declaration, Model library, FiacreFactory factory, MessageConsoleStream _out) {
		val InstanceDeclaration id = factory.createInstanceDeclaration		
		val fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance ci = factory.createComponentInstance
		id.instance = ci 
		ci.component = getProcessControllerName(aadl_ci,library,_out)
		if(ComponentInstanceUtil.isSporadic(aadl_ci)){
			val TypeInstance type_ins = factory.createTypeInstance
			ci.generics.add(type_ins)
			val ReferencedType ref_type = factory.createReferencedType
			type_ins.type = ref_type
			ref_type.type = model.declarations.filter(TypeDeclaration).filter[type|type.name==RTFiacreCore.unionTypeUid(aadl_ci)].get(0)
		}
		for (port: getProcessControllerPorts(aadl_ci,declaration)){
			val IdentifierExpression identifier = factory.createIdentifierExpression
			identifier.declaration = port
			ci.ports.add(identifier)		
		}
		id
	}
	
	def static ArrayList<PortDeclaration> getProcessControllerPorts(ComponentInstance instance, ComponentDeclaration declaration) {
		val ArrayList<PortDeclaration> ports = new ArrayList<PortDeclaration>()
		ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(instance)+"_d"].get(0))
		ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(instance)+"_c"].get(0))
		ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(instance)+"_dl"].get(0))
		ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(instance)+"_w"].get(0))
		if(ComponentInstanceUtil.hasDeadline(instance)){

			ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(instance)+"_wd"].get(0))
		}
		if(ThreadUtil.hasDispatchOffset(instance)){
			ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(instance)+"_o"].get(0))
		}
		ports
	}
	
	def static ProcessDeclaration getProcessControllerName(ComponentInstance instance, Model library, MessageConsoleStream _out) {
		var String process_name =""
		if (ComponentInstanceUtil.isPeriodic(instance)){
// TODO voir cette histoire de_offset avec JPB
//			if(ThreadUtil.hasDispatchOffset(instance)){
//				process_name="periodic_controller_offset"	
//			}
//			else{
				process_name="periodic_controller"
			}
//		}
		else{
			process_name="sporadic_controller"
		}
		if(ComponentInstanceUtil.hasDeadline(instance)){
			process_name=process_name+"_dl"
		}
		_out.println("process_name "+process_name)
		getProcessDeclaration(process_name, library)
	}
	
	def static ProcessDeclaration getProcessDeclaration(String name, Model library){
		library.declarations.filter(ProcessDeclaration).filter[process|process.name==name].get(0)
	}
	
	def static InstanceDeclaration genProcessInstantiation(ComponentInstance aadl_ci, SystemInstance instance2, Model model, ComponentDeclaration declaration, Model library, FiacreFactory factory, MessageConsoleStream _out) {
		val InstanceDeclaration id = factory.createInstanceDeclaration		
		val fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance ci = factory.createComponentInstance
		id.instance = ci 
		ci.component =  model.declarations.filter(ProcessDeclaration).filter[process|process.name==RTFiacreCore.processUid(aadl_ci)].get(0)
		
		val IdentifierExpression identifier_d = factory.createIdentifierExpression
		identifier_d.declaration = declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(aadl_ci)+"_d"].get(0)
		ci.ports.add(identifier_d)	
		
		val IdentifierExpression identifier_e = factory.createIdentifierExpression
		identifier_e.declaration = declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(aadl_ci)+"_e"].get(0)
		ci.ports.add(identifier_e)
		
		val IdentifierExpression identifier_c = factory.createIdentifierExpression
		identifier_c.declaration = declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(aadl_ci)+"_c"].get(0)
		ci.ports.add(identifier_c)
		
		val IdentifierExpression identifier_dl = factory.createIdentifierExpression
		identifier_dl.declaration = declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(aadl_ci)+"_dl"].get(0)
		ci.ports.add(identifier_dl)
		for(port : ProcessGeneration.getPortParams(aadl_ci)){
			val IdentifierExpression identifier_port = factory.createIdentifierExpression
			identifier_port.declaration = declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(aadl_ci)+"_"+RTFiacreCore.id(port)].get(0)
			ci.ports.add(identifier_port)
		}	
		if(ComponentInstanceUtil.isSporadic(aadl_ci) || (! ProcessGeneration.getProcessArguments(aadl_ci).empty)){
			genProcessArgumentInstantiations(aadl_ci, instance2, model, declaration, ci, factory, _out)
		}
		id
	}
	
	def static genProcessArgumentInstantiations(ComponentInstance aadl_ci, SystemInstance instance2, Model model, ComponentDeclaration declaration, fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance ci, FiacreFactory factory, MessageConsoleStream _out) {
		if(ComponentInstanceUtil.isSporadic(aadl_ci)||ComponentInstanceUtil.isPeriodic(aadl_ci)){
			if(ComponentInstanceUtil.isSporadic(aadl_ci)){
				val ReferenceExpression ref = factory.createReferenceExpression
				ref.declaration = declaration.variables.map[vs|vs.variables].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(aadl_ci)+"_d"].get(0)
				ci.parameters.add(ref)
			}
			else{
				genProcessArguments(aadl_ci, instance2, model, declaration,ci, factory, _out)
			}	
		}
		
	}
	
	def static genProcessArguments(ComponentInstance aadl_ci, SystemInstance instance2, Model model, ComponentDeclaration declaration, fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance ci, FiacreFactory factory, MessageConsoleStream _out) {
		for(fi: ProcessGeneration.getProcessArguments(aadl_ci)){
			genProcessArgument(fi.feature, aadl_ci, instance2,model, declaration, ci, factory, _out)
		}
	}
	
	def static genProcessArgument(Feature feature, ComponentInstance aadl_ci, SystemInstance instance2, Model model, ComponentDeclaration declaration, fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance ci, FiacreFactory factory, MessageConsoleStream _out){
		if(feature instanceof DataAccess){
			if ((feature as Access).kind == AccessType.REQUIRES){
				_out.println("here "+ RTFiacreCore.dataAccessUid(ComponentInstanceUtil.getDataAccessProvider(aadl_ci,feature)))
				val ReferenceExpression ref1 = factory.createReferenceExpression
				ref1.declaration = declaration.variables.map[vs|vs.variables].flatten.filter[vdecl|vdecl.name==RTFiacreCore.dataAccessUid(ComponentInstanceUtil.getDataAccessProvider(aadl_ci,feature))].get(0)
				ci.parameters.add(ref1)
			}
			else{
				_out.println("provided")
			}
		}
		else{
			if(feature instanceof DataPort && PortUtil.isInputPort(feature as Port)){
				val ReferenceExpression ref1 = factory.createReferenceExpression
				ref1.declaration = declaration.variables.map[vs|vs.variables].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(aadl_ci)+"_"+RTFiacreCore.id(feature)].get(0)
				ci.parameters.add(ref1)
				
				val ReferenceExpression ref2 = factory.createReferenceExpression
				ref2.declaration = declaration.variables.map[vs|vs.variables].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(aadl_ci)+"_"+RTFiacreCore.fresh_id(RTFiacreCore.id(feature))].get(0)
				ci.parameters.add(ref2)
			}
			else{
				if(feature instanceof Port){
					val ReferenceExpression ref1 = factory.createReferenceExpression
					ref1.declaration = declaration.variables.map[vs|vs.variables].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(aadl_ci)+"_"+RTFiacreCore.id(feature)].get(0)
					ci.parameters.add(ref1)
				}
				else{
					if(feature instanceof Feature){
						_out.println("dd")
					}
				}
			}
			
		}
		
	}
	
	def static InstanceDeclaration genProcessUrgencyInstantiation(ComponentInstance aadl_ci, SystemInstance instance2, Model model, ComponentDeclaration declaration, Model library, FiacreFactory factory, MessageConsoleStream _out) {
		val InstanceDeclaration id = factory.createInstanceDeclaration		
		val fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance ci = factory.createComponentInstance
		id.instance = ci 
		ci.component =  model.declarations.filter(ProcessDeclaration).filter[process|process.name==RTFiacreCore.urgencyProcessUid(aadl_ci)].get(0)
		val IdentifierExpression identifier_d = factory.createIdentifierExpression
		identifier_d.declaration = declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(aadl_ci)+"_d"].get(0)
		ci.ports.add(identifier_d)	
		id
	}	
	
}