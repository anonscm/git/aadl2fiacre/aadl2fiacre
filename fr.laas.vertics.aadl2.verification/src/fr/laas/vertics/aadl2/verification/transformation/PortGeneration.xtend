/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.instance.SystemInstance
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import org.osate.aadl2.instance.ComponentInstance
import fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ReferencedType
import fr.irit.fiacre.etoile.xtext.fiacre.ChannelType
import fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.NaturalLowerBound
import fr.irit.fiacre.etoile.xtext.fiacre.NaturalUpperBound
import org.osate.aadl2.Port
import org.eclipse.ui.console.MessageConsoleStream

class PortGeneration {
	new() {
	}

	def static genPortDeclarations(SystemInstance instance, Model model, ComponentDeclaration declaration,
		FiacreFactory factory, MessageConsoleStream _out) {
		for (ComponentInstance ci : SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out)) {
			genPredefinedPortDeclarations(ci, model, declaration, factory, _out)
			genUserDefinedPortDeclarations(ci,model,declaration,factory, _out)
		}
	}

	def static genPredefinedPortDeclarations(ComponentInstance instance, Model model, ComponentDeclaration declaration,
		FiacreFactory factory, MessageConsoleStream _out) {
		val PortDeclaration pdecl_d = factory.createPortDeclaration
		pdecl_d.name = RTFiacreCore.processUid(instance) + "_d"

		val PortDeclaration pdecl_e = factory.createPortDeclaration
		pdecl_e.name = RTFiacreCore.processUid(instance) + "_e"

		val PortDeclaration pdecl_c = factory.createPortDeclaration
		pdecl_c.name = RTFiacreCore.processUid(instance) + "_c"

		val PortDeclaration pdecl_dl = factory.createPortDeclaration
		pdecl_dl.name = RTFiacreCore.processUid(instance) + "_dl"

		if (ComponentInstanceUtil.isSporadic(instance)) {

			// just for the "_d" port
			val LocalPortsDeclaration lpsd = factory.createLocalPortsDeclaration
			declaration.localPorts.add(lpsd)
			lpsd.ports.add(pdecl_d)
			val ChannelType chaty = factory.createChannelType
			chaty.in = true
			chaty.out = true
			lpsd.type = chaty

			val NaturalLowerBound nlbd = factory.createNaturalLowerBound
			lpsd.left = nlbd
			nlbd.left = true

			val NaturalUpperBound nubd = factory.createNaturalUpperBound
			lpsd.right = nubd
			nubd.right = true

			val ReferencedType refty = factory.createReferencedType
			chaty.type = refty
			refty.type = model.declarations.filter(TypeDeclaration).filter[td|
				td.name == RTFiacreCore.unionTypeUid(instance)].get(0)

			// the rest
			val LocalPortsDeclaration lps_rest = factory.createLocalPortsDeclaration
			declaration.localPorts.add(lps_rest)
			lps_rest.ports.add(pdecl_e)
			lps_rest.ports.add(pdecl_c)
			lps_rest.ports.add(pdecl_dl)
			val ChannelType chaty2 = factory.createChannelType
			lps_rest.type = chaty2

			val NaturalLowerBound nlb = factory.createNaturalLowerBound
			lps_rest.left = nlb
			nlb.left = true

			val NaturalUpperBound nub = factory.createNaturalUpperBound
			lps_rest.right = nub
			nub.right = true
		} else {
			val LocalPortsDeclaration lpsall = factory.createLocalPortsDeclaration
			declaration.localPorts.add(lpsall)
			lpsall.ports.add(pdecl_d)
			lpsall.ports.add(pdecl_e)
			lpsall.ports.add(pdecl_c)
			lpsall.ports.add(pdecl_dl)
			val ChannelType chaty = factory.createChannelType
			lpsall.type = chaty

			val NaturalLowerBound nlb = factory.createNaturalLowerBound
			lpsall.left = nlb
			nlb.left = true

			val NaturalUpperBound nub = factory.createNaturalUpperBound
			lpsall.right = nub
			nub.right = true
		}

		_out.println("period " + ComponentInstanceUtil.getPeriod(instance))

		// generate the "_w" port declaration
		val PortDeclaration pdecl_w = factory.createPortDeclaration
		pdecl_w.name = RTFiacreCore.processUid(instance) + "_w"
		val LocalPortsDeclaration lpsw = factory.createLocalPortsDeclaration
		declaration.localPorts.add(lpsw)
		lpsw.ports.add(pdecl_w)

		val ChannelType chatyw = factory.createChannelType
		lpsw.type = chatyw

		val NaturalLowerBound nlbw = factory.createNaturalLowerBound
		lpsw.left = nlbw
		nlbw.left = true
		nlbw.value = Utils.safeLongToInt(ComponentInstanceUtil.getPeriod(instance))

		val NaturalUpperBound nubw = factory.createNaturalUpperBound
		lpsw.right = nubw
		nubw.right = true
		nubw.value = Utils.safeLongToInt(ComponentInstanceUtil.getPeriod(instance))

		// generate the "_wd" port declaration
		if (ComponentInstanceUtil.hasDeadline(instance)) {
			val PortDeclaration pdecl_wd = factory.createPortDeclaration
			pdecl_wd.name = RTFiacreCore.processUid(instance) + "_wd"
			val LocalPortsDeclaration lpswd = factory.createLocalPortsDeclaration
			declaration.localPorts.add(lpswd)
			lpswd.ports.add(pdecl_wd)

			val ChannelType chatywd = factory.createChannelType
			lpswd.type = chatywd

			val NaturalLowerBound nlbwd = factory.createNaturalLowerBound
			lpswd.left = nlbwd
			nlbwd.left = true
			nlbwd.value = Utils.safeLongToInt(ComponentInstanceUtil.getDeadline(instance))

			val NaturalUpperBound nubwd = factory.createNaturalUpperBound
			lpswd.right = nubwd
			nubwd.right = true
			nubwd.value = Utils.safeLongToInt(ComponentInstanceUtil.getDeadline(instance))

		}
		if (ThreadUtil.hasDispatchOffset(instance)) {
			val PortDeclaration pdecl_o = factory.createPortDeclaration
			pdecl_o.name = RTFiacreCore.processUid(instance) + "_o"
			val LocalPortsDeclaration lpso = factory.createLocalPortsDeclaration
			declaration.localPorts.add(lpso)
			lpso.ports.add(pdecl_o)

			val ChannelType chatyo = factory.createChannelType
			lpso.type = chatyo

			val NaturalLowerBound nlbo = factory.createNaturalLowerBound
			lpso.left = nlbo
			nlbo.left = true
			nlbo.value = Utils.safeLongToInt(ThreadUtil.getDispatchOffset(instance))

			val NaturalUpperBound nubo = factory.createNaturalUpperBound
			lpso.right = nubo
			nubo.right = true
			nubo.value = Utils.safeLongToInt(ThreadUtil.getDispatchOffset(instance))

		}
	}

	def static genUserDefinedPortDeclarations(ComponentInstance instance, Model model, ComponentDeclaration declaration,
		FiacreFactory factory, MessageConsoleStream _out) {
		_out.println("getports"+ComponentInstanceUtil.getPorts(instance))
		for (Port p : ComponentInstanceUtil.getPorts(instance)){
			val LocalPortsDeclaration lpsdp = factory.createLocalPortsDeclaration
			declaration.localPorts.add(lpsdp)
			
			val PortDeclaration pdecl_p = factory.createPortDeclaration
			pdecl_p.name = RTFiacreCore.processUid(instance) + "_"+RTFiacreCore.id(p)
			
			lpsdp.ports.add(pdecl_p)

			lpsdp.type = DataGeneration.genPortDeclarationType(p,factory, model, _out)
			
			val NaturalLowerBound nlbo = factory.createNaturalLowerBound
			lpsdp.left = nlbo
			nlbo.left = true

			val NaturalUpperBound nubo = factory.createNaturalUpperBound
			lpsdp.right = nubo
			nubo.right = true
		}
		
	}
	// TODO verifier les in out des port
}
