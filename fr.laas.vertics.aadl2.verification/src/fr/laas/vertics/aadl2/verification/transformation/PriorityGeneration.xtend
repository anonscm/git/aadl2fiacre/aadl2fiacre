/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import java.util.ArrayList
import java.util.List
import java.util.Set
import org.osate.aadl2.Port
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.SystemInstance
import fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.PriorityDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.PriorityGroup
import org.eclipse.ui.console.MessageConsoleStream

class PriorityGeneration {
	new(){}
	
	def static genComponentPriority(SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		genDispatchPriority(instance, model, declaration, factory, _out)
		genExecutePriority(instance, model, declaration, factory, _out)
		genDeadlinePriority(instance, model, declaration, factory, _out)
		genDeadlineGreaterThanWaitDeadlinePriority(instance, model, declaration, factory, _out)
		genDispatchGreaterThanExecutePriority(instance, model, declaration, factory, _out)
		genUrgentTransitionsPriority(instance, model, declaration, factory, _out)
		genInternalTransitionsPriority(instance, model, declaration, factory, _out)
	}
	
	def static genInternalTransitionsPriority(SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		if(SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out).size > 1){
			genInternalTransitionsPriorityAux(instance, model, declaration, factory, _out)
		}
	}
	
	def static genInternalTransitionsPriorityAux(SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		val List<PortDeclaration> ports = getUrgentTransitionsHigherPriorityPorts(instance, model, declaration, factory, _out)
		ports.sortBy[name.endsWith("_w") ||name.endsWith("_o") || name.endsWith("_wd")]
		val PriorityDeclaration prio_decl = factory.createPriorityDeclaration
		declaration.priorities.add(prio_decl)
		for (port: ports){
			val PriorityGroup pg = factory.createPriorityGroup
			prio_decl.groups.add(pg)
			pg.ports.add(port)
		}
	}
	
	def static genUrgentTransitionsPriority(SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		genUrgentTransitionsPriorityAux(instance, model, declaration, factory, _out)
	}
	
	def static genUrgentTransitionsPriorityAux(SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		val PriorityDeclaration prio_decl = factory.createPriorityDeclaration
		declaration.priorities.add(prio_decl)
		val PriorityGroup pg1 = factory.createPriorityGroup
		prio_decl.groups.add(pg1)
		pg1.ports.addAll(getUrgentTransitionsHigherPriorityPorts(instance, model, declaration,factory, _out))
		val PriorityGroup pg2 = factory.createPriorityGroup
		prio_decl.groups.add(pg2)
		pg2.ports.addAll(getUrgentTransitionsLowerPriorityPorts(instance, model, declaration,factory, _out))
	}
	
	def static List<PortDeclaration> getUrgentTransitionsLowerPriorityPorts(SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		val List<ComponentInstance> component_array = SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out)
		val ArrayList<PortDeclaration> ports = new ArrayList<PortDeclaration>()
		for (ci :component_array){
			val String context = RTFiacreCore.processUid(ci)
			val Iterable<Port> incomingedp = PortUtil.outgoingPorts(PortUtil.withEventKindPorts(ComponentInstanceUtil.getPorts(ci)).toSet)
			for (p :incomingedp){
				ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==context+"_"+RTFiacreCore.id(p)].get(0))
			}
			ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==context+"_d"].get(0))
			ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==context+"_e"].get(0))
			ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==context+"_c"].get(0))
			ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==context+"_dl"].get(0))
		}
		_out.println("outgoing "+ports)
		return ports.sortBy[name]
	}
	
	def static List<PortDeclaration> getUrgentTransitionsHigherPriorityPorts(SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		val List<ComponentInstance> component_array = SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out)
		val ArrayList<PortDeclaration> ports = new ArrayList<PortDeclaration>()
		for (ci :component_array){
			val String context = RTFiacreCore.processUid(ci)
			val Iterable<Port> incomingedp = PortUtil.incomingPorts(PortUtil.withEventKindPorts(ComponentInstanceUtil.getPorts(ci)).toSet)
			for (p :incomingedp){
				ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==context+"_"+RTFiacreCore.id(p)].get(0))
			}
			ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==context+"_w"].get(0))
			if(ThreadUtil.hasDispatchOffset(ci)){
				ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==context+"_o"].get(0))
			}
			if(ComponentInstanceUtil.hasDeadline(ci)){
				ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==context+"_wd"].get(0))
			}
		}
		_out.println("incoming "+ports)
		return ports.sortBy[name]
	}
	
	def static genDispatchGreaterThanExecutePriority(SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		val PriorityDeclaration prio_decl = factory.createPriorityDeclaration
		declaration.priorities.add(prio_decl)
		val PriorityGroup pg1 = factory.createPriorityGroup
		prio_decl.groups.add(pg1)
		pg1.ports.addAll(genPrioritySubGroup("_d",instance, model, declaration,factory,_out))
		val PriorityGroup pg2 = factory.createPriorityGroup
		prio_decl.groups.add(pg2)
		pg2.ports.addAll(genPrioritySubGroup("_e",instance, model, declaration,factory,_out))
	}
	
	def static genDeadlineGreaterThanWaitDeadlinePriority(SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		val PriorityDeclaration prio_decl = factory.createPriorityDeclaration
		declaration.priorities.add(prio_decl)
		val PriorityGroup pg1 = factory. createPriorityGroup
		prio_decl.groups.add(pg1)
		pg1.ports.addAll(genPrioritySubGroup("_dl",instance, model, declaration, factory, _out))
		if(hasDeadlines(instance, _out)){
			pg1.ports.addAll(genDLPrioritySubGroup("_wd",instance, model, declaration, factory, _out))
		}
		val PriorityGroup pg2 = factory. createPriorityGroup
		prio_decl.groups.add(pg2)
		pg2.ports.addAll(genPrioritySubGroup("_d",instance, model, declaration, factory, _out))
	}
	
	def static genExecutePriority(SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		if (getExecutePriorityThreadPartition(instance, declaration, factory, _out).size > 1){
			val PriorityDeclaration prio_decl = factory.createPriorityDeclaration
		declaration.priorities.add(prio_decl)
		for (compo_ite :getExecutePriorityThreadPartition(instance, declaration, factory, _out)){
			val PriorityGroup prio_grp = factory.createPriorityGroup
			prio_decl.groups.add(prio_grp)
			for(ci : compo_ite){
				prio_grp.ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(ci)+"_e"].get(0))
			}	
		}
		_out.println("number "+prio_decl.groups.size)
	}
	}
	
	def static ArrayList<Iterable<ComponentInstance>> getExecutePriorityThreadPartition(SystemInstance instance, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		val ArrayList<ComponentInstance> threads = SystemInstanceUtil.getThreads(instance, _out)
		val Set<Integer> priorities = threads.map[th|Utils.safeLongToInt(ComponentInstanceUtil.getPriorityWithDefaultValue(th))].sort.toSet
		_out.println("priorities"+ priorities)
		val ArrayList<Iterable<ComponentInstance>> prioSortedSeq = new ArrayList<Iterable<ComponentInstance>>()
		for(priority : priorities){
			prioSortedSeq.add(threads.filter[th|Utils.safeLongToInt(ComponentInstanceUtil.getPriorityWithDefaultValue(th))==priority])
		}
		return prioSortedSeq
		
	}
	
	def static genDispatchPriority(SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out){
		if(SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out).size > 1){
			genPriorityDeclaration("_d",instance, model, declaration, factory, _out)	
		}
	}
	
	def static genPriorityDeclaration(String suffix, SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		val PriorityDeclaration prio_d = factory.createPriorityDeclaration
		declaration.priorities.add(prio_d)
		for (ComponentInstance ci : SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out).sortBy[comp|ComponentInstanceUtil.isSporadic(comp)]){
			val PriorityGroup priog = factory.createPriorityGroup
			prio_d.groups.add(priog)
			priog.ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(ci)+suffix].get(0))
		}	 	
	}
	
	def static ArrayList<PortDeclaration> genDLPrioritySubGroup(String suffix, SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		val ArrayList<PortDeclaration> ports = new ArrayList<PortDeclaration> ()	
		for (ComponentInstance ci : SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out).filter[ci|ThreadUtil.hasThreadDeadline(ci)].sortBy[comp|ComponentInstanceUtil.isSporadic(comp)]){
			ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(ci)+suffix].get(0))
		}	 
		return ports
	}
	def static ArrayList<PortDeclaration> genPrioritySubGroup(String suffix, SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out) {
		val ArrayList<PortDeclaration> ports = new ArrayList<PortDeclaration> ()	
		for (ComponentInstance ci : SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out).sortBy[comp|ComponentInstanceUtil.isSporadic(comp)]){
			ports.add(declaration.localPorts.map[lp|lp.ports].flatten.filter[pdecl|pdecl.name==RTFiacreCore.processUid(ci)+suffix].get(0))
		}	 
		return ports
	}
		def static genDeadlinePriority(SystemInstance instance, Model model, ComponentDeclaration declaration, FiacreFactory factory, MessageConsoleStream _out){
		if(SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out).size > 1){
			PriorityGeneration.genPriorityDeclaration("_dl",instance, model, declaration, factory, _out)	
		}
	}
	def static boolean hasDeadlines(SystemInstance instance, MessageConsoleStream _out){
		SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out).exists[component|ThreadUtil.hasThreadDeadline(component)]
	}
}