/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.IntegerLiteral
import org.osate.aadl2.instance.ComponentInstance
import org.osate.ba.aadlba.BehaviorBooleanLiteral
import org.osate.ba.aadlba.BehaviorElement
import org.osate.ba.aadlba.BehaviorIntegerLiteral
import org.osate.ba.aadlba.BehaviorTime
import org.osate.ba.aadlba.Factor
import org.osate.ba.aadlba.Relation
import org.osate.ba.aadlba.SimpleExpression
import org.osate.ba.aadlba.Term
import org.osate.ba.aadlba.Value
import org.osate.ba.aadlba.ValueExpression
import fr.irit.fiacre.etoile.xtext.fiacre.Conjunction
import fr.irit.fiacre.etoile.xtext.fiacre.Disjunction
import fr.irit.fiacre.etoile.xtext.fiacre.Expression
import fr.irit.fiacre.etoile.xtext.fiacre.FalseLiteral
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.NaturalLiteral
import fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.TrueLiteral
import fr.irit.fiacre.etoile.xtext.fiacre.ComparisonEqual
import fr.irit.fiacre.etoile.xtext.fiacre.ComparisonNotEqual
import fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesser
import fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesserEqual
import fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreater
import fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreaterEqual
import org.osate.ba.aadlba.DataSubcomponentHolder
import fr.irit.fiacre.etoile.xtext.fiacre.IdentifierExpression
import org.osate.aadl2.DataSubcomponent
import org.osate.ba.aadlba.EventDataPortHolder
import org.osate.aadl2.Feature
import fr.irit.fiacre.etoile.xtext.fiacre.Addition
import fr.irit.fiacre.etoile.xtext.fiacre.Substraction
import fr.irit.fiacre.etoile.xtext.fiacre.UnaryPlusExpression
import fr.irit.fiacre.etoile.xtext.fiacre.UnaryMinusExpression
import fr.irit.fiacre.etoile.xtext.fiacre.Multiplication
import fr.irit.fiacre.etoile.xtext.fiacre.Division
import fr.irit.fiacre.etoile.xtext.fiacre.Modulo
import fr.irit.fiacre.etoile.xtext.fiacre.UnaryNegationExpression
import org.osate.ba.aadlba.DataPortHolder
import org.osate.ba.aadlba.DataComponentReference
import org.osate.ba.aadlba.DataHolder
import fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessExpression
import org.osate.ba.aadlba.BehaviorVariableHolder
import org.osate.ba.aadlba.DataAccessHolder
import org.osate.ba.aadlba.BehaviorVariable
import org.osate.aadl2.DataAccess
import org.osate.ba.aadlba.PortFreshValue
import org.osate.ba.aadlba.PortCountValue
import org.osate.aadl2.EventPort
import org.osate.aadl2.EventDataPort
import fr.irit.fiacre.etoile.xtext.fiacre.UnaryLengthExpression
import org.osate.ba.aadlba.EventPortHolder
import org.eclipse.ui.console.MessageConsoleStream
import org.osate.ba.aadlba.BehaviorActionBlock
import org.osate.ba.aadlba.BehaviorActions
import org.osate.ba.aadlba.BehaviorAnnex
import org.osate.ba.aadlba.BehaviorCondition

class BehaviorExpressionGeneration {
	new() {
	}

	def static String genExpression(BehaviorTime crt) {
		((crt.integerValue as BehaviorIntegerLiteral).value * (crt.unit.factor as IntegerLiteral).value).toString
	}

	// pour une value expression ve
	/*
	 * if (ve.isSetLogicalOperators)
	 * 	 une ou plusieurs expression logiques
	 * else
	 *   donc une seule expression relation
	 *  if(ve.relations.get(0).isSetrelationOperator)
	 *  il y a first expression et second expression
	 * else
	 *  il y a just first
	 *  if( ve.relations.get(0).firstexpression.isSetAddingOperators)
	 *  il y a un ensemble de terms
	 * else
	 * il y a un seul terme
	 *  if(ve.relations.get(0).firstexpression.terms.get(0).isSetMultiplingOperatos)
	 *  il ya un ensemble de factors
	 * else
	 * il y a un seul factor
	 * 
	 */
	def static Expression genExpression(BehaviorElement be, ProcessDeclaration declaration, ComponentInstance instance,
		Model model, FiacreFactory factory, MessageConsoleStream _out) {
		_out.println('Expression generation starts: ' + be)
		switch be {
			// BinaryExpression
			ValueExpression: {
				_out.println('Value Expression')
				if ((be as ValueExpression).isSetLogicalOperators) {
					val expression = createMultipleLogicalExpression((be as ValueExpression), declaration, instance, model, factory, _out)
					_out.println('Expression generation ends.')
					return expression
				} else {
					val expression = genExpression((be as ValueExpression).relations.get(0), declaration, instance, model, factory, _out)
					_out.println('Expression generation ends.')
					return expression
				}	
			}
			Relation: {
				_out.print('Relation: ')
				if ((be as Relation).isSetRelationalOperator) {
					switch (be as Relation).relationalOperator.getName() {
						case "Equal": {
							_out.println('Equal')
							val ComparisonEqual com_eq = factory.createComparisonEqual
							com_eq.left = genExpression((be as Relation).firstExpression, declaration, instance, model,
								factory, _out)
							print((be as Relation).relationalOperator.getName())
							com_eq.right = genExpression((be as Relation).secondExpression, declaration, instance, model,
								factory, _out)
							_out.println('Expression generation ends.')
							return com_eq
						}
						case "NotEqual": {
							_out.println('Not Equal')
							val ComparisonNotEqual com_not_eq = factory.createComparisonNotEqual
							com_not_eq.left = genExpression((be as Relation).firstExpression, declaration, instance,
								model, factory, _out)
							print((be as Relation).relationalOperator.getName())
							com_not_eq.right = genExpression((be as Relation).secondExpression, declaration, instance,
								model, factory, _out)
							_out.println('Expression generation ends.')
							return com_not_eq
						}
						case "LessThan": {
							_out.println('Less Than')
							val ComparisonLesser com_lesser = factory.createComparisonLesser
							com_lesser.left = genExpression((be as Relation).firstExpression, declaration, instance,
								model, factory, _out)
							print((be as Relation).relationalOperator.getName())
							com_lesser.right = genExpression((be as Relation).secondExpression, declaration, instance,
								model, factory, _out)
							_out.println('Expression generation ends.')
							return com_lesser
						}
						case "LessOrEqualThan": {
							_out.println('Lesser or Equal Than')
							val ComparisonLesserEqual com_lesser_eq = factory.createComparisonLesserEqual
							com_lesser_eq.left = genExpression((be as Relation).firstExpression, declaration, instance,
								model, factory, _out)
							print((be as Relation).relationalOperator.getName())
							com_lesser_eq.right = genExpression((be as Relation).secondExpression, declaration, instance,
								model, factory, _out)
							_out.println('Expression generation ends.')
							return com_lesser_eq
						}
						case "GreaterThan": {
							_out.println('Greater Than')
							val ComparisonGreater com_grea = factory.createComparisonGreater
							com_grea.left = genExpression((be as Relation).firstExpression, declaration, instance, model,
								factory, _out)
							print((be as Relation).relationalOperator.getName())
							com_grea.right = genExpression((be as Relation).secondExpression, declaration, instance,
								model, factory, _out)
							_out.println('Expression generation ends.')
							return com_grea
						}
						case "GreaterOrEqualThan": {
							_out.println('Greater or Equal Than')
							val ComparisonGreaterEqual com_grea_eq = factory.createComparisonGreaterEqual
							com_grea_eq.left = genExpression((be as Relation).firstExpression, declaration, instance,
								model, factory, _out)
							print((be as Relation).relationalOperator.getName())
							com_grea_eq.right = genExpression((be as Relation).secondExpression, declaration, instance,
								model, factory, _out)
							_out.println('Expression generation ends.')
							return com_grea_eq
						}
					}
				} else {
					_out.println('Unknown case of Relation')
					val expression = genExpression((be as Relation).firstExpression, declaration, instance, model, factory, _out)
					_out.println('Expression generation ends.')
					return expression 
				}
			}
			SimpleExpression: {
				if ((be as SimpleExpression).isSetBinaryAddingOperators) {
					val expression = createMultipleSimpleExpression((be as SimpleExpression), declaration, instance, model, factory, _out)
					_out.println('Expression generation ends.')
					return expression
				} else {
					if ((be as SimpleExpression).isSetUnaryAddingOperator) {
						switch (be as SimpleExpression).unaryAddingOperator.getName() {
							case "Plus": {
								_out.println('Unary Plus')
								print((be as SimpleExpression).unaryAddingOperator.getName())
								val UnaryPlusExpression upe = factory.createUnaryPlusExpression
								upe.child = genExpression((be as SimpleExpression).terms.get(0), declaration, instance,
									model, factory, _out)
								_out.println('Expression generation ends.')
								return upe
							}
							case "Minus": {
								_out.println('Unary Minus')
								print((be as SimpleExpression).unaryAddingOperator.getName())
								val UnaryMinusExpression upe = factory.createUnaryMinusExpression
								upe.child = genExpression((be as SimpleExpression).terms.get(0), declaration, instance,
									model, factory, _out)
								_out.println('Expression generation ends.')
								return upe
							}
						}
					} else {
						_out.println('Unknown case of Simple Expression')
						val expression = genExpression((be as SimpleExpression).terms.get(0), declaration, instance, model, factory, _out)
						_out.println('Expression generation ends.')
						return expression
					}
				}
			}
			Term: {
				_out.println('Term')
				if ((be as Term).setMultiplyingOperators) {
					val expression = createMultipleTerm((be as Term), declaration, instance, model, factory, _out)
					_out.println('Expression generation ends.')
					return expression
				} else {
					val expression = genExpression((be as Term).factors.get(0), declaration, instance, model, factory, _out)
					_out.println('Expression generation ends.')
					return expression
				}
			}
			// Unary Expression
			Factor: {
				_out.println('Factor')
				if ((be as Factor).isSetUnaryBooleanOperator) {
					// il y a un seul opérateur "not"
					print((be as Factor).unaryBooleanOperator.getName())
					val UnaryNegationExpression une = factory.createUnaryNegationExpression
					une.child = genExpression((be as Factor).firstValue, declaration, instance, model, factory, _out)
					_out.println('Expression generation ends.')
					return une
				} else {
					if ((be as Factor).isSetUnaryNumericOperator) {
						val expression = genExpression((be as Factor).firstValue, declaration, instance, model, factory, _out)
						_out.println('Expression generation ends.')
						return expression
					} else {
						if ((be as Factor).isSetBinaryNumericOperator) {
							val Multiplication mult = factory.createMultiplication
							mult.left = genExpression((be as Factor).firstValue, declaration, instance, model, factory, _out)
							mult.right = genExpression((be as Factor).secondValue, declaration, instance, model, factory, _out)
							_out.println('Expression generation ends.')
							return mult
						} else {
							val expression = genExpression((be as Factor).firstValue, declaration, instance, model, factory, _out)
							_out.println('Expression generation ends.')
							return expression
						}
					}
				}
			}
			BehaviorIntegerLiteral: {
				_out.println('Integer Literal')
				_out.println((be as BehaviorIntegerLiteral).value.toString())

				val NaturalLiteral nl = factory.createNaturalLiteral
				nl.value = Utils.safeLongToInt((be as BehaviorIntegerLiteral).value)
				_out.println('Expression generation ends.')
				return nl

			}
			BehaviorBooleanLiteral: {
				_out.println('Boolean Literal')
				_out.println((be as BehaviorBooleanLiteral).getValue().toString())
				if ((be as BehaviorBooleanLiteral).getValue()) {
					val TrueLiteral tl = factory.createTrueLiteral
					_out.println('Expression generation ends.')
					return tl
				} else {
					val FalseLiteral fl = factory.createFalseLiteral
					_out.println('Expression generation ends.')
					return fl
				}
			}
			DataSubcomponentHolder: {
				_out.println('Data Subcomponent Holder')
				val IdentifierExpression ip = factory.createIdentifierExpression
				ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration,
					be.element as DataSubcomponent, _out)
				_out.println('Expression generation ends.')
				return ip
			}
			EventDataPortHolder: {
				_out.println('Event Data Port Holder')
				val IdentifierExpression ip = factory.createIdentifierExpression
				ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration,
					be.element as Feature, _out)
				_out.println('Expression generation ends.')
				return ip
			}
			EventPortHolder: {
				_out.println('Event Port Holder')
				val IdentifierExpression ip = factory.createIdentifierExpression
				ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration,
					be.element as Feature, _out)
				_out.println('Expression generation ends.')
				return ip
			}
			DataPortHolder: {
				_out.println('Data Port Holder')
				val IdentifierExpression ip = factory.createIdentifierExpression
				ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration,
					be.element as Feature, _out)
				_out.println('Expression generation ends.')
				return ip
			}
			DataComponentReference: {
				_out.println('Data Component Reference')
				// le dernier paramètre (data.size -1), c'est le dernier reference dans l'ensemble de data
				// qui va être le container dans le modèle fiacre
				// ce genre de chose est dû à aux manières dont la grammaire fiacre et la grammaire ba ont été définies 
				val expression = genExpression(declaration, model, factory,be as DataComponentReference,
					(be as DataComponentReference).data.get((be  as DataComponentReference).data.size - 1), _out)
				_out.println('Expression generation ends.')
				return expression
			}
			BehaviorVariableHolder: {
				_out.println('Variable Holder')
				val IdentifierExpression ip = factory.createIdentifierExpression
				ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration,be.element as BehaviorVariable, _out)
				_out.println('Expression generation ends.')
				return ip		
			}
			DataAccessHolder : {
				_out.println('Data Access Holder')
				val IdentifierExpression ip = factory.createIdentifierExpression
				ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration,be.element as DataAccess, _out)
				_out.println('Expression generation ends.')
				return ip	
			}
			PortFreshValue :{
				_out.println('Port Fresh Value')
				val IdentifierExpression ip = factory.createIdentifierExpression
				ip.declaration = BehaviorReferencedElementGeneration.genReferencedFreshElement(declaration,be.element as Feature, _out)
				_out.println('Expression generation ends.')
				return ip
			}
			PortCountValue:{
				_out.println('Port Count Value')
				if((be as PortCountValue).port instanceof EventPort){
					val IdentifierExpression ip = factory.createIdentifierExpression
					ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration,be.element as Feature, _out)
					_out.println('Expression generation ends.')
					return ip	
				} else {
					if((be as PortCountValue).port instanceof EventDataPort){
						val UnaryLengthExpression ule = factory.createUnaryLengthExpression
						val IdentifierExpression ip = factory.createIdentifierExpression
						ule.child = ip
						ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration,be.element as Feature, _out)
						_out.println('Expression generation ends.')
						return ule
					}
				}
			}

			default:{
				throw new ForbiddenConstructException(be,"The AADL2 verifier does not currently handle this construct.")
			}
		}
	}
	
	def static IdentifierExpression genExpression (ProcessDeclaration declaration, Model model, FiacreFactory factory, DataComponentReference dcr, DataHolder data, MessageConsoleStream _out){
	_out.println('Expression generation starts.')
	val int index = dcr.data.indexOf(data)
		if (!(dcr.data.get(0) == data)) {

			// ce n'est pas le premier (donc vi di etc.)
			val RecordAccessExpression rae = factory.createRecordAccessExpression
			rae.field = BehaviorReferencedElementGeneration.genReferencedElement(model,dcr,data, _out)
			rae.child = genExpression(declaration, model, factory, dcr, dcr.data.get(index - 1), _out)
			_out.println('Expression generation ends.')
			return rae
		} else {
			// si c'est le dernier (x)
			val IdentifierExpression ip = factory.createIdentifierExpression
			ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration, data.element as DataSubcomponent, _out)
			_out.println('Expression generation ends.')
			return ip
		}	
	}
	
	def static createMultipleTerm(Term term, ProcessDeclaration declaration, ComponentInstance instance, Model model,
		FiacreFactory factory, MessageConsoleStream _out) {
		_out.println('Multiple Term Expression creation begins.')
		val name = term.multiplyingOperators.get(0).getName()
		switch name {
			case "Multiply": {
				_out.println('Multiply')
				val Multiplication e = factory.createMultiplication
				e.left = genExpression(term.factors.get(0), declaration, instance, model, factory, _out)
				e.right = genExpression(term.factors.get(1), declaration, instance, model, factory, _out)
				val expression = BehaviorExpressionGeneration.addMultipleTerm(2, e, term, declaration, instance, model, factory, _out)
				_out.println('Multiple Term Expression creation ends.')
				return expression
			}
			case "Divide": {
				_out.println('Divide')
				val Division e = factory.createDivision
				e.left = genExpression(term.factors.get(0), declaration, instance, model, factory, _out)
				e.right = genExpression(term.factors.get(1), declaration, instance, model, factory, _out)
				val expression = BehaviorExpressionGeneration.addMultipleTerm(2, e, term, declaration, instance, model, factory, _out)
				_out.println('Multiple Term Expression creation ends.')
				return expression
			}
			case "Mod": {
				_out.println('Mod')
				val Modulo e = factory.createModulo
				e.left = genExpression(term.factors.get(0), declaration, instance, model, factory, _out)
				e.right = genExpression(term.factors.get(1), declaration, instance, model, factory, _out)
				val expression = BehaviorExpressionGeneration.addMultipleTerm(2, e, term, declaration, instance, model, factory, _out)
				_out.println('Multiple Term Expression creation ends.')
				return expression
			}
			default:{
				throw new ForbiddenConstructException(name,"The AADL2 verifier does not currently handle this construct.")
			}
		// REM operation to do
		}
	}

	def static Expression addMultipleTerm(int i, Expression exp, Term term, ProcessDeclaration declaration,
		ComponentInstance instance, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		_out.println('Multiple Term Expression addition begins.')
		if (term.factors.size == i) {
			_out.println('Multiple Term Expression addition ends.')
			return exp
		} else {
			val name = term.multiplyingOperators.get(i-1).getName()
			switch name {
				case "Multiply": {
					_out.println('Multiply')
					val Multiplication e = factory.createMultiplication
					e.left = exp
					e.right = genExpression(term.factors.get(i), declaration, instance, model, factory, _out)
					val expression = BehaviorExpressionGeneration.addMultipleTerm(i + 1, e, term, declaration, instance, model, factory, _out)
					_out.println('Multiple Term Expression addition ends.')
					return expression
				}
				case "Divide": {
					_out.println('Divide')
					val Division e = factory.createDivision
					e.left = exp
					e.right = genExpression(term.factors.get(i), declaration, instance, model, factory, _out)
					val expression = BehaviorExpressionGeneration.addMultipleTerm(i + 1, e, term, declaration, instance, model, factory, _out)
					_out.println('Multiple Term Expression addition ends.')
					return expression
				}
				case "Mod": {
					_out.println('Mod')
					val Modulo e = factory.createModulo
					e.left = exp
					e.right = genExpression(term.factors.get(i), declaration, instance, model, factory, _out)
					val expression = BehaviorExpressionGeneration.addMultipleTerm(i + 1, e, term, declaration, instance, model, factory, _out)
					_out.println('Multiple Term Expression addition ends.')
					return expression
				}
				default:{
					throw new ForbiddenConstructException(name,"The AADL2 verifier does not currently handle this construct.")
				}
			// REM operation to do
			}
		}
	}

	def static createMultipleSimpleExpression(SimpleExpression sexpression, ProcessDeclaration declaration,
		ComponentInstance instance, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		_out.println('Multiple Simple Expression creation begins.')
		val name = sexpression.binaryAddingOperators.get(0).getName()
		switch name {
			case "Plus": {
				_out.println('Plus')
				val Addition e = factory.createAddition
				e.left = genExpression(sexpression.terms.get(0), declaration, instance, model, factory, _out)
				e.right = genExpression(sexpression.terms.get(1), declaration, instance, model, factory, _out)
				val expression = BehaviorExpressionGeneration.addMultipleSimpleExpression(2, e, sexpression, declaration, instance, model,
					factory, _out)
				_out.println('Multiple Simple Expression creation ends.')
				return expression
			}
			case "Minus": {
				_out.println('Minus')
				val Substraction e = factory.createSubstraction
				e.left = genExpression(sexpression.terms.get(0), declaration, instance, model, factory,_out)
				e.right = genExpression(sexpression.terms.get(1), declaration, instance, model, factory, _out)
				val expression = BehaviorExpressionGeneration.addMultipleSimpleExpression(2, e, sexpression, declaration, instance, model,
					factory, _out)
				_out.println('Multiple Simple Expression creation ends.')
				return expression
			}
			default:{
				throw new ForbiddenConstructException(name,"The AADL2 verifier does not currently handle this construct.")
			}
		}
	}

	def static Expression addMultipleSimpleExpression(int i, Expression exp, SimpleExpression sexpression,
		ProcessDeclaration declaration, ComponentInstance instance, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		_out.println('Multiple Simple Expression addition begins.')
		if (sexpression.terms.size == i) {
			_out.println('Multiple Simple Expression addition ends.')
			return exp
		} else {
			val name = sexpression.binaryAddingOperators.get(i-1).getName()
			switch name {
				case "Plus": {
					_out.println('Plus')
					val Addition e = factory.createAddition
					e.left = exp
					e.right = genExpression(sexpression.terms.get(i), declaration, instance, model, factory, _out)
					val expression = BehaviorExpressionGeneration.addMultipleSimpleExpression(i + 1, e, sexpression, declaration,
						instance, model, factory, _out)
					_out.println('Multiple Simple Expression addition ends.')
					return expression
				}
				case "Minus": {
					_out.println('Minus')
					val Substraction e = factory.createSubstraction
					e.left = exp
					e.right = genExpression(sexpression.terms.get(i), declaration, instance, model, factory, _out)
					val expression = BehaviorExpressionGeneration.addMultipleSimpleExpression(i + 1, e, sexpression, declaration,
						instance, model, factory, _out)
					_out.println('Multiple Simple Expression addition ends.')
					return expression
				}
				default:{
					throw new ForbiddenConstructException(name,"The AADL2 verifier does not currently handle this construct.")
				}
			}
		}
	}

	def static Expression createMultipleLogicalExpression(Value v, ProcessDeclaration declaration,
		ComponentInstance instance, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		_out.println('Multiple Logical Expression creation begins.')
		//		if ((v as ValueExpression).isSetLogicalOperators) {
		// on a un ensemble de relation
		val name = (v as ValueExpression).logicalOperators.get(0).getName()
		switch name {
			case "And": {
				_out.println('And')
				val Conjunction e = factory.createConjunction
				e.left = genExpression((v as ValueExpression).relations.get(0), declaration, instance, model,
					factory, _out)
				e.right = genExpression((v as ValueExpression).relations.get(1), declaration, instance, model,
					factory, _out)
				val expression = BehaviorExpressionGeneration.addMultipleLogicalExpression(2, e, (v as ValueExpression), declaration,
					instance, model, factory, _out)
				_out.println('Multiple Logical Expression creation ends.')
				return expression
			}
			case "Or": {
				_out.println('Or')
				val Disjunction e = factory.createDisjunction
				e.left = genExpression((v as ValueExpression).relations.get(0), declaration, instance, model,
					factory, _out)
				e.right = genExpression((v as ValueExpression).relations.get(1), declaration, instance, model,
					factory, _out)
				val expression = BehaviorExpressionGeneration.addMultipleLogicalExpression(2, e, (v as ValueExpression), declaration,
					instance, model, factory, _out)
				_out.println('Multiple Logical Expression creation ends.')
				return expression
			}
			default:{
				throw new ForbiddenConstructException(name,"The AADL2 verifier does not currently handle this construct.")
			}
		}
	}

	def static Expression addMultipleLogicalExpression(int i, Expression exp, ValueExpression ve,
		ProcessDeclaration declaration, ComponentInstance instance, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		_out.println('Multiple Logical Expression addition begins.')
		if (ve.relations.size == i) {
			_out.println('Multiple Logical Expression addition ends.')
			return exp
		} else {
			val name = ve.logicalOperators.get(i-1).getName()
			switch name {
				case "And": {
					_out.println('And')
					val Conjunction c = factory.createConjunction
					c.left = exp
					c.right = genExpression(ve.relations.get(i), declaration, instance, model, factory, _out)
					val expression = BehaviorExpressionGeneration.addMultipleLogicalExpression(i + 1, c, ve, declaration, instance, model,
						factory, _out)
					_out.println('Multiple Logical Expression addition ends.')
					return expression
				}
				case "Or": {
					_out.println('Or')
					val Disjunction d = factory.createDisjunction
					d.left = exp
					d.right = genExpression(ve.relations.get(i), declaration, instance, model, factory, _out)
					val expression = BehaviorExpressionGeneration.addMultipleLogicalExpression(i + 1, d, ve, declaration, instance, model,
						factory, _out)
					_out.println('Multiple Logical Expression addition ends.')
					return expression
				}
				default:{
					throw new ForbiddenConstructException(name,"The AADL2 verifier does not currently handle this construct.")
				}
			}
		}
	}

}
