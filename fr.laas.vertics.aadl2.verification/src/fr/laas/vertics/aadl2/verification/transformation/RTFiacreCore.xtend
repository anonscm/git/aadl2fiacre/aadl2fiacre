/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.Classifier
import org.osate.aadl2.DataSubcomponent
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.FeatureInstance
import org.osate.aadl2.instance.InstanceObject
import org.osate.aadl2.Feature
import org.osate.ba.aadlba.BehaviorVariable
import org.eclipse.ui.console.MessageConsoleStream

class RTFiacreCore {
	new (){}
	
	
	def static String idPrefix(){
		"aadl_"
	}
	def static String asIdent (String s){
		s.replaceAll('\\.', '_').replaceAll(':','_')
	}
	def static String uid (Classifier s, MessageConsoleStream _out){
		_out.println ("qualifiedName "+ Utils.getQualifiedName(s))
		_out.println ("asident"+asIdent(Utils.getQualifiedName(s)))
		idPrefix()+asIdent(Utils.getQualifiedName(s))
	}
	def static String id (Classifier s, MessageConsoleStream _out){
		_out.println ("qualifiedName "+ Utils.getQualifiedName(s))
		_out.println ("asident"+asIdent(Utils.getQualifiedName(s)))
		idPrefix()+asIdent(s.name)
	}
	def static String id (ComponentInstance instance){
		idPrefix()+ asIdent(instance.name)
	}
	def static String id (BehaviorVariable bv){
		idPrefix()+ asIdent(bv.name)
	}
	def static String id (DataSubcomponent data_sub_compo){
		idPrefix()+ asIdent(data_sub_compo.name)
	}
	// to delete
	def static String id (FeatureInstance fi){
		idPrefix()+ asIdent(fi.name)
	}
	def static String id (Feature f){
		idPrefix()+ asIdent(f.name)
	}
	def static String unionTypeUid(ComponentInstance c){
		"events_"+RTFiacreCore.uid(c)
	}
	def static String uid (ComponentInstance c){
		idPrefix()+asIdent((c as InstanceObject).componentInstancePath)
	}
	def static String constructorUid(FeatureInstance fi, ComponentInstance ci){
		"e_"+RTFiacreCore.uid(ci)+"_"+RTFiacreCore.id(fi)
	}
	def static String constructorUid(Feature f, ComponentInstance ci){
		"e_"+RTFiacreCore.uid(ci)+"_"+RTFiacreCore.id(f)
	}
	def static String constructorUid (String timeout, ComponentInstance ci){
		"e_"+uid(ci)+"_timeout_"+timeout
	}
	def static String fresh_id(String id){
		"fresh_"+id
	}
	def static String dataAccessUid (ComponentInstance c){
		"data_access_"+uid(c)	
	}
	def static processUid(ComponentInstance instance) {
		"proc_"+uid(instance)
	}
	def static processUid(FeatureInstance fi) {
		(Utils.eContainer(fi,ComponentInstance) as ComponentInstance).processUid
	}
	def static urgencyProcessUid(ComponentInstance instance) {
		"urgency_"+processUid(instance)
	}
	

	
	
}