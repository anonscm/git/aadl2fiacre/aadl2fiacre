/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.DataAccess
import org.osate.aadl2.DataSubcomponent
import org.osate.aadl2.Port
import org.osate.ba.aadlba.ActualPortHolder
import org.osate.ba.aadlba.BehaviorAction
import org.osate.ba.aadlba.BehaviorElement
import org.osate.ba.aadlba.BehaviorVariable
import org.osate.ba.aadlba.BehaviorVariableHolder
import org.osate.ba.aadlba.DataAccessHolder
import org.osate.ba.aadlba.DataComponentReference
import org.osate.ba.aadlba.DataHolder
import org.osate.ba.aadlba.DataSubcomponentHolder
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import fr.irit.fiacre.etoile.xtext.fiacre.IdentifierPattern
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessPattern
import org.eclipse.ui.console.MessageConsoleStream

class BehaviorPatternGeneration {
	new() {
	}

	def static IdentifierPattern genPattern(ProcessDeclaration declaration, Model model, FiacreFactory factory,
		DataComponentReference target, DataHolder data, MessageConsoleStream _out) {
		_out.println('Pattern generation starts.')
		val int index = target.data.indexOf(data)
		if (!(target.data.get(0) == data)) {

			// ce n'est pas le premier (donc vi di etc.)
			val RecordAccessPattern rap = factory.createRecordAccessPattern
			rap.field = BehaviorReferencedElementGeneration.genReferencedElement(model,target,data, _out)
			rap.source = genPattern(declaration, model, factory, target, target.data.get(index - 1), _out)
			_out.println('Pattern generation ends.')
			return rap
		} else {

			// si c'est le dernier (x)
			val IdentifierPattern ip = factory.createIdentifierPattern
			ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration, data.element as DataSubcomponent, _out)
			_out.println('Pattern generation ends.')
			return ip
		}
	}
	
	def static IdentifierPattern genPattern (ProcessDeclaration declaration, Model model, FiacreFactory factory,
		BehaviorElement be, MessageConsoleStream _out){
			_out.println('Pattern generation starts.')
			val IdentifierPattern ip = factory.createIdentifierPattern
			switch be{
				DataSubcomponentHolder:{
					ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration, be.element as DataSubcomponent, _out)
				}
				DataAccessHolder: {
					ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration, be.element as DataAccess, _out)
				}
				BehaviorVariableHolder:{
					ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration, be.element as BehaviorVariable, _out)
				}
				ActualPortHolder:{
					_out.println("the container statement"+ Utils.eContainer(be,BehaviorAction))
					ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration, be.element as Port, _out)
				}
				default:{
					throw new ForbiddenConstructException(be,"The AADL2 verifier does not currently handle this construct.")
				}
			}
			_out.println('Pattern generation ends.')
			return ip
		}
	
	def static IdentifierPattern genPortPattern(ProcessDeclaration declaration, Model model, FiacreFactory factory, ActualPortHolder holder, MessageConsoleStream _out) {
		_out.println('Port Pattern generation starts.')
		val IdentifierPattern ip = factory.createIdentifierPattern
		ip.declaration = BehaviorReferencedElementGeneration.genReferencedPort(declaration, holder.element as Port, _out)	
		_out.println('Port Pattern generation ends.')
		return ip
	}
	
//	def static IdentifierPattern genPattern(ProcessDeclaration declaration, Model model, FiacreFactory factory,
//		DataSubcomponentHolder target) {
//			val IdentifierPattern ip = factory.createIdentifierPattern
//			ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration, target.element as DataSubcomponent)
//			ip
//	}
	
//	def static IdentifierPattern genPattern(ProcessDeclaration declaration, Model model, FiacreFactory factory,
//		DataAccessHolder target) {
//			val IdentifierPattern ip = factory.createIdentifierPattern
//			ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration, target.element as DataAccess)
//			ip
//	}
	
//	def static IdentifierPattern genPattern(ProcessDeclaration declaration, Model model, FiacreFactory factory,
//		BehaviorVariableHolder target) {
//			val IdentifierPattern ip = factory.createIdentifierPattern
//			ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration, target.element as BehaviorVariable)
//			ip
//	}
//	def static IdentifierPattern genPattern(ProcessDeclaration declaration, Model model, FiacreFactory factory,
//		DataPortHolder target) {
//			val IdentifierPattern ip = factory.createIdentifierPattern
//			ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration, target.element as Port)
//			ip
//	}
//	def static IdentifierPattern genPattern(ProcessDeclaration declaration, Model model, FiacreFactory factory,
//		EventDataPortHolder target) {
//			val IdentifierPattern ip = factory.createIdentifierPattern
//			ip.declaration = BehaviorReferencedElementGeneration.genReferencedElement(declaration, target.element as Port)
//			ip
//	}

}
