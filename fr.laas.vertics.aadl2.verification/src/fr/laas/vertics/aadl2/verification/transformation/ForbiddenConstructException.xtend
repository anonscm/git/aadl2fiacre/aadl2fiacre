package fr.laas.vertics.aadl2.verification.transformation

import java.lang.RuntimeException
import org.osate.aadl2.Element

public class ForbiddenConstructException extends RuntimeException {
	public Element element
	String name
	public new(Element _element,String _message) {
		super(_message)
		this.element = _element
	}
	public new(String _name,String _message) {
		super(_message)
		this.name = _name
	}
	
	public override toString() {
		if (this.element != null) {
			return this.element.toString()
		} else {
			if (this.name != null) {
				return this.name
			}
		}
	}
}