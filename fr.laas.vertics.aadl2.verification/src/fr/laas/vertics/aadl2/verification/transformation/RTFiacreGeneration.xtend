/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.instance.SystemInstance
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import fr.irit.fiacre.etoile.xtext.fiacre.ImportDeclaration
import org.eclipse.ui.console.MessageConsoleStream

class RTFiacreGeneration {
	new(){}
		//Generate all types derived from AADL system instance
	def static genTypes(SystemInstance instance, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		// namely record (from data components) 
		RecordGeneration.generateRecordType(instance, model, factory, _out) 
		// union (from event/event data input port).
		UnionGeneration.generateUnionTypes(instance, model, factory, _out)
	}
	def static genHeader(Model model, FiacreFactory factory, MessageConsoleStream _out) {
		val ImportDeclaration id = factory.createImportDeclaration
		id.importURI = "libV4.fiacre"
		model.imports.add(id)
	}
	
	def static generateAllProcesses(SystemInstance instance, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		ProcessGeneration.genProcesses(instance, model, factory, _out)
	}
	
	def static genComponent(SystemInstance instance, Model model,Model library, FiacreFactory factory, MessageConsoleStream _out) {
		ComponentGeneration.genMainComponent(instance, model,library, factory, _out)
	}
	
}