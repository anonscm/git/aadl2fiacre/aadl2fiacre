/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import org.osate.aadl2.instance.SystemInstance
import fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.Composition
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.PortSet
import org.eclipse.ui.console.MessageConsoleStream

class ParBlockGeneration {
	new(){
	}
	
	def static genComponentParBlock(SystemInstance instance, Model model, ComponentDeclaration declaration, Model library, FiacreFactory factory, MessageConsoleStream _out) {
		//create a test block. just to generate the component
		// create the composition and assign it to the component
		val Composition composition = factory.createComposition
		declaration.body = composition
		//
		val PortSet ps = factory.createPortSet
		composition.global = ps
		//
		ps.allPorts = true
		// 
		
		////////////////////////////////
		composition.blocks.add(ProcessInstantiationGeneration.genSchedulerInstantiation(instance, model, declaration, library, factory, _out)) 
		
		//TODO pourquoi OrderedSet in the acceleo ? why Ordered? 
		for (ci: SystemInstanceUtil.getThreads(instance, _out).toSet){
			composition.blocks.add(ProcessInstantiationGeneration.genProcessControllerInstantiation(ci, instance, model, declaration, library, factory, _out))
			composition.blocks.add(ProcessInstantiationGeneration.genProcessInstantiation(ci, instance, model, declaration, library, factory, _out))
		}
	 	for (ci: SystemInstanceUtil.getDevices(instance, _out).toSet){
			composition.blocks.add(ProcessInstantiationGeneration.genProcessControllerInstantiation(ci, instance, model, declaration, library, factory, _out))
			composition.blocks.add(ProcessInstantiationGeneration.genProcessInstantiation(ci, instance, model, declaration, library, factory, _out))
		}
		for (ci: SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out)){
			if(ComponentInstanceUtil.isSporadic(ci)){
			composition.blocks.add(ProcessInstantiationGeneration.genProcessUrgencyInstantiation(ci, instance, model, declaration, library, factory, _out))
			}
		}
		//composition.blocks.addAll(
			composition.blocks.addAll(InputPortProcessInstantiation.genInputEventPortProcessControllers(instance, model, declaration, library, factory, _out))
			
			composition.blocks.addAll(ConnectionProcessInstantiation.genDataConnections(instance, model, declaration, library, factory, _out))
		//)
	//composition.blocks.filter(Block)
	} 

	
}