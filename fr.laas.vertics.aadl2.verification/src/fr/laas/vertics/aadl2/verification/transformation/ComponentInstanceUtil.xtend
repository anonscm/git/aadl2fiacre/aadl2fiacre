/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import java.util.ArrayList
import java.util.Set
import org.eclipse.emf.common.util.EList
import org.osate.aadl2.ComponentCategory
import org.osate.aadl2.DataAccess
import org.osate.aadl2.DefaultAnnexSubclause
import org.osate.aadl2.DeviceImplementation
import org.osate.aadl2.DeviceSubcomponent
import org.osate.aadl2.Feature
import org.osate.aadl2.IntegerLiteral
import org.osate.aadl2.ListValue
import org.osate.aadl2.NamedElement
import org.osate.aadl2.NamedValue
import org.osate.aadl2.Port
import org.osate.aadl2.PropertyAssociation
import org.osate.aadl2.StringLiteral
import org.osate.aadl2.ThreadImplementation
import org.osate.aadl2.ThreadSubcomponent
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.ConnectionInstance
import org.osate.aadl2.instance.ConnectionKind
import org.osate.aadl2.instance.FeatureInstance
import org.osate.ba.aadlba.BehaviorAnnex
import org.eclipse.ui.console.MessageConsoleStream

class ComponentInstanceUtil {
	new(){}
	
	def static ArrayList<ComponentInstance> getComponentProcesses(ComponentInstance comp_ins){
		if (comp_ins.category == ComponentCategory.PROCESS)
		{	val ArrayList<ComponentInstance> it_comp = new ArrayList<ComponentInstance>()
			 it_comp.add(comp_ins)
			 return it_comp
		}
		else
			if (comp_ins.category ==ComponentCategory.SYSTEM)
			{	
				val ArrayList<ComponentInstance> it_comp2 = new ArrayList<ComponentInstance>() 
			
				for (ComponentInstance ci : comp_ins.componentInstances )
					    it_comp2.addAll(ComponentInstanceUtil.getComponentProcesses(ci))      	
				return it_comp2
			}	
				else
			{	 
				val ArrayList<ComponentInstance> it_comp3 = new ArrayList<ComponentInstance>()
				return 	it_comp3
			}
	}
	
	def static boolean isSporadic( ComponentInstance ci){
		hasDispatchProtocol(ci,vSporadic()) || hasDispatchProtocol(ci,vAperiodic())
	}
	def static boolean isPeriodic( ComponentInstance ci){
		hasDispatchProtocol(ci,vPeriodic()) 
	}
	def static long getPeriod(ComponentInstance instance){
		if (hasDispatchProtocol(instance,vAperiodic())){
			0
		}
		else{
			val IntegerLiteral intlit = AADLCore.getPropertyValueForKey(instance.ownedPropertyAssociations,kPeriod()).ownedValue as IntegerLiteral
			val long factor = (intlit.unit.factor as IntegerLiteral).value
			return (intlit.value * factor)
			
		}
	}
	
	def static boolean hasDispatchProtocol(ComponentInstance instance, String string) {
		if( ! (instance.category == ComponentCategory.THREAD || instance.category == ComponentCategory.DEVICE) || instance.ownedPropertyAssociations.empty)
			false
		else
			((getDispatchProtocol(instance.ownedPropertyAssociations).ownedValues.get(0).ownedValue as NamedValue).namedValue as NamedElement).getQualifiedName == string
	}
	
	def static PropertyAssociation getDispatchProtocol(EList<PropertyAssociation> list) {
		if (list.size == 0) {
			return null
		}
		else{
		return list.filter[property.getQualifiedName =="Thread_Properties::Dispatch_Protocol"].get(0)
		}	
	}  
	
	def static PropertyAssociation getInitialValueProperty(EList<PropertyAssociation> list) {
		if (list.size == 0) {
			return null
		}
		else{
			list.filter[property.getQualifiedName =="Data_Model::Initial_Value"].get(0)
		}
	}
	def static String getInitialValue(PropertyAssociation pa) {
		((pa.ownedValues.get(0).ownedValue as ListValue).ownedListElements.get(0) as StringLiteral).value
	}
	def static String vSporadic(){
		"AADL_Project::Supported_Dispatch_Protocols.Sporadic"
	}
	def static String vAperiodic(){
		"AADL_Project::Supported_Dispatch_Protocols.Aperiodic"
	}
	def static String vPeriodic(){
		"AADL_Project::Supported_Dispatch_Protocols.Periodic"
	}
	
	def static Iterable<Port> getPorts(ComponentInstance instance) {
		instance.featureInstances.map[fi|fi.feature].filter(Port)
	}
	
	// TODO 
	// cette méthode devrait e^tre supprimer et son appel devrait être remplacer par getPorts
	def static Iterable<FeatureInstance> getPortsFeatureInstances(ComponentInstance instance) {
		instance.featureInstances.filter[f|f.category.value<= 2]
	}
	def static Iterable<FeatureInstance>  getPortFeatureInstances (ComponentInstance instance){
		instance.featureInstances.filter[f|f.feature instanceof Port] 
	}
	def static Iterable<FeatureInstance>  getDataAccessFeatureInstances(ComponentInstance instance){
		instance.featureInstances.filter[f|f.feature instanceof DataAccess] 
	}
	def static BehaviorAnnex getBehaviorAnnex(ComponentInstance ci){
		if (ci.category == ComponentCategory.THREAD){
			val asc = ((ci.subcomponent as ThreadSubcomponent).threadSubcomponentType as ThreadImplementation).ownedAnnexSubclauses.filter[oas|oas.name=="behavior_specification"]
			if (asc.length > 0) {
				 val r = (asc.get(0) as DefaultAnnexSubclause)
				 r.parsedAnnexSubclause as BehaviorAnnex
			} else {
				 	null
			}
		} else{
			if (ci.category == ComponentCategory.DEVICE){
				val asc = ((ci.subcomponent as DeviceSubcomponent).deviceSubcomponentType as DeviceImplementation).ownedAnnexSubclauses.filter[oas|oas.name=="behavior_specification"]
				if (asc.length > 0) {
					asc.get(0) as BehaviorAnnex
				} else  {
					null
				}
			} else {
				null
			}
			
		}
	}
	def final static String kPeriod() {
		"Period"
	}
	def final static String kPriority() {
		"Priority"
	}
	
	def static boolean hasDeadline(ComponentInstance instance){
		if (AADLCore.hasPropertyValueForKey(instance.ownedPropertyAssociations,kDeadline()) && hasPeriod(instance)){
			! (getPeriod(instance) == getDeadline(instance)) 
		}
		else{
			false
			}
	}
	
	def static boolean hasPeriod(ComponentInstance instance){
		AADLCore.hasPropertyValueForKey(instance.ownedPropertyAssociations,kPeriod())
	}
	def static long getDeadline(ComponentInstance instance) {
		val IntegerLiteral deadline = AADLCore.getPropertyValueForKey(instance.ownedPropertyAssociations,kDeadline()).ownedValue as IntegerLiteral
			val long factor = (deadline.unit.factor as IntegerLiteral).value
			return (deadline.value * factor)
			
		}	
	 
	def final static String kDeadline() {
		"Deadline"
	}
	def static long getPriorityWithDefaultValue(ComponentInstance ci){
		if (ci.category == ComponentCategory.THREAD && AADLCore.hasPropertyValueForKey(ci.ownedPropertyAssociations,kPriority())){
			val IntegerLiteral priority = AADLCore.getPropertyValueForKey(ci.ownedPropertyAssociations,kPriority()).ownedValue as IntegerLiteral
			return priority.value		
		}
		else{
			0
		}
	}
	def static ComponentInstance getDataAccessProvider(ComponentInstance ci, DataAccess da){
//		_out.println("before filter" + ci.systemInstance.getAllConnectionInstances)
//		_out.println("first filter" + ci.systemInstance.getAllConnectionInstances.filter[cii|cii.kind==org.osate.aadl2.instance.ConnectionKind.ACCESS_CONNECTION])
//		_out.println("second filter" + ci.systemInstance.getAllConnectionInstances.filter[cii|cii.kind==org.osate.aadl2.instance.ConnectionKind.ACCESS_CONNECTION].filter[cii|(cii.destination as FeatureInstance).feature==da])
//		return ci
		ci.systemInstance.getAllConnectionInstances.filter[cii|cii.kind==ConnectionKind.ACCESS_CONNECTION].filter[cii|(cii.destination as FeatureInstance).feature==da].get(0).source as ComponentInstance
	}
	
	// TODO order the output 
	def static Set<FeatureInstance> getIncomingPortFeatureInstances(ComponentInstance instance, MessageConsoleStream _out) {
		getPortConnectionInstances(instance,_out).map[cnxi|cnxi.destination as FeatureInstance].toSet
	}
	
	def static Iterable<ConnectionInstance> getPortConnectionInstances(ComponentInstance instance, MessageConsoleStream _out){
		val Iterable<ConnectionInstance> ci = instance.systemInstance.getAllConnectionInstances.filter[cnxi|cnxi.kind == ConnectionKind.PORT_CONNECTION]
		.filter[cnxi|PortUtil.isEventOrEventDataPort((cnxi.destination as FeatureInstance).feature as Port) && PortUtil.isInputPort((cnxi.destination as FeatureInstance).feature as Port)]
		.filter[cnxi|Utils.eContainer(cnxi.destination,ComponentInstance)==instance].toSet
		_out.println("getPortConnectionInstances before"+instance.systemInstance.getAllConnectionInstances.filter[cnxi|cnxi.kind == ConnectionKind.PORT_CONNECTION].filter[cnxi|PortUtil.isEventOrEventDataPort((cnxi.destination as FeatureInstance).feature as Port) && PortUtil.isInputPort((cnxi.destination as FeatureInstance).feature as Port)])
		_out.println("getPortConnectionInstances "+ci)
		ci.filter[cnxi|Utils.eContainer(cnxi.destination,ComponentInstance)==instance].toSet
//		val ArrayList<ConnectionInstance> cnxarray = new ArrayList<ConnectionInstance>()
//		for (acnxi : ci.filter[cnxi|Utils.eContainer(cnxi.destination,ComponentInstance)==instance].toSet){
//			cnxarray.add(acnxi)
//		}
//		cnxarray
	}
	def static Iterable<FeatureInstance> getPortSourcesForDestination (ComponentInstance ci, Feature f, MessageConsoleStream _out){
		ComponentInstanceUtil.getPortConnectionInstances(ci,_out).filter[cnxi|((cnxi.destination) as FeatureInstance).feature == f].map[cnxi|cnxi.source as FeatureInstance]
	}
	
}