/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.transformation

import java.util.ArrayList
import java.util.List
import org.osate.aadl2.DataAccess
import org.osate.aadl2.DataClassifier
import org.osate.aadl2.DataSubcomponent
import org.osate.aadl2.EventDataPort
import org.osate.aadl2.Feature
import org.osate.aadl2.Port
import org.osate.aadl2.impl.DataPortImpl
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.FeatureInstance
import org.osate.aadl2.instance.SystemInstance
import fr.irit.fiacre.etoile.xtext.fiacre.AnyPattern
import fr.irit.fiacre.etoile.xtext.fiacre.BooleanType
import fr.irit.fiacre.etoile.xtext.fiacre.ChannelType
import fr.irit.fiacre.etoile.xtext.fiacre.ConstructorPattern
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
import fr.irit.fiacre.etoile.xtext.fiacre.IdentifierPattern
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.PortsDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ReceiveStatement
import fr.irit.fiacre.etoile.xtext.fiacre.ReferencedType
import fr.irit.fiacre.etoile.xtext.fiacre.SelectStatement
import fr.irit.fiacre.etoile.xtext.fiacre.StateDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.Statement
import fr.irit.fiacre.etoile.xtext.fiacre.StatementChoice
import fr.irit.fiacre.etoile.xtext.fiacre.StatementSequence
import fr.irit.fiacre.etoile.xtext.fiacre.ToStatement
import fr.irit.fiacre.etoile.xtext.fiacre.Transition
import fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.UnionType
import fr.irit.fiacre.etoile.xtext.fiacre.UnlessStatement
import fr.irit.fiacre.etoile.xtext.fiacre.VariableDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration
import org.eclipse.ui.console.MessageConsoleStream

class ProcessGeneration {
	new() {
	}

	def static genProcesses(SystemInstance instance, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		for (ComponentInstance component : SystemInstanceUtil.getComponentsGeneratingProcesses(instance, _out)) {
			genProcess( component, model, factory, _out)
			_out.print("#")
		}
	}

	def static genProcess(ComponentInstance component, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		genUserProcess( component, model, factory, _out)
		genUrgencyProcess ( component, model, factory, _out) 
	}

	def static genUserProcess(ComponentInstance component, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		_out.println('User Process generation begins.')
		val ProcessDeclaration process_decl = factory.createProcessDeclaration
		process_decl.name = RTFiacreCore.processUid( component)
		model.declarations.add( process_decl)
		_out.println('Created and added Process Declaration')

		// test block: just to verify that processes are generated correctly=> To Delete
		/* 
		val StateDeclaration sd_test = factory.createStateDeclaration
		sd_test.name = "s_test"
		process_decl.states.add(sd_test)

		val Transition t_test = factory.createTransition
		process_decl.transitions.add(t_test)
		t_test.origin = sd_test

		val ToStatement st_test = factory.createToStatement
		st_test.state = sd_test
		t_test.action = st_test
		*/
		// end test block: heureusement que j'ai fait ce petit bout
		// pour détecter les anomalies sur le Scope
		// la solution était d'aller ajouter les Scope 
		// de stateDeclaration dans un Process
		// même si la version textuelle d'un model ne signale pas d'erreur,
		// ça ne veut pas dire que le passage xmi => text est garanti
		genPortParams(process_decl, component, model, factory, _out)
		genArgumentParams(process_decl, component, model, factory, _out)
		genProcessStateDecls(process_decl, component, model, factory, _out)
		genProcessLocalVars(process_decl, component, model, factory, _out)
		genProcessInitStmt(process_decl, component, model, factory, _out)
		genProcessAutomaton(process_decl, component, model, factory, _out)
		_out.println('User Process generation ends.')
	}
	
	def static genProcessAutomaton(ProcessDeclaration declaration, ComponentInstance instance, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		AutomatonGeneration.genTransitions(declaration, instance, model, factory, _out)
	}
	
	def static genProcessInitStmt(ProcessDeclaration declaration, ComponentInstance instance, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		AutomatonGeneration.genStateInitDeclaration(declaration, instance, model, factory, _out)
	}
	
	def static genProcessLocalVars(ProcessDeclaration declaration, ComponentInstance instance, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		/*
		 * ça correspond à 204-207 dans ProcessGeneration
		 * statesVariables n'existe plus puisque la v2 de la BA ne la supporte plus 
		 * donc toutes les déclarations sont dans LocalDataSubcomponents
		 */
		_out.println( 'Process local variables generation begins.')
		for (DataSubcomponent dsc : getLocalDataSubcomponents(instance)){
			val VariablesDeclaration vds = factory.createVariablesDeclaration
			declaration.variables.add(vds)
			val VariableDeclaration vd = factory.createVariableDeclaration
			vds.variables.add(vd)
			vd.name = RTFiacreCore.id(dsc)
			vds.type = DataGeneration.genType(dsc.classifier as DataClassifier, factory, model, _out)
			
			//println ("dsc id -----"+RTFiacreCore.id(dsc) )
			//TODO initialisation of variables
		}
		_out.println('Process behavior annex generation begins.')
		if(ComponentInstanceUtil.getBehaviorAnnex(instance) != null){	
			for( bv :  ComponentInstanceUtil.getBehaviorAnnex(instance).variables){
				_out.print('.')
				val VariablesDeclaration vds = factory.createVariablesDeclaration
				declaration.variables.add(vds)
				val VariableDeclaration vd = factory.createVariableDeclaration
				vds.variables.add(vd)
				vd.name = RTFiacreCore.id(bv)
				vds.type = DataGeneration.genType(bv.dataClassifier as DataClassifier, factory, model, _out)
			}
		}
		_out.print('Process behavior annex generation ends.')
		_out.println('Process local variables generation ends.')
	}
	
	def static Iterable<DataSubcomponent> getLocalDataSubcomponents(ComponentInstance instance) {
		/*
		 * ça correspond à 243-247 dans ProcessGeneration.mtl
		 * le let et le reject je les fait pas parce que j'ai des doutes
		 * sur le c.getSystemInstance qui ne correspond pas au exemple dans le pdf
		 * les connexions doivent être cherchés dans le threadimplentation 
		 * et pas dans le systemeinstance: à vérifier avec JPB
		 */
		val Iterable<DataSubcomponent> subcompo_list = instance.componentInstances.map[ci|ci.subcomponent].filter(DataSubcomponent)
		//println("system instance---"+AADLCore.getSystemInstance(instance))
		//println("subcompo_list---"+subcompo_list)
		return subcompo_list
	}
	
	def static genProcessStateDecls(ProcessDeclaration declaration, ComponentInstance instance, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		AutomatonGeneration.genStateDeclarations(declaration, instance, model, factory, _out)
	}

	def static void genPortParams(ProcessDeclaration process_decl, ComponentInstance ci, Model model,
		FiacreFactory factory, MessageConsoleStream _out) {
		_out.println('Ports parameters generation begins.')
		if (ComponentInstanceUtil.isSporadic(ci)) {
			_out.println('Sporadic case')
			// d: in out ..........
			val PortsDeclaration psd = factory.createPortsDeclaration
			val PortDeclaration prd = factory.createPortDeclaration
			prd.name = "_dispatch"
			psd.ports.add(prd)
			val ChannelType cht = factory.createChannelType
			psd.type = cht
			cht.in = true
			cht.out = true
			val ReferencedType reft = factory.createReferencedType
			cht.type = reft
			reft.type = model.declarations.filter(TypeDeclaration).filter[name == RTFiacreCore.unionTypeUid(ci)].get(0)

			//
			process_decl.ports.add(psd)

			// e,c, dl :none
			val PortsDeclaration psd2 = factory.createPortsDeclaration
			val ChannelType cht2 = factory.createChannelType
			psd2.type = cht2
			val PortDeclaration prde = factory.createPortDeclaration
			prde.name = "_execute"
			val PortDeclaration prdc = factory.createPortDeclaration
			prdc.name = "_complete"
			val PortDeclaration prdl = factory.createPortDeclaration
			prdl.name = "_deadline"
			psd2.ports.add(prde)
			psd2.ports.add(prdc)
			psd2.ports.add(prdl)
			process_decl.ports.add(psd2)
			genOutgoingPortParams(process_decl, ci, factory, model, _out)
		} else {
			if (ComponentInstanceUtil.isPeriodic(ci)) {
				_out.println('Periodic case')
				val PortsDeclaration psd2 = factory.createPortsDeclaration
				val ChannelType cht2 = factory.createChannelType
				psd2.type = cht2
				val PortDeclaration prdd = factory.createPortDeclaration
				prdd.name = "_dispatch"
				val PortDeclaration prde = factory.createPortDeclaration
				prde.name = "_execute"
				val PortDeclaration prdc = factory.createPortDeclaration
				prdc.name = "_complete"
				val PortDeclaration prdl = factory.createPortDeclaration
				prdl.name = "_deadline"
				psd2.ports.add(prdd)
				psd2.ports.add(prde)
				psd2.ports.add(prdc)
				psd2.ports.add(prdl)
				process_decl.ports.add(psd2)
				genOutgoingPortParams(process_decl, ci, factory, model, _out)
			}
		}
		_out.println('Ports parameters generation ends.')
	}

	def static genArgumentParams(ProcessDeclaration proc_declaration, ComponentInstance ci, Model model,
		FiacreFactory factory, MessageConsoleStream _out) {
		_out.println('Arguments parameters generation begins.')
		if (ComponentInstanceUtil.isSporadic(ci)) {
			_out.println('Sporadic case')
			val ParametersDeclaration params_decl = factory.createParametersDeclaration
			proc_declaration.parameters.add(params_decl)
			val ReferencedType ref_union_type = factory.createReferencedType
			ref_union_type.type = model.declarations.filter(TypeDeclaration).filter[
				name == RTFiacreCore.unionTypeUid(ci)].get(0)
			params_decl.type = ref_union_type
			val ParameterDeclaration param_decl = factory.createParameterDeclaration
			param_decl.reference = true
			param_decl.name = "_dispatch"
			params_decl.parameters.add(param_decl)
			genProcessArguments(proc_declaration, ci, model, factory, _out)
		} else {
			if (ComponentInstanceUtil.isPeriodic(ci)) {
				_out.println('Periodic case')
				if (getProcessArguments(ci).size > 0) {
					genProcessArguments(proc_declaration, ci, model, factory, _out)
				}
			}
		}
		_out.println('Arguments parameters generation ends.')
	}

	def static void genOutgoingPortParams(ProcessDeclaration process_decl, ComponentInstance cinstance,
		FiacreFactory factory, Model model, MessageConsoleStream _out) {
		_out.println('Outgoing port parameters generation begins.')
		for (FeatureInstance p : getPortParams(cinstance)) {

			//to do
			val PortsDeclaration psd = factory.createPortsDeclaration
			val PortDeclaration prd = factory.createPortDeclaration
			prd.name = RTFiacreCore.id(p)
			psd.ports.add(prd)
			psd.type = DataGeneration.genPortDeclarationType(p.feature, factory, model, _out)
			process_decl.ports.add(psd)

			//_out.println()
			//_out.println("channel----")
			//_out.println("test")
		}
		_out.println('Outgoing port parameters generation ends.')
	}

	def static Iterable<FeatureInstance> getPortParams(ComponentInstance ci) {
		PortUtil.outgoing(PortUtil.withEventKind(ComponentInstanceUtil.getPortsFeatureInstances(ci))).sortBy[category == 2]
	}

	def static List<FeatureInstance> getProcessArguments(ComponentInstance ci) {
		val ArrayList<FeatureInstance> list = new ArrayList<FeatureInstance>()
		val Iterable<FeatureInstance> port_list = ComponentInstanceUtil.getPortFeatureInstances(ci)
		val Iterable<FeatureInstance> dataAccess_list = ComponentInstanceUtil.getDataAccessFeatureInstances(ci)
		for (FeatureInstance port : port_list) {
			//_out.println("port qualified" + port.getQualifiedName())
			list.add(port)
		}
		for (FeatureInstance dataAccess : dataAccess_list) {
			//_out.println("dataaccess qualified" + dataAccess.getQualifiedName())
			list.add(dataAccess)
		}
		return list.sortBy[name].sortBy[f|! (f.feature instanceof Port)]

	}

	def static genProcessArguments(ProcessDeclaration proc_declaration, ComponentInstance instance, Model model,
		FiacreFactory factory, MessageConsoleStream _out) {
		_out.println('Process arguments generation begins.')
		for (FeatureInstance f : getProcessArguments(instance)) {
			genProcessArgument(proc_declaration, f.feature, model, factory, _out)
		}
		_out.println('Process arguments generation ends.')
	}

	def static genProcessArgument(ProcessDeclaration proc_declaration, Feature f, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		_out.println('Process argument generation begins.')
		if (f.class == DataPortImpl && PortUtil.isInputPort(f as Port)) {
			val ParametersDeclaration params_decl = factory.createParametersDeclaration
			proc_declaration.parameters.add(params_decl)
			val ParameterDeclaration param_decl = factory.createParameterDeclaration
			param_decl.reference = true
			param_decl.name = RTFiacreCore.id(f)
			params_decl.parameters.add(param_decl)
			params_decl.type = DataGeneration.genSharedVariableType(f, model, factory, _out)
			//_out.println(RTFiacreCore.fresh_id(RTFiacreCore.id(f)))
			val ParametersDeclaration params_decl_fresh = factory.createParametersDeclaration
			proc_declaration.parameters.add(params_decl_fresh)
			val ParameterDeclaration param_decl_fresh = factory.createParameterDeclaration
			param_decl_fresh.reference = true
			param_decl_fresh.name = RTFiacreCore.fresh_id(RTFiacreCore.id(f))
			params_decl_fresh.parameters.add(param_decl_fresh)
			val BooleanType bool_type = factory.createBooleanType
			params_decl_fresh.type = bool_type
		} else {
			if (f instanceof Port) {
				//_out.println("--port " + RTFiacreCore.id(f))
				val ParametersDeclaration params_decl = factory.createParametersDeclaration
				proc_declaration.parameters.add(params_decl)
				val ParameterDeclaration param_decl = factory.createParameterDeclaration
				param_decl.reference = true
				param_decl.name = RTFiacreCore.id(f)
				params_decl.parameters.add(param_decl)
				params_decl.type = DataGeneration.genSharedVariableType(f, model, factory, _out)
			} else {
				if (f instanceof DataAccess) {
					//_out.println("--dataaaccess" + RTFiacreCore.id(f))
					val ParametersDeclaration params_decl = factory.createParametersDeclaration
					proc_declaration.parameters.add(params_decl)
					val ParameterDeclaration param_decl = factory.createParameterDeclaration
					param_decl.reference = true
					param_decl.name = RTFiacreCore.id(f)
					params_decl.parameters.add(param_decl)
					params_decl.type = DataGeneration.genSharedVariableType(f, model, factory, _out)
				}
			}
		}
		_out.println('Process argument generation ends.')
	}

	def static genUrgencyProcess(ComponentInstance ci, Model model, FiacreFactory factory, MessageConsoleStream _out) {
		_out.println("Urgency process generation begins.")
		if (ComponentInstanceUtil.isSporadic(ci)){
			_out.println('Sporadic case:')
			// process urgency_proc_
			val ProcessDeclaration process_decl = factory.createProcessDeclaration
			process_decl.name = RTFiacreCore.urgencyProcessUid(ci)
			model.declarations.add(process_decl)
			
			
			// [d : in out events_aadl_t_receiver_i]
			val PortsDeclaration psd = factory.createPortsDeclaration
			val PortDeclaration prd = factory.createPortDeclaration
			prd.name = "_dispatch"
			psd.ports.add(prd)
			val ChannelType cht = factory.createChannelType
			psd.type = cht
			cht.in = true
			cht.out = true
			val ReferencedType reft = factory.createReferencedType
			cht.type = reft
			reft.type = model.declarations.filter(TypeDeclaration).filter[name == RTFiacreCore.unionTypeUid(ci)].get(0)

			process_decl.ports.add(psd)
			
			// states s0
			val StateDeclaration sd_s0 = factory.createStateDeclaration
			sd_s0.name = "s0"
			process_decl.states.add(sd_s0)
			
			// test transition
			val Transition t_tr = factory.createTransition
			process_decl.transitions.add(t_tr)
			t_tr.origin = sd_s0
		
			
			
			val ToStatement st_to = factory.createToStatement
			st_to.state = sd_s0
//			t_test.action = st_test
			//
		
		val ArrayList<ArrayList<ReceiveStatement>> statements = new ArrayList<ArrayList<ReceiveStatement>>()
		for ( ArrayList<Port> arrayport: getUrgencyPortPartition(ci))
		{
			_out.print('.')
			val ArrayList<ReceiveStatement> arraystatements = new ArrayList<ReceiveStatement>()
			statements.add(arraystatements)
			for (Port aport : arrayport){
				_out.print('-')
				val ReceiveStatement rstatement = factory.createReceiveStatement
				arraystatements.add(rstatement)
				val IdentifierPattern ip = factory.createIdentifierPattern
				ip.declaration = prd
				rstatement.port = ip
				
				
				//if any
				if(PortUtil.isAllItems(aport) || aport instanceof EventDataPort){
					// then constructorpattern with anypattern
					val ConstructorPattern cp = factory.createConstructorPattern
					rstatement.patterns.add(cp)
					cp.declaration = model.declarations.filter(TypeDeclaration).map[t|t.value].filter(UnionType).map[ut|ut.tags].flatten.map[t|t.tags].flatten.filter[tag|tag.name==RTFiacreCore.constructorUid(aport,ci)].get(0)
				 	val AnyPattern anypar = factory.createAnyPattern
				 	cp.parameter = anypar
				 	//_out.println("receivestatement"+ rstatement+ (cp.declaration as UnionTagDeclaration).name)		
				 }
				//else just identifierpattern
				else
				{
					val IdentifierPattern ipatt = factory.createIdentifierPattern
					rstatement.patterns.add(ipatt)
					ipatt.declaration = model.declarations.filter(TypeDeclaration).map[t|t.value].filter(UnionType).map[ut|ut.tags].flatten.map[t|t.tags].flatten.filter[tag|tag.name==RTFiacreCore.constructorUid(aport,ci)].get(0)		
				}	
			}	
		}
		// add timeouts
		val ArrayList<ReceiveStatement> timeoutarraystatements = new ArrayList<ReceiveStatement>()
		for (String timeout : UnionGeneration.getTimeoutConstructors(ci))
		{
			_out.print('+')
			val ReceiveStatement timeoutstatement = factory.createReceiveStatement
			timeoutarraystatements.add(timeoutstatement)
			val IdentifierPattern timeip = factory.createIdentifierPattern
			timeoutstatement.port = timeip
			timeip.declaration = prd
			val IdentifierPattern timepattern = factory.createIdentifierPattern
			timeoutstatement.patterns.add(timepattern)
			timepattern.declaration = model.declarations.filter(TypeDeclaration).map[t|t.value].filter(UnionType).map[ut|ut.tags].flatten.map[t|t.tags].flatten.filter[tag|tag.name==timeout].get(0)
		}
		// add the timeout array in the main array
		if (timeoutarraystatements.size > 0){
			statements.add(timeoutarraystatements)	
		}
		
		
		// each sub array will be transformed into a choicestatement []
		//_out.println("arrayarraystatements"+ statements)	
		val ArrayList<Statement> arraystatements = new ArrayList<Statement>()
		for (ArrayList<ReceiveStatement> arrayreceivestatements : statements){
			_out.print('*')
			arraystatements.add(createFiacreStatementChoice(arrayreceivestatements, factory))
		}
			
		//_out.println("receivestatement"+ arraystatements + "  "+arraystatements.size)	
		
		
		// the array that contains now choicestatement will be transformed into an unlessstatement	
		val SelectStatement ssunless = factory.createSelectStatement
		ssunless.body = createFiacreUnlessStatement(arraystatements, factory)
		val StatementSequence sstmt = factory.createStatementSequence
		t_tr.action = sstmt 
		sstmt.statements.add(ssunless) 
		sstmt.statements.add(st_to) 
		}
		_out.println("Urgency process generation ends.")
	}
	
	def static ArrayList<ArrayList<Port>> getUrgencyPortPartition (ComponentInstance c){
		val ArrayList<ArrayList<Port>> p = new ArrayList<ArrayList<Port>>()
		val ArrayList<Port> ports = new ArrayList<Port>()
		for(FeatureInstance portinstance : PortUtil.incoming(PortUtil.withEventKind(ComponentInstanceUtil.getPortsFeatureInstances(c)))){
			ports.add(portinstance.feature as Port)
		}
		//_out.println("port set"+ ports)
		
		val ArrayList<Integer> urgencySortedSeq = new  ArrayList<Integer> ()
		urgencySortedSeq.addAll(ports.map[port|PortUtil.getUrgencyWithDefaultValue(port.ownedPropertyAssociations)].toSet.sort)

		//_out.println("urgency urgencySortedSeq"+ urgencySortedSeq)
		
		
		for (Integer index : urgencySortedSeq){
			val  Iterable<Port> iports = ports.filter[port|PortUtil.getUrgencyWithDefaultValue(port.ownedPropertyAssociations)==index]
			val ArrayList<Port> aports = new ArrayList<Port>()
			for(Port aport : iports){
			aports.add(aport)
			}
			p.add(aports)
		}
		//_out.println ("arrayarrayport" + p)
		return p
	}
	
	
	
	
	// ces 2 méthodes c'est pour créer un StatementChoice imbriqué
	def static Statement createFiacreStatementChoice(ArrayList<ReceiveStatement> statements, FiacreFactory factory){
		//_out.println("createFiacreStatementChoice")
		if(statements.size == 1){
		 	statements.get(0)
		 }
		 else{
		 		val StatementChoice ss = 	factory.createStatementChoice
		 		ss.choices.add(statements.get(0))
		 		ss.choices.add(statements.get(1))
		 		addMultipleStatementChoice(2, ss, statements, factory)
		 }
	}
	def static StatementChoice addMultipleStatementChoice(int i, StatementChoice sequence, ArrayList<ReceiveStatement> statements, FiacreFactory factory) {
		if(statements.size == i){
			return sequence
		}
		else{
			val StatementChoice ssi = factory.createStatementChoice
		 	ssi.choices.add(sequence)
		 	ssi.choices.add(statements.get(i))
		 	addMultipleStatementChoice(i+1, ssi, statements,factory)
		}
		
	} 
	// ces 2 méthodes c'est pour créer un UnlessStatement imbriqué
	def static Statement createFiacreUnlessStatement(ArrayList<Statement> statements, FiacreFactory factory){
		//_out.println("createFiacreUnlessStatement")
		if(statements.size == 1){
		 	statements.get(0)
		 }
		 else{
		 		val UnlessStatement ss = 	factory.createUnlessStatement
		 		ss.followers.add(statements.get(0))
		 		ss.followers.add(statements.get(1))
		 		addMultipleUnlessStatement(2, ss, statements, factory)
		 }
	}
	def static UnlessStatement addMultipleUnlessStatement(int i, UnlessStatement sequence, ArrayList<Statement> statements, FiacreFactory factory) {
		if(statements.size == i){
			return sequence
		}
		else{
			val UnlessStatement ssi = factory.createUnlessStatement
		 	ssi.followers.add(sequence)
		 	ssi.followers.add(statements.get(i))
		 	addMultipleUnlessStatement(i+1, ssi, statements , factory) 
		}
		
	} 

}
