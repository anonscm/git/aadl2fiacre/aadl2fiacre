/* Copyright (C) 2018 LAAS/CNRS and UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original author: Faiez Zalila (LAAS/CNRS)
* Designed by: Jean-Paul Bodeveix, Mamoun Filali (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.laas.vertics.aadl2.verification.popup.actions;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.Element;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.modelsupport.util.AadlUtil;
import org.osate.xtext.aadl2.ui.internal.Aadl2Activator;
import fr.irit.fiacre.etoile.xtext.FiacreStandaloneSetup;
import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory;
import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;
import fr.irit.fiacre.etoile.xtext.fiacre.Model;

import fr.laas.vertics.aadl2.verification.transformation.AADLTransformer;
import fr.laas.vertics.aadl2.verification.transformation.ForbiddenConstructException;

public class VerificationAction implements IObjectActionDelegate {

	private Shell shell;
	private ISelection selection;

	/**
	 * Constructor for Action1.
	 */

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */

	public void run(IAction action) {

		// create my console
		MessageConsole myConsole = findConsole("Aadl 2 verification toolchain");
		myConsole.clearConsole();
		MessageConsoleStream out = myConsole.newMessageStream();
		PrintStream pout = new PrintStream(out);
		out.println("Start the generation process");
		try {
			out.flush();
		} catch (IOException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace(pout);
		}

		

		// create the verification folder that contains the generated fiacre model and the libV4 model
		IFile aadl_file = getFile();
		IFolder aadl_folder = (IFolder) aadl_file.getParent();
		out.println("Aadl model input file " + aadl_file.getName() + " in folder " + nameWithoutExtension(aadl_file));

		// load the library with platform:/plugin

		// load my instance model .aaxl2
		Aadl2Activator.getInstance();
		Element aadl_model_root = AadlUtil.getElement(aadl_file);
		out.println("Type of the model root element: " + aadl_model_root.getClass().getCanonicalName());
		// TODO : Find in the AADL metamodel how to the SystemInstance from a Package.
		if (aadl_model_root instanceof SystemInstance) {
			SystemInstance aadl_instance = (SystemInstance) aadl_model_root;
			out.println("Trace de l'instance du systeme : " + aadl_instance);

			AadlPackage aadl_package = (AadlPackage) aadl_instance
					.getComponentImplementation().eContainer().eContainer();
			out.println("Trace de paquetage du systeme : " + aadl_package);
			
			// out.println("Trace : " + instances.getName());
			IFolder fiacre_folder = aadl_folder.getFolder(nameWithoutExtension(aadl_file) + "_verification");
			out.println("Creating folder for Fiacre file generation" + fiacre_folder.getName());
			if (fiacre_folder.exists())
				try {
					fiacre_folder.delete(true, null);
				} catch (CoreException e2) {
					// TODO Auto-generated catch block
					out.println("Exception when deleting existing generation folder : " + e2);
					e2.printStackTrace(pout);
				}
			try {
				fiacre_folder.create(true, true, null);
			} catch (CoreException e1) {
				// TODO Auto-generated catch block
				out.println("Exception when creating new generation folder : " + e1);
				e1.printStackTrace(pout);
			}
			// out.println("Trace : " + repertoire_verification.getName() + " = " + repertoire_verification.exists());
			
			// preload fiacre xmi resource
			FiacreStandaloneSetup.doSetup();
			// out.println("Trace initialisation Xtext Fiacre ");
			ResourceSet resourceSet = new ResourceSetImpl();
			URI fiacre_file_uri = URI.createFileURI(fiacre_folder.getLocation().toString() + ".xmi");
			// out.println("Trace nom fichier XMI : " + uri_fiacre);
			XMIResourceFactoryImpl xmi_factory = new XMIResourceFactoryImpl();
			XMIResource fiacre_xmi_resource = (XMIResource) xmi_factory.createResource(fiacre_file_uri);
			// out.println("Trace resource Fiacre XMI: " + fiacre_xmi_resource);
			FiacrePackage.eINSTANCE.eClass();

			// create fiacre resource (as xmi)
			FiacreFactory fiacre_factory = FiacreFactory.eINSTANCE;
			Model fiacre_model = fiacre_factory.createModel();
			// out.println("Trace modèle vide Fiacre : " + model);
			// load libV4 from lib folder in the plugin
			Resource fiacre_library_resource = resourceSet
					.getResource(
							URI.createURI("platform:/plugin/fr.laas.vertics.aadl2.verification/lib/libV4.fiacre"),
							true);
			// out.println("Trace resource librairie Fiacre : " + resource_library);
			Model library_model = (Model) fiacre_library_resource.getContents().get(0);
			// out.println("Trace modèle librairie Fiacre : " + library_model);
			// create a new resource to copy the lib in a fiacre resource created in the verification folder
			String fiacre_library_copy_file = fiacre_folder.getLocation().toString() + "/libV4.fiacre";
			Resource fiacre_library_copy_resource = resourceSet.createResource(URI.createFileURI(fiacre_library_copy_file));
			Model library_model_copy = library_model;
			fiacre_library_copy_resource.getContents().add(library_model_copy);
			// out.println("Trace modèle copie : " + copied_library_model);
			try {
				// save the the new libV4.fiacre
				fiacre_library_copy_resource.save(null);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				out.println("Trace : " + e1);
				e1.printStackTrace(pout);
			}

			try {
				// THE TRANSFORMATION AADL2FIACRE
				AADLTransformer aadl_transformer = new AADLTransformer(aadl_instance, fiacre_model, library_model_copy, out); 
				// out.println( "Trace : " + aadl_transformer);
				aadl_transformer.transform();

				// fill the fiacre xmi resource by a fiacre model
				fiacre_xmi_resource.getContents().add(fiacre_model);
				Map<Object, Object> m1 = fiacre_xmi_resource.getDefaultSaveOptions();
				m1.put(XMIResource.OPTION_USE_XMI_TYPE, Boolean.TRUE);

				// save the fiacre xtext resource
				// out.println("Trace : " + file.getLocation());
				String fiacre_model_filename = fiacre_folder.getLocation().toString()
					+ "/"
					+ nameWithoutExtension(aadl_file)
					+ ".fiacre";
				Resource fiacre_model_resource = resourceSet.createResource(URI.createFileURI(fiacre_model_filename));
				out.println("Target Fiacre file: " + fiacre_model_filename);
				try {
					fiacre_xmi_resource.save(null);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					out.println("Trace : " + e);
					e.printStackTrace(pout);
				}
				EObject fiacre_model_root = fiacre_xmi_resource.getContents().get(0);
				fiacre_model_resource.getContents().add(fiacre_model_root);

				try {
					fiacre_model_resource.save(null);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					out.println("Trace : " + e);
					e.printStackTrace(pout);
				}

				// refresh target file project
				try {
					aadl_file.getProject().refreshLocal(IResource.DEPTH_INFINITE, null);
				} catch (Exception ex) {
					out.println("Trace : " + ex);
				}
			}
			catch (ForbiddenConstructException _fce) {
				out.println("This AADL2 construct is not handled by the AADL2 Verifier.");
				out.println(_fce.getMessage());
				out.println(_fce.element.toString());
				out.println("Trace: " + _fce);
				_fce.printStackTrace(pout);
			}
			finally {
			}
		} else {
			out.println("This AADL model is not a System Instance model.");
		}

	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
	}

	private IFile getFile() {
		return (IFile) ((IStructuredSelection) selection).getFirstElement();
	}

	private MessageConsole findConsole(String name) {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++)
			if (name.equals(existing[i].getName()))
				return (MessageConsole) existing[i];
		// no console found, so create a new one
		MessageConsole myConsole = new MessageConsole(name, null);
		conMan.addConsoles(new IConsole[] { myConsole });
		return myConsole;
	}

	private String nameWithoutExtension(IFile file) {
		String fileName = file.getName();
		int pos = fileName.lastIndexOf(".");
		if (pos > 0) {
			fileName = fileName.substring(0, pos);
		}
		return fileName;
	}
}
