/*
 * generated by Xtext
 */
package fr.irit.fiacre.etoile.xtext.serializer;

import com.google.inject.Inject;
import fr.irit.fiacre.etoile.xtext.services.FiacreGrammarAccess;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AlternativeAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;

@SuppressWarnings("all")
public class FiacreSyntacticSequencer extends AbstractSyntacticSequencer {

	protected FiacreGrammarAccess grammarAccess;
	protected AbstractElementAlias match_AtomicExpression_LeftParenthesisKeyword_8_0_a;
	protected AbstractElementAlias match_AtomicExpression_LeftParenthesisKeyword_8_0_p;
	protected AbstractElementAlias match_AtomicLTL_LeftParenthesisKeyword_2_0_a;
	protected AbstractElementAlias match_AtomicLTL_LeftParenthesisKeyword_2_0_p;
	protected AbstractElementAlias match_AtomicObservable_LeftParenthesisKeyword_0_0_a;
	protected AbstractElementAlias match_AtomicObservable_LeftParenthesisKeyword_0_0_p;
	protected AbstractElementAlias match_AtomicProperty_LeftParenthesisKeyword_1_0_a;
	protected AbstractElementAlias match_AtomicProperty_LeftParenthesisKeyword_1_0_p;
	protected AbstractElementAlias match_CaseStatement_CaseKeyword_8_q;
	protected AbstractElementAlias match_Composition_ParKeyword_6_q;
	protected AbstractElementAlias match_ConditionalStatement_IfKeyword_7_q;
	protected AbstractElementAlias match_ForeachStatement_ForeachKeyword_5_q;
	protected AbstractElementAlias match_GenericDeclaration_ConstKeyword_1_0_q;
	protected AbstractElementAlias match_GenericDeclaration_Constr0Keyword_2_0_0_or_Constr1Keyword_2_0_1;
	protected AbstractElementAlias match_Pattern_LeftParenthesisKeyword_3_0_a;
	protected AbstractElementAlias match_RecordType_RecordKeyword_4_q;
	protected AbstractElementAlias match_SelectStatement_SelectKeyword_4_q;
	protected AbstractElementAlias match_UnionType_UnionKeyword_4_q;
	protected AbstractElementAlias match_WhileStatement_WhileKeyword_5_q;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (FiacreGrammarAccess) access;
		match_AtomicExpression_LeftParenthesisKeyword_8_0_a = new TokenAlias(true, true, grammarAccess.getAtomicExpressionAccess().getLeftParenthesisKeyword_8_0());
		match_AtomicExpression_LeftParenthesisKeyword_8_0_p = new TokenAlias(true, false, grammarAccess.getAtomicExpressionAccess().getLeftParenthesisKeyword_8_0());
		match_AtomicLTL_LeftParenthesisKeyword_2_0_a = new TokenAlias(true, true, grammarAccess.getAtomicLTLAccess().getLeftParenthesisKeyword_2_0());
		match_AtomicLTL_LeftParenthesisKeyword_2_0_p = new TokenAlias(true, false, grammarAccess.getAtomicLTLAccess().getLeftParenthesisKeyword_2_0());
		match_AtomicObservable_LeftParenthesisKeyword_0_0_a = new TokenAlias(true, true, grammarAccess.getAtomicObservableAccess().getLeftParenthesisKeyword_0_0());
		match_AtomicObservable_LeftParenthesisKeyword_0_0_p = new TokenAlias(true, false, grammarAccess.getAtomicObservableAccess().getLeftParenthesisKeyword_0_0());
		match_AtomicProperty_LeftParenthesisKeyword_1_0_a = new TokenAlias(true, true, grammarAccess.getAtomicPropertyAccess().getLeftParenthesisKeyword_1_0());
		match_AtomicProperty_LeftParenthesisKeyword_1_0_p = new TokenAlias(true, false, grammarAccess.getAtomicPropertyAccess().getLeftParenthesisKeyword_1_0());
		match_CaseStatement_CaseKeyword_8_q = new TokenAlias(false, true, grammarAccess.getCaseStatementAccess().getCaseKeyword_8());
		match_Composition_ParKeyword_6_q = new TokenAlias(false, true, grammarAccess.getCompositionAccess().getParKeyword_6());
		match_ConditionalStatement_IfKeyword_7_q = new TokenAlias(false, true, grammarAccess.getConditionalStatementAccess().getIfKeyword_7());
		match_ForeachStatement_ForeachKeyword_5_q = new TokenAlias(false, true, grammarAccess.getForeachStatementAccess().getForeachKeyword_5());
		match_GenericDeclaration_ConstKeyword_1_0_q = new TokenAlias(false, true, grammarAccess.getGenericDeclarationAccess().getConstKeyword_1_0());
		match_GenericDeclaration_Constr0Keyword_2_0_0_or_Constr1Keyword_2_0_1 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getGenericDeclarationAccess().getConstr0Keyword_2_0_0()), new TokenAlias(false, false, grammarAccess.getGenericDeclarationAccess().getConstr1Keyword_2_0_1()));
		match_Pattern_LeftParenthesisKeyword_3_0_a = new TokenAlias(true, true, grammarAccess.getPatternAccess().getLeftParenthesisKeyword_3_0());
		match_RecordType_RecordKeyword_4_q = new TokenAlias(false, true, grammarAccess.getRecordTypeAccess().getRecordKeyword_4());
		match_SelectStatement_SelectKeyword_4_q = new TokenAlias(false, true, grammarAccess.getSelectStatementAccess().getSelectKeyword_4());
		match_UnionType_UnionKeyword_4_q = new TokenAlias(false, true, grammarAccess.getUnionTypeAccess().getUnionKeyword_4());
		match_WhileStatement_WhileKeyword_5_q = new TokenAlias(false, true, grammarAccess.getWhileStatementAccess().getWhileKeyword_5());
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		return "";
	}
	
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if(match_AtomicExpression_LeftParenthesisKeyword_8_0_a.equals(syntax))
				emit_AtomicExpression_LeftParenthesisKeyword_8_0_a(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_AtomicExpression_LeftParenthesisKeyword_8_0_p.equals(syntax))
				emit_AtomicExpression_LeftParenthesisKeyword_8_0_p(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_AtomicLTL_LeftParenthesisKeyword_2_0_a.equals(syntax))
				emit_AtomicLTL_LeftParenthesisKeyword_2_0_a(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_AtomicLTL_LeftParenthesisKeyword_2_0_p.equals(syntax))
				emit_AtomicLTL_LeftParenthesisKeyword_2_0_p(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_AtomicObservable_LeftParenthesisKeyword_0_0_a.equals(syntax))
				emit_AtomicObservable_LeftParenthesisKeyword_0_0_a(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_AtomicObservable_LeftParenthesisKeyword_0_0_p.equals(syntax))
				emit_AtomicObservable_LeftParenthesisKeyword_0_0_p(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_AtomicProperty_LeftParenthesisKeyword_1_0_a.equals(syntax))
				emit_AtomicProperty_LeftParenthesisKeyword_1_0_a(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_AtomicProperty_LeftParenthesisKeyword_1_0_p.equals(syntax))
				emit_AtomicProperty_LeftParenthesisKeyword_1_0_p(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_CaseStatement_CaseKeyword_8_q.equals(syntax))
				emit_CaseStatement_CaseKeyword_8_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Composition_ParKeyword_6_q.equals(syntax))
				emit_Composition_ParKeyword_6_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_ConditionalStatement_IfKeyword_7_q.equals(syntax))
				emit_ConditionalStatement_IfKeyword_7_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_ForeachStatement_ForeachKeyword_5_q.equals(syntax))
				emit_ForeachStatement_ForeachKeyword_5_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_GenericDeclaration_ConstKeyword_1_0_q.equals(syntax))
				emit_GenericDeclaration_ConstKeyword_1_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_GenericDeclaration_Constr0Keyword_2_0_0_or_Constr1Keyword_2_0_1.equals(syntax))
				emit_GenericDeclaration_Constr0Keyword_2_0_0_or_Constr1Keyword_2_0_1(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Pattern_LeftParenthesisKeyword_3_0_a.equals(syntax))
				emit_Pattern_LeftParenthesisKeyword_3_0_a(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_RecordType_RecordKeyword_4_q.equals(syntax))
				emit_RecordType_RecordKeyword_4_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_SelectStatement_SelectKeyword_4_q.equals(syntax))
				emit_SelectStatement_SelectKeyword_4_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_UnionType_UnionKeyword_4_q.equals(syntax))
				emit_UnionType_UnionKeyword_4_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_WhileStatement_WhileKeyword_5_q.equals(syntax))
				emit_WhileStatement_WhileKeyword_5_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Ambiguous syntax:
	 *     '('*
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) '[' (ambiguity) '$' child=UnaryExpression
	 *     (rule start) '[' (ambiguity) '&' declaration=[ReferenceDeclarationUse|ID]
	 *     (rule start) '[' (ambiguity) '+' child=UnaryExpression
	 *     (rule start) '[' (ambiguity) '-' child=UnaryExpression
	 *     (rule start) '[' (ambiguity) 'all' index=VariableDeclaration
	 *     (rule start) '[' (ambiguity) 'append' '(' left=Expression
	 *     (rule start) '[' (ambiguity) 'dequeue' child=UnaryExpression
	 *     (rule start) '[' (ambiguity) 'empty' child=UnaryExpression
	 *     (rule start) '[' (ambiguity) 'enqueue' '(' element=Expression
	 *     (rule start) '[' (ambiguity) 'exists' index=VariableDeclaration
	 *     (rule start) '[' (ambiguity) 'false' '..' (rule start)
	 *     (rule start) '[' (ambiguity) 'false' ']' (rule start)
	 *     (rule start) '[' (ambiguity) 'false' (rule start)
	 *     (rule start) '[' (ambiguity) 'first' child=UnaryExpression
	 *     (rule start) '[' (ambiguity) 'full' child=UnaryExpression
	 *     (rule start) '[' (ambiguity) 'length' child=UnaryExpression
	 *     (rule start) '[' (ambiguity) 'not' child=UnaryExpression
	 *     (rule start) '[' (ambiguity) 'true' '..' (rule start)
	 *     (rule start) '[' (ambiguity) 'true' ']' (rule start)
	 *     (rule start) '[' (ambiguity) 'true' (rule start)
	 *     (rule start) '[' (ambiguity) '{' fields+=FieldExpression
	 *     (rule start) '[' (ambiguity) '{|' '|}' '..' (rule start)
	 *     (rule start) '[' (ambiguity) '{|' '|}' ']' (rule start)
	 *     (rule start) '[' (ambiguity) '{|' '|}' (rule start)
	 *     (rule start) '[' (ambiguity) '{|' values+=Expression
	 *     (rule start) '[' (ambiguity) declaration=[ExpressionDeclarationUse|ID]
	 *     (rule start) '[' (ambiguity) value=INT
	 *     (rule start) '[' (ambiguity) {Addition.left=}
	 *     (rule start) '[' (ambiguity) {ArrayAccessExpression.child=}
	 *     (rule start) '[' (ambiguity) {ComparisonEqual.left=}
	 *     (rule start) '[' (ambiguity) {ComparisonGreater.left=}
	 *     (rule start) '[' (ambiguity) {ComparisonGreaterEqual.left=}
	 *     (rule start) '[' (ambiguity) {ComparisonLesser.left=}
	 *     (rule start) '[' (ambiguity) {ComparisonLesserEqual.left=}
	 *     (rule start) '[' (ambiguity) {ComparisonNotEqual.left=}
	 *     (rule start) '[' (ambiguity) {Conditional.condition=}
	 *     (rule start) '[' (ambiguity) {Conjunction.left=}
	 *     (rule start) '[' (ambiguity) {Disjunction.left=}
	 *     (rule start) '[' (ambiguity) {Division.left=}
	 *     (rule start) '[' (ambiguity) {ExplicitArrayExpression.values+=}
	 *     (rule start) '[' (ambiguity) {Implication.left=}
	 *     (rule start) '[' (ambiguity) {ImplicitArrayExpression.body+=}
	 *     (rule start) '[' (ambiguity) {Modulo.left=}
	 *     (rule start) '[' (ambiguity) {Multiplication.left=}
	 *     (rule start) '[' (ambiguity) {Projection.channel=}
	 *     (rule start) '[' (ambiguity) {RecordAccessExpression.child=}
	 *     (rule start) '[' (ambiguity) {Substraction.left=}
	 *     (rule start) (ambiguity) '$' child=UnaryExpression
	 *     (rule start) (ambiguity) '&' declaration=[ReferenceDeclarationUse|ID]
	 *     (rule start) (ambiguity) '+' child=UnaryExpression
	 *     (rule start) (ambiguity) '-' child=UnaryExpression
	 *     (rule start) (ambiguity) 'all' index=VariableDeclaration
	 *     (rule start) (ambiguity) 'append' '(' left=Expression
	 *     (rule start) (ambiguity) 'dequeue' child=UnaryExpression
	 *     (rule start) (ambiguity) 'empty' child=UnaryExpression
	 *     (rule start) (ambiguity) 'enqueue' '(' element=Expression
	 *     (rule start) (ambiguity) 'exists' index=VariableDeclaration
	 *     (rule start) (ambiguity) 'false' '?' (rule start)
	 *     (rule start) (ambiguity) 'false' (rule start)
	 *     (rule start) (ambiguity) 'first' child=UnaryExpression
	 *     (rule start) (ambiguity) 'full' child=UnaryExpression
	 *     (rule start) (ambiguity) 'length' child=UnaryExpression
	 *     (rule start) (ambiguity) 'not' child=UnaryExpression
	 *     (rule start) (ambiguity) 'true' '?' (rule start)
	 *     (rule start) (ambiguity) 'true' (rule start)
	 *     (rule start) (ambiguity) '{' fields+=FieldExpression
	 *     (rule start) (ambiguity) '{|' '|}' '?' (rule start)
	 *     (rule start) (ambiguity) '{|' '|}' (rule start)
	 *     (rule start) (ambiguity) '{|' values+=Expression
	 *     (rule start) (ambiguity) declaration=[ExpressionDeclarationUse|ID]
	 *     (rule start) (ambiguity) value=INT
	 *     (rule start) (ambiguity) {Addition.left=}
	 *     (rule start) (ambiguity) {ArrayAccessExpression.child=}
	 *     (rule start) (ambiguity) {ComparisonEqual.left=}
	 *     (rule start) (ambiguity) {ComparisonGreater.left=}
	 *     (rule start) (ambiguity) {ComparisonGreaterEqual.left=}
	 *     (rule start) (ambiguity) {ComparisonLesser.left=}
	 *     (rule start) (ambiguity) {ComparisonLesserEqual.left=}
	 *     (rule start) (ambiguity) {ComparisonNotEqual.left=}
	 *     (rule start) (ambiguity) {Conditional.condition=}
	 *     (rule start) (ambiguity) {Conjunction.left=}
	 *     (rule start) (ambiguity) {Disjunction.left=}
	 *     (rule start) (ambiguity) {Division.left=}
	 *     (rule start) (ambiguity) {ExplicitArrayExpression.values+=}
	 *     (rule start) (ambiguity) {Implication.left=}
	 *     (rule start) (ambiguity) {ImplicitArrayExpression.body+=}
	 *     (rule start) (ambiguity) {Modulo.left=}
	 *     (rule start) (ambiguity) {Multiplication.left=}
	 *     (rule start) (ambiguity) {Projection.channel=}
	 *     (rule start) (ambiguity) {RecordAccessExpression.child=}
	 *     (rule start) (ambiguity) {Substraction.left=}
	 */
	protected void emit_AtomicExpression_LeftParenthesisKeyword_8_0_a(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     '('+
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) '$' child=UnaryExpression
	 *     (rule start) (ambiguity) '+' child=UnaryExpression
	 *     (rule start) (ambiguity) '-' child=UnaryExpression
	 *     (rule start) (ambiguity) 'all' index=VariableDeclaration
	 *     (rule start) (ambiguity) 'dequeue' child=UnaryExpression
	 *     (rule start) (ambiguity) 'empty' child=UnaryExpression
	 *     (rule start) (ambiguity) 'exists' index=VariableDeclaration
	 *     (rule start) (ambiguity) 'first' child=UnaryExpression
	 *     (rule start) (ambiguity) 'full' child=UnaryExpression
	 *     (rule start) (ambiguity) 'length' child=UnaryExpression
	 *     (rule start) (ambiguity) 'not' child=UnaryExpression
	 *     (rule start) (ambiguity) {Addition.left=}
	 *     (rule start) (ambiguity) {ComparisonEqual.left=}
	 *     (rule start) (ambiguity) {ComparisonGreater.left=}
	 *     (rule start) (ambiguity) {ComparisonGreaterEqual.left=}
	 *     (rule start) (ambiguity) {ComparisonLesser.left=}
	 *     (rule start) (ambiguity) {ComparisonLesserEqual.left=}
	 *     (rule start) (ambiguity) {ComparisonNotEqual.left=}
	 *     (rule start) (ambiguity) {Conditional.condition=}
	 *     (rule start) (ambiguity) {Conjunction.left=}
	 *     (rule start) (ambiguity) {Disjunction.left=}
	 *     (rule start) (ambiguity) {Division.left=}
	 *     (rule start) (ambiguity) {Implication.left=}
	 *     (rule start) (ambiguity) {Modulo.left=}
	 *     (rule start) (ambiguity) {Multiplication.left=}
	 *     (rule start) (ambiguity) {Projection.channel=}
	 *     (rule start) (ambiguity) {Substraction.left=}
	 */
	protected void emit_AtomicExpression_LeftParenthesisKeyword_8_0_p(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     '('*
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) '<>' operand=LTLUnary
	 *     (rule start) (ambiguity) '[]' operand=LTLUnary
	 *     (rule start) (ambiguity) 'all' index=VariableDeclaration
	 *     (rule start) (ambiguity) 'enter' subject=AtomicObservable
	 *     (rule start) (ambiguity) 'exists' index=VariableDeclaration
	 *     (rule start) (ambiguity) 'leave' subject=AtomicObservable
	 *     (rule start) (ambiguity) 'next' operand=LTLUnary
	 *     (rule start) (ambiguity) 'not' operand=LTLUnary
	 *     (rule start) (ambiguity) path=Path
	 *     (rule start) (ambiguity) variable=[VariableDeclaration|ID]
	 *     (rule start) (ambiguity) {LTLConjunction.left=}
	 *     (rule start) (ambiguity) {LTLDisjunction.left=}
	 *     (rule start) (ambiguity) {LTLImplication.left=}
	 *     (rule start) (ambiguity) {LTLRelease.left=}
	 *     (rule start) (ambiguity) {LTLUntil.left=}
	 */
	protected void emit_AtomicLTL_LeftParenthesisKeyword_2_0_a(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     '('+
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) '<>' operand=LTLUnary
	 *     (rule start) (ambiguity) '[]' operand=LTLUnary
	 *     (rule start) (ambiguity) 'all' index=VariableDeclaration
	 *     (rule start) (ambiguity) 'exists' index=VariableDeclaration
	 *     (rule start) (ambiguity) 'next' operand=LTLUnary
	 *     (rule start) (ambiguity) 'not' operand=LTLUnary
	 *     (rule start) (ambiguity) {LTLConjunction.left=}
	 *     (rule start) (ambiguity) {LTLDisjunction.left=}
	 *     (rule start) (ambiguity) {LTLImplication.left=}
	 *     (rule start) (ambiguity) {LTLRelease.left=}
	 *     (rule start) (ambiguity) {LTLUntil.left=}
	 */
	protected void emit_AtomicLTL_LeftParenthesisKeyword_2_0_p(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     '('*
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) 'enter' subject=AtomicObservable
	 *     (rule start) (ambiguity) 'leave' subject=AtomicObservable
	 *     (rule start) (ambiguity) 'not' child=ObservableNegation
	 *     (rule start) (ambiguity) path=Path
	 *     (rule start) (ambiguity) {ObservableConjunction.left=}
	 *     (rule start) (ambiguity) {ObservableDisjunction.left=}
	 *     (rule start) (ambiguity) {ObservableImplication.left=}
	 */
	protected void emit_AtomicObservable_LeftParenthesisKeyword_0_0_a(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     '('+
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) 'not' child=ObservableNegation
	 *     (rule start) (ambiguity) {ObservableConjunction.left=}
	 *     (rule start) (ambiguity) {ObservableDisjunction.left=}
	 *     (rule start) (ambiguity) {ObservableImplication.left=}
	 */
	protected void emit_AtomicObservable_LeftParenthesisKeyword_0_0_p(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     '('*
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) '<<' 'absent' subject=AtomicObservable
	 *     (rule start) (ambiguity) '<<' 'always' subject=AtomicObservable
	 *     (rule start) (ambiguity) '<<' 'deadlockfree' '>>' (rule start)
	 *     (rule start) (ambiguity) '<<' 'infinitelyoften' subject=AtomicObservable
	 *     (rule start) (ambiguity) '<<' 'ltl' property=LTLUnary
	 *     (rule start) (ambiguity) '<<' 'mortal' subject=AtomicObservable
	 *     (rule start) (ambiguity) '<<' 'present' subject=AtomicObservable
	 *     (rule start) (ambiguity) '<<' {LeadsToPattern.subject=}
	 *     (rule start) (ambiguity) '<<' {PrecedesPattern.subject=}
	 *     (rule start) (ambiguity) 'all' variable=VariableDeclaration
	 *     (rule start) (ambiguity) 'exists' variable=VariableDeclaration
	 *     (rule start) (ambiguity) 'not' child=PropertyNegation
	 *     (rule start) (ambiguity) {PropertyConjunction.left=}
	 *     (rule start) (ambiguity) {PropertyDisjunction.left=}
	 *     (rule start) (ambiguity) {PropertyImplication.left=}
	 */
	protected void emit_AtomicProperty_LeftParenthesisKeyword_1_0_a(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     '('+
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) 'all' variable=VariableDeclaration
	 *     (rule start) (ambiguity) 'exists' variable=VariableDeclaration
	 *     (rule start) (ambiguity) 'not' child=PropertyNegation
	 *     (rule start) (ambiguity) {PropertyConjunction.left=}
	 *     (rule start) (ambiguity) {PropertyDisjunction.left=}
	 *     (rule start) (ambiguity) {PropertyImplication.left=}
	 */
	protected void emit_AtomicProperty_LeftParenthesisKeyword_1_0_p(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     'case'?
	 *
	 * This ambiguous syntax occurs at:
	 *     body+=StatementSequence 'end' (ambiguity) (rule end)
	 */
	protected void emit_CaseStatement_CaseKeyword_8_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     'par'?
	 *
	 * This ambiguous syntax occurs at:
	 *     blocks+=Block 'end' (ambiguity) (rule end)
	 *     type=RangeType 'end' (ambiguity) (rule end)
	 */
	protected void emit_Composition_ParKeyword_6_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     'if'?
	 *
	 * This ambiguous syntax occurs at:
	 *     else=StatementSequence 'end' (ambiguity) (rule end)
	 *     elseif+=ExtendedConditionalStatement 'end' (ambiguity) (rule end)
	 *     then=StatementSequence 'end' (ambiguity) (rule end)
	 */
	protected void emit_ConditionalStatement_IfKeyword_7_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     'foreach'?
	 *
	 * This ambiguous syntax occurs at:
	 *     body=StatementSequence 'end' (ambiguity) (rule end)
	 */
	protected void emit_ForeachStatement_ForeachKeyword_5_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     'const'?
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) name=ID
	 */
	protected void emit_GenericDeclaration_ConstKeyword_1_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     'constr0' | 'constr1'
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) name=ID
	 */
	protected void emit_GenericDeclaration_Constr0Keyword_2_0_0_or_Constr1Keyword_2_0_1(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     '('*
	 *
	 * This ambiguous syntax occurs at:
	 *     (rule start) (ambiguity) '+' value=INT
	 *     (rule start) (ambiguity) 'any' '!' (rule start)
	 *     (rule start) (ambiguity) 'any' ',' (rule start)
	 *     (rule start) (ambiguity) 'any' ':=' (rule start)
	 *     (rule start) (ambiguity) 'any' '?' (rule start)
	 *     (rule start) (ambiguity) 'any' (rule start)
	 *     (rule start) (ambiguity) declaration=[PatternDeclarationUse|ID]
	 *     (rule start) (ambiguity) negative?='-'
	 *     (rule start) (ambiguity) value=LiteralExpression
	 *     (rule start) (ambiguity) {ArrayAccessPattern.source=}
	 *     (rule start) (ambiguity) {RecordAccessPattern.source=}
	 */
	protected void emit_Pattern_LeftParenthesisKeyword_3_0_a(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     'record'?
	 *
	 * This ambiguous syntax occurs at:
	 *     fields+=RecordFields 'end' (ambiguity) (rule end)
	 */
	protected void emit_RecordType_RecordKeyword_4_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     'select'?
	 *
	 * This ambiguous syntax occurs at:
	 *     body=UnlessStatement 'end' (ambiguity) (rule end)
	 *     type=RangeType 'end' (ambiguity) (rule end)
	 */
	protected void emit_SelectStatement_SelectKeyword_4_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     'union'?
	 *
	 * This ambiguous syntax occurs at:
	 *     tags+=UnionTags 'end' (ambiguity) (rule end)
	 */
	protected void emit_UnionType_UnionKeyword_4_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Ambiguous syntax:
	 *     'while'?
	 *
	 * This ambiguous syntax occurs at:
	 *     body=StatementSequence 'end' (ambiguity) (rule end)
	 */
	protected void emit_WhileStatement_WhileKeyword_5_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
