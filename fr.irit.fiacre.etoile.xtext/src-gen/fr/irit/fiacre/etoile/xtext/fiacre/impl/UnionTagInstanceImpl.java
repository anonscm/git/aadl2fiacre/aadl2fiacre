/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;
import fr.irit.fiacre.etoile.xtext.fiacre.UnionTagDeclarationUse;
import fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Union Tag Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnionTagInstanceImpl#isConstr0 <em>Constr0</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnionTagInstanceImpl#isConstr1 <em>Constr1</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnionTagInstanceImpl#getHead <em>Head</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UnionTagInstanceImpl extends GenericInstanceImpl implements UnionTagInstance
{
  /**
   * The default value of the '{@link #isConstr0() <em>Constr0</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isConstr0()
   * @generated
   * @ordered
   */
  protected static final boolean CONSTR0_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isConstr0() <em>Constr0</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isConstr0()
   * @generated
   * @ordered
   */
  protected boolean constr0 = CONSTR0_EDEFAULT;

  /**
   * The default value of the '{@link #isConstr1() <em>Constr1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isConstr1()
   * @generated
   * @ordered
   */
  protected static final boolean CONSTR1_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isConstr1() <em>Constr1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isConstr1()
   * @generated
   * @ordered
   */
  protected boolean constr1 = CONSTR1_EDEFAULT;

  /**
   * The cached value of the '{@link #getHead() <em>Head</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHead()
   * @generated
   * @ordered
   */
  protected UnionTagDeclarationUse head;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected UnionTagInstanceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FiacrePackage.eINSTANCE.getUnionTagInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isConstr0()
  {
    return constr0;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConstr0(boolean newConstr0)
  {
    boolean oldConstr0 = constr0;
    constr0 = newConstr0;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.UNION_TAG_INSTANCE__CONSTR0, oldConstr0, constr0));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isConstr1()
  {
    return constr1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConstr1(boolean newConstr1)
  {
    boolean oldConstr1 = constr1;
    constr1 = newConstr1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.UNION_TAG_INSTANCE__CONSTR1, oldConstr1, constr1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnionTagDeclarationUse getHead()
  {
    if (head != null && head.eIsProxy())
    {
      InternalEObject oldHead = (InternalEObject)head;
      head = (UnionTagDeclarationUse)eResolveProxy(oldHead);
      if (head != oldHead)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, FiacrePackage.UNION_TAG_INSTANCE__HEAD, oldHead, head));
      }
    }
    return head;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnionTagDeclarationUse basicGetHead()
  {
    return head;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setHead(UnionTagDeclarationUse newHead)
  {
    UnionTagDeclarationUse oldHead = head;
    head = newHead;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.UNION_TAG_INSTANCE__HEAD, oldHead, head));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case FiacrePackage.UNION_TAG_INSTANCE__CONSTR0:
        return isConstr0();
      case FiacrePackage.UNION_TAG_INSTANCE__CONSTR1:
        return isConstr1();
      case FiacrePackage.UNION_TAG_INSTANCE__HEAD:
        if (resolve) return getHead();
        return basicGetHead();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case FiacrePackage.UNION_TAG_INSTANCE__CONSTR0:
        setConstr0((Boolean)newValue);
        return;
      case FiacrePackage.UNION_TAG_INSTANCE__CONSTR1:
        setConstr1((Boolean)newValue);
        return;
      case FiacrePackage.UNION_TAG_INSTANCE__HEAD:
        setHead((UnionTagDeclarationUse)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.UNION_TAG_INSTANCE__CONSTR0:
        setConstr0(CONSTR0_EDEFAULT);
        return;
      case FiacrePackage.UNION_TAG_INSTANCE__CONSTR1:
        setConstr1(CONSTR1_EDEFAULT);
        return;
      case FiacrePackage.UNION_TAG_INSTANCE__HEAD:
        setHead((UnionTagDeclarationUse)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.UNION_TAG_INSTANCE__CONSTR0:
        return constr0 != CONSTR0_EDEFAULT;
      case FiacrePackage.UNION_TAG_INSTANCE__CONSTR1:
        return constr1 != CONSTR1_EDEFAULT;
      case FiacrePackage.UNION_TAG_INSTANCE__HEAD:
        return head != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (constr0: ");
    result.append(constr0);
    result.append(", constr1: ");
    result.append(constr1);
    result.append(')');
    return result.toString();
  }

} //UnionTagInstanceImpl
