/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance#getComponent <em>Component</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance#getGenerics <em>Generics</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance#getPorts <em>Ports</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentInstance()
 * @model
 * @generated
 */
public interface ComponentInstance extends EObject
{
  /**
   * Returns the value of the '<em><b>Component</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Component</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Component</em>' reference.
   * @see #setComponent(ParameterizedDeclaration)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentInstance_Component()
   * @model
   * @generated
   */
  ParameterizedDeclaration getComponent();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance#getComponent <em>Component</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Component</em>' reference.
   * @see #getComponent()
   * @generated
   */
  void setComponent(ParameterizedDeclaration value);

  /**
   * Returns the value of the '<em><b>Generics</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.GenericInstance}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Generics</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Generics</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentInstance_Generics()
   * @model containment="true"
   * @generated
   */
  EList<GenericInstance> getGenerics();

  /**
   * Returns the value of the '<em><b>Ports</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.Expression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ports</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ports</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentInstance_Ports()
   * @model containment="true"
   * @generated
   */
  EList<Expression> getPorts();

  /**
   * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.Expression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parameters</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentInstance_Parameters()
   * @model containment="true"
   * @generated
   */
  EList<Expression> getParameters();

} // ComponentInstance
