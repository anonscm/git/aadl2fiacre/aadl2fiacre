/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Union Tag Declaration Use</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getUnionTagDeclarationUse()
 * @model
 * @generated
 */
public interface UnionTagDeclarationUse extends EObject
{
} // UnionTagDeclarationUse
