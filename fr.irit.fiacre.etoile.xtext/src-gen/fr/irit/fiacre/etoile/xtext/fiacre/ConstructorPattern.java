/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constructor Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstructorPattern#getParameter <em>Parameter</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getConstructorPattern()
 * @model
 * @generated
 */
public interface ConstructorPattern extends IdentifierPattern
{
  /**
   * Returns the value of the '<em><b>Parameter</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parameter</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parameter</em>' containment reference.
   * @see #setParameter(Pattern)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getConstructorPattern_Parameter()
   * @model containment="true"
   * @generated
   */
  Pattern getParameter();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstructorPattern#getParameter <em>Parameter</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parameter</em>' containment reference.
   * @see #getParameter()
   * @generated
   */
  void setParameter(Pattern value);

} // ConstructorPattern
