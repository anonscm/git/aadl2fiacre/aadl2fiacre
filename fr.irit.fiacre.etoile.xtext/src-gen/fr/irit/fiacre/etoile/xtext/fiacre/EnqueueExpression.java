/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enqueue Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.EnqueueExpression#getElement <em>Element</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.EnqueueExpression#getQueue <em>Queue</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getEnqueueExpression()
 * @model
 * @generated
 */
public interface EnqueueExpression extends Expression
{
  /**
   * Returns the value of the '<em><b>Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Element</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Element</em>' containment reference.
   * @see #setElement(Expression)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getEnqueueExpression_Element()
   * @model containment="true"
   * @generated
   */
  Expression getElement();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.EnqueueExpression#getElement <em>Element</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Element</em>' containment reference.
   * @see #getElement()
   * @generated
   */
  void setElement(Expression value);

  /**
   * Returns the value of the '<em><b>Queue</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Queue</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Queue</em>' containment reference.
   * @see #setQueue(Expression)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getEnqueueExpression_Queue()
   * @model containment="true"
   * @generated
   */
  Expression getQueue();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.EnqueueExpression#getQueue <em>Queue</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Queue</em>' containment reference.
   * @see #getQueue()
   * @generated
   */
  void setQueue(Expression value);

} // EnqueueExpression
