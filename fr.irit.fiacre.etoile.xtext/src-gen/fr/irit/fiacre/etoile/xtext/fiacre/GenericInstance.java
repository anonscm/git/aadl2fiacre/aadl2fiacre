/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getGenericInstance()
 * @model
 * @generated
 */
public interface GenericInstance extends EObject
{
} // GenericInstance
