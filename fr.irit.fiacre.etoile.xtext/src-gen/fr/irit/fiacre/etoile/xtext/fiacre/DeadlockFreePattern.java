/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deadlock Free Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getDeadlockFreePattern()
 * @model
 * @generated
 */
public interface DeadlockFreePattern extends PatternProperty
{
} // DeadlockFreePattern
