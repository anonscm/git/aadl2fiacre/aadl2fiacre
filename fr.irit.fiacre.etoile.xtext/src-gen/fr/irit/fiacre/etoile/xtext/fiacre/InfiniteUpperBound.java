/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Infinite Upper Bound</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getInfiniteUpperBound()
 * @model
 * @generated
 */
public interface InfiniteUpperBound extends UpperBound
{
} // InfiniteUpperBound
