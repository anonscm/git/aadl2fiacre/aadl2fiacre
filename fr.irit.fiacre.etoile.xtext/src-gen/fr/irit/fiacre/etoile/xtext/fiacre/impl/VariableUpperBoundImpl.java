/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.BoundDeclarationUse;
import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;
import fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable Upper Bound</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.VariableUpperBoundImpl#getVariable <em>Variable</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.VariableUpperBoundImpl#isLeft <em>Left</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.VariableUpperBoundImpl#isRight <em>Right</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VariableUpperBoundImpl extends UpperBoundImpl implements VariableUpperBound
{
  /**
   * The cached value of the '{@link #getVariable() <em>Variable</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVariable()
   * @generated
   * @ordered
   */
  protected BoundDeclarationUse variable;

  /**
   * The default value of the '{@link #isLeft() <em>Left</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isLeft()
   * @generated
   * @ordered
   */
  protected static final boolean LEFT_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isLeft() <em>Left</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isLeft()
   * @generated
   * @ordered
   */
  protected boolean left = LEFT_EDEFAULT;

  /**
   * The default value of the '{@link #isRight() <em>Right</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isRight()
   * @generated
   * @ordered
   */
  protected static final boolean RIGHT_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isRight() <em>Right</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isRight()
   * @generated
   * @ordered
   */
  protected boolean right = RIGHT_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected VariableUpperBoundImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FiacrePackage.eINSTANCE.getVariableUpperBound();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoundDeclarationUse getVariable()
  {
    if (variable != null && variable.eIsProxy())
    {
      InternalEObject oldVariable = (InternalEObject)variable;
      variable = (BoundDeclarationUse)eResolveProxy(oldVariable);
      if (variable != oldVariable)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, FiacrePackage.VARIABLE_UPPER_BOUND__VARIABLE, oldVariable, variable));
      }
    }
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoundDeclarationUse basicGetVariable()
  {
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVariable(BoundDeclarationUse newVariable)
  {
    BoundDeclarationUse oldVariable = variable;
    variable = newVariable;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.VARIABLE_UPPER_BOUND__VARIABLE, oldVariable, variable));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isLeft()
  {
    return left;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLeft(boolean newLeft)
  {
    boolean oldLeft = left;
    left = newLeft;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.VARIABLE_UPPER_BOUND__LEFT, oldLeft, left));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isRight()
  {
    return right;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRight(boolean newRight)
  {
    boolean oldRight = right;
    right = newRight;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.VARIABLE_UPPER_BOUND__RIGHT, oldRight, right));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case FiacrePackage.VARIABLE_UPPER_BOUND__VARIABLE:
        if (resolve) return getVariable();
        return basicGetVariable();
      case FiacrePackage.VARIABLE_UPPER_BOUND__LEFT:
        return isLeft();
      case FiacrePackage.VARIABLE_UPPER_BOUND__RIGHT:
        return isRight();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case FiacrePackage.VARIABLE_UPPER_BOUND__VARIABLE:
        setVariable((BoundDeclarationUse)newValue);
        return;
      case FiacrePackage.VARIABLE_UPPER_BOUND__LEFT:
        setLeft((Boolean)newValue);
        return;
      case FiacrePackage.VARIABLE_UPPER_BOUND__RIGHT:
        setRight((Boolean)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.VARIABLE_UPPER_BOUND__VARIABLE:
        setVariable((BoundDeclarationUse)null);
        return;
      case FiacrePackage.VARIABLE_UPPER_BOUND__LEFT:
        setLeft(LEFT_EDEFAULT);
        return;
      case FiacrePackage.VARIABLE_UPPER_BOUND__RIGHT:
        setRight(RIGHT_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.VARIABLE_UPPER_BOUND__VARIABLE:
        return variable != null;
      case FiacrePackage.VARIABLE_UPPER_BOUND__LEFT:
        return left != LEFT_EDEFAULT;
      case FiacrePackage.VARIABLE_UPPER_BOUND__RIGHT:
        return right != RIGHT_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (left: ");
    result.append(left);
    result.append(", right: ");
    result.append(right);
    result.append(')');
    return result.toString();
  }

} //VariableUpperBoundImpl
