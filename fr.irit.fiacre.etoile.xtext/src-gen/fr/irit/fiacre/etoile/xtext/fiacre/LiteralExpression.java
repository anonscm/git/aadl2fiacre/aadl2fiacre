/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Literal Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLiteralExpression()
 * @model
 * @generated
 */
public interface LiteralExpression extends Expression
{
} // LiteralExpression
