/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PortSet#isAllPorts <em>All Ports</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PortSet#getPorts <em>Ports</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPortSet()
 * @model
 * @generated
 */
public interface PortSet extends Block
{
  /**
   * Returns the value of the '<em><b>All Ports</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>All Ports</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>All Ports</em>' attribute.
   * @see #setAllPorts(boolean)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPortSet_AllPorts()
   * @model
   * @generated
   */
  boolean isAllPorts();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PortSet#isAllPorts <em>All Ports</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>All Ports</em>' attribute.
   * @see #isAllPorts()
   * @generated
   */
  void setAllPorts(boolean value);

  /**
   * Returns the value of the '<em><b>Ports</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.Expression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ports</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ports</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPortSet_Ports()
   * @model containment="true"
   * @generated
   */
  EList<Expression> getPorts();

} // PortSet
