/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;
import fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration;
import fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration;
import fr.irit.fiacre.etoile.xtext.fiacre.Type;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameters Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ParametersDeclarationImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ParametersDeclarationImpl#isRead <em>Read</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ParametersDeclarationImpl#isWrite <em>Write</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ParametersDeclarationImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParametersDeclarationImpl extends MinimalEObjectImpl.Container implements ParametersDeclaration
{
  /**
   * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParameters()
   * @generated
   * @ordered
   */
  protected EList<ParameterDeclaration> parameters;

  /**
   * The default value of the '{@link #isRead() <em>Read</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isRead()
   * @generated
   * @ordered
   */
  protected static final boolean READ_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isRead() <em>Read</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isRead()
   * @generated
   * @ordered
   */
  protected boolean read = READ_EDEFAULT;

  /**
   * The default value of the '{@link #isWrite() <em>Write</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isWrite()
   * @generated
   * @ordered
   */
  protected static final boolean WRITE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isWrite() <em>Write</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isWrite()
   * @generated
   * @ordered
   */
  protected boolean write = WRITE_EDEFAULT;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected Type type;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ParametersDeclarationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FiacrePackage.eINSTANCE.getParametersDeclaration();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ParameterDeclaration> getParameters()
  {
    if (parameters == null)
    {
      parameters = new EObjectContainmentEList<ParameterDeclaration>(ParameterDeclaration.class, this, FiacrePackage.PARAMETERS_DECLARATION__PARAMETERS);
    }
    return parameters;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isRead()
  {
    return read;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRead(boolean newRead)
  {
    boolean oldRead = read;
    read = newRead;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PARAMETERS_DECLARATION__READ, oldRead, read));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isWrite()
  {
    return write;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWrite(boolean newWrite)
  {
    boolean oldWrite = write;
    write = newWrite;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PARAMETERS_DECLARATION__WRITE, oldWrite, write));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(Type newType, NotificationChain msgs)
  {
    Type oldType = type;
    type = newType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.PARAMETERS_DECLARATION__TYPE, oldType, newType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(Type newType)
  {
    if (newType != type)
    {
      NotificationChain msgs = null;
      if (type != null)
        msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PARAMETERS_DECLARATION__TYPE, null, msgs);
      if (newType != null)
        msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PARAMETERS_DECLARATION__TYPE, null, msgs);
      msgs = basicSetType(newType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PARAMETERS_DECLARATION__TYPE, newType, newType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case FiacrePackage.PARAMETERS_DECLARATION__PARAMETERS:
        return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
      case FiacrePackage.PARAMETERS_DECLARATION__TYPE:
        return basicSetType(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case FiacrePackage.PARAMETERS_DECLARATION__PARAMETERS:
        return getParameters();
      case FiacrePackage.PARAMETERS_DECLARATION__READ:
        return isRead();
      case FiacrePackage.PARAMETERS_DECLARATION__WRITE:
        return isWrite();
      case FiacrePackage.PARAMETERS_DECLARATION__TYPE:
        return getType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case FiacrePackage.PARAMETERS_DECLARATION__PARAMETERS:
        getParameters().clear();
        getParameters().addAll((Collection<? extends ParameterDeclaration>)newValue);
        return;
      case FiacrePackage.PARAMETERS_DECLARATION__READ:
        setRead((Boolean)newValue);
        return;
      case FiacrePackage.PARAMETERS_DECLARATION__WRITE:
        setWrite((Boolean)newValue);
        return;
      case FiacrePackage.PARAMETERS_DECLARATION__TYPE:
        setType((Type)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.PARAMETERS_DECLARATION__PARAMETERS:
        getParameters().clear();
        return;
      case FiacrePackage.PARAMETERS_DECLARATION__READ:
        setRead(READ_EDEFAULT);
        return;
      case FiacrePackage.PARAMETERS_DECLARATION__WRITE:
        setWrite(WRITE_EDEFAULT);
        return;
      case FiacrePackage.PARAMETERS_DECLARATION__TYPE:
        setType((Type)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.PARAMETERS_DECLARATION__PARAMETERS:
        return parameters != null && !parameters.isEmpty();
      case FiacrePackage.PARAMETERS_DECLARATION__READ:
        return read != READ_EDEFAULT;
      case FiacrePackage.PARAMETERS_DECLARATION__WRITE:
        return write != WRITE_EDEFAULT;
      case FiacrePackage.PARAMETERS_DECLARATION__TYPE:
        return type != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (read: ");
    result.append(read);
    result.append(", write: ");
    result.append(write);
    result.append(')');
    return result.toString();
  }

} //ParametersDeclarationImpl
