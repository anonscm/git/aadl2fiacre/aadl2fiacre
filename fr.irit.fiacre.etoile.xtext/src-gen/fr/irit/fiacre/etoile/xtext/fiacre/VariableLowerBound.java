/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Lower Bound</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableLowerBound#getVariable <em>Variable</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getVariableLowerBound()
 * @model
 * @generated
 */
public interface VariableLowerBound extends LowerBound
{
  /**
   * Returns the value of the '<em><b>Variable</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variable</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variable</em>' reference.
   * @see #setVariable(BoundDeclarationUse)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getVariableLowerBound_Variable()
   * @model
   * @generated
   */
  BoundDeclarationUse getVariable();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableLowerBound#getVariable <em>Variable</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variable</em>' reference.
   * @see #getVariable()
   * @generated
   */
  void setVariable(BoundDeclarationUse value);

} // VariableLowerBound
