/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory
 * @model kind="package"
 * @generated
 */
public interface FiacrePackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "fiacre";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.irit.fr/fiacre/etoile/xtext/Fiacre";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "fiacre";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  FiacrePackage eINSTANCE = fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl.init();

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ModelImpl <em>Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ModelImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getModel()
   * @generated
   */
  int MODEL = 0;

  /**
   * The feature id for the '<em><b>Imports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__IMPORTS = 0;

  /**
   * The feature id for the '<em><b>Declarations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__DECLARATIONS = 1;

  /**
   * The feature id for the '<em><b>Requirements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__REQUIREMENTS = 2;

  /**
   * The feature id for the '<em><b>Root</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__ROOT = 3;

  /**
   * The number of structural features of the '<em>Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RootDeclarationImpl <em>Root Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.RootDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getRootDeclaration()
   * @generated
   */
  int ROOT_DECLARATION = 1;

  /**
   * The number of structural features of the '<em>Root Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ROOT_DECLARATION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.NamedElementImpl <em>Named Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.NamedElementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getNamedElement()
   * @generated
   */
  int NAMED_ELEMENT = 2;

  /**
   * The number of structural features of the '<em>Named Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMED_ELEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.TypeDeclarationUseImpl <em>Type Declaration Use</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.TypeDeclarationUseImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getTypeDeclarationUse()
   * @generated
   */
  int TYPE_DECLARATION_USE = 3;

  /**
   * The number of structural features of the '<em>Type Declaration Use</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DECLARATION_USE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstantDeclarationUseImpl <em>Constant Declaration Use</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstantDeclarationUseImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getConstantDeclarationUse()
   * @generated
   */
  int CONSTANT_DECLARATION_USE = 4;

  /**
   * The number of structural features of the '<em>Constant Declaration Use</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_DECLARATION_USE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ExpressionDeclarationUseImpl <em>Expression Declaration Use</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ExpressionDeclarationUseImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getExpressionDeclarationUse()
   * @generated
   */
  int EXPRESSION_DECLARATION_USE = 5;

  /**
   * The number of structural features of the '<em>Expression Declaration Use</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_DECLARATION_USE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ReferenceDeclarationUseImpl <em>Reference Declaration Use</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ReferenceDeclarationUseImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getReferenceDeclarationUse()
   * @generated
   */
  int REFERENCE_DECLARATION_USE = 6;

  /**
   * The number of structural features of the '<em>Reference Declaration Use</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_DECLARATION_USE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnionTagDeclarationUseImpl <em>Union Tag Declaration Use</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnionTagDeclarationUseImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnionTagDeclarationUse()
   * @generated
   */
  int UNION_TAG_DECLARATION_USE = 7;

  /**
   * The number of structural features of the '<em>Union Tag Declaration Use</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_TAG_DECLARATION_USE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordFieldDeclarationUseImpl <em>Record Field Declaration Use</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordFieldDeclarationUseImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getRecordFieldDeclarationUse()
   * @generated
   */
  int RECORD_FIELD_DECLARATION_USE = 8;

  /**
   * The number of structural features of the '<em>Record Field Declaration Use</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_FIELD_DECLARATION_USE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PatternDeclarationUseImpl <em>Pattern Declaration Use</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PatternDeclarationUseImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPatternDeclarationUse()
   * @generated
   */
  int PATTERN_DECLARATION_USE = 9;

  /**
   * The number of structural features of the '<em>Pattern Declaration Use</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATTERN_DECLARATION_USE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.BoundDeclarationUseImpl <em>Bound Declaration Use</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.BoundDeclarationUseImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getBoundDeclarationUse()
   * @generated
   */
  int BOUND_DECLARATION_USE = 10;

  /**
   * The number of structural features of the '<em>Bound Declaration Use</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOUND_DECLARATION_USE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PathDeclarationUseImpl <em>Path Declaration Use</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PathDeclarationUseImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPathDeclarationUse()
   * @generated
   */
  int PATH_DECLARATION_USE = 11;

  /**
   * The number of structural features of the '<em>Path Declaration Use</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATH_DECLARATION_USE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ImportDeclarationImpl <em>Import Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ImportDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getImportDeclaration()
   * @generated
   */
  int IMPORT_DECLARATION = 12;

  /**
   * The feature id for the '<em><b>Import URI</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_DECLARATION__IMPORT_URI = 0;

  /**
   * The number of structural features of the '<em>Import Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPORT_DECLARATION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.DeclarationImpl <em>Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.DeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getDeclaration()
   * @generated
   */
  int DECLARATION = 13;

  /**
   * The number of structural features of the '<em>Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ParameterizedDeclarationImpl <em>Parameterized Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ParameterizedDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getParameterizedDeclaration()
   * @generated
   */
  int PARAMETERIZED_DECLARATION = 14;

  /**
   * The number of structural features of the '<em>Parameterized Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETERIZED_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.TypeDeclarationImpl <em>Type Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.TypeDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getTypeDeclaration()
   * @generated
   */
  int TYPE_DECLARATION = 15;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DECLARATION__NAME = TYPE_DECLARATION_USE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DECLARATION__VALUE = TYPE_DECLARATION_USE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Type Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DECLARATION_FEATURE_COUNT = TYPE_DECLARATION_USE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ChannelDeclarationImpl <em>Channel Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ChannelDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getChannelDeclaration()
   * @generated
   */
  int CHANNEL_DECLARATION = 16;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHANNEL_DECLARATION__NAME = TYPE_DECLARATION_USE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHANNEL_DECLARATION__VALUE = TYPE_DECLARATION_USE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Channel Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHANNEL_DECLARATION_FEATURE_COUNT = TYPE_DECLARATION_USE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ChannelTypeImpl <em>Channel Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ChannelTypeImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getChannelType()
   * @generated
   */
  int CHANNEL_TYPE = 17;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHANNEL_TYPE__SIZE = 0;

  /**
   * The feature id for the '<em><b>In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHANNEL_TYPE__IN = 1;

  /**
   * The feature id for the '<em><b>Out</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHANNEL_TYPE__OUT = 2;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHANNEL_TYPE__TYPE = 3;

  /**
   * The number of structural features of the '<em>Channel Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHANNEL_TYPE_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.TypeImpl <em>Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.TypeImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getType()
   * @generated
   */
  int TYPE = 18;

  /**
   * The number of structural features of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.BasicTypeImpl <em>Basic Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.BasicTypeImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getBasicType()
   * @generated
   */
  int BASIC_TYPE = 19;

  /**
   * The number of structural features of the '<em>Basic Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BASIC_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RangeTypeImpl <em>Range Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.RangeTypeImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getRangeType()
   * @generated
   */
  int RANGE_TYPE = 20;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RANGE_TYPE__MINIMUM = TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RANGE_TYPE__MAXIMUM = TYPE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RANGE_TYPE__SIZE = TYPE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Range Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RANGE_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnionTypeImpl <em>Union Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnionTypeImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnionType()
   * @generated
   */
  int UNION_TYPE = 21;

  /**
   * The feature id for the '<em><b>Tags</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_TYPE__TAGS = TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Union Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnionTagsImpl <em>Union Tags</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnionTagsImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnionTags()
   * @generated
   */
  int UNION_TAGS = 22;

  /**
   * The feature id for the '<em><b>Tags</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_TAGS__TAGS = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_TAGS__TYPE = 1;

  /**
   * The number of structural features of the '<em>Union Tags</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_TAGS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnionTagDeclarationImpl <em>Union Tag Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnionTagDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnionTagDeclaration()
   * @generated
   */
  int UNION_TAG_DECLARATION = 23;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_TAG_DECLARATION__NAME = NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Union Tag Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_TAG_DECLARATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordTypeImpl <em>Record Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordTypeImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getRecordType()
   * @generated
   */
  int RECORD_TYPE = 24;

  /**
   * The feature id for the '<em><b>Fields</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_TYPE__FIELDS = TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Record Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordFieldsImpl <em>Record Fields</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordFieldsImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getRecordFields()
   * @generated
   */
  int RECORD_FIELDS = 25;

  /**
   * The feature id for the '<em><b>Fields</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_FIELDS__FIELDS = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_FIELDS__TYPE = 1;

  /**
   * The number of structural features of the '<em>Record Fields</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_FIELDS_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordFieldDeclarationImpl <em>Record Field Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordFieldDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getRecordFieldDeclaration()
   * @generated
   */
  int RECORD_FIELD_DECLARATION = 26;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_FIELD_DECLARATION__NAME = NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Record Field Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_FIELD_DECLARATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.QueueTypeImpl <em>Queue Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.QueueTypeImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getQueueType()
   * @generated
   */
  int QUEUE_TYPE = 27;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUEUE_TYPE__SIZE = TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUEUE_TYPE__TYPE = TYPE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Queue Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUEUE_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ArrayTypeImpl <em>Array Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ArrayTypeImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getArrayType()
   * @generated
   */
  int ARRAY_TYPE = 28;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_TYPE__SIZE = TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_TYPE__TYPE = TYPE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Array Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ReferencedTypeImpl <em>Referenced Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ReferencedTypeImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getReferencedType()
   * @generated
   */
  int REFERENCED_TYPE = 29;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCED_TYPE__TYPE = TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Referenced Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCED_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstantDeclarationImpl <em>Constant Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstantDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getConstantDeclaration()
   * @generated
   */
  int CONSTANT_DECLARATION = 30;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_DECLARATION__NAME = CONSTANT_DECLARATION_USE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_DECLARATION__TYPE = CONSTANT_DECLARATION_USE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_DECLARATION__VALUE = CONSTANT_DECLARATION_USE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Constant Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_DECLARATION_FEATURE_COUNT = CONSTANT_DECLARATION_USE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ProcessDeclarationImpl <em>Process Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ProcessDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getProcessDeclaration()
   * @generated
   */
  int PROCESS_DECLARATION = 31;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_DECLARATION__NAME = ROOT_DECLARATION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Generics</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_DECLARATION__GENERICS = ROOT_DECLARATION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Ports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_DECLARATION__PORTS = ROOT_DECLARATION_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_DECLARATION__PARAMETERS = ROOT_DECLARATION_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Local Ports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_DECLARATION__LOCAL_PORTS = ROOT_DECLARATION_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Priorities</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_DECLARATION__PRIORITIES = ROOT_DECLARATION_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>States</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_DECLARATION__STATES = ROOT_DECLARATION_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Variables</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_DECLARATION__VARIABLES = ROOT_DECLARATION_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Prelude</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_DECLARATION__PRELUDE = ROOT_DECLARATION_FEATURE_COUNT + 8;

  /**
   * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_DECLARATION__TRANSITIONS = ROOT_DECLARATION_FEATURE_COUNT + 9;

  /**
   * The number of structural features of the '<em>Process Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_DECLARATION_FEATURE_COUNT = ROOT_DECLARATION_FEATURE_COUNT + 10;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.GenericDeclarationImpl <em>Generic Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.GenericDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getGenericDeclaration()
   * @generated
   */
  int GENERIC_DECLARATION = 32;

  /**
   * The number of structural features of the '<em>Generic Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_DECLARATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.GenericTypeDeclarationImpl <em>Generic Type Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.GenericTypeDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getGenericTypeDeclaration()
   * @generated
   */
  int GENERIC_TYPE_DECLARATION = 33;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_TYPE_DECLARATION__NAME = TYPE_DECLARATION_USE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Generic Type Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_TYPE_DECLARATION_FEATURE_COUNT = TYPE_DECLARATION_USE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.GenericConstantDeclarationImpl <em>Generic Constant Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.GenericConstantDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getGenericConstantDeclaration()
   * @generated
   */
  int GENERIC_CONSTANT_DECLARATION = 34;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_CONSTANT_DECLARATION__NAME = TYPE_DECLARATION_USE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Generic Constant Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_CONSTANT_DECLARATION_FEATURE_COUNT = TYPE_DECLARATION_USE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.GenericUnionTagDeclarationImpl <em>Generic Union Tag Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.GenericUnionTagDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getGenericUnionTagDeclaration()
   * @generated
   */
  int GENERIC_UNION_TAG_DECLARATION = 35;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_UNION_TAG_DECLARATION__NAME = UNION_TAG_DECLARATION_USE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Generic Union Tag Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_UNION_TAG_DECLARATION_FEATURE_COUNT = UNION_TAG_DECLARATION_USE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.GenericRecordFieldDeclarationImpl <em>Generic Record Field Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.GenericRecordFieldDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getGenericRecordFieldDeclaration()
   * @generated
   */
  int GENERIC_RECORD_FIELD_DECLARATION = 36;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_RECORD_FIELD_DECLARATION__NAME = RECORD_FIELD_DECLARATION_USE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Generic Record Field Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_RECORD_FIELD_DECLARATION_FEATURE_COUNT = RECORD_FIELD_DECLARATION_USE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.StateDeclarationImpl <em>State Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.StateDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getStateDeclaration()
   * @generated
   */
  int STATE_DECLARATION = 37;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATE_DECLARATION__NAME = NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>State Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATE_DECLARATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.TransitionImpl <em>Transition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.TransitionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getTransition()
   * @generated
   */
  int TRANSITION = 38;

  /**
   * The feature id for the '<em><b>Origin</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITION__ORIGIN = 0;

  /**
   * The feature id for the '<em><b>Action</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITION__ACTION = 1;

  /**
   * The number of structural features of the '<em>Transition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRANSITION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ComponentDeclarationImpl <em>Component Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ComponentDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getComponentDeclaration()
   * @generated
   */
  int COMPONENT_DECLARATION = 39;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DECLARATION__NAME = ROOT_DECLARATION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Generics</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DECLARATION__GENERICS = ROOT_DECLARATION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Ports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DECLARATION__PORTS = ROOT_DECLARATION_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DECLARATION__PARAMETERS = ROOT_DECLARATION_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Variables</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DECLARATION__VARIABLES = ROOT_DECLARATION_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Local Ports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DECLARATION__LOCAL_PORTS = ROOT_DECLARATION_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Priorities</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DECLARATION__PRIORITIES = ROOT_DECLARATION_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Prelude</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DECLARATION__PRELUDE = ROOT_DECLARATION_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DECLARATION__BODY = ROOT_DECLARATION_FEATURE_COUNT + 8;

  /**
   * The number of structural features of the '<em>Component Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DECLARATION_FEATURE_COUNT = ROOT_DECLARATION_FEATURE_COUNT + 9;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PortsDeclarationImpl <em>Ports Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PortsDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPortsDeclaration()
   * @generated
   */
  int PORTS_DECLARATION = 40;

  /**
   * The feature id for the '<em><b>Ports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORTS_DECLARATION__PORTS = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORTS_DECLARATION__TYPE = 1;

  /**
   * The number of structural features of the '<em>Ports Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORTS_DECLARATION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PortDeclarationImpl <em>Port Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PortDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPortDeclaration()
   * @generated
   */
  int PORT_DECLARATION = 41;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_DECLARATION__NAME = NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Port Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_DECLARATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LocalPortsDeclarationImpl <em>Local Ports Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LocalPortsDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLocalPortsDeclaration()
   * @generated
   */
  int LOCAL_PORTS_DECLARATION = 42;

  /**
   * The feature id for the '<em><b>Ports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOCAL_PORTS_DECLARATION__PORTS = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOCAL_PORTS_DECLARATION__TYPE = 1;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOCAL_PORTS_DECLARATION__LEFT = 2;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOCAL_PORTS_DECLARATION__RIGHT = 3;

  /**
   * The number of structural features of the '<em>Local Ports Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOCAL_PORTS_DECLARATION_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ParametersDeclarationImpl <em>Parameters Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ParametersDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getParametersDeclaration()
   * @generated
   */
  int PARAMETERS_DECLARATION = 43;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETERS_DECLARATION__PARAMETERS = 0;

  /**
   * The feature id for the '<em><b>Read</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETERS_DECLARATION__READ = 1;

  /**
   * The feature id for the '<em><b>Write</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETERS_DECLARATION__WRITE = 2;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETERS_DECLARATION__TYPE = 3;

  /**
   * The number of structural features of the '<em>Parameters Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETERS_DECLARATION_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ParameterDeclarationImpl <em>Parameter Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ParameterDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getParameterDeclaration()
   * @generated
   */
  int PARAMETER_DECLARATION = 44;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_DECLARATION__REFERENCE = NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Array</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_DECLARATION__ARRAY = NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_DECLARATION__SIZE = NAMED_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_DECLARATION__NAME = NAMED_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Parameter Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_DECLARATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.VariablesDeclarationImpl <em>Variables Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.VariablesDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getVariablesDeclaration()
   * @generated
   */
  int VARIABLES_DECLARATION = 45;

  /**
   * The feature id for the '<em><b>Variables</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLES_DECLARATION__VARIABLES = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLES_DECLARATION__TYPE = 1;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLES_DECLARATION__VALUE = 2;

  /**
   * The number of structural features of the '<em>Variables Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLES_DECLARATION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.VariableDeclarationImpl <em>Variable Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.VariableDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getVariableDeclaration()
   * @generated
   */
  int VARIABLE_DECLARATION = 46;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_DECLARATION__NAME = NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Variable Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_DECLARATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PriorityDeclarationImpl <em>Priority Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PriorityDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPriorityDeclaration()
   * @generated
   */
  int PRIORITY_DECLARATION = 47;

  /**
   * The feature id for the '<em><b>Groups</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRIORITY_DECLARATION__GROUPS = 0;

  /**
   * The number of structural features of the '<em>Priority Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRIORITY_DECLARATION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PriorityGroupImpl <em>Priority Group</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PriorityGroupImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPriorityGroup()
   * @generated
   */
  int PRIORITY_GROUP = 48;

  /**
   * The feature id for the '<em><b>Ports</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRIORITY_GROUP__PORTS = 0;

  /**
   * The number of structural features of the '<em>Priority Group</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRIORITY_GROUP_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.StatementImpl <em>Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.StatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getStatement()
   * @generated
   */
  int STATEMENT = 49;

  /**
   * The number of structural features of the '<em>Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.NullStatementImpl <em>Null Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.NullStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getNullStatement()
   * @generated
   */
  int NULL_STATEMENT = 50;

  /**
   * The number of structural features of the '<em>Null Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NULL_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.TaggedStatementImpl <em>Tagged Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.TaggedStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getTaggedStatement()
   * @generated
   */
  int TAGGED_STATEMENT = 51;

  /**
   * The feature id for the '<em><b>Tag</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TAGGED_STATEMENT__TAG = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Tagged Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TAGGED_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.TagDeclarationImpl <em>Tag Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.TagDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getTagDeclaration()
   * @generated
   */
  int TAG_DECLARATION = 52;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TAG_DECLARATION__NAME = NAMED_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Tag Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TAG_DECLARATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PatternStatementImpl <em>Pattern Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PatternStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPatternStatement()
   * @generated
   */
  int PATTERN_STATEMENT = 53;

  /**
   * The number of structural features of the '<em>Pattern Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATTERN_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PatternImpl <em>Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPattern()
   * @generated
   */
  int PATTERN = 54;

  /**
   * The number of structural features of the '<em>Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATTERN_FEATURE_COUNT = PATTERN_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.AnyPatternImpl <em>Any Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.AnyPatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getAnyPattern()
   * @generated
   */
  int ANY_PATTERN = 55;

  /**
   * The number of structural features of the '<em>Any Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANY_PATTERN_FEATURE_COUNT = PATTERN_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstantPatternImpl <em>Constant Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstantPatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getConstantPattern()
   * @generated
   */
  int CONSTANT_PATTERN = 56;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_PATTERN__VALUE = PATTERN_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Constant Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_PATTERN_FEATURE_COUNT = PATTERN_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.IntegerPatternImpl <em>Integer Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.IntegerPatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getIntegerPattern()
   * @generated
   */
  int INTEGER_PATTERN = 57;

  /**
   * The feature id for the '<em><b>Negative</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_PATTERN__NEGATIVE = PATTERN_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_PATTERN__VALUE = PATTERN_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Integer Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_PATTERN_FEATURE_COUNT = PATTERN_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.IdentifierPatternImpl <em>Identifier Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.IdentifierPatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getIdentifierPattern()
   * @generated
   */
  int IDENTIFIER_PATTERN = 58;

  /**
   * The feature id for the '<em><b>Declaration</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_PATTERN__DECLARATION = PATTERN_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Identifier Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_PATTERN_FEATURE_COUNT = PATTERN_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConditionalStatementImpl <em>Conditional Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ConditionalStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getConditionalStatement()
   * @generated
   */
  int CONDITIONAL_STATEMENT = 59;

  /**
   * The feature id for the '<em><b>Conditions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_STATEMENT__CONDITIONS = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Then</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_STATEMENT__THEN = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Elseif</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_STATEMENT__ELSEIF = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Else</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_STATEMENT__ELSE = STATEMENT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Conditional Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ExtendedConditionalStatementImpl <em>Extended Conditional Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ExtendedConditionalStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getExtendedConditionalStatement()
   * @generated
   */
  int EXTENDED_CONDITIONAL_STATEMENT = 60;

  /**
   * The feature id for the '<em><b>Condition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENDED_CONDITIONAL_STATEMENT__CONDITION = 0;

  /**
   * The feature id for the '<em><b>Then</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENDED_CONDITIONAL_STATEMENT__THEN = 1;

  /**
   * The number of structural features of the '<em>Extended Conditional Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENDED_CONDITIONAL_STATEMENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.SelectStatementImpl <em>Select Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.SelectStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getSelectStatement()
   * @generated
   */
  int SELECT_STATEMENT = 61;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_STATEMENT__BODY = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_STATEMENT__INDEX = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_STATEMENT__TYPE = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Select Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.WhileStatementImpl <em>While Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.WhileStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getWhileStatement()
   * @generated
   */
  int WHILE_STATEMENT = 62;

  /**
   * The feature id for the '<em><b>Condition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_STATEMENT__CONDITION = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_STATEMENT__BODY = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>While Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ForeachStatementImpl <em>Foreach Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ForeachStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getForeachStatement()
   * @generated
   */
  int FOREACH_STATEMENT = 63;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOREACH_STATEMENT__VARIABLE = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOREACH_STATEMENT__BODY = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Foreach Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FOREACH_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ToStatementImpl <em>To Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ToStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getToStatement()
   * @generated
   */
  int TO_STATEMENT = 64;

  /**
   * The feature id for the '<em><b>State</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TO_STATEMENT__STATE = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>To Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TO_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.CaseStatementImpl <em>Case Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.CaseStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getCaseStatement()
   * @generated
   */
  int CASE_STATEMENT = 65;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_STATEMENT__VALUE = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Pattern</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_STATEMENT__PATTERN = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_STATEMENT__BODY = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Case Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LoopStatementImpl <em>Loop Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LoopStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLoopStatement()
   * @generated
   */
  int LOOP_STATEMENT = 66;

  /**
   * The number of structural features of the '<em>Loop Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOOP_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.OnStatementImpl <em>On Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.OnStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getOnStatement()
   * @generated
   */
  int ON_STATEMENT = 67;

  /**
   * The feature id for the '<em><b>Condition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ON_STATEMENT__CONDITION = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>On Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ON_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.WaitStatementImpl <em>Wait Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.WaitStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getWaitStatement()
   * @generated
   */
  int WAIT_STATEMENT = 68;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WAIT_STATEMENT__LEFT = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WAIT_STATEMENT__RIGHT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Wait Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WAIT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.CompositionImpl <em>Composition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.CompositionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getComposition()
   * @generated
   */
  int COMPOSITION = 69;

  /**
   * The feature id for the '<em><b>Global</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITION__GLOBAL = 0;

  /**
   * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITION__BLOCKS = 1;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITION__INDEX = 2;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITION__TYPE = 3;

  /**
   * The number of structural features of the '<em>Composition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITION_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.BlockImpl <em>Block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.BlockImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getBlock()
   * @generated
   */
  int BLOCK = 70;

  /**
   * The number of structural features of the '<em>Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BLOCK_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.CompositeBlockImpl <em>Composite Block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.CompositeBlockImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getCompositeBlock()
   * @generated
   */
  int COMPOSITE_BLOCK = 71;

  /**
   * The feature id for the '<em><b>Local</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_BLOCK__LOCAL = BLOCK_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Composition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_BLOCK__COMPOSITION = BLOCK_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Composite Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITE_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.InstanceDeclarationImpl <em>Instance Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.InstanceDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getInstanceDeclaration()
   * @generated
   */
  int INSTANCE_DECLARATION = 72;

  /**
   * The feature id for the '<em><b>Local</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE_DECLARATION__LOCAL = PATH_DECLARATION_USE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE_DECLARATION__NAME = PATH_DECLARATION_USE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Instance</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE_DECLARATION__INSTANCE = PATH_DECLARATION_USE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Instance Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE_DECLARATION_FEATURE_COUNT = PATH_DECLARATION_USE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PortSetImpl <em>Port Set</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PortSetImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPortSet()
   * @generated
   */
  int PORT_SET = 73;

  /**
   * The feature id for the '<em><b>All Ports</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_SET__ALL_PORTS = BLOCK_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Ports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_SET__PORTS = BLOCK_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Port Set</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_SET_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ComponentInstanceImpl <em>Component Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ComponentInstanceImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getComponentInstance()
   * @generated
   */
  int COMPONENT_INSTANCE = 74;

  /**
   * The feature id for the '<em><b>Component</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_INSTANCE__COMPONENT = 0;

  /**
   * The feature id for the '<em><b>Generics</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_INSTANCE__GENERICS = 1;

  /**
   * The feature id for the '<em><b>Ports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_INSTANCE__PORTS = 2;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_INSTANCE__PARAMETERS = 3;

  /**
   * The number of structural features of the '<em>Component Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_INSTANCE_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.GenericInstanceImpl <em>Generic Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.GenericInstanceImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getGenericInstance()
   * @generated
   */
  int GENERIC_INSTANCE = 75;

  /**
   * The number of structural features of the '<em>Generic Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GENERIC_INSTANCE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.TypeInstanceImpl <em>Type Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.TypeInstanceImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getTypeInstance()
   * @generated
   */
  int TYPE_INSTANCE = 76;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_INSTANCE__TYPE = GENERIC_INSTANCE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Type Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_INSTANCE_FEATURE_COUNT = GENERIC_INSTANCE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstantInstanceImpl <em>Constant Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstantInstanceImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getConstantInstance()
   * @generated
   */
  int CONSTANT_INSTANCE = 77;

  /**
   * The feature id for the '<em><b>Const</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_INSTANCE__CONST = GENERIC_INSTANCE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_INSTANCE__VALUE = GENERIC_INSTANCE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Constant Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_INSTANCE_FEATURE_COUNT = GENERIC_INSTANCE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnionTagInstanceImpl <em>Union Tag Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnionTagInstanceImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnionTagInstance()
   * @generated
   */
  int UNION_TAG_INSTANCE = 78;

  /**
   * The feature id for the '<em><b>Constr0</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_TAG_INSTANCE__CONSTR0 = GENERIC_INSTANCE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Constr1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_TAG_INSTANCE__CONSTR1 = GENERIC_INSTANCE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Head</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_TAG_INSTANCE__HEAD = GENERIC_INSTANCE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Union Tag Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNION_TAG_INSTANCE_FEATURE_COUNT = GENERIC_INSTANCE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordFieldInstanceImpl <em>Record Field Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordFieldInstanceImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getRecordFieldInstance()
   * @generated
   */
  int RECORD_FIELD_INSTANCE = 79;

  /**
   * The feature id for the '<em><b>Field</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_FIELD_INSTANCE__FIELD = GENERIC_INSTANCE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Record Field Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_FIELD_INSTANCE_FEATURE_COUNT = GENERIC_INSTANCE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ExpressionImpl <em>Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getExpression()
   * @generated
   */
  int EXPRESSION = 80;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION__MINIMUM = RANGE_TYPE__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION__MAXIMUM = RANGE_TYPE__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION__SIZE = RANGE_TYPE__SIZE;

  /**
   * The number of structural features of the '<em>Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FEATURE_COUNT = RANGE_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ReferenceExpressionImpl <em>Reference Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ReferenceExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getReferenceExpression()
   * @generated
   */
  int REFERENCE_EXPRESSION = 81;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Declaration</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_EXPRESSION__DECLARATION = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Reference Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.IdentifierExpressionImpl <em>Identifier Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.IdentifierExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getIdentifierExpression()
   * @generated
   */
  int IDENTIFIER_EXPRESSION = 82;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Declaration</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_EXPRESSION__DECLARATION = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Identifier Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIER_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordExpressionImpl <em>Record Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getRecordExpression()
   * @generated
   */
  int RECORD_EXPRESSION = 83;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Fields</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_EXPRESSION__FIELDS = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Record Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.FieldExpressionImpl <em>Field Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FieldExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getFieldExpression()
   * @generated
   */
  int FIELD_EXPRESSION = 84;

  /**
   * The feature id for the '<em><b>Field</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_EXPRESSION__FIELD = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_EXPRESSION__VALUE = 1;

  /**
   * The number of structural features of the '<em>Field Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_EXPRESSION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.QueueExpressionImpl <em>Queue Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.QueueExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getQueueExpression()
   * @generated
   */
  int QUEUE_EXPRESSION = 85;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUEUE_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUEUE_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUEUE_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUEUE_EXPRESSION__VALUES = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Queue Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUEUE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.EnqueueExpressionImpl <em>Enqueue Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.EnqueueExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getEnqueueExpression()
   * @generated
   */
  int ENQUEUE_EXPRESSION = 86;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENQUEUE_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENQUEUE_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENQUEUE_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Element</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENQUEUE_EXPRESSION__ELEMENT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Queue</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENQUEUE_EXPRESSION__QUEUE = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Enqueue Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENQUEUE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.AppendExpressionImpl <em>Append Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.AppendExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getAppendExpression()
   * @generated
   */
  int APPEND_EXPRESSION = 87;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int APPEND_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int APPEND_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int APPEND_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int APPEND_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int APPEND_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Append Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int APPEND_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LiteralExpressionImpl <em>Literal Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LiteralExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLiteralExpression()
   * @generated
   */
  int LITERAL_EXPRESSION = 88;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LITERAL_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LITERAL_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LITERAL_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The number of structural features of the '<em>Literal Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LITERAL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.BooleanLiteralImpl <em>Boolean Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.BooleanLiteralImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getBooleanLiteral()
   * @generated
   */
  int BOOLEAN_LITERAL = 89;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL__MINIMUM = LITERAL_EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL__MAXIMUM = LITERAL_EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL__SIZE = LITERAL_EXPRESSION__SIZE;

  /**
   * The number of structural features of the '<em>Boolean Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_LITERAL_FEATURE_COUNT = LITERAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.NaturalLiteralImpl <em>Natural Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.NaturalLiteralImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getNaturalLiteral()
   * @generated
   */
  int NATURAL_LITERAL = 90;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_LITERAL__MINIMUM = LITERAL_EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_LITERAL__MAXIMUM = LITERAL_EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_LITERAL__SIZE = LITERAL_EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_LITERAL__VALUE = LITERAL_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Natural Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_LITERAL_FEATURE_COUNT = LITERAL_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LowerBoundImpl <em>Lower Bound</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LowerBoundImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLowerBound()
   * @generated
   */
  int LOWER_BOUND = 91;

  /**
   * The feature id for the '<em><b>Left</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOWER_BOUND__LEFT = 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOWER_BOUND__RIGHT = 1;

  /**
   * The number of structural features of the '<em>Lower Bound</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LOWER_BOUND_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UpperBoundImpl <em>Upper Bound</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UpperBoundImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUpperBound()
   * @generated
   */
  int UPPER_BOUND = 92;

  /**
   * The number of structural features of the '<em>Upper Bound</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UPPER_BOUND_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.NaturalLowerBoundImpl <em>Natural Lower Bound</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.NaturalLowerBoundImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getNaturalLowerBound()
   * @generated
   */
  int NATURAL_LOWER_BOUND = 93;

  /**
   * The feature id for the '<em><b>Left</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_LOWER_BOUND__LEFT = LOWER_BOUND__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_LOWER_BOUND__RIGHT = LOWER_BOUND__RIGHT;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_LOWER_BOUND__VALUE = LOWER_BOUND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Natural Lower Bound</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_LOWER_BOUND_FEATURE_COUNT = LOWER_BOUND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.NaturalUpperBoundImpl <em>Natural Upper Bound</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.NaturalUpperBoundImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getNaturalUpperBound()
   * @generated
   */
  int NATURAL_UPPER_BOUND = 94;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_UPPER_BOUND__VALUE = UPPER_BOUND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Left</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_UPPER_BOUND__LEFT = UPPER_BOUND_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_UPPER_BOUND__RIGHT = UPPER_BOUND_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Natural Upper Bound</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_UPPER_BOUND_FEATURE_COUNT = UPPER_BOUND_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.DecimalLowerBoundImpl <em>Decimal Lower Bound</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.DecimalLowerBoundImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getDecimalLowerBound()
   * @generated
   */
  int DECIMAL_LOWER_BOUND = 95;

  /**
   * The feature id for the '<em><b>Left</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_LOWER_BOUND__LEFT = LOWER_BOUND__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_LOWER_BOUND__RIGHT = LOWER_BOUND__RIGHT;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_LOWER_BOUND__VALUE = LOWER_BOUND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Decimal Lower Bound</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_LOWER_BOUND_FEATURE_COUNT = LOWER_BOUND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.DecimalUpperBoundImpl <em>Decimal Upper Bound</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.DecimalUpperBoundImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getDecimalUpperBound()
   * @generated
   */
  int DECIMAL_UPPER_BOUND = 96;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_UPPER_BOUND__VALUE = UPPER_BOUND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Left</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_UPPER_BOUND__LEFT = UPPER_BOUND_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_UPPER_BOUND__RIGHT = UPPER_BOUND_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Decimal Upper Bound</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECIMAL_UPPER_BOUND_FEATURE_COUNT = UPPER_BOUND_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.VariableLowerBoundImpl <em>Variable Lower Bound</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.VariableLowerBoundImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getVariableLowerBound()
   * @generated
   */
  int VARIABLE_LOWER_BOUND = 97;

  /**
   * The feature id for the '<em><b>Left</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_LOWER_BOUND__LEFT = LOWER_BOUND__LEFT;

  /**
   * The feature id for the '<em><b>Right</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_LOWER_BOUND__RIGHT = LOWER_BOUND__RIGHT;

  /**
   * The feature id for the '<em><b>Variable</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_LOWER_BOUND__VARIABLE = LOWER_BOUND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Variable Lower Bound</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_LOWER_BOUND_FEATURE_COUNT = LOWER_BOUND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.VariableUpperBoundImpl <em>Variable Upper Bound</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.VariableUpperBoundImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getVariableUpperBound()
   * @generated
   */
  int VARIABLE_UPPER_BOUND = 98;

  /**
   * The feature id for the '<em><b>Variable</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_UPPER_BOUND__VARIABLE = UPPER_BOUND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Left</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_UPPER_BOUND__LEFT = UPPER_BOUND_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_UPPER_BOUND__RIGHT = UPPER_BOUND_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Variable Upper Bound</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_UPPER_BOUND_FEATURE_COUNT = UPPER_BOUND_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.InfiniteUpperBoundImpl <em>Infinite Upper Bound</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.InfiniteUpperBoundImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getInfiniteUpperBound()
   * @generated
   */
  int INFINITE_UPPER_BOUND = 99;

  /**
   * The number of structural features of the '<em>Infinite Upper Bound</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INFINITE_UPPER_BOUND_FEATURE_COUNT = UPPER_BOUND_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RequirementImpl <em>Requirement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.RequirementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getRequirement()
   * @generated
   */
  int REQUIREMENT = 100;

  /**
   * The feature id for the '<em><b>Property</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REQUIREMENT__PROPERTY = 0;

  /**
   * The feature id for the '<em><b>Positive</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REQUIREMENT__POSITIVE = 1;

  /**
   * The feature id for the '<em><b>Negative</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REQUIREMENT__NEGATIVE = 2;

  /**
   * The number of structural features of the '<em>Requirement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REQUIREMENT_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PropertyDeclarationImpl <em>Property Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PropertyDeclarationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPropertyDeclaration()
   * @generated
   */
  int PROPERTY_DECLARATION = 101;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_DECLARATION__NAME = DECLARATION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Property</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_DECLARATION__PROPERTY = DECLARATION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Property Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PropertyImpl <em>Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PropertyImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getProperty()
   * @generated
   */
  int PROPERTY = 102;

  /**
   * The number of structural features of the '<em>Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PatternPropertyImpl <em>Pattern Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PatternPropertyImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPatternProperty()
   * @generated
   */
  int PATTERN_PROPERTY = 103;

  /**
   * The number of structural features of the '<em>Pattern Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATTERN_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLPatternImpl <em>LTL Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLPatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLPattern()
   * @generated
   */
  int LTL_PATTERN = 104;

  /**
   * The feature id for the '<em><b>Property</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_PATTERN__PROPERTY = PATTERN_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>LTL Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_PATTERN_FEATURE_COUNT = PATTERN_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.DeadlockFreePatternImpl <em>Deadlock Free Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.DeadlockFreePatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getDeadlockFreePattern()
   * @generated
   */
  int DEADLOCK_FREE_PATTERN = 105;

  /**
   * The number of structural features of the '<em>Deadlock Free Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEADLOCK_FREE_PATTERN_FEATURE_COUNT = PATTERN_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.InfinitelyOftenPatternImpl <em>Infinitely Often Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.InfinitelyOftenPatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getInfinitelyOftenPattern()
   * @generated
   */
  int INFINITELY_OFTEN_PATTERN = 106;

  /**
   * The feature id for the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INFINITELY_OFTEN_PATTERN__SUBJECT = PATTERN_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Infinitely Often Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INFINITELY_OFTEN_PATTERN_FEATURE_COUNT = PATTERN_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.MortalPatternImpl <em>Mortal Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.MortalPatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getMortalPattern()
   * @generated
   */
  int MORTAL_PATTERN = 107;

  /**
   * The feature id for the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MORTAL_PATTERN__SUBJECT = PATTERN_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Mortal Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MORTAL_PATTERN_FEATURE_COUNT = PATTERN_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PresencePatternImpl <em>Presence Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PresencePatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPresencePattern()
   * @generated
   */
  int PRESENCE_PATTERN = 108;

  /**
   * The feature id for the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRESENCE_PATTERN__SUBJECT = PATTERN_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Lasting</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRESENCE_PATTERN__LASTING = PATTERN_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>After</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRESENCE_PATTERN__AFTER = PATTERN_PROPERTY_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Lower</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRESENCE_PATTERN__LOWER = PATTERN_PROPERTY_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Upper</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRESENCE_PATTERN__UPPER = PATTERN_PROPERTY_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Until</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRESENCE_PATTERN__UNTIL = PATTERN_PROPERTY_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Before</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRESENCE_PATTERN__BEFORE = PATTERN_PROPERTY_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Min</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRESENCE_PATTERN__MIN = PATTERN_PROPERTY_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Max</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRESENCE_PATTERN__MAX = PATTERN_PROPERTY_FEATURE_COUNT + 8;

  /**
   * The number of structural features of the '<em>Presence Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRESENCE_PATTERN_FEATURE_COUNT = PATTERN_PROPERTY_FEATURE_COUNT + 9;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.AbsencePatternImpl <em>Absence Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.AbsencePatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getAbsencePattern()
   * @generated
   */
  int ABSENCE_PATTERN = 109;

  /**
   * The feature id for the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSENCE_PATTERN__SUBJECT = PATTERN_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>After</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSENCE_PATTERN__AFTER = PATTERN_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Lower</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSENCE_PATTERN__LOWER = PATTERN_PROPERTY_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Upper</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSENCE_PATTERN__UPPER = PATTERN_PROPERTY_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Until</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSENCE_PATTERN__UNTIL = PATTERN_PROPERTY_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Before</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSENCE_PATTERN__BEFORE = PATTERN_PROPERTY_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Min</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSENCE_PATTERN__MIN = PATTERN_PROPERTY_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Max</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSENCE_PATTERN__MAX = PATTERN_PROPERTY_FEATURE_COUNT + 7;

  /**
   * The number of structural features of the '<em>Absence Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSENCE_PATTERN_FEATURE_COUNT = PATTERN_PROPERTY_FEATURE_COUNT + 8;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.AlwaysPatternImpl <em>Always Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.AlwaysPatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getAlwaysPattern()
   * @generated
   */
  int ALWAYS_PATTERN = 110;

  /**
   * The feature id for the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALWAYS_PATTERN__SUBJECT = PATTERN_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Before</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALWAYS_PATTERN__BEFORE = PATTERN_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>After</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALWAYS_PATTERN__AFTER = PATTERN_PROPERTY_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Until</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALWAYS_PATTERN__UNTIL = PATTERN_PROPERTY_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Min</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALWAYS_PATTERN__MIN = PATTERN_PROPERTY_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Max</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALWAYS_PATTERN__MAX = PATTERN_PROPERTY_FEATURE_COUNT + 5;

  /**
   * The number of structural features of the '<em>Always Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALWAYS_PATTERN_FEATURE_COUNT = PATTERN_PROPERTY_FEATURE_COUNT + 6;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.SequencePatternImpl <em>Sequence Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.SequencePatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getSequencePattern()
   * @generated
   */
  int SEQUENCE_PATTERN = 111;

  /**
   * The number of structural features of the '<em>Sequence Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCE_PATTERN_FEATURE_COUNT = PATTERN_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLPropertyImpl <em>LTL Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLPropertyImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLProperty()
   * @generated
   */
  int LTL_PROPERTY = 112;

  /**
   * The number of structural features of the '<em>LTL Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_PROPERTY_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ObservableEventImpl <em>Observable Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ObservableEventImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getObservableEvent()
   * @generated
   */
  int OBSERVABLE_EVENT = 117;

  /**
   * The number of structural features of the '<em>Observable Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSERVABLE_EVENT_FEATURE_COUNT = LTL_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.StateEventImpl <em>State Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.StateEventImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getStateEvent()
   * @generated
   */
  int STATE_EVENT = 113;

  /**
   * The feature id for the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATE_EVENT__SUBJECT = OBSERVABLE_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>State Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATE_EVENT_FEATURE_COUNT = OBSERVABLE_EVENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.EnterStateEventImpl <em>Enter State Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.EnterStateEventImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getEnterStateEvent()
   * @generated
   */
  int ENTER_STATE_EVENT = 114;

  /**
   * The feature id for the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENTER_STATE_EVENT__SUBJECT = STATE_EVENT__SUBJECT;

  /**
   * The number of structural features of the '<em>Enter State Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENTER_STATE_EVENT_FEATURE_COUNT = STATE_EVENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LeaveStateEventImpl <em>Leave State Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LeaveStateEventImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLeaveStateEvent()
   * @generated
   */
  int LEAVE_STATE_EVENT = 115;

  /**
   * The feature id for the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEAVE_STATE_EVENT__SUBJECT = STATE_EVENT__SUBJECT;

  /**
   * The number of structural features of the '<em>Leave State Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEAVE_STATE_EVENT_FEATURE_COUNT = STATE_EVENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ObservableImpl <em>Observable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ObservableImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getObservable()
   * @generated
   */
  int OBSERVABLE = 116;

  /**
   * The number of structural features of the '<em>Observable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSERVABLE_FEATURE_COUNT = SEQUENCE_PATTERN_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PathEventImpl <em>Path Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PathEventImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPathEvent()
   * @generated
   */
  int PATH_EVENT = 118;

  /**
   * The feature id for the '<em><b>Path</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATH_EVENT__PATH = OBSERVABLE_EVENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Item</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATH_EVENT__ITEM = OBSERVABLE_EVENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Path Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATH_EVENT_FEATURE_COUNT = OBSERVABLE_EVENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PathImpl <em>Path</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PathImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPath()
   * @generated
   */
  int PATH = 119;

  /**
   * The feature id for the '<em><b>Items</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATH__ITEMS = 0;

  /**
   * The number of structural features of the '<em>Path</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATH_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PathItemImpl <em>Path Item</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PathItemImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPathItem()
   * @generated
   */
  int PATH_ITEM = 120;

  /**
   * The number of structural features of the '<em>Path Item</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATH_ITEM_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.NaturalItemImpl <em>Natural Item</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.NaturalItemImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getNaturalItem()
   * @generated
   */
  int NATURAL_ITEM = 121;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_ITEM__VALUE = PATH_ITEM_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Natural Item</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_ITEM_FEATURE_COUNT = PATH_ITEM_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.NamedItemImpl <em>Named Item</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.NamedItemImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getNamedItem()
   * @generated
   */
  int NAMED_ITEM = 122;

  /**
   * The feature id for the '<em><b>Declaration</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMED_ITEM__DECLARATION = PATH_ITEM_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Named Item</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMED_ITEM_FEATURE_COUNT = PATH_ITEM_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.SubjectImpl <em>Subject</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.SubjectImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getSubject()
   * @generated
   */
  int SUBJECT = 123;

  /**
   * The number of structural features of the '<em>Subject</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBJECT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.StateSubjectImpl <em>State Subject</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.StateSubjectImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getStateSubject()
   * @generated
   */
  int STATE_SUBJECT = 124;

  /**
   * The feature id for the '<em><b>State</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATE_SUBJECT__STATE = SUBJECT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>State Subject</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATE_SUBJECT_FEATURE_COUNT = SUBJECT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ValueSubjectImpl <em>Value Subject</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ValueSubjectImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getValueSubject()
   * @generated
   */
  int VALUE_SUBJECT = 125;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_SUBJECT__VALUE = SUBJECT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Value Subject</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_SUBJECT_FEATURE_COUNT = SUBJECT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.TagSubjectImpl <em>Tag Subject</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.TagSubjectImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getTagSubject()
   * @generated
   */
  int TAG_SUBJECT = 126;

  /**
   * The feature id for the '<em><b>Tag</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TAG_SUBJECT__TAG = SUBJECT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Tag Subject</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TAG_SUBJECT_FEATURE_COUNT = SUBJECT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.EventSubjectImpl <em>Event Subject</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.EventSubjectImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getEventSubject()
   * @generated
   */
  int EVENT_SUBJECT = 127;

  /**
   * The feature id for the '<em><b>Event</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_SUBJECT__EVENT = SUBJECT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Event Subject</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_SUBJECT_FEATURE_COUNT = SUBJECT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.TupleTypeImpl <em>Tuple Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.TupleTypeImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getTupleType()
   * @generated
   */
  int TUPLE_TYPE = 128;

  /**
   * The feature id for the '<em><b>Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TUPLE_TYPE__TYPES = TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Tuple Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TUPLE_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.NaturalTypeImpl <em>Natural Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.NaturalTypeImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getNaturalType()
   * @generated
   */
  int NATURAL_TYPE = 129;

  /**
   * The number of structural features of the '<em>Natural Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATURAL_TYPE_FEATURE_COUNT = BASIC_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.IntegerTypeImpl <em>Integer Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.IntegerTypeImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getIntegerType()
   * @generated
   */
  int INTEGER_TYPE = 130;

  /**
   * The number of structural features of the '<em>Integer Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TYPE_FEATURE_COUNT = BASIC_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.BooleanTypeImpl <em>Boolean Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.BooleanTypeImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getBooleanType()
   * @generated
   */
  int BOOLEAN_TYPE = 131;

  /**
   * The number of structural features of the '<em>Boolean Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_TYPE_FEATURE_COUNT = BASIC_TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnlessStatementImpl <em>Unless Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnlessStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnlessStatement()
   * @generated
   */
  int UNLESS_STATEMENT = 132;

  /**
   * The feature id for the '<em><b>Followers</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNLESS_STATEMENT__FOLLOWERS = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Unless Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNLESS_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.StatementChoiceImpl <em>Statement Choice</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.StatementChoiceImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getStatementChoice()
   * @generated
   */
  int STATEMENT_CHOICE = 133;

  /**
   * The feature id for the '<em><b>Choices</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_CHOICE__CHOICES = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Statement Choice</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_CHOICE_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.StatementSequenceImpl <em>Statement Sequence</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.StatementSequenceImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getStatementSequence()
   * @generated
   */
  int STATEMENT_SEQUENCE = 134;

  /**
   * The feature id for the '<em><b>Statements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_SEQUENCE__STATEMENTS = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Statement Sequence</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_SEQUENCE_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.AssignStatementImpl <em>Assign Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.AssignStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getAssignStatement()
   * @generated
   */
  int ASSIGN_STATEMENT = 135;

  /**
   * The feature id for the '<em><b>Patterns</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGN_STATEMENT__PATTERNS = PATTERN_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGN_STATEMENT__VALUES = PATTERN_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Assign Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGN_STATEMENT_FEATURE_COUNT = PATTERN_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.SendStatementImpl <em>Send Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.SendStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getSendStatement()
   * @generated
   */
  int SEND_STATEMENT = 136;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_STATEMENT__PORT = PATTERN_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_STATEMENT__VALUES = PATTERN_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Send Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEND_STATEMENT_FEATURE_COUNT = PATTERN_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ReceiveStatementImpl <em>Receive Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ReceiveStatementImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getReceiveStatement()
   * @generated
   */
  int RECEIVE_STATEMENT = 137;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECEIVE_STATEMENT__PORT = PATTERN_STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Patterns</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECEIVE_STATEMENT__PATTERNS = PATTERN_STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECEIVE_STATEMENT__EXP = PATTERN_STATEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Receive Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECEIVE_STATEMENT_FEATURE_COUNT = PATTERN_STATEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstructorPatternImpl <em>Constructor Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstructorPatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getConstructorPattern()
   * @generated
   */
  int CONSTRUCTOR_PATTERN = 138;

  /**
   * The feature id for the '<em><b>Declaration</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTRUCTOR_PATTERN__DECLARATION = IDENTIFIER_PATTERN__DECLARATION;

  /**
   * The feature id for the '<em><b>Parameter</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTRUCTOR_PATTERN__PARAMETER = IDENTIFIER_PATTERN_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Constructor Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTRUCTOR_PATTERN_FEATURE_COUNT = IDENTIFIER_PATTERN_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ArrayAccessPatternImpl <em>Array Access Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ArrayAccessPatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getArrayAccessPattern()
   * @generated
   */
  int ARRAY_ACCESS_PATTERN = 139;

  /**
   * The feature id for the '<em><b>Declaration</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ACCESS_PATTERN__DECLARATION = IDENTIFIER_PATTERN__DECLARATION;

  /**
   * The feature id for the '<em><b>Source</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ACCESS_PATTERN__SOURCE = IDENTIFIER_PATTERN_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ACCESS_PATTERN__INDEX = IDENTIFIER_PATTERN_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Array Access Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ACCESS_PATTERN_FEATURE_COUNT = IDENTIFIER_PATTERN_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordAccessPatternImpl <em>Record Access Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordAccessPatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getRecordAccessPattern()
   * @generated
   */
  int RECORD_ACCESS_PATTERN = 140;

  /**
   * The feature id for the '<em><b>Declaration</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_ACCESS_PATTERN__DECLARATION = IDENTIFIER_PATTERN__DECLARATION;

  /**
   * The feature id for the '<em><b>Source</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_ACCESS_PATTERN__SOURCE = IDENTIFIER_PATTERN_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Field</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_ACCESS_PATTERN__FIELD = IDENTIFIER_PATTERN_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Record Access Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_ACCESS_PATTERN_FEATURE_COUNT = IDENTIFIER_PATTERN_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.AllExpressionImpl <em>All Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.AllExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getAllExpression()
   * @generated
   */
  int ALL_EXPRESSION = 141;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_EXPRESSION__INDEX = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_EXPRESSION__TYPE = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_EXPRESSION__CHILD = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>All Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ExistsExpressionImpl <em>Exists Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ExistsExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getExistsExpression()
   * @generated
   */
  int EXISTS_EXPRESSION = 142;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXISTS_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXISTS_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXISTS_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXISTS_EXPRESSION__INDEX = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXISTS_EXPRESSION__TYPE = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXISTS_EXPRESSION__CHILD = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Exists Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXISTS_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConditionalImpl <em>Conditional</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ConditionalImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getConditional()
   * @generated
   */
  int CONDITIONAL = 143;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Condition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL__CONDITION = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Then</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL__THEN = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Else</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL__ELSE = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Conditional</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.DisjunctionImpl <em>Disjunction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.DisjunctionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getDisjunction()
   * @generated
   */
  int DISJUNCTION = 144;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISJUNCTION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISJUNCTION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISJUNCTION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISJUNCTION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISJUNCTION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Disjunction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DISJUNCTION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ImplicationImpl <em>Implication</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ImplicationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getImplication()
   * @generated
   */
  int IMPLICATION = 145;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLICATION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLICATION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLICATION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLICATION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLICATION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Implication</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLICATION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConjunctionImpl <em>Conjunction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ConjunctionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getConjunction()
   * @generated
   */
  int CONJUNCTION = 146;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONJUNCTION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONJUNCTION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONJUNCTION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONJUNCTION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONJUNCTION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Conjunction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONJUNCTION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ComparisonEqualImpl <em>Comparison Equal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ComparisonEqualImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getComparisonEqual()
   * @generated
   */
  int COMPARISON_EQUAL = 147;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_EQUAL__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_EQUAL__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_EQUAL__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_EQUAL__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_EQUAL__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Comparison Equal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_EQUAL_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ComparisonNotEqualImpl <em>Comparison Not Equal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ComparisonNotEqualImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getComparisonNotEqual()
   * @generated
   */
  int COMPARISON_NOT_EQUAL = 148;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_NOT_EQUAL__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_NOT_EQUAL__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_NOT_EQUAL__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_NOT_EQUAL__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_NOT_EQUAL__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Comparison Not Equal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_NOT_EQUAL_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ComparisonLesserImpl <em>Comparison Lesser</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ComparisonLesserImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getComparisonLesser()
   * @generated
   */
  int COMPARISON_LESSER = 149;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_LESSER__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_LESSER__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_LESSER__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_LESSER__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_LESSER__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Comparison Lesser</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_LESSER_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ComparisonLesserEqualImpl <em>Comparison Lesser Equal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ComparisonLesserEqualImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getComparisonLesserEqual()
   * @generated
   */
  int COMPARISON_LESSER_EQUAL = 150;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_LESSER_EQUAL__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_LESSER_EQUAL__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_LESSER_EQUAL__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_LESSER_EQUAL__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_LESSER_EQUAL__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Comparison Lesser Equal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_LESSER_EQUAL_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ComparisonGreaterImpl <em>Comparison Greater</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ComparisonGreaterImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getComparisonGreater()
   * @generated
   */
  int COMPARISON_GREATER = 151;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_GREATER__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_GREATER__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_GREATER__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_GREATER__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_GREATER__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Comparison Greater</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_GREATER_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ComparisonGreaterEqualImpl <em>Comparison Greater Equal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ComparisonGreaterEqualImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getComparisonGreaterEqual()
   * @generated
   */
  int COMPARISON_GREATER_EQUAL = 152;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_GREATER_EQUAL__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_GREATER_EQUAL__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_GREATER_EQUAL__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_GREATER_EQUAL__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_GREATER_EQUAL__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Comparison Greater Equal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARISON_GREATER_EQUAL_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.AdditionImpl <em>Addition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.AdditionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getAddition()
   * @generated
   */
  int ADDITION = 153;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Addition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.SubstractionImpl <em>Substraction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.SubstractionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getSubstraction()
   * @generated
   */
  int SUBSTRACTION = 154;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBSTRACTION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBSTRACTION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBSTRACTION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBSTRACTION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBSTRACTION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Substraction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBSTRACTION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.MultiplicationImpl <em>Multiplication</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.MultiplicationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getMultiplication()
   * @generated
   */
  int MULTIPLICATION = 155;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLICATION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLICATION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLICATION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLICATION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLICATION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Multiplication</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLICATION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.DivisionImpl <em>Division</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.DivisionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getDivision()
   * @generated
   */
  int DIVISION = 156;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVISION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVISION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVISION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVISION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVISION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Division</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVISION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ModuloImpl <em>Modulo</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ModuloImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getModulo()
   * @generated
   */
  int MODULO = 157;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULO__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULO__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULO__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULO__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULO__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Modulo</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODULO_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryPlusExpressionImpl <em>Unary Plus Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryPlusExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnaryPlusExpression()
   * @generated
   */
  int UNARY_PLUS_EXPRESSION = 158;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_PLUS_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_PLUS_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_PLUS_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_PLUS_EXPRESSION__CHILD = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Unary Plus Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_PLUS_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryMinusExpressionImpl <em>Unary Minus Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryMinusExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnaryMinusExpression()
   * @generated
   */
  int UNARY_MINUS_EXPRESSION = 159;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_MINUS_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_MINUS_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_MINUS_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_MINUS_EXPRESSION__CHILD = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Unary Minus Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_MINUS_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryNegationExpressionImpl <em>Unary Negation Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryNegationExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnaryNegationExpression()
   * @generated
   */
  int UNARY_NEGATION_EXPRESSION = 160;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_NEGATION_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_NEGATION_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_NEGATION_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_NEGATION_EXPRESSION__CHILD = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Unary Negation Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_NEGATION_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryFirstExpressionImpl <em>Unary First Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryFirstExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnaryFirstExpression()
   * @generated
   */
  int UNARY_FIRST_EXPRESSION = 161;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_FIRST_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_FIRST_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_FIRST_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_FIRST_EXPRESSION__CHILD = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Unary First Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_FIRST_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryLengthExpressionImpl <em>Unary Length Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryLengthExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnaryLengthExpression()
   * @generated
   */
  int UNARY_LENGTH_EXPRESSION = 162;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_LENGTH_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_LENGTH_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_LENGTH_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_LENGTH_EXPRESSION__CHILD = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Unary Length Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_LENGTH_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryCoerceExpressionImpl <em>Unary Coerce Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryCoerceExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnaryCoerceExpression()
   * @generated
   */
  int UNARY_COERCE_EXPRESSION = 163;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_COERCE_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_COERCE_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_COERCE_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_COERCE_EXPRESSION__CHILD = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Unary Coerce Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_COERCE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryFullExpressionImpl <em>Unary Full Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryFullExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnaryFullExpression()
   * @generated
   */
  int UNARY_FULL_EXPRESSION = 164;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_FULL_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_FULL_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_FULL_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_FULL_EXPRESSION__CHILD = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Unary Full Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_FULL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryDeQueueExpressionImpl <em>Unary De Queue Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryDeQueueExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnaryDeQueueExpression()
   * @generated
   */
  int UNARY_DE_QUEUE_EXPRESSION = 165;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_DE_QUEUE_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_DE_QUEUE_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_DE_QUEUE_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_DE_QUEUE_EXPRESSION__CHILD = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Unary De Queue Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_DE_QUEUE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryEmptyExpressionImpl <em>Unary Empty Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.UnaryEmptyExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getUnaryEmptyExpression()
   * @generated
   */
  int UNARY_EMPTY_EXPRESSION = 166;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_EMPTY_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_EMPTY_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_EMPTY_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_EMPTY_EXPRESSION__CHILD = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Unary Empty Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNARY_EMPTY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ProjectionImpl <em>Projection</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ProjectionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getProjection()
   * @generated
   */
  int PROJECTION = 167;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECTION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECTION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECTION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Channel</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECTION__CHANNEL = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Field</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECTION__FIELD = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Projection</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECTION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ArrayAccessExpressionImpl <em>Array Access Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ArrayAccessExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getArrayAccessExpression()
   * @generated
   */
  int ARRAY_ACCESS_EXPRESSION = 168;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ACCESS_EXPRESSION__MINIMUM = IDENTIFIER_EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ACCESS_EXPRESSION__MAXIMUM = IDENTIFIER_EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ACCESS_EXPRESSION__SIZE = IDENTIFIER_EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Declaration</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ACCESS_EXPRESSION__DECLARATION = IDENTIFIER_EXPRESSION__DECLARATION;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ACCESS_EXPRESSION__CHILD = IDENTIFIER_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Indexes</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ACCESS_EXPRESSION__INDEXES = IDENTIFIER_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Array Access Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ARRAY_ACCESS_EXPRESSION_FEATURE_COUNT = IDENTIFIER_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordAccessExpressionImpl <em>Record Access Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.RecordAccessExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getRecordAccessExpression()
   * @generated
   */
  int RECORD_ACCESS_EXPRESSION = 169;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_ACCESS_EXPRESSION__MINIMUM = IDENTIFIER_EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_ACCESS_EXPRESSION__MAXIMUM = IDENTIFIER_EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_ACCESS_EXPRESSION__SIZE = IDENTIFIER_EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Declaration</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_ACCESS_EXPRESSION__DECLARATION = IDENTIFIER_EXPRESSION__DECLARATION;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_ACCESS_EXPRESSION__CHILD = IDENTIFIER_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Field</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_ACCESS_EXPRESSION__FIELD = IDENTIFIER_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Record Access Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RECORD_ACCESS_EXPRESSION_FEATURE_COUNT = IDENTIFIER_EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstructionExpressionImpl <em>Construction Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstructionExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getConstructionExpression()
   * @generated
   */
  int CONSTRUCTION_EXPRESSION = 170;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTRUCTION_EXPRESSION__MINIMUM = IDENTIFIER_EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTRUCTION_EXPRESSION__MAXIMUM = IDENTIFIER_EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTRUCTION_EXPRESSION__SIZE = IDENTIFIER_EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Declaration</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTRUCTION_EXPRESSION__DECLARATION = IDENTIFIER_EXPRESSION__DECLARATION;

  /**
   * The feature id for the '<em><b>Parameter</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTRUCTION_EXPRESSION__PARAMETER = IDENTIFIER_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Construction Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTRUCTION_EXPRESSION_FEATURE_COUNT = IDENTIFIER_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ExplicitArrayExpressionImpl <em>Explicit Array Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ExplicitArrayExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getExplicitArrayExpression()
   * @generated
   */
  int EXPLICIT_ARRAY_EXPRESSION = 171;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPLICIT_ARRAY_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPLICIT_ARRAY_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPLICIT_ARRAY_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPLICIT_ARRAY_EXPRESSION__VALUES = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Explicit Array Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPLICIT_ARRAY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ImplicitArrayExpressionImpl <em>Implicit Array Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ImplicitArrayExpressionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getImplicitArrayExpression()
   * @generated
   */
  int IMPLICIT_ARRAY_EXPRESSION = 172;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLICIT_ARRAY_EXPRESSION__MINIMUM = EXPRESSION__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLICIT_ARRAY_EXPRESSION__MAXIMUM = EXPRESSION__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLICIT_ARRAY_EXPRESSION__SIZE = EXPRESSION__SIZE;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLICIT_ARRAY_EXPRESSION__BODY = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLICIT_ARRAY_EXPRESSION__INDEX = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLICIT_ARRAY_EXPRESSION__TYPE = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Implicit Array Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLICIT_ARRAY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.TrueLiteralImpl <em>True Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.TrueLiteralImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getTrueLiteral()
   * @generated
   */
  int TRUE_LITERAL = 173;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRUE_LITERAL__MINIMUM = BOOLEAN_LITERAL__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRUE_LITERAL__MAXIMUM = BOOLEAN_LITERAL__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRUE_LITERAL__SIZE = BOOLEAN_LITERAL__SIZE;

  /**
   * The number of structural features of the '<em>True Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRUE_LITERAL_FEATURE_COUNT = BOOLEAN_LITERAL_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.FalseLiteralImpl <em>False Literal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FalseLiteralImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getFalseLiteral()
   * @generated
   */
  int FALSE_LITERAL = 174;

  /**
   * The feature id for the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FALSE_LITERAL__MINIMUM = BOOLEAN_LITERAL__MINIMUM;

  /**
   * The feature id for the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FALSE_LITERAL__MAXIMUM = BOOLEAN_LITERAL__MAXIMUM;

  /**
   * The feature id for the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FALSE_LITERAL__SIZE = BOOLEAN_LITERAL__SIZE;

  /**
   * The number of structural features of the '<em>False Literal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FALSE_LITERAL_FEATURE_COUNT = BOOLEAN_LITERAL_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.AllPropertyImpl <em>All Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.AllPropertyImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getAllProperty()
   * @generated
   */
  int ALL_PROPERTY = 175;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_PROPERTY__VARIABLE = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_PROPERTY__TYPE = PROPERTY_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_PROPERTY__CHILD = PROPERTY_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>All Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALL_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ExistsPropertyImpl <em>Exists Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ExistsPropertyImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getExistsProperty()
   * @generated
   */
  int EXISTS_PROPERTY = 176;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXISTS_PROPERTY__VARIABLE = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXISTS_PROPERTY__TYPE = PROPERTY_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXISTS_PROPERTY__CHILD = PROPERTY_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Exists Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXISTS_PROPERTY_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PropertyDisjunctionImpl <em>Property Disjunction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PropertyDisjunctionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPropertyDisjunction()
   * @generated
   */
  int PROPERTY_DISJUNCTION = 177;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_DISJUNCTION__LEFT = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_DISJUNCTION__RIGHT = PROPERTY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Property Disjunction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_DISJUNCTION_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PropertyImplicationImpl <em>Property Implication</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PropertyImplicationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPropertyImplication()
   * @generated
   */
  int PROPERTY_IMPLICATION = 178;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_IMPLICATION__LEFT = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_IMPLICATION__RIGHT = PROPERTY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Property Implication</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_IMPLICATION_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PropertyConjunctionImpl <em>Property Conjunction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PropertyConjunctionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPropertyConjunction()
   * @generated
   */
  int PROPERTY_CONJUNCTION = 179;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_CONJUNCTION__LEFT = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_CONJUNCTION__RIGHT = PROPERTY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Property Conjunction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_CONJUNCTION_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PropertyNegationImpl <em>Property Negation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PropertyNegationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPropertyNegation()
   * @generated
   */
  int PROPERTY_NEGATION = 180;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_NEGATION__CHILD = PROPERTY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Property Negation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_NEGATION_FEATURE_COUNT = PROPERTY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LeadsToPatternImpl <em>Leads To Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LeadsToPatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLeadsToPattern()
   * @generated
   */
  int LEADS_TO_PATTERN = 181;

  /**
   * The feature id for the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEADS_TO_PATTERN__SUBJECT = SEQUENCE_PATTERN_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Follower</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEADS_TO_PATTERN__FOLLOWER = SEQUENCE_PATTERN_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Before</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEADS_TO_PATTERN__BEFORE = SEQUENCE_PATTERN_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Lower</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEADS_TO_PATTERN__LOWER = SEQUENCE_PATTERN_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Upper</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEADS_TO_PATTERN__UPPER = SEQUENCE_PATTERN_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>After</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEADS_TO_PATTERN__AFTER = SEQUENCE_PATTERN_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Until</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEADS_TO_PATTERN__UNTIL = SEQUENCE_PATTERN_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Min</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEADS_TO_PATTERN__MIN = SEQUENCE_PATTERN_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Max</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEADS_TO_PATTERN__MAX = SEQUENCE_PATTERN_FEATURE_COUNT + 8;

  /**
   * The number of structural features of the '<em>Leads To Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LEADS_TO_PATTERN_FEATURE_COUNT = SEQUENCE_PATTERN_FEATURE_COUNT + 9;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PrecedesPatternImpl <em>Precedes Pattern</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.PrecedesPatternImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getPrecedesPattern()
   * @generated
   */
  int PRECEDES_PATTERN = 182;

  /**
   * The feature id for the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRECEDES_PATTERN__SUBJECT = SEQUENCE_PATTERN_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Follower</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRECEDES_PATTERN__FOLLOWER = SEQUENCE_PATTERN_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Before</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRECEDES_PATTERN__BEFORE = SEQUENCE_PATTERN_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>After</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRECEDES_PATTERN__AFTER = SEQUENCE_PATTERN_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Until</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRECEDES_PATTERN__UNTIL = SEQUENCE_PATTERN_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Min</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRECEDES_PATTERN__MIN = SEQUENCE_PATTERN_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Max</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRECEDES_PATTERN__MAX = SEQUENCE_PATTERN_FEATURE_COUNT + 6;

  /**
   * The number of structural features of the '<em>Precedes Pattern</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PRECEDES_PATTERN_FEATURE_COUNT = SEQUENCE_PATTERN_FEATURE_COUNT + 7;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLAllImpl <em>LTL All</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLAllImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLAll()
   * @generated
   */
  int LTL_ALL = 183;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_ALL__INDEX = LTL_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_ALL__TYPE = LTL_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_ALL__CHILD = LTL_PROPERTY_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>LTL All</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_ALL_FEATURE_COUNT = LTL_PROPERTY_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLExistsImpl <em>LTL Exists</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLExistsImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLExists()
   * @generated
   */
  int LTL_EXISTS = 184;

  /**
   * The feature id for the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_EXISTS__INDEX = LTL_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_EXISTS__TYPE = LTL_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_EXISTS__CHILD = LTL_PROPERTY_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>LTL Exists</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_EXISTS_FEATURE_COUNT = LTL_PROPERTY_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLDisjunctionImpl <em>LTL Disjunction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLDisjunctionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLDisjunction()
   * @generated
   */
  int LTL_DISJUNCTION = 185;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_DISJUNCTION__LEFT = LTL_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_DISJUNCTION__RIGHT = LTL_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>LTL Disjunction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_DISJUNCTION_FEATURE_COUNT = LTL_PROPERTY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLImplicationImpl <em>LTL Implication</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLImplicationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLImplication()
   * @generated
   */
  int LTL_IMPLICATION = 186;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_IMPLICATION__LEFT = LTL_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_IMPLICATION__RIGHT = LTL_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>LTL Implication</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_IMPLICATION_FEATURE_COUNT = LTL_PROPERTY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLConjunctionImpl <em>LTL Conjunction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLConjunctionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLConjunction()
   * @generated
   */
  int LTL_CONJUNCTION = 187;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_CONJUNCTION__LEFT = LTL_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_CONJUNCTION__RIGHT = LTL_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>LTL Conjunction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_CONJUNCTION_FEATURE_COUNT = LTL_PROPERTY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLUntilImpl <em>LTL Until</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLUntilImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLUntil()
   * @generated
   */
  int LTL_UNTIL = 188;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNTIL__LEFT = LTL_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNTIL__RIGHT = LTL_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>LTL Until</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNTIL_FEATURE_COUNT = LTL_PROPERTY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLReleaseImpl <em>LTL Release</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLReleaseImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLRelease()
   * @generated
   */
  int LTL_RELEASE = 189;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_RELEASE__LEFT = LTL_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_RELEASE__RIGHT = LTL_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>LTL Release</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_RELEASE_FEATURE_COUNT = LTL_PROPERTY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLUnaryNegationImpl <em>LTL Unary Negation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLUnaryNegationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLUnaryNegation()
   * @generated
   */
  int LTL_UNARY_NEGATION = 190;

  /**
   * The feature id for the '<em><b>Operand</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNARY_NEGATION__OPERAND = LTL_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>LTL Unary Negation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNARY_NEGATION_FEATURE_COUNT = LTL_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLUnaryNextImpl <em>LTL Unary Next</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLUnaryNextImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLUnaryNext()
   * @generated
   */
  int LTL_UNARY_NEXT = 191;

  /**
   * The feature id for the '<em><b>Operand</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNARY_NEXT__OPERAND = LTL_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>LTL Unary Next</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNARY_NEXT_FEATURE_COUNT = LTL_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLUnaryAlwaysImpl <em>LTL Unary Always</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLUnaryAlwaysImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLUnaryAlways()
   * @generated
   */
  int LTL_UNARY_ALWAYS = 192;

  /**
   * The feature id for the '<em><b>Operand</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNARY_ALWAYS__OPERAND = LTL_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>LTL Unary Always</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNARY_ALWAYS_FEATURE_COUNT = LTL_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLUnaryEventuallyImpl <em>LTL Unary Eventually</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLUnaryEventuallyImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLUnaryEventually()
   * @generated
   */
  int LTL_UNARY_EVENTUALLY = 193;

  /**
   * The feature id for the '<em><b>Operand</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNARY_EVENTUALLY__OPERAND = LTL_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>LTL Unary Eventually</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNARY_EVENTUALLY_FEATURE_COUNT = LTL_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLVariableImpl <em>LTL Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.LTLVariableImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getLTLVariable()
   * @generated
   */
  int LTL_VARIABLE = 194;

  /**
   * The feature id for the '<em><b>Variable</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_VARIABLE__VARIABLE = LTL_PROPERTY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>LTL Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_VARIABLE_FEATURE_COUNT = LTL_PROPERTY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ObservableDisjunctionImpl <em>Observable Disjunction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ObservableDisjunctionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getObservableDisjunction()
   * @generated
   */
  int OBSERVABLE_DISJUNCTION = 195;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSERVABLE_DISJUNCTION__LEFT = OBSERVABLE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSERVABLE_DISJUNCTION__RIGHT = OBSERVABLE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Observable Disjunction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSERVABLE_DISJUNCTION_FEATURE_COUNT = OBSERVABLE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ObservableImplicationImpl <em>Observable Implication</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ObservableImplicationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getObservableImplication()
   * @generated
   */
  int OBSERVABLE_IMPLICATION = 196;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSERVABLE_IMPLICATION__LEFT = OBSERVABLE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSERVABLE_IMPLICATION__RIGHT = OBSERVABLE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Observable Implication</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSERVABLE_IMPLICATION_FEATURE_COUNT = OBSERVABLE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ObservableConjunctionImpl <em>Observable Conjunction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ObservableConjunctionImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getObservableConjunction()
   * @generated
   */
  int OBSERVABLE_CONJUNCTION = 197;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSERVABLE_CONJUNCTION__LEFT = OBSERVABLE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSERVABLE_CONJUNCTION__RIGHT = OBSERVABLE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Observable Conjunction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSERVABLE_CONJUNCTION_FEATURE_COUNT = OBSERVABLE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ObservableNegationImpl <em>Observable Negation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.ObservableNegationImpl
   * @see fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacrePackageImpl#getObservableNegation()
   * @generated
   */
  int OBSERVABLE_NEGATION = 198;

  /**
   * The feature id for the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSERVABLE_NEGATION__CHILD = OBSERVABLE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Observable Negation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSERVABLE_NEGATION_FEATURE_COUNT = OBSERVABLE_FEATURE_COUNT + 1;


  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Model</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Model
   * @generated
   */
  EClass getModel();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.Model#getImports <em>Imports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Imports</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Model#getImports()
   * @see #getModel()
   * @generated
   */
  EReference getModel_Imports();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.Model#getDeclarations <em>Declarations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Declarations</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Model#getDeclarations()
   * @see #getModel()
   * @generated
   */
  EReference getModel_Declarations();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.Model#getRequirements <em>Requirements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Requirements</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Model#getRequirements()
   * @see #getModel()
   * @generated
   */
  EReference getModel_Requirements();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Model#getRoot <em>Root</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Root</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Model#getRoot()
   * @see #getModel()
   * @generated
   */
  EReference getModel_Root();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RootDeclaration <em>Root Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Root Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RootDeclaration
   * @generated
   */
  EClass getRootDeclaration();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NamedElement <em>Named Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Named Element</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NamedElement
   * @generated
   */
  EClass getNamedElement();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclarationUse <em>Type Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Declaration Use</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclarationUse
   * @generated
   */
  EClass getTypeDeclarationUse();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclarationUse <em>Constant Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Constant Declaration Use</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclarationUse
   * @generated
   */
  EClass getConstantDeclarationUse();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExpressionDeclarationUse <em>Expression Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression Declaration Use</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExpressionDeclarationUse
   * @generated
   */
  EClass getExpressionDeclarationUse();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ReferenceDeclarationUse <em>Reference Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Reference Declaration Use</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ReferenceDeclarationUse
   * @generated
   */
  EClass getReferenceDeclarationUse();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagDeclarationUse <em>Union Tag Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Union Tag Declaration Use</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTagDeclarationUse
   * @generated
   */
  EClass getUnionTagDeclarationUse();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldDeclarationUse <em>Record Field Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Record Field Declaration Use</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldDeclarationUse
   * @generated
   */
  EClass getRecordFieldDeclarationUse();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PatternDeclarationUse <em>Pattern Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Pattern Declaration Use</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PatternDeclarationUse
   * @generated
   */
  EClass getPatternDeclarationUse();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.BoundDeclarationUse <em>Bound Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bound Declaration Use</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.BoundDeclarationUse
   * @generated
   */
  EClass getBoundDeclarationUse();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PathDeclarationUse <em>Path Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Path Declaration Use</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PathDeclarationUse
   * @generated
   */
  EClass getPathDeclarationUse();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ImportDeclaration <em>Import Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Import Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ImportDeclaration
   * @generated
   */
  EClass getImportDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.ImportDeclaration#getImportURI <em>Import URI</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Import URI</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ImportDeclaration#getImportURI()
   * @see #getImportDeclaration()
   * @generated
   */
  EAttribute getImportDeclaration_ImportURI();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Declaration <em>Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Declaration
   * @generated
   */
  EClass getDeclaration();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParameterizedDeclaration <em>Parameterized Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parameterized Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParameterizedDeclaration
   * @generated
   */
  EClass getParameterizedDeclaration();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration <em>Type Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration
   * @generated
   */
  EClass getTypeDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration#getName()
   * @see #getTypeDeclaration()
   * @generated
   */
  EAttribute getTypeDeclaration_Name();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration#getValue()
   * @see #getTypeDeclaration()
   * @generated
   */
  EReference getTypeDeclaration_Value();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ChannelDeclaration <em>Channel Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Channel Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ChannelDeclaration
   * @generated
   */
  EClass getChannelDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.ChannelDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ChannelDeclaration#getName()
   * @see #getChannelDeclaration()
   * @generated
   */
  EAttribute getChannelDeclaration_Name();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ChannelDeclaration#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ChannelDeclaration#getValue()
   * @see #getChannelDeclaration()
   * @generated
   */
  EReference getChannelDeclaration_Value();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ChannelType <em>Channel Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Channel Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ChannelType
   * @generated
   */
  EClass getChannelType();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ChannelType#getSize <em>Size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Size</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ChannelType#getSize()
   * @see #getChannelType()
   * @generated
   */
  EReference getChannelType_Size();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.ChannelType#isIn <em>In</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>In</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ChannelType#isIn()
   * @see #getChannelType()
   * @generated
   */
  EAttribute getChannelType_In();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.ChannelType#isOut <em>Out</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Out</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ChannelType#isOut()
   * @see #getChannelType()
   * @generated
   */
  EAttribute getChannelType_Out();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ChannelType#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ChannelType#getType()
   * @see #getChannelType()
   * @generated
   */
  EReference getChannelType_Type();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Type
   * @generated
   */
  EClass getType();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.BasicType <em>Basic Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Basic Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.BasicType
   * @generated
   */
  EClass getBasicType();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RangeType <em>Range Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Range Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RangeType
   * @generated
   */
  EClass getRangeType();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.RangeType#getMinimum <em>Minimum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Minimum</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RangeType#getMinimum()
   * @see #getRangeType()
   * @generated
   */
  EReference getRangeType_Minimum();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.RangeType#getMaximum <em>Maximum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Maximum</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RangeType#getMaximum()
   * @see #getRangeType()
   * @generated
   */
  EReference getRangeType_Maximum();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.RangeType#getSize <em>Size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Size</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RangeType#getSize()
   * @see #getRangeType()
   * @generated
   */
  EReference getRangeType_Size();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionType <em>Union Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Union Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionType
   * @generated
   */
  EClass getUnionType();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionType#getTags <em>Tags</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Tags</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionType#getTags()
   * @see #getUnionType()
   * @generated
   */
  EReference getUnionType_Tags();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTags <em>Union Tags</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Union Tags</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTags
   * @generated
   */
  EClass getUnionTags();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTags#getTags <em>Tags</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Tags</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTags#getTags()
   * @see #getUnionTags()
   * @generated
   */
  EReference getUnionTags_Tags();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTags#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTags#getType()
   * @see #getUnionTags()
   * @generated
   */
  EReference getUnionTags_Type();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagDeclaration <em>Union Tag Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Union Tag Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTagDeclaration
   * @generated
   */
  EClass getUnionTagDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTagDeclaration#getName()
   * @see #getUnionTagDeclaration()
   * @generated
   */
  EAttribute getUnionTagDeclaration_Name();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordType <em>Record Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Record Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordType
   * @generated
   */
  EClass getRecordType();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordType#getFields <em>Fields</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Fields</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordType#getFields()
   * @see #getRecordType()
   * @generated
   */
  EReference getRecordType_Fields();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFields <em>Record Fields</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Record Fields</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordFields
   * @generated
   */
  EClass getRecordFields();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFields#getFields <em>Fields</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Fields</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordFields#getFields()
   * @see #getRecordFields()
   * @generated
   */
  EReference getRecordFields_Fields();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFields#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordFields#getType()
   * @see #getRecordFields()
   * @generated
   */
  EReference getRecordFields_Type();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldDeclaration <em>Record Field Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Record Field Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldDeclaration
   * @generated
   */
  EClass getRecordFieldDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldDeclaration#getName()
   * @see #getRecordFieldDeclaration()
   * @generated
   */
  EAttribute getRecordFieldDeclaration_Name();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.QueueType <em>Queue Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Queue Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.QueueType
   * @generated
   */
  EClass getQueueType();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.QueueType#getSize <em>Size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Size</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.QueueType#getSize()
   * @see #getQueueType()
   * @generated
   */
  EReference getQueueType_Size();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.QueueType#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.QueueType#getType()
   * @see #getQueueType()
   * @generated
   */
  EReference getQueueType_Type();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayType <em>Array Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ArrayType
   * @generated
   */
  EClass getArrayType();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayType#getSize <em>Size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Size</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ArrayType#getSize()
   * @see #getArrayType()
   * @generated
   */
  EReference getArrayType_Size();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayType#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ArrayType#getType()
   * @see #getArrayType()
   * @generated
   */
  EReference getArrayType_Type();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ReferencedType <em>Referenced Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Referenced Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ReferencedType
   * @generated
   */
  EClass getReferencedType();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ReferencedType#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ReferencedType#getType()
   * @see #getReferencedType()
   * @generated
   */
  EReference getReferencedType_Type();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclaration <em>Constant Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Constant Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclaration
   * @generated
   */
  EClass getConstantDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclaration#getName()
   * @see #getConstantDeclaration()
   * @generated
   */
  EAttribute getConstantDeclaration_Name();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclaration#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclaration#getType()
   * @see #getConstantDeclaration()
   * @generated
   */
  EReference getConstantDeclaration_Type();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclaration#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclaration#getValue()
   * @see #getConstantDeclaration()
   * @generated
   */
  EReference getConstantDeclaration_Value();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration <em>Process Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Process Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration
   * @generated
   */
  EClass getProcessDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getName()
   * @see #getProcessDeclaration()
   * @generated
   */
  EAttribute getProcessDeclaration_Name();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getGenerics <em>Generics</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Generics</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getGenerics()
   * @see #getProcessDeclaration()
   * @generated
   */
  EReference getProcessDeclaration_Generics();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getPorts <em>Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ports</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getPorts()
   * @see #getProcessDeclaration()
   * @generated
   */
  EReference getProcessDeclaration_Ports();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getParameters <em>Parameters</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Parameters</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getParameters()
   * @see #getProcessDeclaration()
   * @generated
   */
  EReference getProcessDeclaration_Parameters();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getLocalPorts <em>Local Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Local Ports</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getLocalPorts()
   * @see #getProcessDeclaration()
   * @generated
   */
  EReference getProcessDeclaration_LocalPorts();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getPriorities <em>Priorities</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Priorities</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getPriorities()
   * @see #getProcessDeclaration()
   * @generated
   */
  EReference getProcessDeclaration_Priorities();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getStates <em>States</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>States</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getStates()
   * @see #getProcessDeclaration()
   * @generated
   */
  EReference getProcessDeclaration_States();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getVariables <em>Variables</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Variables</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getVariables()
   * @see #getProcessDeclaration()
   * @generated
   */
  EReference getProcessDeclaration_Variables();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getPrelude <em>Prelude</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Prelude</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getPrelude()
   * @see #getProcessDeclaration()
   * @generated
   */
  EReference getProcessDeclaration_Prelude();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getTransitions <em>Transitions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Transitions</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration#getTransitions()
   * @see #getProcessDeclaration()
   * @generated
   */
  EReference getProcessDeclaration_Transitions();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericDeclaration <em>Generic Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericDeclaration
   * @generated
   */
  EClass getGenericDeclaration();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericTypeDeclaration <em>Generic Type Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Type Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericTypeDeclaration
   * @generated
   */
  EClass getGenericTypeDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericTypeDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericTypeDeclaration#getName()
   * @see #getGenericTypeDeclaration()
   * @generated
   */
  EAttribute getGenericTypeDeclaration_Name();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericConstantDeclaration <em>Generic Constant Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Constant Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericConstantDeclaration
   * @generated
   */
  EClass getGenericConstantDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericConstantDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericConstantDeclaration#getName()
   * @see #getGenericConstantDeclaration()
   * @generated
   */
  EAttribute getGenericConstantDeclaration_Name();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericUnionTagDeclaration <em>Generic Union Tag Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Union Tag Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericUnionTagDeclaration
   * @generated
   */
  EClass getGenericUnionTagDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericUnionTagDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericUnionTagDeclaration#getName()
   * @see #getGenericUnionTagDeclaration()
   * @generated
   */
  EAttribute getGenericUnionTagDeclaration_Name();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericRecordFieldDeclaration <em>Generic Record Field Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Record Field Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericRecordFieldDeclaration
   * @generated
   */
  EClass getGenericRecordFieldDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericRecordFieldDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericRecordFieldDeclaration#getName()
   * @see #getGenericRecordFieldDeclaration()
   * @generated
   */
  EAttribute getGenericRecordFieldDeclaration_Name();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.StateDeclaration <em>State Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>State Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StateDeclaration
   * @generated
   */
  EClass getStateDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.StateDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StateDeclaration#getName()
   * @see #getStateDeclaration()
   * @generated
   */
  EAttribute getStateDeclaration_Name();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Transition <em>Transition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Transition</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Transition
   * @generated
   */
  EClass getTransition();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Transition#getOrigin <em>Origin</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Origin</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Transition#getOrigin()
   * @see #getTransition()
   * @generated
   */
  EReference getTransition_Origin();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Transition#getAction <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Action</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Transition#getAction()
   * @see #getTransition()
   * @generated
   */
  EReference getTransition_Action();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration <em>Component Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Component Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration
   * @generated
   */
  EClass getComponentDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getName()
   * @see #getComponentDeclaration()
   * @generated
   */
  EAttribute getComponentDeclaration_Name();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getGenerics <em>Generics</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Generics</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getGenerics()
   * @see #getComponentDeclaration()
   * @generated
   */
  EReference getComponentDeclaration_Generics();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getPorts <em>Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ports</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getPorts()
   * @see #getComponentDeclaration()
   * @generated
   */
  EReference getComponentDeclaration_Ports();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getParameters <em>Parameters</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Parameters</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getParameters()
   * @see #getComponentDeclaration()
   * @generated
   */
  EReference getComponentDeclaration_Parameters();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getVariables <em>Variables</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Variables</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getVariables()
   * @see #getComponentDeclaration()
   * @generated
   */
  EReference getComponentDeclaration_Variables();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getLocalPorts <em>Local Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Local Ports</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getLocalPorts()
   * @see #getComponentDeclaration()
   * @generated
   */
  EReference getComponentDeclaration_LocalPorts();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getPriorities <em>Priorities</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Priorities</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getPriorities()
   * @see #getComponentDeclaration()
   * @generated
   */
  EReference getComponentDeclaration_Priorities();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getPrelude <em>Prelude</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Prelude</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getPrelude()
   * @see #getComponentDeclaration()
   * @generated
   */
  EReference getComponentDeclaration_Prelude();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getBody()
   * @see #getComponentDeclaration()
   * @generated
   */
  EReference getComponentDeclaration_Body();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PortsDeclaration <em>Ports Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ports Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PortsDeclaration
   * @generated
   */
  EClass getPortsDeclaration();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.PortsDeclaration#getPorts <em>Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ports</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PortsDeclaration#getPorts()
   * @see #getPortsDeclaration()
   * @generated
   */
  EReference getPortsDeclaration_Ports();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PortsDeclaration#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PortsDeclaration#getType()
   * @see #getPortsDeclaration()
   * @generated
   */
  EReference getPortsDeclaration_Type();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration <em>Port Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration
   * @generated
   */
  EClass getPortDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration#getName()
   * @see #getPortDeclaration()
   * @generated
   */
  EAttribute getPortDeclaration_Name();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration <em>Local Ports Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Local Ports Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration
   * @generated
   */
  EClass getLocalPortsDeclaration();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getPorts <em>Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ports</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getPorts()
   * @see #getLocalPortsDeclaration()
   * @generated
   */
  EReference getLocalPortsDeclaration_Ports();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getType()
   * @see #getLocalPortsDeclaration()
   * @generated
   */
  EReference getLocalPortsDeclaration_Type();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getLeft()
   * @see #getLocalPortsDeclaration()
   * @generated
   */
  EReference getLocalPortsDeclaration_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getRight()
   * @see #getLocalPortsDeclaration()
   * @generated
   */
  EReference getLocalPortsDeclaration_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration <em>Parameters Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parameters Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration
   * @generated
   */
  EClass getParametersDeclaration();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#getParameters <em>Parameters</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Parameters</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#getParameters()
   * @see #getParametersDeclaration()
   * @generated
   */
  EReference getParametersDeclaration_Parameters();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#isRead <em>Read</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Read</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#isRead()
   * @see #getParametersDeclaration()
   * @generated
   */
  EAttribute getParametersDeclaration_Read();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#isWrite <em>Write</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Write</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#isWrite()
   * @see #getParametersDeclaration()
   * @generated
   */
  EAttribute getParametersDeclaration_Write();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#getType()
   * @see #getParametersDeclaration()
   * @generated
   */
  EReference getParametersDeclaration_Type();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration <em>Parameter Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parameter Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration
   * @generated
   */
  EClass getParameterDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration#isReference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Reference</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration#isReference()
   * @see #getParameterDeclaration()
   * @generated
   */
  EAttribute getParameterDeclaration_Reference();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration#isArray <em>Array</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Array</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration#isArray()
   * @see #getParameterDeclaration()
   * @generated
   */
  EAttribute getParameterDeclaration_Array();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration#getSize <em>Size</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Size</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration#getSize()
   * @see #getParameterDeclaration()
   * @generated
   */
  EReference getParameterDeclaration_Size();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration#getName()
   * @see #getParameterDeclaration()
   * @generated
   */
  EAttribute getParameterDeclaration_Name();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration <em>Variables Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variables Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration
   * @generated
   */
  EClass getVariablesDeclaration();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration#getVariables <em>Variables</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Variables</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration#getVariables()
   * @see #getVariablesDeclaration()
   * @generated
   */
  EReference getVariablesDeclaration_Variables();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration#getType()
   * @see #getVariablesDeclaration()
   * @generated
   */
  EReference getVariablesDeclaration_Type();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration#getValue()
   * @see #getVariablesDeclaration()
   * @generated
   */
  EReference getVariablesDeclaration_Value();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableDeclaration <em>Variable Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariableDeclaration
   * @generated
   */
  EClass getVariableDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariableDeclaration#getName()
   * @see #getVariableDeclaration()
   * @generated
   */
  EAttribute getVariableDeclaration_Name();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PriorityDeclaration <em>Priority Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Priority Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PriorityDeclaration
   * @generated
   */
  EClass getPriorityDeclaration();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.PriorityDeclaration#getGroups <em>Groups</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Groups</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PriorityDeclaration#getGroups()
   * @see #getPriorityDeclaration()
   * @generated
   */
  EReference getPriorityDeclaration_Groups();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PriorityGroup <em>Priority Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Priority Group</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PriorityGroup
   * @generated
   */
  EClass getPriorityGroup();

  /**
   * Returns the meta object for the reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.PriorityGroup#getPorts <em>Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Ports</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PriorityGroup#getPorts()
   * @see #getPriorityGroup()
   * @generated
   */
  EReference getPriorityGroup_Ports();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Statement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Statement
   * @generated
   */
  EClass getStatement();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NullStatement <em>Null Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Null Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NullStatement
   * @generated
   */
  EClass getNullStatement();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TaggedStatement <em>Tagged Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Tagged Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TaggedStatement
   * @generated
   */
  EClass getTaggedStatement();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.TaggedStatement#getTag <em>Tag</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Tag</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TaggedStatement#getTag()
   * @see #getTaggedStatement()
   * @generated
   */
  EReference getTaggedStatement_Tag();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TagDeclaration <em>Tag Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Tag Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TagDeclaration
   * @generated
   */
  EClass getTagDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.TagDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TagDeclaration#getName()
   * @see #getTagDeclaration()
   * @generated
   */
  EAttribute getTagDeclaration_Name();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PatternStatement <em>Pattern Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Pattern Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PatternStatement
   * @generated
   */
  EClass getPatternStatement();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Pattern <em>Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Pattern
   * @generated
   */
  EClass getPattern();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AnyPattern <em>Any Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Any Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AnyPattern
   * @generated
   */
  EClass getAnyPattern();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantPattern <em>Constant Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Constant Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantPattern
   * @generated
   */
  EClass getConstantPattern();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantPattern#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantPattern#getValue()
   * @see #getConstantPattern()
   * @generated
   */
  EReference getConstantPattern_Value();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.IntegerPattern <em>Integer Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.IntegerPattern
   * @generated
   */
  EClass getIntegerPattern();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.IntegerPattern#isNegative <em>Negative</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Negative</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.IntegerPattern#isNegative()
   * @see #getIntegerPattern()
   * @generated
   */
  EAttribute getIntegerPattern_Negative();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.IntegerPattern#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.IntegerPattern#getValue()
   * @see #getIntegerPattern()
   * @generated
   */
  EAttribute getIntegerPattern_Value();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.IdentifierPattern <em>Identifier Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Identifier Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.IdentifierPattern
   * @generated
   */
  EClass getIdentifierPattern();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.IdentifierPattern#getDeclaration <em>Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.IdentifierPattern#getDeclaration()
   * @see #getIdentifierPattern()
   * @generated
   */
  EReference getIdentifierPattern_Declaration();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement <em>Conditional Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Conditional Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement
   * @generated
   */
  EClass getConditionalStatement();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getConditions <em>Conditions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Conditions</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getConditions()
   * @see #getConditionalStatement()
   * @generated
   */
  EReference getConditionalStatement_Conditions();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getThen <em>Then</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Then</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getThen()
   * @see #getConditionalStatement()
   * @generated
   */
  EReference getConditionalStatement_Then();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getElseif <em>Elseif</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elseif</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getElseif()
   * @see #getConditionalStatement()
   * @generated
   */
  EReference getConditionalStatement_Elseif();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getElse <em>Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Else</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getElse()
   * @see #getConditionalStatement()
   * @generated
   */
  EReference getConditionalStatement_Else();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExtendedConditionalStatement <em>Extended Conditional Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Extended Conditional Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExtendedConditionalStatement
   * @generated
   */
  EClass getExtendedConditionalStatement();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExtendedConditionalStatement#getCondition <em>Condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Condition</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExtendedConditionalStatement#getCondition()
   * @see #getExtendedConditionalStatement()
   * @generated
   */
  EReference getExtendedConditionalStatement_Condition();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExtendedConditionalStatement#getThen <em>Then</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Then</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExtendedConditionalStatement#getThen()
   * @see #getExtendedConditionalStatement()
   * @generated
   */
  EReference getExtendedConditionalStatement_Then();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.SelectStatement <em>Select Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Select Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.SelectStatement
   * @generated
   */
  EClass getSelectStatement();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.SelectStatement#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.SelectStatement#getBody()
   * @see #getSelectStatement()
   * @generated
   */
  EReference getSelectStatement_Body();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.SelectStatement#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.SelectStatement#getIndex()
   * @see #getSelectStatement()
   * @generated
   */
  EReference getSelectStatement_Index();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.SelectStatement#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.SelectStatement#getType()
   * @see #getSelectStatement()
   * @generated
   */
  EReference getSelectStatement_Type();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.WhileStatement <em>While Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>While Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.WhileStatement
   * @generated
   */
  EClass getWhileStatement();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.WhileStatement#getCondition <em>Condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Condition</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.WhileStatement#getCondition()
   * @see #getWhileStatement()
   * @generated
   */
  EReference getWhileStatement_Condition();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.WhileStatement#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.WhileStatement#getBody()
   * @see #getWhileStatement()
   * @generated
   */
  EReference getWhileStatement_Body();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ForeachStatement <em>Foreach Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Foreach Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ForeachStatement
   * @generated
   */
  EClass getForeachStatement();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ForeachStatement#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ForeachStatement#getVariable()
   * @see #getForeachStatement()
   * @generated
   */
  EReference getForeachStatement_Variable();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ForeachStatement#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ForeachStatement#getBody()
   * @see #getForeachStatement()
   * @generated
   */
  EReference getForeachStatement_Body();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ToStatement <em>To Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>To Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ToStatement
   * @generated
   */
  EClass getToStatement();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ToStatement#getState <em>State</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>State</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ToStatement#getState()
   * @see #getToStatement()
   * @generated
   */
  EReference getToStatement_State();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.CaseStatement <em>Case Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Case Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.CaseStatement
   * @generated
   */
  EClass getCaseStatement();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.CaseStatement#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.CaseStatement#getValue()
   * @see #getCaseStatement()
   * @generated
   */
  EReference getCaseStatement_Value();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.CaseStatement#getPattern <em>Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.CaseStatement#getPattern()
   * @see #getCaseStatement()
   * @generated
   */
  EReference getCaseStatement_Pattern();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.CaseStatement#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Body</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.CaseStatement#getBody()
   * @see #getCaseStatement()
   * @generated
   */
  EReference getCaseStatement_Body();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LoopStatement <em>Loop Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Loop Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LoopStatement
   * @generated
   */
  EClass getLoopStatement();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.OnStatement <em>On Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>On Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.OnStatement
   * @generated
   */
  EClass getOnStatement();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.OnStatement#getCondition <em>Condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Condition</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.OnStatement#getCondition()
   * @see #getOnStatement()
   * @generated
   */
  EReference getOnStatement_Condition();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.WaitStatement <em>Wait Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Wait Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.WaitStatement
   * @generated
   */
  EClass getWaitStatement();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.WaitStatement#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.WaitStatement#getLeft()
   * @see #getWaitStatement()
   * @generated
   */
  EReference getWaitStatement_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.WaitStatement#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.WaitStatement#getRight()
   * @see #getWaitStatement()
   * @generated
   */
  EReference getWaitStatement_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Composition <em>Composition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Composition</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Composition
   * @generated
   */
  EClass getComposition();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Composition#getGlobal <em>Global</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Global</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Composition#getGlobal()
   * @see #getComposition()
   * @generated
   */
  EReference getComposition_Global();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.Composition#getBlocks <em>Blocks</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Blocks</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Composition#getBlocks()
   * @see #getComposition()
   * @generated
   */
  EReference getComposition_Blocks();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Composition#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Composition#getIndex()
   * @see #getComposition()
   * @generated
   */
  EReference getComposition_Index();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Composition#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Composition#getType()
   * @see #getComposition()
   * @generated
   */
  EReference getComposition_Type();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Block <em>Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Block</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Block
   * @generated
   */
  EClass getBlock();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.CompositeBlock <em>Composite Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Composite Block</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.CompositeBlock
   * @generated
   */
  EClass getCompositeBlock();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.CompositeBlock#getLocal <em>Local</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Local</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.CompositeBlock#getLocal()
   * @see #getCompositeBlock()
   * @generated
   */
  EReference getCompositeBlock_Local();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.CompositeBlock#getComposition <em>Composition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Composition</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.CompositeBlock#getComposition()
   * @see #getCompositeBlock()
   * @generated
   */
  EReference getCompositeBlock_Composition();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration <em>Instance Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Instance Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration
   * @generated
   */
  EClass getInstanceDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration#getLocal <em>Local</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Local</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration#getLocal()
   * @see #getInstanceDeclaration()
   * @generated
   */
  EReference getInstanceDeclaration_Local();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration#getName()
   * @see #getInstanceDeclaration()
   * @generated
   */
  EAttribute getInstanceDeclaration_Name();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration#getInstance <em>Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Instance</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration#getInstance()
   * @see #getInstanceDeclaration()
   * @generated
   */
  EReference getInstanceDeclaration_Instance();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PortSet <em>Port Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port Set</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PortSet
   * @generated
   */
  EClass getPortSet();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.PortSet#isAllPorts <em>All Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>All Ports</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PortSet#isAllPorts()
   * @see #getPortSet()
   * @generated
   */
  EAttribute getPortSet_AllPorts();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.PortSet#getPorts <em>Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ports</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PortSet#getPorts()
   * @see #getPortSet()
   * @generated
   */
  EReference getPortSet_Ports();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance <em>Component Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Component Instance</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance
   * @generated
   */
  EClass getComponentInstance();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Component</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance#getComponent()
   * @see #getComponentInstance()
   * @generated
   */
  EReference getComponentInstance_Component();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance#getGenerics <em>Generics</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Generics</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance#getGenerics()
   * @see #getComponentInstance()
   * @generated
   */
  EReference getComponentInstance_Generics();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance#getPorts <em>Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ports</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance#getPorts()
   * @see #getComponentInstance()
   * @generated
   */
  EReference getComponentInstance_Ports();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance#getParameters <em>Parameters</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Parameters</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance#getParameters()
   * @see #getComponentInstance()
   * @generated
   */
  EReference getComponentInstance_Parameters();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericInstance <em>Generic Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Generic Instance</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericInstance
   * @generated
   */
  EClass getGenericInstance();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TypeInstance <em>Type Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Instance</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TypeInstance
   * @generated
   */
  EClass getTypeInstance();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.TypeInstance#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TypeInstance#getType()
   * @see #getTypeInstance()
   * @generated
   */
  EReference getTypeInstance_Type();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance <em>Constant Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Constant Instance</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance
   * @generated
   */
  EClass getConstantInstance();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance#getConst <em>Const</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Const</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance#getConst()
   * @see #getConstantInstance()
   * @generated
   */
  EReference getConstantInstance_Const();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance#getValue()
   * @see #getConstantInstance()
   * @generated
   */
  EReference getConstantInstance_Value();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance <em>Union Tag Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Union Tag Instance</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance
   * @generated
   */
  EClass getUnionTagInstance();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance#isConstr0 <em>Constr0</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Constr0</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance#isConstr0()
   * @see #getUnionTagInstance()
   * @generated
   */
  EAttribute getUnionTagInstance_Constr0();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance#isConstr1 <em>Constr1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Constr1</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance#isConstr1()
   * @see #getUnionTagInstance()
   * @generated
   */
  EAttribute getUnionTagInstance_Constr1();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance#getHead <em>Head</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Head</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance#getHead()
   * @see #getUnionTagInstance()
   * @generated
   */
  EReference getUnionTagInstance_Head();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldInstance <em>Record Field Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Record Field Instance</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldInstance
   * @generated
   */
  EClass getRecordFieldInstance();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldInstance#getField <em>Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Field</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldInstance#getField()
   * @see #getRecordFieldInstance()
   * @generated
   */
  EReference getRecordFieldInstance_Field();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Expression
   * @generated
   */
  EClass getExpression();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ReferenceExpression <em>Reference Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Reference Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ReferenceExpression
   * @generated
   */
  EClass getReferenceExpression();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ReferenceExpression#getDeclaration <em>Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ReferenceExpression#getDeclaration()
   * @see #getReferenceExpression()
   * @generated
   */
  EReference getReferenceExpression_Declaration();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.IdentifierExpression <em>Identifier Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Identifier Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.IdentifierExpression
   * @generated
   */
  EClass getIdentifierExpression();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.IdentifierExpression#getDeclaration <em>Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.IdentifierExpression#getDeclaration()
   * @see #getIdentifierExpression()
   * @generated
   */
  EReference getIdentifierExpression_Declaration();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordExpression <em>Record Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Record Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordExpression
   * @generated
   */
  EClass getRecordExpression();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordExpression#getFields <em>Fields</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Fields</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordExpression#getFields()
   * @see #getRecordExpression()
   * @generated
   */
  EReference getRecordExpression_Fields();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.FieldExpression <em>Field Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FieldExpression
   * @generated
   */
  EClass getFieldExpression();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.FieldExpression#getField <em>Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Field</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FieldExpression#getField()
   * @see #getFieldExpression()
   * @generated
   */
  EReference getFieldExpression_Field();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.FieldExpression#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FieldExpression#getValue()
   * @see #getFieldExpression()
   * @generated
   */
  EReference getFieldExpression_Value();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.QueueExpression <em>Queue Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Queue Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.QueueExpression
   * @generated
   */
  EClass getQueueExpression();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.QueueExpression#getValues <em>Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Values</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.QueueExpression#getValues()
   * @see #getQueueExpression()
   * @generated
   */
  EReference getQueueExpression_Values();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.EnqueueExpression <em>Enqueue Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enqueue Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.EnqueueExpression
   * @generated
   */
  EClass getEnqueueExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.EnqueueExpression#getElement <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Element</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.EnqueueExpression#getElement()
   * @see #getEnqueueExpression()
   * @generated
   */
  EReference getEnqueueExpression_Element();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.EnqueueExpression#getQueue <em>Queue</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Queue</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.EnqueueExpression#getQueue()
   * @see #getEnqueueExpression()
   * @generated
   */
  EReference getEnqueueExpression_Queue();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AppendExpression <em>Append Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Append Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AppendExpression
   * @generated
   */
  EClass getAppendExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AppendExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AppendExpression#getLeft()
   * @see #getAppendExpression()
   * @generated
   */
  EReference getAppendExpression_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AppendExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AppendExpression#getRight()
   * @see #getAppendExpression()
   * @generated
   */
  EReference getAppendExpression_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LiteralExpression <em>Literal Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Literal Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LiteralExpression
   * @generated
   */
  EClass getLiteralExpression();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.BooleanLiteral <em>Boolean Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Literal</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.BooleanLiteral
   * @generated
   */
  EClass getBooleanLiteral();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalLiteral <em>Natural Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Natural Literal</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalLiteral
   * @generated
   */
  EClass getNaturalLiteral();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalLiteral#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalLiteral#getValue()
   * @see #getNaturalLiteral()
   * @generated
   */
  EAttribute getNaturalLiteral_Value();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LowerBound <em>Lower Bound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Lower Bound</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LowerBound
   * @generated
   */
  EClass getLowerBound();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.LowerBound#isLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LowerBound#isLeft()
   * @see #getLowerBound()
   * @generated
   */
  EAttribute getLowerBound_Left();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.LowerBound#isRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LowerBound#isRight()
   * @see #getLowerBound()
   * @generated
   */
  EAttribute getLowerBound_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UpperBound <em>Upper Bound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Upper Bound</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UpperBound
   * @generated
   */
  EClass getUpperBound();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalLowerBound <em>Natural Lower Bound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Natural Lower Bound</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalLowerBound
   * @generated
   */
  EClass getNaturalLowerBound();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalLowerBound#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalLowerBound#getValue()
   * @see #getNaturalLowerBound()
   * @generated
   */
  EAttribute getNaturalLowerBound_Value();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalUpperBound <em>Natural Upper Bound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Natural Upper Bound</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalUpperBound
   * @generated
   */
  EClass getNaturalUpperBound();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalUpperBound#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalUpperBound#getValue()
   * @see #getNaturalUpperBound()
   * @generated
   */
  EAttribute getNaturalUpperBound_Value();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalUpperBound#isLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalUpperBound#isLeft()
   * @see #getNaturalUpperBound()
   * @generated
   */
  EAttribute getNaturalUpperBound_Left();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalUpperBound#isRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalUpperBound#isRight()
   * @see #getNaturalUpperBound()
   * @generated
   */
  EAttribute getNaturalUpperBound_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.DecimalLowerBound <em>Decimal Lower Bound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Decimal Lower Bound</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.DecimalLowerBound
   * @generated
   */
  EClass getDecimalLowerBound();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.DecimalLowerBound#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.DecimalLowerBound#getValue()
   * @see #getDecimalLowerBound()
   * @generated
   */
  EAttribute getDecimalLowerBound_Value();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.DecimalUpperBound <em>Decimal Upper Bound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Decimal Upper Bound</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.DecimalUpperBound
   * @generated
   */
  EClass getDecimalUpperBound();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.DecimalUpperBound#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.DecimalUpperBound#getValue()
   * @see #getDecimalUpperBound()
   * @generated
   */
  EAttribute getDecimalUpperBound_Value();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.DecimalUpperBound#isLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.DecimalUpperBound#isLeft()
   * @see #getDecimalUpperBound()
   * @generated
   */
  EAttribute getDecimalUpperBound_Left();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.DecimalUpperBound#isRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.DecimalUpperBound#isRight()
   * @see #getDecimalUpperBound()
   * @generated
   */
  EAttribute getDecimalUpperBound_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableLowerBound <em>Variable Lower Bound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable Lower Bound</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariableLowerBound
   * @generated
   */
  EClass getVariableLowerBound();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableLowerBound#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Variable</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariableLowerBound#getVariable()
   * @see #getVariableLowerBound()
   * @generated
   */
  EReference getVariableLowerBound_Variable();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound <em>Variable Upper Bound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable Upper Bound</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound
   * @generated
   */
  EClass getVariableUpperBound();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Variable</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound#getVariable()
   * @see #getVariableUpperBound()
   * @generated
   */
  EReference getVariableUpperBound_Variable();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound#isLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound#isLeft()
   * @see #getVariableUpperBound()
   * @generated
   */
  EAttribute getVariableUpperBound_Left();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound#isRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound#isRight()
   * @see #getVariableUpperBound()
   * @generated
   */
  EAttribute getVariableUpperBound_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.InfiniteUpperBound <em>Infinite Upper Bound</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Infinite Upper Bound</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.InfiniteUpperBound
   * @generated
   */
  EClass getInfiniteUpperBound();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Requirement <em>Requirement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Requirement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Requirement
   * @generated
   */
  EClass getRequirement();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Requirement#getProperty <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Property</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Requirement#getProperty()
   * @see #getRequirement()
   * @generated
   */
  EReference getRequirement_Property();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.Requirement#getPositive <em>Positive</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Positive</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Requirement#getPositive()
   * @see #getRequirement()
   * @generated
   */
  EAttribute getRequirement_Positive();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.Requirement#getNegative <em>Negative</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Negative</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Requirement#getNegative()
   * @see #getRequirement()
   * @generated
   */
  EAttribute getRequirement_Negative();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyDeclaration <em>Property Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Property Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyDeclaration
   * @generated
   */
  EClass getPropertyDeclaration();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyDeclaration#getName()
   * @see #getPropertyDeclaration()
   * @generated
   */
  EAttribute getPropertyDeclaration_Name();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyDeclaration#getProperty <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Property</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyDeclaration#getProperty()
   * @see #getPropertyDeclaration()
   * @generated
   */
  EReference getPropertyDeclaration_Property();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Property <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Property</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Property
   * @generated
   */
  EClass getProperty();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PatternProperty <em>Pattern Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Pattern Property</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PatternProperty
   * @generated
   */
  EClass getPatternProperty();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLPattern <em>LTL Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLPattern
   * @generated
   */
  EClass getLTLPattern();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLPattern#getProperty <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Property</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLPattern#getProperty()
   * @see #getLTLPattern()
   * @generated
   */
  EReference getLTLPattern_Property();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.DeadlockFreePattern <em>Deadlock Free Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Deadlock Free Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.DeadlockFreePattern
   * @generated
   */
  EClass getDeadlockFreePattern();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.InfinitelyOftenPattern <em>Infinitely Often Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Infinitely Often Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.InfinitelyOftenPattern
   * @generated
   */
  EClass getInfinitelyOftenPattern();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.InfinitelyOftenPattern#getSubject <em>Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Subject</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.InfinitelyOftenPattern#getSubject()
   * @see #getInfinitelyOftenPattern()
   * @generated
   */
  EReference getInfinitelyOftenPattern_Subject();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.MortalPattern <em>Mortal Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mortal Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.MortalPattern
   * @generated
   */
  EClass getMortalPattern();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.MortalPattern#getSubject <em>Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Subject</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.MortalPattern#getSubject()
   * @see #getMortalPattern()
   * @generated
   */
  EReference getMortalPattern_Subject();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern <em>Presence Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Presence Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern
   * @generated
   */
  EClass getPresencePattern();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getSubject <em>Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Subject</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getSubject()
   * @see #getPresencePattern()
   * @generated
   */
  EReference getPresencePattern_Subject();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getLasting <em>Lasting</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Lasting</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getLasting()
   * @see #getPresencePattern()
   * @generated
   */
  EAttribute getPresencePattern_Lasting();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getAfter <em>After</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>After</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getAfter()
   * @see #getPresencePattern()
   * @generated
   */
  EReference getPresencePattern_After();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getLower <em>Lower</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Lower</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getLower()
   * @see #getPresencePattern()
   * @generated
   */
  EReference getPresencePattern_Lower();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getUpper <em>Upper</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Upper</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getUpper()
   * @see #getPresencePattern()
   * @generated
   */
  EReference getPresencePattern_Upper();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getUntil <em>Until</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Until</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getUntil()
   * @see #getPresencePattern()
   * @generated
   */
  EReference getPresencePattern_Until();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getBefore <em>Before</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Before</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getBefore()
   * @see #getPresencePattern()
   * @generated
   */
  EReference getPresencePattern_Before();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getMin <em>Min</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Min</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getMin()
   * @see #getPresencePattern()
   * @generated
   */
  EReference getPresencePattern_Min();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getMax <em>Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Max</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getMax()
   * @see #getPresencePattern()
   * @generated
   */
  EReference getPresencePattern_Max();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern <em>Absence Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Absence Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern
   * @generated
   */
  EClass getAbsencePattern();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getSubject <em>Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Subject</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getSubject()
   * @see #getAbsencePattern()
   * @generated
   */
  EReference getAbsencePattern_Subject();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getAfter <em>After</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>After</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getAfter()
   * @see #getAbsencePattern()
   * @generated
   */
  EReference getAbsencePattern_After();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getLower <em>Lower</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Lower</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getLower()
   * @see #getAbsencePattern()
   * @generated
   */
  EReference getAbsencePattern_Lower();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getUpper <em>Upper</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Upper</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getUpper()
   * @see #getAbsencePattern()
   * @generated
   */
  EReference getAbsencePattern_Upper();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getUntil <em>Until</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Until</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getUntil()
   * @see #getAbsencePattern()
   * @generated
   */
  EReference getAbsencePattern_Until();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getBefore <em>Before</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Before</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getBefore()
   * @see #getAbsencePattern()
   * @generated
   */
  EReference getAbsencePattern_Before();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getMin <em>Min</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Min</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getMin()
   * @see #getAbsencePattern()
   * @generated
   */
  EReference getAbsencePattern_Min();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getMax <em>Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Max</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern#getMax()
   * @see #getAbsencePattern()
   * @generated
   */
  EReference getAbsencePattern_Max();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern <em>Always Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Always Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern
   * @generated
   */
  EClass getAlwaysPattern();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern#getSubject <em>Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Subject</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern#getSubject()
   * @see #getAlwaysPattern()
   * @generated
   */
  EReference getAlwaysPattern_Subject();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern#getBefore <em>Before</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Before</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern#getBefore()
   * @see #getAlwaysPattern()
   * @generated
   */
  EReference getAlwaysPattern_Before();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern#getAfter <em>After</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>After</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern#getAfter()
   * @see #getAlwaysPattern()
   * @generated
   */
  EReference getAlwaysPattern_After();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern#getUntil <em>Until</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Until</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern#getUntil()
   * @see #getAlwaysPattern()
   * @generated
   */
  EReference getAlwaysPattern_Until();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern#getMin <em>Min</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Min</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern#getMin()
   * @see #getAlwaysPattern()
   * @generated
   */
  EReference getAlwaysPattern_Min();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern#getMax <em>Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Max</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern#getMax()
   * @see #getAlwaysPattern()
   * @generated
   */
  EReference getAlwaysPattern_Max();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.SequencePattern <em>Sequence Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sequence Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.SequencePattern
   * @generated
   */
  EClass getSequencePattern();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLProperty <em>LTL Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Property</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLProperty
   * @generated
   */
  EClass getLTLProperty();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.StateEvent <em>State Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>State Event</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StateEvent
   * @generated
   */
  EClass getStateEvent();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.StateEvent#getSubject <em>Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Subject</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StateEvent#getSubject()
   * @see #getStateEvent()
   * @generated
   */
  EReference getStateEvent_Subject();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.EnterStateEvent <em>Enter State Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Enter State Event</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.EnterStateEvent
   * @generated
   */
  EClass getEnterStateEvent();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LeaveStateEvent <em>Leave State Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Leave State Event</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LeaveStateEvent
   * @generated
   */
  EClass getLeaveStateEvent();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Observable <em>Observable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Observable</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Observable
   * @generated
   */
  EClass getObservable();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableEvent <em>Observable Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Observable Event</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableEvent
   * @generated
   */
  EClass getObservableEvent();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PathEvent <em>Path Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Path Event</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PathEvent
   * @generated
   */
  EClass getPathEvent();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PathEvent#getPath <em>Path</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Path</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PathEvent#getPath()
   * @see #getPathEvent()
   * @generated
   */
  EReference getPathEvent_Path();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PathEvent#getItem <em>Item</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Item</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PathEvent#getItem()
   * @see #getPathEvent()
   * @generated
   */
  EReference getPathEvent_Item();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Path <em>Path</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Path</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Path
   * @generated
   */
  EClass getPath();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.Path#getItems <em>Items</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Items</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Path#getItems()
   * @see #getPath()
   * @generated
   */
  EReference getPath_Items();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PathItem <em>Path Item</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Path Item</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PathItem
   * @generated
   */
  EClass getPathItem();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalItem <em>Natural Item</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Natural Item</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalItem
   * @generated
   */
  EClass getNaturalItem();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalItem#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalItem#getValue()
   * @see #getNaturalItem()
   * @generated
   */
  EAttribute getNaturalItem_Value();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NamedItem <em>Named Item</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Named Item</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NamedItem
   * @generated
   */
  EClass getNamedItem();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.NamedItem#getDeclaration <em>Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Declaration</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NamedItem#getDeclaration()
   * @see #getNamedItem()
   * @generated
   */
  EReference getNamedItem_Declaration();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Subject <em>Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Subject</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Subject
   * @generated
   */
  EClass getSubject();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.StateSubject <em>State Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>State Subject</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StateSubject
   * @generated
   */
  EClass getStateSubject();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.StateSubject#getState <em>State</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>State</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StateSubject#getState()
   * @see #getStateSubject()
   * @generated
   */
  EReference getStateSubject_State();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ValueSubject <em>Value Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Value Subject</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ValueSubject
   * @generated
   */
  EClass getValueSubject();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ValueSubject#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ValueSubject#getValue()
   * @see #getValueSubject()
   * @generated
   */
  EReference getValueSubject_Value();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TagSubject <em>Tag Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Tag Subject</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TagSubject
   * @generated
   */
  EClass getTagSubject();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.TagSubject#getTag <em>Tag</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Tag</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TagSubject#getTag()
   * @see #getTagSubject()
   * @generated
   */
  EReference getTagSubject_Tag();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.EventSubject <em>Event Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Event Subject</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.EventSubject
   * @generated
   */
  EClass getEventSubject();

  /**
   * Returns the meta object for the attribute '{@link fr.irit.fiacre.etoile.xtext.fiacre.EventSubject#getEvent <em>Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Event</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.EventSubject#getEvent()
   * @see #getEventSubject()
   * @generated
   */
  EAttribute getEventSubject_Event();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TupleType <em>Tuple Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Tuple Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TupleType
   * @generated
   */
  EClass getTupleType();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.TupleType#getTypes <em>Types</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Types</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TupleType#getTypes()
   * @see #getTupleType()
   * @generated
   */
  EReference getTupleType_Types();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalType <em>Natural Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Natural Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalType
   * @generated
   */
  EClass getNaturalType();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.IntegerType <em>Integer Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.IntegerType
   * @generated
   */
  EClass getIntegerType();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.BooleanType <em>Boolean Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.BooleanType
   * @generated
   */
  EClass getBooleanType();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnlessStatement <em>Unless Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unless Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnlessStatement
   * @generated
   */
  EClass getUnlessStatement();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnlessStatement#getFollowers <em>Followers</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Followers</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnlessStatement#getFollowers()
   * @see #getUnlessStatement()
   * @generated
   */
  EReference getUnlessStatement_Followers();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.StatementChoice <em>Statement Choice</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement Choice</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StatementChoice
   * @generated
   */
  EClass getStatementChoice();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.StatementChoice#getChoices <em>Choices</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Choices</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StatementChoice#getChoices()
   * @see #getStatementChoice()
   * @generated
   */
  EReference getStatementChoice_Choices();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.StatementSequence <em>Statement Sequence</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement Sequence</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StatementSequence
   * @generated
   */
  EClass getStatementSequence();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.StatementSequence#getStatements <em>Statements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Statements</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StatementSequence#getStatements()
   * @see #getStatementSequence()
   * @generated
   */
  EReference getStatementSequence_Statements();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AssignStatement <em>Assign Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Assign Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AssignStatement
   * @generated
   */
  EClass getAssignStatement();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.AssignStatement#getPatterns <em>Patterns</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Patterns</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AssignStatement#getPatterns()
   * @see #getAssignStatement()
   * @generated
   */
  EReference getAssignStatement_Patterns();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.AssignStatement#getValues <em>Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Values</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AssignStatement#getValues()
   * @see #getAssignStatement()
   * @generated
   */
  EReference getAssignStatement_Values();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.SendStatement <em>Send Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Send Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.SendStatement
   * @generated
   */
  EClass getSendStatement();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.SendStatement#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.SendStatement#getPort()
   * @see #getSendStatement()
   * @generated
   */
  EReference getSendStatement_Port();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.SendStatement#getValues <em>Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Values</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.SendStatement#getValues()
   * @see #getSendStatement()
   * @generated
   */
  EReference getSendStatement_Values();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ReceiveStatement <em>Receive Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Receive Statement</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ReceiveStatement
   * @generated
   */
  EClass getReceiveStatement();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ReceiveStatement#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ReceiveStatement#getPort()
   * @see #getReceiveStatement()
   * @generated
   */
  EReference getReceiveStatement_Port();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ReceiveStatement#getPatterns <em>Patterns</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Patterns</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ReceiveStatement#getPatterns()
   * @see #getReceiveStatement()
   * @generated
   */
  EReference getReceiveStatement_Patterns();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ReceiveStatement#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ReceiveStatement#getExp()
   * @see #getReceiveStatement()
   * @generated
   */
  EReference getReceiveStatement_Exp();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstructorPattern <em>Constructor Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Constructor Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstructorPattern
   * @generated
   */
  EClass getConstructorPattern();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstructorPattern#getParameter <em>Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Parameter</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstructorPattern#getParameter()
   * @see #getConstructorPattern()
   * @generated
   */
  EReference getConstructorPattern_Parameter();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessPattern <em>Array Access Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Access Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessPattern
   * @generated
   */
  EClass getArrayAccessPattern();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessPattern#getSource <em>Source</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Source</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessPattern#getSource()
   * @see #getArrayAccessPattern()
   * @generated
   */
  EReference getArrayAccessPattern_Source();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessPattern#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessPattern#getIndex()
   * @see #getArrayAccessPattern()
   * @generated
   */
  EReference getArrayAccessPattern_Index();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessPattern <em>Record Access Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Record Access Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessPattern
   * @generated
   */
  EClass getRecordAccessPattern();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessPattern#getSource <em>Source</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Source</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessPattern#getSource()
   * @see #getRecordAccessPattern()
   * @generated
   */
  EReference getRecordAccessPattern_Source();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessPattern#getField <em>Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Field</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessPattern#getField()
   * @see #getRecordAccessPattern()
   * @generated
   */
  EReference getRecordAccessPattern_Field();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AllExpression <em>All Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>All Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AllExpression
   * @generated
   */
  EClass getAllExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AllExpression#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AllExpression#getIndex()
   * @see #getAllExpression()
   * @generated
   */
  EReference getAllExpression_Index();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AllExpression#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AllExpression#getType()
   * @see #getAllExpression()
   * @generated
   */
  EReference getAllExpression_Type();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AllExpression#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AllExpression#getChild()
   * @see #getAllExpression()
   * @generated
   */
  EReference getAllExpression_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExistsExpression <em>Exists Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Exists Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExistsExpression
   * @generated
   */
  EClass getExistsExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExistsExpression#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExistsExpression#getIndex()
   * @see #getExistsExpression()
   * @generated
   */
  EReference getExistsExpression_Index();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExistsExpression#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExistsExpression#getType()
   * @see #getExistsExpression()
   * @generated
   */
  EReference getExistsExpression_Type();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExistsExpression#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExistsExpression#getChild()
   * @see #getExistsExpression()
   * @generated
   */
  EReference getExistsExpression_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Conditional <em>Conditional</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Conditional</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Conditional
   * @generated
   */
  EClass getConditional();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Conditional#getCondition <em>Condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Condition</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Conditional#getCondition()
   * @see #getConditional()
   * @generated
   */
  EReference getConditional_Condition();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Conditional#getThen <em>Then</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Then</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Conditional#getThen()
   * @see #getConditional()
   * @generated
   */
  EReference getConditional_Then();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Conditional#getElse <em>Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Else</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Conditional#getElse()
   * @see #getConditional()
   * @generated
   */
  EReference getConditional_Else();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Disjunction <em>Disjunction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Disjunction</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Disjunction
   * @generated
   */
  EClass getDisjunction();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Disjunction#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Disjunction#getLeft()
   * @see #getDisjunction()
   * @generated
   */
  EReference getDisjunction_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Disjunction#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Disjunction#getRight()
   * @see #getDisjunction()
   * @generated
   */
  EReference getDisjunction_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Implication <em>Implication</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Implication</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Implication
   * @generated
   */
  EClass getImplication();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Implication#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Implication#getLeft()
   * @see #getImplication()
   * @generated
   */
  EReference getImplication_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Implication#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Implication#getRight()
   * @see #getImplication()
   * @generated
   */
  EReference getImplication_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Conjunction <em>Conjunction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Conjunction</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Conjunction
   * @generated
   */
  EClass getConjunction();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Conjunction#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Conjunction#getLeft()
   * @see #getConjunction()
   * @generated
   */
  EReference getConjunction_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Conjunction#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Conjunction#getRight()
   * @see #getConjunction()
   * @generated
   */
  EReference getConjunction_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonEqual <em>Comparison Equal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Comparison Equal</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonEqual
   * @generated
   */
  EClass getComparisonEqual();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonEqual#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonEqual#getLeft()
   * @see #getComparisonEqual()
   * @generated
   */
  EReference getComparisonEqual_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonEqual#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonEqual#getRight()
   * @see #getComparisonEqual()
   * @generated
   */
  EReference getComparisonEqual_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonNotEqual <em>Comparison Not Equal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Comparison Not Equal</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonNotEqual
   * @generated
   */
  EClass getComparisonNotEqual();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonNotEqual#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonNotEqual#getLeft()
   * @see #getComparisonNotEqual()
   * @generated
   */
  EReference getComparisonNotEqual_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonNotEqual#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonNotEqual#getRight()
   * @see #getComparisonNotEqual()
   * @generated
   */
  EReference getComparisonNotEqual_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesser <em>Comparison Lesser</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Comparison Lesser</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesser
   * @generated
   */
  EClass getComparisonLesser();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesser#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesser#getLeft()
   * @see #getComparisonLesser()
   * @generated
   */
  EReference getComparisonLesser_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesser#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesser#getRight()
   * @see #getComparisonLesser()
   * @generated
   */
  EReference getComparisonLesser_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesserEqual <em>Comparison Lesser Equal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Comparison Lesser Equal</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesserEqual
   * @generated
   */
  EClass getComparisonLesserEqual();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesserEqual#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesserEqual#getLeft()
   * @see #getComparisonLesserEqual()
   * @generated
   */
  EReference getComparisonLesserEqual_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesserEqual#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesserEqual#getRight()
   * @see #getComparisonLesserEqual()
   * @generated
   */
  EReference getComparisonLesserEqual_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreater <em>Comparison Greater</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Comparison Greater</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreater
   * @generated
   */
  EClass getComparisonGreater();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreater#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreater#getLeft()
   * @see #getComparisonGreater()
   * @generated
   */
  EReference getComparisonGreater_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreater#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreater#getRight()
   * @see #getComparisonGreater()
   * @generated
   */
  EReference getComparisonGreater_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreaterEqual <em>Comparison Greater Equal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Comparison Greater Equal</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreaterEqual
   * @generated
   */
  EClass getComparisonGreaterEqual();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreaterEqual#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreaterEqual#getLeft()
   * @see #getComparisonGreaterEqual()
   * @generated
   */
  EReference getComparisonGreaterEqual_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreaterEqual#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreaterEqual#getRight()
   * @see #getComparisonGreaterEqual()
   * @generated
   */
  EReference getComparisonGreaterEqual_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Addition <em>Addition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Addition</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Addition
   * @generated
   */
  EClass getAddition();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Addition#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Addition#getLeft()
   * @see #getAddition()
   * @generated
   */
  EReference getAddition_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Addition#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Addition#getRight()
   * @see #getAddition()
   * @generated
   */
  EReference getAddition_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Substraction <em>Substraction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Substraction</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Substraction
   * @generated
   */
  EClass getSubstraction();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Substraction#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Substraction#getLeft()
   * @see #getSubstraction()
   * @generated
   */
  EReference getSubstraction_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Substraction#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Substraction#getRight()
   * @see #getSubstraction()
   * @generated
   */
  EReference getSubstraction_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Multiplication <em>Multiplication</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Multiplication</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Multiplication
   * @generated
   */
  EClass getMultiplication();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Multiplication#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Multiplication#getLeft()
   * @see #getMultiplication()
   * @generated
   */
  EReference getMultiplication_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Multiplication#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Multiplication#getRight()
   * @see #getMultiplication()
   * @generated
   */
  EReference getMultiplication_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Division <em>Division</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Division</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Division
   * @generated
   */
  EClass getDivision();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Division#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Division#getLeft()
   * @see #getDivision()
   * @generated
   */
  EReference getDivision_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Division#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Division#getRight()
   * @see #getDivision()
   * @generated
   */
  EReference getDivision_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Modulo <em>Modulo</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Modulo</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Modulo
   * @generated
   */
  EClass getModulo();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Modulo#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Modulo#getLeft()
   * @see #getModulo()
   * @generated
   */
  EReference getModulo_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Modulo#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Modulo#getRight()
   * @see #getModulo()
   * @generated
   */
  EReference getModulo_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryPlusExpression <em>Unary Plus Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unary Plus Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryPlusExpression
   * @generated
   */
  EClass getUnaryPlusExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryPlusExpression#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryPlusExpression#getChild()
   * @see #getUnaryPlusExpression()
   * @generated
   */
  EReference getUnaryPlusExpression_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryMinusExpression <em>Unary Minus Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unary Minus Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryMinusExpression
   * @generated
   */
  EClass getUnaryMinusExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryMinusExpression#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryMinusExpression#getChild()
   * @see #getUnaryMinusExpression()
   * @generated
   */
  EReference getUnaryMinusExpression_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryNegationExpression <em>Unary Negation Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unary Negation Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryNegationExpression
   * @generated
   */
  EClass getUnaryNegationExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryNegationExpression#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryNegationExpression#getChild()
   * @see #getUnaryNegationExpression()
   * @generated
   */
  EReference getUnaryNegationExpression_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryFirstExpression <em>Unary First Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unary First Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryFirstExpression
   * @generated
   */
  EClass getUnaryFirstExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryFirstExpression#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryFirstExpression#getChild()
   * @see #getUnaryFirstExpression()
   * @generated
   */
  EReference getUnaryFirstExpression_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryLengthExpression <em>Unary Length Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unary Length Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryLengthExpression
   * @generated
   */
  EClass getUnaryLengthExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryLengthExpression#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryLengthExpression#getChild()
   * @see #getUnaryLengthExpression()
   * @generated
   */
  EReference getUnaryLengthExpression_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryCoerceExpression <em>Unary Coerce Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unary Coerce Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryCoerceExpression
   * @generated
   */
  EClass getUnaryCoerceExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryCoerceExpression#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryCoerceExpression#getChild()
   * @see #getUnaryCoerceExpression()
   * @generated
   */
  EReference getUnaryCoerceExpression_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryFullExpression <em>Unary Full Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unary Full Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryFullExpression
   * @generated
   */
  EClass getUnaryFullExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryFullExpression#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryFullExpression#getChild()
   * @see #getUnaryFullExpression()
   * @generated
   */
  EReference getUnaryFullExpression_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryDeQueueExpression <em>Unary De Queue Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unary De Queue Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryDeQueueExpression
   * @generated
   */
  EClass getUnaryDeQueueExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryDeQueueExpression#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryDeQueueExpression#getChild()
   * @see #getUnaryDeQueueExpression()
   * @generated
   */
  EReference getUnaryDeQueueExpression_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryEmptyExpression <em>Unary Empty Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unary Empty Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryEmptyExpression
   * @generated
   */
  EClass getUnaryEmptyExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryEmptyExpression#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryEmptyExpression#getChild()
   * @see #getUnaryEmptyExpression()
   * @generated
   */
  EReference getUnaryEmptyExpression_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Projection <em>Projection</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Projection</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Projection
   * @generated
   */
  EClass getProjection();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Projection#getChannel <em>Channel</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Channel</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Projection#getChannel()
   * @see #getProjection()
   * @generated
   */
  EReference getProjection_Channel();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.Projection#getField <em>Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Field</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Projection#getField()
   * @see #getProjection()
   * @generated
   */
  EReference getProjection_Field();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessExpression <em>Array Access Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Array Access Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessExpression
   * @generated
   */
  EClass getArrayAccessExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessExpression#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessExpression#getChild()
   * @see #getArrayAccessExpression()
   * @generated
   */
  EReference getArrayAccessExpression_Child();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessExpression#getIndexes <em>Indexes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Indexes</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessExpression#getIndexes()
   * @see #getArrayAccessExpression()
   * @generated
   */
  EReference getArrayAccessExpression_Indexes();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessExpression <em>Record Access Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Record Access Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessExpression
   * @generated
   */
  EClass getRecordAccessExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessExpression#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessExpression#getChild()
   * @see #getRecordAccessExpression()
   * @generated
   */
  EReference getRecordAccessExpression_Child();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessExpression#getField <em>Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Field</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessExpression#getField()
   * @see #getRecordAccessExpression()
   * @generated
   */
  EReference getRecordAccessExpression_Field();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstructionExpression <em>Construction Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Construction Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstructionExpression
   * @generated
   */
  EClass getConstructionExpression();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstructionExpression#getParameter <em>Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Parameter</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstructionExpression#getParameter()
   * @see #getConstructionExpression()
   * @generated
   */
  EReference getConstructionExpression_Parameter();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExplicitArrayExpression <em>Explicit Array Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Explicit Array Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExplicitArrayExpression
   * @generated
   */
  EClass getExplicitArrayExpression();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExplicitArrayExpression#getValues <em>Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Values</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExplicitArrayExpression#getValues()
   * @see #getExplicitArrayExpression()
   * @generated
   */
  EReference getExplicitArrayExpression_Values();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ImplicitArrayExpression <em>Implicit Array Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Implicit Array Expression</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ImplicitArrayExpression
   * @generated
   */
  EClass getImplicitArrayExpression();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irit.fiacre.etoile.xtext.fiacre.ImplicitArrayExpression#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Body</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ImplicitArrayExpression#getBody()
   * @see #getImplicitArrayExpression()
   * @generated
   */
  EReference getImplicitArrayExpression_Body();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ImplicitArrayExpression#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ImplicitArrayExpression#getIndex()
   * @see #getImplicitArrayExpression()
   * @generated
   */
  EReference getImplicitArrayExpression_Index();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ImplicitArrayExpression#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ImplicitArrayExpression#getType()
   * @see #getImplicitArrayExpression()
   * @generated
   */
  EReference getImplicitArrayExpression_Type();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TrueLiteral <em>True Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>True Literal</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TrueLiteral
   * @generated
   */
  EClass getTrueLiteral();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.FalseLiteral <em>False Literal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>False Literal</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FalseLiteral
   * @generated
   */
  EClass getFalseLiteral();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AllProperty <em>All Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>All Property</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AllProperty
   * @generated
   */
  EClass getAllProperty();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AllProperty#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AllProperty#getVariable()
   * @see #getAllProperty()
   * @generated
   */
  EReference getAllProperty_Variable();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AllProperty#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AllProperty#getType()
   * @see #getAllProperty()
   * @generated
   */
  EReference getAllProperty_Type();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.AllProperty#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AllProperty#getChild()
   * @see #getAllProperty()
   * @generated
   */
  EReference getAllProperty_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExistsProperty <em>Exists Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Exists Property</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExistsProperty
   * @generated
   */
  EClass getExistsProperty();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExistsProperty#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExistsProperty#getVariable()
   * @see #getExistsProperty()
   * @generated
   */
  EReference getExistsProperty_Variable();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExistsProperty#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExistsProperty#getType()
   * @see #getExistsProperty()
   * @generated
   */
  EReference getExistsProperty_Type();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExistsProperty#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExistsProperty#getChild()
   * @see #getExistsProperty()
   * @generated
   */
  EReference getExistsProperty_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyDisjunction <em>Property Disjunction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Property Disjunction</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyDisjunction
   * @generated
   */
  EClass getPropertyDisjunction();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyDisjunction#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyDisjunction#getLeft()
   * @see #getPropertyDisjunction()
   * @generated
   */
  EReference getPropertyDisjunction_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyDisjunction#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyDisjunction#getRight()
   * @see #getPropertyDisjunction()
   * @generated
   */
  EReference getPropertyDisjunction_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyImplication <em>Property Implication</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Property Implication</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyImplication
   * @generated
   */
  EClass getPropertyImplication();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyImplication#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyImplication#getLeft()
   * @see #getPropertyImplication()
   * @generated
   */
  EReference getPropertyImplication_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyImplication#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyImplication#getRight()
   * @see #getPropertyImplication()
   * @generated
   */
  EReference getPropertyImplication_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyConjunction <em>Property Conjunction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Property Conjunction</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyConjunction
   * @generated
   */
  EClass getPropertyConjunction();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyConjunction#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyConjunction#getLeft()
   * @see #getPropertyConjunction()
   * @generated
   */
  EReference getPropertyConjunction_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyConjunction#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyConjunction#getRight()
   * @see #getPropertyConjunction()
   * @generated
   */
  EReference getPropertyConjunction_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyNegation <em>Property Negation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Property Negation</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyNegation
   * @generated
   */
  EClass getPropertyNegation();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyNegation#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyNegation#getChild()
   * @see #getPropertyNegation()
   * @generated
   */
  EReference getPropertyNegation_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern <em>Leads To Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Leads To Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern
   * @generated
   */
  EClass getLeadsToPattern();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getSubject <em>Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Subject</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getSubject()
   * @see #getLeadsToPattern()
   * @generated
   */
  EReference getLeadsToPattern_Subject();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getFollower <em>Follower</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Follower</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getFollower()
   * @see #getLeadsToPattern()
   * @generated
   */
  EReference getLeadsToPattern_Follower();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getBefore <em>Before</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Before</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getBefore()
   * @see #getLeadsToPattern()
   * @generated
   */
  EReference getLeadsToPattern_Before();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getLower <em>Lower</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Lower</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getLower()
   * @see #getLeadsToPattern()
   * @generated
   */
  EReference getLeadsToPattern_Lower();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getUpper <em>Upper</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Upper</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getUpper()
   * @see #getLeadsToPattern()
   * @generated
   */
  EReference getLeadsToPattern_Upper();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getAfter <em>After</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>After</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getAfter()
   * @see #getLeadsToPattern()
   * @generated
   */
  EReference getLeadsToPattern_After();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getUntil <em>Until</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Until</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getUntil()
   * @see #getLeadsToPattern()
   * @generated
   */
  EReference getLeadsToPattern_Until();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getMin <em>Min</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Min</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getMin()
   * @see #getLeadsToPattern()
   * @generated
   */
  EReference getLeadsToPattern_Min();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getMax <em>Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Max</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern#getMax()
   * @see #getLeadsToPattern()
   * @generated
   */
  EReference getLeadsToPattern_Max();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern <em>Precedes Pattern</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Precedes Pattern</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern
   * @generated
   */
  EClass getPrecedesPattern();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getSubject <em>Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Subject</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getSubject()
   * @see #getPrecedesPattern()
   * @generated
   */
  EReference getPrecedesPattern_Subject();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getFollower <em>Follower</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Follower</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getFollower()
   * @see #getPrecedesPattern()
   * @generated
   */
  EReference getPrecedesPattern_Follower();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getBefore <em>Before</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Before</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getBefore()
   * @see #getPrecedesPattern()
   * @generated
   */
  EReference getPrecedesPattern_Before();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getAfter <em>After</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>After</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getAfter()
   * @see #getPrecedesPattern()
   * @generated
   */
  EReference getPrecedesPattern_After();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getUntil <em>Until</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Until</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getUntil()
   * @see #getPrecedesPattern()
   * @generated
   */
  EReference getPrecedesPattern_Until();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getMin <em>Min</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Min</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getMin()
   * @see #getPrecedesPattern()
   * @generated
   */
  EReference getPrecedesPattern_Min();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getMax <em>Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Max</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getMax()
   * @see #getPrecedesPattern()
   * @generated
   */
  EReference getPrecedesPattern_Max();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLAll <em>LTL All</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL All</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLAll
   * @generated
   */
  EClass getLTLAll();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLAll#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLAll#getIndex()
   * @see #getLTLAll()
   * @generated
   */
  EReference getLTLAll_Index();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLAll#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLAll#getType()
   * @see #getLTLAll()
   * @generated
   */
  EReference getLTLAll_Type();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLAll#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLAll#getChild()
   * @see #getLTLAll()
   * @generated
   */
  EReference getLTLAll_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLExists <em>LTL Exists</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Exists</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLExists
   * @generated
   */
  EClass getLTLExists();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLExists#getIndex <em>Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Index</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLExists#getIndex()
   * @see #getLTLExists()
   * @generated
   */
  EReference getLTLExists_Index();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLExists#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLExists#getType()
   * @see #getLTLExists()
   * @generated
   */
  EReference getLTLExists_Type();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLExists#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLExists#getChild()
   * @see #getLTLExists()
   * @generated
   */
  EReference getLTLExists_Child();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLDisjunction <em>LTL Disjunction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Disjunction</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLDisjunction
   * @generated
   */
  EClass getLTLDisjunction();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLDisjunction#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLDisjunction#getLeft()
   * @see #getLTLDisjunction()
   * @generated
   */
  EReference getLTLDisjunction_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLDisjunction#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLDisjunction#getRight()
   * @see #getLTLDisjunction()
   * @generated
   */
  EReference getLTLDisjunction_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLImplication <em>LTL Implication</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Implication</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLImplication
   * @generated
   */
  EClass getLTLImplication();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLImplication#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLImplication#getLeft()
   * @see #getLTLImplication()
   * @generated
   */
  EReference getLTLImplication_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLImplication#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLImplication#getRight()
   * @see #getLTLImplication()
   * @generated
   */
  EReference getLTLImplication_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLConjunction <em>LTL Conjunction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Conjunction</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLConjunction
   * @generated
   */
  EClass getLTLConjunction();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLConjunction#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLConjunction#getLeft()
   * @see #getLTLConjunction()
   * @generated
   */
  EReference getLTLConjunction_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLConjunction#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLConjunction#getRight()
   * @see #getLTLConjunction()
   * @generated
   */
  EReference getLTLConjunction_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUntil <em>LTL Until</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Until</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUntil
   * @generated
   */
  EClass getLTLUntil();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUntil#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUntil#getLeft()
   * @see #getLTLUntil()
   * @generated
   */
  EReference getLTLUntil_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUntil#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUntil#getRight()
   * @see #getLTLUntil()
   * @generated
   */
  EReference getLTLUntil_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLRelease <em>LTL Release</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Release</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLRelease
   * @generated
   */
  EClass getLTLRelease();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLRelease#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLRelease#getLeft()
   * @see #getLTLRelease()
   * @generated
   */
  EReference getLTLRelease_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLRelease#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLRelease#getRight()
   * @see #getLTLRelease()
   * @generated
   */
  EReference getLTLRelease_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryNegation <em>LTL Unary Negation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Unary Negation</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryNegation
   * @generated
   */
  EClass getLTLUnaryNegation();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryNegation#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Operand</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryNegation#getOperand()
   * @see #getLTLUnaryNegation()
   * @generated
   */
  EReference getLTLUnaryNegation_Operand();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryNext <em>LTL Unary Next</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Unary Next</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryNext
   * @generated
   */
  EClass getLTLUnaryNext();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryNext#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Operand</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryNext#getOperand()
   * @see #getLTLUnaryNext()
   * @generated
   */
  EReference getLTLUnaryNext_Operand();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryAlways <em>LTL Unary Always</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Unary Always</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryAlways
   * @generated
   */
  EClass getLTLUnaryAlways();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryAlways#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Operand</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryAlways#getOperand()
   * @see #getLTLUnaryAlways()
   * @generated
   */
  EReference getLTLUnaryAlways_Operand();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryEventually <em>LTL Unary Eventually</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Unary Eventually</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryEventually
   * @generated
   */
  EClass getLTLUnaryEventually();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryEventually#getOperand <em>Operand</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Operand</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryEventually#getOperand()
   * @see #getLTLUnaryEventually()
   * @generated
   */
  EReference getLTLUnaryEventually_Operand();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLVariable <em>LTL Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Variable</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLVariable
   * @generated
   */
  EClass getLTLVariable();

  /**
   * Returns the meta object for the reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLVariable#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Variable</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLVariable#getVariable()
   * @see #getLTLVariable()
   * @generated
   */
  EReference getLTLVariable_Variable();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableDisjunction <em>Observable Disjunction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Observable Disjunction</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableDisjunction
   * @generated
   */
  EClass getObservableDisjunction();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableDisjunction#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableDisjunction#getLeft()
   * @see #getObservableDisjunction()
   * @generated
   */
  EReference getObservableDisjunction_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableDisjunction#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableDisjunction#getRight()
   * @see #getObservableDisjunction()
   * @generated
   */
  EReference getObservableDisjunction_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableImplication <em>Observable Implication</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Observable Implication</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableImplication
   * @generated
   */
  EClass getObservableImplication();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableImplication#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableImplication#getLeft()
   * @see #getObservableImplication()
   * @generated
   */
  EReference getObservableImplication_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableImplication#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableImplication#getRight()
   * @see #getObservableImplication()
   * @generated
   */
  EReference getObservableImplication_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableConjunction <em>Observable Conjunction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Observable Conjunction</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableConjunction
   * @generated
   */
  EClass getObservableConjunction();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableConjunction#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableConjunction#getLeft()
   * @see #getObservableConjunction()
   * @generated
   */
  EReference getObservableConjunction_Left();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableConjunction#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableConjunction#getRight()
   * @see #getObservableConjunction()
   * @generated
   */
  EReference getObservableConjunction_Right();

  /**
   * Returns the meta object for class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableNegation <em>Observable Negation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Observable Negation</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableNegation
   * @generated
   */
  EClass getObservableNegation();

  /**
   * Returns the meta object for the containment reference '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableNegation#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Child</em>'.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableNegation#getChild()
   * @see #getObservableNegation()
   * @generated
   */
  EReference getObservableNegation_Child();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  FiacreFactory getFiacreFactory();

} //FiacrePackage
