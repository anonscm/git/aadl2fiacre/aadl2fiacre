/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Access Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessPattern#getSource <em>Source</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessPattern#getIndex <em>Index</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getArrayAccessPattern()
 * @model
 * @generated
 */
public interface ArrayAccessPattern extends IdentifierPattern
{
  /**
   * Returns the value of the '<em><b>Source</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Source</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Source</em>' containment reference.
   * @see #setSource(IdentifierPattern)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getArrayAccessPattern_Source()
   * @model containment="true"
   * @generated
   */
  IdentifierPattern getSource();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessPattern#getSource <em>Source</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Source</em>' containment reference.
   * @see #getSource()
   * @generated
   */
  void setSource(IdentifierPattern value);

  /**
   * Returns the value of the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Index</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Index</em>' containment reference.
   * @see #setIndex(Expression)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getArrayAccessPattern_Index()
   * @model containment="true"
   * @generated
   */
  Expression getIndex();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessPattern#getIndex <em>Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Index</em>' containment reference.
   * @see #getIndex()
   * @generated
   */
  void setIndex(Expression value);

} // ArrayAccessPattern
