/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance;
import fr.irit.fiacre.etoile.xtext.fiacre.Expression;
import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;
import fr.irit.fiacre.etoile.xtext.fiacre.GenericInstance;
import fr.irit.fiacre.etoile.xtext.fiacre.ParameterizedDeclaration;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ComponentInstanceImpl#getComponent <em>Component</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ComponentInstanceImpl#getGenerics <em>Generics</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ComponentInstanceImpl#getPorts <em>Ports</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ComponentInstanceImpl#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentInstanceImpl extends MinimalEObjectImpl.Container implements ComponentInstance
{
  /**
   * The cached value of the '{@link #getComponent() <em>Component</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComponent()
   * @generated
   * @ordered
   */
  protected ParameterizedDeclaration component;

  /**
   * The cached value of the '{@link #getGenerics() <em>Generics</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGenerics()
   * @generated
   * @ordered
   */
  protected EList<GenericInstance> generics;

  /**
   * The cached value of the '{@link #getPorts() <em>Ports</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPorts()
   * @generated
   * @ordered
   */
  protected EList<Expression> ports;

  /**
   * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParameters()
   * @generated
   * @ordered
   */
  protected EList<Expression> parameters;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ComponentInstanceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FiacrePackage.eINSTANCE.getComponentInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParameterizedDeclaration getComponent()
  {
    if (component != null && component.eIsProxy())
    {
      InternalEObject oldComponent = (InternalEObject)component;
      component = (ParameterizedDeclaration)eResolveProxy(oldComponent);
      if (component != oldComponent)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, FiacrePackage.COMPONENT_INSTANCE__COMPONENT, oldComponent, component));
      }
    }
    return component;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParameterizedDeclaration basicGetComponent()
  {
    return component;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setComponent(ParameterizedDeclaration newComponent)
  {
    ParameterizedDeclaration oldComponent = component;
    component = newComponent;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.COMPONENT_INSTANCE__COMPONENT, oldComponent, component));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<GenericInstance> getGenerics()
  {
    if (generics == null)
    {
      generics = new EObjectContainmentEList<GenericInstance>(GenericInstance.class, this, FiacrePackage.COMPONENT_INSTANCE__GENERICS);
    }
    return generics;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Expression> getPorts()
  {
    if (ports == null)
    {
      ports = new EObjectContainmentEList<Expression>(Expression.class, this, FiacrePackage.COMPONENT_INSTANCE__PORTS);
    }
    return ports;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Expression> getParameters()
  {
    if (parameters == null)
    {
      parameters = new EObjectContainmentEList<Expression>(Expression.class, this, FiacrePackage.COMPONENT_INSTANCE__PARAMETERS);
    }
    return parameters;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case FiacrePackage.COMPONENT_INSTANCE__GENERICS:
        return ((InternalEList<?>)getGenerics()).basicRemove(otherEnd, msgs);
      case FiacrePackage.COMPONENT_INSTANCE__PORTS:
        return ((InternalEList<?>)getPorts()).basicRemove(otherEnd, msgs);
      case FiacrePackage.COMPONENT_INSTANCE__PARAMETERS:
        return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case FiacrePackage.COMPONENT_INSTANCE__COMPONENT:
        if (resolve) return getComponent();
        return basicGetComponent();
      case FiacrePackage.COMPONENT_INSTANCE__GENERICS:
        return getGenerics();
      case FiacrePackage.COMPONENT_INSTANCE__PORTS:
        return getPorts();
      case FiacrePackage.COMPONENT_INSTANCE__PARAMETERS:
        return getParameters();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case FiacrePackage.COMPONENT_INSTANCE__COMPONENT:
        setComponent((ParameterizedDeclaration)newValue);
        return;
      case FiacrePackage.COMPONENT_INSTANCE__GENERICS:
        getGenerics().clear();
        getGenerics().addAll((Collection<? extends GenericInstance>)newValue);
        return;
      case FiacrePackage.COMPONENT_INSTANCE__PORTS:
        getPorts().clear();
        getPorts().addAll((Collection<? extends Expression>)newValue);
        return;
      case FiacrePackage.COMPONENT_INSTANCE__PARAMETERS:
        getParameters().clear();
        getParameters().addAll((Collection<? extends Expression>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.COMPONENT_INSTANCE__COMPONENT:
        setComponent((ParameterizedDeclaration)null);
        return;
      case FiacrePackage.COMPONENT_INSTANCE__GENERICS:
        getGenerics().clear();
        return;
      case FiacrePackage.COMPONENT_INSTANCE__PORTS:
        getPorts().clear();
        return;
      case FiacrePackage.COMPONENT_INSTANCE__PARAMETERS:
        getParameters().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.COMPONENT_INSTANCE__COMPONENT:
        return component != null;
      case FiacrePackage.COMPONENT_INSTANCE__GENERICS:
        return generics != null && !generics.isEmpty();
      case FiacrePackage.COMPONENT_INSTANCE__PORTS:
        return ports != null && !ports.isEmpty();
      case FiacrePackage.COMPONENT_INSTANCE__PARAMETERS:
        return parameters != null && !parameters.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ComponentInstanceImpl
