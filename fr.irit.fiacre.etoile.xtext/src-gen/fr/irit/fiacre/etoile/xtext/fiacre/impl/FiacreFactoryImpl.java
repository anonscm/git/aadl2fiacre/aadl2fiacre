/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FiacreFactoryImpl extends EFactoryImpl implements FiacreFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static FiacreFactory init()
  {
    try
    {
      FiacreFactory theFiacreFactory = (FiacreFactory)EPackage.Registry.INSTANCE.getEFactory(FiacrePackage.eNS_URI);
      if (theFiacreFactory != null)
      {
        return theFiacreFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new FiacreFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FiacreFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case FiacrePackage.MODEL: return createModel();
      case FiacrePackage.ROOT_DECLARATION: return createRootDeclaration();
      case FiacrePackage.NAMED_ELEMENT: return createNamedElement();
      case FiacrePackage.TYPE_DECLARATION_USE: return createTypeDeclarationUse();
      case FiacrePackage.CONSTANT_DECLARATION_USE: return createConstantDeclarationUse();
      case FiacrePackage.EXPRESSION_DECLARATION_USE: return createExpressionDeclarationUse();
      case FiacrePackage.REFERENCE_DECLARATION_USE: return createReferenceDeclarationUse();
      case FiacrePackage.UNION_TAG_DECLARATION_USE: return createUnionTagDeclarationUse();
      case FiacrePackage.RECORD_FIELD_DECLARATION_USE: return createRecordFieldDeclarationUse();
      case FiacrePackage.PATTERN_DECLARATION_USE: return createPatternDeclarationUse();
      case FiacrePackage.BOUND_DECLARATION_USE: return createBoundDeclarationUse();
      case FiacrePackage.PATH_DECLARATION_USE: return createPathDeclarationUse();
      case FiacrePackage.IMPORT_DECLARATION: return createImportDeclaration();
      case FiacrePackage.DECLARATION: return createDeclaration();
      case FiacrePackage.PARAMETERIZED_DECLARATION: return createParameterizedDeclaration();
      case FiacrePackage.TYPE_DECLARATION: return createTypeDeclaration();
      case FiacrePackage.CHANNEL_DECLARATION: return createChannelDeclaration();
      case FiacrePackage.CHANNEL_TYPE: return createChannelType();
      case FiacrePackage.TYPE: return createType();
      case FiacrePackage.BASIC_TYPE: return createBasicType();
      case FiacrePackage.RANGE_TYPE: return createRangeType();
      case FiacrePackage.UNION_TYPE: return createUnionType();
      case FiacrePackage.UNION_TAGS: return createUnionTags();
      case FiacrePackage.UNION_TAG_DECLARATION: return createUnionTagDeclaration();
      case FiacrePackage.RECORD_TYPE: return createRecordType();
      case FiacrePackage.RECORD_FIELDS: return createRecordFields();
      case FiacrePackage.RECORD_FIELD_DECLARATION: return createRecordFieldDeclaration();
      case FiacrePackage.QUEUE_TYPE: return createQueueType();
      case FiacrePackage.ARRAY_TYPE: return createArrayType();
      case FiacrePackage.REFERENCED_TYPE: return createReferencedType();
      case FiacrePackage.CONSTANT_DECLARATION: return createConstantDeclaration();
      case FiacrePackage.PROCESS_DECLARATION: return createProcessDeclaration();
      case FiacrePackage.GENERIC_DECLARATION: return createGenericDeclaration();
      case FiacrePackage.GENERIC_TYPE_DECLARATION: return createGenericTypeDeclaration();
      case FiacrePackage.GENERIC_CONSTANT_DECLARATION: return createGenericConstantDeclaration();
      case FiacrePackage.GENERIC_UNION_TAG_DECLARATION: return createGenericUnionTagDeclaration();
      case FiacrePackage.GENERIC_RECORD_FIELD_DECLARATION: return createGenericRecordFieldDeclaration();
      case FiacrePackage.STATE_DECLARATION: return createStateDeclaration();
      case FiacrePackage.TRANSITION: return createTransition();
      case FiacrePackage.COMPONENT_DECLARATION: return createComponentDeclaration();
      case FiacrePackage.PORTS_DECLARATION: return createPortsDeclaration();
      case FiacrePackage.PORT_DECLARATION: return createPortDeclaration();
      case FiacrePackage.LOCAL_PORTS_DECLARATION: return createLocalPortsDeclaration();
      case FiacrePackage.PARAMETERS_DECLARATION: return createParametersDeclaration();
      case FiacrePackage.PARAMETER_DECLARATION: return createParameterDeclaration();
      case FiacrePackage.VARIABLES_DECLARATION: return createVariablesDeclaration();
      case FiacrePackage.VARIABLE_DECLARATION: return createVariableDeclaration();
      case FiacrePackage.PRIORITY_DECLARATION: return createPriorityDeclaration();
      case FiacrePackage.PRIORITY_GROUP: return createPriorityGroup();
      case FiacrePackage.STATEMENT: return createStatement();
      case FiacrePackage.NULL_STATEMENT: return createNullStatement();
      case FiacrePackage.TAGGED_STATEMENT: return createTaggedStatement();
      case FiacrePackage.TAG_DECLARATION: return createTagDeclaration();
      case FiacrePackage.PATTERN_STATEMENT: return createPatternStatement();
      case FiacrePackage.PATTERN: return createPattern();
      case FiacrePackage.ANY_PATTERN: return createAnyPattern();
      case FiacrePackage.CONSTANT_PATTERN: return createConstantPattern();
      case FiacrePackage.INTEGER_PATTERN: return createIntegerPattern();
      case FiacrePackage.IDENTIFIER_PATTERN: return createIdentifierPattern();
      case FiacrePackage.CONDITIONAL_STATEMENT: return createConditionalStatement();
      case FiacrePackage.EXTENDED_CONDITIONAL_STATEMENT: return createExtendedConditionalStatement();
      case FiacrePackage.SELECT_STATEMENT: return createSelectStatement();
      case FiacrePackage.WHILE_STATEMENT: return createWhileStatement();
      case FiacrePackage.FOREACH_STATEMENT: return createForeachStatement();
      case FiacrePackage.TO_STATEMENT: return createToStatement();
      case FiacrePackage.CASE_STATEMENT: return createCaseStatement();
      case FiacrePackage.LOOP_STATEMENT: return createLoopStatement();
      case FiacrePackage.ON_STATEMENT: return createOnStatement();
      case FiacrePackage.WAIT_STATEMENT: return createWaitStatement();
      case FiacrePackage.COMPOSITION: return createComposition();
      case FiacrePackage.BLOCK: return createBlock();
      case FiacrePackage.COMPOSITE_BLOCK: return createCompositeBlock();
      case FiacrePackage.INSTANCE_DECLARATION: return createInstanceDeclaration();
      case FiacrePackage.PORT_SET: return createPortSet();
      case FiacrePackage.COMPONENT_INSTANCE: return createComponentInstance();
      case FiacrePackage.GENERIC_INSTANCE: return createGenericInstance();
      case FiacrePackage.TYPE_INSTANCE: return createTypeInstance();
      case FiacrePackage.CONSTANT_INSTANCE: return createConstantInstance();
      case FiacrePackage.UNION_TAG_INSTANCE: return createUnionTagInstance();
      case FiacrePackage.RECORD_FIELD_INSTANCE: return createRecordFieldInstance();
      case FiacrePackage.EXPRESSION: return createExpression();
      case FiacrePackage.REFERENCE_EXPRESSION: return createReferenceExpression();
      case FiacrePackage.IDENTIFIER_EXPRESSION: return createIdentifierExpression();
      case FiacrePackage.RECORD_EXPRESSION: return createRecordExpression();
      case FiacrePackage.FIELD_EXPRESSION: return createFieldExpression();
      case FiacrePackage.QUEUE_EXPRESSION: return createQueueExpression();
      case FiacrePackage.ENQUEUE_EXPRESSION: return createEnqueueExpression();
      case FiacrePackage.APPEND_EXPRESSION: return createAppendExpression();
      case FiacrePackage.LITERAL_EXPRESSION: return createLiteralExpression();
      case FiacrePackage.BOOLEAN_LITERAL: return createBooleanLiteral();
      case FiacrePackage.NATURAL_LITERAL: return createNaturalLiteral();
      case FiacrePackage.LOWER_BOUND: return createLowerBound();
      case FiacrePackage.UPPER_BOUND: return createUpperBound();
      case FiacrePackage.NATURAL_LOWER_BOUND: return createNaturalLowerBound();
      case FiacrePackage.NATURAL_UPPER_BOUND: return createNaturalUpperBound();
      case FiacrePackage.DECIMAL_LOWER_BOUND: return createDecimalLowerBound();
      case FiacrePackage.DECIMAL_UPPER_BOUND: return createDecimalUpperBound();
      case FiacrePackage.VARIABLE_LOWER_BOUND: return createVariableLowerBound();
      case FiacrePackage.VARIABLE_UPPER_BOUND: return createVariableUpperBound();
      case FiacrePackage.INFINITE_UPPER_BOUND: return createInfiniteUpperBound();
      case FiacrePackage.REQUIREMENT: return createRequirement();
      case FiacrePackage.PROPERTY_DECLARATION: return createPropertyDeclaration();
      case FiacrePackage.PROPERTY: return createProperty();
      case FiacrePackage.PATTERN_PROPERTY: return createPatternProperty();
      case FiacrePackage.LTL_PATTERN: return createLTLPattern();
      case FiacrePackage.DEADLOCK_FREE_PATTERN: return createDeadlockFreePattern();
      case FiacrePackage.INFINITELY_OFTEN_PATTERN: return createInfinitelyOftenPattern();
      case FiacrePackage.MORTAL_PATTERN: return createMortalPattern();
      case FiacrePackage.PRESENCE_PATTERN: return createPresencePattern();
      case FiacrePackage.ABSENCE_PATTERN: return createAbsencePattern();
      case FiacrePackage.ALWAYS_PATTERN: return createAlwaysPattern();
      case FiacrePackage.SEQUENCE_PATTERN: return createSequencePattern();
      case FiacrePackage.LTL_PROPERTY: return createLTLProperty();
      case FiacrePackage.STATE_EVENT: return createStateEvent();
      case FiacrePackage.ENTER_STATE_EVENT: return createEnterStateEvent();
      case FiacrePackage.LEAVE_STATE_EVENT: return createLeaveStateEvent();
      case FiacrePackage.OBSERVABLE: return createObservable();
      case FiacrePackage.OBSERVABLE_EVENT: return createObservableEvent();
      case FiacrePackage.PATH_EVENT: return createPathEvent();
      case FiacrePackage.PATH: return createPath();
      case FiacrePackage.PATH_ITEM: return createPathItem();
      case FiacrePackage.NATURAL_ITEM: return createNaturalItem();
      case FiacrePackage.NAMED_ITEM: return createNamedItem();
      case FiacrePackage.SUBJECT: return createSubject();
      case FiacrePackage.STATE_SUBJECT: return createStateSubject();
      case FiacrePackage.VALUE_SUBJECT: return createValueSubject();
      case FiacrePackage.TAG_SUBJECT: return createTagSubject();
      case FiacrePackage.EVENT_SUBJECT: return createEventSubject();
      case FiacrePackage.TUPLE_TYPE: return createTupleType();
      case FiacrePackage.NATURAL_TYPE: return createNaturalType();
      case FiacrePackage.INTEGER_TYPE: return createIntegerType();
      case FiacrePackage.BOOLEAN_TYPE: return createBooleanType();
      case FiacrePackage.UNLESS_STATEMENT: return createUnlessStatement();
      case FiacrePackage.STATEMENT_CHOICE: return createStatementChoice();
      case FiacrePackage.STATEMENT_SEQUENCE: return createStatementSequence();
      case FiacrePackage.ASSIGN_STATEMENT: return createAssignStatement();
      case FiacrePackage.SEND_STATEMENT: return createSendStatement();
      case FiacrePackage.RECEIVE_STATEMENT: return createReceiveStatement();
      case FiacrePackage.CONSTRUCTOR_PATTERN: return createConstructorPattern();
      case FiacrePackage.ARRAY_ACCESS_PATTERN: return createArrayAccessPattern();
      case FiacrePackage.RECORD_ACCESS_PATTERN: return createRecordAccessPattern();
      case FiacrePackage.ALL_EXPRESSION: return createAllExpression();
      case FiacrePackage.EXISTS_EXPRESSION: return createExistsExpression();
      case FiacrePackage.CONDITIONAL: return createConditional();
      case FiacrePackage.DISJUNCTION: return createDisjunction();
      case FiacrePackage.IMPLICATION: return createImplication();
      case FiacrePackage.CONJUNCTION: return createConjunction();
      case FiacrePackage.COMPARISON_EQUAL: return createComparisonEqual();
      case FiacrePackage.COMPARISON_NOT_EQUAL: return createComparisonNotEqual();
      case FiacrePackage.COMPARISON_LESSER: return createComparisonLesser();
      case FiacrePackage.COMPARISON_LESSER_EQUAL: return createComparisonLesserEqual();
      case FiacrePackage.COMPARISON_GREATER: return createComparisonGreater();
      case FiacrePackage.COMPARISON_GREATER_EQUAL: return createComparisonGreaterEqual();
      case FiacrePackage.ADDITION: return createAddition();
      case FiacrePackage.SUBSTRACTION: return createSubstraction();
      case FiacrePackage.MULTIPLICATION: return createMultiplication();
      case FiacrePackage.DIVISION: return createDivision();
      case FiacrePackage.MODULO: return createModulo();
      case FiacrePackage.UNARY_PLUS_EXPRESSION: return createUnaryPlusExpression();
      case FiacrePackage.UNARY_MINUS_EXPRESSION: return createUnaryMinusExpression();
      case FiacrePackage.UNARY_NEGATION_EXPRESSION: return createUnaryNegationExpression();
      case FiacrePackage.UNARY_FIRST_EXPRESSION: return createUnaryFirstExpression();
      case FiacrePackage.UNARY_LENGTH_EXPRESSION: return createUnaryLengthExpression();
      case FiacrePackage.UNARY_COERCE_EXPRESSION: return createUnaryCoerceExpression();
      case FiacrePackage.UNARY_FULL_EXPRESSION: return createUnaryFullExpression();
      case FiacrePackage.UNARY_DE_QUEUE_EXPRESSION: return createUnaryDeQueueExpression();
      case FiacrePackage.UNARY_EMPTY_EXPRESSION: return createUnaryEmptyExpression();
      case FiacrePackage.PROJECTION: return createProjection();
      case FiacrePackage.ARRAY_ACCESS_EXPRESSION: return createArrayAccessExpression();
      case FiacrePackage.RECORD_ACCESS_EXPRESSION: return createRecordAccessExpression();
      case FiacrePackage.CONSTRUCTION_EXPRESSION: return createConstructionExpression();
      case FiacrePackage.EXPLICIT_ARRAY_EXPRESSION: return createExplicitArrayExpression();
      case FiacrePackage.IMPLICIT_ARRAY_EXPRESSION: return createImplicitArrayExpression();
      case FiacrePackage.TRUE_LITERAL: return createTrueLiteral();
      case FiacrePackage.FALSE_LITERAL: return createFalseLiteral();
      case FiacrePackage.ALL_PROPERTY: return createAllProperty();
      case FiacrePackage.EXISTS_PROPERTY: return createExistsProperty();
      case FiacrePackage.PROPERTY_DISJUNCTION: return createPropertyDisjunction();
      case FiacrePackage.PROPERTY_IMPLICATION: return createPropertyImplication();
      case FiacrePackage.PROPERTY_CONJUNCTION: return createPropertyConjunction();
      case FiacrePackage.PROPERTY_NEGATION: return createPropertyNegation();
      case FiacrePackage.LEADS_TO_PATTERN: return createLeadsToPattern();
      case FiacrePackage.PRECEDES_PATTERN: return createPrecedesPattern();
      case FiacrePackage.LTL_ALL: return createLTLAll();
      case FiacrePackage.LTL_EXISTS: return createLTLExists();
      case FiacrePackage.LTL_DISJUNCTION: return createLTLDisjunction();
      case FiacrePackage.LTL_IMPLICATION: return createLTLImplication();
      case FiacrePackage.LTL_CONJUNCTION: return createLTLConjunction();
      case FiacrePackage.LTL_UNTIL: return createLTLUntil();
      case FiacrePackage.LTL_RELEASE: return createLTLRelease();
      case FiacrePackage.LTL_UNARY_NEGATION: return createLTLUnaryNegation();
      case FiacrePackage.LTL_UNARY_NEXT: return createLTLUnaryNext();
      case FiacrePackage.LTL_UNARY_ALWAYS: return createLTLUnaryAlways();
      case FiacrePackage.LTL_UNARY_EVENTUALLY: return createLTLUnaryEventually();
      case FiacrePackage.LTL_VARIABLE: return createLTLVariable();
      case FiacrePackage.OBSERVABLE_DISJUNCTION: return createObservableDisjunction();
      case FiacrePackage.OBSERVABLE_IMPLICATION: return createObservableImplication();
      case FiacrePackage.OBSERVABLE_CONJUNCTION: return createObservableConjunction();
      case FiacrePackage.OBSERVABLE_NEGATION: return createObservableNegation();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Model createModel()
  {
    ModelImpl model = new ModelImpl();
    return model;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RootDeclaration createRootDeclaration()
  {
    RootDeclarationImpl rootDeclaration = new RootDeclarationImpl();
    return rootDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NamedElement createNamedElement()
  {
    NamedElementImpl namedElement = new NamedElementImpl();
    return namedElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeDeclarationUse createTypeDeclarationUse()
  {
    TypeDeclarationUseImpl typeDeclarationUse = new TypeDeclarationUseImpl();
    return typeDeclarationUse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstantDeclarationUse createConstantDeclarationUse()
  {
    ConstantDeclarationUseImpl constantDeclarationUse = new ConstantDeclarationUseImpl();
    return constantDeclarationUse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExpressionDeclarationUse createExpressionDeclarationUse()
  {
    ExpressionDeclarationUseImpl expressionDeclarationUse = new ExpressionDeclarationUseImpl();
    return expressionDeclarationUse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReferenceDeclarationUse createReferenceDeclarationUse()
  {
    ReferenceDeclarationUseImpl referenceDeclarationUse = new ReferenceDeclarationUseImpl();
    return referenceDeclarationUse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnionTagDeclarationUse createUnionTagDeclarationUse()
  {
    UnionTagDeclarationUseImpl unionTagDeclarationUse = new UnionTagDeclarationUseImpl();
    return unionTagDeclarationUse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordFieldDeclarationUse createRecordFieldDeclarationUse()
  {
    RecordFieldDeclarationUseImpl recordFieldDeclarationUse = new RecordFieldDeclarationUseImpl();
    return recordFieldDeclarationUse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PatternDeclarationUse createPatternDeclarationUse()
  {
    PatternDeclarationUseImpl patternDeclarationUse = new PatternDeclarationUseImpl();
    return patternDeclarationUse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoundDeclarationUse createBoundDeclarationUse()
  {
    BoundDeclarationUseImpl boundDeclarationUse = new BoundDeclarationUseImpl();
    return boundDeclarationUse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PathDeclarationUse createPathDeclarationUse()
  {
    PathDeclarationUseImpl pathDeclarationUse = new PathDeclarationUseImpl();
    return pathDeclarationUse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportDeclaration createImportDeclaration()
  {
    ImportDeclarationImpl importDeclaration = new ImportDeclarationImpl();
    return importDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Declaration createDeclaration()
  {
    DeclarationImpl declaration = new DeclarationImpl();
    return declaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParameterizedDeclaration createParameterizedDeclaration()
  {
    ParameterizedDeclarationImpl parameterizedDeclaration = new ParameterizedDeclarationImpl();
    return parameterizedDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeDeclaration createTypeDeclaration()
  {
    TypeDeclarationImpl typeDeclaration = new TypeDeclarationImpl();
    return typeDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ChannelDeclaration createChannelDeclaration()
  {
    ChannelDeclarationImpl channelDeclaration = new ChannelDeclarationImpl();
    return channelDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ChannelType createChannelType()
  {
    ChannelTypeImpl channelType = new ChannelTypeImpl();
    return channelType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type createType()
  {
    TypeImpl type = new TypeImpl();
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BasicType createBasicType()
  {
    BasicTypeImpl basicType = new BasicTypeImpl();
    return basicType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RangeType createRangeType()
  {
    RangeTypeImpl rangeType = new RangeTypeImpl();
    return rangeType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnionType createUnionType()
  {
    UnionTypeImpl unionType = new UnionTypeImpl();
    return unionType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnionTags createUnionTags()
  {
    UnionTagsImpl unionTags = new UnionTagsImpl();
    return unionTags;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnionTagDeclaration createUnionTagDeclaration()
  {
    UnionTagDeclarationImpl unionTagDeclaration = new UnionTagDeclarationImpl();
    return unionTagDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordType createRecordType()
  {
    RecordTypeImpl recordType = new RecordTypeImpl();
    return recordType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordFields createRecordFields()
  {
    RecordFieldsImpl recordFields = new RecordFieldsImpl();
    return recordFields;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordFieldDeclaration createRecordFieldDeclaration()
  {
    RecordFieldDeclarationImpl recordFieldDeclaration = new RecordFieldDeclarationImpl();
    return recordFieldDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QueueType createQueueType()
  {
    QueueTypeImpl queueType = new QueueTypeImpl();
    return queueType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayType createArrayType()
  {
    ArrayTypeImpl arrayType = new ArrayTypeImpl();
    return arrayType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReferencedType createReferencedType()
  {
    ReferencedTypeImpl referencedType = new ReferencedTypeImpl();
    return referencedType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstantDeclaration createConstantDeclaration()
  {
    ConstantDeclarationImpl constantDeclaration = new ConstantDeclarationImpl();
    return constantDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProcessDeclaration createProcessDeclaration()
  {
    ProcessDeclarationImpl processDeclaration = new ProcessDeclarationImpl();
    return processDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericDeclaration createGenericDeclaration()
  {
    GenericDeclarationImpl genericDeclaration = new GenericDeclarationImpl();
    return genericDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericTypeDeclaration createGenericTypeDeclaration()
  {
    GenericTypeDeclarationImpl genericTypeDeclaration = new GenericTypeDeclarationImpl();
    return genericTypeDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericConstantDeclaration createGenericConstantDeclaration()
  {
    GenericConstantDeclarationImpl genericConstantDeclaration = new GenericConstantDeclarationImpl();
    return genericConstantDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericUnionTagDeclaration createGenericUnionTagDeclaration()
  {
    GenericUnionTagDeclarationImpl genericUnionTagDeclaration = new GenericUnionTagDeclarationImpl();
    return genericUnionTagDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericRecordFieldDeclaration createGenericRecordFieldDeclaration()
  {
    GenericRecordFieldDeclarationImpl genericRecordFieldDeclaration = new GenericRecordFieldDeclarationImpl();
    return genericRecordFieldDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StateDeclaration createStateDeclaration()
  {
    StateDeclarationImpl stateDeclaration = new StateDeclarationImpl();
    return stateDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Transition createTransition()
  {
    TransitionImpl transition = new TransitionImpl();
    return transition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentDeclaration createComponentDeclaration()
  {
    ComponentDeclarationImpl componentDeclaration = new ComponentDeclarationImpl();
    return componentDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortsDeclaration createPortsDeclaration()
  {
    PortsDeclarationImpl portsDeclaration = new PortsDeclarationImpl();
    return portsDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortDeclaration createPortDeclaration()
  {
    PortDeclarationImpl portDeclaration = new PortDeclarationImpl();
    return portDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LocalPortsDeclaration createLocalPortsDeclaration()
  {
    LocalPortsDeclarationImpl localPortsDeclaration = new LocalPortsDeclarationImpl();
    return localPortsDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParametersDeclaration createParametersDeclaration()
  {
    ParametersDeclarationImpl parametersDeclaration = new ParametersDeclarationImpl();
    return parametersDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParameterDeclaration createParameterDeclaration()
  {
    ParameterDeclarationImpl parameterDeclaration = new ParameterDeclarationImpl();
    return parameterDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariablesDeclaration createVariablesDeclaration()
  {
    VariablesDeclarationImpl variablesDeclaration = new VariablesDeclarationImpl();
    return variablesDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableDeclaration createVariableDeclaration()
  {
    VariableDeclarationImpl variableDeclaration = new VariableDeclarationImpl();
    return variableDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PriorityDeclaration createPriorityDeclaration()
  {
    PriorityDeclarationImpl priorityDeclaration = new PriorityDeclarationImpl();
    return priorityDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PriorityGroup createPriorityGroup()
  {
    PriorityGroupImpl priorityGroup = new PriorityGroupImpl();
    return priorityGroup;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement createStatement()
  {
    StatementImpl statement = new StatementImpl();
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NullStatement createNullStatement()
  {
    NullStatementImpl nullStatement = new NullStatementImpl();
    return nullStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TaggedStatement createTaggedStatement()
  {
    TaggedStatementImpl taggedStatement = new TaggedStatementImpl();
    return taggedStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TagDeclaration createTagDeclaration()
  {
    TagDeclarationImpl tagDeclaration = new TagDeclarationImpl();
    return tagDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PatternStatement createPatternStatement()
  {
    PatternStatementImpl patternStatement = new PatternStatementImpl();
    return patternStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Pattern createPattern()
  {
    PatternImpl pattern = new PatternImpl();
    return pattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AnyPattern createAnyPattern()
  {
    AnyPatternImpl anyPattern = new AnyPatternImpl();
    return anyPattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstantPattern createConstantPattern()
  {
    ConstantPatternImpl constantPattern = new ConstantPatternImpl();
    return constantPattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerPattern createIntegerPattern()
  {
    IntegerPatternImpl integerPattern = new IntegerPatternImpl();
    return integerPattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifierPattern createIdentifierPattern()
  {
    IdentifierPatternImpl identifierPattern = new IdentifierPatternImpl();
    return identifierPattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConditionalStatement createConditionalStatement()
  {
    ConditionalStatementImpl conditionalStatement = new ConditionalStatementImpl();
    return conditionalStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExtendedConditionalStatement createExtendedConditionalStatement()
  {
    ExtendedConditionalStatementImpl extendedConditionalStatement = new ExtendedConditionalStatementImpl();
    return extendedConditionalStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectStatement createSelectStatement()
  {
    SelectStatementImpl selectStatement = new SelectStatementImpl();
    return selectStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WhileStatement createWhileStatement()
  {
    WhileStatementImpl whileStatement = new WhileStatementImpl();
    return whileStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ForeachStatement createForeachStatement()
  {
    ForeachStatementImpl foreachStatement = new ForeachStatementImpl();
    return foreachStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ToStatement createToStatement()
  {
    ToStatementImpl toStatement = new ToStatementImpl();
    return toStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CaseStatement createCaseStatement()
  {
    CaseStatementImpl caseStatement = new CaseStatementImpl();
    return caseStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LoopStatement createLoopStatement()
  {
    LoopStatementImpl loopStatement = new LoopStatementImpl();
    return loopStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OnStatement createOnStatement()
  {
    OnStatementImpl onStatement = new OnStatementImpl();
    return onStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WaitStatement createWaitStatement()
  {
    WaitStatementImpl waitStatement = new WaitStatementImpl();
    return waitStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Composition createComposition()
  {
    CompositionImpl composition = new CompositionImpl();
    return composition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Block createBlock()
  {
    BlockImpl block = new BlockImpl();
    return block;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CompositeBlock createCompositeBlock()
  {
    CompositeBlockImpl compositeBlock = new CompositeBlockImpl();
    return compositeBlock;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InstanceDeclaration createInstanceDeclaration()
  {
    InstanceDeclarationImpl instanceDeclaration = new InstanceDeclarationImpl();
    return instanceDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortSet createPortSet()
  {
    PortSetImpl portSet = new PortSetImpl();
    return portSet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentInstance createComponentInstance()
  {
    ComponentInstanceImpl componentInstance = new ComponentInstanceImpl();
    return componentInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GenericInstance createGenericInstance()
  {
    GenericInstanceImpl genericInstance = new GenericInstanceImpl();
    return genericInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeInstance createTypeInstance()
  {
    TypeInstanceImpl typeInstance = new TypeInstanceImpl();
    return typeInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstantInstance createConstantInstance()
  {
    ConstantInstanceImpl constantInstance = new ConstantInstanceImpl();
    return constantInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnionTagInstance createUnionTagInstance()
  {
    UnionTagInstanceImpl unionTagInstance = new UnionTagInstanceImpl();
    return unionTagInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordFieldInstance createRecordFieldInstance()
  {
    RecordFieldInstanceImpl recordFieldInstance = new RecordFieldInstanceImpl();
    return recordFieldInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression createExpression()
  {
    ExpressionImpl expression = new ExpressionImpl();
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReferenceExpression createReferenceExpression()
  {
    ReferenceExpressionImpl referenceExpression = new ReferenceExpressionImpl();
    return referenceExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifierExpression createIdentifierExpression()
  {
    IdentifierExpressionImpl identifierExpression = new IdentifierExpressionImpl();
    return identifierExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordExpression createRecordExpression()
  {
    RecordExpressionImpl recordExpression = new RecordExpressionImpl();
    return recordExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldExpression createFieldExpression()
  {
    FieldExpressionImpl fieldExpression = new FieldExpressionImpl();
    return fieldExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QueueExpression createQueueExpression()
  {
    QueueExpressionImpl queueExpression = new QueueExpressionImpl();
    return queueExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnqueueExpression createEnqueueExpression()
  {
    EnqueueExpressionImpl enqueueExpression = new EnqueueExpressionImpl();
    return enqueueExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AppendExpression createAppendExpression()
  {
    AppendExpressionImpl appendExpression = new AppendExpressionImpl();
    return appendExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LiteralExpression createLiteralExpression()
  {
    LiteralExpressionImpl literalExpression = new LiteralExpressionImpl();
    return literalExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanLiteral createBooleanLiteral()
  {
    BooleanLiteralImpl booleanLiteral = new BooleanLiteralImpl();
    return booleanLiteral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NaturalLiteral createNaturalLiteral()
  {
    NaturalLiteralImpl naturalLiteral = new NaturalLiteralImpl();
    return naturalLiteral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LowerBound createLowerBound()
  {
    LowerBoundImpl lowerBound = new LowerBoundImpl();
    return lowerBound;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UpperBound createUpperBound()
  {
    UpperBoundImpl upperBound = new UpperBoundImpl();
    return upperBound;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NaturalLowerBound createNaturalLowerBound()
  {
    NaturalLowerBoundImpl naturalLowerBound = new NaturalLowerBoundImpl();
    return naturalLowerBound;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NaturalUpperBound createNaturalUpperBound()
  {
    NaturalUpperBoundImpl naturalUpperBound = new NaturalUpperBoundImpl();
    return naturalUpperBound;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DecimalLowerBound createDecimalLowerBound()
  {
    DecimalLowerBoundImpl decimalLowerBound = new DecimalLowerBoundImpl();
    return decimalLowerBound;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DecimalUpperBound createDecimalUpperBound()
  {
    DecimalUpperBoundImpl decimalUpperBound = new DecimalUpperBoundImpl();
    return decimalUpperBound;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableLowerBound createVariableLowerBound()
  {
    VariableLowerBoundImpl variableLowerBound = new VariableLowerBoundImpl();
    return variableLowerBound;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableUpperBound createVariableUpperBound()
  {
    VariableUpperBoundImpl variableUpperBound = new VariableUpperBoundImpl();
    return variableUpperBound;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InfiniteUpperBound createInfiniteUpperBound()
  {
    InfiniteUpperBoundImpl infiniteUpperBound = new InfiniteUpperBoundImpl();
    return infiniteUpperBound;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Requirement createRequirement()
  {
    RequirementImpl requirement = new RequirementImpl();
    return requirement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PropertyDeclaration createPropertyDeclaration()
  {
    PropertyDeclarationImpl propertyDeclaration = new PropertyDeclarationImpl();
    return propertyDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Property createProperty()
  {
    PropertyImpl property = new PropertyImpl();
    return property;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PatternProperty createPatternProperty()
  {
    PatternPropertyImpl patternProperty = new PatternPropertyImpl();
    return patternProperty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLPattern createLTLPattern()
  {
    LTLPatternImpl ltlPattern = new LTLPatternImpl();
    return ltlPattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DeadlockFreePattern createDeadlockFreePattern()
  {
    DeadlockFreePatternImpl deadlockFreePattern = new DeadlockFreePatternImpl();
    return deadlockFreePattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InfinitelyOftenPattern createInfinitelyOftenPattern()
  {
    InfinitelyOftenPatternImpl infinitelyOftenPattern = new InfinitelyOftenPatternImpl();
    return infinitelyOftenPattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MortalPattern createMortalPattern()
  {
    MortalPatternImpl mortalPattern = new MortalPatternImpl();
    return mortalPattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PresencePattern createPresencePattern()
  {
    PresencePatternImpl presencePattern = new PresencePatternImpl();
    return presencePattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbsencePattern createAbsencePattern()
  {
    AbsencePatternImpl absencePattern = new AbsencePatternImpl();
    return absencePattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AlwaysPattern createAlwaysPattern()
  {
    AlwaysPatternImpl alwaysPattern = new AlwaysPatternImpl();
    return alwaysPattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SequencePattern createSequencePattern()
  {
    SequencePatternImpl sequencePattern = new SequencePatternImpl();
    return sequencePattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLProperty createLTLProperty()
  {
    LTLPropertyImpl ltlProperty = new LTLPropertyImpl();
    return ltlProperty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StateEvent createStateEvent()
  {
    StateEventImpl stateEvent = new StateEventImpl();
    return stateEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnterStateEvent createEnterStateEvent()
  {
    EnterStateEventImpl enterStateEvent = new EnterStateEventImpl();
    return enterStateEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LeaveStateEvent createLeaveStateEvent()
  {
    LeaveStateEventImpl leaveStateEvent = new LeaveStateEventImpl();
    return leaveStateEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Observable createObservable()
  {
    ObservableImpl observable = new ObservableImpl();
    return observable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ObservableEvent createObservableEvent()
  {
    ObservableEventImpl observableEvent = new ObservableEventImpl();
    return observableEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PathEvent createPathEvent()
  {
    PathEventImpl pathEvent = new PathEventImpl();
    return pathEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Path createPath()
  {
    PathImpl path = new PathImpl();
    return path;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PathItem createPathItem()
  {
    PathItemImpl pathItem = new PathItemImpl();
    return pathItem;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NaturalItem createNaturalItem()
  {
    NaturalItemImpl naturalItem = new NaturalItemImpl();
    return naturalItem;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NamedItem createNamedItem()
  {
    NamedItemImpl namedItem = new NamedItemImpl();
    return namedItem;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Subject createSubject()
  {
    SubjectImpl subject = new SubjectImpl();
    return subject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StateSubject createStateSubject()
  {
    StateSubjectImpl stateSubject = new StateSubjectImpl();
    return stateSubject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ValueSubject createValueSubject()
  {
    ValueSubjectImpl valueSubject = new ValueSubjectImpl();
    return valueSubject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TagSubject createTagSubject()
  {
    TagSubjectImpl tagSubject = new TagSubjectImpl();
    return tagSubject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EventSubject createEventSubject()
  {
    EventSubjectImpl eventSubject = new EventSubjectImpl();
    return eventSubject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TupleType createTupleType()
  {
    TupleTypeImpl tupleType = new TupleTypeImpl();
    return tupleType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NaturalType createNaturalType()
  {
    NaturalTypeImpl naturalType = new NaturalTypeImpl();
    return naturalType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerType createIntegerType()
  {
    IntegerTypeImpl integerType = new IntegerTypeImpl();
    return integerType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanType createBooleanType()
  {
    BooleanTypeImpl booleanType = new BooleanTypeImpl();
    return booleanType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnlessStatement createUnlessStatement()
  {
    UnlessStatementImpl unlessStatement = new UnlessStatementImpl();
    return unlessStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementChoice createStatementChoice()
  {
    StatementChoiceImpl statementChoice = new StatementChoiceImpl();
    return statementChoice;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementSequence createStatementSequence()
  {
    StatementSequenceImpl statementSequence = new StatementSequenceImpl();
    return statementSequence;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AssignStatement createAssignStatement()
  {
    AssignStatementImpl assignStatement = new AssignStatementImpl();
    return assignStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SendStatement createSendStatement()
  {
    SendStatementImpl sendStatement = new SendStatementImpl();
    return sendStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReceiveStatement createReceiveStatement()
  {
    ReceiveStatementImpl receiveStatement = new ReceiveStatementImpl();
    return receiveStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstructorPattern createConstructorPattern()
  {
    ConstructorPatternImpl constructorPattern = new ConstructorPatternImpl();
    return constructorPattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayAccessPattern createArrayAccessPattern()
  {
    ArrayAccessPatternImpl arrayAccessPattern = new ArrayAccessPatternImpl();
    return arrayAccessPattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordAccessPattern createRecordAccessPattern()
  {
    RecordAccessPatternImpl recordAccessPattern = new RecordAccessPatternImpl();
    return recordAccessPattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllExpression createAllExpression()
  {
    AllExpressionImpl allExpression = new AllExpressionImpl();
    return allExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExistsExpression createExistsExpression()
  {
    ExistsExpressionImpl existsExpression = new ExistsExpressionImpl();
    return existsExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Conditional createConditional()
  {
    ConditionalImpl conditional = new ConditionalImpl();
    return conditional;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Disjunction createDisjunction()
  {
    DisjunctionImpl disjunction = new DisjunctionImpl();
    return disjunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Implication createImplication()
  {
    ImplicationImpl implication = new ImplicationImpl();
    return implication;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Conjunction createConjunction()
  {
    ConjunctionImpl conjunction = new ConjunctionImpl();
    return conjunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComparisonEqual createComparisonEqual()
  {
    ComparisonEqualImpl comparisonEqual = new ComparisonEqualImpl();
    return comparisonEqual;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComparisonNotEqual createComparisonNotEqual()
  {
    ComparisonNotEqualImpl comparisonNotEqual = new ComparisonNotEqualImpl();
    return comparisonNotEqual;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComparisonLesser createComparisonLesser()
  {
    ComparisonLesserImpl comparisonLesser = new ComparisonLesserImpl();
    return comparisonLesser;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComparisonLesserEqual createComparisonLesserEqual()
  {
    ComparisonLesserEqualImpl comparisonLesserEqual = new ComparisonLesserEqualImpl();
    return comparisonLesserEqual;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComparisonGreater createComparisonGreater()
  {
    ComparisonGreaterImpl comparisonGreater = new ComparisonGreaterImpl();
    return comparisonGreater;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComparisonGreaterEqual createComparisonGreaterEqual()
  {
    ComparisonGreaterEqualImpl comparisonGreaterEqual = new ComparisonGreaterEqualImpl();
    return comparisonGreaterEqual;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Addition createAddition()
  {
    AdditionImpl addition = new AdditionImpl();
    return addition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Substraction createSubstraction()
  {
    SubstractionImpl substraction = new SubstractionImpl();
    return substraction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Multiplication createMultiplication()
  {
    MultiplicationImpl multiplication = new MultiplicationImpl();
    return multiplication;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Division createDivision()
  {
    DivisionImpl division = new DivisionImpl();
    return division;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Modulo createModulo()
  {
    ModuloImpl modulo = new ModuloImpl();
    return modulo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnaryPlusExpression createUnaryPlusExpression()
  {
    UnaryPlusExpressionImpl unaryPlusExpression = new UnaryPlusExpressionImpl();
    return unaryPlusExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnaryMinusExpression createUnaryMinusExpression()
  {
    UnaryMinusExpressionImpl unaryMinusExpression = new UnaryMinusExpressionImpl();
    return unaryMinusExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnaryNegationExpression createUnaryNegationExpression()
  {
    UnaryNegationExpressionImpl unaryNegationExpression = new UnaryNegationExpressionImpl();
    return unaryNegationExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnaryFirstExpression createUnaryFirstExpression()
  {
    UnaryFirstExpressionImpl unaryFirstExpression = new UnaryFirstExpressionImpl();
    return unaryFirstExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnaryLengthExpression createUnaryLengthExpression()
  {
    UnaryLengthExpressionImpl unaryLengthExpression = new UnaryLengthExpressionImpl();
    return unaryLengthExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnaryCoerceExpression createUnaryCoerceExpression()
  {
    UnaryCoerceExpressionImpl unaryCoerceExpression = new UnaryCoerceExpressionImpl();
    return unaryCoerceExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnaryFullExpression createUnaryFullExpression()
  {
    UnaryFullExpressionImpl unaryFullExpression = new UnaryFullExpressionImpl();
    return unaryFullExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnaryDeQueueExpression createUnaryDeQueueExpression()
  {
    UnaryDeQueueExpressionImpl unaryDeQueueExpression = new UnaryDeQueueExpressionImpl();
    return unaryDeQueueExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnaryEmptyExpression createUnaryEmptyExpression()
  {
    UnaryEmptyExpressionImpl unaryEmptyExpression = new UnaryEmptyExpressionImpl();
    return unaryEmptyExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Projection createProjection()
  {
    ProjectionImpl projection = new ProjectionImpl();
    return projection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayAccessExpression createArrayAccessExpression()
  {
    ArrayAccessExpressionImpl arrayAccessExpression = new ArrayAccessExpressionImpl();
    return arrayAccessExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordAccessExpression createRecordAccessExpression()
  {
    RecordAccessExpressionImpl recordAccessExpression = new RecordAccessExpressionImpl();
    return recordAccessExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstructionExpression createConstructionExpression()
  {
    ConstructionExpressionImpl constructionExpression = new ConstructionExpressionImpl();
    return constructionExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExplicitArrayExpression createExplicitArrayExpression()
  {
    ExplicitArrayExpressionImpl explicitArrayExpression = new ExplicitArrayExpressionImpl();
    return explicitArrayExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImplicitArrayExpression createImplicitArrayExpression()
  {
    ImplicitArrayExpressionImpl implicitArrayExpression = new ImplicitArrayExpressionImpl();
    return implicitArrayExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TrueLiteral createTrueLiteral()
  {
    TrueLiteralImpl trueLiteral = new TrueLiteralImpl();
    return trueLiteral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FalseLiteral createFalseLiteral()
  {
    FalseLiteralImpl falseLiteral = new FalseLiteralImpl();
    return falseLiteral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllProperty createAllProperty()
  {
    AllPropertyImpl allProperty = new AllPropertyImpl();
    return allProperty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExistsProperty createExistsProperty()
  {
    ExistsPropertyImpl existsProperty = new ExistsPropertyImpl();
    return existsProperty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PropertyDisjunction createPropertyDisjunction()
  {
    PropertyDisjunctionImpl propertyDisjunction = new PropertyDisjunctionImpl();
    return propertyDisjunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PropertyImplication createPropertyImplication()
  {
    PropertyImplicationImpl propertyImplication = new PropertyImplicationImpl();
    return propertyImplication;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PropertyConjunction createPropertyConjunction()
  {
    PropertyConjunctionImpl propertyConjunction = new PropertyConjunctionImpl();
    return propertyConjunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PropertyNegation createPropertyNegation()
  {
    PropertyNegationImpl propertyNegation = new PropertyNegationImpl();
    return propertyNegation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LeadsToPattern createLeadsToPattern()
  {
    LeadsToPatternImpl leadsToPattern = new LeadsToPatternImpl();
    return leadsToPattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PrecedesPattern createPrecedesPattern()
  {
    PrecedesPatternImpl precedesPattern = new PrecedesPatternImpl();
    return precedesPattern;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLAll createLTLAll()
  {
    LTLAllImpl ltlAll = new LTLAllImpl();
    return ltlAll;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLExists createLTLExists()
  {
    LTLExistsImpl ltlExists = new LTLExistsImpl();
    return ltlExists;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLDisjunction createLTLDisjunction()
  {
    LTLDisjunctionImpl ltlDisjunction = new LTLDisjunctionImpl();
    return ltlDisjunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLImplication createLTLImplication()
  {
    LTLImplicationImpl ltlImplication = new LTLImplicationImpl();
    return ltlImplication;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLConjunction createLTLConjunction()
  {
    LTLConjunctionImpl ltlConjunction = new LTLConjunctionImpl();
    return ltlConjunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLUntil createLTLUntil()
  {
    LTLUntilImpl ltlUntil = new LTLUntilImpl();
    return ltlUntil;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLRelease createLTLRelease()
  {
    LTLReleaseImpl ltlRelease = new LTLReleaseImpl();
    return ltlRelease;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLUnaryNegation createLTLUnaryNegation()
  {
    LTLUnaryNegationImpl ltlUnaryNegation = new LTLUnaryNegationImpl();
    return ltlUnaryNegation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLUnaryNext createLTLUnaryNext()
  {
    LTLUnaryNextImpl ltlUnaryNext = new LTLUnaryNextImpl();
    return ltlUnaryNext;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLUnaryAlways createLTLUnaryAlways()
  {
    LTLUnaryAlwaysImpl ltlUnaryAlways = new LTLUnaryAlwaysImpl();
    return ltlUnaryAlways;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLUnaryEventually createLTLUnaryEventually()
  {
    LTLUnaryEventuallyImpl ltlUnaryEventually = new LTLUnaryEventuallyImpl();
    return ltlUnaryEventually;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLVariable createLTLVariable()
  {
    LTLVariableImpl ltlVariable = new LTLVariableImpl();
    return ltlVariable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ObservableDisjunction createObservableDisjunction()
  {
    ObservableDisjunctionImpl observableDisjunction = new ObservableDisjunctionImpl();
    return observableDisjunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ObservableImplication createObservableImplication()
  {
    ObservableImplicationImpl observableImplication = new ObservableImplicationImpl();
    return observableImplication;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ObservableConjunction createObservableConjunction()
  {
    ObservableConjunctionImpl observableConjunction = new ObservableConjunctionImpl();
    return observableConjunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ObservableNegation createObservableNegation()
  {
    ObservableNegationImpl observableNegation = new ObservableNegationImpl();
    return observableNegation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FiacrePackage getFiacrePackage()
  {
    return (FiacrePackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static FiacrePackage getPackage()
  {
    return FiacrePackage.eINSTANCE;
  }

} //FiacreFactoryImpl
