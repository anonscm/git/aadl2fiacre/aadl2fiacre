/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Named Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.NamedItem#getDeclaration <em>Declaration</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getNamedItem()
 * @model
 * @generated
 */
public interface NamedItem extends PathItem
{
  /**
   * Returns the value of the '<em><b>Declaration</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Declaration</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Declaration</em>' reference.
   * @see #setDeclaration(PathDeclarationUse)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getNamedItem_Declaration()
   * @model
   * @generated
   */
  PathDeclarationUse getDeclaration();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.NamedItem#getDeclaration <em>Declaration</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Declaration</em>' reference.
   * @see #getDeclaration()
   * @generated
   */
  void setDeclaration(PathDeclarationUse value);

} // NamedItem
