/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.DeadlockFreePattern;
import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deadlock Free Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DeadlockFreePatternImpl extends PatternPropertyImpl implements DeadlockFreePattern
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DeadlockFreePatternImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FiacrePackage.eINSTANCE.getDeadlockFreePattern();
  }

} //DeadlockFreePatternImpl
