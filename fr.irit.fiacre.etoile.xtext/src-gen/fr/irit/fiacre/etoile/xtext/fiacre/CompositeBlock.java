/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.CompositeBlock#getLocal <em>Local</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.CompositeBlock#getComposition <em>Composition</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getCompositeBlock()
 * @model
 * @generated
 */
public interface CompositeBlock extends Block
{
  /**
   * Returns the value of the '<em><b>Local</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Local</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Local</em>' containment reference.
   * @see #setLocal(PortSet)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getCompositeBlock_Local()
   * @model containment="true"
   * @generated
   */
  PortSet getLocal();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.CompositeBlock#getLocal <em>Local</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Local</em>' containment reference.
   * @see #getLocal()
   * @generated
   */
  void setLocal(PortSet value);

  /**
   * Returns the value of the '<em><b>Composition</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Composition</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Composition</em>' containment reference.
   * @see #setComposition(Composition)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getCompositeBlock_Composition()
   * @model containment="true"
   * @generated
   */
  Composition getComposition();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.CompositeBlock#getComposition <em>Composition</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Composition</em>' containment reference.
   * @see #getComposition()
   * @generated
   */
  void setComposition(Composition value);

} // CompositeBlock
