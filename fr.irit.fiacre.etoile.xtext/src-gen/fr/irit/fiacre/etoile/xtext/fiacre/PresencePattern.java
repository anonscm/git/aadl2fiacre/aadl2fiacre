/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Presence Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getSubject <em>Subject</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getLasting <em>Lasting</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getAfter <em>After</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getLower <em>Lower</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getUpper <em>Upper</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getUntil <em>Until</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getBefore <em>Before</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getMin <em>Min</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPresencePattern()
 * @model
 * @generated
 */
public interface PresencePattern extends PatternProperty
{
  /**
   * Returns the value of the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subject</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subject</em>' containment reference.
   * @see #setSubject(Observable)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPresencePattern_Subject()
   * @model containment="true"
   * @generated
   */
  Observable getSubject();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getSubject <em>Subject</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Subject</em>' containment reference.
   * @see #getSubject()
   * @generated
   */
  void setSubject(Observable value);

  /**
   * Returns the value of the '<em><b>Lasting</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Lasting</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Lasting</em>' attribute.
   * @see #setLasting(int)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPresencePattern_Lasting()
   * @model
   * @generated
   */
  int getLasting();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getLasting <em>Lasting</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Lasting</em>' attribute.
   * @see #getLasting()
   * @generated
   */
  void setLasting(int value);

  /**
   * Returns the value of the '<em><b>After</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>After</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>After</em>' containment reference.
   * @see #setAfter(Observable)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPresencePattern_After()
   * @model containment="true"
   * @generated
   */
  Observable getAfter();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getAfter <em>After</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>After</em>' containment reference.
   * @see #getAfter()
   * @generated
   */
  void setAfter(Observable value);

  /**
   * Returns the value of the '<em><b>Lower</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Lower</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Lower</em>' containment reference.
   * @see #setLower(LowerBound)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPresencePattern_Lower()
   * @model containment="true"
   * @generated
   */
  LowerBound getLower();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getLower <em>Lower</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Lower</em>' containment reference.
   * @see #getLower()
   * @generated
   */
  void setLower(LowerBound value);

  /**
   * Returns the value of the '<em><b>Upper</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Upper</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Upper</em>' containment reference.
   * @see #setUpper(UpperBound)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPresencePattern_Upper()
   * @model containment="true"
   * @generated
   */
  UpperBound getUpper();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getUpper <em>Upper</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Upper</em>' containment reference.
   * @see #getUpper()
   * @generated
   */
  void setUpper(UpperBound value);

  /**
   * Returns the value of the '<em><b>Until</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Until</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Until</em>' containment reference.
   * @see #setUntil(Observable)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPresencePattern_Until()
   * @model containment="true"
   * @generated
   */
  Observable getUntil();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getUntil <em>Until</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Until</em>' containment reference.
   * @see #getUntil()
   * @generated
   */
  void setUntil(Observable value);

  /**
   * Returns the value of the '<em><b>Before</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Before</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Before</em>' containment reference.
   * @see #setBefore(Observable)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPresencePattern_Before()
   * @model containment="true"
   * @generated
   */
  Observable getBefore();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getBefore <em>Before</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Before</em>' containment reference.
   * @see #getBefore()
   * @generated
   */
  void setBefore(Observable value);

  /**
   * Returns the value of the '<em><b>Min</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Min</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Min</em>' containment reference.
   * @see #setMin(Observable)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPresencePattern_Min()
   * @model containment="true"
   * @generated
   */
  Observable getMin();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getMin <em>Min</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Min</em>' containment reference.
   * @see #getMin()
   * @generated
   */
  void setMin(Observable value);

  /**
   * Returns the value of the '<em><b>Max</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Max</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Max</em>' containment reference.
   * @see #setMax(Observable)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPresencePattern_Max()
   * @model containment="true"
   * @generated
   */
  Observable getMax();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern#getMax <em>Max</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Max</em>' containment reference.
   * @see #getMax()
   * @generated
   */
  void setMax(Observable value);

} // PresencePattern
