/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Precedes Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getSubject <em>Subject</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getFollower <em>Follower</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getBefore <em>Before</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getAfter <em>After</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getUntil <em>Until</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getMin <em>Min</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPrecedesPattern()
 * @model
 * @generated
 */
public interface PrecedesPattern extends SequencePattern
{
  /**
   * Returns the value of the '<em><b>Subject</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subject</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subject</em>' containment reference.
   * @see #setSubject(Observable)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPrecedesPattern_Subject()
   * @model containment="true"
   * @generated
   */
  Observable getSubject();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getSubject <em>Subject</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Subject</em>' containment reference.
   * @see #getSubject()
   * @generated
   */
  void setSubject(Observable value);

  /**
   * Returns the value of the '<em><b>Follower</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Follower</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Follower</em>' containment reference.
   * @see #setFollower(Observable)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPrecedesPattern_Follower()
   * @model containment="true"
   * @generated
   */
  Observable getFollower();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getFollower <em>Follower</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Follower</em>' containment reference.
   * @see #getFollower()
   * @generated
   */
  void setFollower(Observable value);

  /**
   * Returns the value of the '<em><b>Before</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Before</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Before</em>' containment reference.
   * @see #setBefore(Observable)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPrecedesPattern_Before()
   * @model containment="true"
   * @generated
   */
  Observable getBefore();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getBefore <em>Before</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Before</em>' containment reference.
   * @see #getBefore()
   * @generated
   */
  void setBefore(Observable value);

  /**
   * Returns the value of the '<em><b>After</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>After</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>After</em>' containment reference.
   * @see #setAfter(Observable)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPrecedesPattern_After()
   * @model containment="true"
   * @generated
   */
  Observable getAfter();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getAfter <em>After</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>After</em>' containment reference.
   * @see #getAfter()
   * @generated
   */
  void setAfter(Observable value);

  /**
   * Returns the value of the '<em><b>Until</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Until</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Until</em>' containment reference.
   * @see #setUntil(Observable)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPrecedesPattern_Until()
   * @model containment="true"
   * @generated
   */
  Observable getUntil();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getUntil <em>Until</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Until</em>' containment reference.
   * @see #getUntil()
   * @generated
   */
  void setUntil(Observable value);

  /**
   * Returns the value of the '<em><b>Min</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Min</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Min</em>' containment reference.
   * @see #setMin(Observable)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPrecedesPattern_Min()
   * @model containment="true"
   * @generated
   */
  Observable getMin();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getMin <em>Min</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Min</em>' containment reference.
   * @see #getMin()
   * @generated
   */
  void setMin(Observable value);

  /**
   * Returns the value of the '<em><b>Max</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Max</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Max</em>' containment reference.
   * @see #setMax(Observable)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPrecedesPattern_Max()
   * @model containment="true"
   * @generated
   */
  Observable getMax();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern#getMax <em>Max</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Max</em>' containment reference.
   * @see #getMax()
   * @generated
   */
  void setMax(Observable value);

} // PrecedesPattern
