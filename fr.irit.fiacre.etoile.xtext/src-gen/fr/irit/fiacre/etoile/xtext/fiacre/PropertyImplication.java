/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property Implication</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyImplication#getLeft <em>Left</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyImplication#getRight <em>Right</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPropertyImplication()
 * @model
 * @generated
 */
public interface PropertyImplication extends Property
{
  /**
   * Returns the value of the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left</em>' containment reference.
   * @see #setLeft(Property)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPropertyImplication_Left()
   * @model containment="true"
   * @generated
   */
  Property getLeft();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyImplication#getLeft <em>Left</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Left</em>' containment reference.
   * @see #getLeft()
   * @generated
   */
  void setLeft(Property value);

  /**
   * Returns the value of the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Right</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Right</em>' containment reference.
   * @see #setRight(Property)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPropertyImplication_Right()
   * @model containment="true"
   * @generated
   */
  Property getRight();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyImplication#getRight <em>Right</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Right</em>' containment reference.
   * @see #getRight()
   * @generated
   */
  void setRight(Property value);

} // PropertyImplication
