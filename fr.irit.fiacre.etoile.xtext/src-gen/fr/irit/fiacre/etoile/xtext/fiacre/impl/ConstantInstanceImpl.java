/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclarationUse;
import fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance;
import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;
import fr.irit.fiacre.etoile.xtext.fiacre.LiteralExpression;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constant Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstantInstanceImpl#getConst <em>Const</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConstantInstanceImpl#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConstantInstanceImpl extends GenericInstanceImpl implements ConstantInstance
{
  /**
   * The cached value of the '{@link #getConst() <em>Const</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConst()
   * @generated
   * @ordered
   */
  protected ConstantDeclarationUse const_;

  /**
   * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected LiteralExpression value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ConstantInstanceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FiacrePackage.eINSTANCE.getConstantInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstantDeclarationUse getConst()
  {
    if (const_ != null && const_.eIsProxy())
    {
      InternalEObject oldConst = (InternalEObject)const_;
      const_ = (ConstantDeclarationUse)eResolveProxy(oldConst);
      if (const_ != oldConst)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, FiacrePackage.CONSTANT_INSTANCE__CONST, oldConst, const_));
      }
    }
    return const_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstantDeclarationUse basicGetConst()
  {
    return const_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConst(ConstantDeclarationUse newConst)
  {
    ConstantDeclarationUse oldConst = const_;
    const_ = newConst;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.CONSTANT_INSTANCE__CONST, oldConst, const_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LiteralExpression getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetValue(LiteralExpression newValue, NotificationChain msgs)
  {
    LiteralExpression oldValue = value;
    value = newValue;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.CONSTANT_INSTANCE__VALUE, oldValue, newValue);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValue(LiteralExpression newValue)
  {
    if (newValue != value)
    {
      NotificationChain msgs = null;
      if (value != null)
        msgs = ((InternalEObject)value).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.CONSTANT_INSTANCE__VALUE, null, msgs);
      if (newValue != null)
        msgs = ((InternalEObject)newValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.CONSTANT_INSTANCE__VALUE, null, msgs);
      msgs = basicSetValue(newValue, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.CONSTANT_INSTANCE__VALUE, newValue, newValue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case FiacrePackage.CONSTANT_INSTANCE__VALUE:
        return basicSetValue(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case FiacrePackage.CONSTANT_INSTANCE__CONST:
        if (resolve) return getConst();
        return basicGetConst();
      case FiacrePackage.CONSTANT_INSTANCE__VALUE:
        return getValue();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case FiacrePackage.CONSTANT_INSTANCE__CONST:
        setConst((ConstantDeclarationUse)newValue);
        return;
      case FiacrePackage.CONSTANT_INSTANCE__VALUE:
        setValue((LiteralExpression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.CONSTANT_INSTANCE__CONST:
        setConst((ConstantDeclarationUse)null);
        return;
      case FiacrePackage.CONSTANT_INSTANCE__VALUE:
        setValue((LiteralExpression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.CONSTANT_INSTANCE__CONST:
        return const_ != null;
      case FiacrePackage.CONSTANT_INSTANCE__VALUE:
        return value != null;
    }
    return super.eIsSet(featureID);
  }

} //ConstantInstanceImpl
