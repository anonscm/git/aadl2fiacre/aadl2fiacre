/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LTL Unary Eventually</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryEventually#getOperand <em>Operand</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLTLUnaryEventually()
 * @model
 * @generated
 */
public interface LTLUnaryEventually extends LTLProperty
{
  /**
   * Returns the value of the '<em><b>Operand</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Operand</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Operand</em>' containment reference.
   * @see #setOperand(LTLProperty)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLTLUnaryEventually_Operand()
   * @model containment="true"
   * @generated
   */
  LTLProperty getOperand();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryEventually#getOperand <em>Operand</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Operand</em>' containment reference.
   * @see #getOperand()
   * @generated
   */
  void setOperand(LTLProperty value);

} // LTLUnaryEventually
