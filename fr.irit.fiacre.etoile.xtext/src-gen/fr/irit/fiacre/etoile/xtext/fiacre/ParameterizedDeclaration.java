/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameterized Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getParameterizedDeclaration()
 * @model
 * @generated
 */
public interface ParameterizedDeclaration extends Declaration
{
} // ParameterizedDeclaration
