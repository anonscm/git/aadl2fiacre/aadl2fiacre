/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getGenericDeclaration()
 * @model
 * @generated
 */
public interface GenericDeclaration extends NamedElement, ExpressionDeclarationUse
{
} // GenericDeclaration
