/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.util;

import fr.irit.fiacre.etoile.xtext.fiacre.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage
 * @generated
 */
public class FiacreSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static FiacrePackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FiacreSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = FiacrePackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case FiacrePackage.MODEL:
      {
        Model model = (Model)theEObject;
        T result = caseModel(model);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ROOT_DECLARATION:
      {
        RootDeclaration rootDeclaration = (RootDeclaration)theEObject;
        T result = caseRootDeclaration(rootDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.NAMED_ELEMENT:
      {
        NamedElement namedElement = (NamedElement)theEObject;
        T result = caseNamedElement(namedElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.TYPE_DECLARATION_USE:
      {
        TypeDeclarationUse typeDeclarationUse = (TypeDeclarationUse)theEObject;
        T result = caseTypeDeclarationUse(typeDeclarationUse);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.CONSTANT_DECLARATION_USE:
      {
        ConstantDeclarationUse constantDeclarationUse = (ConstantDeclarationUse)theEObject;
        T result = caseConstantDeclarationUse(constantDeclarationUse);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.EXPRESSION_DECLARATION_USE:
      {
        ExpressionDeclarationUse expressionDeclarationUse = (ExpressionDeclarationUse)theEObject;
        T result = caseExpressionDeclarationUse(expressionDeclarationUse);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.REFERENCE_DECLARATION_USE:
      {
        ReferenceDeclarationUse referenceDeclarationUse = (ReferenceDeclarationUse)theEObject;
        T result = caseReferenceDeclarationUse(referenceDeclarationUse);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNION_TAG_DECLARATION_USE:
      {
        UnionTagDeclarationUse unionTagDeclarationUse = (UnionTagDeclarationUse)theEObject;
        T result = caseUnionTagDeclarationUse(unionTagDeclarationUse);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.RECORD_FIELD_DECLARATION_USE:
      {
        RecordFieldDeclarationUse recordFieldDeclarationUse = (RecordFieldDeclarationUse)theEObject;
        T result = caseRecordFieldDeclarationUse(recordFieldDeclarationUse);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PATTERN_DECLARATION_USE:
      {
        PatternDeclarationUse patternDeclarationUse = (PatternDeclarationUse)theEObject;
        T result = casePatternDeclarationUse(patternDeclarationUse);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.BOUND_DECLARATION_USE:
      {
        BoundDeclarationUse boundDeclarationUse = (BoundDeclarationUse)theEObject;
        T result = caseBoundDeclarationUse(boundDeclarationUse);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PATH_DECLARATION_USE:
      {
        PathDeclarationUse pathDeclarationUse = (PathDeclarationUse)theEObject;
        T result = casePathDeclarationUse(pathDeclarationUse);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.IMPORT_DECLARATION:
      {
        ImportDeclaration importDeclaration = (ImportDeclaration)theEObject;
        T result = caseImportDeclaration(importDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.DECLARATION:
      {
        Declaration declaration = (Declaration)theEObject;
        T result = caseDeclaration(declaration);
        if (result == null) result = caseNamedElement(declaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PARAMETERIZED_DECLARATION:
      {
        ParameterizedDeclaration parameterizedDeclaration = (ParameterizedDeclaration)theEObject;
        T result = caseParameterizedDeclaration(parameterizedDeclaration);
        if (result == null) result = caseDeclaration(parameterizedDeclaration);
        if (result == null) result = caseNamedElement(parameterizedDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.TYPE_DECLARATION:
      {
        TypeDeclaration typeDeclaration = (TypeDeclaration)theEObject;
        T result = caseTypeDeclaration(typeDeclaration);
        if (result == null) result = caseTypeDeclarationUse(typeDeclaration);
        if (result == null) result = caseDeclaration(typeDeclaration);
        if (result == null) result = caseNamedElement(typeDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.CHANNEL_DECLARATION:
      {
        ChannelDeclaration channelDeclaration = (ChannelDeclaration)theEObject;
        T result = caseChannelDeclaration(channelDeclaration);
        if (result == null) result = caseTypeDeclarationUse(channelDeclaration);
        if (result == null) result = caseDeclaration(channelDeclaration);
        if (result == null) result = caseNamedElement(channelDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.CHANNEL_TYPE:
      {
        ChannelType channelType = (ChannelType)theEObject;
        T result = caseChannelType(channelType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.TYPE:
      {
        Type type = (Type)theEObject;
        T result = caseType(type);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.BASIC_TYPE:
      {
        BasicType basicType = (BasicType)theEObject;
        T result = caseBasicType(basicType);
        if (result == null) result = caseType(basicType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.RANGE_TYPE:
      {
        RangeType rangeType = (RangeType)theEObject;
        T result = caseRangeType(rangeType);
        if (result == null) result = caseType(rangeType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNION_TYPE:
      {
        UnionType unionType = (UnionType)theEObject;
        T result = caseUnionType(unionType);
        if (result == null) result = caseType(unionType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNION_TAGS:
      {
        UnionTags unionTags = (UnionTags)theEObject;
        T result = caseUnionTags(unionTags);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNION_TAG_DECLARATION:
      {
        UnionTagDeclaration unionTagDeclaration = (UnionTagDeclaration)theEObject;
        T result = caseUnionTagDeclaration(unionTagDeclaration);
        if (result == null) result = caseNamedElement(unionTagDeclaration);
        if (result == null) result = caseExpressionDeclarationUse(unionTagDeclaration);
        if (result == null) result = caseUnionTagDeclarationUse(unionTagDeclaration);
        if (result == null) result = casePatternDeclarationUse(unionTagDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.RECORD_TYPE:
      {
        RecordType recordType = (RecordType)theEObject;
        T result = caseRecordType(recordType);
        if (result == null) result = caseType(recordType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.RECORD_FIELDS:
      {
        RecordFields recordFields = (RecordFields)theEObject;
        T result = caseRecordFields(recordFields);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.RECORD_FIELD_DECLARATION:
      {
        RecordFieldDeclaration recordFieldDeclaration = (RecordFieldDeclaration)theEObject;
        T result = caseRecordFieldDeclaration(recordFieldDeclaration);
        if (result == null) result = caseNamedElement(recordFieldDeclaration);
        if (result == null) result = caseRecordFieldDeclarationUse(recordFieldDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.QUEUE_TYPE:
      {
        QueueType queueType = (QueueType)theEObject;
        T result = caseQueueType(queueType);
        if (result == null) result = caseType(queueType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ARRAY_TYPE:
      {
        ArrayType arrayType = (ArrayType)theEObject;
        T result = caseArrayType(arrayType);
        if (result == null) result = caseType(arrayType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.REFERENCED_TYPE:
      {
        ReferencedType referencedType = (ReferencedType)theEObject;
        T result = caseReferencedType(referencedType);
        if (result == null) result = caseType(referencedType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.CONSTANT_DECLARATION:
      {
        ConstantDeclaration constantDeclaration = (ConstantDeclaration)theEObject;
        T result = caseConstantDeclaration(constantDeclaration);
        if (result == null) result = caseConstantDeclarationUse(constantDeclaration);
        if (result == null) result = caseExpressionDeclarationUse(constantDeclaration);
        if (result == null) result = caseBoundDeclarationUse(constantDeclaration);
        if (result == null) result = caseDeclaration(constantDeclaration);
        if (result == null) result = caseNamedElement(constantDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PROCESS_DECLARATION:
      {
        ProcessDeclaration processDeclaration = (ProcessDeclaration)theEObject;
        T result = caseProcessDeclaration(processDeclaration);
        if (result == null) result = caseRootDeclaration(processDeclaration);
        if (result == null) result = casePathDeclarationUse(processDeclaration);
        if (result == null) result = caseParameterizedDeclaration(processDeclaration);
        if (result == null) result = caseDeclaration(processDeclaration);
        if (result == null) result = caseNamedElement(processDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.GENERIC_DECLARATION:
      {
        GenericDeclaration genericDeclaration = (GenericDeclaration)theEObject;
        T result = caseGenericDeclaration(genericDeclaration);
        if (result == null) result = caseNamedElement(genericDeclaration);
        if (result == null) result = caseExpressionDeclarationUse(genericDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.GENERIC_TYPE_DECLARATION:
      {
        GenericTypeDeclaration genericTypeDeclaration = (GenericTypeDeclaration)theEObject;
        T result = caseGenericTypeDeclaration(genericTypeDeclaration);
        if (result == null) result = caseTypeDeclarationUse(genericTypeDeclaration);
        if (result == null) result = caseGenericDeclaration(genericTypeDeclaration);
        if (result == null) result = caseNamedElement(genericTypeDeclaration);
        if (result == null) result = caseExpressionDeclarationUse(genericTypeDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.GENERIC_CONSTANT_DECLARATION:
      {
        GenericConstantDeclaration genericConstantDeclaration = (GenericConstantDeclaration)theEObject;
        T result = caseGenericConstantDeclaration(genericConstantDeclaration);
        if (result == null) result = caseTypeDeclarationUse(genericConstantDeclaration);
        if (result == null) result = caseConstantDeclarationUse(genericConstantDeclaration);
        if (result == null) result = caseBoundDeclarationUse(genericConstantDeclaration);
        if (result == null) result = caseGenericDeclaration(genericConstantDeclaration);
        if (result == null) result = caseExpressionDeclarationUse(genericConstantDeclaration);
        if (result == null) result = caseNamedElement(genericConstantDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.GENERIC_UNION_TAG_DECLARATION:
      {
        GenericUnionTagDeclaration genericUnionTagDeclaration = (GenericUnionTagDeclaration)theEObject;
        T result = caseGenericUnionTagDeclaration(genericUnionTagDeclaration);
        if (result == null) result = caseUnionTagDeclarationUse(genericUnionTagDeclaration);
        if (result == null) result = casePatternDeclarationUse(genericUnionTagDeclaration);
        if (result == null) result = caseGenericDeclaration(genericUnionTagDeclaration);
        if (result == null) result = caseNamedElement(genericUnionTagDeclaration);
        if (result == null) result = caseExpressionDeclarationUse(genericUnionTagDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.GENERIC_RECORD_FIELD_DECLARATION:
      {
        GenericRecordFieldDeclaration genericRecordFieldDeclaration = (GenericRecordFieldDeclaration)theEObject;
        T result = caseGenericRecordFieldDeclaration(genericRecordFieldDeclaration);
        if (result == null) result = caseRecordFieldDeclarationUse(genericRecordFieldDeclaration);
        if (result == null) result = caseGenericDeclaration(genericRecordFieldDeclaration);
        if (result == null) result = caseNamedElement(genericRecordFieldDeclaration);
        if (result == null) result = caseExpressionDeclarationUse(genericRecordFieldDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.STATE_DECLARATION:
      {
        StateDeclaration stateDeclaration = (StateDeclaration)theEObject;
        T result = caseStateDeclaration(stateDeclaration);
        if (result == null) result = caseNamedElement(stateDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.TRANSITION:
      {
        Transition transition = (Transition)theEObject;
        T result = caseTransition(transition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.COMPONENT_DECLARATION:
      {
        ComponentDeclaration componentDeclaration = (ComponentDeclaration)theEObject;
        T result = caseComponentDeclaration(componentDeclaration);
        if (result == null) result = caseRootDeclaration(componentDeclaration);
        if (result == null) result = casePathDeclarationUse(componentDeclaration);
        if (result == null) result = caseParameterizedDeclaration(componentDeclaration);
        if (result == null) result = caseDeclaration(componentDeclaration);
        if (result == null) result = caseNamedElement(componentDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PORTS_DECLARATION:
      {
        PortsDeclaration portsDeclaration = (PortsDeclaration)theEObject;
        T result = casePortsDeclaration(portsDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PORT_DECLARATION:
      {
        PortDeclaration portDeclaration = (PortDeclaration)theEObject;
        T result = casePortDeclaration(portDeclaration);
        if (result == null) result = caseNamedElement(portDeclaration);
        if (result == null) result = caseExpressionDeclarationUse(portDeclaration);
        if (result == null) result = casePatternDeclarationUse(portDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LOCAL_PORTS_DECLARATION:
      {
        LocalPortsDeclaration localPortsDeclaration = (LocalPortsDeclaration)theEObject;
        T result = caseLocalPortsDeclaration(localPortsDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PARAMETERS_DECLARATION:
      {
        ParametersDeclaration parametersDeclaration = (ParametersDeclaration)theEObject;
        T result = caseParametersDeclaration(parametersDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PARAMETER_DECLARATION:
      {
        ParameterDeclaration parameterDeclaration = (ParameterDeclaration)theEObject;
        T result = caseParameterDeclaration(parameterDeclaration);
        if (result == null) result = caseNamedElement(parameterDeclaration);
        if (result == null) result = caseExpressionDeclarationUse(parameterDeclaration);
        if (result == null) result = caseReferenceDeclarationUse(parameterDeclaration);
        if (result == null) result = casePatternDeclarationUse(parameterDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.VARIABLES_DECLARATION:
      {
        VariablesDeclaration variablesDeclaration = (VariablesDeclaration)theEObject;
        T result = caseVariablesDeclaration(variablesDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.VARIABLE_DECLARATION:
      {
        VariableDeclaration variableDeclaration = (VariableDeclaration)theEObject;
        T result = caseVariableDeclaration(variableDeclaration);
        if (result == null) result = caseNamedElement(variableDeclaration);
        if (result == null) result = caseExpressionDeclarationUse(variableDeclaration);
        if (result == null) result = caseReferenceDeclarationUse(variableDeclaration);
        if (result == null) result = casePatternDeclarationUse(variableDeclaration);
        if (result == null) result = caseBoundDeclarationUse(variableDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PRIORITY_DECLARATION:
      {
        PriorityDeclaration priorityDeclaration = (PriorityDeclaration)theEObject;
        T result = casePriorityDeclaration(priorityDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PRIORITY_GROUP:
      {
        PriorityGroup priorityGroup = (PriorityGroup)theEObject;
        T result = casePriorityGroup(priorityGroup);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.STATEMENT:
      {
        Statement statement = (Statement)theEObject;
        T result = caseStatement(statement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.NULL_STATEMENT:
      {
        NullStatement nullStatement = (NullStatement)theEObject;
        T result = caseNullStatement(nullStatement);
        if (result == null) result = caseStatement(nullStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.TAGGED_STATEMENT:
      {
        TaggedStatement taggedStatement = (TaggedStatement)theEObject;
        T result = caseTaggedStatement(taggedStatement);
        if (result == null) result = caseStatement(taggedStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.TAG_DECLARATION:
      {
        TagDeclaration tagDeclaration = (TagDeclaration)theEObject;
        T result = caseTagDeclaration(tagDeclaration);
        if (result == null) result = caseNamedElement(tagDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PATTERN_STATEMENT:
      {
        PatternStatement patternStatement = (PatternStatement)theEObject;
        T result = casePatternStatement(patternStatement);
        if (result == null) result = caseStatement(patternStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PATTERN:
      {
        Pattern pattern = (Pattern)theEObject;
        T result = casePattern(pattern);
        if (result == null) result = casePatternStatement(pattern);
        if (result == null) result = caseStatement(pattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ANY_PATTERN:
      {
        AnyPattern anyPattern = (AnyPattern)theEObject;
        T result = caseAnyPattern(anyPattern);
        if (result == null) result = casePattern(anyPattern);
        if (result == null) result = casePatternStatement(anyPattern);
        if (result == null) result = caseStatement(anyPattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.CONSTANT_PATTERN:
      {
        ConstantPattern constantPattern = (ConstantPattern)theEObject;
        T result = caseConstantPattern(constantPattern);
        if (result == null) result = casePattern(constantPattern);
        if (result == null) result = casePatternStatement(constantPattern);
        if (result == null) result = caseStatement(constantPattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.INTEGER_PATTERN:
      {
        IntegerPattern integerPattern = (IntegerPattern)theEObject;
        T result = caseIntegerPattern(integerPattern);
        if (result == null) result = casePattern(integerPattern);
        if (result == null) result = casePatternStatement(integerPattern);
        if (result == null) result = caseStatement(integerPattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.IDENTIFIER_PATTERN:
      {
        IdentifierPattern identifierPattern = (IdentifierPattern)theEObject;
        T result = caseIdentifierPattern(identifierPattern);
        if (result == null) result = casePattern(identifierPattern);
        if (result == null) result = casePatternStatement(identifierPattern);
        if (result == null) result = caseStatement(identifierPattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.CONDITIONAL_STATEMENT:
      {
        ConditionalStatement conditionalStatement = (ConditionalStatement)theEObject;
        T result = caseConditionalStatement(conditionalStatement);
        if (result == null) result = caseStatement(conditionalStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.EXTENDED_CONDITIONAL_STATEMENT:
      {
        ExtendedConditionalStatement extendedConditionalStatement = (ExtendedConditionalStatement)theEObject;
        T result = caseExtendedConditionalStatement(extendedConditionalStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.SELECT_STATEMENT:
      {
        SelectStatement selectStatement = (SelectStatement)theEObject;
        T result = caseSelectStatement(selectStatement);
        if (result == null) result = caseStatement(selectStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.WHILE_STATEMENT:
      {
        WhileStatement whileStatement = (WhileStatement)theEObject;
        T result = caseWhileStatement(whileStatement);
        if (result == null) result = caseStatement(whileStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.FOREACH_STATEMENT:
      {
        ForeachStatement foreachStatement = (ForeachStatement)theEObject;
        T result = caseForeachStatement(foreachStatement);
        if (result == null) result = caseStatement(foreachStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.TO_STATEMENT:
      {
        ToStatement toStatement = (ToStatement)theEObject;
        T result = caseToStatement(toStatement);
        if (result == null) result = caseStatement(toStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.CASE_STATEMENT:
      {
        CaseStatement caseStatement = (CaseStatement)theEObject;
        T result = caseCaseStatement(caseStatement);
        if (result == null) result = caseStatement(caseStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LOOP_STATEMENT:
      {
        LoopStatement loopStatement = (LoopStatement)theEObject;
        T result = caseLoopStatement(loopStatement);
        if (result == null) result = caseStatement(loopStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ON_STATEMENT:
      {
        OnStatement onStatement = (OnStatement)theEObject;
        T result = caseOnStatement(onStatement);
        if (result == null) result = caseStatement(onStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.WAIT_STATEMENT:
      {
        WaitStatement waitStatement = (WaitStatement)theEObject;
        T result = caseWaitStatement(waitStatement);
        if (result == null) result = caseStatement(waitStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.COMPOSITION:
      {
        Composition composition = (Composition)theEObject;
        T result = caseComposition(composition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.BLOCK:
      {
        Block block = (Block)theEObject;
        T result = caseBlock(block);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.COMPOSITE_BLOCK:
      {
        CompositeBlock compositeBlock = (CompositeBlock)theEObject;
        T result = caseCompositeBlock(compositeBlock);
        if (result == null) result = caseBlock(compositeBlock);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.INSTANCE_DECLARATION:
      {
        InstanceDeclaration instanceDeclaration = (InstanceDeclaration)theEObject;
        T result = caseInstanceDeclaration(instanceDeclaration);
        if (result == null) result = casePathDeclarationUse(instanceDeclaration);
        if (result == null) result = caseBlock(instanceDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PORT_SET:
      {
        PortSet portSet = (PortSet)theEObject;
        T result = casePortSet(portSet);
        if (result == null) result = caseBlock(portSet);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.COMPONENT_INSTANCE:
      {
        ComponentInstance componentInstance = (ComponentInstance)theEObject;
        T result = caseComponentInstance(componentInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.GENERIC_INSTANCE:
      {
        GenericInstance genericInstance = (GenericInstance)theEObject;
        T result = caseGenericInstance(genericInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.TYPE_INSTANCE:
      {
        TypeInstance typeInstance = (TypeInstance)theEObject;
        T result = caseTypeInstance(typeInstance);
        if (result == null) result = caseGenericInstance(typeInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.CONSTANT_INSTANCE:
      {
        ConstantInstance constantInstance = (ConstantInstance)theEObject;
        T result = caseConstantInstance(constantInstance);
        if (result == null) result = caseGenericInstance(constantInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNION_TAG_INSTANCE:
      {
        UnionTagInstance unionTagInstance = (UnionTagInstance)theEObject;
        T result = caseUnionTagInstance(unionTagInstance);
        if (result == null) result = caseGenericInstance(unionTagInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.RECORD_FIELD_INSTANCE:
      {
        RecordFieldInstance recordFieldInstance = (RecordFieldInstance)theEObject;
        T result = caseRecordFieldInstance(recordFieldInstance);
        if (result == null) result = caseGenericInstance(recordFieldInstance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.EXPRESSION:
      {
        Expression expression = (Expression)theEObject;
        T result = caseExpression(expression);
        if (result == null) result = caseRangeType(expression);
        if (result == null) result = caseType(expression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.REFERENCE_EXPRESSION:
      {
        ReferenceExpression referenceExpression = (ReferenceExpression)theEObject;
        T result = caseReferenceExpression(referenceExpression);
        if (result == null) result = caseExpression(referenceExpression);
        if (result == null) result = caseRangeType(referenceExpression);
        if (result == null) result = caseType(referenceExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.IDENTIFIER_EXPRESSION:
      {
        IdentifierExpression identifierExpression = (IdentifierExpression)theEObject;
        T result = caseIdentifierExpression(identifierExpression);
        if (result == null) result = caseExpression(identifierExpression);
        if (result == null) result = caseRangeType(identifierExpression);
        if (result == null) result = caseType(identifierExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.RECORD_EXPRESSION:
      {
        RecordExpression recordExpression = (RecordExpression)theEObject;
        T result = caseRecordExpression(recordExpression);
        if (result == null) result = caseExpression(recordExpression);
        if (result == null) result = caseRangeType(recordExpression);
        if (result == null) result = caseType(recordExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.FIELD_EXPRESSION:
      {
        FieldExpression fieldExpression = (FieldExpression)theEObject;
        T result = caseFieldExpression(fieldExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.QUEUE_EXPRESSION:
      {
        QueueExpression queueExpression = (QueueExpression)theEObject;
        T result = caseQueueExpression(queueExpression);
        if (result == null) result = caseExpression(queueExpression);
        if (result == null) result = caseRangeType(queueExpression);
        if (result == null) result = caseType(queueExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ENQUEUE_EXPRESSION:
      {
        EnqueueExpression enqueueExpression = (EnqueueExpression)theEObject;
        T result = caseEnqueueExpression(enqueueExpression);
        if (result == null) result = caseExpression(enqueueExpression);
        if (result == null) result = caseRangeType(enqueueExpression);
        if (result == null) result = caseType(enqueueExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.APPEND_EXPRESSION:
      {
        AppendExpression appendExpression = (AppendExpression)theEObject;
        T result = caseAppendExpression(appendExpression);
        if (result == null) result = caseExpression(appendExpression);
        if (result == null) result = caseRangeType(appendExpression);
        if (result == null) result = caseType(appendExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LITERAL_EXPRESSION:
      {
        LiteralExpression literalExpression = (LiteralExpression)theEObject;
        T result = caseLiteralExpression(literalExpression);
        if (result == null) result = caseExpression(literalExpression);
        if (result == null) result = caseRangeType(literalExpression);
        if (result == null) result = caseType(literalExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.BOOLEAN_LITERAL:
      {
        BooleanLiteral booleanLiteral = (BooleanLiteral)theEObject;
        T result = caseBooleanLiteral(booleanLiteral);
        if (result == null) result = caseLiteralExpression(booleanLiteral);
        if (result == null) result = caseExpression(booleanLiteral);
        if (result == null) result = caseRangeType(booleanLiteral);
        if (result == null) result = caseType(booleanLiteral);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.NATURAL_LITERAL:
      {
        NaturalLiteral naturalLiteral = (NaturalLiteral)theEObject;
        T result = caseNaturalLiteral(naturalLiteral);
        if (result == null) result = caseLiteralExpression(naturalLiteral);
        if (result == null) result = caseExpression(naturalLiteral);
        if (result == null) result = caseRangeType(naturalLiteral);
        if (result == null) result = caseType(naturalLiteral);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LOWER_BOUND:
      {
        LowerBound lowerBound = (LowerBound)theEObject;
        T result = caseLowerBound(lowerBound);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UPPER_BOUND:
      {
        UpperBound upperBound = (UpperBound)theEObject;
        T result = caseUpperBound(upperBound);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.NATURAL_LOWER_BOUND:
      {
        NaturalLowerBound naturalLowerBound = (NaturalLowerBound)theEObject;
        T result = caseNaturalLowerBound(naturalLowerBound);
        if (result == null) result = caseLowerBound(naturalLowerBound);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.NATURAL_UPPER_BOUND:
      {
        NaturalUpperBound naturalUpperBound = (NaturalUpperBound)theEObject;
        T result = caseNaturalUpperBound(naturalUpperBound);
        if (result == null) result = caseUpperBound(naturalUpperBound);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.DECIMAL_LOWER_BOUND:
      {
        DecimalLowerBound decimalLowerBound = (DecimalLowerBound)theEObject;
        T result = caseDecimalLowerBound(decimalLowerBound);
        if (result == null) result = caseLowerBound(decimalLowerBound);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.DECIMAL_UPPER_BOUND:
      {
        DecimalUpperBound decimalUpperBound = (DecimalUpperBound)theEObject;
        T result = caseDecimalUpperBound(decimalUpperBound);
        if (result == null) result = caseUpperBound(decimalUpperBound);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.VARIABLE_LOWER_BOUND:
      {
        VariableLowerBound variableLowerBound = (VariableLowerBound)theEObject;
        T result = caseVariableLowerBound(variableLowerBound);
        if (result == null) result = caseLowerBound(variableLowerBound);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.VARIABLE_UPPER_BOUND:
      {
        VariableUpperBound variableUpperBound = (VariableUpperBound)theEObject;
        T result = caseVariableUpperBound(variableUpperBound);
        if (result == null) result = caseUpperBound(variableUpperBound);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.INFINITE_UPPER_BOUND:
      {
        InfiniteUpperBound infiniteUpperBound = (InfiniteUpperBound)theEObject;
        T result = caseInfiniteUpperBound(infiniteUpperBound);
        if (result == null) result = caseUpperBound(infiniteUpperBound);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.REQUIREMENT:
      {
        Requirement requirement = (Requirement)theEObject;
        T result = caseRequirement(requirement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PROPERTY_DECLARATION:
      {
        PropertyDeclaration propertyDeclaration = (PropertyDeclaration)theEObject;
        T result = casePropertyDeclaration(propertyDeclaration);
        if (result == null) result = caseDeclaration(propertyDeclaration);
        if (result == null) result = caseNamedElement(propertyDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PROPERTY:
      {
        Property property = (Property)theEObject;
        T result = caseProperty(property);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PATTERN_PROPERTY:
      {
        PatternProperty patternProperty = (PatternProperty)theEObject;
        T result = casePatternProperty(patternProperty);
        if (result == null) result = caseProperty(patternProperty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_PATTERN:
      {
        LTLPattern ltlPattern = (LTLPattern)theEObject;
        T result = caseLTLPattern(ltlPattern);
        if (result == null) result = casePatternProperty(ltlPattern);
        if (result == null) result = caseProperty(ltlPattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.DEADLOCK_FREE_PATTERN:
      {
        DeadlockFreePattern deadlockFreePattern = (DeadlockFreePattern)theEObject;
        T result = caseDeadlockFreePattern(deadlockFreePattern);
        if (result == null) result = casePatternProperty(deadlockFreePattern);
        if (result == null) result = caseProperty(deadlockFreePattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.INFINITELY_OFTEN_PATTERN:
      {
        InfinitelyOftenPattern infinitelyOftenPattern = (InfinitelyOftenPattern)theEObject;
        T result = caseInfinitelyOftenPattern(infinitelyOftenPattern);
        if (result == null) result = casePatternProperty(infinitelyOftenPattern);
        if (result == null) result = caseProperty(infinitelyOftenPattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.MORTAL_PATTERN:
      {
        MortalPattern mortalPattern = (MortalPattern)theEObject;
        T result = caseMortalPattern(mortalPattern);
        if (result == null) result = casePatternProperty(mortalPattern);
        if (result == null) result = caseProperty(mortalPattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PRESENCE_PATTERN:
      {
        PresencePattern presencePattern = (PresencePattern)theEObject;
        T result = casePresencePattern(presencePattern);
        if (result == null) result = casePatternProperty(presencePattern);
        if (result == null) result = caseProperty(presencePattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ABSENCE_PATTERN:
      {
        AbsencePattern absencePattern = (AbsencePattern)theEObject;
        T result = caseAbsencePattern(absencePattern);
        if (result == null) result = casePatternProperty(absencePattern);
        if (result == null) result = caseProperty(absencePattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ALWAYS_PATTERN:
      {
        AlwaysPattern alwaysPattern = (AlwaysPattern)theEObject;
        T result = caseAlwaysPattern(alwaysPattern);
        if (result == null) result = casePatternProperty(alwaysPattern);
        if (result == null) result = caseProperty(alwaysPattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.SEQUENCE_PATTERN:
      {
        SequencePattern sequencePattern = (SequencePattern)theEObject;
        T result = caseSequencePattern(sequencePattern);
        if (result == null) result = casePatternProperty(sequencePattern);
        if (result == null) result = caseProperty(sequencePattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_PROPERTY:
      {
        LTLProperty ltlProperty = (LTLProperty)theEObject;
        T result = caseLTLProperty(ltlProperty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.STATE_EVENT:
      {
        StateEvent stateEvent = (StateEvent)theEObject;
        T result = caseStateEvent(stateEvent);
        if (result == null) result = caseObservableEvent(stateEvent);
        if (result == null) result = caseLTLProperty(stateEvent);
        if (result == null) result = caseObservable(stateEvent);
        if (result == null) result = caseSequencePattern(stateEvent);
        if (result == null) result = casePatternProperty(stateEvent);
        if (result == null) result = caseProperty(stateEvent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ENTER_STATE_EVENT:
      {
        EnterStateEvent enterStateEvent = (EnterStateEvent)theEObject;
        T result = caseEnterStateEvent(enterStateEvent);
        if (result == null) result = caseStateEvent(enterStateEvent);
        if (result == null) result = caseObservableEvent(enterStateEvent);
        if (result == null) result = caseLTLProperty(enterStateEvent);
        if (result == null) result = caseObservable(enterStateEvent);
        if (result == null) result = caseSequencePattern(enterStateEvent);
        if (result == null) result = casePatternProperty(enterStateEvent);
        if (result == null) result = caseProperty(enterStateEvent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LEAVE_STATE_EVENT:
      {
        LeaveStateEvent leaveStateEvent = (LeaveStateEvent)theEObject;
        T result = caseLeaveStateEvent(leaveStateEvent);
        if (result == null) result = caseStateEvent(leaveStateEvent);
        if (result == null) result = caseObservableEvent(leaveStateEvent);
        if (result == null) result = caseLTLProperty(leaveStateEvent);
        if (result == null) result = caseObservable(leaveStateEvent);
        if (result == null) result = caseSequencePattern(leaveStateEvent);
        if (result == null) result = casePatternProperty(leaveStateEvent);
        if (result == null) result = caseProperty(leaveStateEvent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.OBSERVABLE:
      {
        Observable observable = (Observable)theEObject;
        T result = caseObservable(observable);
        if (result == null) result = caseSequencePattern(observable);
        if (result == null) result = casePatternProperty(observable);
        if (result == null) result = caseProperty(observable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.OBSERVABLE_EVENT:
      {
        ObservableEvent observableEvent = (ObservableEvent)theEObject;
        T result = caseObservableEvent(observableEvent);
        if (result == null) result = caseLTLProperty(observableEvent);
        if (result == null) result = caseObservable(observableEvent);
        if (result == null) result = caseSequencePattern(observableEvent);
        if (result == null) result = casePatternProperty(observableEvent);
        if (result == null) result = caseProperty(observableEvent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PATH_EVENT:
      {
        PathEvent pathEvent = (PathEvent)theEObject;
        T result = casePathEvent(pathEvent);
        if (result == null) result = caseObservableEvent(pathEvent);
        if (result == null) result = caseLTLProperty(pathEvent);
        if (result == null) result = caseObservable(pathEvent);
        if (result == null) result = caseSequencePattern(pathEvent);
        if (result == null) result = casePatternProperty(pathEvent);
        if (result == null) result = caseProperty(pathEvent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PATH:
      {
        Path path = (Path)theEObject;
        T result = casePath(path);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PATH_ITEM:
      {
        PathItem pathItem = (PathItem)theEObject;
        T result = casePathItem(pathItem);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.NATURAL_ITEM:
      {
        NaturalItem naturalItem = (NaturalItem)theEObject;
        T result = caseNaturalItem(naturalItem);
        if (result == null) result = casePathItem(naturalItem);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.NAMED_ITEM:
      {
        NamedItem namedItem = (NamedItem)theEObject;
        T result = caseNamedItem(namedItem);
        if (result == null) result = casePathItem(namedItem);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.SUBJECT:
      {
        Subject subject = (Subject)theEObject;
        T result = caseSubject(subject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.STATE_SUBJECT:
      {
        StateSubject stateSubject = (StateSubject)theEObject;
        T result = caseStateSubject(stateSubject);
        if (result == null) result = caseSubject(stateSubject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.VALUE_SUBJECT:
      {
        ValueSubject valueSubject = (ValueSubject)theEObject;
        T result = caseValueSubject(valueSubject);
        if (result == null) result = caseSubject(valueSubject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.TAG_SUBJECT:
      {
        TagSubject tagSubject = (TagSubject)theEObject;
        T result = caseTagSubject(tagSubject);
        if (result == null) result = caseSubject(tagSubject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.EVENT_SUBJECT:
      {
        EventSubject eventSubject = (EventSubject)theEObject;
        T result = caseEventSubject(eventSubject);
        if (result == null) result = caseSubject(eventSubject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.TUPLE_TYPE:
      {
        TupleType tupleType = (TupleType)theEObject;
        T result = caseTupleType(tupleType);
        if (result == null) result = caseType(tupleType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.NATURAL_TYPE:
      {
        NaturalType naturalType = (NaturalType)theEObject;
        T result = caseNaturalType(naturalType);
        if (result == null) result = caseBasicType(naturalType);
        if (result == null) result = caseType(naturalType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.INTEGER_TYPE:
      {
        IntegerType integerType = (IntegerType)theEObject;
        T result = caseIntegerType(integerType);
        if (result == null) result = caseBasicType(integerType);
        if (result == null) result = caseType(integerType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.BOOLEAN_TYPE:
      {
        BooleanType booleanType = (BooleanType)theEObject;
        T result = caseBooleanType(booleanType);
        if (result == null) result = caseBasicType(booleanType);
        if (result == null) result = caseType(booleanType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNLESS_STATEMENT:
      {
        UnlessStatement unlessStatement = (UnlessStatement)theEObject;
        T result = caseUnlessStatement(unlessStatement);
        if (result == null) result = caseStatement(unlessStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.STATEMENT_CHOICE:
      {
        StatementChoice statementChoice = (StatementChoice)theEObject;
        T result = caseStatementChoice(statementChoice);
        if (result == null) result = caseStatement(statementChoice);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.STATEMENT_SEQUENCE:
      {
        StatementSequence statementSequence = (StatementSequence)theEObject;
        T result = caseStatementSequence(statementSequence);
        if (result == null) result = caseStatement(statementSequence);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ASSIGN_STATEMENT:
      {
        AssignStatement assignStatement = (AssignStatement)theEObject;
        T result = caseAssignStatement(assignStatement);
        if (result == null) result = casePatternStatement(assignStatement);
        if (result == null) result = caseStatement(assignStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.SEND_STATEMENT:
      {
        SendStatement sendStatement = (SendStatement)theEObject;
        T result = caseSendStatement(sendStatement);
        if (result == null) result = casePatternStatement(sendStatement);
        if (result == null) result = caseStatement(sendStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.RECEIVE_STATEMENT:
      {
        ReceiveStatement receiveStatement = (ReceiveStatement)theEObject;
        T result = caseReceiveStatement(receiveStatement);
        if (result == null) result = casePatternStatement(receiveStatement);
        if (result == null) result = caseStatement(receiveStatement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.CONSTRUCTOR_PATTERN:
      {
        ConstructorPattern constructorPattern = (ConstructorPattern)theEObject;
        T result = caseConstructorPattern(constructorPattern);
        if (result == null) result = caseIdentifierPattern(constructorPattern);
        if (result == null) result = casePattern(constructorPattern);
        if (result == null) result = casePatternStatement(constructorPattern);
        if (result == null) result = caseStatement(constructorPattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ARRAY_ACCESS_PATTERN:
      {
        ArrayAccessPattern arrayAccessPattern = (ArrayAccessPattern)theEObject;
        T result = caseArrayAccessPattern(arrayAccessPattern);
        if (result == null) result = caseIdentifierPattern(arrayAccessPattern);
        if (result == null) result = casePattern(arrayAccessPattern);
        if (result == null) result = casePatternStatement(arrayAccessPattern);
        if (result == null) result = caseStatement(arrayAccessPattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.RECORD_ACCESS_PATTERN:
      {
        RecordAccessPattern recordAccessPattern = (RecordAccessPattern)theEObject;
        T result = caseRecordAccessPattern(recordAccessPattern);
        if (result == null) result = caseIdentifierPattern(recordAccessPattern);
        if (result == null) result = casePattern(recordAccessPattern);
        if (result == null) result = casePatternStatement(recordAccessPattern);
        if (result == null) result = caseStatement(recordAccessPattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ALL_EXPRESSION:
      {
        AllExpression allExpression = (AllExpression)theEObject;
        T result = caseAllExpression(allExpression);
        if (result == null) result = caseExpression(allExpression);
        if (result == null) result = caseRangeType(allExpression);
        if (result == null) result = caseType(allExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.EXISTS_EXPRESSION:
      {
        ExistsExpression existsExpression = (ExistsExpression)theEObject;
        T result = caseExistsExpression(existsExpression);
        if (result == null) result = caseExpression(existsExpression);
        if (result == null) result = caseRangeType(existsExpression);
        if (result == null) result = caseType(existsExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.CONDITIONAL:
      {
        Conditional conditional = (Conditional)theEObject;
        T result = caseConditional(conditional);
        if (result == null) result = caseExpression(conditional);
        if (result == null) result = caseRangeType(conditional);
        if (result == null) result = caseType(conditional);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.DISJUNCTION:
      {
        Disjunction disjunction = (Disjunction)theEObject;
        T result = caseDisjunction(disjunction);
        if (result == null) result = caseExpression(disjunction);
        if (result == null) result = caseRangeType(disjunction);
        if (result == null) result = caseType(disjunction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.IMPLICATION:
      {
        Implication implication = (Implication)theEObject;
        T result = caseImplication(implication);
        if (result == null) result = caseExpression(implication);
        if (result == null) result = caseRangeType(implication);
        if (result == null) result = caseType(implication);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.CONJUNCTION:
      {
        Conjunction conjunction = (Conjunction)theEObject;
        T result = caseConjunction(conjunction);
        if (result == null) result = caseExpression(conjunction);
        if (result == null) result = caseRangeType(conjunction);
        if (result == null) result = caseType(conjunction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.COMPARISON_EQUAL:
      {
        ComparisonEqual comparisonEqual = (ComparisonEqual)theEObject;
        T result = caseComparisonEqual(comparisonEqual);
        if (result == null) result = caseExpression(comparisonEqual);
        if (result == null) result = caseRangeType(comparisonEqual);
        if (result == null) result = caseType(comparisonEqual);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.COMPARISON_NOT_EQUAL:
      {
        ComparisonNotEqual comparisonNotEqual = (ComparisonNotEqual)theEObject;
        T result = caseComparisonNotEqual(comparisonNotEqual);
        if (result == null) result = caseExpression(comparisonNotEqual);
        if (result == null) result = caseRangeType(comparisonNotEqual);
        if (result == null) result = caseType(comparisonNotEqual);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.COMPARISON_LESSER:
      {
        ComparisonLesser comparisonLesser = (ComparisonLesser)theEObject;
        T result = caseComparisonLesser(comparisonLesser);
        if (result == null) result = caseExpression(comparisonLesser);
        if (result == null) result = caseRangeType(comparisonLesser);
        if (result == null) result = caseType(comparisonLesser);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.COMPARISON_LESSER_EQUAL:
      {
        ComparisonLesserEqual comparisonLesserEqual = (ComparisonLesserEqual)theEObject;
        T result = caseComparisonLesserEqual(comparisonLesserEqual);
        if (result == null) result = caseExpression(comparisonLesserEqual);
        if (result == null) result = caseRangeType(comparisonLesserEqual);
        if (result == null) result = caseType(comparisonLesserEqual);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.COMPARISON_GREATER:
      {
        ComparisonGreater comparisonGreater = (ComparisonGreater)theEObject;
        T result = caseComparisonGreater(comparisonGreater);
        if (result == null) result = caseExpression(comparisonGreater);
        if (result == null) result = caseRangeType(comparisonGreater);
        if (result == null) result = caseType(comparisonGreater);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.COMPARISON_GREATER_EQUAL:
      {
        ComparisonGreaterEqual comparisonGreaterEqual = (ComparisonGreaterEqual)theEObject;
        T result = caseComparisonGreaterEqual(comparisonGreaterEqual);
        if (result == null) result = caseExpression(comparisonGreaterEqual);
        if (result == null) result = caseRangeType(comparisonGreaterEqual);
        if (result == null) result = caseType(comparisonGreaterEqual);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ADDITION:
      {
        Addition addition = (Addition)theEObject;
        T result = caseAddition(addition);
        if (result == null) result = caseExpression(addition);
        if (result == null) result = caseRangeType(addition);
        if (result == null) result = caseType(addition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.SUBSTRACTION:
      {
        Substraction substraction = (Substraction)theEObject;
        T result = caseSubstraction(substraction);
        if (result == null) result = caseExpression(substraction);
        if (result == null) result = caseRangeType(substraction);
        if (result == null) result = caseType(substraction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.MULTIPLICATION:
      {
        Multiplication multiplication = (Multiplication)theEObject;
        T result = caseMultiplication(multiplication);
        if (result == null) result = caseExpression(multiplication);
        if (result == null) result = caseRangeType(multiplication);
        if (result == null) result = caseType(multiplication);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.DIVISION:
      {
        Division division = (Division)theEObject;
        T result = caseDivision(division);
        if (result == null) result = caseExpression(division);
        if (result == null) result = caseRangeType(division);
        if (result == null) result = caseType(division);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.MODULO:
      {
        Modulo modulo = (Modulo)theEObject;
        T result = caseModulo(modulo);
        if (result == null) result = caseExpression(modulo);
        if (result == null) result = caseRangeType(modulo);
        if (result == null) result = caseType(modulo);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNARY_PLUS_EXPRESSION:
      {
        UnaryPlusExpression unaryPlusExpression = (UnaryPlusExpression)theEObject;
        T result = caseUnaryPlusExpression(unaryPlusExpression);
        if (result == null) result = caseExpression(unaryPlusExpression);
        if (result == null) result = caseRangeType(unaryPlusExpression);
        if (result == null) result = caseType(unaryPlusExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNARY_MINUS_EXPRESSION:
      {
        UnaryMinusExpression unaryMinusExpression = (UnaryMinusExpression)theEObject;
        T result = caseUnaryMinusExpression(unaryMinusExpression);
        if (result == null) result = caseExpression(unaryMinusExpression);
        if (result == null) result = caseRangeType(unaryMinusExpression);
        if (result == null) result = caseType(unaryMinusExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNARY_NEGATION_EXPRESSION:
      {
        UnaryNegationExpression unaryNegationExpression = (UnaryNegationExpression)theEObject;
        T result = caseUnaryNegationExpression(unaryNegationExpression);
        if (result == null) result = caseExpression(unaryNegationExpression);
        if (result == null) result = caseRangeType(unaryNegationExpression);
        if (result == null) result = caseType(unaryNegationExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNARY_FIRST_EXPRESSION:
      {
        UnaryFirstExpression unaryFirstExpression = (UnaryFirstExpression)theEObject;
        T result = caseUnaryFirstExpression(unaryFirstExpression);
        if (result == null) result = caseExpression(unaryFirstExpression);
        if (result == null) result = caseRangeType(unaryFirstExpression);
        if (result == null) result = caseType(unaryFirstExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNARY_LENGTH_EXPRESSION:
      {
        UnaryLengthExpression unaryLengthExpression = (UnaryLengthExpression)theEObject;
        T result = caseUnaryLengthExpression(unaryLengthExpression);
        if (result == null) result = caseExpression(unaryLengthExpression);
        if (result == null) result = caseRangeType(unaryLengthExpression);
        if (result == null) result = caseType(unaryLengthExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNARY_COERCE_EXPRESSION:
      {
        UnaryCoerceExpression unaryCoerceExpression = (UnaryCoerceExpression)theEObject;
        T result = caseUnaryCoerceExpression(unaryCoerceExpression);
        if (result == null) result = caseExpression(unaryCoerceExpression);
        if (result == null) result = caseRangeType(unaryCoerceExpression);
        if (result == null) result = caseType(unaryCoerceExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNARY_FULL_EXPRESSION:
      {
        UnaryFullExpression unaryFullExpression = (UnaryFullExpression)theEObject;
        T result = caseUnaryFullExpression(unaryFullExpression);
        if (result == null) result = caseExpression(unaryFullExpression);
        if (result == null) result = caseRangeType(unaryFullExpression);
        if (result == null) result = caseType(unaryFullExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNARY_DE_QUEUE_EXPRESSION:
      {
        UnaryDeQueueExpression unaryDeQueueExpression = (UnaryDeQueueExpression)theEObject;
        T result = caseUnaryDeQueueExpression(unaryDeQueueExpression);
        if (result == null) result = caseExpression(unaryDeQueueExpression);
        if (result == null) result = caseRangeType(unaryDeQueueExpression);
        if (result == null) result = caseType(unaryDeQueueExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.UNARY_EMPTY_EXPRESSION:
      {
        UnaryEmptyExpression unaryEmptyExpression = (UnaryEmptyExpression)theEObject;
        T result = caseUnaryEmptyExpression(unaryEmptyExpression);
        if (result == null) result = caseExpression(unaryEmptyExpression);
        if (result == null) result = caseRangeType(unaryEmptyExpression);
        if (result == null) result = caseType(unaryEmptyExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PROJECTION:
      {
        Projection projection = (Projection)theEObject;
        T result = caseProjection(projection);
        if (result == null) result = caseExpression(projection);
        if (result == null) result = caseRangeType(projection);
        if (result == null) result = caseType(projection);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ARRAY_ACCESS_EXPRESSION:
      {
        ArrayAccessExpression arrayAccessExpression = (ArrayAccessExpression)theEObject;
        T result = caseArrayAccessExpression(arrayAccessExpression);
        if (result == null) result = caseIdentifierExpression(arrayAccessExpression);
        if (result == null) result = caseExpression(arrayAccessExpression);
        if (result == null) result = caseRangeType(arrayAccessExpression);
        if (result == null) result = caseType(arrayAccessExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.RECORD_ACCESS_EXPRESSION:
      {
        RecordAccessExpression recordAccessExpression = (RecordAccessExpression)theEObject;
        T result = caseRecordAccessExpression(recordAccessExpression);
        if (result == null) result = caseIdentifierExpression(recordAccessExpression);
        if (result == null) result = caseExpression(recordAccessExpression);
        if (result == null) result = caseRangeType(recordAccessExpression);
        if (result == null) result = caseType(recordAccessExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.CONSTRUCTION_EXPRESSION:
      {
        ConstructionExpression constructionExpression = (ConstructionExpression)theEObject;
        T result = caseConstructionExpression(constructionExpression);
        if (result == null) result = caseIdentifierExpression(constructionExpression);
        if (result == null) result = caseExpression(constructionExpression);
        if (result == null) result = caseRangeType(constructionExpression);
        if (result == null) result = caseType(constructionExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.EXPLICIT_ARRAY_EXPRESSION:
      {
        ExplicitArrayExpression explicitArrayExpression = (ExplicitArrayExpression)theEObject;
        T result = caseExplicitArrayExpression(explicitArrayExpression);
        if (result == null) result = caseExpression(explicitArrayExpression);
        if (result == null) result = caseRangeType(explicitArrayExpression);
        if (result == null) result = caseType(explicitArrayExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.IMPLICIT_ARRAY_EXPRESSION:
      {
        ImplicitArrayExpression implicitArrayExpression = (ImplicitArrayExpression)theEObject;
        T result = caseImplicitArrayExpression(implicitArrayExpression);
        if (result == null) result = caseExpression(implicitArrayExpression);
        if (result == null) result = caseRangeType(implicitArrayExpression);
        if (result == null) result = caseType(implicitArrayExpression);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.TRUE_LITERAL:
      {
        TrueLiteral trueLiteral = (TrueLiteral)theEObject;
        T result = caseTrueLiteral(trueLiteral);
        if (result == null) result = caseBooleanLiteral(trueLiteral);
        if (result == null) result = caseLiteralExpression(trueLiteral);
        if (result == null) result = caseExpression(trueLiteral);
        if (result == null) result = caseRangeType(trueLiteral);
        if (result == null) result = caseType(trueLiteral);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.FALSE_LITERAL:
      {
        FalseLiteral falseLiteral = (FalseLiteral)theEObject;
        T result = caseFalseLiteral(falseLiteral);
        if (result == null) result = caseBooleanLiteral(falseLiteral);
        if (result == null) result = caseLiteralExpression(falseLiteral);
        if (result == null) result = caseExpression(falseLiteral);
        if (result == null) result = caseRangeType(falseLiteral);
        if (result == null) result = caseType(falseLiteral);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.ALL_PROPERTY:
      {
        AllProperty allProperty = (AllProperty)theEObject;
        T result = caseAllProperty(allProperty);
        if (result == null) result = caseProperty(allProperty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.EXISTS_PROPERTY:
      {
        ExistsProperty existsProperty = (ExistsProperty)theEObject;
        T result = caseExistsProperty(existsProperty);
        if (result == null) result = caseProperty(existsProperty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PROPERTY_DISJUNCTION:
      {
        PropertyDisjunction propertyDisjunction = (PropertyDisjunction)theEObject;
        T result = casePropertyDisjunction(propertyDisjunction);
        if (result == null) result = caseProperty(propertyDisjunction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PROPERTY_IMPLICATION:
      {
        PropertyImplication propertyImplication = (PropertyImplication)theEObject;
        T result = casePropertyImplication(propertyImplication);
        if (result == null) result = caseProperty(propertyImplication);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PROPERTY_CONJUNCTION:
      {
        PropertyConjunction propertyConjunction = (PropertyConjunction)theEObject;
        T result = casePropertyConjunction(propertyConjunction);
        if (result == null) result = caseProperty(propertyConjunction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PROPERTY_NEGATION:
      {
        PropertyNegation propertyNegation = (PropertyNegation)theEObject;
        T result = casePropertyNegation(propertyNegation);
        if (result == null) result = caseProperty(propertyNegation);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LEADS_TO_PATTERN:
      {
        LeadsToPattern leadsToPattern = (LeadsToPattern)theEObject;
        T result = caseLeadsToPattern(leadsToPattern);
        if (result == null) result = caseSequencePattern(leadsToPattern);
        if (result == null) result = casePatternProperty(leadsToPattern);
        if (result == null) result = caseProperty(leadsToPattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.PRECEDES_PATTERN:
      {
        PrecedesPattern precedesPattern = (PrecedesPattern)theEObject;
        T result = casePrecedesPattern(precedesPattern);
        if (result == null) result = caseSequencePattern(precedesPattern);
        if (result == null) result = casePatternProperty(precedesPattern);
        if (result == null) result = caseProperty(precedesPattern);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_ALL:
      {
        LTLAll ltlAll = (LTLAll)theEObject;
        T result = caseLTLAll(ltlAll);
        if (result == null) result = caseLTLProperty(ltlAll);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_EXISTS:
      {
        LTLExists ltlExists = (LTLExists)theEObject;
        T result = caseLTLExists(ltlExists);
        if (result == null) result = caseLTLProperty(ltlExists);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_DISJUNCTION:
      {
        LTLDisjunction ltlDisjunction = (LTLDisjunction)theEObject;
        T result = caseLTLDisjunction(ltlDisjunction);
        if (result == null) result = caseLTLProperty(ltlDisjunction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_IMPLICATION:
      {
        LTLImplication ltlImplication = (LTLImplication)theEObject;
        T result = caseLTLImplication(ltlImplication);
        if (result == null) result = caseLTLProperty(ltlImplication);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_CONJUNCTION:
      {
        LTLConjunction ltlConjunction = (LTLConjunction)theEObject;
        T result = caseLTLConjunction(ltlConjunction);
        if (result == null) result = caseLTLProperty(ltlConjunction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_UNTIL:
      {
        LTLUntil ltlUntil = (LTLUntil)theEObject;
        T result = caseLTLUntil(ltlUntil);
        if (result == null) result = caseLTLProperty(ltlUntil);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_RELEASE:
      {
        LTLRelease ltlRelease = (LTLRelease)theEObject;
        T result = caseLTLRelease(ltlRelease);
        if (result == null) result = caseLTLProperty(ltlRelease);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_UNARY_NEGATION:
      {
        LTLUnaryNegation ltlUnaryNegation = (LTLUnaryNegation)theEObject;
        T result = caseLTLUnaryNegation(ltlUnaryNegation);
        if (result == null) result = caseLTLProperty(ltlUnaryNegation);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_UNARY_NEXT:
      {
        LTLUnaryNext ltlUnaryNext = (LTLUnaryNext)theEObject;
        T result = caseLTLUnaryNext(ltlUnaryNext);
        if (result == null) result = caseLTLProperty(ltlUnaryNext);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_UNARY_ALWAYS:
      {
        LTLUnaryAlways ltlUnaryAlways = (LTLUnaryAlways)theEObject;
        T result = caseLTLUnaryAlways(ltlUnaryAlways);
        if (result == null) result = caseLTLProperty(ltlUnaryAlways);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_UNARY_EVENTUALLY:
      {
        LTLUnaryEventually ltlUnaryEventually = (LTLUnaryEventually)theEObject;
        T result = caseLTLUnaryEventually(ltlUnaryEventually);
        if (result == null) result = caseLTLProperty(ltlUnaryEventually);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.LTL_VARIABLE:
      {
        LTLVariable ltlVariable = (LTLVariable)theEObject;
        T result = caseLTLVariable(ltlVariable);
        if (result == null) result = caseLTLProperty(ltlVariable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.OBSERVABLE_DISJUNCTION:
      {
        ObservableDisjunction observableDisjunction = (ObservableDisjunction)theEObject;
        T result = caseObservableDisjunction(observableDisjunction);
        if (result == null) result = caseObservable(observableDisjunction);
        if (result == null) result = caseSequencePattern(observableDisjunction);
        if (result == null) result = casePatternProperty(observableDisjunction);
        if (result == null) result = caseProperty(observableDisjunction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.OBSERVABLE_IMPLICATION:
      {
        ObservableImplication observableImplication = (ObservableImplication)theEObject;
        T result = caseObservableImplication(observableImplication);
        if (result == null) result = caseObservable(observableImplication);
        if (result == null) result = caseSequencePattern(observableImplication);
        if (result == null) result = casePatternProperty(observableImplication);
        if (result == null) result = caseProperty(observableImplication);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.OBSERVABLE_CONJUNCTION:
      {
        ObservableConjunction observableConjunction = (ObservableConjunction)theEObject;
        T result = caseObservableConjunction(observableConjunction);
        if (result == null) result = caseObservable(observableConjunction);
        if (result == null) result = caseSequencePattern(observableConjunction);
        if (result == null) result = casePatternProperty(observableConjunction);
        if (result == null) result = caseProperty(observableConjunction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case FiacrePackage.OBSERVABLE_NEGATION:
      {
        ObservableNegation observableNegation = (ObservableNegation)theEObject;
        T result = caseObservableNegation(observableNegation);
        if (result == null) result = caseObservable(observableNegation);
        if (result == null) result = caseSequencePattern(observableNegation);
        if (result == null) result = casePatternProperty(observableNegation);
        if (result == null) result = caseProperty(observableNegation);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Model</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModel(Model object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Root Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Root Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRootDeclaration(RootDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNamedElement(NamedElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Declaration Use</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeDeclarationUse(TypeDeclarationUse object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Constant Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Constant Declaration Use</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConstantDeclarationUse(ConstantDeclarationUse object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression Declaration Use</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpressionDeclarationUse(ExpressionDeclarationUse object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Reference Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Reference Declaration Use</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReferenceDeclarationUse(ReferenceDeclarationUse object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Union Tag Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Union Tag Declaration Use</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnionTagDeclarationUse(UnionTagDeclarationUse object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Record Field Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Record Field Declaration Use</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRecordFieldDeclarationUse(RecordFieldDeclarationUse object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Pattern Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Pattern Declaration Use</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePatternDeclarationUse(PatternDeclarationUse object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Bound Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Bound Declaration Use</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBoundDeclarationUse(BoundDeclarationUse object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Path Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Path Declaration Use</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePathDeclarationUse(PathDeclarationUse object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Import Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Import Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImportDeclaration(ImportDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDeclaration(Declaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Parameterized Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Parameterized Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParameterizedDeclaration(ParameterizedDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeDeclaration(TypeDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Channel Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Channel Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseChannelDeclaration(ChannelDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Channel Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Channel Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseChannelType(ChannelType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseType(Type object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Basic Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Basic Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBasicType(BasicType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Range Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Range Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRangeType(RangeType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Union Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Union Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnionType(UnionType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Union Tags</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Union Tags</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnionTags(UnionTags object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Union Tag Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Union Tag Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnionTagDeclaration(UnionTagDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Record Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Record Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRecordType(RecordType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Record Fields</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Record Fields</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRecordFields(RecordFields object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Record Field Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Record Field Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRecordFieldDeclaration(RecordFieldDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Queue Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Queue Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseQueueType(QueueType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArrayType(ArrayType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Referenced Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Referenced Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReferencedType(ReferencedType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Constant Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Constant Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConstantDeclaration(ConstantDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Process Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Process Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseProcessDeclaration(ProcessDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericDeclaration(GenericDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Type Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Type Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericTypeDeclaration(GenericTypeDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Constant Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Constant Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericConstantDeclaration(GenericConstantDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Union Tag Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Union Tag Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericUnionTagDeclaration(GenericUnionTagDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Record Field Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Record Field Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericRecordFieldDeclaration(GenericRecordFieldDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>State Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>State Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStateDeclaration(StateDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Transition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Transition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTransition(Transition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Component Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Component Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComponentDeclaration(ComponentDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ports Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ports Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortsDeclaration(PortsDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortDeclaration(PortDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Local Ports Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Local Ports Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLocalPortsDeclaration(LocalPortsDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Parameters Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Parameters Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParametersDeclaration(ParametersDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Parameter Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Parameter Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseParameterDeclaration(ParameterDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variables Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variables Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVariablesDeclaration(VariablesDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variable Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variable Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVariableDeclaration(VariableDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Priority Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Priority Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePriorityDeclaration(PriorityDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Priority Group</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Priority Group</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePriorityGroup(PriorityGroup object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStatement(Statement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Null Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Null Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNullStatement(NullStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Tagged Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Tagged Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTaggedStatement(TaggedStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Tag Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Tag Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTagDeclaration(TagDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Pattern Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Pattern Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePatternStatement(PatternStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePattern(Pattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Any Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Any Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAnyPattern(AnyPattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Constant Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Constant Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConstantPattern(ConstantPattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Integer Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Integer Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIntegerPattern(IntegerPattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Identifier Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Identifier Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIdentifierPattern(IdentifierPattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Conditional Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Conditional Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConditionalStatement(ConditionalStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Extended Conditional Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Extended Conditional Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExtendedConditionalStatement(ExtendedConditionalStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Select Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Select Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectStatement(SelectStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>While Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>While Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseWhileStatement(WhileStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Foreach Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Foreach Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseForeachStatement(ForeachStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>To Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>To Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseToStatement(ToStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Case Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Case Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCaseStatement(CaseStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Loop Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Loop Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLoopStatement(LoopStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>On Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>On Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOnStatement(OnStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Wait Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Wait Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseWaitStatement(WaitStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Composition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Composition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComposition(Composition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Block</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Block</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBlock(Block object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Composite Block</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Composite Block</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCompositeBlock(CompositeBlock object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Instance Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Instance Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInstanceDeclaration(InstanceDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Port Set</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Port Set</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePortSet(PortSet object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Component Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Component Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComponentInstance(ComponentInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Generic Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Generic Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGenericInstance(GenericInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTypeInstance(TypeInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Constant Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Constant Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConstantInstance(ConstantInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Union Tag Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Union Tag Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnionTagInstance(UnionTagInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Record Field Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Record Field Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRecordFieldInstance(RecordFieldInstance object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExpression(Expression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Reference Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Reference Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReferenceExpression(ReferenceExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Identifier Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Identifier Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIdentifierExpression(IdentifierExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Record Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Record Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRecordExpression(RecordExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Field Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Field Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFieldExpression(FieldExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Queue Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Queue Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseQueueExpression(QueueExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Enqueue Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Enqueue Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEnqueueExpression(EnqueueExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Append Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Append Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAppendExpression(AppendExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Literal Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Literal Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLiteralExpression(LiteralExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBooleanLiteral(BooleanLiteral object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Natural Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Natural Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNaturalLiteral(NaturalLiteral object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Lower Bound</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Lower Bound</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLowerBound(LowerBound object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Upper Bound</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Upper Bound</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUpperBound(UpperBound object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Natural Lower Bound</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Natural Lower Bound</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNaturalLowerBound(NaturalLowerBound object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Natural Upper Bound</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Natural Upper Bound</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNaturalUpperBound(NaturalUpperBound object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Decimal Lower Bound</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Decimal Lower Bound</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDecimalLowerBound(DecimalLowerBound object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Decimal Upper Bound</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Decimal Upper Bound</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDecimalUpperBound(DecimalUpperBound object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variable Lower Bound</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variable Lower Bound</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVariableLowerBound(VariableLowerBound object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variable Upper Bound</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variable Upper Bound</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVariableUpperBound(VariableUpperBound object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Infinite Upper Bound</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Infinite Upper Bound</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInfiniteUpperBound(InfiniteUpperBound object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Requirement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Requirement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRequirement(Requirement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Property Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Property Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePropertyDeclaration(PropertyDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseProperty(Property object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Pattern Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Pattern Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePatternProperty(PatternProperty object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLPattern(LTLPattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Deadlock Free Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Deadlock Free Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDeadlockFreePattern(DeadlockFreePattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Infinitely Often Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Infinitely Often Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInfinitelyOftenPattern(InfinitelyOftenPattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Mortal Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Mortal Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMortalPattern(MortalPattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Presence Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Presence Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePresencePattern(PresencePattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Absence Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Absence Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAbsencePattern(AbsencePattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Always Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Always Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAlwaysPattern(AlwaysPattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Sequence Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Sequence Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSequencePattern(SequencePattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLProperty(LTLProperty object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>State Event</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>State Event</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStateEvent(StateEvent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Enter State Event</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Enter State Event</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEnterStateEvent(EnterStateEvent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Leave State Event</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Leave State Event</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLeaveStateEvent(LeaveStateEvent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Observable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Observable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseObservable(Observable object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Observable Event</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Observable Event</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseObservableEvent(ObservableEvent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Path Event</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Path Event</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePathEvent(PathEvent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Path</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Path</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePath(Path object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Path Item</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Path Item</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePathItem(PathItem object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Natural Item</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Natural Item</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNaturalItem(NaturalItem object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Named Item</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Named Item</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNamedItem(NamedItem object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Subject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Subject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSubject(Subject object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>State Subject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>State Subject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStateSubject(StateSubject object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Value Subject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Value Subject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseValueSubject(ValueSubject object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Tag Subject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Tag Subject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTagSubject(TagSubject object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Event Subject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Event Subject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEventSubject(EventSubject object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Tuple Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Tuple Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTupleType(TupleType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Natural Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Natural Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNaturalType(NaturalType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Integer Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Integer Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIntegerType(IntegerType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBooleanType(BooleanType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unless Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unless Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnlessStatement(UnlessStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement Choice</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement Choice</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStatementChoice(StatementChoice object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Statement Sequence</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Statement Sequence</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStatementSequence(StatementSequence object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Assign Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Assign Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAssignStatement(AssignStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Send Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Send Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSendStatement(SendStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Receive Statement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Receive Statement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReceiveStatement(ReceiveStatement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Constructor Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Constructor Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConstructorPattern(ConstructorPattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Access Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Access Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArrayAccessPattern(ArrayAccessPattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Record Access Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Record Access Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRecordAccessPattern(RecordAccessPattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>All Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>All Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAllExpression(AllExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Exists Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Exists Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExistsExpression(ExistsExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Conditional</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Conditional</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConditional(Conditional object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Disjunction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Disjunction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDisjunction(Disjunction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Implication</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Implication</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImplication(Implication object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Conjunction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Conjunction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConjunction(Conjunction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Comparison Equal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Comparison Equal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComparisonEqual(ComparisonEqual object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Comparison Not Equal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Comparison Not Equal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComparisonNotEqual(ComparisonNotEqual object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Comparison Lesser</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Comparison Lesser</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComparisonLesser(ComparisonLesser object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Comparison Lesser Equal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Comparison Lesser Equal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComparisonLesserEqual(ComparisonLesserEqual object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Comparison Greater</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Comparison Greater</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComparisonGreater(ComparisonGreater object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Comparison Greater Equal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Comparison Greater Equal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComparisonGreaterEqual(ComparisonGreaterEqual object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Addition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Addition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAddition(Addition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Substraction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Substraction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSubstraction(Substraction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Multiplication</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Multiplication</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMultiplication(Multiplication object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Division</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Division</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDivision(Division object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Modulo</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Modulo</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModulo(Modulo object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unary Plus Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unary Plus Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnaryPlusExpression(UnaryPlusExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unary Minus Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unary Minus Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnaryMinusExpression(UnaryMinusExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unary Negation Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unary Negation Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnaryNegationExpression(UnaryNegationExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unary First Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unary First Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnaryFirstExpression(UnaryFirstExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unary Length Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unary Length Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnaryLengthExpression(UnaryLengthExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unary Coerce Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unary Coerce Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnaryCoerceExpression(UnaryCoerceExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unary Full Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unary Full Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnaryFullExpression(UnaryFullExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unary De Queue Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unary De Queue Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnaryDeQueueExpression(UnaryDeQueueExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unary Empty Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unary Empty Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnaryEmptyExpression(UnaryEmptyExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Projection</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Projection</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseProjection(Projection object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Array Access Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Array Access Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseArrayAccessExpression(ArrayAccessExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Record Access Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Record Access Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRecordAccessExpression(RecordAccessExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Construction Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Construction Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConstructionExpression(ConstructionExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Explicit Array Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Explicit Array Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExplicitArrayExpression(ExplicitArrayExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Implicit Array Expression</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Implicit Array Expression</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImplicitArrayExpression(ImplicitArrayExpression object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>True Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>True Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTrueLiteral(TrueLiteral object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>False Literal</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>False Literal</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFalseLiteral(FalseLiteral object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>All Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>All Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAllProperty(AllProperty object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Exists Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Exists Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExistsProperty(ExistsProperty object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Property Disjunction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Property Disjunction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePropertyDisjunction(PropertyDisjunction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Property Implication</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Property Implication</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePropertyImplication(PropertyImplication object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Property Conjunction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Property Conjunction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePropertyConjunction(PropertyConjunction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Property Negation</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Property Negation</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePropertyNegation(PropertyNegation object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Leads To Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Leads To Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLeadsToPattern(LeadsToPattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Precedes Pattern</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Precedes Pattern</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePrecedesPattern(PrecedesPattern object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL All</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL All</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLAll(LTLAll object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL Exists</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL Exists</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLExists(LTLExists object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL Disjunction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL Disjunction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLDisjunction(LTLDisjunction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL Implication</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL Implication</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLImplication(LTLImplication object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL Conjunction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL Conjunction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLConjunction(LTLConjunction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL Until</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL Until</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLUntil(LTLUntil object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL Release</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL Release</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLRelease(LTLRelease object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL Unary Negation</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL Unary Negation</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLUnaryNegation(LTLUnaryNegation object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL Unary Next</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL Unary Next</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLUnaryNext(LTLUnaryNext object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL Unary Always</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL Unary Always</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLUnaryAlways(LTLUnaryAlways object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL Unary Eventually</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL Unary Eventually</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLUnaryEventually(LTLUnaryEventually object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>LTL Variable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>LTL Variable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLTLVariable(LTLVariable object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Observable Disjunction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Observable Disjunction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseObservableDisjunction(ObservableDisjunction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Observable Implication</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Observable Implication</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseObservableImplication(ObservableImplication object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Observable Conjunction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Observable Conjunction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseObservableConjunction(ObservableConjunction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Observable Negation</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Observable Negation</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseObservableNegation(ObservableNegation object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //FiacreSwitch
