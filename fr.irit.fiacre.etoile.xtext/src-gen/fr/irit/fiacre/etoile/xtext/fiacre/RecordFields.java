/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record Fields</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFields#getFields <em>Fields</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFields#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRecordFields()
 * @model
 * @generated
 */
public interface RecordFields extends EObject
{
  /**
   * Returns the value of the '<em><b>Fields</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fields</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fields</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRecordFields_Fields()
   * @model containment="true"
   * @generated
   */
  EList<RecordFieldDeclaration> getFields();

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRecordFields_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFields#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

} // RecordFields
