/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;
import fr.irit.fiacre.etoile.xtext.fiacre.GenericDeclaration;
import fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration;
import fr.irit.fiacre.etoile.xtext.fiacre.PortsDeclaration;
import fr.irit.fiacre.etoile.xtext.fiacre.PriorityDeclaration;
import fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration;
import fr.irit.fiacre.etoile.xtext.fiacre.StateDeclaration;
import fr.irit.fiacre.etoile.xtext.fiacre.Statement;
import fr.irit.fiacre.etoile.xtext.fiacre.Transition;
import fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ProcessDeclarationImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ProcessDeclarationImpl#getGenerics <em>Generics</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ProcessDeclarationImpl#getPorts <em>Ports</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ProcessDeclarationImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ProcessDeclarationImpl#getLocalPorts <em>Local Ports</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ProcessDeclarationImpl#getPriorities <em>Priorities</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ProcessDeclarationImpl#getStates <em>States</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ProcessDeclarationImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ProcessDeclarationImpl#getPrelude <em>Prelude</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ProcessDeclarationImpl#getTransitions <em>Transitions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcessDeclarationImpl extends RootDeclarationImpl implements ProcessDeclaration
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getGenerics() <em>Generics</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGenerics()
   * @generated
   * @ordered
   */
  protected EList<GenericDeclaration> generics;

  /**
   * The cached value of the '{@link #getPorts() <em>Ports</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPorts()
   * @generated
   * @ordered
   */
  protected EList<PortsDeclaration> ports;

  /**
   * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParameters()
   * @generated
   * @ordered
   */
  protected EList<ParametersDeclaration> parameters;

  /**
   * The cached value of the '{@link #getLocalPorts() <em>Local Ports</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLocalPorts()
   * @generated
   * @ordered
   */
  protected EList<PortsDeclaration> localPorts;

  /**
   * The cached value of the '{@link #getPriorities() <em>Priorities</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPriorities()
   * @generated
   * @ordered
   */
  protected EList<PriorityDeclaration> priorities;

  /**
   * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStates()
   * @generated
   * @ordered
   */
  protected EList<StateDeclaration> states;

  /**
   * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVariables()
   * @generated
   * @ordered
   */
  protected EList<VariablesDeclaration> variables;

  /**
   * The cached value of the '{@link #getPrelude() <em>Prelude</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrelude()
   * @generated
   * @ordered
   */
  protected Statement prelude;

  /**
   * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTransitions()
   * @generated
   * @ordered
   */
  protected EList<Transition> transitions;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ProcessDeclarationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FiacrePackage.eINSTANCE.getProcessDeclaration();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PROCESS_DECLARATION__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<GenericDeclaration> getGenerics()
  {
    if (generics == null)
    {
      generics = new EObjectContainmentEList<GenericDeclaration>(GenericDeclaration.class, this, FiacrePackage.PROCESS_DECLARATION__GENERICS);
    }
    return generics;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<PortsDeclaration> getPorts()
  {
    if (ports == null)
    {
      ports = new EObjectContainmentEList<PortsDeclaration>(PortsDeclaration.class, this, FiacrePackage.PROCESS_DECLARATION__PORTS);
    }
    return ports;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ParametersDeclaration> getParameters()
  {
    if (parameters == null)
    {
      parameters = new EObjectContainmentEList<ParametersDeclaration>(ParametersDeclaration.class, this, FiacrePackage.PROCESS_DECLARATION__PARAMETERS);
    }
    return parameters;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<PortsDeclaration> getLocalPorts()
  {
    if (localPorts == null)
    {
      localPorts = new EObjectContainmentEList<PortsDeclaration>(PortsDeclaration.class, this, FiacrePackage.PROCESS_DECLARATION__LOCAL_PORTS);
    }
    return localPorts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<PriorityDeclaration> getPriorities()
  {
    if (priorities == null)
    {
      priorities = new EObjectContainmentEList<PriorityDeclaration>(PriorityDeclaration.class, this, FiacrePackage.PROCESS_DECLARATION__PRIORITIES);
    }
    return priorities;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<StateDeclaration> getStates()
  {
    if (states == null)
    {
      states = new EObjectContainmentEList<StateDeclaration>(StateDeclaration.class, this, FiacrePackage.PROCESS_DECLARATION__STATES);
    }
    return states;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<VariablesDeclaration> getVariables()
  {
    if (variables == null)
    {
      variables = new EObjectContainmentEList<VariablesDeclaration>(VariablesDeclaration.class, this, FiacrePackage.PROCESS_DECLARATION__VARIABLES);
    }
    return variables;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement getPrelude()
  {
    return prelude;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPrelude(Statement newPrelude, NotificationChain msgs)
  {
    Statement oldPrelude = prelude;
    prelude = newPrelude;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.PROCESS_DECLARATION__PRELUDE, oldPrelude, newPrelude);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPrelude(Statement newPrelude)
  {
    if (newPrelude != prelude)
    {
      NotificationChain msgs = null;
      if (prelude != null)
        msgs = ((InternalEObject)prelude).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PROCESS_DECLARATION__PRELUDE, null, msgs);
      if (newPrelude != null)
        msgs = ((InternalEObject)newPrelude).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PROCESS_DECLARATION__PRELUDE, null, msgs);
      msgs = basicSetPrelude(newPrelude, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PROCESS_DECLARATION__PRELUDE, newPrelude, newPrelude));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Transition> getTransitions()
  {
    if (transitions == null)
    {
      transitions = new EObjectContainmentEList<Transition>(Transition.class, this, FiacrePackage.PROCESS_DECLARATION__TRANSITIONS);
    }
    return transitions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case FiacrePackage.PROCESS_DECLARATION__GENERICS:
        return ((InternalEList<?>)getGenerics()).basicRemove(otherEnd, msgs);
      case FiacrePackage.PROCESS_DECLARATION__PORTS:
        return ((InternalEList<?>)getPorts()).basicRemove(otherEnd, msgs);
      case FiacrePackage.PROCESS_DECLARATION__PARAMETERS:
        return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
      case FiacrePackage.PROCESS_DECLARATION__LOCAL_PORTS:
        return ((InternalEList<?>)getLocalPorts()).basicRemove(otherEnd, msgs);
      case FiacrePackage.PROCESS_DECLARATION__PRIORITIES:
        return ((InternalEList<?>)getPriorities()).basicRemove(otherEnd, msgs);
      case FiacrePackage.PROCESS_DECLARATION__STATES:
        return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
      case FiacrePackage.PROCESS_DECLARATION__VARIABLES:
        return ((InternalEList<?>)getVariables()).basicRemove(otherEnd, msgs);
      case FiacrePackage.PROCESS_DECLARATION__PRELUDE:
        return basicSetPrelude(null, msgs);
      case FiacrePackage.PROCESS_DECLARATION__TRANSITIONS:
        return ((InternalEList<?>)getTransitions()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case FiacrePackage.PROCESS_DECLARATION__NAME:
        return getName();
      case FiacrePackage.PROCESS_DECLARATION__GENERICS:
        return getGenerics();
      case FiacrePackage.PROCESS_DECLARATION__PORTS:
        return getPorts();
      case FiacrePackage.PROCESS_DECLARATION__PARAMETERS:
        return getParameters();
      case FiacrePackage.PROCESS_DECLARATION__LOCAL_PORTS:
        return getLocalPorts();
      case FiacrePackage.PROCESS_DECLARATION__PRIORITIES:
        return getPriorities();
      case FiacrePackage.PROCESS_DECLARATION__STATES:
        return getStates();
      case FiacrePackage.PROCESS_DECLARATION__VARIABLES:
        return getVariables();
      case FiacrePackage.PROCESS_DECLARATION__PRELUDE:
        return getPrelude();
      case FiacrePackage.PROCESS_DECLARATION__TRANSITIONS:
        return getTransitions();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case FiacrePackage.PROCESS_DECLARATION__NAME:
        setName((String)newValue);
        return;
      case FiacrePackage.PROCESS_DECLARATION__GENERICS:
        getGenerics().clear();
        getGenerics().addAll((Collection<? extends GenericDeclaration>)newValue);
        return;
      case FiacrePackage.PROCESS_DECLARATION__PORTS:
        getPorts().clear();
        getPorts().addAll((Collection<? extends PortsDeclaration>)newValue);
        return;
      case FiacrePackage.PROCESS_DECLARATION__PARAMETERS:
        getParameters().clear();
        getParameters().addAll((Collection<? extends ParametersDeclaration>)newValue);
        return;
      case FiacrePackage.PROCESS_DECLARATION__LOCAL_PORTS:
        getLocalPorts().clear();
        getLocalPorts().addAll((Collection<? extends PortsDeclaration>)newValue);
        return;
      case FiacrePackage.PROCESS_DECLARATION__PRIORITIES:
        getPriorities().clear();
        getPriorities().addAll((Collection<? extends PriorityDeclaration>)newValue);
        return;
      case FiacrePackage.PROCESS_DECLARATION__STATES:
        getStates().clear();
        getStates().addAll((Collection<? extends StateDeclaration>)newValue);
        return;
      case FiacrePackage.PROCESS_DECLARATION__VARIABLES:
        getVariables().clear();
        getVariables().addAll((Collection<? extends VariablesDeclaration>)newValue);
        return;
      case FiacrePackage.PROCESS_DECLARATION__PRELUDE:
        setPrelude((Statement)newValue);
        return;
      case FiacrePackage.PROCESS_DECLARATION__TRANSITIONS:
        getTransitions().clear();
        getTransitions().addAll((Collection<? extends Transition>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.PROCESS_DECLARATION__NAME:
        setName(NAME_EDEFAULT);
        return;
      case FiacrePackage.PROCESS_DECLARATION__GENERICS:
        getGenerics().clear();
        return;
      case FiacrePackage.PROCESS_DECLARATION__PORTS:
        getPorts().clear();
        return;
      case FiacrePackage.PROCESS_DECLARATION__PARAMETERS:
        getParameters().clear();
        return;
      case FiacrePackage.PROCESS_DECLARATION__LOCAL_PORTS:
        getLocalPorts().clear();
        return;
      case FiacrePackage.PROCESS_DECLARATION__PRIORITIES:
        getPriorities().clear();
        return;
      case FiacrePackage.PROCESS_DECLARATION__STATES:
        getStates().clear();
        return;
      case FiacrePackage.PROCESS_DECLARATION__VARIABLES:
        getVariables().clear();
        return;
      case FiacrePackage.PROCESS_DECLARATION__PRELUDE:
        setPrelude((Statement)null);
        return;
      case FiacrePackage.PROCESS_DECLARATION__TRANSITIONS:
        getTransitions().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.PROCESS_DECLARATION__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case FiacrePackage.PROCESS_DECLARATION__GENERICS:
        return generics != null && !generics.isEmpty();
      case FiacrePackage.PROCESS_DECLARATION__PORTS:
        return ports != null && !ports.isEmpty();
      case FiacrePackage.PROCESS_DECLARATION__PARAMETERS:
        return parameters != null && !parameters.isEmpty();
      case FiacrePackage.PROCESS_DECLARATION__LOCAL_PORTS:
        return localPorts != null && !localPorts.isEmpty();
      case FiacrePackage.PROCESS_DECLARATION__PRIORITIES:
        return priorities != null && !priorities.isEmpty();
      case FiacrePackage.PROCESS_DECLARATION__STATES:
        return states != null && !states.isEmpty();
      case FiacrePackage.PROCESS_DECLARATION__VARIABLES:
        return variables != null && !variables.isEmpty();
      case FiacrePackage.PROCESS_DECLARATION__PRELUDE:
        return prelude != null;
      case FiacrePackage.PROCESS_DECLARATION__TRANSITIONS:
        return transitions != null && !transitions.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //ProcessDeclarationImpl
