/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Projection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.Projection#getChannel <em>Channel</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.Projection#getField <em>Field</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getProjection()
 * @model
 * @generated
 */
public interface Projection extends Expression
{
  /**
   * Returns the value of the '<em><b>Channel</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Channel</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Channel</em>' containment reference.
   * @see #setChannel(Expression)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getProjection_Channel()
   * @model containment="true"
   * @generated
   */
  Expression getChannel();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.Projection#getChannel <em>Channel</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Channel</em>' containment reference.
   * @see #getChannel()
   * @generated
   */
  void setChannel(Expression value);

  /**
   * Returns the value of the '<em><b>Field</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field</em>' containment reference.
   * @see #setField(Expression)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getProjection_Field()
   * @model containment="true"
   * @generated
   */
  Expression getField();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.Projection#getField <em>Field</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field</em>' containment reference.
   * @see #getField()
   * @generated
   */
  void setField(Expression value);

} // Projection
