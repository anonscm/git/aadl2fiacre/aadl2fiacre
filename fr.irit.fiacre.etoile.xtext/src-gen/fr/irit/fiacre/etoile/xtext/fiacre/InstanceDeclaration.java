/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instance Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration#getLocal <em>Local</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration#getName <em>Name</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration#getInstance <em>Instance</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getInstanceDeclaration()
 * @model
 * @generated
 */
public interface InstanceDeclaration extends PathDeclarationUse, Block
{
  /**
   * Returns the value of the '<em><b>Local</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Local</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Local</em>' containment reference.
   * @see #setLocal(PortSet)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getInstanceDeclaration_Local()
   * @model containment="true"
   * @generated
   */
  PortSet getLocal();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration#getLocal <em>Local</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Local</em>' containment reference.
   * @see #getLocal()
   * @generated
   */
  void setLocal(PortSet value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getInstanceDeclaration_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Instance</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Instance</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Instance</em>' containment reference.
   * @see #setInstance(ComponentInstance)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getInstanceDeclaration_Instance()
   * @model containment="true"
   * @generated
   */
  ComponentInstance getInstance();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration#getInstance <em>Instance</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Instance</em>' containment reference.
   * @see #getInstance()
   * @generated
   */
  void setInstance(ComponentInstance value);

} // InstanceDeclaration
