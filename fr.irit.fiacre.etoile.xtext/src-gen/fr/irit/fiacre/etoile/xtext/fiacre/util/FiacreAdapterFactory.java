/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.util;

import fr.irit.fiacre.etoile.xtext.fiacre.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage
 * @generated
 */
public class FiacreAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static FiacrePackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FiacreAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = FiacrePackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FiacreSwitch<Adapter> modelSwitch =
    new FiacreSwitch<Adapter>()
    {
      @Override
      public Adapter caseModel(Model object)
      {
        return createModelAdapter();
      }
      @Override
      public Adapter caseRootDeclaration(RootDeclaration object)
      {
        return createRootDeclarationAdapter();
      }
      @Override
      public Adapter caseNamedElement(NamedElement object)
      {
        return createNamedElementAdapter();
      }
      @Override
      public Adapter caseTypeDeclarationUse(TypeDeclarationUse object)
      {
        return createTypeDeclarationUseAdapter();
      }
      @Override
      public Adapter caseConstantDeclarationUse(ConstantDeclarationUse object)
      {
        return createConstantDeclarationUseAdapter();
      }
      @Override
      public Adapter caseExpressionDeclarationUse(ExpressionDeclarationUse object)
      {
        return createExpressionDeclarationUseAdapter();
      }
      @Override
      public Adapter caseReferenceDeclarationUse(ReferenceDeclarationUse object)
      {
        return createReferenceDeclarationUseAdapter();
      }
      @Override
      public Adapter caseUnionTagDeclarationUse(UnionTagDeclarationUse object)
      {
        return createUnionTagDeclarationUseAdapter();
      }
      @Override
      public Adapter caseRecordFieldDeclarationUse(RecordFieldDeclarationUse object)
      {
        return createRecordFieldDeclarationUseAdapter();
      }
      @Override
      public Adapter casePatternDeclarationUse(PatternDeclarationUse object)
      {
        return createPatternDeclarationUseAdapter();
      }
      @Override
      public Adapter caseBoundDeclarationUse(BoundDeclarationUse object)
      {
        return createBoundDeclarationUseAdapter();
      }
      @Override
      public Adapter casePathDeclarationUse(PathDeclarationUse object)
      {
        return createPathDeclarationUseAdapter();
      }
      @Override
      public Adapter caseImportDeclaration(ImportDeclaration object)
      {
        return createImportDeclarationAdapter();
      }
      @Override
      public Adapter caseDeclaration(Declaration object)
      {
        return createDeclarationAdapter();
      }
      @Override
      public Adapter caseParameterizedDeclaration(ParameterizedDeclaration object)
      {
        return createParameterizedDeclarationAdapter();
      }
      @Override
      public Adapter caseTypeDeclaration(TypeDeclaration object)
      {
        return createTypeDeclarationAdapter();
      }
      @Override
      public Adapter caseChannelDeclaration(ChannelDeclaration object)
      {
        return createChannelDeclarationAdapter();
      }
      @Override
      public Adapter caseChannelType(ChannelType object)
      {
        return createChannelTypeAdapter();
      }
      @Override
      public Adapter caseType(Type object)
      {
        return createTypeAdapter();
      }
      @Override
      public Adapter caseBasicType(BasicType object)
      {
        return createBasicTypeAdapter();
      }
      @Override
      public Adapter caseRangeType(RangeType object)
      {
        return createRangeTypeAdapter();
      }
      @Override
      public Adapter caseUnionType(UnionType object)
      {
        return createUnionTypeAdapter();
      }
      @Override
      public Adapter caseUnionTags(UnionTags object)
      {
        return createUnionTagsAdapter();
      }
      @Override
      public Adapter caseUnionTagDeclaration(UnionTagDeclaration object)
      {
        return createUnionTagDeclarationAdapter();
      }
      @Override
      public Adapter caseRecordType(RecordType object)
      {
        return createRecordTypeAdapter();
      }
      @Override
      public Adapter caseRecordFields(RecordFields object)
      {
        return createRecordFieldsAdapter();
      }
      @Override
      public Adapter caseRecordFieldDeclaration(RecordFieldDeclaration object)
      {
        return createRecordFieldDeclarationAdapter();
      }
      @Override
      public Adapter caseQueueType(QueueType object)
      {
        return createQueueTypeAdapter();
      }
      @Override
      public Adapter caseArrayType(ArrayType object)
      {
        return createArrayTypeAdapter();
      }
      @Override
      public Adapter caseReferencedType(ReferencedType object)
      {
        return createReferencedTypeAdapter();
      }
      @Override
      public Adapter caseConstantDeclaration(ConstantDeclaration object)
      {
        return createConstantDeclarationAdapter();
      }
      @Override
      public Adapter caseProcessDeclaration(ProcessDeclaration object)
      {
        return createProcessDeclarationAdapter();
      }
      @Override
      public Adapter caseGenericDeclaration(GenericDeclaration object)
      {
        return createGenericDeclarationAdapter();
      }
      @Override
      public Adapter caseGenericTypeDeclaration(GenericTypeDeclaration object)
      {
        return createGenericTypeDeclarationAdapter();
      }
      @Override
      public Adapter caseGenericConstantDeclaration(GenericConstantDeclaration object)
      {
        return createGenericConstantDeclarationAdapter();
      }
      @Override
      public Adapter caseGenericUnionTagDeclaration(GenericUnionTagDeclaration object)
      {
        return createGenericUnionTagDeclarationAdapter();
      }
      @Override
      public Adapter caseGenericRecordFieldDeclaration(GenericRecordFieldDeclaration object)
      {
        return createGenericRecordFieldDeclarationAdapter();
      }
      @Override
      public Adapter caseStateDeclaration(StateDeclaration object)
      {
        return createStateDeclarationAdapter();
      }
      @Override
      public Adapter caseTransition(Transition object)
      {
        return createTransitionAdapter();
      }
      @Override
      public Adapter caseComponentDeclaration(ComponentDeclaration object)
      {
        return createComponentDeclarationAdapter();
      }
      @Override
      public Adapter casePortsDeclaration(PortsDeclaration object)
      {
        return createPortsDeclarationAdapter();
      }
      @Override
      public Adapter casePortDeclaration(PortDeclaration object)
      {
        return createPortDeclarationAdapter();
      }
      @Override
      public Adapter caseLocalPortsDeclaration(LocalPortsDeclaration object)
      {
        return createLocalPortsDeclarationAdapter();
      }
      @Override
      public Adapter caseParametersDeclaration(ParametersDeclaration object)
      {
        return createParametersDeclarationAdapter();
      }
      @Override
      public Adapter caseParameterDeclaration(ParameterDeclaration object)
      {
        return createParameterDeclarationAdapter();
      }
      @Override
      public Adapter caseVariablesDeclaration(VariablesDeclaration object)
      {
        return createVariablesDeclarationAdapter();
      }
      @Override
      public Adapter caseVariableDeclaration(VariableDeclaration object)
      {
        return createVariableDeclarationAdapter();
      }
      @Override
      public Adapter casePriorityDeclaration(PriorityDeclaration object)
      {
        return createPriorityDeclarationAdapter();
      }
      @Override
      public Adapter casePriorityGroup(PriorityGroup object)
      {
        return createPriorityGroupAdapter();
      }
      @Override
      public Adapter caseStatement(Statement object)
      {
        return createStatementAdapter();
      }
      @Override
      public Adapter caseNullStatement(NullStatement object)
      {
        return createNullStatementAdapter();
      }
      @Override
      public Adapter caseTaggedStatement(TaggedStatement object)
      {
        return createTaggedStatementAdapter();
      }
      @Override
      public Adapter caseTagDeclaration(TagDeclaration object)
      {
        return createTagDeclarationAdapter();
      }
      @Override
      public Adapter casePatternStatement(PatternStatement object)
      {
        return createPatternStatementAdapter();
      }
      @Override
      public Adapter casePattern(Pattern object)
      {
        return createPatternAdapter();
      }
      @Override
      public Adapter caseAnyPattern(AnyPattern object)
      {
        return createAnyPatternAdapter();
      }
      @Override
      public Adapter caseConstantPattern(ConstantPattern object)
      {
        return createConstantPatternAdapter();
      }
      @Override
      public Adapter caseIntegerPattern(IntegerPattern object)
      {
        return createIntegerPatternAdapter();
      }
      @Override
      public Adapter caseIdentifierPattern(IdentifierPattern object)
      {
        return createIdentifierPatternAdapter();
      }
      @Override
      public Adapter caseConditionalStatement(ConditionalStatement object)
      {
        return createConditionalStatementAdapter();
      }
      @Override
      public Adapter caseExtendedConditionalStatement(ExtendedConditionalStatement object)
      {
        return createExtendedConditionalStatementAdapter();
      }
      @Override
      public Adapter caseSelectStatement(SelectStatement object)
      {
        return createSelectStatementAdapter();
      }
      @Override
      public Adapter caseWhileStatement(WhileStatement object)
      {
        return createWhileStatementAdapter();
      }
      @Override
      public Adapter caseForeachStatement(ForeachStatement object)
      {
        return createForeachStatementAdapter();
      }
      @Override
      public Adapter caseToStatement(ToStatement object)
      {
        return createToStatementAdapter();
      }
      @Override
      public Adapter caseCaseStatement(CaseStatement object)
      {
        return createCaseStatementAdapter();
      }
      @Override
      public Adapter caseLoopStatement(LoopStatement object)
      {
        return createLoopStatementAdapter();
      }
      @Override
      public Adapter caseOnStatement(OnStatement object)
      {
        return createOnStatementAdapter();
      }
      @Override
      public Adapter caseWaitStatement(WaitStatement object)
      {
        return createWaitStatementAdapter();
      }
      @Override
      public Adapter caseComposition(Composition object)
      {
        return createCompositionAdapter();
      }
      @Override
      public Adapter caseBlock(Block object)
      {
        return createBlockAdapter();
      }
      @Override
      public Adapter caseCompositeBlock(CompositeBlock object)
      {
        return createCompositeBlockAdapter();
      }
      @Override
      public Adapter caseInstanceDeclaration(InstanceDeclaration object)
      {
        return createInstanceDeclarationAdapter();
      }
      @Override
      public Adapter casePortSet(PortSet object)
      {
        return createPortSetAdapter();
      }
      @Override
      public Adapter caseComponentInstance(ComponentInstance object)
      {
        return createComponentInstanceAdapter();
      }
      @Override
      public Adapter caseGenericInstance(GenericInstance object)
      {
        return createGenericInstanceAdapter();
      }
      @Override
      public Adapter caseTypeInstance(TypeInstance object)
      {
        return createTypeInstanceAdapter();
      }
      @Override
      public Adapter caseConstantInstance(ConstantInstance object)
      {
        return createConstantInstanceAdapter();
      }
      @Override
      public Adapter caseUnionTagInstance(UnionTagInstance object)
      {
        return createUnionTagInstanceAdapter();
      }
      @Override
      public Adapter caseRecordFieldInstance(RecordFieldInstance object)
      {
        return createRecordFieldInstanceAdapter();
      }
      @Override
      public Adapter caseExpression(Expression object)
      {
        return createExpressionAdapter();
      }
      @Override
      public Adapter caseReferenceExpression(ReferenceExpression object)
      {
        return createReferenceExpressionAdapter();
      }
      @Override
      public Adapter caseIdentifierExpression(IdentifierExpression object)
      {
        return createIdentifierExpressionAdapter();
      }
      @Override
      public Adapter caseRecordExpression(RecordExpression object)
      {
        return createRecordExpressionAdapter();
      }
      @Override
      public Adapter caseFieldExpression(FieldExpression object)
      {
        return createFieldExpressionAdapter();
      }
      @Override
      public Adapter caseQueueExpression(QueueExpression object)
      {
        return createQueueExpressionAdapter();
      }
      @Override
      public Adapter caseEnqueueExpression(EnqueueExpression object)
      {
        return createEnqueueExpressionAdapter();
      }
      @Override
      public Adapter caseAppendExpression(AppendExpression object)
      {
        return createAppendExpressionAdapter();
      }
      @Override
      public Adapter caseLiteralExpression(LiteralExpression object)
      {
        return createLiteralExpressionAdapter();
      }
      @Override
      public Adapter caseBooleanLiteral(BooleanLiteral object)
      {
        return createBooleanLiteralAdapter();
      }
      @Override
      public Adapter caseNaturalLiteral(NaturalLiteral object)
      {
        return createNaturalLiteralAdapter();
      }
      @Override
      public Adapter caseLowerBound(LowerBound object)
      {
        return createLowerBoundAdapter();
      }
      @Override
      public Adapter caseUpperBound(UpperBound object)
      {
        return createUpperBoundAdapter();
      }
      @Override
      public Adapter caseNaturalLowerBound(NaturalLowerBound object)
      {
        return createNaturalLowerBoundAdapter();
      }
      @Override
      public Adapter caseNaturalUpperBound(NaturalUpperBound object)
      {
        return createNaturalUpperBoundAdapter();
      }
      @Override
      public Adapter caseDecimalLowerBound(DecimalLowerBound object)
      {
        return createDecimalLowerBoundAdapter();
      }
      @Override
      public Adapter caseDecimalUpperBound(DecimalUpperBound object)
      {
        return createDecimalUpperBoundAdapter();
      }
      @Override
      public Adapter caseVariableLowerBound(VariableLowerBound object)
      {
        return createVariableLowerBoundAdapter();
      }
      @Override
      public Adapter caseVariableUpperBound(VariableUpperBound object)
      {
        return createVariableUpperBoundAdapter();
      }
      @Override
      public Adapter caseInfiniteUpperBound(InfiniteUpperBound object)
      {
        return createInfiniteUpperBoundAdapter();
      }
      @Override
      public Adapter caseRequirement(Requirement object)
      {
        return createRequirementAdapter();
      }
      @Override
      public Adapter casePropertyDeclaration(PropertyDeclaration object)
      {
        return createPropertyDeclarationAdapter();
      }
      @Override
      public Adapter caseProperty(Property object)
      {
        return createPropertyAdapter();
      }
      @Override
      public Adapter casePatternProperty(PatternProperty object)
      {
        return createPatternPropertyAdapter();
      }
      @Override
      public Adapter caseLTLPattern(LTLPattern object)
      {
        return createLTLPatternAdapter();
      }
      @Override
      public Adapter caseDeadlockFreePattern(DeadlockFreePattern object)
      {
        return createDeadlockFreePatternAdapter();
      }
      @Override
      public Adapter caseInfinitelyOftenPattern(InfinitelyOftenPattern object)
      {
        return createInfinitelyOftenPatternAdapter();
      }
      @Override
      public Adapter caseMortalPattern(MortalPattern object)
      {
        return createMortalPatternAdapter();
      }
      @Override
      public Adapter casePresencePattern(PresencePattern object)
      {
        return createPresencePatternAdapter();
      }
      @Override
      public Adapter caseAbsencePattern(AbsencePattern object)
      {
        return createAbsencePatternAdapter();
      }
      @Override
      public Adapter caseAlwaysPattern(AlwaysPattern object)
      {
        return createAlwaysPatternAdapter();
      }
      @Override
      public Adapter caseSequencePattern(SequencePattern object)
      {
        return createSequencePatternAdapter();
      }
      @Override
      public Adapter caseLTLProperty(LTLProperty object)
      {
        return createLTLPropertyAdapter();
      }
      @Override
      public Adapter caseStateEvent(StateEvent object)
      {
        return createStateEventAdapter();
      }
      @Override
      public Adapter caseEnterStateEvent(EnterStateEvent object)
      {
        return createEnterStateEventAdapter();
      }
      @Override
      public Adapter caseLeaveStateEvent(LeaveStateEvent object)
      {
        return createLeaveStateEventAdapter();
      }
      @Override
      public Adapter caseObservable(Observable object)
      {
        return createObservableAdapter();
      }
      @Override
      public Adapter caseObservableEvent(ObservableEvent object)
      {
        return createObservableEventAdapter();
      }
      @Override
      public Adapter casePathEvent(PathEvent object)
      {
        return createPathEventAdapter();
      }
      @Override
      public Adapter casePath(Path object)
      {
        return createPathAdapter();
      }
      @Override
      public Adapter casePathItem(PathItem object)
      {
        return createPathItemAdapter();
      }
      @Override
      public Adapter caseNaturalItem(NaturalItem object)
      {
        return createNaturalItemAdapter();
      }
      @Override
      public Adapter caseNamedItem(NamedItem object)
      {
        return createNamedItemAdapter();
      }
      @Override
      public Adapter caseSubject(Subject object)
      {
        return createSubjectAdapter();
      }
      @Override
      public Adapter caseStateSubject(StateSubject object)
      {
        return createStateSubjectAdapter();
      }
      @Override
      public Adapter caseValueSubject(ValueSubject object)
      {
        return createValueSubjectAdapter();
      }
      @Override
      public Adapter caseTagSubject(TagSubject object)
      {
        return createTagSubjectAdapter();
      }
      @Override
      public Adapter caseEventSubject(EventSubject object)
      {
        return createEventSubjectAdapter();
      }
      @Override
      public Adapter caseTupleType(TupleType object)
      {
        return createTupleTypeAdapter();
      }
      @Override
      public Adapter caseNaturalType(NaturalType object)
      {
        return createNaturalTypeAdapter();
      }
      @Override
      public Adapter caseIntegerType(IntegerType object)
      {
        return createIntegerTypeAdapter();
      }
      @Override
      public Adapter caseBooleanType(BooleanType object)
      {
        return createBooleanTypeAdapter();
      }
      @Override
      public Adapter caseUnlessStatement(UnlessStatement object)
      {
        return createUnlessStatementAdapter();
      }
      @Override
      public Adapter caseStatementChoice(StatementChoice object)
      {
        return createStatementChoiceAdapter();
      }
      @Override
      public Adapter caseStatementSequence(StatementSequence object)
      {
        return createStatementSequenceAdapter();
      }
      @Override
      public Adapter caseAssignStatement(AssignStatement object)
      {
        return createAssignStatementAdapter();
      }
      @Override
      public Adapter caseSendStatement(SendStatement object)
      {
        return createSendStatementAdapter();
      }
      @Override
      public Adapter caseReceiveStatement(ReceiveStatement object)
      {
        return createReceiveStatementAdapter();
      }
      @Override
      public Adapter caseConstructorPattern(ConstructorPattern object)
      {
        return createConstructorPatternAdapter();
      }
      @Override
      public Adapter caseArrayAccessPattern(ArrayAccessPattern object)
      {
        return createArrayAccessPatternAdapter();
      }
      @Override
      public Adapter caseRecordAccessPattern(RecordAccessPattern object)
      {
        return createRecordAccessPatternAdapter();
      }
      @Override
      public Adapter caseAllExpression(AllExpression object)
      {
        return createAllExpressionAdapter();
      }
      @Override
      public Adapter caseExistsExpression(ExistsExpression object)
      {
        return createExistsExpressionAdapter();
      }
      @Override
      public Adapter caseConditional(Conditional object)
      {
        return createConditionalAdapter();
      }
      @Override
      public Adapter caseDisjunction(Disjunction object)
      {
        return createDisjunctionAdapter();
      }
      @Override
      public Adapter caseImplication(Implication object)
      {
        return createImplicationAdapter();
      }
      @Override
      public Adapter caseConjunction(Conjunction object)
      {
        return createConjunctionAdapter();
      }
      @Override
      public Adapter caseComparisonEqual(ComparisonEqual object)
      {
        return createComparisonEqualAdapter();
      }
      @Override
      public Adapter caseComparisonNotEqual(ComparisonNotEqual object)
      {
        return createComparisonNotEqualAdapter();
      }
      @Override
      public Adapter caseComparisonLesser(ComparisonLesser object)
      {
        return createComparisonLesserAdapter();
      }
      @Override
      public Adapter caseComparisonLesserEqual(ComparisonLesserEqual object)
      {
        return createComparisonLesserEqualAdapter();
      }
      @Override
      public Adapter caseComparisonGreater(ComparisonGreater object)
      {
        return createComparisonGreaterAdapter();
      }
      @Override
      public Adapter caseComparisonGreaterEqual(ComparisonGreaterEqual object)
      {
        return createComparisonGreaterEqualAdapter();
      }
      @Override
      public Adapter caseAddition(Addition object)
      {
        return createAdditionAdapter();
      }
      @Override
      public Adapter caseSubstraction(Substraction object)
      {
        return createSubstractionAdapter();
      }
      @Override
      public Adapter caseMultiplication(Multiplication object)
      {
        return createMultiplicationAdapter();
      }
      @Override
      public Adapter caseDivision(Division object)
      {
        return createDivisionAdapter();
      }
      @Override
      public Adapter caseModulo(Modulo object)
      {
        return createModuloAdapter();
      }
      @Override
      public Adapter caseUnaryPlusExpression(UnaryPlusExpression object)
      {
        return createUnaryPlusExpressionAdapter();
      }
      @Override
      public Adapter caseUnaryMinusExpression(UnaryMinusExpression object)
      {
        return createUnaryMinusExpressionAdapter();
      }
      @Override
      public Adapter caseUnaryNegationExpression(UnaryNegationExpression object)
      {
        return createUnaryNegationExpressionAdapter();
      }
      @Override
      public Adapter caseUnaryFirstExpression(UnaryFirstExpression object)
      {
        return createUnaryFirstExpressionAdapter();
      }
      @Override
      public Adapter caseUnaryLengthExpression(UnaryLengthExpression object)
      {
        return createUnaryLengthExpressionAdapter();
      }
      @Override
      public Adapter caseUnaryCoerceExpression(UnaryCoerceExpression object)
      {
        return createUnaryCoerceExpressionAdapter();
      }
      @Override
      public Adapter caseUnaryFullExpression(UnaryFullExpression object)
      {
        return createUnaryFullExpressionAdapter();
      }
      @Override
      public Adapter caseUnaryDeQueueExpression(UnaryDeQueueExpression object)
      {
        return createUnaryDeQueueExpressionAdapter();
      }
      @Override
      public Adapter caseUnaryEmptyExpression(UnaryEmptyExpression object)
      {
        return createUnaryEmptyExpressionAdapter();
      }
      @Override
      public Adapter caseProjection(Projection object)
      {
        return createProjectionAdapter();
      }
      @Override
      public Adapter caseArrayAccessExpression(ArrayAccessExpression object)
      {
        return createArrayAccessExpressionAdapter();
      }
      @Override
      public Adapter caseRecordAccessExpression(RecordAccessExpression object)
      {
        return createRecordAccessExpressionAdapter();
      }
      @Override
      public Adapter caseConstructionExpression(ConstructionExpression object)
      {
        return createConstructionExpressionAdapter();
      }
      @Override
      public Adapter caseExplicitArrayExpression(ExplicitArrayExpression object)
      {
        return createExplicitArrayExpressionAdapter();
      }
      @Override
      public Adapter caseImplicitArrayExpression(ImplicitArrayExpression object)
      {
        return createImplicitArrayExpressionAdapter();
      }
      @Override
      public Adapter caseTrueLiteral(TrueLiteral object)
      {
        return createTrueLiteralAdapter();
      }
      @Override
      public Adapter caseFalseLiteral(FalseLiteral object)
      {
        return createFalseLiteralAdapter();
      }
      @Override
      public Adapter caseAllProperty(AllProperty object)
      {
        return createAllPropertyAdapter();
      }
      @Override
      public Adapter caseExistsProperty(ExistsProperty object)
      {
        return createExistsPropertyAdapter();
      }
      @Override
      public Adapter casePropertyDisjunction(PropertyDisjunction object)
      {
        return createPropertyDisjunctionAdapter();
      }
      @Override
      public Adapter casePropertyImplication(PropertyImplication object)
      {
        return createPropertyImplicationAdapter();
      }
      @Override
      public Adapter casePropertyConjunction(PropertyConjunction object)
      {
        return createPropertyConjunctionAdapter();
      }
      @Override
      public Adapter casePropertyNegation(PropertyNegation object)
      {
        return createPropertyNegationAdapter();
      }
      @Override
      public Adapter caseLeadsToPattern(LeadsToPattern object)
      {
        return createLeadsToPatternAdapter();
      }
      @Override
      public Adapter casePrecedesPattern(PrecedesPattern object)
      {
        return createPrecedesPatternAdapter();
      }
      @Override
      public Adapter caseLTLAll(LTLAll object)
      {
        return createLTLAllAdapter();
      }
      @Override
      public Adapter caseLTLExists(LTLExists object)
      {
        return createLTLExistsAdapter();
      }
      @Override
      public Adapter caseLTLDisjunction(LTLDisjunction object)
      {
        return createLTLDisjunctionAdapter();
      }
      @Override
      public Adapter caseLTLImplication(LTLImplication object)
      {
        return createLTLImplicationAdapter();
      }
      @Override
      public Adapter caseLTLConjunction(LTLConjunction object)
      {
        return createLTLConjunctionAdapter();
      }
      @Override
      public Adapter caseLTLUntil(LTLUntil object)
      {
        return createLTLUntilAdapter();
      }
      @Override
      public Adapter caseLTLRelease(LTLRelease object)
      {
        return createLTLReleaseAdapter();
      }
      @Override
      public Adapter caseLTLUnaryNegation(LTLUnaryNegation object)
      {
        return createLTLUnaryNegationAdapter();
      }
      @Override
      public Adapter caseLTLUnaryNext(LTLUnaryNext object)
      {
        return createLTLUnaryNextAdapter();
      }
      @Override
      public Adapter caseLTLUnaryAlways(LTLUnaryAlways object)
      {
        return createLTLUnaryAlwaysAdapter();
      }
      @Override
      public Adapter caseLTLUnaryEventually(LTLUnaryEventually object)
      {
        return createLTLUnaryEventuallyAdapter();
      }
      @Override
      public Adapter caseLTLVariable(LTLVariable object)
      {
        return createLTLVariableAdapter();
      }
      @Override
      public Adapter caseObservableDisjunction(ObservableDisjunction object)
      {
        return createObservableDisjunctionAdapter();
      }
      @Override
      public Adapter caseObservableImplication(ObservableImplication object)
      {
        return createObservableImplicationAdapter();
      }
      @Override
      public Adapter caseObservableConjunction(ObservableConjunction object)
      {
        return createObservableConjunctionAdapter();
      }
      @Override
      public Adapter caseObservableNegation(ObservableNegation object)
      {
        return createObservableNegationAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Model
   * @generated
   */
  public Adapter createModelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RootDeclaration <em>Root Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RootDeclaration
   * @generated
   */
  public Adapter createRootDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NamedElement <em>Named Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NamedElement
   * @generated
   */
  public Adapter createNamedElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclarationUse <em>Type Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclarationUse
   * @generated
   */
  public Adapter createTypeDeclarationUseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclarationUse <em>Constant Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclarationUse
   * @generated
   */
  public Adapter createConstantDeclarationUseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExpressionDeclarationUse <em>Expression Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExpressionDeclarationUse
   * @generated
   */
  public Adapter createExpressionDeclarationUseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ReferenceDeclarationUse <em>Reference Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ReferenceDeclarationUse
   * @generated
   */
  public Adapter createReferenceDeclarationUseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagDeclarationUse <em>Union Tag Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTagDeclarationUse
   * @generated
   */
  public Adapter createUnionTagDeclarationUseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldDeclarationUse <em>Record Field Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldDeclarationUse
   * @generated
   */
  public Adapter createRecordFieldDeclarationUseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PatternDeclarationUse <em>Pattern Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PatternDeclarationUse
   * @generated
   */
  public Adapter createPatternDeclarationUseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.BoundDeclarationUse <em>Bound Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.BoundDeclarationUse
   * @generated
   */
  public Adapter createBoundDeclarationUseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PathDeclarationUse <em>Path Declaration Use</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PathDeclarationUse
   * @generated
   */
  public Adapter createPathDeclarationUseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ImportDeclaration <em>Import Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ImportDeclaration
   * @generated
   */
  public Adapter createImportDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Declaration <em>Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Declaration
   * @generated
   */
  public Adapter createDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParameterizedDeclaration <em>Parameterized Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParameterizedDeclaration
   * @generated
   */
  public Adapter createParameterizedDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration <em>Type Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration
   * @generated
   */
  public Adapter createTypeDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ChannelDeclaration <em>Channel Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ChannelDeclaration
   * @generated
   */
  public Adapter createChannelDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ChannelType <em>Channel Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ChannelType
   * @generated
   */
  public Adapter createChannelTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Type
   * @generated
   */
  public Adapter createTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.BasicType <em>Basic Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.BasicType
   * @generated
   */
  public Adapter createBasicTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RangeType <em>Range Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RangeType
   * @generated
   */
  public Adapter createRangeTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionType <em>Union Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionType
   * @generated
   */
  public Adapter createUnionTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTags <em>Union Tags</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTags
   * @generated
   */
  public Adapter createUnionTagsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagDeclaration <em>Union Tag Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTagDeclaration
   * @generated
   */
  public Adapter createUnionTagDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordType <em>Record Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordType
   * @generated
   */
  public Adapter createRecordTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFields <em>Record Fields</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordFields
   * @generated
   */
  public Adapter createRecordFieldsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldDeclaration <em>Record Field Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldDeclaration
   * @generated
   */
  public Adapter createRecordFieldDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.QueueType <em>Queue Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.QueueType
   * @generated
   */
  public Adapter createQueueTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayType <em>Array Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ArrayType
   * @generated
   */
  public Adapter createArrayTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ReferencedType <em>Referenced Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ReferencedType
   * @generated
   */
  public Adapter createReferencedTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclaration <em>Constant Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclaration
   * @generated
   */
  public Adapter createConstantDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration <em>Process Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration
   * @generated
   */
  public Adapter createProcessDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericDeclaration <em>Generic Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericDeclaration
   * @generated
   */
  public Adapter createGenericDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericTypeDeclaration <em>Generic Type Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericTypeDeclaration
   * @generated
   */
  public Adapter createGenericTypeDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericConstantDeclaration <em>Generic Constant Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericConstantDeclaration
   * @generated
   */
  public Adapter createGenericConstantDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericUnionTagDeclaration <em>Generic Union Tag Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericUnionTagDeclaration
   * @generated
   */
  public Adapter createGenericUnionTagDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericRecordFieldDeclaration <em>Generic Record Field Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericRecordFieldDeclaration
   * @generated
   */
  public Adapter createGenericRecordFieldDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.StateDeclaration <em>State Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StateDeclaration
   * @generated
   */
  public Adapter createStateDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Transition <em>Transition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Transition
   * @generated
   */
  public Adapter createTransitionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration <em>Component Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration
   * @generated
   */
  public Adapter createComponentDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PortsDeclaration <em>Ports Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PortsDeclaration
   * @generated
   */
  public Adapter createPortsDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration <em>Port Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration
   * @generated
   */
  public Adapter createPortDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration <em>Local Ports Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration
   * @generated
   */
  public Adapter createLocalPortsDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration <em>Parameters Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration
   * @generated
   */
  public Adapter createParametersDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration <em>Parameter Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration
   * @generated
   */
  public Adapter createParameterDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration <em>Variables Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration
   * @generated
   */
  public Adapter createVariablesDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableDeclaration <em>Variable Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariableDeclaration
   * @generated
   */
  public Adapter createVariableDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PriorityDeclaration <em>Priority Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PriorityDeclaration
   * @generated
   */
  public Adapter createPriorityDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PriorityGroup <em>Priority Group</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PriorityGroup
   * @generated
   */
  public Adapter createPriorityGroupAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Statement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Statement
   * @generated
   */
  public Adapter createStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NullStatement <em>Null Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NullStatement
   * @generated
   */
  public Adapter createNullStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TaggedStatement <em>Tagged Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TaggedStatement
   * @generated
   */
  public Adapter createTaggedStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TagDeclaration <em>Tag Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TagDeclaration
   * @generated
   */
  public Adapter createTagDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PatternStatement <em>Pattern Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PatternStatement
   * @generated
   */
  public Adapter createPatternStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Pattern <em>Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Pattern
   * @generated
   */
  public Adapter createPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AnyPattern <em>Any Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AnyPattern
   * @generated
   */
  public Adapter createAnyPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantPattern <em>Constant Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantPattern
   * @generated
   */
  public Adapter createConstantPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.IntegerPattern <em>Integer Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.IntegerPattern
   * @generated
   */
  public Adapter createIntegerPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.IdentifierPattern <em>Identifier Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.IdentifierPattern
   * @generated
   */
  public Adapter createIdentifierPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement <em>Conditional Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement
   * @generated
   */
  public Adapter createConditionalStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExtendedConditionalStatement <em>Extended Conditional Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExtendedConditionalStatement
   * @generated
   */
  public Adapter createExtendedConditionalStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.SelectStatement <em>Select Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.SelectStatement
   * @generated
   */
  public Adapter createSelectStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.WhileStatement <em>While Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.WhileStatement
   * @generated
   */
  public Adapter createWhileStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ForeachStatement <em>Foreach Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ForeachStatement
   * @generated
   */
  public Adapter createForeachStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ToStatement <em>To Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ToStatement
   * @generated
   */
  public Adapter createToStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.CaseStatement <em>Case Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.CaseStatement
   * @generated
   */
  public Adapter createCaseStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LoopStatement <em>Loop Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LoopStatement
   * @generated
   */
  public Adapter createLoopStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.OnStatement <em>On Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.OnStatement
   * @generated
   */
  public Adapter createOnStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.WaitStatement <em>Wait Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.WaitStatement
   * @generated
   */
  public Adapter createWaitStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Composition <em>Composition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Composition
   * @generated
   */
  public Adapter createCompositionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Block <em>Block</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Block
   * @generated
   */
  public Adapter createBlockAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.CompositeBlock <em>Composite Block</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.CompositeBlock
   * @generated
   */
  public Adapter createCompositeBlockAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration <em>Instance Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.InstanceDeclaration
   * @generated
   */
  public Adapter createInstanceDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PortSet <em>Port Set</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PortSet
   * @generated
   */
  public Adapter createPortSetAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance <em>Component Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance
   * @generated
   */
  public Adapter createComponentInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.GenericInstance <em>Generic Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.GenericInstance
   * @generated
   */
  public Adapter createGenericInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TypeInstance <em>Type Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TypeInstance
   * @generated
   */
  public Adapter createTypeInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance <em>Constant Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance
   * @generated
   */
  public Adapter createConstantInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance <em>Union Tag Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance
   * @generated
   */
  public Adapter createUnionTagInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldInstance <em>Record Field Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordFieldInstance
   * @generated
   */
  public Adapter createRecordFieldInstanceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Expression
   * @generated
   */
  public Adapter createExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ReferenceExpression <em>Reference Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ReferenceExpression
   * @generated
   */
  public Adapter createReferenceExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.IdentifierExpression <em>Identifier Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.IdentifierExpression
   * @generated
   */
  public Adapter createIdentifierExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordExpression <em>Record Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordExpression
   * @generated
   */
  public Adapter createRecordExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.FieldExpression <em>Field Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FieldExpression
   * @generated
   */
  public Adapter createFieldExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.QueueExpression <em>Queue Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.QueueExpression
   * @generated
   */
  public Adapter createQueueExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.EnqueueExpression <em>Enqueue Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.EnqueueExpression
   * @generated
   */
  public Adapter createEnqueueExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AppendExpression <em>Append Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AppendExpression
   * @generated
   */
  public Adapter createAppendExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LiteralExpression <em>Literal Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LiteralExpression
   * @generated
   */
  public Adapter createLiteralExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.BooleanLiteral <em>Boolean Literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.BooleanLiteral
   * @generated
   */
  public Adapter createBooleanLiteralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalLiteral <em>Natural Literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalLiteral
   * @generated
   */
  public Adapter createNaturalLiteralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LowerBound <em>Lower Bound</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LowerBound
   * @generated
   */
  public Adapter createLowerBoundAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UpperBound <em>Upper Bound</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UpperBound
   * @generated
   */
  public Adapter createUpperBoundAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalLowerBound <em>Natural Lower Bound</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalLowerBound
   * @generated
   */
  public Adapter createNaturalLowerBoundAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalUpperBound <em>Natural Upper Bound</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalUpperBound
   * @generated
   */
  public Adapter createNaturalUpperBoundAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.DecimalLowerBound <em>Decimal Lower Bound</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.DecimalLowerBound
   * @generated
   */
  public Adapter createDecimalLowerBoundAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.DecimalUpperBound <em>Decimal Upper Bound</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.DecimalUpperBound
   * @generated
   */
  public Adapter createDecimalUpperBoundAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableLowerBound <em>Variable Lower Bound</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariableLowerBound
   * @generated
   */
  public Adapter createVariableLowerBoundAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound <em>Variable Upper Bound</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound
   * @generated
   */
  public Adapter createVariableUpperBoundAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.InfiniteUpperBound <em>Infinite Upper Bound</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.InfiniteUpperBound
   * @generated
   */
  public Adapter createInfiniteUpperBoundAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Requirement <em>Requirement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Requirement
   * @generated
   */
  public Adapter createRequirementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyDeclaration <em>Property Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyDeclaration
   * @generated
   */
  public Adapter createPropertyDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Property <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Property
   * @generated
   */
  public Adapter createPropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PatternProperty <em>Pattern Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PatternProperty
   * @generated
   */
  public Adapter createPatternPropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLPattern <em>LTL Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLPattern
   * @generated
   */
  public Adapter createLTLPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.DeadlockFreePattern <em>Deadlock Free Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.DeadlockFreePattern
   * @generated
   */
  public Adapter createDeadlockFreePatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.InfinitelyOftenPattern <em>Infinitely Often Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.InfinitelyOftenPattern
   * @generated
   */
  public Adapter createInfinitelyOftenPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.MortalPattern <em>Mortal Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.MortalPattern
   * @generated
   */
  public Adapter createMortalPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern <em>Presence Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern
   * @generated
   */
  public Adapter createPresencePatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern <em>Absence Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AbsencePattern
   * @generated
   */
  public Adapter createAbsencePatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern <em>Always Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AlwaysPattern
   * @generated
   */
  public Adapter createAlwaysPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.SequencePattern <em>Sequence Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.SequencePattern
   * @generated
   */
  public Adapter createSequencePatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLProperty <em>LTL Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLProperty
   * @generated
   */
  public Adapter createLTLPropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.StateEvent <em>State Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StateEvent
   * @generated
   */
  public Adapter createStateEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.EnterStateEvent <em>Enter State Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.EnterStateEvent
   * @generated
   */
  public Adapter createEnterStateEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LeaveStateEvent <em>Leave State Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LeaveStateEvent
   * @generated
   */
  public Adapter createLeaveStateEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Observable <em>Observable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Observable
   * @generated
   */
  public Adapter createObservableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableEvent <em>Observable Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableEvent
   * @generated
   */
  public Adapter createObservableEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PathEvent <em>Path Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PathEvent
   * @generated
   */
  public Adapter createPathEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Path <em>Path</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Path
   * @generated
   */
  public Adapter createPathAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PathItem <em>Path Item</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PathItem
   * @generated
   */
  public Adapter createPathItemAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalItem <em>Natural Item</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalItem
   * @generated
   */
  public Adapter createNaturalItemAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NamedItem <em>Named Item</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NamedItem
   * @generated
   */
  public Adapter createNamedItemAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Subject <em>Subject</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Subject
   * @generated
   */
  public Adapter createSubjectAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.StateSubject <em>State Subject</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StateSubject
   * @generated
   */
  public Adapter createStateSubjectAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ValueSubject <em>Value Subject</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ValueSubject
   * @generated
   */
  public Adapter createValueSubjectAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TagSubject <em>Tag Subject</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TagSubject
   * @generated
   */
  public Adapter createTagSubjectAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.EventSubject <em>Event Subject</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.EventSubject
   * @generated
   */
  public Adapter createEventSubjectAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TupleType <em>Tuple Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TupleType
   * @generated
   */
  public Adapter createTupleTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.NaturalType <em>Natural Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.NaturalType
   * @generated
   */
  public Adapter createNaturalTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.IntegerType <em>Integer Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.IntegerType
   * @generated
   */
  public Adapter createIntegerTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.BooleanType <em>Boolean Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.BooleanType
   * @generated
   */
  public Adapter createBooleanTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnlessStatement <em>Unless Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnlessStatement
   * @generated
   */
  public Adapter createUnlessStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.StatementChoice <em>Statement Choice</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StatementChoice
   * @generated
   */
  public Adapter createStatementChoiceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.StatementSequence <em>Statement Sequence</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.StatementSequence
   * @generated
   */
  public Adapter createStatementSequenceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AssignStatement <em>Assign Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AssignStatement
   * @generated
   */
  public Adapter createAssignStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.SendStatement <em>Send Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.SendStatement
   * @generated
   */
  public Adapter createSendStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ReceiveStatement <em>Receive Statement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ReceiveStatement
   * @generated
   */
  public Adapter createReceiveStatementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstructorPattern <em>Constructor Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstructorPattern
   * @generated
   */
  public Adapter createConstructorPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessPattern <em>Array Access Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessPattern
   * @generated
   */
  public Adapter createArrayAccessPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessPattern <em>Record Access Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessPattern
   * @generated
   */
  public Adapter createRecordAccessPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AllExpression <em>All Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AllExpression
   * @generated
   */
  public Adapter createAllExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExistsExpression <em>Exists Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExistsExpression
   * @generated
   */
  public Adapter createExistsExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Conditional <em>Conditional</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Conditional
   * @generated
   */
  public Adapter createConditionalAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Disjunction <em>Disjunction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Disjunction
   * @generated
   */
  public Adapter createDisjunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Implication <em>Implication</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Implication
   * @generated
   */
  public Adapter createImplicationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Conjunction <em>Conjunction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Conjunction
   * @generated
   */
  public Adapter createConjunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonEqual <em>Comparison Equal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonEqual
   * @generated
   */
  public Adapter createComparisonEqualAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonNotEqual <em>Comparison Not Equal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonNotEqual
   * @generated
   */
  public Adapter createComparisonNotEqualAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesser <em>Comparison Lesser</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesser
   * @generated
   */
  public Adapter createComparisonLesserAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesserEqual <em>Comparison Lesser Equal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonLesserEqual
   * @generated
   */
  public Adapter createComparisonLesserEqualAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreater <em>Comparison Greater</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreater
   * @generated
   */
  public Adapter createComparisonGreaterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreaterEqual <em>Comparison Greater Equal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ComparisonGreaterEqual
   * @generated
   */
  public Adapter createComparisonGreaterEqualAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Addition <em>Addition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Addition
   * @generated
   */
  public Adapter createAdditionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Substraction <em>Substraction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Substraction
   * @generated
   */
  public Adapter createSubstractionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Multiplication <em>Multiplication</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Multiplication
   * @generated
   */
  public Adapter createMultiplicationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Division <em>Division</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Division
   * @generated
   */
  public Adapter createDivisionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Modulo <em>Modulo</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Modulo
   * @generated
   */
  public Adapter createModuloAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryPlusExpression <em>Unary Plus Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryPlusExpression
   * @generated
   */
  public Adapter createUnaryPlusExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryMinusExpression <em>Unary Minus Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryMinusExpression
   * @generated
   */
  public Adapter createUnaryMinusExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryNegationExpression <em>Unary Negation Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryNegationExpression
   * @generated
   */
  public Adapter createUnaryNegationExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryFirstExpression <em>Unary First Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryFirstExpression
   * @generated
   */
  public Adapter createUnaryFirstExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryLengthExpression <em>Unary Length Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryLengthExpression
   * @generated
   */
  public Adapter createUnaryLengthExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryCoerceExpression <em>Unary Coerce Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryCoerceExpression
   * @generated
   */
  public Adapter createUnaryCoerceExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryFullExpression <em>Unary Full Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryFullExpression
   * @generated
   */
  public Adapter createUnaryFullExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryDeQueueExpression <em>Unary De Queue Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryDeQueueExpression
   * @generated
   */
  public Adapter createUnaryDeQueueExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnaryEmptyExpression <em>Unary Empty Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.UnaryEmptyExpression
   * @generated
   */
  public Adapter createUnaryEmptyExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.Projection <em>Projection</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.Projection
   * @generated
   */
  public Adapter createProjectionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessExpression <em>Array Access Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessExpression
   * @generated
   */
  public Adapter createArrayAccessExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessExpression <em>Record Access Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessExpression
   * @generated
   */
  public Adapter createRecordAccessExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstructionExpression <em>Construction Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ConstructionExpression
   * @generated
   */
  public Adapter createConstructionExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExplicitArrayExpression <em>Explicit Array Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExplicitArrayExpression
   * @generated
   */
  public Adapter createExplicitArrayExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ImplicitArrayExpression <em>Implicit Array Expression</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ImplicitArrayExpression
   * @generated
   */
  public Adapter createImplicitArrayExpressionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.TrueLiteral <em>True Literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.TrueLiteral
   * @generated
   */
  public Adapter createTrueLiteralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.FalseLiteral <em>False Literal</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FalseLiteral
   * @generated
   */
  public Adapter createFalseLiteralAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.AllProperty <em>All Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.AllProperty
   * @generated
   */
  public Adapter createAllPropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ExistsProperty <em>Exists Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ExistsProperty
   * @generated
   */
  public Adapter createExistsPropertyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyDisjunction <em>Property Disjunction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyDisjunction
   * @generated
   */
  public Adapter createPropertyDisjunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyImplication <em>Property Implication</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyImplication
   * @generated
   */
  public Adapter createPropertyImplicationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyConjunction <em>Property Conjunction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyConjunction
   * @generated
   */
  public Adapter createPropertyConjunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PropertyNegation <em>Property Negation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PropertyNegation
   * @generated
   */
  public Adapter createPropertyNegationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern <em>Leads To Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LeadsToPattern
   * @generated
   */
  public Adapter createLeadsToPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern <em>Precedes Pattern</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.PrecedesPattern
   * @generated
   */
  public Adapter createPrecedesPatternAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLAll <em>LTL All</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLAll
   * @generated
   */
  public Adapter createLTLAllAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLExists <em>LTL Exists</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLExists
   * @generated
   */
  public Adapter createLTLExistsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLDisjunction <em>LTL Disjunction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLDisjunction
   * @generated
   */
  public Adapter createLTLDisjunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLImplication <em>LTL Implication</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLImplication
   * @generated
   */
  public Adapter createLTLImplicationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLConjunction <em>LTL Conjunction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLConjunction
   * @generated
   */
  public Adapter createLTLConjunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUntil <em>LTL Until</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUntil
   * @generated
   */
  public Adapter createLTLUntilAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLRelease <em>LTL Release</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLRelease
   * @generated
   */
  public Adapter createLTLReleaseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryNegation <em>LTL Unary Negation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryNegation
   * @generated
   */
  public Adapter createLTLUnaryNegationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryNext <em>LTL Unary Next</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryNext
   * @generated
   */
  public Adapter createLTLUnaryNextAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryAlways <em>LTL Unary Always</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryAlways
   * @generated
   */
  public Adapter createLTLUnaryAlwaysAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryEventually <em>LTL Unary Eventually</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLUnaryEventually
   * @generated
   */
  public Adapter createLTLUnaryEventuallyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLVariable <em>LTL Variable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.LTLVariable
   * @generated
   */
  public Adapter createLTLVariableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableDisjunction <em>Observable Disjunction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableDisjunction
   * @generated
   */
  public Adapter createObservableDisjunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableImplication <em>Observable Implication</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableImplication
   * @generated
   */
  public Adapter createObservableImplicationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableConjunction <em>Observable Conjunction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableConjunction
   * @generated
   */
  public Adapter createObservableConjunctionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irit.fiacre.etoile.xtext.fiacre.ObservableNegation <em>Observable Negation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.ObservableNegation
   * @generated
   */
  public Adapter createObservableNegationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //FiacreAdapterFactory
