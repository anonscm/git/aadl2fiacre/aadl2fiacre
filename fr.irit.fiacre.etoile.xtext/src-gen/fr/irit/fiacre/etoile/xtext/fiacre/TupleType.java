/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tuple Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.TupleType#getTypes <em>Types</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getTupleType()
 * @model
 * @generated
 */
public interface TupleType extends Type
{
  /**
   * Returns the value of the '<em><b>Types</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.Type}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Types</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Types</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getTupleType_Types()
   * @model containment="true"
   * @generated
   */
  EList<Type> getTypes();

} // TupleType
