/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LTL All</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLAll#getIndex <em>Index</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLAll#getType <em>Type</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLAll#getChild <em>Child</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLTLAll()
 * @model
 * @generated
 */
public interface LTLAll extends LTLProperty
{
  /**
   * Returns the value of the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Index</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Index</em>' containment reference.
   * @see #setIndex(VariableDeclaration)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLTLAll_Index()
   * @model containment="true"
   * @generated
   */
  VariableDeclaration getIndex();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLAll#getIndex <em>Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Index</em>' containment reference.
   * @see #getIndex()
   * @generated
   */
  void setIndex(VariableDeclaration value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(RangeType)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLTLAll_Type()
   * @model containment="true"
   * @generated
   */
  RangeType getType();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLAll#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(RangeType value);

  /**
   * Returns the value of the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Child</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Child</em>' containment reference.
   * @see #setChild(LTLProperty)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLTLAll_Child()
   * @model containment="true"
   * @generated
   */
  LTLProperty getChild();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.LTLAll#getChild <em>Child</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Child</em>' containment reference.
   * @see #getChild()
   * @generated
   */
  void setChild(LTLProperty value);

} // LTLAll
