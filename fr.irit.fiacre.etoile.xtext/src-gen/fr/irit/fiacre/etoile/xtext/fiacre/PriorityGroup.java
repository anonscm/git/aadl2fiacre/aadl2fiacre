/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Priority Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.PriorityGroup#getPorts <em>Ports</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPriorityGroup()
 * @model
 * @generated
 */
public interface PriorityGroup extends EObject
{
  /**
   * Returns the value of the '<em><b>Ports</b></em>' reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ports</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ports</em>' reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPriorityGroup_Ports()
   * @model
   * @generated
   */
  EList<PortDeclaration> getPorts();

} // PriorityGroup
