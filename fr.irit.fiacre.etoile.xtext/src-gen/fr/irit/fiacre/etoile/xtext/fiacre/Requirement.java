/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.Requirement#getProperty <em>Property</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.Requirement#getPositive <em>Positive</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.Requirement#getNegative <em>Negative</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRequirement()
 * @model
 * @generated
 */
public interface Requirement extends EObject
{
  /**
   * Returns the value of the '<em><b>Property</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Property</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Property</em>' reference.
   * @see #setProperty(PropertyDeclaration)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRequirement_Property()
   * @model
   * @generated
   */
  PropertyDeclaration getProperty();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.Requirement#getProperty <em>Property</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Property</em>' reference.
   * @see #getProperty()
   * @generated
   */
  void setProperty(PropertyDeclaration value);

  /**
   * Returns the value of the '<em><b>Positive</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Positive</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Positive</em>' attribute.
   * @see #setPositive(String)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRequirement_Positive()
   * @model
   * @generated
   */
  String getPositive();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.Requirement#getPositive <em>Positive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Positive</em>' attribute.
   * @see #getPositive()
   * @generated
   */
  void setPositive(String value);

  /**
   * Returns the value of the '<em><b>Negative</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Negative</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Negative</em>' attribute.
   * @see #setNegative(String)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRequirement_Negative()
   * @model
   * @generated
   */
  String getNegative();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.Requirement#getNegative <em>Negative</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Negative</em>' attribute.
   * @see #getNegative()
   * @generated
   */
  void setNegative(String value);

} // Requirement
