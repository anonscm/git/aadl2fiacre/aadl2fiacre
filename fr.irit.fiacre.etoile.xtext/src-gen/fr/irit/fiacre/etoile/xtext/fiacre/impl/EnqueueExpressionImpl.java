/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.EnqueueExpression;
import fr.irit.fiacre.etoile.xtext.fiacre.Expression;
import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enqueue Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.EnqueueExpressionImpl#getElement <em>Element</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.EnqueueExpressionImpl#getQueue <em>Queue</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnqueueExpressionImpl extends ExpressionImpl implements EnqueueExpression
{
  /**
   * The cached value of the '{@link #getElement() <em>Element</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElement()
   * @generated
   * @ordered
   */
  protected Expression element;

  /**
   * The cached value of the '{@link #getQueue() <em>Queue</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getQueue()
   * @generated
   * @ordered
   */
  protected Expression queue;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EnqueueExpressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FiacrePackage.eINSTANCE.getEnqueueExpression();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getElement()
  {
    return element;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetElement(Expression newElement, NotificationChain msgs)
  {
    Expression oldElement = element;
    element = newElement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.ENQUEUE_EXPRESSION__ELEMENT, oldElement, newElement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setElement(Expression newElement)
  {
    if (newElement != element)
    {
      NotificationChain msgs = null;
      if (element != null)
        msgs = ((InternalEObject)element).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.ENQUEUE_EXPRESSION__ELEMENT, null, msgs);
      if (newElement != null)
        msgs = ((InternalEObject)newElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.ENQUEUE_EXPRESSION__ELEMENT, null, msgs);
      msgs = basicSetElement(newElement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.ENQUEUE_EXPRESSION__ELEMENT, newElement, newElement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getQueue()
  {
    return queue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetQueue(Expression newQueue, NotificationChain msgs)
  {
    Expression oldQueue = queue;
    queue = newQueue;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.ENQUEUE_EXPRESSION__QUEUE, oldQueue, newQueue);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setQueue(Expression newQueue)
  {
    if (newQueue != queue)
    {
      NotificationChain msgs = null;
      if (queue != null)
        msgs = ((InternalEObject)queue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.ENQUEUE_EXPRESSION__QUEUE, null, msgs);
      if (newQueue != null)
        msgs = ((InternalEObject)newQueue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.ENQUEUE_EXPRESSION__QUEUE, null, msgs);
      msgs = basicSetQueue(newQueue, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.ENQUEUE_EXPRESSION__QUEUE, newQueue, newQueue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case FiacrePackage.ENQUEUE_EXPRESSION__ELEMENT:
        return basicSetElement(null, msgs);
      case FiacrePackage.ENQUEUE_EXPRESSION__QUEUE:
        return basicSetQueue(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case FiacrePackage.ENQUEUE_EXPRESSION__ELEMENT:
        return getElement();
      case FiacrePackage.ENQUEUE_EXPRESSION__QUEUE:
        return getQueue();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case FiacrePackage.ENQUEUE_EXPRESSION__ELEMENT:
        setElement((Expression)newValue);
        return;
      case FiacrePackage.ENQUEUE_EXPRESSION__QUEUE:
        setQueue((Expression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.ENQUEUE_EXPRESSION__ELEMENT:
        setElement((Expression)null);
        return;
      case FiacrePackage.ENQUEUE_EXPRESSION__QUEUE:
        setQueue((Expression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.ENQUEUE_EXPRESSION__ELEMENT:
        return element != null;
      case FiacrePackage.ENQUEUE_EXPRESSION__QUEUE:
        return queue != null;
    }
    return super.eIsSet(featureID);
  }

} //EnqueueExpressionImpl
