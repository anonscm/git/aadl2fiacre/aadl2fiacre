/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constant Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance#getConst <em>Const</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getConstantInstance()
 * @model
 * @generated
 */
public interface ConstantInstance extends GenericInstance
{
  /**
   * Returns the value of the '<em><b>Const</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Const</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Const</em>' reference.
   * @see #setConst(ConstantDeclarationUse)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getConstantInstance_Const()
   * @model
   * @generated
   */
  ConstantDeclarationUse getConst();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance#getConst <em>Const</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Const</em>' reference.
   * @see #getConst()
   * @generated
   */
  void setConst(ConstantDeclarationUse value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(LiteralExpression)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getConstantInstance_Value()
   * @model containment="true"
   * @generated
   */
  LiteralExpression getValue();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantInstance#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(LiteralExpression value);

} // ConstantInstance
