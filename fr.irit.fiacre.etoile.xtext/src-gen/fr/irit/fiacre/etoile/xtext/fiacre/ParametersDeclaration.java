/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameters Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#getParameters <em>Parameters</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#isRead <em>Read</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#isWrite <em>Write</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getParametersDeclaration()
 * @model
 * @generated
 */
public interface ParametersDeclaration extends EObject
{
  /**
   * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parameters</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getParametersDeclaration_Parameters()
   * @model containment="true"
   * @generated
   */
  EList<ParameterDeclaration> getParameters();

  /**
   * Returns the value of the '<em><b>Read</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Read</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Read</em>' attribute.
   * @see #setRead(boolean)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getParametersDeclaration_Read()
   * @model
   * @generated
   */
  boolean isRead();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#isRead <em>Read</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Read</em>' attribute.
   * @see #isRead()
   * @generated
   */
  void setRead(boolean value);

  /**
   * Returns the value of the '<em><b>Write</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Write</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Write</em>' attribute.
   * @see #setWrite(boolean)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getParametersDeclaration_Write()
   * @model
   * @generated
   */
  boolean isWrite();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#isWrite <em>Write</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Write</em>' attribute.
   * @see #isWrite()
   * @generated
   */
  void setWrite(boolean value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getParametersDeclaration_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

} // ParametersDeclaration
