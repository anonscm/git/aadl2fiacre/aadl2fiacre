/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.RangeType#getMinimum <em>Minimum</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.RangeType#getMaximum <em>Maximum</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.RangeType#getSize <em>Size</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRangeType()
 * @model
 * @generated
 */
public interface RangeType extends Type
{
  /**
   * Returns the value of the '<em><b>Minimum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Minimum</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Minimum</em>' containment reference.
   * @see #setMinimum(Expression)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRangeType_Minimum()
   * @model containment="true"
   * @generated
   */
  Expression getMinimum();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.RangeType#getMinimum <em>Minimum</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Minimum</em>' containment reference.
   * @see #getMinimum()
   * @generated
   */
  void setMinimum(Expression value);

  /**
   * Returns the value of the '<em><b>Maximum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Maximum</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Maximum</em>' containment reference.
   * @see #setMaximum(Expression)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRangeType_Maximum()
   * @model containment="true"
   * @generated
   */
  Expression getMaximum();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.RangeType#getMaximum <em>Maximum</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Maximum</em>' containment reference.
   * @see #getMaximum()
   * @generated
   */
  void setMaximum(Expression value);

  /**
   * Returns the value of the '<em><b>Size</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Size</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Size</em>' containment reference.
   * @see #setSize(Expression)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRangeType_Size()
   * @model containment="true"
   * @generated
   */
  Expression getSize();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.RangeType#getSize <em>Size</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Size</em>' containment reference.
   * @see #getSize()
   * @generated
   */
  void setSize(Expression value);

} // RangeType
