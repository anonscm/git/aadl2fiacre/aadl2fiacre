/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Union Tag Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance#isConstr0 <em>Constr0</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance#isConstr1 <em>Constr1</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance#getHead <em>Head</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getUnionTagInstance()
 * @model
 * @generated
 */
public interface UnionTagInstance extends GenericInstance
{
  /**
   * Returns the value of the '<em><b>Constr0</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Constr0</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Constr0</em>' attribute.
   * @see #setConstr0(boolean)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getUnionTagInstance_Constr0()
   * @model
   * @generated
   */
  boolean isConstr0();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance#isConstr0 <em>Constr0</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Constr0</em>' attribute.
   * @see #isConstr0()
   * @generated
   */
  void setConstr0(boolean value);

  /**
   * Returns the value of the '<em><b>Constr1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Constr1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Constr1</em>' attribute.
   * @see #setConstr1(boolean)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getUnionTagInstance_Constr1()
   * @model
   * @generated
   */
  boolean isConstr1();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance#isConstr1 <em>Constr1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Constr1</em>' attribute.
   * @see #isConstr1()
   * @generated
   */
  void setConstr1(boolean value);

  /**
   * Returns the value of the '<em><b>Head</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Head</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Head</em>' reference.
   * @see #setHead(UnionTagDeclarationUse)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getUnionTagInstance_Head()
   * @model
   * @generated
   */
  UnionTagDeclarationUse getHead();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.UnionTagInstance#getHead <em>Head</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Head</em>' reference.
   * @see #getHead()
   * @generated
   */
  void setHead(UnionTagDeclarationUse value);

} // UnionTagInstance
