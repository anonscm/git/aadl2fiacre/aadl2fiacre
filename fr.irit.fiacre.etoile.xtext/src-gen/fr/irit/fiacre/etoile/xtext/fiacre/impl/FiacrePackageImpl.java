/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.FiacreFactory;
import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;

import java.io.IOException;

import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FiacrePackageImpl extends EPackageImpl implements FiacrePackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected String packageFilename = "fiacre.loadinitialization_ecore";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass modelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass rootDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass namedElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeDeclarationUseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass constantDeclarationUseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionDeclarationUseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass referenceDeclarationUseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unionTagDeclarationUseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass recordFieldDeclarationUseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass patternDeclarationUseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boundDeclarationUseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass pathDeclarationUseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass declarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass parameterizedDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass channelDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass channelTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass basicTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass rangeTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unionTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unionTagsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unionTagDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass recordTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass recordFieldsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass recordFieldDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass queueTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass arrayTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass referencedTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass constantDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass processDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericTypeDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericConstantDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericUnionTagDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericRecordFieldDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass stateDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass transitionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portsDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass localPortsDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass parametersDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass parameterDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass variablesDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass variableDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass priorityDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass priorityGroupEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nullStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass taggedStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tagDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass patternStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass patternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass anyPatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass constantPatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerPatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass identifierPatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass conditionalStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass extendedConditionalStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass whileStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass foreachStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass toStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass caseStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass loopStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass onStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass waitStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass compositionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass blockEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass compositeBlockEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass instanceDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portSetEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass genericInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass constantInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unionTagInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass recordFieldInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass referenceExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass identifierExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass recordExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fieldExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass queueExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass enqueueExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass appendExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass literalExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanLiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass naturalLiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass lowerBoundEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass upperBoundEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass naturalLowerBoundEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass naturalUpperBoundEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass decimalLowerBoundEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass decimalUpperBoundEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass variableLowerBoundEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass variableUpperBoundEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass infiniteUpperBoundEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass requirementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass propertyDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass propertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass patternPropertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlPatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass deadlockFreePatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass infinitelyOftenPatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mortalPatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass presencePatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass absencePatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass alwaysPatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sequencePatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlPropertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass stateEventEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass enterStateEventEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass leaveStateEventEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass observableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass observableEventEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass pathEventEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass pathEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass pathItemEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass naturalItemEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass namedItemEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass stateSubjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass valueSubjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tagSubjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass eventSubjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tupleTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass naturalTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unlessStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementChoiceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementSequenceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass assignStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sendStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass receiveStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass constructorPatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass arrayAccessPatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass recordAccessPatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass allExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass existsExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass conditionalEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass disjunctionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass implicationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass conjunctionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass comparisonEqualEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass comparisonNotEqualEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass comparisonLesserEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass comparisonLesserEqualEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass comparisonGreaterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass comparisonGreaterEqualEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass additionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass substractionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass multiplicationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass divisionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass moduloEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unaryPlusExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unaryMinusExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unaryNegationExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unaryFirstExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unaryLengthExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unaryCoerceExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unaryFullExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unaryDeQueueExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unaryEmptyExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass projectionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass arrayAccessExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass recordAccessExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass constructionExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass explicitArrayExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass implicitArrayExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass trueLiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass falseLiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass allPropertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass existsPropertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass propertyDisjunctionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass propertyImplicationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass propertyConjunctionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass propertyNegationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass leadsToPatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass precedesPatternEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlAllEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlExistsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlDisjunctionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlImplicationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlConjunctionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlUntilEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlReleaseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlUnaryNegationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlUnaryNextEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlUnaryAlwaysEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlUnaryEventuallyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlVariableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass observableDisjunctionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass observableImplicationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass observableConjunctionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass observableNegationEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#eNS_URI
   * @see #init()
   * @generated
   */
  private FiacrePackageImpl()
  {
    super(eNS_URI, FiacreFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link FiacrePackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @generated
   */
  public static FiacrePackage init()
  {
    if (isInited) return (FiacrePackage)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI);

    // Obtain or create and register package
    FiacrePackageImpl theFiacrePackage = (FiacrePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof FiacrePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new FiacrePackageImpl());

    isInited = true;

    // Load packages
    theFiacrePackage.loadPackage();

    // Fix loaded packages
    theFiacrePackage.fixPackageContents();

    // Mark meta-data to indicate it can't be changed
    theFiacrePackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(FiacrePackage.eNS_URI, theFiacrePackage);
    return theFiacrePackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModel()
  {
    if (modelEClass == null)
    {
      modelEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(0);
    }
    return modelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModel_Imports()
  {
        return (EReference)getModel().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModel_Declarations()
  {
        return (EReference)getModel().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModel_Requirements()
  {
        return (EReference)getModel().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModel_Root()
  {
        return (EReference)getModel().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRootDeclaration()
  {
    if (rootDeclarationEClass == null)
    {
      rootDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(1);
    }
    return rootDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNamedElement()
  {
    if (namedElementEClass == null)
    {
      namedElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(2);
    }
    return namedElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeDeclarationUse()
  {
    if (typeDeclarationUseEClass == null)
    {
      typeDeclarationUseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(3);
    }
    return typeDeclarationUseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConstantDeclarationUse()
  {
    if (constantDeclarationUseEClass == null)
    {
      constantDeclarationUseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(4);
    }
    return constantDeclarationUseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpressionDeclarationUse()
  {
    if (expressionDeclarationUseEClass == null)
    {
      expressionDeclarationUseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(5);
    }
    return expressionDeclarationUseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReferenceDeclarationUse()
  {
    if (referenceDeclarationUseEClass == null)
    {
      referenceDeclarationUseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(6);
    }
    return referenceDeclarationUseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnionTagDeclarationUse()
  {
    if (unionTagDeclarationUseEClass == null)
    {
      unionTagDeclarationUseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(7);
    }
    return unionTagDeclarationUseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRecordFieldDeclarationUse()
  {
    if (recordFieldDeclarationUseEClass == null)
    {
      recordFieldDeclarationUseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(8);
    }
    return recordFieldDeclarationUseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPatternDeclarationUse()
  {
    if (patternDeclarationUseEClass == null)
    {
      patternDeclarationUseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(9);
    }
    return patternDeclarationUseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBoundDeclarationUse()
  {
    if (boundDeclarationUseEClass == null)
    {
      boundDeclarationUseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(10);
    }
    return boundDeclarationUseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPathDeclarationUse()
  {
    if (pathDeclarationUseEClass == null)
    {
      pathDeclarationUseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(11);
    }
    return pathDeclarationUseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImportDeclaration()
  {
    if (importDeclarationEClass == null)
    {
      importDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(12);
    }
    return importDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getImportDeclaration_ImportURI()
  {
        return (EAttribute)getImportDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDeclaration()
  {
    if (declarationEClass == null)
    {
      declarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(13);
    }
    return declarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getParameterizedDeclaration()
  {
    if (parameterizedDeclarationEClass == null)
    {
      parameterizedDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(14);
    }
    return parameterizedDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeDeclaration()
  {
    if (typeDeclarationEClass == null)
    {
      typeDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(15);
    }
    return typeDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTypeDeclaration_Name()
  {
        return (EAttribute)getTypeDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTypeDeclaration_Value()
  {
        return (EReference)getTypeDeclaration().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getChannelDeclaration()
  {
    if (channelDeclarationEClass == null)
    {
      channelDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(16);
    }
    return channelDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getChannelDeclaration_Name()
  {
        return (EAttribute)getChannelDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getChannelDeclaration_Value()
  {
        return (EReference)getChannelDeclaration().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getChannelType()
  {
    if (channelTypeEClass == null)
    {
      channelTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(17);
    }
    return channelTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getChannelType_Size()
  {
        return (EReference)getChannelType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getChannelType_In()
  {
        return (EAttribute)getChannelType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getChannelType_Out()
  {
        return (EAttribute)getChannelType().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getChannelType_Type()
  {
        return (EReference)getChannelType().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getType()
  {
    if (typeEClass == null)
    {
      typeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(18);
    }
    return typeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBasicType()
  {
    if (basicTypeEClass == null)
    {
      basicTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(19);
    }
    return basicTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRangeType()
  {
    if (rangeTypeEClass == null)
    {
      rangeTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(20);
    }
    return rangeTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRangeType_Minimum()
  {
        return (EReference)getRangeType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRangeType_Maximum()
  {
        return (EReference)getRangeType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRangeType_Size()
  {
        return (EReference)getRangeType().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnionType()
  {
    if (unionTypeEClass == null)
    {
      unionTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(21);
    }
    return unionTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnionType_Tags()
  {
        return (EReference)getUnionType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnionTags()
  {
    if (unionTagsEClass == null)
    {
      unionTagsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(22);
    }
    return unionTagsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnionTags_Tags()
  {
        return (EReference)getUnionTags().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnionTags_Type()
  {
        return (EReference)getUnionTags().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnionTagDeclaration()
  {
    if (unionTagDeclarationEClass == null)
    {
      unionTagDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(23);
    }
    return unionTagDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getUnionTagDeclaration_Name()
  {
        return (EAttribute)getUnionTagDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRecordType()
  {
    if (recordTypeEClass == null)
    {
      recordTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(24);
    }
    return recordTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordType_Fields()
  {
        return (EReference)getRecordType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRecordFields()
  {
    if (recordFieldsEClass == null)
    {
      recordFieldsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(25);
    }
    return recordFieldsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordFields_Fields()
  {
        return (EReference)getRecordFields().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordFields_Type()
  {
        return (EReference)getRecordFields().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRecordFieldDeclaration()
  {
    if (recordFieldDeclarationEClass == null)
    {
      recordFieldDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(26);
    }
    return recordFieldDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRecordFieldDeclaration_Name()
  {
        return (EAttribute)getRecordFieldDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getQueueType()
  {
    if (queueTypeEClass == null)
    {
      queueTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(27);
    }
    return queueTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getQueueType_Size()
  {
        return (EReference)getQueueType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getQueueType_Type()
  {
        return (EReference)getQueueType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArrayType()
  {
    if (arrayTypeEClass == null)
    {
      arrayTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(28);
    }
    return arrayTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayType_Size()
  {
        return (EReference)getArrayType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayType_Type()
  {
        return (EReference)getArrayType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReferencedType()
  {
    if (referencedTypeEClass == null)
    {
      referencedTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(29);
    }
    return referencedTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReferencedType_Type()
  {
        return (EReference)getReferencedType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConstantDeclaration()
  {
    if (constantDeclarationEClass == null)
    {
      constantDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(30);
    }
    return constantDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getConstantDeclaration_Name()
  {
        return (EAttribute)getConstantDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConstantDeclaration_Type()
  {
        return (EReference)getConstantDeclaration().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConstantDeclaration_Value()
  {
        return (EReference)getConstantDeclaration().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getProcessDeclaration()
  {
    if (processDeclarationEClass == null)
    {
      processDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(31);
    }
    return processDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getProcessDeclaration_Name()
  {
        return (EAttribute)getProcessDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcessDeclaration_Generics()
  {
        return (EReference)getProcessDeclaration().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcessDeclaration_Ports()
  {
        return (EReference)getProcessDeclaration().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcessDeclaration_Parameters()
  {
        return (EReference)getProcessDeclaration().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcessDeclaration_LocalPorts()
  {
        return (EReference)getProcessDeclaration().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcessDeclaration_Priorities()
  {
        return (EReference)getProcessDeclaration().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcessDeclaration_States()
  {
        return (EReference)getProcessDeclaration().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcessDeclaration_Variables()
  {
        return (EReference)getProcessDeclaration().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcessDeclaration_Prelude()
  {
        return (EReference)getProcessDeclaration().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcessDeclaration_Transitions()
  {
        return (EReference)getProcessDeclaration().getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericDeclaration()
  {
    if (genericDeclarationEClass == null)
    {
      genericDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(32);
    }
    return genericDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericTypeDeclaration()
  {
    if (genericTypeDeclarationEClass == null)
    {
      genericTypeDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(33);
    }
    return genericTypeDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGenericTypeDeclaration_Name()
  {
        return (EAttribute)getGenericTypeDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericConstantDeclaration()
  {
    if (genericConstantDeclarationEClass == null)
    {
      genericConstantDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(34);
    }
    return genericConstantDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGenericConstantDeclaration_Name()
  {
        return (EAttribute)getGenericConstantDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericUnionTagDeclaration()
  {
    if (genericUnionTagDeclarationEClass == null)
    {
      genericUnionTagDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(35);
    }
    return genericUnionTagDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGenericUnionTagDeclaration_Name()
  {
        return (EAttribute)getGenericUnionTagDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericRecordFieldDeclaration()
  {
    if (genericRecordFieldDeclarationEClass == null)
    {
      genericRecordFieldDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(36);
    }
    return genericRecordFieldDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGenericRecordFieldDeclaration_Name()
  {
        return (EAttribute)getGenericRecordFieldDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStateDeclaration()
  {
    if (stateDeclarationEClass == null)
    {
      stateDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(37);
    }
    return stateDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getStateDeclaration_Name()
  {
        return (EAttribute)getStateDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTransition()
  {
    if (transitionEClass == null)
    {
      transitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(38);
    }
    return transitionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTransition_Origin()
  {
        return (EReference)getTransition().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTransition_Action()
  {
        return (EReference)getTransition().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentDeclaration()
  {
    if (componentDeclarationEClass == null)
    {
      componentDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(39);
    }
    return componentDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getComponentDeclaration_Name()
  {
        return (EAttribute)getComponentDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentDeclaration_Generics()
  {
        return (EReference)getComponentDeclaration().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentDeclaration_Ports()
  {
        return (EReference)getComponentDeclaration().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentDeclaration_Parameters()
  {
        return (EReference)getComponentDeclaration().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentDeclaration_Variables()
  {
        return (EReference)getComponentDeclaration().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentDeclaration_LocalPorts()
  {
        return (EReference)getComponentDeclaration().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentDeclaration_Priorities()
  {
        return (EReference)getComponentDeclaration().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentDeclaration_Prelude()
  {
        return (EReference)getComponentDeclaration().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentDeclaration_Body()
  {
        return (EReference)getComponentDeclaration().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortsDeclaration()
  {
    if (portsDeclarationEClass == null)
    {
      portsDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(40);
    }
    return portsDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortsDeclaration_Ports()
  {
        return (EReference)getPortsDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortsDeclaration_Type()
  {
        return (EReference)getPortsDeclaration().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortDeclaration()
  {
    if (portDeclarationEClass == null)
    {
      portDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(41);
    }
    return portDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPortDeclaration_Name()
  {
        return (EAttribute)getPortDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLocalPortsDeclaration()
  {
    if (localPortsDeclarationEClass == null)
    {
      localPortsDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(42);
    }
    return localPortsDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLocalPortsDeclaration_Ports()
  {
        return (EReference)getLocalPortsDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLocalPortsDeclaration_Type()
  {
        return (EReference)getLocalPortsDeclaration().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLocalPortsDeclaration_Left()
  {
        return (EReference)getLocalPortsDeclaration().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLocalPortsDeclaration_Right()
  {
        return (EReference)getLocalPortsDeclaration().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getParametersDeclaration()
  {
    if (parametersDeclarationEClass == null)
    {
      parametersDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(43);
    }
    return parametersDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getParametersDeclaration_Parameters()
  {
        return (EReference)getParametersDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getParametersDeclaration_Read()
  {
        return (EAttribute)getParametersDeclaration().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getParametersDeclaration_Write()
  {
        return (EAttribute)getParametersDeclaration().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getParametersDeclaration_Type()
  {
        return (EReference)getParametersDeclaration().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getParameterDeclaration()
  {
    if (parameterDeclarationEClass == null)
    {
      parameterDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(44);
    }
    return parameterDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getParameterDeclaration_Reference()
  {
        return (EAttribute)getParameterDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getParameterDeclaration_Array()
  {
        return (EAttribute)getParameterDeclaration().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getParameterDeclaration_Size()
  {
        return (EReference)getParameterDeclaration().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getParameterDeclaration_Name()
  {
        return (EAttribute)getParameterDeclaration().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVariablesDeclaration()
  {
    if (variablesDeclarationEClass == null)
    {
      variablesDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(45);
    }
    return variablesDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariablesDeclaration_Variables()
  {
        return (EReference)getVariablesDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariablesDeclaration_Type()
  {
        return (EReference)getVariablesDeclaration().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariablesDeclaration_Value()
  {
        return (EReference)getVariablesDeclaration().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVariableDeclaration()
  {
    if (variableDeclarationEClass == null)
    {
      variableDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(46);
    }
    return variableDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getVariableDeclaration_Name()
  {
        return (EAttribute)getVariableDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPriorityDeclaration()
  {
    if (priorityDeclarationEClass == null)
    {
      priorityDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(47);
    }
    return priorityDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPriorityDeclaration_Groups()
  {
        return (EReference)getPriorityDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPriorityGroup()
  {
    if (priorityGroupEClass == null)
    {
      priorityGroupEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(48);
    }
    return priorityGroupEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPriorityGroup_Ports()
  {
        return (EReference)getPriorityGroup().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatement()
  {
    if (statementEClass == null)
    {
      statementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(49);
    }
    return statementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNullStatement()
  {
    if (nullStatementEClass == null)
    {
      nullStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(50);
    }
    return nullStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTaggedStatement()
  {
    if (taggedStatementEClass == null)
    {
      taggedStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(51);
    }
    return taggedStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTaggedStatement_Tag()
  {
        return (EReference)getTaggedStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTagDeclaration()
  {
    if (tagDeclarationEClass == null)
    {
      tagDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(52);
    }
    return tagDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTagDeclaration_Name()
  {
        return (EAttribute)getTagDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPatternStatement()
  {
    if (patternStatementEClass == null)
    {
      patternStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(53);
    }
    return patternStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPattern()
  {
    if (patternEClass == null)
    {
      patternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(54);
    }
    return patternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAnyPattern()
  {
    if (anyPatternEClass == null)
    {
      anyPatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(55);
    }
    return anyPatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConstantPattern()
  {
    if (constantPatternEClass == null)
    {
      constantPatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(56);
    }
    return constantPatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConstantPattern_Value()
  {
        return (EReference)getConstantPattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntegerPattern()
  {
    if (integerPatternEClass == null)
    {
      integerPatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(57);
    }
    return integerPatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIntegerPattern_Negative()
  {
        return (EAttribute)getIntegerPattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIntegerPattern_Value()
  {
        return (EAttribute)getIntegerPattern().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIdentifierPattern()
  {
    if (identifierPatternEClass == null)
    {
      identifierPatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(58);
    }
    return identifierPatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIdentifierPattern_Declaration()
  {
        return (EReference)getIdentifierPattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConditionalStatement()
  {
    if (conditionalStatementEClass == null)
    {
      conditionalStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(59);
    }
    return conditionalStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditionalStatement_Conditions()
  {
        return (EReference)getConditionalStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditionalStatement_Then()
  {
        return (EReference)getConditionalStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditionalStatement_Elseif()
  {
        return (EReference)getConditionalStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditionalStatement_Else()
  {
        return (EReference)getConditionalStatement().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExtendedConditionalStatement()
  {
    if (extendedConditionalStatementEClass == null)
    {
      extendedConditionalStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(60);
    }
    return extendedConditionalStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExtendedConditionalStatement_Condition()
  {
        return (EReference)getExtendedConditionalStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExtendedConditionalStatement_Then()
  {
        return (EReference)getExtendedConditionalStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectStatement()
  {
    if (selectStatementEClass == null)
    {
      selectStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(61);
    }
    return selectStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectStatement_Body()
  {
        return (EReference)getSelectStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectStatement_Index()
  {
        return (EReference)getSelectStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectStatement_Type()
  {
        return (EReference)getSelectStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getWhileStatement()
  {
    if (whileStatementEClass == null)
    {
      whileStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(62);
    }
    return whileStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWhileStatement_Condition()
  {
        return (EReference)getWhileStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWhileStatement_Body()
  {
        return (EReference)getWhileStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getForeachStatement()
  {
    if (foreachStatementEClass == null)
    {
      foreachStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(63);
    }
    return foreachStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getForeachStatement_Variable()
  {
        return (EReference)getForeachStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getForeachStatement_Body()
  {
        return (EReference)getForeachStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getToStatement()
  {
    if (toStatementEClass == null)
    {
      toStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(64);
    }
    return toStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getToStatement_State()
  {
        return (EReference)getToStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCaseStatement()
  {
    if (caseStatementEClass == null)
    {
      caseStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(65);
    }
    return caseStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCaseStatement_Value()
  {
        return (EReference)getCaseStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCaseStatement_Pattern()
  {
        return (EReference)getCaseStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCaseStatement_Body()
  {
        return (EReference)getCaseStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLoopStatement()
  {
    if (loopStatementEClass == null)
    {
      loopStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(66);
    }
    return loopStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOnStatement()
  {
    if (onStatementEClass == null)
    {
      onStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(67);
    }
    return onStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOnStatement_Condition()
  {
        return (EReference)getOnStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getWaitStatement()
  {
    if (waitStatementEClass == null)
    {
      waitStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(68);
    }
    return waitStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWaitStatement_Left()
  {
        return (EReference)getWaitStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWaitStatement_Right()
  {
        return (EReference)getWaitStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComposition()
  {
    if (compositionEClass == null)
    {
      compositionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(69);
    }
    return compositionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComposition_Global()
  {
        return (EReference)getComposition().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComposition_Blocks()
  {
        return (EReference)getComposition().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComposition_Index()
  {
        return (EReference)getComposition().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComposition_Type()
  {
        return (EReference)getComposition().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBlock()
  {
    if (blockEClass == null)
    {
      blockEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(70);
    }
    return blockEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCompositeBlock()
  {
    if (compositeBlockEClass == null)
    {
      compositeBlockEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(71);
    }
    return compositeBlockEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCompositeBlock_Local()
  {
        return (EReference)getCompositeBlock().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCompositeBlock_Composition()
  {
        return (EReference)getCompositeBlock().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInstanceDeclaration()
  {
    if (instanceDeclarationEClass == null)
    {
      instanceDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(72);
    }
    return instanceDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInstanceDeclaration_Local()
  {
        return (EReference)getInstanceDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getInstanceDeclaration_Name()
  {
        return (EAttribute)getInstanceDeclaration().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInstanceDeclaration_Instance()
  {
        return (EReference)getInstanceDeclaration().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortSet()
  {
    if (portSetEClass == null)
    {
      portSetEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(73);
    }
    return portSetEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPortSet_AllPorts()
  {
        return (EAttribute)getPortSet().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortSet_Ports()
  {
        return (EReference)getPortSet().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentInstance()
  {
    if (componentInstanceEClass == null)
    {
      componentInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(74);
    }
    return componentInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentInstance_Component()
  {
        return (EReference)getComponentInstance().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentInstance_Generics()
  {
        return (EReference)getComponentInstance().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentInstance_Ports()
  {
        return (EReference)getComponentInstance().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentInstance_Parameters()
  {
        return (EReference)getComponentInstance().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGenericInstance()
  {
    if (genericInstanceEClass == null)
    {
      genericInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(75);
    }
    return genericInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeInstance()
  {
    if (typeInstanceEClass == null)
    {
      typeInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(76);
    }
    return typeInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTypeInstance_Type()
  {
        return (EReference)getTypeInstance().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConstantInstance()
  {
    if (constantInstanceEClass == null)
    {
      constantInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(77);
    }
    return constantInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConstantInstance_Const()
  {
        return (EReference)getConstantInstance().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConstantInstance_Value()
  {
        return (EReference)getConstantInstance().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnionTagInstance()
  {
    if (unionTagInstanceEClass == null)
    {
      unionTagInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(78);
    }
    return unionTagInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getUnionTagInstance_Constr0()
  {
        return (EAttribute)getUnionTagInstance().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getUnionTagInstance_Constr1()
  {
        return (EAttribute)getUnionTagInstance().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnionTagInstance_Head()
  {
        return (EReference)getUnionTagInstance().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRecordFieldInstance()
  {
    if (recordFieldInstanceEClass == null)
    {
      recordFieldInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(79);
    }
    return recordFieldInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordFieldInstance_Field()
  {
        return (EReference)getRecordFieldInstance().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpression()
  {
    if (expressionEClass == null)
    {
      expressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(80);
    }
    return expressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReferenceExpression()
  {
    if (referenceExpressionEClass == null)
    {
      referenceExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(81);
    }
    return referenceExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReferenceExpression_Declaration()
  {
        return (EReference)getReferenceExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIdentifierExpression()
  {
    if (identifierExpressionEClass == null)
    {
      identifierExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(82);
    }
    return identifierExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIdentifierExpression_Declaration()
  {
        return (EReference)getIdentifierExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRecordExpression()
  {
    if (recordExpressionEClass == null)
    {
      recordExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(83);
    }
    return recordExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordExpression_Fields()
  {
        return (EReference)getRecordExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFieldExpression()
  {
    if (fieldExpressionEClass == null)
    {
      fieldExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(84);
    }
    return fieldExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFieldExpression_Field()
  {
        return (EReference)getFieldExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFieldExpression_Value()
  {
        return (EReference)getFieldExpression().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getQueueExpression()
  {
    if (queueExpressionEClass == null)
    {
      queueExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(85);
    }
    return queueExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getQueueExpression_Values()
  {
        return (EReference)getQueueExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEnqueueExpression()
  {
    if (enqueueExpressionEClass == null)
    {
      enqueueExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(86);
    }
    return enqueueExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEnqueueExpression_Element()
  {
        return (EReference)getEnqueueExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEnqueueExpression_Queue()
  {
        return (EReference)getEnqueueExpression().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAppendExpression()
  {
    if (appendExpressionEClass == null)
    {
      appendExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(87);
    }
    return appendExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAppendExpression_Left()
  {
        return (EReference)getAppendExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAppendExpression_Right()
  {
        return (EReference)getAppendExpression().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLiteralExpression()
  {
    if (literalExpressionEClass == null)
    {
      literalExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(88);
    }
    return literalExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanLiteral()
  {
    if (booleanLiteralEClass == null)
    {
      booleanLiteralEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(89);
    }
    return booleanLiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNaturalLiteral()
  {
    if (naturalLiteralEClass == null)
    {
      naturalLiteralEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(90);
    }
    return naturalLiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNaturalLiteral_Value()
  {
        return (EAttribute)getNaturalLiteral().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLowerBound()
  {
    if (lowerBoundEClass == null)
    {
      lowerBoundEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(91);
    }
    return lowerBoundEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLowerBound_Left()
  {
        return (EAttribute)getLowerBound().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLowerBound_Right()
  {
        return (EAttribute)getLowerBound().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUpperBound()
  {
    if (upperBoundEClass == null)
    {
      upperBoundEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(92);
    }
    return upperBoundEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNaturalLowerBound()
  {
    if (naturalLowerBoundEClass == null)
    {
      naturalLowerBoundEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(93);
    }
    return naturalLowerBoundEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNaturalLowerBound_Value()
  {
        return (EAttribute)getNaturalLowerBound().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNaturalUpperBound()
  {
    if (naturalUpperBoundEClass == null)
    {
      naturalUpperBoundEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(94);
    }
    return naturalUpperBoundEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNaturalUpperBound_Value()
  {
        return (EAttribute)getNaturalUpperBound().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNaturalUpperBound_Left()
  {
        return (EAttribute)getNaturalUpperBound().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNaturalUpperBound_Right()
  {
        return (EAttribute)getNaturalUpperBound().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDecimalLowerBound()
  {
    if (decimalLowerBoundEClass == null)
    {
      decimalLowerBoundEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(95);
    }
    return decimalLowerBoundEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDecimalLowerBound_Value()
  {
        return (EAttribute)getDecimalLowerBound().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDecimalUpperBound()
  {
    if (decimalUpperBoundEClass == null)
    {
      decimalUpperBoundEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(96);
    }
    return decimalUpperBoundEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDecimalUpperBound_Value()
  {
        return (EAttribute)getDecimalUpperBound().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDecimalUpperBound_Left()
  {
        return (EAttribute)getDecimalUpperBound().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDecimalUpperBound_Right()
  {
        return (EAttribute)getDecimalUpperBound().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVariableLowerBound()
  {
    if (variableLowerBoundEClass == null)
    {
      variableLowerBoundEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(97);
    }
    return variableLowerBoundEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariableLowerBound_Variable()
  {
        return (EReference)getVariableLowerBound().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVariableUpperBound()
  {
    if (variableUpperBoundEClass == null)
    {
      variableUpperBoundEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(98);
    }
    return variableUpperBoundEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariableUpperBound_Variable()
  {
        return (EReference)getVariableUpperBound().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getVariableUpperBound_Left()
  {
        return (EAttribute)getVariableUpperBound().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getVariableUpperBound_Right()
  {
        return (EAttribute)getVariableUpperBound().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInfiniteUpperBound()
  {
    if (infiniteUpperBoundEClass == null)
    {
      infiniteUpperBoundEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(99);
    }
    return infiniteUpperBoundEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRequirement()
  {
    if (requirementEClass == null)
    {
      requirementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(100);
    }
    return requirementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRequirement_Property()
  {
        return (EReference)getRequirement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRequirement_Positive()
  {
        return (EAttribute)getRequirement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRequirement_Negative()
  {
        return (EAttribute)getRequirement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPropertyDeclaration()
  {
    if (propertyDeclarationEClass == null)
    {
      propertyDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(101);
    }
    return propertyDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPropertyDeclaration_Name()
  {
        return (EAttribute)getPropertyDeclaration().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPropertyDeclaration_Property()
  {
        return (EReference)getPropertyDeclaration().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getProperty()
  {
    if (propertyEClass == null)
    {
      propertyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(102);
    }
    return propertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPatternProperty()
  {
    if (patternPropertyEClass == null)
    {
      patternPropertyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(103);
    }
    return patternPropertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLPattern()
  {
    if (ltlPatternEClass == null)
    {
      ltlPatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(104);
    }
    return ltlPatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLPattern_Property()
  {
        return (EReference)getLTLPattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDeadlockFreePattern()
  {
    if (deadlockFreePatternEClass == null)
    {
      deadlockFreePatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(105);
    }
    return deadlockFreePatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInfinitelyOftenPattern()
  {
    if (infinitelyOftenPatternEClass == null)
    {
      infinitelyOftenPatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(106);
    }
    return infinitelyOftenPatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInfinitelyOftenPattern_Subject()
  {
        return (EReference)getInfinitelyOftenPattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMortalPattern()
  {
    if (mortalPatternEClass == null)
    {
      mortalPatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(107);
    }
    return mortalPatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMortalPattern_Subject()
  {
        return (EReference)getMortalPattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPresencePattern()
  {
    if (presencePatternEClass == null)
    {
      presencePatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(108);
    }
    return presencePatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPresencePattern_Subject()
  {
        return (EReference)getPresencePattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPresencePattern_Lasting()
  {
        return (EAttribute)getPresencePattern().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPresencePattern_After()
  {
        return (EReference)getPresencePattern().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPresencePattern_Lower()
  {
        return (EReference)getPresencePattern().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPresencePattern_Upper()
  {
        return (EReference)getPresencePattern().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPresencePattern_Until()
  {
        return (EReference)getPresencePattern().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPresencePattern_Before()
  {
        return (EReference)getPresencePattern().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPresencePattern_Min()
  {
        return (EReference)getPresencePattern().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPresencePattern_Max()
  {
        return (EReference)getPresencePattern().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAbsencePattern()
  {
    if (absencePatternEClass == null)
    {
      absencePatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(109);
    }
    return absencePatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAbsencePattern_Subject()
  {
        return (EReference)getAbsencePattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAbsencePattern_After()
  {
        return (EReference)getAbsencePattern().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAbsencePattern_Lower()
  {
        return (EReference)getAbsencePattern().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAbsencePattern_Upper()
  {
        return (EReference)getAbsencePattern().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAbsencePattern_Until()
  {
        return (EReference)getAbsencePattern().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAbsencePattern_Before()
  {
        return (EReference)getAbsencePattern().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAbsencePattern_Min()
  {
        return (EReference)getAbsencePattern().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAbsencePattern_Max()
  {
        return (EReference)getAbsencePattern().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAlwaysPattern()
  {
    if (alwaysPatternEClass == null)
    {
      alwaysPatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(110);
    }
    return alwaysPatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAlwaysPattern_Subject()
  {
        return (EReference)getAlwaysPattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAlwaysPattern_Before()
  {
        return (EReference)getAlwaysPattern().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAlwaysPattern_After()
  {
        return (EReference)getAlwaysPattern().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAlwaysPattern_Until()
  {
        return (EReference)getAlwaysPattern().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAlwaysPattern_Min()
  {
        return (EReference)getAlwaysPattern().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAlwaysPattern_Max()
  {
        return (EReference)getAlwaysPattern().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSequencePattern()
  {
    if (sequencePatternEClass == null)
    {
      sequencePatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(111);
    }
    return sequencePatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLProperty()
  {
    if (ltlPropertyEClass == null)
    {
      ltlPropertyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(112);
    }
    return ltlPropertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStateEvent()
  {
    if (stateEventEClass == null)
    {
      stateEventEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(113);
    }
    return stateEventEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStateEvent_Subject()
  {
        return (EReference)getStateEvent().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEnterStateEvent()
  {
    if (enterStateEventEClass == null)
    {
      enterStateEventEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(114);
    }
    return enterStateEventEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLeaveStateEvent()
  {
    if (leaveStateEventEClass == null)
    {
      leaveStateEventEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(115);
    }
    return leaveStateEventEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getObservable()
  {
    if (observableEClass == null)
    {
      observableEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(116);
    }
    return observableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getObservableEvent()
  {
    if (observableEventEClass == null)
    {
      observableEventEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(117);
    }
    return observableEventEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPathEvent()
  {
    if (pathEventEClass == null)
    {
      pathEventEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(118);
    }
    return pathEventEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPathEvent_Path()
  {
        return (EReference)getPathEvent().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPathEvent_Item()
  {
        return (EReference)getPathEvent().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPath()
  {
    if (pathEClass == null)
    {
      pathEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(119);
    }
    return pathEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPath_Items()
  {
        return (EReference)getPath().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPathItem()
  {
    if (pathItemEClass == null)
    {
      pathItemEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(120);
    }
    return pathItemEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNaturalItem()
  {
    if (naturalItemEClass == null)
    {
      naturalItemEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(121);
    }
    return naturalItemEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNaturalItem_Value()
  {
        return (EAttribute)getNaturalItem().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNamedItem()
  {
    if (namedItemEClass == null)
    {
      namedItemEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(122);
    }
    return namedItemEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNamedItem_Declaration()
  {
        return (EReference)getNamedItem().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSubject()
  {
    if (subjectEClass == null)
    {
      subjectEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(123);
    }
    return subjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStateSubject()
  {
    if (stateSubjectEClass == null)
    {
      stateSubjectEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(124);
    }
    return stateSubjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStateSubject_State()
  {
        return (EReference)getStateSubject().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getValueSubject()
  {
    if (valueSubjectEClass == null)
    {
      valueSubjectEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(125);
    }
    return valueSubjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getValueSubject_Value()
  {
        return (EReference)getValueSubject().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTagSubject()
  {
    if (tagSubjectEClass == null)
    {
      tagSubjectEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(126);
    }
    return tagSubjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTagSubject_Tag()
  {
        return (EReference)getTagSubject().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEventSubject()
  {
    if (eventSubjectEClass == null)
    {
      eventSubjectEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(127);
    }
    return eventSubjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getEventSubject_Event()
  {
        return (EAttribute)getEventSubject().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTupleType()
  {
    if (tupleTypeEClass == null)
    {
      tupleTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(128);
    }
    return tupleTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTupleType_Types()
  {
        return (EReference)getTupleType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNaturalType()
  {
    if (naturalTypeEClass == null)
    {
      naturalTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(129);
    }
    return naturalTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntegerType()
  {
    if (integerTypeEClass == null)
    {
      integerTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(130);
    }
    return integerTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanType()
  {
    if (booleanTypeEClass == null)
    {
      booleanTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(131);
    }
    return booleanTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnlessStatement()
  {
    if (unlessStatementEClass == null)
    {
      unlessStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(132);
    }
    return unlessStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnlessStatement_Followers()
  {
        return (EReference)getUnlessStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatementChoice()
  {
    if (statementChoiceEClass == null)
    {
      statementChoiceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(133);
    }
    return statementChoiceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatementChoice_Choices()
  {
        return (EReference)getStatementChoice().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatementSequence()
  {
    if (statementSequenceEClass == null)
    {
      statementSequenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(134);
    }
    return statementSequenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatementSequence_Statements()
  {
        return (EReference)getStatementSequence().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAssignStatement()
  {
    if (assignStatementEClass == null)
    {
      assignStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(135);
    }
    return assignStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAssignStatement_Patterns()
  {
        return (EReference)getAssignStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAssignStatement_Values()
  {
        return (EReference)getAssignStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSendStatement()
  {
    if (sendStatementEClass == null)
    {
      sendStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(136);
    }
    return sendStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSendStatement_Port()
  {
        return (EReference)getSendStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSendStatement_Values()
  {
        return (EReference)getSendStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReceiveStatement()
  {
    if (receiveStatementEClass == null)
    {
      receiveStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(137);
    }
    return receiveStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReceiveStatement_Port()
  {
        return (EReference)getReceiveStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReceiveStatement_Patterns()
  {
        return (EReference)getReceiveStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReceiveStatement_Exp()
  {
        return (EReference)getReceiveStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConstructorPattern()
  {
    if (constructorPatternEClass == null)
    {
      constructorPatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(138);
    }
    return constructorPatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConstructorPattern_Parameter()
  {
        return (EReference)getConstructorPattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArrayAccessPattern()
  {
    if (arrayAccessPatternEClass == null)
    {
      arrayAccessPatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(139);
    }
    return arrayAccessPatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayAccessPattern_Source()
  {
        return (EReference)getArrayAccessPattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayAccessPattern_Index()
  {
        return (EReference)getArrayAccessPattern().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRecordAccessPattern()
  {
    if (recordAccessPatternEClass == null)
    {
      recordAccessPatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(140);
    }
    return recordAccessPatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordAccessPattern_Source()
  {
        return (EReference)getRecordAccessPattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordAccessPattern_Field()
  {
        return (EReference)getRecordAccessPattern().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAllExpression()
  {
    if (allExpressionEClass == null)
    {
      allExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(141);
    }
    return allExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllExpression_Index()
  {
        return (EReference)getAllExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllExpression_Type()
  {
        return (EReference)getAllExpression().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllExpression_Child()
  {
        return (EReference)getAllExpression().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExistsExpression()
  {
    if (existsExpressionEClass == null)
    {
      existsExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(142);
    }
    return existsExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExistsExpression_Index()
  {
        return (EReference)getExistsExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExistsExpression_Type()
  {
        return (EReference)getExistsExpression().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExistsExpression_Child()
  {
        return (EReference)getExistsExpression().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConditional()
  {
    if (conditionalEClass == null)
    {
      conditionalEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(143);
    }
    return conditionalEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditional_Condition()
  {
        return (EReference)getConditional().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditional_Then()
  {
        return (EReference)getConditional().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditional_Else()
  {
        return (EReference)getConditional().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDisjunction()
  {
    if (disjunctionEClass == null)
    {
      disjunctionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(144);
    }
    return disjunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDisjunction_Left()
  {
        return (EReference)getDisjunction().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDisjunction_Right()
  {
        return (EReference)getDisjunction().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImplication()
  {
    if (implicationEClass == null)
    {
      implicationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(145);
    }
    return implicationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImplication_Left()
  {
        return (EReference)getImplication().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImplication_Right()
  {
        return (EReference)getImplication().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConjunction()
  {
    if (conjunctionEClass == null)
    {
      conjunctionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(146);
    }
    return conjunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConjunction_Left()
  {
        return (EReference)getConjunction().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConjunction_Right()
  {
        return (EReference)getConjunction().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComparisonEqual()
  {
    if (comparisonEqualEClass == null)
    {
      comparisonEqualEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(147);
    }
    return comparisonEqualEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComparisonEqual_Left()
  {
        return (EReference)getComparisonEqual().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComparisonEqual_Right()
  {
        return (EReference)getComparisonEqual().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComparisonNotEqual()
  {
    if (comparisonNotEqualEClass == null)
    {
      comparisonNotEqualEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(148);
    }
    return comparisonNotEqualEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComparisonNotEqual_Left()
  {
        return (EReference)getComparisonNotEqual().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComparisonNotEqual_Right()
  {
        return (EReference)getComparisonNotEqual().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComparisonLesser()
  {
    if (comparisonLesserEClass == null)
    {
      comparisonLesserEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(149);
    }
    return comparisonLesserEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComparisonLesser_Left()
  {
        return (EReference)getComparisonLesser().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComparisonLesser_Right()
  {
        return (EReference)getComparisonLesser().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComparisonLesserEqual()
  {
    if (comparisonLesserEqualEClass == null)
    {
      comparisonLesserEqualEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(150);
    }
    return comparisonLesserEqualEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComparisonLesserEqual_Left()
  {
        return (EReference)getComparisonLesserEqual().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComparisonLesserEqual_Right()
  {
        return (EReference)getComparisonLesserEqual().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComparisonGreater()
  {
    if (comparisonGreaterEClass == null)
    {
      comparisonGreaterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(151);
    }
    return comparisonGreaterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComparisonGreater_Left()
  {
        return (EReference)getComparisonGreater().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComparisonGreater_Right()
  {
        return (EReference)getComparisonGreater().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComparisonGreaterEqual()
  {
    if (comparisonGreaterEqualEClass == null)
    {
      comparisonGreaterEqualEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(152);
    }
    return comparisonGreaterEqualEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComparisonGreaterEqual_Left()
  {
        return (EReference)getComparisonGreaterEqual().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComparisonGreaterEqual_Right()
  {
        return (EReference)getComparisonGreaterEqual().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAddition()
  {
    if (additionEClass == null)
    {
      additionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(153);
    }
    return additionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAddition_Left()
  {
        return (EReference)getAddition().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAddition_Right()
  {
        return (EReference)getAddition().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSubstraction()
  {
    if (substractionEClass == null)
    {
      substractionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(154);
    }
    return substractionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSubstraction_Left()
  {
        return (EReference)getSubstraction().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSubstraction_Right()
  {
        return (EReference)getSubstraction().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMultiplication()
  {
    if (multiplicationEClass == null)
    {
      multiplicationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(155);
    }
    return multiplicationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMultiplication_Left()
  {
        return (EReference)getMultiplication().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMultiplication_Right()
  {
        return (EReference)getMultiplication().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDivision()
  {
    if (divisionEClass == null)
    {
      divisionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(156);
    }
    return divisionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDivision_Left()
  {
        return (EReference)getDivision().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDivision_Right()
  {
        return (EReference)getDivision().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModulo()
  {
    if (moduloEClass == null)
    {
      moduloEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(157);
    }
    return moduloEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModulo_Left()
  {
        return (EReference)getModulo().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModulo_Right()
  {
        return (EReference)getModulo().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnaryPlusExpression()
  {
    if (unaryPlusExpressionEClass == null)
    {
      unaryPlusExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(158);
    }
    return unaryPlusExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnaryPlusExpression_Child()
  {
        return (EReference)getUnaryPlusExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnaryMinusExpression()
  {
    if (unaryMinusExpressionEClass == null)
    {
      unaryMinusExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(159);
    }
    return unaryMinusExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnaryMinusExpression_Child()
  {
        return (EReference)getUnaryMinusExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnaryNegationExpression()
  {
    if (unaryNegationExpressionEClass == null)
    {
      unaryNegationExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(160);
    }
    return unaryNegationExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnaryNegationExpression_Child()
  {
        return (EReference)getUnaryNegationExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnaryFirstExpression()
  {
    if (unaryFirstExpressionEClass == null)
    {
      unaryFirstExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(161);
    }
    return unaryFirstExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnaryFirstExpression_Child()
  {
        return (EReference)getUnaryFirstExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnaryLengthExpression()
  {
    if (unaryLengthExpressionEClass == null)
    {
      unaryLengthExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(162);
    }
    return unaryLengthExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnaryLengthExpression_Child()
  {
        return (EReference)getUnaryLengthExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnaryCoerceExpression()
  {
    if (unaryCoerceExpressionEClass == null)
    {
      unaryCoerceExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(163);
    }
    return unaryCoerceExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnaryCoerceExpression_Child()
  {
        return (EReference)getUnaryCoerceExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnaryFullExpression()
  {
    if (unaryFullExpressionEClass == null)
    {
      unaryFullExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(164);
    }
    return unaryFullExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnaryFullExpression_Child()
  {
        return (EReference)getUnaryFullExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnaryDeQueueExpression()
  {
    if (unaryDeQueueExpressionEClass == null)
    {
      unaryDeQueueExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(165);
    }
    return unaryDeQueueExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnaryDeQueueExpression_Child()
  {
        return (EReference)getUnaryDeQueueExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnaryEmptyExpression()
  {
    if (unaryEmptyExpressionEClass == null)
    {
      unaryEmptyExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(166);
    }
    return unaryEmptyExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnaryEmptyExpression_Child()
  {
        return (EReference)getUnaryEmptyExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getProjection()
  {
    if (projectionEClass == null)
    {
      projectionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(167);
    }
    return projectionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProjection_Channel()
  {
        return (EReference)getProjection().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProjection_Field()
  {
        return (EReference)getProjection().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArrayAccessExpression()
  {
    if (arrayAccessExpressionEClass == null)
    {
      arrayAccessExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(168);
    }
    return arrayAccessExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayAccessExpression_Child()
  {
        return (EReference)getArrayAccessExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayAccessExpression_Indexes()
  {
        return (EReference)getArrayAccessExpression().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRecordAccessExpression()
  {
    if (recordAccessExpressionEClass == null)
    {
      recordAccessExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(169);
    }
    return recordAccessExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordAccessExpression_Child()
  {
        return (EReference)getRecordAccessExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordAccessExpression_Field()
  {
        return (EReference)getRecordAccessExpression().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConstructionExpression()
  {
    if (constructionExpressionEClass == null)
    {
      constructionExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(170);
    }
    return constructionExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConstructionExpression_Parameter()
  {
        return (EReference)getConstructionExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExplicitArrayExpression()
  {
    if (explicitArrayExpressionEClass == null)
    {
      explicitArrayExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(171);
    }
    return explicitArrayExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExplicitArrayExpression_Values()
  {
        return (EReference)getExplicitArrayExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImplicitArrayExpression()
  {
    if (implicitArrayExpressionEClass == null)
    {
      implicitArrayExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(172);
    }
    return implicitArrayExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImplicitArrayExpression_Body()
  {
        return (EReference)getImplicitArrayExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImplicitArrayExpression_Index()
  {
        return (EReference)getImplicitArrayExpression().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImplicitArrayExpression_Type()
  {
        return (EReference)getImplicitArrayExpression().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTrueLiteral()
  {
    if (trueLiteralEClass == null)
    {
      trueLiteralEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(173);
    }
    return trueLiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFalseLiteral()
  {
    if (falseLiteralEClass == null)
    {
      falseLiteralEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(174);
    }
    return falseLiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAllProperty()
  {
    if (allPropertyEClass == null)
    {
      allPropertyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(175);
    }
    return allPropertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllProperty_Variable()
  {
        return (EReference)getAllProperty().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllProperty_Type()
  {
        return (EReference)getAllProperty().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllProperty_Child()
  {
        return (EReference)getAllProperty().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExistsProperty()
  {
    if (existsPropertyEClass == null)
    {
      existsPropertyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(176);
    }
    return existsPropertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExistsProperty_Variable()
  {
        return (EReference)getExistsProperty().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExistsProperty_Type()
  {
        return (EReference)getExistsProperty().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExistsProperty_Child()
  {
        return (EReference)getExistsProperty().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPropertyDisjunction()
  {
    if (propertyDisjunctionEClass == null)
    {
      propertyDisjunctionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(177);
    }
    return propertyDisjunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPropertyDisjunction_Left()
  {
        return (EReference)getPropertyDisjunction().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPropertyDisjunction_Right()
  {
        return (EReference)getPropertyDisjunction().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPropertyImplication()
  {
    if (propertyImplicationEClass == null)
    {
      propertyImplicationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(178);
    }
    return propertyImplicationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPropertyImplication_Left()
  {
        return (EReference)getPropertyImplication().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPropertyImplication_Right()
  {
        return (EReference)getPropertyImplication().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPropertyConjunction()
  {
    if (propertyConjunctionEClass == null)
    {
      propertyConjunctionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(179);
    }
    return propertyConjunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPropertyConjunction_Left()
  {
        return (EReference)getPropertyConjunction().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPropertyConjunction_Right()
  {
        return (EReference)getPropertyConjunction().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPropertyNegation()
  {
    if (propertyNegationEClass == null)
    {
      propertyNegationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(180);
    }
    return propertyNegationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPropertyNegation_Child()
  {
        return (EReference)getPropertyNegation().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLeadsToPattern()
  {
    if (leadsToPatternEClass == null)
    {
      leadsToPatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(181);
    }
    return leadsToPatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLeadsToPattern_Subject()
  {
        return (EReference)getLeadsToPattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLeadsToPattern_Follower()
  {
        return (EReference)getLeadsToPattern().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLeadsToPattern_Before()
  {
        return (EReference)getLeadsToPattern().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLeadsToPattern_Lower()
  {
        return (EReference)getLeadsToPattern().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLeadsToPattern_Upper()
  {
        return (EReference)getLeadsToPattern().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLeadsToPattern_After()
  {
        return (EReference)getLeadsToPattern().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLeadsToPattern_Until()
  {
        return (EReference)getLeadsToPattern().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLeadsToPattern_Min()
  {
        return (EReference)getLeadsToPattern().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLeadsToPattern_Max()
  {
        return (EReference)getLeadsToPattern().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPrecedesPattern()
  {
    if (precedesPatternEClass == null)
    {
      precedesPatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(182);
    }
    return precedesPatternEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPrecedesPattern_Subject()
  {
        return (EReference)getPrecedesPattern().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPrecedesPattern_Follower()
  {
        return (EReference)getPrecedesPattern().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPrecedesPattern_Before()
  {
        return (EReference)getPrecedesPattern().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPrecedesPattern_After()
  {
        return (EReference)getPrecedesPattern().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPrecedesPattern_Until()
  {
        return (EReference)getPrecedesPattern().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPrecedesPattern_Min()
  {
        return (EReference)getPrecedesPattern().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPrecedesPattern_Max()
  {
        return (EReference)getPrecedesPattern().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLAll()
  {
    if (ltlAllEClass == null)
    {
      ltlAllEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(183);
    }
    return ltlAllEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLAll_Index()
  {
        return (EReference)getLTLAll().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLAll_Type()
  {
        return (EReference)getLTLAll().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLAll_Child()
  {
        return (EReference)getLTLAll().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLExists()
  {
    if (ltlExistsEClass == null)
    {
      ltlExistsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(184);
    }
    return ltlExistsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLExists_Index()
  {
        return (EReference)getLTLExists().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLExists_Type()
  {
        return (EReference)getLTLExists().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLExists_Child()
  {
        return (EReference)getLTLExists().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLDisjunction()
  {
    if (ltlDisjunctionEClass == null)
    {
      ltlDisjunctionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(185);
    }
    return ltlDisjunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLDisjunction_Left()
  {
        return (EReference)getLTLDisjunction().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLDisjunction_Right()
  {
        return (EReference)getLTLDisjunction().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLImplication()
  {
    if (ltlImplicationEClass == null)
    {
      ltlImplicationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(186);
    }
    return ltlImplicationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLImplication_Left()
  {
        return (EReference)getLTLImplication().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLImplication_Right()
  {
        return (EReference)getLTLImplication().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLConjunction()
  {
    if (ltlConjunctionEClass == null)
    {
      ltlConjunctionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(187);
    }
    return ltlConjunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLConjunction_Left()
  {
        return (EReference)getLTLConjunction().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLConjunction_Right()
  {
        return (EReference)getLTLConjunction().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLUntil()
  {
    if (ltlUntilEClass == null)
    {
      ltlUntilEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(188);
    }
    return ltlUntilEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLUntil_Left()
  {
        return (EReference)getLTLUntil().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLUntil_Right()
  {
        return (EReference)getLTLUntil().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLRelease()
  {
    if (ltlReleaseEClass == null)
    {
      ltlReleaseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(189);
    }
    return ltlReleaseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLRelease_Left()
  {
        return (EReference)getLTLRelease().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLRelease_Right()
  {
        return (EReference)getLTLRelease().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLUnaryNegation()
  {
    if (ltlUnaryNegationEClass == null)
    {
      ltlUnaryNegationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(190);
    }
    return ltlUnaryNegationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLUnaryNegation_Operand()
  {
        return (EReference)getLTLUnaryNegation().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLUnaryNext()
  {
    if (ltlUnaryNextEClass == null)
    {
      ltlUnaryNextEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(191);
    }
    return ltlUnaryNextEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLUnaryNext_Operand()
  {
        return (EReference)getLTLUnaryNext().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLUnaryAlways()
  {
    if (ltlUnaryAlwaysEClass == null)
    {
      ltlUnaryAlwaysEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(192);
    }
    return ltlUnaryAlwaysEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLUnaryAlways_Operand()
  {
        return (EReference)getLTLUnaryAlways().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLUnaryEventually()
  {
    if (ltlUnaryEventuallyEClass == null)
    {
      ltlUnaryEventuallyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(193);
    }
    return ltlUnaryEventuallyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLUnaryEventually_Operand()
  {
        return (EReference)getLTLUnaryEventually().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLVariable()
  {
    if (ltlVariableEClass == null)
    {
      ltlVariableEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(194);
    }
    return ltlVariableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLVariable_Variable()
  {
        return (EReference)getLTLVariable().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getObservableDisjunction()
  {
    if (observableDisjunctionEClass == null)
    {
      observableDisjunctionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(195);
    }
    return observableDisjunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObservableDisjunction_Left()
  {
        return (EReference)getObservableDisjunction().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObservableDisjunction_Right()
  {
        return (EReference)getObservableDisjunction().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getObservableImplication()
  {
    if (observableImplicationEClass == null)
    {
      observableImplicationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(196);
    }
    return observableImplicationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObservableImplication_Left()
  {
        return (EReference)getObservableImplication().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObservableImplication_Right()
  {
        return (EReference)getObservableImplication().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getObservableConjunction()
  {
    if (observableConjunctionEClass == null)
    {
      observableConjunctionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(197);
    }
    return observableConjunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObservableConjunction_Left()
  {
        return (EReference)getObservableConjunction().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObservableConjunction_Right()
  {
        return (EReference)getObservableConjunction().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getObservableNegation()
  {
    if (observableNegationEClass == null)
    {
      observableNegationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(FiacrePackage.eNS_URI).getEClassifiers().get(198);
    }
    return observableNegationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObservableNegation_Child()
  {
        return (EReference)getObservableNegation().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FiacreFactory getFiacreFactory()
  {
    return (FiacreFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isLoaded = false;

  /**
   * Laods the package and any sub-packages from their serialized form.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void loadPackage()
  {
    if (isLoaded) return;
    isLoaded = true;

    URL url = getClass().getResource(packageFilename);
    if (url == null)
    {
      throw new RuntimeException("Missing serialized package: " + packageFilename);
    }
    URI uri = URI.createURI(url.toString());
    Resource resource = new EcoreResourceFactoryImpl().createResource(uri);
    try
    {
      resource.load(null);
    }
    catch (IOException exception)
    {
      throw new WrappedException(exception);
    }
    initializeFromLoadedEPackage(this, (EPackage)resource.getContents().get(0));
    createResource(eNS_URI);
  }


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isFixed = false;

  /**
   * Fixes up the loaded package, to make it appear as if it had been programmatically built.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void fixPackageContents()
  {
    if (isFixed) return;
    isFixed = true;
    fixEClassifiers();
  }

  /**
   * Sets the instance class on the given classifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void fixInstanceClass(EClassifier eClassifier)
  {
    if (eClassifier.getInstanceClassName() == null)
    {
      eClassifier.setInstanceClassName("fr.irit.fiacre.etoile.xtext.fiacre." + eClassifier.getName());
      setGeneratedClassName(eClassifier);
    }
  }

} //FiacrePackageImpl
