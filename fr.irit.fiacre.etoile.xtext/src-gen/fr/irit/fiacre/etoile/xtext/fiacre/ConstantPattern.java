/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constant Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantPattern#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getConstantPattern()
 * @model
 * @generated
 */
public interface ConstantPattern extends Pattern
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(LiteralExpression)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getConstantPattern_Value()
   * @model containment="true"
   * @generated
   */
  LiteralExpression getValue();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConstantPattern#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(LiteralExpression value);

} // ConstantPattern
