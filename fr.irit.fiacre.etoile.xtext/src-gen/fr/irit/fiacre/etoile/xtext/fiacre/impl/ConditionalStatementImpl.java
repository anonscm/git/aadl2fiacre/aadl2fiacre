/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement;
import fr.irit.fiacre.etoile.xtext.fiacre.Expression;
import fr.irit.fiacre.etoile.xtext.fiacre.ExtendedConditionalStatement;
import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;
import fr.irit.fiacre.etoile.xtext.fiacre.Statement;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conditional Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConditionalStatementImpl#getConditions <em>Conditions</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConditionalStatementImpl#getThen <em>Then</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConditionalStatementImpl#getElseif <em>Elseif</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.ConditionalStatementImpl#getElse <em>Else</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConditionalStatementImpl extends StatementImpl implements ConditionalStatement
{
  /**
   * The cached value of the '{@link #getConditions() <em>Conditions</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConditions()
   * @generated
   * @ordered
   */
  protected EList<Expression> conditions;

  /**
   * The cached value of the '{@link #getThen() <em>Then</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getThen()
   * @generated
   * @ordered
   */
  protected Statement then;

  /**
   * The cached value of the '{@link #getElseif() <em>Elseif</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElseif()
   * @generated
   * @ordered
   */
  protected EList<ExtendedConditionalStatement> elseif;

  /**
   * The cached value of the '{@link #getElse() <em>Else</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElse()
   * @generated
   * @ordered
   */
  protected Statement else_;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ConditionalStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FiacrePackage.eINSTANCE.getConditionalStatement();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Expression> getConditions()
  {
    if (conditions == null)
    {
      conditions = new EObjectContainmentEList<Expression>(Expression.class, this, FiacrePackage.CONDITIONAL_STATEMENT__CONDITIONS);
    }
    return conditions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement getThen()
  {
    return then;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetThen(Statement newThen, NotificationChain msgs)
  {
    Statement oldThen = then;
    then = newThen;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.CONDITIONAL_STATEMENT__THEN, oldThen, newThen);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setThen(Statement newThen)
  {
    if (newThen != then)
    {
      NotificationChain msgs = null;
      if (then != null)
        msgs = ((InternalEObject)then).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.CONDITIONAL_STATEMENT__THEN, null, msgs);
      if (newThen != null)
        msgs = ((InternalEObject)newThen).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.CONDITIONAL_STATEMENT__THEN, null, msgs);
      msgs = basicSetThen(newThen, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.CONDITIONAL_STATEMENT__THEN, newThen, newThen));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ExtendedConditionalStatement> getElseif()
  {
    if (elseif == null)
    {
      elseif = new EObjectContainmentEList<ExtendedConditionalStatement>(ExtendedConditionalStatement.class, this, FiacrePackage.CONDITIONAL_STATEMENT__ELSEIF);
    }
    return elseif;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement getElse()
  {
    return else_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetElse(Statement newElse, NotificationChain msgs)
  {
    Statement oldElse = else_;
    else_ = newElse;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.CONDITIONAL_STATEMENT__ELSE, oldElse, newElse);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setElse(Statement newElse)
  {
    if (newElse != else_)
    {
      NotificationChain msgs = null;
      if (else_ != null)
        msgs = ((InternalEObject)else_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.CONDITIONAL_STATEMENT__ELSE, null, msgs);
      if (newElse != null)
        msgs = ((InternalEObject)newElse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.CONDITIONAL_STATEMENT__ELSE, null, msgs);
      msgs = basicSetElse(newElse, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.CONDITIONAL_STATEMENT__ELSE, newElse, newElse));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case FiacrePackage.CONDITIONAL_STATEMENT__CONDITIONS:
        return ((InternalEList<?>)getConditions()).basicRemove(otherEnd, msgs);
      case FiacrePackage.CONDITIONAL_STATEMENT__THEN:
        return basicSetThen(null, msgs);
      case FiacrePackage.CONDITIONAL_STATEMENT__ELSEIF:
        return ((InternalEList<?>)getElseif()).basicRemove(otherEnd, msgs);
      case FiacrePackage.CONDITIONAL_STATEMENT__ELSE:
        return basicSetElse(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case FiacrePackage.CONDITIONAL_STATEMENT__CONDITIONS:
        return getConditions();
      case FiacrePackage.CONDITIONAL_STATEMENT__THEN:
        return getThen();
      case FiacrePackage.CONDITIONAL_STATEMENT__ELSEIF:
        return getElseif();
      case FiacrePackage.CONDITIONAL_STATEMENT__ELSE:
        return getElse();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case FiacrePackage.CONDITIONAL_STATEMENT__CONDITIONS:
        getConditions().clear();
        getConditions().addAll((Collection<? extends Expression>)newValue);
        return;
      case FiacrePackage.CONDITIONAL_STATEMENT__THEN:
        setThen((Statement)newValue);
        return;
      case FiacrePackage.CONDITIONAL_STATEMENT__ELSEIF:
        getElseif().clear();
        getElseif().addAll((Collection<? extends ExtendedConditionalStatement>)newValue);
        return;
      case FiacrePackage.CONDITIONAL_STATEMENT__ELSE:
        setElse((Statement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.CONDITIONAL_STATEMENT__CONDITIONS:
        getConditions().clear();
        return;
      case FiacrePackage.CONDITIONAL_STATEMENT__THEN:
        setThen((Statement)null);
        return;
      case FiacrePackage.CONDITIONAL_STATEMENT__ELSEIF:
        getElseif().clear();
        return;
      case FiacrePackage.CONDITIONAL_STATEMENT__ELSE:
        setElse((Statement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.CONDITIONAL_STATEMENT__CONDITIONS:
        return conditions != null && !conditions.isEmpty();
      case FiacrePackage.CONDITIONAL_STATEMENT__THEN:
        return then != null;
      case FiacrePackage.CONDITIONAL_STATEMENT__ELSEIF:
        return elseif != null && !elseif.isEmpty();
      case FiacrePackage.CONDITIONAL_STATEMENT__ELSE:
        return else_ != null;
    }
    return super.eIsSet(featureID);
  }

} //ConditionalStatementImpl
