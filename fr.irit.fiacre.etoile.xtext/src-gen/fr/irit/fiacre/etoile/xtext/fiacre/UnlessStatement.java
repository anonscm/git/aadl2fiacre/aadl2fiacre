/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unless Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.UnlessStatement#getFollowers <em>Followers</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getUnlessStatement()
 * @model
 * @generated
 */
public interface UnlessStatement extends Statement
{
  /**
   * Returns the value of the '<em><b>Followers</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.Statement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Followers</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Followers</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getUnlessStatement_Followers()
   * @model containment="true"
   * @generated
   */
  EList<Statement> getFollowers();

} // UnlessStatement
