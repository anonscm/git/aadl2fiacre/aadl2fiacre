/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>True Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getTrueLiteral()
 * @model
 * @generated
 */
public interface TrueLiteral extends BooleanLiteral
{
} // TrueLiteral
