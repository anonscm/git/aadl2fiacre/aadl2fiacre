/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Subject</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.EventSubject#getEvent <em>Event</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getEventSubject()
 * @model
 * @generated
 */
public interface EventSubject extends Subject
{
  /**
   * Returns the value of the '<em><b>Event</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Event</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Event</em>' attribute.
   * @see #setEvent(String)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getEventSubject_Event()
   * @model
   * @generated
   */
  String getEvent();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.EventSubject#getEvent <em>Event</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Event</em>' attribute.
   * @see #getEvent()
   * @generated
   */
  void setEvent(String value);

} // EventSubject
