/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;
import fr.irit.fiacre.etoile.xtext.fiacre.LowerBound;
import fr.irit.fiacre.etoile.xtext.fiacre.Observable;
import fr.irit.fiacre.etoile.xtext.fiacre.PresencePattern;
import fr.irit.fiacre.etoile.xtext.fiacre.UpperBound;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Presence Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PresencePatternImpl#getSubject <em>Subject</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PresencePatternImpl#getLasting <em>Lasting</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PresencePatternImpl#getAfter <em>After</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PresencePatternImpl#getLower <em>Lower</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PresencePatternImpl#getUpper <em>Upper</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PresencePatternImpl#getUntil <em>Until</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PresencePatternImpl#getBefore <em>Before</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PresencePatternImpl#getMin <em>Min</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.PresencePatternImpl#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PresencePatternImpl extends PatternPropertyImpl implements PresencePattern
{
  /**
   * The cached value of the '{@link #getSubject() <em>Subject</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubject()
   * @generated
   * @ordered
   */
  protected Observable subject;

  /**
   * The default value of the '{@link #getLasting() <em>Lasting</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLasting()
   * @generated
   * @ordered
   */
  protected static final int LASTING_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getLasting() <em>Lasting</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLasting()
   * @generated
   * @ordered
   */
  protected int lasting = LASTING_EDEFAULT;

  /**
   * The cached value of the '{@link #getAfter() <em>After</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAfter()
   * @generated
   * @ordered
   */
  protected Observable after;

  /**
   * The cached value of the '{@link #getLower() <em>Lower</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLower()
   * @generated
   * @ordered
   */
  protected LowerBound lower;

  /**
   * The cached value of the '{@link #getUpper() <em>Upper</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUpper()
   * @generated
   * @ordered
   */
  protected UpperBound upper;

  /**
   * The cached value of the '{@link #getUntil() <em>Until</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUntil()
   * @generated
   * @ordered
   */
  protected Observable until;

  /**
   * The cached value of the '{@link #getBefore() <em>Before</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBefore()
   * @generated
   * @ordered
   */
  protected Observable before;

  /**
   * The cached value of the '{@link #getMin() <em>Min</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMin()
   * @generated
   * @ordered
   */
  protected Observable min;

  /**
   * The cached value of the '{@link #getMax() <em>Max</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMax()
   * @generated
   * @ordered
   */
  protected Observable max;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PresencePatternImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FiacrePackage.eINSTANCE.getPresencePattern();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Observable getSubject()
  {
    return subject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSubject(Observable newSubject, NotificationChain msgs)
  {
    Observable oldSubject = subject;
    subject = newSubject;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__SUBJECT, oldSubject, newSubject);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSubject(Observable newSubject)
  {
    if (newSubject != subject)
    {
      NotificationChain msgs = null;
      if (subject != null)
        msgs = ((InternalEObject)subject).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__SUBJECT, null, msgs);
      if (newSubject != null)
        msgs = ((InternalEObject)newSubject).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__SUBJECT, null, msgs);
      msgs = basicSetSubject(newSubject, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__SUBJECT, newSubject, newSubject));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getLasting()
  {
    return lasting;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLasting(int newLasting)
  {
    int oldLasting = lasting;
    lasting = newLasting;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__LASTING, oldLasting, lasting));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Observable getAfter()
  {
    return after;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAfter(Observable newAfter, NotificationChain msgs)
  {
    Observable oldAfter = after;
    after = newAfter;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__AFTER, oldAfter, newAfter);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAfter(Observable newAfter)
  {
    if (newAfter != after)
    {
      NotificationChain msgs = null;
      if (after != null)
        msgs = ((InternalEObject)after).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__AFTER, null, msgs);
      if (newAfter != null)
        msgs = ((InternalEObject)newAfter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__AFTER, null, msgs);
      msgs = basicSetAfter(newAfter, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__AFTER, newAfter, newAfter));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LowerBound getLower()
  {
    return lower;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLower(LowerBound newLower, NotificationChain msgs)
  {
    LowerBound oldLower = lower;
    lower = newLower;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__LOWER, oldLower, newLower);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLower(LowerBound newLower)
  {
    if (newLower != lower)
    {
      NotificationChain msgs = null;
      if (lower != null)
        msgs = ((InternalEObject)lower).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__LOWER, null, msgs);
      if (newLower != null)
        msgs = ((InternalEObject)newLower).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__LOWER, null, msgs);
      msgs = basicSetLower(newLower, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__LOWER, newLower, newLower));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UpperBound getUpper()
  {
    return upper;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetUpper(UpperBound newUpper, NotificationChain msgs)
  {
    UpperBound oldUpper = upper;
    upper = newUpper;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__UPPER, oldUpper, newUpper);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUpper(UpperBound newUpper)
  {
    if (newUpper != upper)
    {
      NotificationChain msgs = null;
      if (upper != null)
        msgs = ((InternalEObject)upper).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__UPPER, null, msgs);
      if (newUpper != null)
        msgs = ((InternalEObject)newUpper).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__UPPER, null, msgs);
      msgs = basicSetUpper(newUpper, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__UPPER, newUpper, newUpper));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Observable getUntil()
  {
    return until;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetUntil(Observable newUntil, NotificationChain msgs)
  {
    Observable oldUntil = until;
    until = newUntil;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__UNTIL, oldUntil, newUntil);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUntil(Observable newUntil)
  {
    if (newUntil != until)
    {
      NotificationChain msgs = null;
      if (until != null)
        msgs = ((InternalEObject)until).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__UNTIL, null, msgs);
      if (newUntil != null)
        msgs = ((InternalEObject)newUntil).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__UNTIL, null, msgs);
      msgs = basicSetUntil(newUntil, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__UNTIL, newUntil, newUntil));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Observable getBefore()
  {
    return before;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBefore(Observable newBefore, NotificationChain msgs)
  {
    Observable oldBefore = before;
    before = newBefore;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__BEFORE, oldBefore, newBefore);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBefore(Observable newBefore)
  {
    if (newBefore != before)
    {
      NotificationChain msgs = null;
      if (before != null)
        msgs = ((InternalEObject)before).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__BEFORE, null, msgs);
      if (newBefore != null)
        msgs = ((InternalEObject)newBefore).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__BEFORE, null, msgs);
      msgs = basicSetBefore(newBefore, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__BEFORE, newBefore, newBefore));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Observable getMin()
  {
    return min;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMin(Observable newMin, NotificationChain msgs)
  {
    Observable oldMin = min;
    min = newMin;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__MIN, oldMin, newMin);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMin(Observable newMin)
  {
    if (newMin != min)
    {
      NotificationChain msgs = null;
      if (min != null)
        msgs = ((InternalEObject)min).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__MIN, null, msgs);
      if (newMin != null)
        msgs = ((InternalEObject)newMin).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__MIN, null, msgs);
      msgs = basicSetMin(newMin, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__MIN, newMin, newMin));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Observable getMax()
  {
    return max;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMax(Observable newMax, NotificationChain msgs)
  {
    Observable oldMax = max;
    max = newMax;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__MAX, oldMax, newMax);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMax(Observable newMax)
  {
    if (newMax != max)
    {
      NotificationChain msgs = null;
      if (max != null)
        msgs = ((InternalEObject)max).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__MAX, null, msgs);
      if (newMax != null)
        msgs = ((InternalEObject)newMax).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FiacrePackage.PRESENCE_PATTERN__MAX, null, msgs);
      msgs = basicSetMax(newMax, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.PRESENCE_PATTERN__MAX, newMax, newMax));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case FiacrePackage.PRESENCE_PATTERN__SUBJECT:
        return basicSetSubject(null, msgs);
      case FiacrePackage.PRESENCE_PATTERN__AFTER:
        return basicSetAfter(null, msgs);
      case FiacrePackage.PRESENCE_PATTERN__LOWER:
        return basicSetLower(null, msgs);
      case FiacrePackage.PRESENCE_PATTERN__UPPER:
        return basicSetUpper(null, msgs);
      case FiacrePackage.PRESENCE_PATTERN__UNTIL:
        return basicSetUntil(null, msgs);
      case FiacrePackage.PRESENCE_PATTERN__BEFORE:
        return basicSetBefore(null, msgs);
      case FiacrePackage.PRESENCE_PATTERN__MIN:
        return basicSetMin(null, msgs);
      case FiacrePackage.PRESENCE_PATTERN__MAX:
        return basicSetMax(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case FiacrePackage.PRESENCE_PATTERN__SUBJECT:
        return getSubject();
      case FiacrePackage.PRESENCE_PATTERN__LASTING:
        return getLasting();
      case FiacrePackage.PRESENCE_PATTERN__AFTER:
        return getAfter();
      case FiacrePackage.PRESENCE_PATTERN__LOWER:
        return getLower();
      case FiacrePackage.PRESENCE_PATTERN__UPPER:
        return getUpper();
      case FiacrePackage.PRESENCE_PATTERN__UNTIL:
        return getUntil();
      case FiacrePackage.PRESENCE_PATTERN__BEFORE:
        return getBefore();
      case FiacrePackage.PRESENCE_PATTERN__MIN:
        return getMin();
      case FiacrePackage.PRESENCE_PATTERN__MAX:
        return getMax();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case FiacrePackage.PRESENCE_PATTERN__SUBJECT:
        setSubject((Observable)newValue);
        return;
      case FiacrePackage.PRESENCE_PATTERN__LASTING:
        setLasting((Integer)newValue);
        return;
      case FiacrePackage.PRESENCE_PATTERN__AFTER:
        setAfter((Observable)newValue);
        return;
      case FiacrePackage.PRESENCE_PATTERN__LOWER:
        setLower((LowerBound)newValue);
        return;
      case FiacrePackage.PRESENCE_PATTERN__UPPER:
        setUpper((UpperBound)newValue);
        return;
      case FiacrePackage.PRESENCE_PATTERN__UNTIL:
        setUntil((Observable)newValue);
        return;
      case FiacrePackage.PRESENCE_PATTERN__BEFORE:
        setBefore((Observable)newValue);
        return;
      case FiacrePackage.PRESENCE_PATTERN__MIN:
        setMin((Observable)newValue);
        return;
      case FiacrePackage.PRESENCE_PATTERN__MAX:
        setMax((Observable)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.PRESENCE_PATTERN__SUBJECT:
        setSubject((Observable)null);
        return;
      case FiacrePackage.PRESENCE_PATTERN__LASTING:
        setLasting(LASTING_EDEFAULT);
        return;
      case FiacrePackage.PRESENCE_PATTERN__AFTER:
        setAfter((Observable)null);
        return;
      case FiacrePackage.PRESENCE_PATTERN__LOWER:
        setLower((LowerBound)null);
        return;
      case FiacrePackage.PRESENCE_PATTERN__UPPER:
        setUpper((UpperBound)null);
        return;
      case FiacrePackage.PRESENCE_PATTERN__UNTIL:
        setUntil((Observable)null);
        return;
      case FiacrePackage.PRESENCE_PATTERN__BEFORE:
        setBefore((Observable)null);
        return;
      case FiacrePackage.PRESENCE_PATTERN__MIN:
        setMin((Observable)null);
        return;
      case FiacrePackage.PRESENCE_PATTERN__MAX:
        setMax((Observable)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.PRESENCE_PATTERN__SUBJECT:
        return subject != null;
      case FiacrePackage.PRESENCE_PATTERN__LASTING:
        return lasting != LASTING_EDEFAULT;
      case FiacrePackage.PRESENCE_PATTERN__AFTER:
        return after != null;
      case FiacrePackage.PRESENCE_PATTERN__LOWER:
        return lower != null;
      case FiacrePackage.PRESENCE_PATTERN__UPPER:
        return upper != null;
      case FiacrePackage.PRESENCE_PATTERN__UNTIL:
        return until != null;
      case FiacrePackage.PRESENCE_PATTERN__BEFORE:
        return before != null;
      case FiacrePackage.PRESENCE_PATTERN__MIN:
        return min != null;
      case FiacrePackage.PRESENCE_PATTERN__MAX:
        return max != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (lasting: ");
    result.append(lasting);
    result.append(')');
    return result.toString();
  }

} //PresencePatternImpl
