/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getName <em>Name</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getGenerics <em>Generics</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getPorts <em>Ports</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getParameters <em>Parameters</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getVariables <em>Variables</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getLocalPorts <em>Local Ports</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getPriorities <em>Priorities</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getPrelude <em>Prelude</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentDeclaration()
 * @model
 * @generated
 */
public interface ComponentDeclaration extends RootDeclaration, PathDeclarationUse, ParameterizedDeclaration
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentDeclaration_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Generics</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.GenericDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Generics</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Generics</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentDeclaration_Generics()
   * @model containment="true"
   * @generated
   */
  EList<GenericDeclaration> getGenerics();

  /**
   * Returns the value of the '<em><b>Ports</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.PortsDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ports</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ports</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentDeclaration_Ports()
   * @model containment="true"
   * @generated
   */
  EList<PortsDeclaration> getPorts();

  /**
   * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parameters</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentDeclaration_Parameters()
   * @model containment="true"
   * @generated
   */
  EList<ParametersDeclaration> getParameters();

  /**
   * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variables</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variables</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentDeclaration_Variables()
   * @model containment="true"
   * @generated
   */
  EList<VariablesDeclaration> getVariables();

  /**
   * Returns the value of the '<em><b>Local Ports</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Local Ports</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Local Ports</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentDeclaration_LocalPorts()
   * @model containment="true"
   * @generated
   */
  EList<LocalPortsDeclaration> getLocalPorts();

  /**
   * Returns the value of the '<em><b>Priorities</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.PriorityDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Priorities</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Priorities</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentDeclaration_Priorities()
   * @model containment="true"
   * @generated
   */
  EList<PriorityDeclaration> getPriorities();

  /**
   * Returns the value of the '<em><b>Prelude</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prelude</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prelude</em>' containment reference.
   * @see #setPrelude(Statement)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentDeclaration_Prelude()
   * @model containment="true"
   * @generated
   */
  Statement getPrelude();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getPrelude <em>Prelude</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prelude</em>' containment reference.
   * @see #getPrelude()
   * @generated
   */
  void setPrelude(Statement value);

  /**
   * Returns the value of the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Body</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Body</em>' containment reference.
   * @see #setBody(Composition)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getComponentDeclaration_Body()
   * @model containment="true"
   * @generated
   */
  Composition getBody();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration#getBody <em>Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Body</em>' containment reference.
   * @see #getBody()
   * @generated
   */
  void setBody(Composition value);

} // ComponentDeclaration
