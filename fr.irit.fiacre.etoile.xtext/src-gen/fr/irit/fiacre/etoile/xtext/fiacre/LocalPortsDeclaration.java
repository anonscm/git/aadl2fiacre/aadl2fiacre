/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Local Ports Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getPorts <em>Ports</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getType <em>Type</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getLeft <em>Left</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getRight <em>Right</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLocalPortsDeclaration()
 * @model
 * @generated
 */
public interface LocalPortsDeclaration extends EObject
{
  /**
   * Returns the value of the '<em><b>Ports</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ports</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ports</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLocalPortsDeclaration_Ports()
   * @model containment="true"
   * @generated
   */
  EList<PortDeclaration> getPorts();

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(ChannelType)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLocalPortsDeclaration_Type()
   * @model containment="true"
   * @generated
   */
  ChannelType getType();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(ChannelType value);

  /**
   * Returns the value of the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left</em>' containment reference.
   * @see #setLeft(LowerBound)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLocalPortsDeclaration_Left()
   * @model containment="true"
   * @generated
   */
  LowerBound getLeft();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getLeft <em>Left</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Left</em>' containment reference.
   * @see #getLeft()
   * @generated
   */
  void setLeft(LowerBound value);

  /**
   * Returns the value of the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Right</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Right</em>' containment reference.
   * @see #setRight(UpperBound)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLocalPortsDeclaration_Right()
   * @model containment="true"
   * @generated
   */
  UpperBound getRight();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration#getRight <em>Right</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Right</em>' containment reference.
   * @see #getRight()
   * @generated
   */
  void setRight(UpperBound value);

} // LocalPortsDeclaration
