/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Access Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessExpression#getChild <em>Child</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessExpression#getIndexes <em>Indexes</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getArrayAccessExpression()
 * @model
 * @generated
 */
public interface ArrayAccessExpression extends IdentifierExpression
{
  /**
   * Returns the value of the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Child</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Child</em>' containment reference.
   * @see #setChild(IdentifierExpression)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getArrayAccessExpression_Child()
   * @model containment="true"
   * @generated
   */
  IdentifierExpression getChild();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ArrayAccessExpression#getChild <em>Child</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Child</em>' containment reference.
   * @see #getChild()
   * @generated
   */
  void setChild(IdentifierExpression value);

  /**
   * Returns the value of the '<em><b>Indexes</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.Expression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Indexes</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Indexes</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getArrayAccessExpression_Indexes()
   * @model containment="true"
   * @generated
   */
  EList<Expression> getIndexes();

} // ArrayAccessExpression
