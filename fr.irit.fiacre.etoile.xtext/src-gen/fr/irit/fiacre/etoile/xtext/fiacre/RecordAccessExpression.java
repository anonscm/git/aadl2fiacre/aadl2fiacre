/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record Access Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessExpression#getChild <em>Child</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessExpression#getField <em>Field</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRecordAccessExpression()
 * @model
 * @generated
 */
public interface RecordAccessExpression extends IdentifierExpression
{
  /**
   * Returns the value of the '<em><b>Child</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Child</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Child</em>' containment reference.
   * @see #setChild(IdentifierExpression)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRecordAccessExpression_Child()
   * @model containment="true"
   * @generated
   */
  IdentifierExpression getChild();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessExpression#getChild <em>Child</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Child</em>' containment reference.
   * @see #getChild()
   * @generated
   */
  void setChild(IdentifierExpression value);

  /**
   * Returns the value of the '<em><b>Field</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field</em>' reference.
   * @see #setField(RecordFieldDeclaration)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRecordAccessExpression_Field()
   * @model
   * @generated
   */
  RecordFieldDeclaration getField();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessExpression#getField <em>Field</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field</em>' reference.
   * @see #getField()
   * @generated
   */
  void setField(RecordFieldDeclaration value);

} // RecordAccessExpression
