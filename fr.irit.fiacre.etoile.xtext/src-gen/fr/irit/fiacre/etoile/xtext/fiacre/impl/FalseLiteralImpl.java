/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.FalseLiteral;
import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>False Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FalseLiteralImpl extends BooleanLiteralImpl implements FalseLiteral
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FalseLiteralImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FiacrePackage.eINSTANCE.getFalseLiteral();
  }

} //FalseLiteralImpl
