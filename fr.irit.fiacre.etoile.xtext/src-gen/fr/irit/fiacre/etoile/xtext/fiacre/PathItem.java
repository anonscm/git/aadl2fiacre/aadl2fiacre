/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Path Item</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getPathItem()
 * @model
 * @generated
 */
public interface PathItem extends EObject
{
} // PathItem
