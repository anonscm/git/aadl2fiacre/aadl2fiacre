/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lower Bound</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.LowerBound#isLeft <em>Left</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.LowerBound#isRight <em>Right</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLowerBound()
 * @model
 * @generated
 */
public interface LowerBound extends EObject
{
  /**
   * Returns the value of the '<em><b>Left</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left</em>' attribute.
   * @see #setLeft(boolean)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLowerBound_Left()
   * @model
   * @generated
   */
  boolean isLeft();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.LowerBound#isLeft <em>Left</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Left</em>' attribute.
   * @see #isLeft()
   * @generated
   */
  void setLeft(boolean value);

  /**
   * Returns the value of the '<em><b>Right</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Right</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Right</em>' attribute.
   * @see #setRight(boolean)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getLowerBound_Right()
   * @model
   * @generated
   */
  boolean isRight();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.LowerBound#isRight <em>Right</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Right</em>' attribute.
   * @see #isRight()
   * @generated
   */
  void setRight(boolean value);

} // LowerBound
