/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Declaration Use</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getReferenceDeclarationUse()
 * @model
 * @generated
 */
public interface ReferenceDeclarationUse extends EObject
{
} // ReferenceDeclarationUse
