/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Upper Bound</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound#getVariable <em>Variable</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound#isLeft <em>Left</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound#isRight <em>Right</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getVariableUpperBound()
 * @model
 * @generated
 */
public interface VariableUpperBound extends UpperBound
{
  /**
   * Returns the value of the '<em><b>Variable</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variable</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variable</em>' reference.
   * @see #setVariable(BoundDeclarationUse)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getVariableUpperBound_Variable()
   * @model
   * @generated
   */
  BoundDeclarationUse getVariable();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound#getVariable <em>Variable</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variable</em>' reference.
   * @see #getVariable()
   * @generated
   */
  void setVariable(BoundDeclarationUse value);

  /**
   * Returns the value of the '<em><b>Left</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left</em>' attribute.
   * @see #setLeft(boolean)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getVariableUpperBound_Left()
   * @model
   * @generated
   */
  boolean isLeft();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound#isLeft <em>Left</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Left</em>' attribute.
   * @see #isLeft()
   * @generated
   */
  void setLeft(boolean value);

  /**
   * Returns the value of the '<em><b>Right</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Right</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Right</em>' attribute.
   * @see #setRight(boolean)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getVariableUpperBound_Right()
   * @model
   * @generated
   */
  boolean isRight();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.VariableUpperBound#isRight <em>Right</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Right</em>' attribute.
   * @see #isRight()
   * @generated
   */
  void setRight(boolean value);

} // VariableUpperBound
