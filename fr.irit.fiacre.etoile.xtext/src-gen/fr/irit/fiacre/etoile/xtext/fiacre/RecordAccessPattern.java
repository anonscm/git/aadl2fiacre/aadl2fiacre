/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record Access Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessPattern#getSource <em>Source</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessPattern#getField <em>Field</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRecordAccessPattern()
 * @model
 * @generated
 */
public interface RecordAccessPattern extends IdentifierPattern
{
  /**
   * Returns the value of the '<em><b>Source</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Source</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Source</em>' containment reference.
   * @see #setSource(IdentifierPattern)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRecordAccessPattern_Source()
   * @model containment="true"
   * @generated
   */
  IdentifierPattern getSource();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessPattern#getSource <em>Source</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Source</em>' containment reference.
   * @see #getSource()
   * @generated
   */
  void setSource(IdentifierPattern value);

  /**
   * Returns the value of the '<em><b>Field</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field</em>' reference.
   * @see #setField(RecordFieldDeclaration)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getRecordAccessPattern_Field()
   * @model
   * @generated
   */
  RecordFieldDeclaration getField();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.RecordAccessPattern#getField <em>Field</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field</em>' reference.
   * @see #getField()
   * @generated
   */
  void setField(RecordFieldDeclaration value);

} // RecordAccessPattern
