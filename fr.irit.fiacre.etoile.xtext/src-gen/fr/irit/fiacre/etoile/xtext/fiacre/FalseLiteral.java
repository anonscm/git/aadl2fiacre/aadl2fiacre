/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>False Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getFalseLiteral()
 * @model
 * @generated
 */
public interface FalseLiteral extends BooleanLiteral
{
} // FalseLiteral
