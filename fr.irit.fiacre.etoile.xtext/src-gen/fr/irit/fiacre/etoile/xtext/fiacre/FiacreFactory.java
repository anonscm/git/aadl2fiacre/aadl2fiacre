/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage
 * @generated
 */
public interface FiacreFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  FiacreFactory eINSTANCE = fr.irit.fiacre.etoile.xtext.fiacre.impl.FiacreFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Model</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Model</em>'.
   * @generated
   */
  Model createModel();

  /**
   * Returns a new object of class '<em>Root Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Root Declaration</em>'.
   * @generated
   */
  RootDeclaration createRootDeclaration();

  /**
   * Returns a new object of class '<em>Named Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Named Element</em>'.
   * @generated
   */
  NamedElement createNamedElement();

  /**
   * Returns a new object of class '<em>Type Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Declaration Use</em>'.
   * @generated
   */
  TypeDeclarationUse createTypeDeclarationUse();

  /**
   * Returns a new object of class '<em>Constant Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Constant Declaration Use</em>'.
   * @generated
   */
  ConstantDeclarationUse createConstantDeclarationUse();

  /**
   * Returns a new object of class '<em>Expression Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression Declaration Use</em>'.
   * @generated
   */
  ExpressionDeclarationUse createExpressionDeclarationUse();

  /**
   * Returns a new object of class '<em>Reference Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Reference Declaration Use</em>'.
   * @generated
   */
  ReferenceDeclarationUse createReferenceDeclarationUse();

  /**
   * Returns a new object of class '<em>Union Tag Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Union Tag Declaration Use</em>'.
   * @generated
   */
  UnionTagDeclarationUse createUnionTagDeclarationUse();

  /**
   * Returns a new object of class '<em>Record Field Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Record Field Declaration Use</em>'.
   * @generated
   */
  RecordFieldDeclarationUse createRecordFieldDeclarationUse();

  /**
   * Returns a new object of class '<em>Pattern Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Pattern Declaration Use</em>'.
   * @generated
   */
  PatternDeclarationUse createPatternDeclarationUse();

  /**
   * Returns a new object of class '<em>Bound Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Bound Declaration Use</em>'.
   * @generated
   */
  BoundDeclarationUse createBoundDeclarationUse();

  /**
   * Returns a new object of class '<em>Path Declaration Use</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Path Declaration Use</em>'.
   * @generated
   */
  PathDeclarationUse createPathDeclarationUse();

  /**
   * Returns a new object of class '<em>Import Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import Declaration</em>'.
   * @generated
   */
  ImportDeclaration createImportDeclaration();

  /**
   * Returns a new object of class '<em>Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Declaration</em>'.
   * @generated
   */
  Declaration createDeclaration();

  /**
   * Returns a new object of class '<em>Parameterized Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Parameterized Declaration</em>'.
   * @generated
   */
  ParameterizedDeclaration createParameterizedDeclaration();

  /**
   * Returns a new object of class '<em>Type Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Declaration</em>'.
   * @generated
   */
  TypeDeclaration createTypeDeclaration();

  /**
   * Returns a new object of class '<em>Channel Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Channel Declaration</em>'.
   * @generated
   */
  ChannelDeclaration createChannelDeclaration();

  /**
   * Returns a new object of class '<em>Channel Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Channel Type</em>'.
   * @generated
   */
  ChannelType createChannelType();

  /**
   * Returns a new object of class '<em>Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type</em>'.
   * @generated
   */
  Type createType();

  /**
   * Returns a new object of class '<em>Basic Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Basic Type</em>'.
   * @generated
   */
  BasicType createBasicType();

  /**
   * Returns a new object of class '<em>Range Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Range Type</em>'.
   * @generated
   */
  RangeType createRangeType();

  /**
   * Returns a new object of class '<em>Union Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Union Type</em>'.
   * @generated
   */
  UnionType createUnionType();

  /**
   * Returns a new object of class '<em>Union Tags</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Union Tags</em>'.
   * @generated
   */
  UnionTags createUnionTags();

  /**
   * Returns a new object of class '<em>Union Tag Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Union Tag Declaration</em>'.
   * @generated
   */
  UnionTagDeclaration createUnionTagDeclaration();

  /**
   * Returns a new object of class '<em>Record Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Record Type</em>'.
   * @generated
   */
  RecordType createRecordType();

  /**
   * Returns a new object of class '<em>Record Fields</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Record Fields</em>'.
   * @generated
   */
  RecordFields createRecordFields();

  /**
   * Returns a new object of class '<em>Record Field Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Record Field Declaration</em>'.
   * @generated
   */
  RecordFieldDeclaration createRecordFieldDeclaration();

  /**
   * Returns a new object of class '<em>Queue Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Queue Type</em>'.
   * @generated
   */
  QueueType createQueueType();

  /**
   * Returns a new object of class '<em>Array Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Type</em>'.
   * @generated
   */
  ArrayType createArrayType();

  /**
   * Returns a new object of class '<em>Referenced Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Referenced Type</em>'.
   * @generated
   */
  ReferencedType createReferencedType();

  /**
   * Returns a new object of class '<em>Constant Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Constant Declaration</em>'.
   * @generated
   */
  ConstantDeclaration createConstantDeclaration();

  /**
   * Returns a new object of class '<em>Process Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Process Declaration</em>'.
   * @generated
   */
  ProcessDeclaration createProcessDeclaration();

  /**
   * Returns a new object of class '<em>Generic Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Declaration</em>'.
   * @generated
   */
  GenericDeclaration createGenericDeclaration();

  /**
   * Returns a new object of class '<em>Generic Type Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Type Declaration</em>'.
   * @generated
   */
  GenericTypeDeclaration createGenericTypeDeclaration();

  /**
   * Returns a new object of class '<em>Generic Constant Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Constant Declaration</em>'.
   * @generated
   */
  GenericConstantDeclaration createGenericConstantDeclaration();

  /**
   * Returns a new object of class '<em>Generic Union Tag Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Union Tag Declaration</em>'.
   * @generated
   */
  GenericUnionTagDeclaration createGenericUnionTagDeclaration();

  /**
   * Returns a new object of class '<em>Generic Record Field Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Record Field Declaration</em>'.
   * @generated
   */
  GenericRecordFieldDeclaration createGenericRecordFieldDeclaration();

  /**
   * Returns a new object of class '<em>State Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>State Declaration</em>'.
   * @generated
   */
  StateDeclaration createStateDeclaration();

  /**
   * Returns a new object of class '<em>Transition</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Transition</em>'.
   * @generated
   */
  Transition createTransition();

  /**
   * Returns a new object of class '<em>Component Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Component Declaration</em>'.
   * @generated
   */
  ComponentDeclaration createComponentDeclaration();

  /**
   * Returns a new object of class '<em>Ports Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ports Declaration</em>'.
   * @generated
   */
  PortsDeclaration createPortsDeclaration();

  /**
   * Returns a new object of class '<em>Port Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Declaration</em>'.
   * @generated
   */
  PortDeclaration createPortDeclaration();

  /**
   * Returns a new object of class '<em>Local Ports Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Local Ports Declaration</em>'.
   * @generated
   */
  LocalPortsDeclaration createLocalPortsDeclaration();

  /**
   * Returns a new object of class '<em>Parameters Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Parameters Declaration</em>'.
   * @generated
   */
  ParametersDeclaration createParametersDeclaration();

  /**
   * Returns a new object of class '<em>Parameter Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Parameter Declaration</em>'.
   * @generated
   */
  ParameterDeclaration createParameterDeclaration();

  /**
   * Returns a new object of class '<em>Variables Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variables Declaration</em>'.
   * @generated
   */
  VariablesDeclaration createVariablesDeclaration();

  /**
   * Returns a new object of class '<em>Variable Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variable Declaration</em>'.
   * @generated
   */
  VariableDeclaration createVariableDeclaration();

  /**
   * Returns a new object of class '<em>Priority Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Priority Declaration</em>'.
   * @generated
   */
  PriorityDeclaration createPriorityDeclaration();

  /**
   * Returns a new object of class '<em>Priority Group</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Priority Group</em>'.
   * @generated
   */
  PriorityGroup createPriorityGroup();

  /**
   * Returns a new object of class '<em>Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement</em>'.
   * @generated
   */
  Statement createStatement();

  /**
   * Returns a new object of class '<em>Null Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Null Statement</em>'.
   * @generated
   */
  NullStatement createNullStatement();

  /**
   * Returns a new object of class '<em>Tagged Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Tagged Statement</em>'.
   * @generated
   */
  TaggedStatement createTaggedStatement();

  /**
   * Returns a new object of class '<em>Tag Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Tag Declaration</em>'.
   * @generated
   */
  TagDeclaration createTagDeclaration();

  /**
   * Returns a new object of class '<em>Pattern Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Pattern Statement</em>'.
   * @generated
   */
  PatternStatement createPatternStatement();

  /**
   * Returns a new object of class '<em>Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Pattern</em>'.
   * @generated
   */
  Pattern createPattern();

  /**
   * Returns a new object of class '<em>Any Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Any Pattern</em>'.
   * @generated
   */
  AnyPattern createAnyPattern();

  /**
   * Returns a new object of class '<em>Constant Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Constant Pattern</em>'.
   * @generated
   */
  ConstantPattern createConstantPattern();

  /**
   * Returns a new object of class '<em>Integer Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer Pattern</em>'.
   * @generated
   */
  IntegerPattern createIntegerPattern();

  /**
   * Returns a new object of class '<em>Identifier Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Identifier Pattern</em>'.
   * @generated
   */
  IdentifierPattern createIdentifierPattern();

  /**
   * Returns a new object of class '<em>Conditional Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Conditional Statement</em>'.
   * @generated
   */
  ConditionalStatement createConditionalStatement();

  /**
   * Returns a new object of class '<em>Extended Conditional Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Extended Conditional Statement</em>'.
   * @generated
   */
  ExtendedConditionalStatement createExtendedConditionalStatement();

  /**
   * Returns a new object of class '<em>Select Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Select Statement</em>'.
   * @generated
   */
  SelectStatement createSelectStatement();

  /**
   * Returns a new object of class '<em>While Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>While Statement</em>'.
   * @generated
   */
  WhileStatement createWhileStatement();

  /**
   * Returns a new object of class '<em>Foreach Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Foreach Statement</em>'.
   * @generated
   */
  ForeachStatement createForeachStatement();

  /**
   * Returns a new object of class '<em>To Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>To Statement</em>'.
   * @generated
   */
  ToStatement createToStatement();

  /**
   * Returns a new object of class '<em>Case Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Case Statement</em>'.
   * @generated
   */
  CaseStatement createCaseStatement();

  /**
   * Returns a new object of class '<em>Loop Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Loop Statement</em>'.
   * @generated
   */
  LoopStatement createLoopStatement();

  /**
   * Returns a new object of class '<em>On Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>On Statement</em>'.
   * @generated
   */
  OnStatement createOnStatement();

  /**
   * Returns a new object of class '<em>Wait Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Wait Statement</em>'.
   * @generated
   */
  WaitStatement createWaitStatement();

  /**
   * Returns a new object of class '<em>Composition</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Composition</em>'.
   * @generated
   */
  Composition createComposition();

  /**
   * Returns a new object of class '<em>Block</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Block</em>'.
   * @generated
   */
  Block createBlock();

  /**
   * Returns a new object of class '<em>Composite Block</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Composite Block</em>'.
   * @generated
   */
  CompositeBlock createCompositeBlock();

  /**
   * Returns a new object of class '<em>Instance Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Instance Declaration</em>'.
   * @generated
   */
  InstanceDeclaration createInstanceDeclaration();

  /**
   * Returns a new object of class '<em>Port Set</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Port Set</em>'.
   * @generated
   */
  PortSet createPortSet();

  /**
   * Returns a new object of class '<em>Component Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Component Instance</em>'.
   * @generated
   */
  ComponentInstance createComponentInstance();

  /**
   * Returns a new object of class '<em>Generic Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Generic Instance</em>'.
   * @generated
   */
  GenericInstance createGenericInstance();

  /**
   * Returns a new object of class '<em>Type Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Instance</em>'.
   * @generated
   */
  TypeInstance createTypeInstance();

  /**
   * Returns a new object of class '<em>Constant Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Constant Instance</em>'.
   * @generated
   */
  ConstantInstance createConstantInstance();

  /**
   * Returns a new object of class '<em>Union Tag Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Union Tag Instance</em>'.
   * @generated
   */
  UnionTagInstance createUnionTagInstance();

  /**
   * Returns a new object of class '<em>Record Field Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Record Field Instance</em>'.
   * @generated
   */
  RecordFieldInstance createRecordFieldInstance();

  /**
   * Returns a new object of class '<em>Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Expression</em>'.
   * @generated
   */
  Expression createExpression();

  /**
   * Returns a new object of class '<em>Reference Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Reference Expression</em>'.
   * @generated
   */
  ReferenceExpression createReferenceExpression();

  /**
   * Returns a new object of class '<em>Identifier Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Identifier Expression</em>'.
   * @generated
   */
  IdentifierExpression createIdentifierExpression();

  /**
   * Returns a new object of class '<em>Record Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Record Expression</em>'.
   * @generated
   */
  RecordExpression createRecordExpression();

  /**
   * Returns a new object of class '<em>Field Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Field Expression</em>'.
   * @generated
   */
  FieldExpression createFieldExpression();

  /**
   * Returns a new object of class '<em>Queue Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Queue Expression</em>'.
   * @generated
   */
  QueueExpression createQueueExpression();

  /**
   * Returns a new object of class '<em>Enqueue Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Enqueue Expression</em>'.
   * @generated
   */
  EnqueueExpression createEnqueueExpression();

  /**
   * Returns a new object of class '<em>Append Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Append Expression</em>'.
   * @generated
   */
  AppendExpression createAppendExpression();

  /**
   * Returns a new object of class '<em>Literal Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Literal Expression</em>'.
   * @generated
   */
  LiteralExpression createLiteralExpression();

  /**
   * Returns a new object of class '<em>Boolean Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Literal</em>'.
   * @generated
   */
  BooleanLiteral createBooleanLiteral();

  /**
   * Returns a new object of class '<em>Natural Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Natural Literal</em>'.
   * @generated
   */
  NaturalLiteral createNaturalLiteral();

  /**
   * Returns a new object of class '<em>Lower Bound</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Lower Bound</em>'.
   * @generated
   */
  LowerBound createLowerBound();

  /**
   * Returns a new object of class '<em>Upper Bound</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Upper Bound</em>'.
   * @generated
   */
  UpperBound createUpperBound();

  /**
   * Returns a new object of class '<em>Natural Lower Bound</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Natural Lower Bound</em>'.
   * @generated
   */
  NaturalLowerBound createNaturalLowerBound();

  /**
   * Returns a new object of class '<em>Natural Upper Bound</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Natural Upper Bound</em>'.
   * @generated
   */
  NaturalUpperBound createNaturalUpperBound();

  /**
   * Returns a new object of class '<em>Decimal Lower Bound</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Decimal Lower Bound</em>'.
   * @generated
   */
  DecimalLowerBound createDecimalLowerBound();

  /**
   * Returns a new object of class '<em>Decimal Upper Bound</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Decimal Upper Bound</em>'.
   * @generated
   */
  DecimalUpperBound createDecimalUpperBound();

  /**
   * Returns a new object of class '<em>Variable Lower Bound</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variable Lower Bound</em>'.
   * @generated
   */
  VariableLowerBound createVariableLowerBound();

  /**
   * Returns a new object of class '<em>Variable Upper Bound</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variable Upper Bound</em>'.
   * @generated
   */
  VariableUpperBound createVariableUpperBound();

  /**
   * Returns a new object of class '<em>Infinite Upper Bound</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Infinite Upper Bound</em>'.
   * @generated
   */
  InfiniteUpperBound createInfiniteUpperBound();

  /**
   * Returns a new object of class '<em>Requirement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Requirement</em>'.
   * @generated
   */
  Requirement createRequirement();

  /**
   * Returns a new object of class '<em>Property Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Property Declaration</em>'.
   * @generated
   */
  PropertyDeclaration createPropertyDeclaration();

  /**
   * Returns a new object of class '<em>Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Property</em>'.
   * @generated
   */
  Property createProperty();

  /**
   * Returns a new object of class '<em>Pattern Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Pattern Property</em>'.
   * @generated
   */
  PatternProperty createPatternProperty();

  /**
   * Returns a new object of class '<em>LTL Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Pattern</em>'.
   * @generated
   */
  LTLPattern createLTLPattern();

  /**
   * Returns a new object of class '<em>Deadlock Free Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Deadlock Free Pattern</em>'.
   * @generated
   */
  DeadlockFreePattern createDeadlockFreePattern();

  /**
   * Returns a new object of class '<em>Infinitely Often Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Infinitely Often Pattern</em>'.
   * @generated
   */
  InfinitelyOftenPattern createInfinitelyOftenPattern();

  /**
   * Returns a new object of class '<em>Mortal Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Mortal Pattern</em>'.
   * @generated
   */
  MortalPattern createMortalPattern();

  /**
   * Returns a new object of class '<em>Presence Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Presence Pattern</em>'.
   * @generated
   */
  PresencePattern createPresencePattern();

  /**
   * Returns a new object of class '<em>Absence Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Absence Pattern</em>'.
   * @generated
   */
  AbsencePattern createAbsencePattern();

  /**
   * Returns a new object of class '<em>Always Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Always Pattern</em>'.
   * @generated
   */
  AlwaysPattern createAlwaysPattern();

  /**
   * Returns a new object of class '<em>Sequence Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sequence Pattern</em>'.
   * @generated
   */
  SequencePattern createSequencePattern();

  /**
   * Returns a new object of class '<em>LTL Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Property</em>'.
   * @generated
   */
  LTLProperty createLTLProperty();

  /**
   * Returns a new object of class '<em>State Event</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>State Event</em>'.
   * @generated
   */
  StateEvent createStateEvent();

  /**
   * Returns a new object of class '<em>Enter State Event</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Enter State Event</em>'.
   * @generated
   */
  EnterStateEvent createEnterStateEvent();

  /**
   * Returns a new object of class '<em>Leave State Event</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Leave State Event</em>'.
   * @generated
   */
  LeaveStateEvent createLeaveStateEvent();

  /**
   * Returns a new object of class '<em>Observable</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Observable</em>'.
   * @generated
   */
  Observable createObservable();

  /**
   * Returns a new object of class '<em>Observable Event</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Observable Event</em>'.
   * @generated
   */
  ObservableEvent createObservableEvent();

  /**
   * Returns a new object of class '<em>Path Event</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Path Event</em>'.
   * @generated
   */
  PathEvent createPathEvent();

  /**
   * Returns a new object of class '<em>Path</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Path</em>'.
   * @generated
   */
  Path createPath();

  /**
   * Returns a new object of class '<em>Path Item</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Path Item</em>'.
   * @generated
   */
  PathItem createPathItem();

  /**
   * Returns a new object of class '<em>Natural Item</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Natural Item</em>'.
   * @generated
   */
  NaturalItem createNaturalItem();

  /**
   * Returns a new object of class '<em>Named Item</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Named Item</em>'.
   * @generated
   */
  NamedItem createNamedItem();

  /**
   * Returns a new object of class '<em>Subject</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Subject</em>'.
   * @generated
   */
  Subject createSubject();

  /**
   * Returns a new object of class '<em>State Subject</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>State Subject</em>'.
   * @generated
   */
  StateSubject createStateSubject();

  /**
   * Returns a new object of class '<em>Value Subject</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Value Subject</em>'.
   * @generated
   */
  ValueSubject createValueSubject();

  /**
   * Returns a new object of class '<em>Tag Subject</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Tag Subject</em>'.
   * @generated
   */
  TagSubject createTagSubject();

  /**
   * Returns a new object of class '<em>Event Subject</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Event Subject</em>'.
   * @generated
   */
  EventSubject createEventSubject();

  /**
   * Returns a new object of class '<em>Tuple Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Tuple Type</em>'.
   * @generated
   */
  TupleType createTupleType();

  /**
   * Returns a new object of class '<em>Natural Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Natural Type</em>'.
   * @generated
   */
  NaturalType createNaturalType();

  /**
   * Returns a new object of class '<em>Integer Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer Type</em>'.
   * @generated
   */
  IntegerType createIntegerType();

  /**
   * Returns a new object of class '<em>Boolean Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Type</em>'.
   * @generated
   */
  BooleanType createBooleanType();

  /**
   * Returns a new object of class '<em>Unless Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unless Statement</em>'.
   * @generated
   */
  UnlessStatement createUnlessStatement();

  /**
   * Returns a new object of class '<em>Statement Choice</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement Choice</em>'.
   * @generated
   */
  StatementChoice createStatementChoice();

  /**
   * Returns a new object of class '<em>Statement Sequence</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Statement Sequence</em>'.
   * @generated
   */
  StatementSequence createStatementSequence();

  /**
   * Returns a new object of class '<em>Assign Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Assign Statement</em>'.
   * @generated
   */
  AssignStatement createAssignStatement();

  /**
   * Returns a new object of class '<em>Send Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Send Statement</em>'.
   * @generated
   */
  SendStatement createSendStatement();

  /**
   * Returns a new object of class '<em>Receive Statement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Receive Statement</em>'.
   * @generated
   */
  ReceiveStatement createReceiveStatement();

  /**
   * Returns a new object of class '<em>Constructor Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Constructor Pattern</em>'.
   * @generated
   */
  ConstructorPattern createConstructorPattern();

  /**
   * Returns a new object of class '<em>Array Access Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Access Pattern</em>'.
   * @generated
   */
  ArrayAccessPattern createArrayAccessPattern();

  /**
   * Returns a new object of class '<em>Record Access Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Record Access Pattern</em>'.
   * @generated
   */
  RecordAccessPattern createRecordAccessPattern();

  /**
   * Returns a new object of class '<em>All Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>All Expression</em>'.
   * @generated
   */
  AllExpression createAllExpression();

  /**
   * Returns a new object of class '<em>Exists Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Exists Expression</em>'.
   * @generated
   */
  ExistsExpression createExistsExpression();

  /**
   * Returns a new object of class '<em>Conditional</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Conditional</em>'.
   * @generated
   */
  Conditional createConditional();

  /**
   * Returns a new object of class '<em>Disjunction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Disjunction</em>'.
   * @generated
   */
  Disjunction createDisjunction();

  /**
   * Returns a new object of class '<em>Implication</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Implication</em>'.
   * @generated
   */
  Implication createImplication();

  /**
   * Returns a new object of class '<em>Conjunction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Conjunction</em>'.
   * @generated
   */
  Conjunction createConjunction();

  /**
   * Returns a new object of class '<em>Comparison Equal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Comparison Equal</em>'.
   * @generated
   */
  ComparisonEqual createComparisonEqual();

  /**
   * Returns a new object of class '<em>Comparison Not Equal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Comparison Not Equal</em>'.
   * @generated
   */
  ComparisonNotEqual createComparisonNotEqual();

  /**
   * Returns a new object of class '<em>Comparison Lesser</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Comparison Lesser</em>'.
   * @generated
   */
  ComparisonLesser createComparisonLesser();

  /**
   * Returns a new object of class '<em>Comparison Lesser Equal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Comparison Lesser Equal</em>'.
   * @generated
   */
  ComparisonLesserEqual createComparisonLesserEqual();

  /**
   * Returns a new object of class '<em>Comparison Greater</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Comparison Greater</em>'.
   * @generated
   */
  ComparisonGreater createComparisonGreater();

  /**
   * Returns a new object of class '<em>Comparison Greater Equal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Comparison Greater Equal</em>'.
   * @generated
   */
  ComparisonGreaterEqual createComparisonGreaterEqual();

  /**
   * Returns a new object of class '<em>Addition</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Addition</em>'.
   * @generated
   */
  Addition createAddition();

  /**
   * Returns a new object of class '<em>Substraction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Substraction</em>'.
   * @generated
   */
  Substraction createSubstraction();

  /**
   * Returns a new object of class '<em>Multiplication</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Multiplication</em>'.
   * @generated
   */
  Multiplication createMultiplication();

  /**
   * Returns a new object of class '<em>Division</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Division</em>'.
   * @generated
   */
  Division createDivision();

  /**
   * Returns a new object of class '<em>Modulo</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Modulo</em>'.
   * @generated
   */
  Modulo createModulo();

  /**
   * Returns a new object of class '<em>Unary Plus Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unary Plus Expression</em>'.
   * @generated
   */
  UnaryPlusExpression createUnaryPlusExpression();

  /**
   * Returns a new object of class '<em>Unary Minus Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unary Minus Expression</em>'.
   * @generated
   */
  UnaryMinusExpression createUnaryMinusExpression();

  /**
   * Returns a new object of class '<em>Unary Negation Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unary Negation Expression</em>'.
   * @generated
   */
  UnaryNegationExpression createUnaryNegationExpression();

  /**
   * Returns a new object of class '<em>Unary First Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unary First Expression</em>'.
   * @generated
   */
  UnaryFirstExpression createUnaryFirstExpression();

  /**
   * Returns a new object of class '<em>Unary Length Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unary Length Expression</em>'.
   * @generated
   */
  UnaryLengthExpression createUnaryLengthExpression();

  /**
   * Returns a new object of class '<em>Unary Coerce Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unary Coerce Expression</em>'.
   * @generated
   */
  UnaryCoerceExpression createUnaryCoerceExpression();

  /**
   * Returns a new object of class '<em>Unary Full Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unary Full Expression</em>'.
   * @generated
   */
  UnaryFullExpression createUnaryFullExpression();

  /**
   * Returns a new object of class '<em>Unary De Queue Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unary De Queue Expression</em>'.
   * @generated
   */
  UnaryDeQueueExpression createUnaryDeQueueExpression();

  /**
   * Returns a new object of class '<em>Unary Empty Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unary Empty Expression</em>'.
   * @generated
   */
  UnaryEmptyExpression createUnaryEmptyExpression();

  /**
   * Returns a new object of class '<em>Projection</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Projection</em>'.
   * @generated
   */
  Projection createProjection();

  /**
   * Returns a new object of class '<em>Array Access Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Array Access Expression</em>'.
   * @generated
   */
  ArrayAccessExpression createArrayAccessExpression();

  /**
   * Returns a new object of class '<em>Record Access Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Record Access Expression</em>'.
   * @generated
   */
  RecordAccessExpression createRecordAccessExpression();

  /**
   * Returns a new object of class '<em>Construction Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Construction Expression</em>'.
   * @generated
   */
  ConstructionExpression createConstructionExpression();

  /**
   * Returns a new object of class '<em>Explicit Array Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Explicit Array Expression</em>'.
   * @generated
   */
  ExplicitArrayExpression createExplicitArrayExpression();

  /**
   * Returns a new object of class '<em>Implicit Array Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Implicit Array Expression</em>'.
   * @generated
   */
  ImplicitArrayExpression createImplicitArrayExpression();

  /**
   * Returns a new object of class '<em>True Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>True Literal</em>'.
   * @generated
   */
  TrueLiteral createTrueLiteral();

  /**
   * Returns a new object of class '<em>False Literal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>False Literal</em>'.
   * @generated
   */
  FalseLiteral createFalseLiteral();

  /**
   * Returns a new object of class '<em>All Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>All Property</em>'.
   * @generated
   */
  AllProperty createAllProperty();

  /**
   * Returns a new object of class '<em>Exists Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Exists Property</em>'.
   * @generated
   */
  ExistsProperty createExistsProperty();

  /**
   * Returns a new object of class '<em>Property Disjunction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Property Disjunction</em>'.
   * @generated
   */
  PropertyDisjunction createPropertyDisjunction();

  /**
   * Returns a new object of class '<em>Property Implication</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Property Implication</em>'.
   * @generated
   */
  PropertyImplication createPropertyImplication();

  /**
   * Returns a new object of class '<em>Property Conjunction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Property Conjunction</em>'.
   * @generated
   */
  PropertyConjunction createPropertyConjunction();

  /**
   * Returns a new object of class '<em>Property Negation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Property Negation</em>'.
   * @generated
   */
  PropertyNegation createPropertyNegation();

  /**
   * Returns a new object of class '<em>Leads To Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Leads To Pattern</em>'.
   * @generated
   */
  LeadsToPattern createLeadsToPattern();

  /**
   * Returns a new object of class '<em>Precedes Pattern</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Precedes Pattern</em>'.
   * @generated
   */
  PrecedesPattern createPrecedesPattern();

  /**
   * Returns a new object of class '<em>LTL All</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL All</em>'.
   * @generated
   */
  LTLAll createLTLAll();

  /**
   * Returns a new object of class '<em>LTL Exists</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Exists</em>'.
   * @generated
   */
  LTLExists createLTLExists();

  /**
   * Returns a new object of class '<em>LTL Disjunction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Disjunction</em>'.
   * @generated
   */
  LTLDisjunction createLTLDisjunction();

  /**
   * Returns a new object of class '<em>LTL Implication</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Implication</em>'.
   * @generated
   */
  LTLImplication createLTLImplication();

  /**
   * Returns a new object of class '<em>LTL Conjunction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Conjunction</em>'.
   * @generated
   */
  LTLConjunction createLTLConjunction();

  /**
   * Returns a new object of class '<em>LTL Until</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Until</em>'.
   * @generated
   */
  LTLUntil createLTLUntil();

  /**
   * Returns a new object of class '<em>LTL Release</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Release</em>'.
   * @generated
   */
  LTLRelease createLTLRelease();

  /**
   * Returns a new object of class '<em>LTL Unary Negation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Unary Negation</em>'.
   * @generated
   */
  LTLUnaryNegation createLTLUnaryNegation();

  /**
   * Returns a new object of class '<em>LTL Unary Next</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Unary Next</em>'.
   * @generated
   */
  LTLUnaryNext createLTLUnaryNext();

  /**
   * Returns a new object of class '<em>LTL Unary Always</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Unary Always</em>'.
   * @generated
   */
  LTLUnaryAlways createLTLUnaryAlways();

  /**
   * Returns a new object of class '<em>LTL Unary Eventually</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Unary Eventually</em>'.
   * @generated
   */
  LTLUnaryEventually createLTLUnaryEventually();

  /**
   * Returns a new object of class '<em>LTL Variable</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Variable</em>'.
   * @generated
   */
  LTLVariable createLTLVariable();

  /**
   * Returns a new object of class '<em>Observable Disjunction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Observable Disjunction</em>'.
   * @generated
   */
  ObservableDisjunction createObservableDisjunction();

  /**
   * Returns a new object of class '<em>Observable Implication</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Observable Implication</em>'.
   * @generated
   */
  ObservableImplication createObservableImplication();

  /**
   * Returns a new object of class '<em>Observable Conjunction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Observable Conjunction</em>'.
   * @generated
   */
  ObservableConjunction createObservableConjunction();

  /**
   * Returns a new object of class '<em>Observable Negation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Observable Negation</em>'.
   * @generated
   */
  ObservableNegation createObservableNegation();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  FiacrePackage getFiacrePackage();

} //FiacreFactory
