/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;
import fr.irit.fiacre.etoile.xtext.fiacre.PropertyDeclaration;
import fr.irit.fiacre.etoile.xtext.fiacre.Requirement;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RequirementImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RequirementImpl#getPositive <em>Positive</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.impl.RequirementImpl#getNegative <em>Negative</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RequirementImpl extends MinimalEObjectImpl.Container implements Requirement
{
  /**
   * The cached value of the '{@link #getProperty() <em>Property</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProperty()
   * @generated
   * @ordered
   */
  protected PropertyDeclaration property;

  /**
   * The default value of the '{@link #getPositive() <em>Positive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPositive()
   * @generated
   * @ordered
   */
  protected static final String POSITIVE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPositive() <em>Positive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPositive()
   * @generated
   * @ordered
   */
  protected String positive = POSITIVE_EDEFAULT;

  /**
   * The default value of the '{@link #getNegative() <em>Negative</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNegative()
   * @generated
   * @ordered
   */
  protected static final String NEGATIVE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getNegative() <em>Negative</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNegative()
   * @generated
   * @ordered
   */
  protected String negative = NEGATIVE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RequirementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FiacrePackage.eINSTANCE.getRequirement();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PropertyDeclaration getProperty()
  {
    if (property != null && property.eIsProxy())
    {
      InternalEObject oldProperty = (InternalEObject)property;
      property = (PropertyDeclaration)eResolveProxy(oldProperty);
      if (property != oldProperty)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, FiacrePackage.REQUIREMENT__PROPERTY, oldProperty, property));
      }
    }
    return property;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PropertyDeclaration basicGetProperty()
  {
    return property;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setProperty(PropertyDeclaration newProperty)
  {
    PropertyDeclaration oldProperty = property;
    property = newProperty;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.REQUIREMENT__PROPERTY, oldProperty, property));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPositive()
  {
    return positive;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPositive(String newPositive)
  {
    String oldPositive = positive;
    positive = newPositive;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.REQUIREMENT__POSITIVE, oldPositive, positive));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getNegative()
  {
    return negative;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNegative(String newNegative)
  {
    String oldNegative = negative;
    negative = newNegative;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, FiacrePackage.REQUIREMENT__NEGATIVE, oldNegative, negative));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case FiacrePackage.REQUIREMENT__PROPERTY:
        if (resolve) return getProperty();
        return basicGetProperty();
      case FiacrePackage.REQUIREMENT__POSITIVE:
        return getPositive();
      case FiacrePackage.REQUIREMENT__NEGATIVE:
        return getNegative();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case FiacrePackage.REQUIREMENT__PROPERTY:
        setProperty((PropertyDeclaration)newValue);
        return;
      case FiacrePackage.REQUIREMENT__POSITIVE:
        setPositive((String)newValue);
        return;
      case FiacrePackage.REQUIREMENT__NEGATIVE:
        setNegative((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.REQUIREMENT__PROPERTY:
        setProperty((PropertyDeclaration)null);
        return;
      case FiacrePackage.REQUIREMENT__POSITIVE:
        setPositive(POSITIVE_EDEFAULT);
        return;
      case FiacrePackage.REQUIREMENT__NEGATIVE:
        setNegative(NEGATIVE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case FiacrePackage.REQUIREMENT__PROPERTY:
        return property != null;
      case FiacrePackage.REQUIREMENT__POSITIVE:
        return POSITIVE_EDEFAULT == null ? positive != null : !POSITIVE_EDEFAULT.equals(positive);
      case FiacrePackage.REQUIREMENT__NEGATIVE:
        return NEGATIVE_EDEFAULT == null ? negative != null : !NEGATIVE_EDEFAULT.equals(negative);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (positive: ");
    result.append(positive);
    result.append(", negative: ");
    result.append(negative);
    result.append(')');
    return result.toString();
  }

} //RequirementImpl
