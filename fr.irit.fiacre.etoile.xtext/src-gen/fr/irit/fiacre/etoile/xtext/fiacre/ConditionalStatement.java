/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conditional Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getConditions <em>Conditions</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getThen <em>Then</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getElseif <em>Elseif</em>}</li>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getElse <em>Else</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getConditionalStatement()
 * @model
 * @generated
 */
public interface ConditionalStatement extends Statement
{
  /**
   * Returns the value of the '<em><b>Conditions</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.Expression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Conditions</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Conditions</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getConditionalStatement_Conditions()
   * @model containment="true"
   * @generated
   */
  EList<Expression> getConditions();

  /**
   * Returns the value of the '<em><b>Then</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Then</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Then</em>' containment reference.
   * @see #setThen(Statement)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getConditionalStatement_Then()
   * @model containment="true"
   * @generated
   */
  Statement getThen();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getThen <em>Then</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Then</em>' containment reference.
   * @see #getThen()
   * @generated
   */
  void setThen(Statement value);

  /**
   * Returns the value of the '<em><b>Elseif</b></em>' containment reference list.
   * The list contents are of type {@link fr.irit.fiacre.etoile.xtext.fiacre.ExtendedConditionalStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Elseif</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elseif</em>' containment reference list.
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getConditionalStatement_Elseif()
   * @model containment="true"
   * @generated
   */
  EList<ExtendedConditionalStatement> getElseif();

  /**
   * Returns the value of the '<em><b>Else</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Else</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Else</em>' containment reference.
   * @see #setElse(Statement)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getConditionalStatement_Else()
   * @model containment="true"
   * @generated
   */
  Statement getElse();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.ConditionalStatement#getElse <em>Else</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Else</em>' containment reference.
   * @see #getElse()
   * @generated
   */
  void setElse(Statement value);

} // ConditionalStatement
