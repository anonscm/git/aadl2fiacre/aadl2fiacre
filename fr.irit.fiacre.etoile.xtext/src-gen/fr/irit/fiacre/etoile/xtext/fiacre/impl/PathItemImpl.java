/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre.impl;

import fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage;
import fr.irit.fiacre.etoile.xtext.fiacre.PathItem;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Path Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PathItemImpl extends MinimalEObjectImpl.Container implements PathItem
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PathItemImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FiacrePackage.eINSTANCE.getPathItem();
  }

} //PathItemImpl
