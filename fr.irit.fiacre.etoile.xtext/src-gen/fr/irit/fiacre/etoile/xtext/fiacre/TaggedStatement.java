/**
 */
package fr.irit.fiacre.etoile.xtext.fiacre;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tagged Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irit.fiacre.etoile.xtext.fiacre.TaggedStatement#getTag <em>Tag</em>}</li>
 * </ul>
 *
 * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getTaggedStatement()
 * @model
 * @generated
 */
public interface TaggedStatement extends Statement
{
  /**
   * Returns the value of the '<em><b>Tag</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tag</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tag</em>' containment reference.
   * @see #setTag(TagDeclaration)
   * @see fr.irit.fiacre.etoile.xtext.fiacre.FiacrePackage#getTaggedStatement_Tag()
   * @model containment="true"
   * @generated
   */
  TagDeclaration getTag();

  /**
   * Sets the value of the '{@link fr.irit.fiacre.etoile.xtext.fiacre.TaggedStatement#getTag <em>Tag</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Tag</em>' containment reference.
   * @see #getTag()
   * @generated
   */
  void setTag(TagDeclaration value);

} // TaggedStatement
