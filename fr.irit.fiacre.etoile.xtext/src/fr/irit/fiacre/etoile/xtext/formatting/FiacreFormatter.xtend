/* Copyright (C) 2018 INPT/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
*/
package fr.irit.fiacre.etoile.xtext.formatting

import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter
import org.eclipse.xtext.formatting.impl.FormattingConfig
import com.google.inject.Inject;
import fr.irit.fiacre.etoile.xtext.services.FiacreGrammarAccess
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.util.Pair
import org.eclipse.xtext.service.AbstractElementFinder.AbstractParserRuleElementFinder

/**
 * This class contains custom formatting declarations.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#formatting
 * on how and when to use it.
 * 
 * Also see {@link org.eclipse.xtext.xtext.XtextFormattingTokenSerializer} as an example
 */
class FiacreFormatter extends AbstractDeclarativeFormatter {

	@Inject FiacreGrammarAccess g

	override protected void configureFormatting(FormattingConfig c) {
		c.setAutoLinewrap(3000);
		// ML_COMMENT
		val ml_comment = g.ML_COMMENTRule
		c.setLinewrap.before(ml_comment)
		c.setLinewrap.after(ml_comment)
		// Model
		val model = g.modelAccess
		c.setLinewrap(2).before(model.rootAssignment_3_0)
		c.setLinewrap.after(model.rootAssignment_3_0)

		// ImportDeclaration
		val importDeclaration = g.importDeclarationAccess
		c.setLinewrap(2).after(importDeclaration.importURIAssignment_1)

		// TypeDeclaration	
		val t = g.typeDeclarationAccess
//		c.setLinewrap.before(t.typeKeyword_0)
//		c.setLinewrap.after(t.isKeyword_2)
		c.setLinewrap(2).after(t.valueAssignment_3)

		// ChannelDeclaration
		// TODO
		// ChannelType
		// TODO
		// TupleType
		// TODO
		// BasicType
		// BooleanType
//		val basic_type = g.basicTypeAccess
//		c.setLinewrap.after(basic_type.boolKeyword_2_1)
//		c.setLinewrap.after(basic_type.intKeyword_1_1)
//		c.setLinewrap.after(basic_type.natKeyword_0_1)
		// RangeType
		val range_type = g.rangeTypeAccess
		manageDelimitors(c, range_type.leftSquareBracketKeyword_0, range_type.rightSquareBracketKeyword_2_0_3)
		manageDelimitors(c, range_type.leftSquareBracketKeyword_0, range_type.rightSquareBracketKeyword_2_1_0)
		c.setIndentationIncrement.before(range_type.leftSquareBracketKeyword_0)
		c.setIndentationDecrement.after(range_type.rightSquareBracketKeyword_2_0_3)
		c.setIndentationDecrement.after(range_type.rightSquareBracketKeyword_2_1_0)
		c.setLinewrap.after(range_type.rightSquareBracketKeyword_2_0_3)
		c.setLinewrap.after(range_type.rightSquareBracketKeyword_2_1_0)

		// UnionType
		val union_type = g.unionTypeAccess
		c.setLinewrap.before(union_type.unionKeyword_0)
		c.setLinewrap.after(union_type.unionKeyword_0)
		c.setIndentationIncrement.after(union_type.unionKeyword_0)
		c.setLinewrap.after(union_type.tagsAssignment_1)
		c.setLinewrap.after(union_type.tagsAssignment_2_1)
		c.setIndentationDecrement().before(union_type.endKeyword_3)
		c.setLinewrap.after(union_type.verticalLineKeyword_2_0)
		c.setLinewrap.before(union_type.endKeyword_3)
		c.setLinewrap(2).after(union_type.endKeyword_3)

		// RecordType
		val record_type = g.recordTypeAccess
		c.setLinewrap().before(record_type.recordKeyword_0)
		c.setLinewrap().after(record_type.recordKeyword_0)
		c.setIndentationIncrement.after(record_type.recordKeyword_0)

		c.setLinewrap.after(record_type.fieldsAssignment_2_1)
		c.setIndentationDecrement().before(record_type.endKeyword_3)
		c.setLinewrap.after(record_type.commaKeyword_2_0)
		c.setLinewrap.before(record_type.endKeyword_3)
		c.setLinewrap(2).after(record_type.endKeyword_3)
		// c.setIndentationDecrement().after(record_type.endKeyword_3)
		// ArrayType
		val array_type = g.arrayTypeAccess
		c.setLinewrap.after(array_type.typeAssignment_3)
		c.setIndentationIncrement.before(array_type.arrayKeyword_0)
		c.setIndentationDecrement.after(array_type.arrayKeyword_0)
		c.setLinewrap.after(array_type.typeTypeParserRuleCall_3_0)

		// ReferencedType
//		val referenced_type = g.referencedTypeAccess
//		c.setLinewrap.after(referenced_type.typeTypeDeclarationUseCrossReference_0)
		// ProcessDeclaration
		val process_decl = g.processDeclarationAccess
//		c.setIndentationDecrement.before(process_decl.processKeyword_0)
//		c.setLinewrap.before(process_decl.processKeyword_0)
		c.setLinewrap.after(process_decl.nameAssignment_1)
//		c.setIndentationIncrement.after(process_decl.nameAssignment_1)
		manageInSideSeparators(c, g.processDeclarationAccess, process_decl.leftSquareBracketKeyword_3_0,
			process_decl.rightSquareBracketKeyword_3_3)
		c.setLinewrap.before(process_decl.portsAssignment_3_1)
		c.setLinewrap.before(process_decl.portsAssignment_3_2_1)
		c.setLinewrap.after(process_decl.commaKeyword_3_2_0)

		manageInSideSeparators(c, g.processDeclarationAccess, process_decl.leftParenthesisKeyword_4_0,
			process_decl.rightParenthesisKeyword_4_3)
		c.setLinewrap.after(process_decl.commaKeyword_4_2_0)

		c.setLinewrap.after(process_decl.isKeyword_5)
//		c.setIndentationIncrement.after(process_decl.isKeyword_5)
		// states
		c.setLinewrap.after(process_decl.statesKeyword_8)
		c.setIndentationIncrement.after(process_decl.statesKeyword_8)
		c.setIndentationDecrement.after(process_decl.statesAssignment_9)
//		c.setIndentationDecrement.after(process_decl.statesAssignment_9)
		c.setIndentationIncrement.before(process_decl.statesAssignment_10_1)
		c.setIndentationDecrement.after(process_decl.statesAssignment_10_1)
//		c.setIndentationDecrement.after(process_decl.statesAssignment_10_1)
		// var
		c.setLinewrap.before(process_decl.varKeyword_11_0)
		c.setLinewrap.after(process_decl.varKeyword_11_0)
		c.setIndentationIncrement.after(process_decl.varKeyword_11_0)
		c.setIndentationDecrement.after(process_decl.variablesAssignment_11_1)
		c.setIndentationIncrement.before(process_decl.variablesAssignment_11_2_1)
		c.setIndentationDecrement.after(process_decl.variablesAssignment_11_2_1)

		c.setLinewrap.before(process_decl.initKeyword_12_0)
		c.setLinewrap.after(process_decl.initKeyword_12_0)

		c.setIndentationIncrement.after(process_decl.initKeyword_12_0)
		c.setIndentationDecrement.after(process_decl.preludeAssignment_12_1)
		c.setLinewrap(2).after(process_decl.preludeAssignment_12_1)
		c.setLinewrap.after(process_decl.commaKeyword_10_0)
		c.setLinewrap.after(process_decl.commaKeyword_11_2_0)
//		c.setLinewrap.before(process_decl.transitionsAssignment_13)
//		c.setLinewrap.after(process_decl.transitionsAssignment_13)
		// Transition
		val transition = g.transitionAccess
//		c.setLinewrap.before(transition.fromKeyword_0)
//		c.setIndentationIncrement.before(transition.fromKeyword_0)
		c.setLinewrap.after(transition.originStateDeclarationCrossReference_1_0)
//		c.setIndentationIncrement.before(transition.actionAssignment_2)
//		c.setIndentationDecrement.after(transition.actionAssignment_2)
//		c.setIndentationDecrement.after(transition.actionAssignment_2)
		c.setLinewrap(2).after(transition.actionAssignment_2)

		// ComponentDeclaration
		val componentDeclaration = g.componentDeclarationAccess
		c.setLinewrap.after(componentDeclaration.nameAssignment_1)
		// generics
		manageOutSideSeparators(c, componentDeclaration, componentDeclaration.lessThanSignVerticalLineKeyword_2_0,
			componentDeclaration.verticalLineGreaterThanSignKeyword_2_3)
		manageInSideSeparators(c, componentDeclaration, componentDeclaration.lessThanSignVerticalLineKeyword_2_0,
			componentDeclaration.verticalLineGreaterThanSignKeyword_2_3)
		c.setLinewrap.before(componentDeclaration.genericsAssignment_2_1)
		c.setLinewrap.before(componentDeclaration.genericsAssignment_2_2_1)
		c.setLinewrap.after(componentDeclaration.commaKeyword_2_2_0)
		// ports
		manageOutSideSeparators(c, componentDeclaration, componentDeclaration.leftSquareBracketKeyword_3_0,
			componentDeclaration.rightSquareBracketKeyword_3_3)
		manageInSideSeparators(c, componentDeclaration, componentDeclaration.leftSquareBracketKeyword_3_0,
			componentDeclaration.rightSquareBracketKeyword_3_3)
		c.setLinewrap.before(componentDeclaration.portsAssignment_3_1)
		c.setLinewrap.before(componentDeclaration.portsAssignment_3_2_1)
		c.setLinewrap.after(componentDeclaration.commaKeyword_3_2_0)
		// parameters
		manageOutSideSeparators(c, componentDeclaration, componentDeclaration.leftParenthesisKeyword_4_0,
			componentDeclaration.rightParenthesisKeyword_4_3)
		manageInSideSeparators(c, componentDeclaration, componentDeclaration.leftParenthesisKeyword_4_0,
			componentDeclaration.rightParenthesisKeyword_4_3)
		c.setLinewrap.after(componentDeclaration.commaKeyword_4_2_0)
		c.setLinewrap.after(componentDeclaration.isKeyword_5)

		// var
//		c.setLinewrap.before(componentDeclaration.varKeyword_6_0)
//		c.setLinewrap.after(componentDeclaration.varKeyword_6_0)
//		c.setLinewrap.after(componentDeclaration.commaKeyword_6_2_0)
//		c.setIndentationIncrement.after(componentDeclaration.varKeyword_6_0)
//		c.setIndentationDecrement.after(componentDeclaration.variablesAssignment_6_1)
//		c.setIndentationIncrement.before(componentDeclaration.variablesAssignment_6_2_1)
//		c.setIndentationDecrement.after(componentDeclaration.variablesAssignment_6_2_1)

		// port
		c.setLinewrap.before(componentDeclaration.portKeyword_7_0)
		c.setLinewrap.after(componentDeclaration.portKeyword_7_0)
		c.setLinewrap.after(componentDeclaration.commaKeyword_7_2_0)
		c.setIndentationIncrement.after(componentDeclaration.portKeyword_7_0)
		c.setIndentationDecrement.after(componentDeclaration.localPortsAssignment_7_1)
		c.setIndentationIncrement.before(componentDeclaration.localPortsAssignment_7_2_1)
		c.setIndentationDecrement.after(componentDeclaration.localPortsAssignment_7_2_1)

		// priority
		c.setLinewrap.before(componentDeclaration.priorityKeyword_8_0)
		c.setLinewrap.after(componentDeclaration.priorityKeyword_8_0)
		c.setLinewrap(2).after(componentDeclaration.commaKeyword_8_2_0)
		c.setLinewrap(2).after(componentDeclaration.prioritiesAssignment_8_2_1)
		c.setIndentationIncrement.after(componentDeclaration.priorityKeyword_8_0)
		c.setIndentationDecrement.after(componentDeclaration.prioritiesAssignment_8_1)
		c.setIndentationIncrement.before(componentDeclaration.prioritiesAssignment_8_2_1)
		c.setIndentationDecrement.after(componentDeclaration.prioritiesAssignment_8_2_1)
		c.setLinewrap.before(componentDeclaration.bodyAssignment_10)

		// LocalPortsDeclaration
		val localPortsDeclaration = g.localPortsDeclarationAccess
		c.setLinewrap.after(localPortsDeclaration.commaKeyword_1_0)
		// PriorityDeclaration
		val priorityDeclaration = g.priorityDeclarationAccess
		c.setLinewrap.before(priorityDeclaration.greaterThanSignKeyword_1_0)
		c.setLinewrap.after(priorityDeclaration.greaterThanSignKeyword_1_0)

		// PriorityGroup
		val priorityGroup = g.priorityGroupAccess
		c.setLinewrap.before(priorityGroup.verticalLineKeyword_1_0)
		c.setLinewrap.after(priorityGroup.verticalLineKeyword_1_0)
		// PortsDeclaration
//		val ports_decl = g.portsDeclarationAccess
//		c.setLinewrap.before(ports_decl.portsAssignment_1_1)
//		
//		//PortDeclaration
//		val port_decl = g.portDeclarationAccess
//		c.setLinewrap.before(port_decl.nameIDTerminalRuleCall_0)
		// find common keywords an specify formatting for them
//		for (Pair<Keyword, Keyword> pair : g.findKeywordPairs("(", ")")) {
//			c.setIndentationIncrement().after(pair.getFirst());
//			c.setNoSpace().after(pair.getFirst());
//			c.setLinewrap.after(pair.getFirst());
//			c.setIndentationDecrement().before(pair.getSecond());
//			c.setNoSpace().before(pair.getSecond());
//			c.setLinewrap(0, 0, 1).before(pair.getSecond());
//		}
		// UnlessStatement
		val unlessStatement = g.unlessStatementAccess
		c.setLinewrap.before(unlessStatement.unlessKeyword_1_1)
		c.setLinewrap.after(unlessStatement.unlessKeyword_1_1)

		// StatementChoice
		val statementChoice = g.statementChoiceAccess
		c.setLinewrap.before(statementChoice.leftSquareBracketRightSquareBracketKeyword_1_1)
		c.setLinewrap.after(statementChoice.leftSquareBracketRightSquareBracketKeyword_1_1)
		// SequenceStatement
		val statementSequence = g.statementSequenceAccess
		c.setLinewrap.after(statementSequence.semicolonKeyword_1_1)
		c.setLinewrap.after(statementSequence.statementsAssignment_1_2)

		// ConditionalStatement
		val conditionalStatement = g.conditionalStatementAccess
		c.setLinewrap.before(conditionalStatement.thenKeyword_2)
		c.setLinewrap.after(conditionalStatement.thenKeyword_2)
		c.setIndentationIncrement.after(conditionalStatement.thenKeyword_2)
		c.setIndentationDecrement.after(conditionalStatement.thenAssignment_3)
		c.setLinewrap.before(conditionalStatement.elseKeyword_5_0)
		c.setLinewrap.after(conditionalStatement.elseKeyword_5_0)
		c.setIndentationIncrement.after(conditionalStatement.elseKeyword_5_0)
		c.setIndentationDecrement.after(conditionalStatement.elseAssignment_5_1)
		c.setLinewrap.before(conditionalStatement.endKeyword_6)

		// ExtendedConditionalStatement
		// SelectStatement
		val selectStatement = g.selectStatementAccess
		c.setLinewrap.after(selectStatement.selectKeyword_0)
		c.setIndentationIncrement.after(selectStatement.selectKeyword_0)
		c.setIndentationDecrement.after(selectStatement.bodyAssignment_1)
		c.setLinewrap.before(selectStatement.endKeyword_3)
		c.setLinewrap.after(selectStatement.endKeyword_3)
		// Composition
		val composition = g.compositionAccess
		c.setLinewrap.after(composition.inKeyword_1_2)
		c.setLinewrap.after(composition.verticalLineVerticalLineKeyword_3_0)
//		c.setIndentationDecrement.before(composition.parKeyword_0)
		c.setIndentationIncrement.after(composition.inKeyword_1_2)
		c.setIndentationDecrement.before(composition.endKeyword_5)
		if (composition.parKeyword_6 != null) {
			c.setLinewrap.after(composition.parKeyword_6)
		} else {
			c.setLinewrap.after(composition.endKeyword_5)
		}
		// ComponentInstance
		val componentInstance = g.componentInstanceAccess
		c.setLinewrap.after(componentInstance.componentParameterizedDeclarationCrossReference_0_0)
		manageOutSideSeparators(c, componentInstance, componentInstance.lessThanSignVerticalLineKeyword_1_0,
			componentInstance.verticalLineGreaterThanSignKeyword_1_3)
		manageInSideSeparators(c, componentInstance, componentInstance.lessThanSignVerticalLineKeyword_1_0,
			componentInstance.verticalLineGreaterThanSignKeyword_1_3)
		manageOutSideSeparators(c, componentInstance, componentInstance.leftSquareBracketKeyword_2_0,
			componentInstance.rightSquareBracketKeyword_2_3)
		manageInSideSeparators(c, componentInstance, componentInstance.leftSquareBracketKeyword_2_0,
			componentInstance.rightSquareBracketKeyword_2_3)
		manageOutSideSeparators(c, componentInstance, componentInstance.leftParenthesisKeyword_3_0,
			componentInstance.rightParenthesisKeyword_3_3)
		manageInSideSeparators(c, componentInstance, componentInstance.leftParenthesisKeyword_3_0,
			componentInstance.rightParenthesisKeyword_3_3)
		c.setLinewrap.after(componentInstance.commaKeyword_1_2_0)
		c.setLinewrap.after(componentInstance.commaKeyword_2_2_0)
		c.setLinewrap.after(componentInstance.commaKeyword_3_2_0)

		// arrayExpression
		val arrayExpression = g.arrayExpressionAccess
		manageOutSideSeparators(c, arrayExpression, arrayExpression.leftSquareBracketKeyword_0,
			arrayExpression.rightSquareBracketKeyword_3)
		manageInSideSeparators(c, arrayExpression, arrayExpression.leftSquareBracketKeyword_0,
			arrayExpression.rightSquareBracketKeyword_3)
		c.setLinewrap.after(arrayExpression.commaKeyword_2_0_1_0)
		
		//VariablesDeclaration
//		val variablesDeclaration = g.variablesDeclarationAccess
//		c.setNoLinewrap.after(variablesDeclaration.colonEqualsSignKeyword_4_0)
//		for (Keyword annex : g.findKeywords("from")) {
//			c.setLinewrap(1, 1, 2).before(annex);
//			c.setIndentationDecrement().before(annex);
//			c.setIndentationIncrement().after(annex);
//		}
//		for (Keyword annex : g.findKeywords("process")) {
//			c.setLinewrap(1, 1, 2).before(annex);
//			c.setIndentationDecrement().before(annex);
////			c.setIndentationIncrement().after(annex);
//		}
//		for (Keyword annex : g.findKeywords("type")) {
//			c.setLinewrap(1, 1, 2).before(annex);
//			c.setIndentationDecrement().before(annex);
//			c.setIndentationIncrement().after(annex);
//		}
//		for (Keyword annex : g.findKeywords("process")) {
//			c.setLinewrap(1, 1, 2).before(annex);
//			c.setIndentationDecrement().before(annex);
//			c.setIndentationIncrement().after(annex);
//		}
//
//		for (Keyword annex : g.findKeywords("component")) {
//			c.setLinewrap(1, 1, 2).before(annex);
//			c.setIndentationDecrement().before(annex);
//			c.setIndentationIncrement().after(annex);
//		}
//		for (Pair<Keyword, Keyword> pair : g.findKeywordPairs("{", "}")) {
//			c.setIndentationIncrement().after(pair.getFirst());
//			c.setNoSpace().after(pair.getFirst());
//			c.setLinewrap(0, 0, 1).after(pair.getFirst());
//			c.setIndentationDecrement().before(pair.getSecond());
//			c.setNoSpace().before(pair.getSecond());
//			c.setLinewrap(0, 0, 1).before(pair.getSecond());
//		}
		for (Keyword comma : g.findKeywords(",")) {
			c.setNoSpace().before(comma);
			c.setNoLinewrap().before(comma);
			c.setLinewrap(0, 0, 1).after(comma);
		}
		for (Keyword semi : g.findKeywords(";")) {
			c.setNoSpace().before(semi);
			c.setNoLinewrap().before(semi);
			c.setLinewrap(0, 1, 2).after(semi);
		}
		for (Keyword dot : g.findKeywords(".")) {
			c.setNoSpace().around(dot);
			c.setNoLinewrap().around(dot);
		}
		for (Keyword colon : g.findKeywords(":")) {
			c.setNoSpace().before(colon);
			c.setNoLinewrap().before(colon);
		}
//		for (Keyword doublecolon : g.findKeywords("::")) {
//			c.setNoSpace().around(doublecolon);
//			c.setNoLinewrap().around(doublecolon);
//		}
//		for (Keyword left : g.findKeywords("-[")) {
//			c.setNoSpace().after(left);
//			c.setNoLinewrap().after(left);
//		}
//		for (Keyword right : g.findKeywords("]->")) {
//			c.setNoSpace().before(right);
//			c.setNoLinewrap().before(right);
//		}
//		for (Keyword arrow : g.findKeywords("->")) {
//			c.setLinewrap(0, 0, 1).after(arrow);
//		}
	}

	def protected void manageInSideSeparators(FormattingConfig c, AbstractParserRuleElementFinder ruleAccess,
		Keyword left, Keyword right) {
		for (Pair<Keyword, Keyword> pair : ruleAccess.findKeywordPairs(left.value, right.value)) {
			c.setLinewrap.after(pair.first)
//			c.setIndentationIncrement.before(pair.first)
			c.setIndentationIncrement.after(pair.first)
			c.setLinewrap.before(pair.second)
			c.setIndentationDecrement.before(pair.second)
//			c.setIndentationDecrement.after(pair.second)
			c.setLinewrap.after(pair.second)
		}
	}

	def protected void manageOutSideSeparators(FormattingConfig c, AbstractParserRuleElementFinder ruleAccess,
		Keyword left, Keyword right) {
		for (Pair<Keyword, Keyword> pair : ruleAccess.findKeywordPairs(left.value, right.value)) {
			c.setIndentationIncrement.before(pair.first)
			c.setIndentationDecrement.after(pair.second)
		}
	}

//	def protected void manageParenthesesForDeclaration(FormattingConfig c, AbstractParserRuleElementFinder ruleAccess) {
//		for (Pair<Keyword, Keyword> pair : ruleAccess.findKeywordPairs("(", ")")) {
//			c.setLinewrap.after(pair.first)
//			c.setIndentationIncrement.before(pair.first)
//			c.setIndentationIncrement.after(pair.first)
//			c.setLinewrap.before(pair.second)
//			c.setIndentationDecrement.before(pair.second)
//			c.setIndentationDecrement.after(pair.second)
//			c.setLinewrap.after(pair.second)
//		}
//	}
	def protected void manageDelimitors(FormattingConfig c, Keyword first, Keyword second) {
//		for (Pair<Keyword, Keyword> pair : ruleAccess.findKeywordPairs("[", "]")) {
//			c.setIndentationIncrement.after(pair.first)
		c.setNoSpace.after(first)
//			c.setLinewrap(0, 0, 1).after(pair.first)
//			c.setIndentationDecrement.before(pair.second)
		c.setNoSpace.before(second)
//			c.setLinewrap(0, 0, 1).before(pair.second)
	}
}
