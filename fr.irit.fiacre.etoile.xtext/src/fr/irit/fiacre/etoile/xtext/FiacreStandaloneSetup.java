/*
 * generated by Xtext
 */
package fr.irit.fiacre.etoile.xtext;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class FiacreStandaloneSetup extends FiacreStandaloneSetupGenerated{

	public static void doSetup() {
		new FiacreStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

