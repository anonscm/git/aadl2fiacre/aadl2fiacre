/* Copyright (C) 2018 INPT/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
*/
package fr.irit.fiacre.etoile.xtext.scoping

import fr.irit.fiacre.etoile.xtext.fiacre.IdentifierExpression
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider
import org.eclipse.emf.ecore.EReference
import fr.irit.fiacre.etoile.xtext.fiacre.ComponentInstance
import fr.irit.fiacre.etoile.xtext.fiacre.ExplicitArrayExpression
import fr.irit.fiacre.etoile.xtext.fiacre.IdentifierPattern
import fr.irit.fiacre.etoile.xtext.fiacre.AssignStatement
import java.util.Collection
import org.eclipse.emf.ecore.EObject
import java.util.ArrayList
import fr.irit.fiacre.etoile.xtext.fiacre.ProcessDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ParametersDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ParameterDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.VariablesDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.VariableDeclaration
import org.eclipse.xtext.scoping.Scopes
import fr.irit.fiacre.etoile.xtext.fiacre.ParameterizedDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.ComponentDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.LocalPortsDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.PortDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.PortsDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.Model
import fr.irit.fiacre.etoile.xtext.fiacre.TypeDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.Type
import fr.irit.fiacre.etoile.xtext.fiacre.RecordType
import java.util.List
import org.eclipse.emf.common.util.TreeIterator
import fr.irit.fiacre.etoile.xtext.fiacre.UnionType
import fr.irit.fiacre.etoile.xtext.fiacre.SelectStatement
import fr.irit.fiacre.etoile.xtext.fiacre.ConstantDeclaration
import fr.irit.fiacre.etoile.xtext.fiacre.SendStatement
import fr.irit.fiacre.etoile.xtext.fiacre.ReceiveStatement
import fr.irit.fiacre.etoile.xtext.fiacre.PatternStatement
import fr.irit.fiacre.etoile.xtext.fiacre.StatementSequence

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 * 
 */
class FiacreScopeProvider extends AbstractDeclarativeScopeProvider {

	def IScope scope_StateDeclaration(ProcessDeclaration process, EReference ref) {
		println("scope_StateDeclaration")
		buildStateDeclaration(process)
	}

	def IScope scope_ExpressionDeclarationUse(ProcessDeclaration process, EReference ref) {
		println("scope_ExpressionDeclarationUse")
		buildExpressionDeclarationUse(process)
	}

	def IScope scope_TypeDeclarationUse(ProcessDeclaration process, EReference ref) {
		println("scope_TypeDeclarationUse")
		buildTypeDeclarationUse(process)
	}

	def IScope scope_RecordFieldDeclarationUse(ProcessDeclaration process, EReference ref) {
		println("scope_RecordFieldDeclarationUse")
		buildRecordFieldDeclarationUse(process)
	}

	def IScope scope_IdentifierExpression_declaration(IdentifierExpression id, EReference ref) {
		if (id.eContainer instanceof ComponentInstance) {
			println("first if")
			if (((id.eContainer as ComponentInstance)).ports.contains(id)) {
				println("second if")
				buildPortDeclarations(id)
			}
		} else {
			if (id.eContainer instanceof ExplicitArrayExpression) {
				if (((id.eContainer as ExplicitArrayExpression).eContainer as ComponentInstance).ports.contains(
					id.eContainer)) {
					buildPortDeclarations(id)
				}
			}
		}
	}

	def IScope scope_IdentifierPattern_declaration(IdentifierPattern id, EReference ref) {
		switch id.eContainer {
			AssignStatement: {
				println("assign")
				if ((id.eContainer as AssignStatement).patterns.contains(id)) {
					println("assign2")
					buildIdentifierPatternParametersAndVariables(id)
				}

			}
			SendStatement: {
				if ((id.eContainer as SendStatement).port == id) {
					buildIdentifierPatternPorts(id)
				}
			}
			ReceiveStatement: {
				if ((id.eContainer as ReceiveStatement).port == id) {
					buildIdentifierPatternPorts(id)
				} else {
					if ((id.eContainer as ReceiveStatement).patterns.contains(id)) {
						buildIdentifierPatternParametersAndVariables(id)
					}
				}
			}
			StatementSequence:{
				buildIdentifierPatternPorts(id)
			}
		}
	}
	



	def buildIdentifierPatternPorts(IdentifierPattern pattern) {
		val Collection<EObject> tags = new ArrayList<EObject>()
		val ProcessDeclaration process_decl = eContainer(pattern, ProcessDeclaration) as ProcessDeclaration
		for (port_d : process_decl.ports) {
			println("Scope PortDeclaration ProcessDeclaration")
			tags.addAll(port_d.ports)
		}
		for (port_d : process_decl.localPorts) {
			println("Scope LocalPortDeclaration ProcessDeclaration")
			tags.addAll(port_d.ports)
		}
		return Scopes.scopeFor(tags)
	}

	def IScope buildIdentifierPatternParametersAndVariables(IdentifierPattern id) {
		val Collection<EObject> tags = new ArrayList<EObject>()
		val ProcessDeclaration process_decl = eContainer(id, ProcessDeclaration) as ProcessDeclaration
		for (psd : process_decl.parameters) {
			for (pard : psd.parameters) {
				tags.add(pard)
			}
		}
		for (vsd : process_decl.variables) {
			for (vard : vsd.variables) {
				tags.add(vard)
			}
		}
		val Model model = process_decl.eContainer as Model
		for (d : model.declarations) {
			if (d instanceof TypeDeclaration) {
				val Type t = (d as TypeDeclaration).value
				if (t instanceof UnionType) {
					for (ut : (t as UnionType).tags) {
						tags.addAll(ut.tags)
					}
				}
			}
		}
		return Scopes.scopeFor(tags)
	}

	def IScope buildPortDeclarations(IdentifierExpression id) {
		val Collection<EObject> tags = new ArrayList<EObject>()
		val ParameterizedDeclaration pard = eContainer(id, ParameterizedDeclaration) as ParameterizedDeclaration
		println(pard)
		if (pard instanceof ComponentDeclaration) {
			for (eports : (pard as ComponentDeclaration).localPorts) {
				for (port : eports.ports) {
					tags.add(port)
				}
			}
			for (eports : (pard as ComponentDeclaration).ports) {
				for (port : eports.ports) {
					tags.add(port)
				}
			}
		} else {
			for (eports : (pard as ProcessDeclaration).ports) {
				for (port : eports.ports) {
					tags.add(port)
				}
			}
		}
		return Scopes.scopeFor(tags)
	}

	def static EObject eContainer(EObject root, Class<?> type) {
		var EObject current = root
		while (current != null) {
			if (type.isInstance(current)) {
				return current
			} else {
				current = current.eContainer
			}
		}
		return null
	}

	def IScope buildRecordFieldDeclarationUse(ProcessDeclaration process) {
		val Collection<EObject> tags = new ArrayList<EObject>()
		for (dec : process.generics) {
			tags.add(dec)
		}

		val Model model = process.eContainer as Model
		for (d : model.getDeclarations()) {
			if (d instanceof TypeDeclaration) {
				val Type t = (d as TypeDeclaration).value
				if (t instanceof RecordType) {
					val RecordType u = t as RecordType
					for (ts : u.fields) {
						for (s : ts.fields) {
							println(s.getName() + " record ")
						}
						tags.addAll(ts.getFields())
					}
				}
			}
		}
		return Scopes.scopeFor(tags)
	}

	def IScope buildPatternDeclarationUse(ProcessDeclaration pdec) {
		val Collection<EObject> tags = new ArrayList<EObject>()

		for (vd : pdec.variables) {
			println("Scope VariableDeclaration ProcessDeclaration")
			tags.addAll(vd.variables)
		}
		for (port_d : pdec.ports) {
			println("Scope PortDeclaration ProcessDeclaration")
			tags.addAll(port_d.ports)
		}
		for (port_d : pdec.localPorts) {
			println("Scope LocalPortDeclaration ProcessDeclaration")
			tags.addAll(port_d.ports)
		}
//		for (VariablesDeclaration vd : pdec.getVariables()) {
//			System.out
//					.println("Scope VariableDeclaration in ProcessDeclaration");
//			for (VariableDeclaration s : vd.getVariables()) {
//				System.out.print(s.getName() + " ");
//			}
//			tags.addAll(vd.getVariables());
//		}
		for (vd : pdec.parameters) {
			println("Scope ParameterDeclaration in ProcessDeclaration")
			tags.addAll(vd.parameters)
		}

		val Model model = pdec.eContainer as Model
		for (d : model.declarations) {
			if (d instanceof TypeDeclaration) {
				val Type t = (d as TypeDeclaration).value
				if (t instanceof UnionType) {
					for (ut : (t as UnionType).tags) {
						tags.addAll(ut.tags)
					}
				}
			}
		}
		return Scopes.scopeFor(tags)
	}

	def static List<EObject> eAllSelectStatement(EObject source) {
		val TreeIterator<EObject> contentIterator = source.eAllContents()
		val List<EObject> result = new ArrayList<EObject>()

		while (contentIterator.hasNext) {
			val EObject next = contentIterator.next
			if (next instanceof SelectStatement) {
				result.add(next)
			}
		}
		return result
	}

	def IScope buildStateDeclaration(ProcessDeclaration process) {
		return Scopes.scopeFor(process.getStates())
	}

	def IScope buildExpressionDeclarationUse(ProcessDeclaration d) {
		val Collection<EObject> tags = new ArrayList<EObject>()
		val Model model = d.eContainer as Model
		for (dec : model.declarations) {
			if (dec instanceof ConstantDeclaration) {
				tags.add(dec)
			}
			if (dec instanceof TypeDeclaration) {
				val Type t = (dec as TypeDeclaration).value
				if (t instanceof UnionType) {
					for (ts : (t as UnionType).tags) {
						tags.addAll(ts.tags)
					}
				}
			}
			for (gd : d.generics) {
				tags.add(gd)
			}
		}

		val ProcessDeclaration pdec = d as ProcessDeclaration
		for (vd : pdec.parameters) {
			println("Scope ParameterDeclaration in ProcessDeclaration")
			tags.addAll(vd.parameters)
		}
		for (vd : pdec.variables) {
			tags.addAll(vd.variables)
		}

		val List<EObject> e = eAllSelectStatement(pdec)
		for (EObject ss : e) {
			if ((ss as SelectStatement).index != null) {
				tags.add((ss as SelectStatement).index)
			}
		}
		return Scopes.scopeFor(tags)
	}

	def IScope buildTypeDeclarationUse(ProcessDeclaration p) {
		val Collection<EObject> tags = new ArrayList<EObject>()
		val Model model = p.eContainer as Model
		for (dec : model.declarations) {
			if (dec instanceof TypeDeclaration) {
				tags.add(dec)
			}
		}
		for (dec : p.generics) {
			tags.add(dec)
		}
		return Scopes.scopeFor(tags)
	}
}
