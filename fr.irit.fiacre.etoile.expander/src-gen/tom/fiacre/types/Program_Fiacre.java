
package fiacre.types;


public abstract class Program_Fiacre extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected Program_Fiacre() {}



  /**
   * Returns true if the term is rooted by the symbol Prog
   *
   * @return true if the term is rooted by the symbol Prog
   */
  public boolean isProg() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot main
   *
   * @return the subterm corresponding to the slot main
   */
  public fiacre.types.Optional_Decl getmain() {
    throw new UnsupportedOperationException("This Program_Fiacre has no main");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot main
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot main is replaced by _arg
   */
  public Program_Fiacre setmain(fiacre.types.Optional_Decl _arg) {
    throw new UnsupportedOperationException("This Program_Fiacre has no main");
  }

  /**
   * Returns the subterm corresponding to the slot decl
   *
   * @return the subterm corresponding to the slot decl
   */
  public fiacre.types.Declarations getdecl() {
    throw new UnsupportedOperationException("This Program_Fiacre has no decl");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot decl
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot decl is replaced by _arg
   */
  public Program_Fiacre setdecl(fiacre.types.Declarations _arg) {
    throw new UnsupportedOperationException("This Program_Fiacre has no decl");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.Program_Fiacre from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.Program_Fiacre fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.Program_Fiacre from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.Program_Fiacre fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.Program_Fiacre from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.Program_Fiacre fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.Program_Fiacre
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.Program_Fiacre fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.Program_Fiacre tmp;
    java.util.ArrayList<fiacre.types.Program_Fiacre> results = new java.util.ArrayList<fiacre.types.Program_Fiacre>();

    tmp = fiacre.types.program_fiacre.Prog.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a Program_Fiacre");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("Program_Fiacre").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.Program_Fiacre", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Program_Fiacre from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Program_Fiacre fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.Program_Fiacre from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Program_Fiacre fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.Program_Fiacre reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
