
package fiacre.types;


public abstract class PortDecl extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected PortDecl() {}



  /**
   * Returns true if the term is rooted by the symbol PortDec
   *
   * @return true if the term is rooted by the symbol PortDec
   */
  public boolean isPortDec() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot lp
   *
   * @return the subterm corresponding to the slot lp
   */
  public fiacre.types.StringList getlp() {
    throw new UnsupportedOperationException("This PortDecl has no lp");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot lp
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot lp is replaced by _arg
   */
  public PortDecl setlp(fiacre.types.StringList _arg) {
    throw new UnsupportedOperationException("This PortDecl has no lp");
  }

  /**
   * Returns the subterm corresponding to the slot chan
   *
   * @return the subterm corresponding to the slot chan
   */
  public fiacre.types.Channel getchan() {
    throw new UnsupportedOperationException("This PortDecl has no chan");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot chan
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot chan is replaced by _arg
   */
  public PortDecl setchan(fiacre.types.Channel _arg) {
    throw new UnsupportedOperationException("This PortDecl has no chan");
  }

  /**
   * Returns the subterm corresponding to the slot pal
   *
   * @return the subterm corresponding to the slot pal
   */
  public fiacre.types.PortAttrList getpal() {
    throw new UnsupportedOperationException("This PortDecl has no pal");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot pal
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot pal is replaced by _arg
   */
  public PortDecl setpal(fiacre.types.PortAttrList _arg) {
    throw new UnsupportedOperationException("This PortDecl has no pal");
  }

  /**
   * Returns the subterm corresponding to the slot type
   *
   * @return the subterm corresponding to the slot type
   */
  public fiacre.types.Type gettype() {
    throw new UnsupportedOperationException("This PortDecl has no type");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot type
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot type is replaced by _arg
   */
  public PortDecl settype(fiacre.types.Type _arg) {
    throw new UnsupportedOperationException("This PortDecl has no type");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.PortDecl from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.PortDecl fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.PortDecl from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.PortDecl fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.PortDecl from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.PortDecl fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.PortDecl
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.PortDecl fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.PortDecl tmp;
    java.util.ArrayList<fiacre.types.PortDecl> results = new java.util.ArrayList<fiacre.types.PortDecl>();

    tmp = fiacre.types.portdecl.PortDec.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a PortDecl");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("PortDecl").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.PortDecl", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.PortDecl from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PortDecl fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.PortDecl from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PortDecl fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.PortDecl reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
