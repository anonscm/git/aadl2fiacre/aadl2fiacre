
package fiacre.types.parcomposition;



public final class parCompoB extends fiacre.types.ParComposition implements tom.library.sl.Visitable  {
  
  private static String symbolName = "parCompoB";


  private parCompoB() {}
  private int hashCode;
  private static parCompoB gomProto = new parCompoB();
    private String _id;
  private fiacre.types.Exp _deb;
  private fiacre.types.Exp _fin;
  private fiacre.types.PortSet _ps;
  private fiacre.types.PortCompList _pcl;

  /**
   * Constructor that builds a term rooted by parCompoB
   *
   * @return a term rooted by parCompoB
   */

  public static parCompoB make(String _id, fiacre.types.Exp _deb, fiacre.types.Exp _fin, fiacre.types.PortSet _ps, fiacre.types.PortCompList _pcl) {

    // use the proto as a model
    gomProto.initHashCode( _id,  _deb,  _fin,  _ps,  _pcl);
    return (parCompoB) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _id
   * @param _deb
   * @param _fin
   * @param _ps
   * @param _pcl
   * @param hashCode hashCode of parCompoB
   */
  private void init(String _id, fiacre.types.Exp _deb, fiacre.types.Exp _fin, fiacre.types.PortSet _ps, fiacre.types.PortCompList _pcl, int hashCode) {
    this._id = _id.intern();
    this._deb = _deb;
    this._fin = _fin;
    this._ps = _ps;
    this._pcl = _pcl;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _id
   * @param _deb
   * @param _fin
   * @param _ps
   * @param _pcl
   */
  private void initHashCode(String _id, fiacre.types.Exp _deb, fiacre.types.Exp _fin, fiacre.types.PortSet _ps, fiacre.types.PortCompList _pcl) {
    this._id = _id.intern();
    this._deb = _deb;
    this._fin = _fin;
    this._ps = _ps;
    this._pcl = _pcl;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "parCompoB";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 5;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    parCompoB clone = new parCompoB();
    clone.init( _id,  _deb,  _fin,  _ps,  _pcl, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("parCompoB(");
    buffer.append('"');
            for (int i = 0; i < _id.length(); i++) {
              char c = _id.charAt(i);
              switch (c) {
                case '\n':
                  buffer.append('\\');
                  buffer.append('n');
                  break;
                case '\t':
                  buffer.append('\\');
                  buffer.append('t');
                  break;
                case '\b':
                  buffer.append('\\');
                  buffer.append('b');
                  break;
                case '\r':
                  buffer.append('\\');
                  buffer.append('r');
                  break;
                case '\f':
                  buffer.append('\\');
                  buffer.append('f');
                  break;
                case '\\':
                  buffer.append('\\');
                  buffer.append('\\');
                  break;
                case '\'':
                  buffer.append('\\');
                  buffer.append('\'');
                  break;
                case '\"':
                  buffer.append('\\');
                  buffer.append('\"');
                  break;
                case '!':
                case '@':
                case '#':
                case '$':
                case '%':
                case '^':
                case '&':
                case '*':
                case '(':
                case ')':
                case '-':
                case '_':
                case '+':
                case '=':
                case '|':
                case '~':
                case '{':
                case '}':
                case '[':
                case ']':
                case ';':
                case ':':
                case '<':
                case '>':
                case ',':
                case '.':
                case '?':
                case ' ':
                case '/':
                  buffer.append(c);
                  break;

                default:
                  if (java.lang.Character.isLetterOrDigit(c)) {
                    buffer.append(c);
                  } else {
                    buffer.append('\\');
                    buffer.append((char) ('0' + c / 64));
                    c = (char) (c % 64);
                    buffer.append((char) ('0' + c / 8));
                    c = (char) (c % 8);
                    buffer.append((char) ('0' + c));
                  }
              }
            }
            buffer.append('"');
buffer.append(",");
    _deb.toStringBuilder(buffer);
buffer.append(",");
    _fin.toStringBuilder(buffer);
buffer.append(",");
    _ps.toStringBuilder(buffer);
buffer.append(",");
    _pcl.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    parCompoB tco = (parCompoB) ao;
    int _idCmp = (this._id).compareTo(tco._id);
    if(_idCmp != 0) {
      return _idCmp;
    }


    int _debCmp = (this._deb).compareToLPO(tco._deb);
    if(_debCmp != 0) {
      return _debCmp;
    }

    int _finCmp = (this._fin).compareToLPO(tco._fin);
    if(_finCmp != 0) {
      return _finCmp;
    }

    int _psCmp = (this._ps).compareToLPO(tco._ps);
    if(_psCmp != 0) {
      return _psCmp;
    }

    int _pclCmp = (this._pcl).compareToLPO(tco._pcl);
    if(_pclCmp != 0) {
      return _pclCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    parCompoB tco = (parCompoB) ao;
    int _idCmp = (this._id).compareTo(tco._id);
    if(_idCmp != 0) {
      return _idCmp;
    }


    int _debCmp = (this._deb).compareTo(tco._deb);
    if(_debCmp != 0) {
      return _debCmp;
    }

    int _finCmp = (this._fin).compareTo(tco._fin);
    if(_finCmp != 0) {
      return _finCmp;
    }

    int _psCmp = (this._ps).compareTo(tco._ps);
    if(_psCmp != 0) {
      return _psCmp;
    }

    int _pclCmp = (this._pcl).compareTo(tco._pcl);
    if(_pclCmp != 0) {
      return _pclCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a parCompoB and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof parCompoB) {

      parCompoB peer = (parCompoB) obj;
      return _id==peer._id && _deb==peer._deb && _fin==peer._fin && _ps==peer._ps && _pcl==peer._pcl && true;
    }
    return false;
  }


   //ParComposition interface
  /**
   * Returns true if the term is rooted by the symbol parCompoB
   *
   * @return true, because this is rooted by parCompoB
   */
  @Override
  public boolean isparCompoB() {
    return true;
  }
  
  /**
   * Returns the attribute String
   *
   * @return the attribute String
   */
  @Override
  public String getid() {
    return _id;
  }

  /**
   * Sets and returns the attribute fiacre.types.ParComposition
   *
   * @param set_arg the argument to set
   * @return the attribute String which just has been set
   */
  @Override
  public fiacre.types.ParComposition setid(String set_arg) {
    return make(set_arg, _deb, _fin, _ps, _pcl);
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp getdeb() {
    return _deb;
  }

  /**
   * Sets and returns the attribute fiacre.types.ParComposition
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.ParComposition setdeb(fiacre.types.Exp set_arg) {
    return make(_id, set_arg, _fin, _ps, _pcl);
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp getfin() {
    return _fin;
  }

  /**
   * Sets and returns the attribute fiacre.types.ParComposition
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.ParComposition setfin(fiacre.types.Exp set_arg) {
    return make(_id, _deb, set_arg, _ps, _pcl);
  }
  
  /**
   * Returns the attribute fiacre.types.PortSet
   *
   * @return the attribute fiacre.types.PortSet
   */
  @Override
  public fiacre.types.PortSet getps() {
    return _ps;
  }

  /**
   * Sets and returns the attribute fiacre.types.ParComposition
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PortSet which just has been set
   */
  @Override
  public fiacre.types.ParComposition setps(fiacre.types.PortSet set_arg) {
    return make(_id, _deb, _fin, set_arg, _pcl);
  }
  
  /**
   * Returns the attribute fiacre.types.PortCompList
   *
   * @return the attribute fiacre.types.PortCompList
   */
  @Override
  public fiacre.types.PortCompList getpcl() {
    return _pcl;
  }

  /**
   * Sets and returns the attribute fiacre.types.ParComposition
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PortCompList which just has been set
   */
  @Override
  public fiacre.types.ParComposition setpcl(fiacre.types.PortCompList set_arg) {
    return make(_id, _deb, _fin, _ps, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {(aterm.ATerm) atermFactory.makeAppl(atermFactory.makeAFun(getid() ,0 , true)), getdeb().toATerm(), getfin().toATerm(), getps().toATerm(), getpcl().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.ParComposition from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.ParComposition fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
convertATermToString(appl.getArgument(0), atConv), fiacre.types.Exp.fromTerm(appl.getArgument(1),atConv), fiacre.types.Exp.fromTerm(appl.getArgument(2),atConv), fiacre.types.PortSet.fromTerm(appl.getArgument(3),atConv), fiacre.types.PortCompList.fromTerm(appl.getArgument(4),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 5;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return new tom.library.sl.VisitableBuiltin<String>(_id);
      case 1: return _deb;
      case 2: return _fin;
      case 3: return _ps;
      case 4: return _pcl;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make(getid(), _deb, _fin, _ps, _pcl);
      case 1: return make(getid(), (fiacre.types.Exp) v, _fin, _ps, _pcl);
      case 2: return make(getid(), _deb, (fiacre.types.Exp) v, _ps, _pcl);
      case 3: return make(getid(), _deb, _fin, (fiacre.types.PortSet) v, _pcl);
      case 4: return make(getid(), _deb, _fin, _ps, (fiacre.types.PortCompList) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 5
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 5  && childs[0] instanceof tom.library.sl.VisitableBuiltin && childs[1] instanceof fiacre.types.Exp && childs[2] instanceof fiacre.types.Exp && childs[3] instanceof fiacre.types.PortSet && childs[4] instanceof fiacre.types.PortCompList) {
      return make(((tom.library.sl.VisitableBuiltin<String>)childs[0]).getBuiltin(), (fiacre.types.Exp) childs[1], (fiacre.types.Exp) childs[2], (fiacre.types.PortSet) childs[3], (fiacre.types.PortCompList) childs[4]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  new tom.library.sl.VisitableBuiltin<String>(_id),  _deb,  _fin,  _ps,  _pcl };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-1197344530<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    b += (shared.HashFunctions.stringHashFunction(_id, 4));
    a += (_deb.hashCode() << 24);
    a += (_fin.hashCode() << 16);
    a += (_ps.hashCode() << 8);
    a += (_pcl.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
