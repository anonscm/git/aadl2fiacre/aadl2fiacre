
package fiacre.types.parcomposition;



public final class parCompo extends fiacre.types.ParComposition implements tom.library.sl.Visitable  {
  
  private static String symbolName = "parCompo";


  private parCompo() {}
  private int hashCode;
  private static parCompo gomProto = new parCompo();
    private fiacre.types.PortSet _ps;
  private fiacre.types.PortCompList _pcl;

  /**
   * Constructor that builds a term rooted by parCompo
   *
   * @return a term rooted by parCompo
   */

  public static parCompo make(fiacre.types.PortSet _ps, fiacre.types.PortCompList _pcl) {

    // use the proto as a model
    gomProto.initHashCode( _ps,  _pcl);
    return (parCompo) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _ps
   * @param _pcl
   * @param hashCode hashCode of parCompo
   */
  private void init(fiacre.types.PortSet _ps, fiacre.types.PortCompList _pcl, int hashCode) {
    this._ps = _ps;
    this._pcl = _pcl;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _ps
   * @param _pcl
   */
  private void initHashCode(fiacre.types.PortSet _ps, fiacre.types.PortCompList _pcl) {
    this._ps = _ps;
    this._pcl = _pcl;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "parCompo";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 2;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    parCompo clone = new parCompo();
    clone.init( _ps,  _pcl, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("parCompo(");
    _ps.toStringBuilder(buffer);
buffer.append(",");
    _pcl.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    parCompo tco = (parCompo) ao;
    int _psCmp = (this._ps).compareToLPO(tco._ps);
    if(_psCmp != 0) {
      return _psCmp;
    }

    int _pclCmp = (this._pcl).compareToLPO(tco._pcl);
    if(_pclCmp != 0) {
      return _pclCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    parCompo tco = (parCompo) ao;
    int _psCmp = (this._ps).compareTo(tco._ps);
    if(_psCmp != 0) {
      return _psCmp;
    }

    int _pclCmp = (this._pcl).compareTo(tco._pcl);
    if(_pclCmp != 0) {
      return _pclCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a parCompo and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof parCompo) {

      parCompo peer = (parCompo) obj;
      return _ps==peer._ps && _pcl==peer._pcl && true;
    }
    return false;
  }


   //ParComposition interface
  /**
   * Returns true if the term is rooted by the symbol parCompo
   *
   * @return true, because this is rooted by parCompo
   */
  @Override
  public boolean isparCompo() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.PortSet
   *
   * @return the attribute fiacre.types.PortSet
   */
  @Override
  public fiacre.types.PortSet getps() {
    return _ps;
  }

  /**
   * Sets and returns the attribute fiacre.types.ParComposition
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PortSet which just has been set
   */
  @Override
  public fiacre.types.ParComposition setps(fiacre.types.PortSet set_arg) {
    return make(set_arg, _pcl);
  }
  
  /**
   * Returns the attribute fiacre.types.PortCompList
   *
   * @return the attribute fiacre.types.PortCompList
   */
  @Override
  public fiacre.types.PortCompList getpcl() {
    return _pcl;
  }

  /**
   * Sets and returns the attribute fiacre.types.ParComposition
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PortCompList which just has been set
   */
  @Override
  public fiacre.types.ParComposition setpcl(fiacre.types.PortCompList set_arg) {
    return make(_ps, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getps().toATerm(), getpcl().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.ParComposition from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.ParComposition fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.PortSet.fromTerm(appl.getArgument(0),atConv), fiacre.types.PortCompList.fromTerm(appl.getArgument(1),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 2;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _ps;
      case 1: return _pcl;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.PortSet) v, _pcl);
      case 1: return make(_ps, (fiacre.types.PortCompList) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 2
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 2  && childs[0] instanceof fiacre.types.PortSet && childs[1] instanceof fiacre.types.PortCompList) {
      return make((fiacre.types.PortSet) childs[0], (fiacre.types.PortCompList) childs[1]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _ps,  _pcl };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-1268276677<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_ps.hashCode() << 8);
    a += (_pcl.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
