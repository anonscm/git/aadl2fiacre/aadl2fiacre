
package fiacre.types.property;



public final class PropOxOxOI extends fiacre.types.Property implements tom.library.sl.Visitable  {
  
  private static String symbolName = "PropOxOxOI";


  private PropOxOxOI() {}
  private int hashCode;
  private static PropOxOxOI gomProto = new PropOxOxOI();
    private String _op1;
  private fiacre.types.Observable _obs1;
  private String _op2;
  private fiacre.types.Observable _obs2;
  private String _op3;
  private fiacre.types.Bound _min;
  private fiacre.types.Bound _max;

  /**
   * Constructor that builds a term rooted by PropOxOxOI
   *
   * @return a term rooted by PropOxOxOI
   */

  public static PropOxOxOI make(String _op1, fiacre.types.Observable _obs1, String _op2, fiacre.types.Observable _obs2, String _op3, fiacre.types.Bound _min, fiacre.types.Bound _max) {

    // use the proto as a model
    gomProto.initHashCode( _op1,  _obs1,  _op2,  _obs2,  _op3,  _min,  _max);
    return (PropOxOxOI) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _op1
   * @param _obs1
   * @param _op2
   * @param _obs2
   * @param _op3
   * @param _min
   * @param _max
   * @param hashCode hashCode of PropOxOxOI
   */
  private void init(String _op1, fiacre.types.Observable _obs1, String _op2, fiacre.types.Observable _obs2, String _op3, fiacre.types.Bound _min, fiacre.types.Bound _max, int hashCode) {
    this._op1 = _op1.intern();
    this._obs1 = _obs1;
    this._op2 = _op2.intern();
    this._obs2 = _obs2;
    this._op3 = _op3.intern();
    this._min = _min;
    this._max = _max;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _op1
   * @param _obs1
   * @param _op2
   * @param _obs2
   * @param _op3
   * @param _min
   * @param _max
   */
  private void initHashCode(String _op1, fiacre.types.Observable _obs1, String _op2, fiacre.types.Observable _obs2, String _op3, fiacre.types.Bound _min, fiacre.types.Bound _max) {
    this._op1 = _op1.intern();
    this._obs1 = _obs1;
    this._op2 = _op2.intern();
    this._obs2 = _obs2;
    this._op3 = _op3.intern();
    this._min = _min;
    this._max = _max;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "PropOxOxOI";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 7;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    PropOxOxOI clone = new PropOxOxOI();
    clone.init( _op1,  _obs1,  _op2,  _obs2,  _op3,  _min,  _max, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("PropOxOxOI(");
    buffer.append('"');
            for (int i = 0; i < _op1.length(); i++) {
              char c = _op1.charAt(i);
              switch (c) {
                case '\n':
                  buffer.append('\\');
                  buffer.append('n');
                  break;
                case '\t':
                  buffer.append('\\');
                  buffer.append('t');
                  break;
                case '\b':
                  buffer.append('\\');
                  buffer.append('b');
                  break;
                case '\r':
                  buffer.append('\\');
                  buffer.append('r');
                  break;
                case '\f':
                  buffer.append('\\');
                  buffer.append('f');
                  break;
                case '\\':
                  buffer.append('\\');
                  buffer.append('\\');
                  break;
                case '\'':
                  buffer.append('\\');
                  buffer.append('\'');
                  break;
                case '\"':
                  buffer.append('\\');
                  buffer.append('\"');
                  break;
                case '!':
                case '@':
                case '#':
                case '$':
                case '%':
                case '^':
                case '&':
                case '*':
                case '(':
                case ')':
                case '-':
                case '_':
                case '+':
                case '=':
                case '|':
                case '~':
                case '{':
                case '}':
                case '[':
                case ']':
                case ';':
                case ':':
                case '<':
                case '>':
                case ',':
                case '.':
                case '?':
                case ' ':
                case '/':
                  buffer.append(c);
                  break;

                default:
                  if (java.lang.Character.isLetterOrDigit(c)) {
                    buffer.append(c);
                  } else {
                    buffer.append('\\');
                    buffer.append((char) ('0' + c / 64));
                    c = (char) (c % 64);
                    buffer.append((char) ('0' + c / 8));
                    c = (char) (c % 8);
                    buffer.append((char) ('0' + c));
                  }
              }
            }
            buffer.append('"');
buffer.append(",");
    _obs1.toStringBuilder(buffer);
buffer.append(",");
    buffer.append('"');
            for (int i = 0; i < _op2.length(); i++) {
              char c = _op2.charAt(i);
              switch (c) {
                case '\n':
                  buffer.append('\\');
                  buffer.append('n');
                  break;
                case '\t':
                  buffer.append('\\');
                  buffer.append('t');
                  break;
                case '\b':
                  buffer.append('\\');
                  buffer.append('b');
                  break;
                case '\r':
                  buffer.append('\\');
                  buffer.append('r');
                  break;
                case '\f':
                  buffer.append('\\');
                  buffer.append('f');
                  break;
                case '\\':
                  buffer.append('\\');
                  buffer.append('\\');
                  break;
                case '\'':
                  buffer.append('\\');
                  buffer.append('\'');
                  break;
                case '\"':
                  buffer.append('\\');
                  buffer.append('\"');
                  break;
                case '!':
                case '@':
                case '#':
                case '$':
                case '%':
                case '^':
                case '&':
                case '*':
                case '(':
                case ')':
                case '-':
                case '_':
                case '+':
                case '=':
                case '|':
                case '~':
                case '{':
                case '}':
                case '[':
                case ']':
                case ';':
                case ':':
                case '<':
                case '>':
                case ',':
                case '.':
                case '?':
                case ' ':
                case '/':
                  buffer.append(c);
                  break;

                default:
                  if (java.lang.Character.isLetterOrDigit(c)) {
                    buffer.append(c);
                  } else {
                    buffer.append('\\');
                    buffer.append((char) ('0' + c / 64));
                    c = (char) (c % 64);
                    buffer.append((char) ('0' + c / 8));
                    c = (char) (c % 8);
                    buffer.append((char) ('0' + c));
                  }
              }
            }
            buffer.append('"');
buffer.append(",");
    _obs2.toStringBuilder(buffer);
buffer.append(",");
    buffer.append('"');
            for (int i = 0; i < _op3.length(); i++) {
              char c = _op3.charAt(i);
              switch (c) {
                case '\n':
                  buffer.append('\\');
                  buffer.append('n');
                  break;
                case '\t':
                  buffer.append('\\');
                  buffer.append('t');
                  break;
                case '\b':
                  buffer.append('\\');
                  buffer.append('b');
                  break;
                case '\r':
                  buffer.append('\\');
                  buffer.append('r');
                  break;
                case '\f':
                  buffer.append('\\');
                  buffer.append('f');
                  break;
                case '\\':
                  buffer.append('\\');
                  buffer.append('\\');
                  break;
                case '\'':
                  buffer.append('\\');
                  buffer.append('\'');
                  break;
                case '\"':
                  buffer.append('\\');
                  buffer.append('\"');
                  break;
                case '!':
                case '@':
                case '#':
                case '$':
                case '%':
                case '^':
                case '&':
                case '*':
                case '(':
                case ')':
                case '-':
                case '_':
                case '+':
                case '=':
                case '|':
                case '~':
                case '{':
                case '}':
                case '[':
                case ']':
                case ';':
                case ':':
                case '<':
                case '>':
                case ',':
                case '.':
                case '?':
                case ' ':
                case '/':
                  buffer.append(c);
                  break;

                default:
                  if (java.lang.Character.isLetterOrDigit(c)) {
                    buffer.append(c);
                  } else {
                    buffer.append('\\');
                    buffer.append((char) ('0' + c / 64));
                    c = (char) (c % 64);
                    buffer.append((char) ('0' + c / 8));
                    c = (char) (c % 8);
                    buffer.append((char) ('0' + c));
                  }
              }
            }
            buffer.append('"');
buffer.append(",");
    _min.toStringBuilder(buffer);
buffer.append(",");
    _max.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    PropOxOxOI tco = (PropOxOxOI) ao;
    int _op1Cmp = (this._op1).compareTo(tco._op1);
    if(_op1Cmp != 0) {
      return _op1Cmp;
    }


    int _obs1Cmp = (this._obs1).compareToLPO(tco._obs1);
    if(_obs1Cmp != 0) {
      return _obs1Cmp;
    }

    int _op2Cmp = (this._op2).compareTo(tco._op2);
    if(_op2Cmp != 0) {
      return _op2Cmp;
    }


    int _obs2Cmp = (this._obs2).compareToLPO(tco._obs2);
    if(_obs2Cmp != 0) {
      return _obs2Cmp;
    }

    int _op3Cmp = (this._op3).compareTo(tco._op3);
    if(_op3Cmp != 0) {
      return _op3Cmp;
    }


    int _minCmp = (this._min).compareToLPO(tco._min);
    if(_minCmp != 0) {
      return _minCmp;
    }

    int _maxCmp = (this._max).compareToLPO(tco._max);
    if(_maxCmp != 0) {
      return _maxCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    PropOxOxOI tco = (PropOxOxOI) ao;
    int _op1Cmp = (this._op1).compareTo(tco._op1);
    if(_op1Cmp != 0) {
      return _op1Cmp;
    }


    int _obs1Cmp = (this._obs1).compareTo(tco._obs1);
    if(_obs1Cmp != 0) {
      return _obs1Cmp;
    }

    int _op2Cmp = (this._op2).compareTo(tco._op2);
    if(_op2Cmp != 0) {
      return _op2Cmp;
    }


    int _obs2Cmp = (this._obs2).compareTo(tco._obs2);
    if(_obs2Cmp != 0) {
      return _obs2Cmp;
    }

    int _op3Cmp = (this._op3).compareTo(tco._op3);
    if(_op3Cmp != 0) {
      return _op3Cmp;
    }


    int _minCmp = (this._min).compareTo(tco._min);
    if(_minCmp != 0) {
      return _minCmp;
    }

    int _maxCmp = (this._max).compareTo(tco._max);
    if(_maxCmp != 0) {
      return _maxCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a PropOxOxOI and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof PropOxOxOI) {

      PropOxOxOI peer = (PropOxOxOI) obj;
      return _op1==peer._op1 && _obs1==peer._obs1 && _op2==peer._op2 && _obs2==peer._obs2 && _op3==peer._op3 && _min==peer._min && _max==peer._max && true;
    }
    return false;
  }


   //Property interface
  /**
   * Returns true if the term is rooted by the symbol PropOxOxOI
   *
   * @return true, because this is rooted by PropOxOxOI
   */
  @Override
  public boolean isPropOxOxOI() {
    return true;
  }
  
  /**
   * Returns the attribute String
   *
   * @return the attribute String
   */
  @Override
  public String getop1() {
    return _op1;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute String which just has been set
   */
  @Override
  public fiacre.types.Property setop1(String set_arg) {
    return make(set_arg, _obs1, _op2, _obs2, _op3, _min, _max);
  }
  
  /**
   * Returns the attribute fiacre.types.Observable
   *
   * @return the attribute fiacre.types.Observable
   */
  @Override
  public fiacre.types.Observable getobs1() {
    return _obs1;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Observable which just has been set
   */
  @Override
  public fiacre.types.Property setobs1(fiacre.types.Observable set_arg) {
    return make(_op1, set_arg, _op2, _obs2, _op3, _min, _max);
  }
  
  /**
   * Returns the attribute String
   *
   * @return the attribute String
   */
  @Override
  public String getop2() {
    return _op2;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute String which just has been set
   */
  @Override
  public fiacre.types.Property setop2(String set_arg) {
    return make(_op1, _obs1, set_arg, _obs2, _op3, _min, _max);
  }
  
  /**
   * Returns the attribute fiacre.types.Observable
   *
   * @return the attribute fiacre.types.Observable
   */
  @Override
  public fiacre.types.Observable getobs2() {
    return _obs2;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Observable which just has been set
   */
  @Override
  public fiacre.types.Property setobs2(fiacre.types.Observable set_arg) {
    return make(_op1, _obs1, _op2, set_arg, _op3, _min, _max);
  }
  
  /**
   * Returns the attribute String
   *
   * @return the attribute String
   */
  @Override
  public String getop3() {
    return _op3;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute String which just has been set
   */
  @Override
  public fiacre.types.Property setop3(String set_arg) {
    return make(_op1, _obs1, _op2, _obs2, set_arg, _min, _max);
  }
  
  /**
   * Returns the attribute fiacre.types.Bound
   *
   * @return the attribute fiacre.types.Bound
   */
  @Override
  public fiacre.types.Bound getmin() {
    return _min;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Bound which just has been set
   */
  @Override
  public fiacre.types.Property setmin(fiacre.types.Bound set_arg) {
    return make(_op1, _obs1, _op2, _obs2, _op3, set_arg, _max);
  }
  
  /**
   * Returns the attribute fiacre.types.Bound
   *
   * @return the attribute fiacre.types.Bound
   */
  @Override
  public fiacre.types.Bound getmax() {
    return _max;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Bound which just has been set
   */
  @Override
  public fiacre.types.Property setmax(fiacre.types.Bound set_arg) {
    return make(_op1, _obs1, _op2, _obs2, _op3, _min, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {(aterm.ATerm) atermFactory.makeAppl(atermFactory.makeAFun(getop1() ,0 , true)), getobs1().toATerm(), (aterm.ATerm) atermFactory.makeAppl(atermFactory.makeAFun(getop2() ,0 , true)), getobs2().toATerm(), (aterm.ATerm) atermFactory.makeAppl(atermFactory.makeAFun(getop3() ,0 , true)), getmin().toATerm(), getmax().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Property from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Property fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
convertATermToString(appl.getArgument(0), atConv), fiacre.types.Observable.fromTerm(appl.getArgument(1),atConv), convertATermToString(appl.getArgument(2), atConv), fiacre.types.Observable.fromTerm(appl.getArgument(3),atConv), convertATermToString(appl.getArgument(4), atConv), fiacre.types.Bound.fromTerm(appl.getArgument(5),atConv), fiacre.types.Bound.fromTerm(appl.getArgument(6),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 7;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return new tom.library.sl.VisitableBuiltin<String>(_op1);
      case 1: return _obs1;
      case 2: return new tom.library.sl.VisitableBuiltin<String>(_op2);
      case 3: return _obs2;
      case 4: return new tom.library.sl.VisitableBuiltin<String>(_op3);
      case 5: return _min;
      case 6: return _max;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make(getop1(), _obs1, getop2(), _obs2, getop3(), _min, _max);
      case 1: return make(getop1(), (fiacre.types.Observable) v, getop2(), _obs2, getop3(), _min, _max);
      case 2: return make(getop1(), _obs1, getop2(), _obs2, getop3(), _min, _max);
      case 3: return make(getop1(), _obs1, getop2(), (fiacre.types.Observable) v, getop3(), _min, _max);
      case 4: return make(getop1(), _obs1, getop2(), _obs2, getop3(), _min, _max);
      case 5: return make(getop1(), _obs1, getop2(), _obs2, getop3(), (fiacre.types.Bound) v, _max);
      case 6: return make(getop1(), _obs1, getop2(), _obs2, getop3(), _min, (fiacre.types.Bound) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 7
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 7  && childs[0] instanceof tom.library.sl.VisitableBuiltin && childs[1] instanceof fiacre.types.Observable && childs[2] instanceof tom.library.sl.VisitableBuiltin && childs[3] instanceof fiacre.types.Observable && childs[4] instanceof tom.library.sl.VisitableBuiltin && childs[5] instanceof fiacre.types.Bound && childs[6] instanceof fiacre.types.Bound) {
      return make(((tom.library.sl.VisitableBuiltin<String>)childs[0]).getBuiltin(), (fiacre.types.Observable) childs[1], ((tom.library.sl.VisitableBuiltin<String>)childs[2]).getBuiltin(), (fiacre.types.Observable) childs[3], ((tom.library.sl.VisitableBuiltin<String>)childs[4]).getBuiltin(), (fiacre.types.Bound) childs[5], (fiacre.types.Bound) childs[6]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  new tom.library.sl.VisitableBuiltin<String>(_op1),  _obs1,  new tom.library.sl.VisitableBuiltin<String>(_op2),  _obs2,  new tom.library.sl.VisitableBuiltin<String>(_op3),  _min,  _max };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (1425759909<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    b += (shared.HashFunctions.stringHashFunction(_op1, 6) << 16);
    b += (_obs1.hashCode() << 8);
    b += (shared.HashFunctions.stringHashFunction(_op2, 4));
    a += (_obs2.hashCode() << 24);
    a += (shared.HashFunctions.stringHashFunction(_op3, 2) << 16);
    a += (_min.hashCode() << 8);
    a += (_max.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
