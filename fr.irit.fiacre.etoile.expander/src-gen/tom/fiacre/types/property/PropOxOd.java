
package fiacre.types.property;



public final class PropOxOd extends fiacre.types.Property implements tom.library.sl.Visitable  {
  
  private static String symbolName = "PropOxOd";


  private PropOxOd() {}
  private int hashCode;
  private static PropOxOd gomProto = new PropOxOd();
    private String _op1;
  private fiacre.types.Observable _obs1;
  private String _op2;
  private fiacre.types.Duration _dur;

  /**
   * Constructor that builds a term rooted by PropOxOd
   *
   * @return a term rooted by PropOxOd
   */

  public static PropOxOd make(String _op1, fiacre.types.Observable _obs1, String _op2, fiacre.types.Duration _dur) {

    // use the proto as a model
    gomProto.initHashCode( _op1,  _obs1,  _op2,  _dur);
    return (PropOxOd) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _op1
   * @param _obs1
   * @param _op2
   * @param _dur
   * @param hashCode hashCode of PropOxOd
   */
  private void init(String _op1, fiacre.types.Observable _obs1, String _op2, fiacre.types.Duration _dur, int hashCode) {
    this._op1 = _op1.intern();
    this._obs1 = _obs1;
    this._op2 = _op2.intern();
    this._dur = _dur;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _op1
   * @param _obs1
   * @param _op2
   * @param _dur
   */
  private void initHashCode(String _op1, fiacre.types.Observable _obs1, String _op2, fiacre.types.Duration _dur) {
    this._op1 = _op1.intern();
    this._obs1 = _obs1;
    this._op2 = _op2.intern();
    this._dur = _dur;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "PropOxOd";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 4;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    PropOxOd clone = new PropOxOd();
    clone.init( _op1,  _obs1,  _op2,  _dur, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("PropOxOd(");
    buffer.append('"');
            for (int i = 0; i < _op1.length(); i++) {
              char c = _op1.charAt(i);
              switch (c) {
                case '\n':
                  buffer.append('\\');
                  buffer.append('n');
                  break;
                case '\t':
                  buffer.append('\\');
                  buffer.append('t');
                  break;
                case '\b':
                  buffer.append('\\');
                  buffer.append('b');
                  break;
                case '\r':
                  buffer.append('\\');
                  buffer.append('r');
                  break;
                case '\f':
                  buffer.append('\\');
                  buffer.append('f');
                  break;
                case '\\':
                  buffer.append('\\');
                  buffer.append('\\');
                  break;
                case '\'':
                  buffer.append('\\');
                  buffer.append('\'');
                  break;
                case '\"':
                  buffer.append('\\');
                  buffer.append('\"');
                  break;
                case '!':
                case '@':
                case '#':
                case '$':
                case '%':
                case '^':
                case '&':
                case '*':
                case '(':
                case ')':
                case '-':
                case '_':
                case '+':
                case '=':
                case '|':
                case '~':
                case '{':
                case '}':
                case '[':
                case ']':
                case ';':
                case ':':
                case '<':
                case '>':
                case ',':
                case '.':
                case '?':
                case ' ':
                case '/':
                  buffer.append(c);
                  break;

                default:
                  if (java.lang.Character.isLetterOrDigit(c)) {
                    buffer.append(c);
                  } else {
                    buffer.append('\\');
                    buffer.append((char) ('0' + c / 64));
                    c = (char) (c % 64);
                    buffer.append((char) ('0' + c / 8));
                    c = (char) (c % 8);
                    buffer.append((char) ('0' + c));
                  }
              }
            }
            buffer.append('"');
buffer.append(",");
    _obs1.toStringBuilder(buffer);
buffer.append(",");
    buffer.append('"');
            for (int i = 0; i < _op2.length(); i++) {
              char c = _op2.charAt(i);
              switch (c) {
                case '\n':
                  buffer.append('\\');
                  buffer.append('n');
                  break;
                case '\t':
                  buffer.append('\\');
                  buffer.append('t');
                  break;
                case '\b':
                  buffer.append('\\');
                  buffer.append('b');
                  break;
                case '\r':
                  buffer.append('\\');
                  buffer.append('r');
                  break;
                case '\f':
                  buffer.append('\\');
                  buffer.append('f');
                  break;
                case '\\':
                  buffer.append('\\');
                  buffer.append('\\');
                  break;
                case '\'':
                  buffer.append('\\');
                  buffer.append('\'');
                  break;
                case '\"':
                  buffer.append('\\');
                  buffer.append('\"');
                  break;
                case '!':
                case '@':
                case '#':
                case '$':
                case '%':
                case '^':
                case '&':
                case '*':
                case '(':
                case ')':
                case '-':
                case '_':
                case '+':
                case '=':
                case '|':
                case '~':
                case '{':
                case '}':
                case '[':
                case ']':
                case ';':
                case ':':
                case '<':
                case '>':
                case ',':
                case '.':
                case '?':
                case ' ':
                case '/':
                  buffer.append(c);
                  break;

                default:
                  if (java.lang.Character.isLetterOrDigit(c)) {
                    buffer.append(c);
                  } else {
                    buffer.append('\\');
                    buffer.append((char) ('0' + c / 64));
                    c = (char) (c % 64);
                    buffer.append((char) ('0' + c / 8));
                    c = (char) (c % 8);
                    buffer.append((char) ('0' + c));
                  }
              }
            }
            buffer.append('"');
buffer.append(",");
    _dur.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    PropOxOd tco = (PropOxOd) ao;
    int _op1Cmp = (this._op1).compareTo(tco._op1);
    if(_op1Cmp != 0) {
      return _op1Cmp;
    }


    int _obs1Cmp = (this._obs1).compareToLPO(tco._obs1);
    if(_obs1Cmp != 0) {
      return _obs1Cmp;
    }

    int _op2Cmp = (this._op2).compareTo(tco._op2);
    if(_op2Cmp != 0) {
      return _op2Cmp;
    }


    int _durCmp = (this._dur).compareToLPO(tco._dur);
    if(_durCmp != 0) {
      return _durCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    PropOxOd tco = (PropOxOd) ao;
    int _op1Cmp = (this._op1).compareTo(tco._op1);
    if(_op1Cmp != 0) {
      return _op1Cmp;
    }


    int _obs1Cmp = (this._obs1).compareTo(tco._obs1);
    if(_obs1Cmp != 0) {
      return _obs1Cmp;
    }

    int _op2Cmp = (this._op2).compareTo(tco._op2);
    if(_op2Cmp != 0) {
      return _op2Cmp;
    }


    int _durCmp = (this._dur).compareTo(tco._dur);
    if(_durCmp != 0) {
      return _durCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a PropOxOd and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof PropOxOd) {

      PropOxOd peer = (PropOxOd) obj;
      return _op1==peer._op1 && _obs1==peer._obs1 && _op2==peer._op2 && _dur==peer._dur && true;
    }
    return false;
  }


   //Property interface
  /**
   * Returns true if the term is rooted by the symbol PropOxOd
   *
   * @return true, because this is rooted by PropOxOd
   */
  @Override
  public boolean isPropOxOd() {
    return true;
  }
  
  /**
   * Returns the attribute String
   *
   * @return the attribute String
   */
  @Override
  public String getop1() {
    return _op1;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute String which just has been set
   */
  @Override
  public fiacre.types.Property setop1(String set_arg) {
    return make(set_arg, _obs1, _op2, _dur);
  }
  
  /**
   * Returns the attribute fiacre.types.Observable
   *
   * @return the attribute fiacre.types.Observable
   */
  @Override
  public fiacre.types.Observable getobs1() {
    return _obs1;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Observable which just has been set
   */
  @Override
  public fiacre.types.Property setobs1(fiacre.types.Observable set_arg) {
    return make(_op1, set_arg, _op2, _dur);
  }
  
  /**
   * Returns the attribute String
   *
   * @return the attribute String
   */
  @Override
  public String getop2() {
    return _op2;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute String which just has been set
   */
  @Override
  public fiacre.types.Property setop2(String set_arg) {
    return make(_op1, _obs1, set_arg, _dur);
  }
  
  /**
   * Returns the attribute fiacre.types.Duration
   *
   * @return the attribute fiacre.types.Duration
   */
  @Override
  public fiacre.types.Duration getdur() {
    return _dur;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Duration which just has been set
   */
  @Override
  public fiacre.types.Property setdur(fiacre.types.Duration set_arg) {
    return make(_op1, _obs1, _op2, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {(aterm.ATerm) atermFactory.makeAppl(atermFactory.makeAFun(getop1() ,0 , true)), getobs1().toATerm(), (aterm.ATerm) atermFactory.makeAppl(atermFactory.makeAFun(getop2() ,0 , true)), getdur().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Property from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Property fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
convertATermToString(appl.getArgument(0), atConv), fiacre.types.Observable.fromTerm(appl.getArgument(1),atConv), convertATermToString(appl.getArgument(2), atConv), fiacre.types.Duration.fromTerm(appl.getArgument(3),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 4;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return new tom.library.sl.VisitableBuiltin<String>(_op1);
      case 1: return _obs1;
      case 2: return new tom.library.sl.VisitableBuiltin<String>(_op2);
      case 3: return _dur;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make(getop1(), _obs1, getop2(), _dur);
      case 1: return make(getop1(), (fiacre.types.Observable) v, getop2(), _dur);
      case 2: return make(getop1(), _obs1, getop2(), _dur);
      case 3: return make(getop1(), _obs1, getop2(), (fiacre.types.Duration) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 4
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 4  && childs[0] instanceof tom.library.sl.VisitableBuiltin && childs[1] instanceof fiacre.types.Observable && childs[2] instanceof tom.library.sl.VisitableBuiltin && childs[3] instanceof fiacre.types.Duration) {
      return make(((tom.library.sl.VisitableBuiltin<String>)childs[0]).getBuiltin(), (fiacre.types.Observable) childs[1], ((tom.library.sl.VisitableBuiltin<String>)childs[2]).getBuiltin(), (fiacre.types.Duration) childs[3]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  new tom.library.sl.VisitableBuiltin<String>(_op1),  _obs1,  new tom.library.sl.VisitableBuiltin<String>(_op2),  _dur };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (1451288494<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (shared.HashFunctions.stringHashFunction(_op1, 3) << 24);
    a += (_obs1.hashCode() << 16);
    a += (shared.HashFunctions.stringHashFunction(_op2, 1) << 8);
    a += (_dur.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
