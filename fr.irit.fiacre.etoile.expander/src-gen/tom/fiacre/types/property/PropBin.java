
package fiacre.types.property;



public final class PropBin extends fiacre.types.Property implements tom.library.sl.Visitable  {
  
  private static String symbolName = "PropBin";


  private PropBin() {}
  private int hashCode;
  private static PropBin gomProto = new PropBin();
    private fiacre.types.Property _arg1;
  private String _op;
  private fiacre.types.Property _arg2;

  /**
   * Constructor that builds a term rooted by PropBin
   *
   * @return a term rooted by PropBin
   */

  public static PropBin make(fiacre.types.Property _arg1, String _op, fiacre.types.Property _arg2) {

    // use the proto as a model
    gomProto.initHashCode( _arg1,  _op,  _arg2);
    return (PropBin) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _arg1
   * @param _op
   * @param _arg2
   * @param hashCode hashCode of PropBin
   */
  private void init(fiacre.types.Property _arg1, String _op, fiacre.types.Property _arg2, int hashCode) {
    this._arg1 = _arg1;
    this._op = _op.intern();
    this._arg2 = _arg2;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _arg1
   * @param _op
   * @param _arg2
   */
  private void initHashCode(fiacre.types.Property _arg1, String _op, fiacre.types.Property _arg2) {
    this._arg1 = _arg1;
    this._op = _op.intern();
    this._arg2 = _arg2;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "PropBin";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 3;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    PropBin clone = new PropBin();
    clone.init( _arg1,  _op,  _arg2, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("PropBin(");
    _arg1.toStringBuilder(buffer);
buffer.append(",");
    buffer.append('"');
            for (int i = 0; i < _op.length(); i++) {
              char c = _op.charAt(i);
              switch (c) {
                case '\n':
                  buffer.append('\\');
                  buffer.append('n');
                  break;
                case '\t':
                  buffer.append('\\');
                  buffer.append('t');
                  break;
                case '\b':
                  buffer.append('\\');
                  buffer.append('b');
                  break;
                case '\r':
                  buffer.append('\\');
                  buffer.append('r');
                  break;
                case '\f':
                  buffer.append('\\');
                  buffer.append('f');
                  break;
                case '\\':
                  buffer.append('\\');
                  buffer.append('\\');
                  break;
                case '\'':
                  buffer.append('\\');
                  buffer.append('\'');
                  break;
                case '\"':
                  buffer.append('\\');
                  buffer.append('\"');
                  break;
                case '!':
                case '@':
                case '#':
                case '$':
                case '%':
                case '^':
                case '&':
                case '*':
                case '(':
                case ')':
                case '-':
                case '_':
                case '+':
                case '=':
                case '|':
                case '~':
                case '{':
                case '}':
                case '[':
                case ']':
                case ';':
                case ':':
                case '<':
                case '>':
                case ',':
                case '.':
                case '?':
                case ' ':
                case '/':
                  buffer.append(c);
                  break;

                default:
                  if (java.lang.Character.isLetterOrDigit(c)) {
                    buffer.append(c);
                  } else {
                    buffer.append('\\');
                    buffer.append((char) ('0' + c / 64));
                    c = (char) (c % 64);
                    buffer.append((char) ('0' + c / 8));
                    c = (char) (c % 8);
                    buffer.append((char) ('0' + c));
                  }
              }
            }
            buffer.append('"');
buffer.append(",");
    _arg2.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    PropBin tco = (PropBin) ao;
    int _arg1Cmp = (this._arg1).compareToLPO(tco._arg1);
    if(_arg1Cmp != 0) {
      return _arg1Cmp;
    }

    int _opCmp = (this._op).compareTo(tco._op);
    if(_opCmp != 0) {
      return _opCmp;
    }


    int _arg2Cmp = (this._arg2).compareToLPO(tco._arg2);
    if(_arg2Cmp != 0) {
      return _arg2Cmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    PropBin tco = (PropBin) ao;
    int _arg1Cmp = (this._arg1).compareTo(tco._arg1);
    if(_arg1Cmp != 0) {
      return _arg1Cmp;
    }

    int _opCmp = (this._op).compareTo(tco._op);
    if(_opCmp != 0) {
      return _opCmp;
    }


    int _arg2Cmp = (this._arg2).compareTo(tco._arg2);
    if(_arg2Cmp != 0) {
      return _arg2Cmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a PropBin and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof PropBin) {

      PropBin peer = (PropBin) obj;
      return _arg1==peer._arg1 && _op==peer._op && _arg2==peer._arg2 && true;
    }
    return false;
  }


   //Property interface
  /**
   * Returns true if the term is rooted by the symbol PropBin
   *
   * @return true, because this is rooted by PropBin
   */
  @Override
  public boolean isPropBin() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.Property
   *
   * @return the attribute fiacre.types.Property
   */
  @Override
  public fiacre.types.Property getarg1() {
    return _arg1;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Property which just has been set
   */
  @Override
  public fiacre.types.Property setarg1(fiacre.types.Property set_arg) {
    return make(set_arg, _op, _arg2);
  }
  
  /**
   * Returns the attribute String
   *
   * @return the attribute String
   */
  @Override
  public String getop() {
    return _op;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute String which just has been set
   */
  @Override
  public fiacre.types.Property setop(String set_arg) {
    return make(_arg1, set_arg, _arg2);
  }
  
  /**
   * Returns the attribute fiacre.types.Property
   *
   * @return the attribute fiacre.types.Property
   */
  @Override
  public fiacre.types.Property getarg2() {
    return _arg2;
  }

  /**
   * Sets and returns the attribute fiacre.types.Property
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Property which just has been set
   */
  @Override
  public fiacre.types.Property setarg2(fiacre.types.Property set_arg) {
    return make(_arg1, _op, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getarg1().toATerm(), (aterm.ATerm) atermFactory.makeAppl(atermFactory.makeAFun(getop() ,0 , true)), getarg2().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Property from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Property fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.Property.fromTerm(appl.getArgument(0),atConv), convertATermToString(appl.getArgument(1), atConv), fiacre.types.Property.fromTerm(appl.getArgument(2),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 3;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _arg1;
      case 1: return new tom.library.sl.VisitableBuiltin<String>(_op);
      case 2: return _arg2;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.Property) v, getop(), _arg2);
      case 1: return make(_arg1, getop(), _arg2);
      case 2: return make(_arg1, getop(), (fiacre.types.Property) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 3
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 3  && childs[0] instanceof fiacre.types.Property && childs[1] instanceof tom.library.sl.VisitableBuiltin && childs[2] instanceof fiacre.types.Property) {
      return make((fiacre.types.Property) childs[0], ((tom.library.sl.VisitableBuiltin<String>)childs[1]).getBuiltin(), (fiacre.types.Property) childs[2]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _arg1,  new tom.library.sl.VisitableBuiltin<String>(_op),  _arg2 };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-921598453<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_arg1.hashCode() << 16);
    a += (shared.HashFunctions.stringHashFunction(_op, 1) << 8);
    a += (_arg2.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
