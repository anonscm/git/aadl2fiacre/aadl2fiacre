
package fiacre.types.portcomplist;



public abstract class PCList extends fiacre.types.PortCompList implements java.util.Collection<fiacre.types.PortComposition>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.portcomplist.ConsPCList) {
      fiacre.types.PortCompList tl = this.getTailPCList();
      if (tl instanceof PCList) {
        return 1+((PCList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.PortCompList fromArray(fiacre.types.PortComposition[] array) {
    fiacre.types.PortCompList res = fiacre.types.portcomplist.EmptyPCList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.portcomplist.ConsPCList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.PortCompList reverse() {
    if(this instanceof fiacre.types.portcomplist.ConsPCList) {
      fiacre.types.PortCompList cur = this;
      fiacre.types.PortCompList rev = fiacre.types.portcomplist.EmptyPCList.make();
      while(cur instanceof fiacre.types.portcomplist.ConsPCList) {
        rev = fiacre.types.portcomplist.ConsPCList.make(cur.getHeadPCList(),rev);
        cur = cur.getTailPCList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.PortCompList append(fiacre.types.PortComposition element) {
    if(this instanceof fiacre.types.portcomplist.ConsPCList) {
      fiacre.types.PortCompList tl = this.getTailPCList();
      if (tl instanceof PCList) {
        return fiacre.types.portcomplist.ConsPCList.make(this.getHeadPCList(),((PCList)tl).append(element));
      } else {

        return fiacre.types.portcomplist.ConsPCList.make(this.getHeadPCList(),fiacre.types.portcomplist.ConsPCList.make(element,tl));

      }
    } else {
      return fiacre.types.portcomplist.ConsPCList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("PCList(");
    if(this instanceof fiacre.types.portcomplist.ConsPCList) {
      fiacre.types.PortCompList cur = this;
      while(cur instanceof fiacre.types.portcomplist.ConsPCList) {
        fiacre.types.PortComposition elem = cur.getHeadPCList();
        cur = cur.getTailPCList();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.portcomplist.ConsPCList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.portcomplist.EmptyPCList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.portcomplist.ConsPCList) {
      fiacre.types.PortCompList tail = this.getTailPCList();
      res = atermFactory.makeList(getHeadPCList().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.PortCompList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PortCompList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("PCList".equals(appl.getName())) {
        fiacre.types.PortCompList res = fiacre.types.portcomplist.EmptyPCList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.PortComposition elem = fiacre.types.PortComposition.fromTerm(array[i],atConv);
          res = fiacre.types.portcomplist.ConsPCList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.PortCompList res = fiacre.types.portcomplist.EmptyPCList.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.PortComposition elem = fiacre.types.PortComposition.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.portcomplist.ConsPCList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.PortCompList contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.PortCompList contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.PortCompList cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.portcomplist.ConsPCList) {
      while(cur instanceof fiacre.types.portcomplist.ConsPCList) {
        if( o.equals(cur.getHeadPCList()) ) {
          return true;
        }
        cur = cur.getTailPCList();
      }
      if(!(cur instanceof fiacre.types.portcomplist.EmptyPCList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyPCList() ; }

  public java.util.Iterator<fiacre.types.PortComposition> iterator() {
    return new java.util.Iterator<fiacre.types.PortComposition>() {
      fiacre.types.PortCompList list = PCList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyPCList();
      }

      public fiacre.types.PortComposition next() {
        if(list.isEmptyPCList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsPCList()) {
          fiacre.types.PortComposition head = list.getHeadPCList();
          list = list.getTailPCList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.PortComposition)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.PortComposition o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.PortComposition> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.portcomplist.ConsPCList) {
      fiacre.types.PortCompList cur = this;
      while(cur instanceof fiacre.types.portcomplist.ConsPCList) {
        fiacre.types.PortComposition elem = cur.getHeadPCList();
        array[i] = elem;
        cur = cur.getTailPCList();
        i++;
      }
      if(!(cur instanceof fiacre.types.portcomplist.EmptyPCList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.portcomplist.ConsPCList) {
      fiacre.types.PortCompList cur = this;
      while(cur instanceof fiacre.types.portcomplist.ConsPCList) {
        fiacre.types.PortComposition elem = cur.getHeadPCList();
        array[i] = (T)elem;
        cur = cur.getTailPCList();
        i++;
      }
      if(!(cur instanceof fiacre.types.portcomplist.EmptyPCList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.PortComposition> getCollection() {
    return new CollectionPCList(this);
  }

  public java.util.Collection<fiacre.types.PortComposition> getCollectionPCList() {
    return new CollectionPCList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionPCList implements java.util.Collection<fiacre.types.PortComposition> {
    private PCList list;

    public PCList getPortCompList() {
      return list;
    }

    public CollectionPCList(PCList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.PortComposition> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.PortComposition> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getPortCompList().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getPortCompList().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getPortCompList().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getPortCompList().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.PortComposition> iterator() {
    return getPortCompList().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getPortCompList().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getPortCompList().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getPortCompList().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getPortCompList().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.PortComposition o) {
      list = (PCList)fiacre.types.portcomplist.ConsPCList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (PCList)fiacre.types.portcomplist.EmptyPCList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyPCList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
