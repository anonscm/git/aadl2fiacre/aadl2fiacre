
package fiacre.types.exp;



public final class Infix extends fiacre.types.Exp implements tom.library.sl.Visitable  {
  
  private static String symbolName = "Infix";


  private Infix() {}
  private int hashCode;
  private static Infix gomProto = new Infix();
    private fiacre.types.Infix _iop;
  private fiacre.types.Type _type;
  private fiacre.types.Exp _e1;
  private fiacre.types.Exp _e2;

  /**
   * Constructor that builds a term rooted by Infix
   *
   * @return a term rooted by Infix
   */

  public static Infix make(fiacre.types.Infix _iop, fiacre.types.Type _type, fiacre.types.Exp _e1, fiacre.types.Exp _e2) {

    // use the proto as a model
    gomProto.initHashCode( _iop,  _type,  _e1,  _e2);
    return (Infix) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _iop
   * @param _type
   * @param _e1
   * @param _e2
   * @param hashCode hashCode of Infix
   */
  private void init(fiacre.types.Infix _iop, fiacre.types.Type _type, fiacre.types.Exp _e1, fiacre.types.Exp _e2, int hashCode) {
    this._iop = _iop;
    this._type = _type;
    this._e1 = _e1;
    this._e2 = _e2;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _iop
   * @param _type
   * @param _e1
   * @param _e2
   */
  private void initHashCode(fiacre.types.Infix _iop, fiacre.types.Type _type, fiacre.types.Exp _e1, fiacre.types.Exp _e2) {
    this._iop = _iop;
    this._type = _type;
    this._e1 = _e1;
    this._e2 = _e2;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "Infix";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 4;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    Infix clone = new Infix();
    clone.init( _iop,  _type,  _e1,  _e2, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("Infix(");
    _iop.toStringBuilder(buffer);
buffer.append(",");
    _type.toStringBuilder(buffer);
buffer.append(",");
    _e1.toStringBuilder(buffer);
buffer.append(",");
    _e2.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    Infix tco = (Infix) ao;
    int _iopCmp = (this._iop).compareToLPO(tco._iop);
    if(_iopCmp != 0) {
      return _iopCmp;
    }

    int _typeCmp = (this._type).compareToLPO(tco._type);
    if(_typeCmp != 0) {
      return _typeCmp;
    }

    int _e1Cmp = (this._e1).compareToLPO(tco._e1);
    if(_e1Cmp != 0) {
      return _e1Cmp;
    }

    int _e2Cmp = (this._e2).compareToLPO(tco._e2);
    if(_e2Cmp != 0) {
      return _e2Cmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    Infix tco = (Infix) ao;
    int _iopCmp = (this._iop).compareTo(tco._iop);
    if(_iopCmp != 0) {
      return _iopCmp;
    }

    int _typeCmp = (this._type).compareTo(tco._type);
    if(_typeCmp != 0) {
      return _typeCmp;
    }

    int _e1Cmp = (this._e1).compareTo(tco._e1);
    if(_e1Cmp != 0) {
      return _e1Cmp;
    }

    int _e2Cmp = (this._e2).compareTo(tco._e2);
    if(_e2Cmp != 0) {
      return _e2Cmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a Infix and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof Infix) {

      Infix peer = (Infix) obj;
      return _iop==peer._iop && _type==peer._type && _e1==peer._e1 && _e2==peer._e2 && true;
    }
    return false;
  }


   //Exp interface
  /**
   * Returns true if the term is rooted by the symbol Infix
   *
   * @return true, because this is rooted by Infix
   */
  @Override
  public boolean isInfix() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.Infix
   *
   * @return the attribute fiacre.types.Infix
   */
  @Override
  public fiacre.types.Infix getiop() {
    return _iop;
  }

  /**
   * Sets and returns the attribute fiacre.types.Exp
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Infix which just has been set
   */
  @Override
  public fiacre.types.Exp setiop(fiacre.types.Infix set_arg) {
    return make(set_arg, _type, _e1, _e2);
  }
  
  /**
   * Returns the attribute fiacre.types.Type
   *
   * @return the attribute fiacre.types.Type
   */
  @Override
  public fiacre.types.Type gettype() {
    return _type;
  }

  /**
   * Sets and returns the attribute fiacre.types.Exp
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Type which just has been set
   */
  @Override
  public fiacre.types.Exp settype(fiacre.types.Type set_arg) {
    return make(_iop, set_arg, _e1, _e2);
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp gete1() {
    return _e1;
  }

  /**
   * Sets and returns the attribute fiacre.types.Exp
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.Exp sete1(fiacre.types.Exp set_arg) {
    return make(_iop, _type, set_arg, _e2);
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp gete2() {
    return _e2;
  }

  /**
   * Sets and returns the attribute fiacre.types.Exp
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.Exp sete2(fiacre.types.Exp set_arg) {
    return make(_iop, _type, _e1, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getiop().toATerm(), gettype().toATerm(), gete1().toATerm(), gete2().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Exp from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Exp fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.Infix.fromTerm(appl.getArgument(0),atConv), fiacre.types.Type.fromTerm(appl.getArgument(1),atConv), fiacre.types.Exp.fromTerm(appl.getArgument(2),atConv), fiacre.types.Exp.fromTerm(appl.getArgument(3),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 4;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _iop;
      case 1: return _type;
      case 2: return _e1;
      case 3: return _e2;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.Infix) v, _type, _e1, _e2);
      case 1: return make(_iop, (fiacre.types.Type) v, _e1, _e2);
      case 2: return make(_iop, _type, (fiacre.types.Exp) v, _e2);
      case 3: return make(_iop, _type, _e1, (fiacre.types.Exp) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 4
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 4  && childs[0] instanceof fiacre.types.Infix && childs[1] instanceof fiacre.types.Type && childs[2] instanceof fiacre.types.Exp && childs[3] instanceof fiacre.types.Exp) {
      return make((fiacre.types.Infix) childs[0], (fiacre.types.Type) childs[1], (fiacre.types.Exp) childs[2], (fiacre.types.Exp) childs[3]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _iop,  _type,  _e1,  _e2 };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (2042380347<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_iop.hashCode() << 24);
    a += (_type.hashCode() << 16);
    a += (_e1.hashCode() << 8);
    a += (_e2.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
