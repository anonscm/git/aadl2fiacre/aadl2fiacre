
package fiacre.types.exp;



public final class CondExp extends fiacre.types.Exp implements tom.library.sl.Visitable  {
  
  private static String symbolName = "CondExp";


  private CondExp() {}
  private int hashCode;
  private static CondExp gomProto = new CondExp();
    private fiacre.types.Exp _test;
  private fiacre.types.Exp _st1;
  private fiacre.types.Exp _st2;
  private fiacre.types.Type _type;

  /**
   * Constructor that builds a term rooted by CondExp
   *
   * @return a term rooted by CondExp
   */

  public static CondExp make(fiacre.types.Exp _test, fiacre.types.Exp _st1, fiacre.types.Exp _st2, fiacre.types.Type _type) {

    // use the proto as a model
    gomProto.initHashCode( _test,  _st1,  _st2,  _type);
    return (CondExp) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _test
   * @param _st1
   * @param _st2
   * @param _type
   * @param hashCode hashCode of CondExp
   */
  private void init(fiacre.types.Exp _test, fiacre.types.Exp _st1, fiacre.types.Exp _st2, fiacre.types.Type _type, int hashCode) {
    this._test = _test;
    this._st1 = _st1;
    this._st2 = _st2;
    this._type = _type;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _test
   * @param _st1
   * @param _st2
   * @param _type
   */
  private void initHashCode(fiacre.types.Exp _test, fiacre.types.Exp _st1, fiacre.types.Exp _st2, fiacre.types.Type _type) {
    this._test = _test;
    this._st1 = _st1;
    this._st2 = _st2;
    this._type = _type;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "CondExp";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 4;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    CondExp clone = new CondExp();
    clone.init( _test,  _st1,  _st2,  _type, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("CondExp(");
    _test.toStringBuilder(buffer);
buffer.append(",");
    _st1.toStringBuilder(buffer);
buffer.append(",");
    _st2.toStringBuilder(buffer);
buffer.append(",");
    _type.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    CondExp tco = (CondExp) ao;
    int _testCmp = (this._test).compareToLPO(tco._test);
    if(_testCmp != 0) {
      return _testCmp;
    }

    int _st1Cmp = (this._st1).compareToLPO(tco._st1);
    if(_st1Cmp != 0) {
      return _st1Cmp;
    }

    int _st2Cmp = (this._st2).compareToLPO(tco._st2);
    if(_st2Cmp != 0) {
      return _st2Cmp;
    }

    int _typeCmp = (this._type).compareToLPO(tco._type);
    if(_typeCmp != 0) {
      return _typeCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    CondExp tco = (CondExp) ao;
    int _testCmp = (this._test).compareTo(tco._test);
    if(_testCmp != 0) {
      return _testCmp;
    }

    int _st1Cmp = (this._st1).compareTo(tco._st1);
    if(_st1Cmp != 0) {
      return _st1Cmp;
    }

    int _st2Cmp = (this._st2).compareTo(tco._st2);
    if(_st2Cmp != 0) {
      return _st2Cmp;
    }

    int _typeCmp = (this._type).compareTo(tco._type);
    if(_typeCmp != 0) {
      return _typeCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a CondExp and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof CondExp) {

      CondExp peer = (CondExp) obj;
      return _test==peer._test && _st1==peer._st1 && _st2==peer._st2 && _type==peer._type && true;
    }
    return false;
  }


   //Exp interface
  /**
   * Returns true if the term is rooted by the symbol CondExp
   *
   * @return true, because this is rooted by CondExp
   */
  @Override
  public boolean isCondExp() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp gettest() {
    return _test;
  }

  /**
   * Sets and returns the attribute fiacre.types.Exp
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.Exp settest(fiacre.types.Exp set_arg) {
    return make(set_arg, _st1, _st2, _type);
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp getst1() {
    return _st1;
  }

  /**
   * Sets and returns the attribute fiacre.types.Exp
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.Exp setst1(fiacre.types.Exp set_arg) {
    return make(_test, set_arg, _st2, _type);
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp getst2() {
    return _st2;
  }

  /**
   * Sets and returns the attribute fiacre.types.Exp
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.Exp setst2(fiacre.types.Exp set_arg) {
    return make(_test, _st1, set_arg, _type);
  }
  
  /**
   * Returns the attribute fiacre.types.Type
   *
   * @return the attribute fiacre.types.Type
   */
  @Override
  public fiacre.types.Type gettype() {
    return _type;
  }

  /**
   * Sets and returns the attribute fiacre.types.Exp
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Type which just has been set
   */
  @Override
  public fiacre.types.Exp settype(fiacre.types.Type set_arg) {
    return make(_test, _st1, _st2, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {gettest().toATerm(), getst1().toATerm(), getst2().toATerm(), gettype().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Exp from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Exp fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.Exp.fromTerm(appl.getArgument(0),atConv), fiacre.types.Exp.fromTerm(appl.getArgument(1),atConv), fiacre.types.Exp.fromTerm(appl.getArgument(2),atConv), fiacre.types.Type.fromTerm(appl.getArgument(3),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 4;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _test;
      case 1: return _st1;
      case 2: return _st2;
      case 3: return _type;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.Exp) v, _st1, _st2, _type);
      case 1: return make(_test, (fiacre.types.Exp) v, _st2, _type);
      case 2: return make(_test, _st1, (fiacre.types.Exp) v, _type);
      case 3: return make(_test, _st1, _st2, (fiacre.types.Type) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 4
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 4  && childs[0] instanceof fiacre.types.Exp && childs[1] instanceof fiacre.types.Exp && childs[2] instanceof fiacre.types.Exp && childs[3] instanceof fiacre.types.Type) {
      return make((fiacre.types.Exp) childs[0], (fiacre.types.Exp) childs[1], (fiacre.types.Exp) childs[2], (fiacre.types.Type) childs[3]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _test,  _st1,  _st2,  _type };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-1517968637<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_test.hashCode() << 24);
    a += (_st1.hashCode() << 16);
    a += (_st2.hashCode() << 8);
    a += (_type.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
