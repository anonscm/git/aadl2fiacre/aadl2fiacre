
package fiacre.types.fieldsdecllist;



public abstract class fieldList extends fiacre.types.FieldsDeclList implements java.util.Collection<fiacre.types.FieldsDecl>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
      fiacre.types.FieldsDeclList tl = this.getTailfieldList();
      if (tl instanceof fieldList) {
        return 1+((fieldList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.FieldsDeclList fromArray(fiacre.types.FieldsDecl[] array) {
    fiacre.types.FieldsDeclList res = fiacre.types.fieldsdecllist.EmptyfieldList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.fieldsdecllist.ConsfieldList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.FieldsDeclList reverse() {
    if(this instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
      fiacre.types.FieldsDeclList cur = this;
      fiacre.types.FieldsDeclList rev = fiacre.types.fieldsdecllist.EmptyfieldList.make();
      while(cur instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
        rev = fiacre.types.fieldsdecllist.ConsfieldList.make(cur.getHeadfieldList(),rev);
        cur = cur.getTailfieldList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.FieldsDeclList append(fiacre.types.FieldsDecl element) {
    if(this instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
      fiacre.types.FieldsDeclList tl = this.getTailfieldList();
      if (tl instanceof fieldList) {
        return fiacre.types.fieldsdecllist.ConsfieldList.make(this.getHeadfieldList(),((fieldList)tl).append(element));
      } else {

        return fiacre.types.fieldsdecllist.ConsfieldList.make(this.getHeadfieldList(),fiacre.types.fieldsdecllist.ConsfieldList.make(element,tl));

      }
    } else {
      return fiacre.types.fieldsdecllist.ConsfieldList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("fieldList(");
    if(this instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
      fiacre.types.FieldsDeclList cur = this;
      while(cur instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
        fiacre.types.FieldsDecl elem = cur.getHeadfieldList();
        cur = cur.getTailfieldList();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.fieldsdecllist.EmptyfieldList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
      fiacre.types.FieldsDeclList tail = this.getTailfieldList();
      res = atermFactory.makeList(getHeadfieldList().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.FieldsDeclList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.FieldsDeclList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("fieldList".equals(appl.getName())) {
        fiacre.types.FieldsDeclList res = fiacre.types.fieldsdecllist.EmptyfieldList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.FieldsDecl elem = fiacre.types.FieldsDecl.fromTerm(array[i],atConv);
          res = fiacre.types.fieldsdecllist.ConsfieldList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.FieldsDeclList res = fiacre.types.fieldsdecllist.EmptyfieldList.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.FieldsDecl elem = fiacre.types.FieldsDecl.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.fieldsdecllist.ConsfieldList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.FieldsDeclList contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.FieldsDeclList contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.FieldsDeclList cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
      while(cur instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
        if( o.equals(cur.getHeadfieldList()) ) {
          return true;
        }
        cur = cur.getTailfieldList();
      }
      if(!(cur instanceof fiacre.types.fieldsdecllist.EmptyfieldList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyfieldList() ; }

  public java.util.Iterator<fiacre.types.FieldsDecl> iterator() {
    return new java.util.Iterator<fiacre.types.FieldsDecl>() {
      fiacre.types.FieldsDeclList list = fieldList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyfieldList();
      }

      public fiacre.types.FieldsDecl next() {
        if(list.isEmptyfieldList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsfieldList()) {
          fiacre.types.FieldsDecl head = list.getHeadfieldList();
          list = list.getTailfieldList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.FieldsDecl)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.FieldsDecl o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.FieldsDecl> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
      fiacre.types.FieldsDeclList cur = this;
      while(cur instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
        fiacre.types.FieldsDecl elem = cur.getHeadfieldList();
        array[i] = elem;
        cur = cur.getTailfieldList();
        i++;
      }
      if(!(cur instanceof fiacre.types.fieldsdecllist.EmptyfieldList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
      fiacre.types.FieldsDeclList cur = this;
      while(cur instanceof fiacre.types.fieldsdecllist.ConsfieldList) {
        fiacre.types.FieldsDecl elem = cur.getHeadfieldList();
        array[i] = (T)elem;
        cur = cur.getTailfieldList();
        i++;
      }
      if(!(cur instanceof fiacre.types.fieldsdecllist.EmptyfieldList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.FieldsDecl> getCollection() {
    return new CollectionfieldList(this);
  }

  public java.util.Collection<fiacre.types.FieldsDecl> getCollectionfieldList() {
    return new CollectionfieldList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionfieldList implements java.util.Collection<fiacre.types.FieldsDecl> {
    private fieldList list;

    public fieldList getFieldsDeclList() {
      return list;
    }

    public CollectionfieldList(fieldList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.FieldsDecl> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.FieldsDecl> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getFieldsDeclList().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getFieldsDeclList().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getFieldsDeclList().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getFieldsDeclList().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.FieldsDecl> iterator() {
    return getFieldsDeclList().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getFieldsDeclList().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getFieldsDeclList().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getFieldsDeclList().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getFieldsDeclList().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.FieldsDecl o) {
      list = (fieldList)fiacre.types.fieldsdecllist.ConsfieldList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (fieldList)fiacre.types.fieldsdecllist.EmptyfieldList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyfieldList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
