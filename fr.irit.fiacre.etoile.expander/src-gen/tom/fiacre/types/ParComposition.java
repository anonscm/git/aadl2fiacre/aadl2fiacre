
package fiacre.types;


public abstract class ParComposition extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected ParComposition() {}



  /**
   * Returns true if the term is rooted by the symbol parCompo
   *
   * @return true if the term is rooted by the symbol parCompo
   */
  public boolean isparCompo() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol parCompoB
   *
   * @return true if the term is rooted by the symbol parCompoB
   */
  public boolean isparCompoB() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot ps
   *
   * @return the subterm corresponding to the slot ps
   */
  public fiacre.types.PortSet getps() {
    throw new UnsupportedOperationException("This ParComposition has no ps");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot ps
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot ps is replaced by _arg
   */
  public ParComposition setps(fiacre.types.PortSet _arg) {
    throw new UnsupportedOperationException("This ParComposition has no ps");
  }

  /**
   * Returns the subterm corresponding to the slot pcl
   *
   * @return the subterm corresponding to the slot pcl
   */
  public fiacre.types.PortCompList getpcl() {
    throw new UnsupportedOperationException("This ParComposition has no pcl");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot pcl
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot pcl is replaced by _arg
   */
  public ParComposition setpcl(fiacre.types.PortCompList _arg) {
    throw new UnsupportedOperationException("This ParComposition has no pcl");
  }

  /**
   * Returns the subterm corresponding to the slot fin
   *
   * @return the subterm corresponding to the slot fin
   */
  public fiacre.types.Exp getfin() {
    throw new UnsupportedOperationException("This ParComposition has no fin");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot fin
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot fin is replaced by _arg
   */
  public ParComposition setfin(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This ParComposition has no fin");
  }

  /**
   * Returns the subterm corresponding to the slot id
   *
   * @return the subterm corresponding to the slot id
   */
  public String getid() {
    throw new UnsupportedOperationException("This ParComposition has no id");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot id
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot id is replaced by _arg
   */
  public ParComposition setid(String _arg) {
    throw new UnsupportedOperationException("This ParComposition has no id");
  }

  /**
   * Returns the subterm corresponding to the slot deb
   *
   * @return the subterm corresponding to the slot deb
   */
  public fiacre.types.Exp getdeb() {
    throw new UnsupportedOperationException("This ParComposition has no deb");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot deb
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot deb is replaced by _arg
   */
  public ParComposition setdeb(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This ParComposition has no deb");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.ParComposition from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.ParComposition fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.ParComposition from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.ParComposition fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.ParComposition from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.ParComposition fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.ParComposition
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.ParComposition fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.ParComposition tmp;
    java.util.ArrayList<fiacre.types.ParComposition> results = new java.util.ArrayList<fiacre.types.ParComposition>();

    tmp = fiacre.types.parcomposition.parCompo.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.parcomposition.parCompoB.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a ParComposition");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("ParComposition").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.ParComposition", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.ParComposition from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.ParComposition fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.ParComposition from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.ParComposition fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.ParComposition reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
