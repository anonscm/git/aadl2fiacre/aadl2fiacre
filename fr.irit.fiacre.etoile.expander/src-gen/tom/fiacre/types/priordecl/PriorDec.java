
package fiacre.types.priordecl;



public abstract class PriorDec extends fiacre.types.PriorDecl implements java.util.Collection<fiacre.types.ExpList>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.priordecl.ConsPriorDec) {
      fiacre.types.PriorDecl tl = this.getTailPriorDec();
      if (tl instanceof PriorDec) {
        return 1+((PriorDec)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.PriorDecl fromArray(fiacre.types.ExpList[] array) {
    fiacre.types.PriorDecl res = fiacre.types.priordecl.EmptyPriorDec.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.priordecl.ConsPriorDec.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.PriorDecl reverse() {
    if(this instanceof fiacre.types.priordecl.ConsPriorDec) {
      fiacre.types.PriorDecl cur = this;
      fiacre.types.PriorDecl rev = fiacre.types.priordecl.EmptyPriorDec.make();
      while(cur instanceof fiacre.types.priordecl.ConsPriorDec) {
        rev = fiacre.types.priordecl.ConsPriorDec.make(cur.getHeadPriorDec(),rev);
        cur = cur.getTailPriorDec();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.PriorDecl append(fiacre.types.ExpList element) {
    if(this instanceof fiacre.types.priordecl.ConsPriorDec) {
      fiacre.types.PriorDecl tl = this.getTailPriorDec();
      if (tl instanceof PriorDec) {
        return fiacre.types.priordecl.ConsPriorDec.make(this.getHeadPriorDec(),((PriorDec)tl).append(element));
      } else {

        return fiacre.types.priordecl.ConsPriorDec.make(this.getHeadPriorDec(),fiacre.types.priordecl.ConsPriorDec.make(element,tl));

      }
    } else {
      return fiacre.types.priordecl.ConsPriorDec.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("PriorDec(");
    if(this instanceof fiacre.types.priordecl.ConsPriorDec) {
      fiacre.types.PriorDecl cur = this;
      while(cur instanceof fiacre.types.priordecl.ConsPriorDec) {
        fiacre.types.ExpList elem = cur.getHeadPriorDec();
        cur = cur.getTailPriorDec();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.priordecl.ConsPriorDec) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.priordecl.EmptyPriorDec)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.priordecl.ConsPriorDec) {
      fiacre.types.PriorDecl tail = this.getTailPriorDec();
      res = atermFactory.makeList(getHeadPriorDec().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.PriorDecl from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PriorDecl fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("PriorDec".equals(appl.getName())) {
        fiacre.types.PriorDecl res = fiacre.types.priordecl.EmptyPriorDec.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.ExpList elem = fiacre.types.ExpList.fromTerm(array[i],atConv);
          res = fiacre.types.priordecl.ConsPriorDec.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.PriorDecl res = fiacre.types.priordecl.EmptyPriorDec.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.ExpList elem = fiacre.types.ExpList.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.priordecl.ConsPriorDec.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.PriorDecl contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.PriorDecl contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.PriorDecl cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.priordecl.ConsPriorDec) {
      while(cur instanceof fiacre.types.priordecl.ConsPriorDec) {
        if( o.equals(cur.getHeadPriorDec()) ) {
          return true;
        }
        cur = cur.getTailPriorDec();
      }
      if(!(cur instanceof fiacre.types.priordecl.EmptyPriorDec)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyPriorDec() ; }

  public java.util.Iterator<fiacre.types.ExpList> iterator() {
    return new java.util.Iterator<fiacre.types.ExpList>() {
      fiacre.types.PriorDecl list = PriorDec.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyPriorDec();
      }

      public fiacre.types.ExpList next() {
        if(list.isEmptyPriorDec()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsPriorDec()) {
          fiacre.types.ExpList head = list.getHeadPriorDec();
          list = list.getTailPriorDec();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.ExpList)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.ExpList o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.ExpList> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.priordecl.ConsPriorDec) {
      fiacre.types.PriorDecl cur = this;
      while(cur instanceof fiacre.types.priordecl.ConsPriorDec) {
        fiacre.types.ExpList elem = cur.getHeadPriorDec();
        array[i] = elem;
        cur = cur.getTailPriorDec();
        i++;
      }
      if(!(cur instanceof fiacre.types.priordecl.EmptyPriorDec)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.priordecl.ConsPriorDec) {
      fiacre.types.PriorDecl cur = this;
      while(cur instanceof fiacre.types.priordecl.ConsPriorDec) {
        fiacre.types.ExpList elem = cur.getHeadPriorDec();
        array[i] = (T)elem;
        cur = cur.getTailPriorDec();
        i++;
      }
      if(!(cur instanceof fiacre.types.priordecl.EmptyPriorDec)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.ExpList> getCollection() {
    return new CollectionPriorDec(this);
  }

  public java.util.Collection<fiacre.types.ExpList> getCollectionPriorDec() {
    return new CollectionPriorDec(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionPriorDec implements java.util.Collection<fiacre.types.ExpList> {
    private PriorDec list;

    public PriorDec getPriorDecl() {
      return list;
    }

    public CollectionPriorDec(PriorDec list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.ExpList> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.ExpList> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getPriorDecl().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getPriorDecl().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getPriorDecl().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getPriorDecl().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.ExpList> iterator() {
    return getPriorDecl().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getPriorDecl().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getPriorDecl().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getPriorDecl().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getPriorDecl().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.ExpList o) {
      list = (PriorDec)fiacre.types.priordecl.ConsPriorDec.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (PriorDec)fiacre.types.priordecl.EmptyPriorDec.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyPriorDec();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
