
package fiacre.types.transitions;



public final class ConsTransList extends fiacre.types.transitions.TransList implements tom.library.sl.Visitable  {
  
  private static String symbolName = "ConsTransList";


  private ConsTransList() {}
  private int hashCode;
  private static ConsTransList gomProto = new ConsTransList();
    private fiacre.types.Transition _HeadTransList;
  private fiacre.types.Transitions _TailTransList;

  /**
   * Constructor that builds a term rooted by ConsTransList
   *
   * @return a term rooted by ConsTransList
   */

  public static ConsTransList make(fiacre.types.Transition _HeadTransList, fiacre.types.Transitions _TailTransList) {

    // use the proto as a model
    gomProto.initHashCode( _HeadTransList,  _TailTransList);
    return (ConsTransList) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadTransList
   * @param _TailTransList
   * @param hashCode hashCode of ConsTransList
   */
  private void init(fiacre.types.Transition _HeadTransList, fiacre.types.Transitions _TailTransList, int hashCode) {
    this._HeadTransList = _HeadTransList;
    this._TailTransList = _TailTransList;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadTransList
   * @param _TailTransList
   */
  private void initHashCode(fiacre.types.Transition _HeadTransList, fiacre.types.Transitions _TailTransList) {
    this._HeadTransList = _HeadTransList;
    this._TailTransList = _TailTransList;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "ConsTransList";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 2;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    ConsTransList clone = new ConsTransList();
    clone.init( _HeadTransList,  _TailTransList, hashCode);
    return clone;
  }
  

  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    ConsTransList tco = (ConsTransList) ao;
    int _HeadTransListCmp = (this._HeadTransList).compareToLPO(tco._HeadTransList);
    if(_HeadTransListCmp != 0) {
      return _HeadTransListCmp;
    }

    int _TailTransListCmp = (this._TailTransList).compareToLPO(tco._TailTransList);
    if(_TailTransListCmp != 0) {
      return _TailTransListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    ConsTransList tco = (ConsTransList) ao;
    int _HeadTransListCmp = (this._HeadTransList).compareTo(tco._HeadTransList);
    if(_HeadTransListCmp != 0) {
      return _HeadTransListCmp;
    }

    int _TailTransListCmp = (this._TailTransList).compareTo(tco._TailTransList);
    if(_TailTransListCmp != 0) {
      return _TailTransListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a ConsTransList and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof ConsTransList) {

      ConsTransList peer = (ConsTransList) obj;
      return _HeadTransList==peer._HeadTransList && _TailTransList==peer._TailTransList && true;
    }
    return false;
  }


   //Transitions interface
  /**
   * Returns true if the term is rooted by the symbol ConsTransList
   *
   * @return true, because this is rooted by ConsTransList
   */
  @Override
  public boolean isConsTransList() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.Transition
   *
   * @return the attribute fiacre.types.Transition
   */
  @Override
  public fiacre.types.Transition getHeadTransList() {
    return _HeadTransList;
  }

  /**
   * Sets and returns the attribute fiacre.types.Transitions
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Transition which just has been set
   */
  @Override
  public fiacre.types.Transitions setHeadTransList(fiacre.types.Transition set_arg) {
    return make(set_arg, _TailTransList);
  }
  
  /**
   * Returns the attribute fiacre.types.Transitions
   *
   * @return the attribute fiacre.types.Transitions
   */
  @Override
  public fiacre.types.Transitions getTailTransList() {
    return _TailTransList;
  }

  /**
   * Sets and returns the attribute fiacre.types.Transitions
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Transitions which just has been set
   */
  @Override
  public fiacre.types.Transitions setTailTransList(fiacre.types.Transitions set_arg) {
    return make(_HeadTransList, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getHeadTransList().toATerm(), getTailTransList().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Transitions from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Transitions fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.Transition.fromTerm(appl.getArgument(0),atConv), fiacre.types.Transitions.fromTerm(appl.getArgument(1),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 2;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadTransList;
      case 1: return _TailTransList;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.Transition) v, _TailTransList);
      case 1: return make(_HeadTransList, (fiacre.types.Transitions) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 2
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 2  && childs[0] instanceof fiacre.types.Transition && childs[1] instanceof fiacre.types.Transitions) {
      return make((fiacre.types.Transition) childs[0], (fiacre.types.Transitions) childs[1]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _HeadTransList,  _TailTransList };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-1331251467<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_HeadTransList.hashCode() << 8);
    a += (_TailTransList.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
