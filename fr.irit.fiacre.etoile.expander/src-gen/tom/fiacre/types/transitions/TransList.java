
package fiacre.types.transitions;



public abstract class TransList extends fiacre.types.Transitions implements java.util.Collection<fiacre.types.Transition>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.transitions.ConsTransList) {
      fiacre.types.Transitions tl = this.getTailTransList();
      if (tl instanceof TransList) {
        return 1+((TransList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.Transitions fromArray(fiacre.types.Transition[] array) {
    fiacre.types.Transitions res = fiacre.types.transitions.EmptyTransList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.transitions.ConsTransList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.Transitions reverse() {
    if(this instanceof fiacre.types.transitions.ConsTransList) {
      fiacre.types.Transitions cur = this;
      fiacre.types.Transitions rev = fiacre.types.transitions.EmptyTransList.make();
      while(cur instanceof fiacre.types.transitions.ConsTransList) {
        rev = fiacre.types.transitions.ConsTransList.make(cur.getHeadTransList(),rev);
        cur = cur.getTailTransList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.Transitions append(fiacre.types.Transition element) {
    if(this instanceof fiacre.types.transitions.ConsTransList) {
      fiacre.types.Transitions tl = this.getTailTransList();
      if (tl instanceof TransList) {
        return fiacre.types.transitions.ConsTransList.make(this.getHeadTransList(),((TransList)tl).append(element));
      } else {

        return fiacre.types.transitions.ConsTransList.make(this.getHeadTransList(),fiacre.types.transitions.ConsTransList.make(element,tl));

      }
    } else {
      return fiacre.types.transitions.ConsTransList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("TransList(");
    if(this instanceof fiacre.types.transitions.ConsTransList) {
      fiacre.types.Transitions cur = this;
      while(cur instanceof fiacre.types.transitions.ConsTransList) {
        fiacre.types.Transition elem = cur.getHeadTransList();
        cur = cur.getTailTransList();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.transitions.ConsTransList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.transitions.EmptyTransList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.transitions.ConsTransList) {
      fiacre.types.Transitions tail = this.getTailTransList();
      res = atermFactory.makeList(getHeadTransList().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Transitions from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Transitions fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("TransList".equals(appl.getName())) {
        fiacre.types.Transitions res = fiacre.types.transitions.EmptyTransList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.Transition elem = fiacre.types.Transition.fromTerm(array[i],atConv);
          res = fiacre.types.transitions.ConsTransList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.Transitions res = fiacre.types.transitions.EmptyTransList.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.Transition elem = fiacre.types.Transition.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.transitions.ConsTransList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.Transitions contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.Transitions contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.Transitions cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.transitions.ConsTransList) {
      while(cur instanceof fiacre.types.transitions.ConsTransList) {
        if( o.equals(cur.getHeadTransList()) ) {
          return true;
        }
        cur = cur.getTailTransList();
      }
      if(!(cur instanceof fiacre.types.transitions.EmptyTransList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyTransList() ; }

  public java.util.Iterator<fiacre.types.Transition> iterator() {
    return new java.util.Iterator<fiacre.types.Transition>() {
      fiacre.types.Transitions list = TransList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyTransList();
      }

      public fiacre.types.Transition next() {
        if(list.isEmptyTransList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsTransList()) {
          fiacre.types.Transition head = list.getHeadTransList();
          list = list.getTailTransList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.Transition)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.Transition o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.Transition> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.transitions.ConsTransList) {
      fiacre.types.Transitions cur = this;
      while(cur instanceof fiacre.types.transitions.ConsTransList) {
        fiacre.types.Transition elem = cur.getHeadTransList();
        array[i] = elem;
        cur = cur.getTailTransList();
        i++;
      }
      if(!(cur instanceof fiacre.types.transitions.EmptyTransList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.transitions.ConsTransList) {
      fiacre.types.Transitions cur = this;
      while(cur instanceof fiacre.types.transitions.ConsTransList) {
        fiacre.types.Transition elem = cur.getHeadTransList();
        array[i] = (T)elem;
        cur = cur.getTailTransList();
        i++;
      }
      if(!(cur instanceof fiacre.types.transitions.EmptyTransList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.Transition> getCollection() {
    return new CollectionTransList(this);
  }

  public java.util.Collection<fiacre.types.Transition> getCollectionTransList() {
    return new CollectionTransList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionTransList implements java.util.Collection<fiacre.types.Transition> {
    private TransList list;

    public TransList getTransitions() {
      return list;
    }

    public CollectionTransList(TransList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.Transition> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.Transition> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getTransitions().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getTransitions().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getTransitions().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getTransitions().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.Transition> iterator() {
    return getTransitions().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getTransitions().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getTransitions().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getTransitions().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getTransitions().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.Transition o) {
      list = (TransList)fiacre.types.transitions.ConsTransList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (TransList)fiacre.types.transitions.EmptyTransList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyTransList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
