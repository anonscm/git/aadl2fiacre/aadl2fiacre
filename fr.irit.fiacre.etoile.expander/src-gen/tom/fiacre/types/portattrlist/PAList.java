
package fiacre.types.portattrlist;



public abstract class PAList extends fiacre.types.PortAttrList implements java.util.Collection<fiacre.types.PortAttr>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.portattrlist.ConsPAList) {
      fiacre.types.PortAttrList tl = this.getTailPAList();
      if (tl instanceof PAList) {
        return 1+((PAList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.PortAttrList fromArray(fiacre.types.PortAttr[] array) {
    fiacre.types.PortAttrList res = fiacre.types.portattrlist.EmptyPAList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.portattrlist.ConsPAList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.PortAttrList reverse() {
    if(this instanceof fiacre.types.portattrlist.ConsPAList) {
      fiacre.types.PortAttrList cur = this;
      fiacre.types.PortAttrList rev = fiacre.types.portattrlist.EmptyPAList.make();
      while(cur instanceof fiacre.types.portattrlist.ConsPAList) {
        rev = fiacre.types.portattrlist.ConsPAList.make(cur.getHeadPAList(),rev);
        cur = cur.getTailPAList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.PortAttrList append(fiacre.types.PortAttr element) {
    if(this instanceof fiacre.types.portattrlist.ConsPAList) {
      fiacre.types.PortAttrList tl = this.getTailPAList();
      if (tl instanceof PAList) {
        return fiacre.types.portattrlist.ConsPAList.make(this.getHeadPAList(),((PAList)tl).append(element));
      } else {

        return fiacre.types.portattrlist.ConsPAList.make(this.getHeadPAList(),fiacre.types.portattrlist.ConsPAList.make(element,tl));

      }
    } else {
      return fiacre.types.portattrlist.ConsPAList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("PAList(");
    if(this instanceof fiacre.types.portattrlist.ConsPAList) {
      fiacre.types.PortAttrList cur = this;
      while(cur instanceof fiacre.types.portattrlist.ConsPAList) {
        fiacre.types.PortAttr elem = cur.getHeadPAList();
        cur = cur.getTailPAList();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.portattrlist.ConsPAList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.portattrlist.EmptyPAList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.portattrlist.ConsPAList) {
      fiacre.types.PortAttrList tail = this.getTailPAList();
      res = atermFactory.makeList(getHeadPAList().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.PortAttrList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PortAttrList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("PAList".equals(appl.getName())) {
        fiacre.types.PortAttrList res = fiacre.types.portattrlist.EmptyPAList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.PortAttr elem = fiacre.types.PortAttr.fromTerm(array[i],atConv);
          res = fiacre.types.portattrlist.ConsPAList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.PortAttrList res = fiacre.types.portattrlist.EmptyPAList.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.PortAttr elem = fiacre.types.PortAttr.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.portattrlist.ConsPAList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.PortAttrList contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.PortAttrList contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.PortAttrList cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.portattrlist.ConsPAList) {
      while(cur instanceof fiacre.types.portattrlist.ConsPAList) {
        if( o.equals(cur.getHeadPAList()) ) {
          return true;
        }
        cur = cur.getTailPAList();
      }
      if(!(cur instanceof fiacre.types.portattrlist.EmptyPAList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyPAList() ; }

  public java.util.Iterator<fiacre.types.PortAttr> iterator() {
    return new java.util.Iterator<fiacre.types.PortAttr>() {
      fiacre.types.PortAttrList list = PAList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyPAList();
      }

      public fiacre.types.PortAttr next() {
        if(list.isEmptyPAList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsPAList()) {
          fiacre.types.PortAttr head = list.getHeadPAList();
          list = list.getTailPAList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.PortAttr)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.PortAttr o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.PortAttr> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.portattrlist.ConsPAList) {
      fiacre.types.PortAttrList cur = this;
      while(cur instanceof fiacre.types.portattrlist.ConsPAList) {
        fiacre.types.PortAttr elem = cur.getHeadPAList();
        array[i] = elem;
        cur = cur.getTailPAList();
        i++;
      }
      if(!(cur instanceof fiacre.types.portattrlist.EmptyPAList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.portattrlist.ConsPAList) {
      fiacre.types.PortAttrList cur = this;
      while(cur instanceof fiacre.types.portattrlist.ConsPAList) {
        fiacre.types.PortAttr elem = cur.getHeadPAList();
        array[i] = (T)elem;
        cur = cur.getTailPAList();
        i++;
      }
      if(!(cur instanceof fiacre.types.portattrlist.EmptyPAList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.PortAttr> getCollection() {
    return new CollectionPAList(this);
  }

  public java.util.Collection<fiacre.types.PortAttr> getCollectionPAList() {
    return new CollectionPAList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionPAList implements java.util.Collection<fiacre.types.PortAttr> {
    private PAList list;

    public PAList getPortAttrList() {
      return list;
    }

    public CollectionPAList(PAList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.PortAttr> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.PortAttr> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getPortAttrList().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getPortAttrList().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getPortAttrList().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getPortAttrList().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.PortAttr> iterator() {
    return getPortAttrList().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getPortAttrList().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getPortAttrList().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getPortAttrList().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getPortAttrList().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.PortAttr o) {
      list = (PAList)fiacre.types.portattrlist.ConsPAList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (PAList)fiacre.types.portattrlist.EmptyPAList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyPAList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
