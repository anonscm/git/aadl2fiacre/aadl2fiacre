
package fiacre.types.portattrlist;



public final class ConsPAList extends fiacre.types.portattrlist.PAList implements tom.library.sl.Visitable  {
  
  private static String symbolName = "ConsPAList";


  private ConsPAList() {}
  private int hashCode;
  private static ConsPAList gomProto = new ConsPAList();
    private fiacre.types.PortAttr _HeadPAList;
  private fiacre.types.PortAttrList _TailPAList;

  /**
   * Constructor that builds a term rooted by ConsPAList
   *
   * @return a term rooted by ConsPAList
   */

  public static ConsPAList make(fiacre.types.PortAttr _HeadPAList, fiacre.types.PortAttrList _TailPAList) {

    // use the proto as a model
    gomProto.initHashCode( _HeadPAList,  _TailPAList);
    return (ConsPAList) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadPAList
   * @param _TailPAList
   * @param hashCode hashCode of ConsPAList
   */
  private void init(fiacre.types.PortAttr _HeadPAList, fiacre.types.PortAttrList _TailPAList, int hashCode) {
    this._HeadPAList = _HeadPAList;
    this._TailPAList = _TailPAList;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadPAList
   * @param _TailPAList
   */
  private void initHashCode(fiacre.types.PortAttr _HeadPAList, fiacre.types.PortAttrList _TailPAList) {
    this._HeadPAList = _HeadPAList;
    this._TailPAList = _TailPAList;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "ConsPAList";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 2;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    ConsPAList clone = new ConsPAList();
    clone.init( _HeadPAList,  _TailPAList, hashCode);
    return clone;
  }
  

  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    ConsPAList tco = (ConsPAList) ao;
    int _HeadPAListCmp = (this._HeadPAList).compareToLPO(tco._HeadPAList);
    if(_HeadPAListCmp != 0) {
      return _HeadPAListCmp;
    }

    int _TailPAListCmp = (this._TailPAList).compareToLPO(tco._TailPAList);
    if(_TailPAListCmp != 0) {
      return _TailPAListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    ConsPAList tco = (ConsPAList) ao;
    int _HeadPAListCmp = (this._HeadPAList).compareTo(tco._HeadPAList);
    if(_HeadPAListCmp != 0) {
      return _HeadPAListCmp;
    }

    int _TailPAListCmp = (this._TailPAList).compareTo(tco._TailPAList);
    if(_TailPAListCmp != 0) {
      return _TailPAListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a ConsPAList and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof ConsPAList) {

      ConsPAList peer = (ConsPAList) obj;
      return _HeadPAList==peer._HeadPAList && _TailPAList==peer._TailPAList && true;
    }
    return false;
  }


   //PortAttrList interface
  /**
   * Returns true if the term is rooted by the symbol ConsPAList
   *
   * @return true, because this is rooted by ConsPAList
   */
  @Override
  public boolean isConsPAList() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.PortAttr
   *
   * @return the attribute fiacre.types.PortAttr
   */
  @Override
  public fiacre.types.PortAttr getHeadPAList() {
    return _HeadPAList;
  }

  /**
   * Sets and returns the attribute fiacre.types.PortAttrList
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PortAttr which just has been set
   */
  @Override
  public fiacre.types.PortAttrList setHeadPAList(fiacre.types.PortAttr set_arg) {
    return make(set_arg, _TailPAList);
  }
  
  /**
   * Returns the attribute fiacre.types.PortAttrList
   *
   * @return the attribute fiacre.types.PortAttrList
   */
  @Override
  public fiacre.types.PortAttrList getTailPAList() {
    return _TailPAList;
  }

  /**
   * Sets and returns the attribute fiacre.types.PortAttrList
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PortAttrList which just has been set
   */
  @Override
  public fiacre.types.PortAttrList setTailPAList(fiacre.types.PortAttrList set_arg) {
    return make(_HeadPAList, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getHeadPAList().toATerm(), getTailPAList().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.PortAttrList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PortAttrList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.PortAttr.fromTerm(appl.getArgument(0),atConv), fiacre.types.PortAttrList.fromTerm(appl.getArgument(1),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 2;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadPAList;
      case 1: return _TailPAList;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.PortAttr) v, _TailPAList);
      case 1: return make(_HeadPAList, (fiacre.types.PortAttrList) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 2
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 2  && childs[0] instanceof fiacre.types.PortAttr && childs[1] instanceof fiacre.types.PortAttrList) {
      return make((fiacre.types.PortAttr) childs[0], (fiacre.types.PortAttrList) childs[1]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _HeadPAList,  _TailPAList };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-880213881<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_HeadPAList.hashCode() << 8);
    a += (_TailPAList.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
