
package fiacre.types.vardecls;



public abstract class ListVarDecl extends fiacre.types.VarDecls implements java.util.Collection<fiacre.types.VarDecl>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.vardecls.ConsListVarDecl) {
      fiacre.types.VarDecls tl = this.getTailListVarDecl();
      if (tl instanceof ListVarDecl) {
        return 1+((ListVarDecl)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.VarDecls fromArray(fiacre.types.VarDecl[] array) {
    fiacre.types.VarDecls res = fiacre.types.vardecls.EmptyListVarDecl.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.vardecls.ConsListVarDecl.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.VarDecls reverse() {
    if(this instanceof fiacre.types.vardecls.ConsListVarDecl) {
      fiacre.types.VarDecls cur = this;
      fiacre.types.VarDecls rev = fiacre.types.vardecls.EmptyListVarDecl.make();
      while(cur instanceof fiacre.types.vardecls.ConsListVarDecl) {
        rev = fiacre.types.vardecls.ConsListVarDecl.make(cur.getHeadListVarDecl(),rev);
        cur = cur.getTailListVarDecl();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.VarDecls append(fiacre.types.VarDecl element) {
    if(this instanceof fiacre.types.vardecls.ConsListVarDecl) {
      fiacre.types.VarDecls tl = this.getTailListVarDecl();
      if (tl instanceof ListVarDecl) {
        return fiacre.types.vardecls.ConsListVarDecl.make(this.getHeadListVarDecl(),((ListVarDecl)tl).append(element));
      } else {

        return fiacre.types.vardecls.ConsListVarDecl.make(this.getHeadListVarDecl(),fiacre.types.vardecls.ConsListVarDecl.make(element,tl));

      }
    } else {
      return fiacre.types.vardecls.ConsListVarDecl.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("ListVarDecl(");
    if(this instanceof fiacre.types.vardecls.ConsListVarDecl) {
      fiacre.types.VarDecls cur = this;
      while(cur instanceof fiacre.types.vardecls.ConsListVarDecl) {
        fiacre.types.VarDecl elem = cur.getHeadListVarDecl();
        cur = cur.getTailListVarDecl();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.vardecls.ConsListVarDecl) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.vardecls.EmptyListVarDecl)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.vardecls.ConsListVarDecl) {
      fiacre.types.VarDecls tail = this.getTailListVarDecl();
      res = atermFactory.makeList(getHeadListVarDecl().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.VarDecls from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.VarDecls fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("ListVarDecl".equals(appl.getName())) {
        fiacre.types.VarDecls res = fiacre.types.vardecls.EmptyListVarDecl.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.VarDecl elem = fiacre.types.VarDecl.fromTerm(array[i],atConv);
          res = fiacre.types.vardecls.ConsListVarDecl.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.VarDecls res = fiacre.types.vardecls.EmptyListVarDecl.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.VarDecl elem = fiacre.types.VarDecl.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.vardecls.ConsListVarDecl.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.VarDecls contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.VarDecls contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.VarDecls cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.vardecls.ConsListVarDecl) {
      while(cur instanceof fiacre.types.vardecls.ConsListVarDecl) {
        if( o.equals(cur.getHeadListVarDecl()) ) {
          return true;
        }
        cur = cur.getTailListVarDecl();
      }
      if(!(cur instanceof fiacre.types.vardecls.EmptyListVarDecl)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyListVarDecl() ; }

  public java.util.Iterator<fiacre.types.VarDecl> iterator() {
    return new java.util.Iterator<fiacre.types.VarDecl>() {
      fiacre.types.VarDecls list = ListVarDecl.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyListVarDecl();
      }

      public fiacre.types.VarDecl next() {
        if(list.isEmptyListVarDecl()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsListVarDecl()) {
          fiacre.types.VarDecl head = list.getHeadListVarDecl();
          list = list.getTailListVarDecl();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.VarDecl)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.VarDecl o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.VarDecl> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.vardecls.ConsListVarDecl) {
      fiacre.types.VarDecls cur = this;
      while(cur instanceof fiacre.types.vardecls.ConsListVarDecl) {
        fiacre.types.VarDecl elem = cur.getHeadListVarDecl();
        array[i] = elem;
        cur = cur.getTailListVarDecl();
        i++;
      }
      if(!(cur instanceof fiacre.types.vardecls.EmptyListVarDecl)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.vardecls.ConsListVarDecl) {
      fiacre.types.VarDecls cur = this;
      while(cur instanceof fiacre.types.vardecls.ConsListVarDecl) {
        fiacre.types.VarDecl elem = cur.getHeadListVarDecl();
        array[i] = (T)elem;
        cur = cur.getTailListVarDecl();
        i++;
      }
      if(!(cur instanceof fiacre.types.vardecls.EmptyListVarDecl)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.VarDecl> getCollection() {
    return new CollectionListVarDecl(this);
  }

  public java.util.Collection<fiacre.types.VarDecl> getCollectionListVarDecl() {
    return new CollectionListVarDecl(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionListVarDecl implements java.util.Collection<fiacre.types.VarDecl> {
    private ListVarDecl list;

    public ListVarDecl getVarDecls() {
      return list;
    }

    public CollectionListVarDecl(ListVarDecl list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.VarDecl> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.VarDecl> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getVarDecls().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getVarDecls().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getVarDecls().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getVarDecls().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.VarDecl> iterator() {
    return getVarDecls().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getVarDecls().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getVarDecls().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getVarDecls().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getVarDecls().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.VarDecl o) {
      list = (ListVarDecl)fiacre.types.vardecls.ConsListVarDecl.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (ListVarDecl)fiacre.types.vardecls.EmptyListVarDecl.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyListVarDecl();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
