
package fiacre.types;


public abstract class PriorDecl extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected PriorDecl() {}



  /**
   * Returns true if the term is rooted by the symbol ConsPriorDec
   *
   * @return true if the term is rooted by the symbol ConsPriorDec
   */
  public boolean isConsPriorDec() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol EmptyPriorDec
   *
   * @return true if the term is rooted by the symbol EmptyPriorDec
   */
  public boolean isEmptyPriorDec() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot TailPriorDec
   *
   * @return the subterm corresponding to the slot TailPriorDec
   */
  public fiacre.types.PriorDecl getTailPriorDec() {
    throw new UnsupportedOperationException("This PriorDecl has no TailPriorDec");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot TailPriorDec
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot TailPriorDec is replaced by _arg
   */
  public PriorDecl setTailPriorDec(fiacre.types.PriorDecl _arg) {
    throw new UnsupportedOperationException("This PriorDecl has no TailPriorDec");
  }

  /**
   * Returns the subterm corresponding to the slot HeadPriorDec
   *
   * @return the subterm corresponding to the slot HeadPriorDec
   */
  public fiacre.types.ExpList getHeadPriorDec() {
    throw new UnsupportedOperationException("This PriorDecl has no HeadPriorDec");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot HeadPriorDec
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot HeadPriorDec is replaced by _arg
   */
  public PriorDecl setHeadPriorDec(fiacre.types.ExpList _arg) {
    throw new UnsupportedOperationException("This PriorDecl has no HeadPriorDec");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.PriorDecl from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.PriorDecl fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.PriorDecl from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.PriorDecl fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.PriorDecl from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.PriorDecl fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.PriorDecl
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.PriorDecl fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.PriorDecl tmp;
    java.util.ArrayList<fiacre.types.PriorDecl> results = new java.util.ArrayList<fiacre.types.PriorDecl>();

    tmp = fiacre.types.priordecl.ConsPriorDec.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.priordecl.EmptyPriorDec.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.priordecl.PriorDec.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a PriorDecl");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("PriorDecl").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.PriorDecl", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.PriorDecl from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PriorDecl fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.PriorDecl from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PriorDecl fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.PriorDecl reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
  /**
   * Returns a Collection extracted from the term
   *
   * @return the collection
   * @throws UnsupportedOperationException if the term is not a list
   */
  public java.util.Collection<fiacre.types.ExpList> getCollectionPriorDec() {
    throw new UnsupportedOperationException("This PriorDecl cannot be converted into a Collection");
  }
          
}
