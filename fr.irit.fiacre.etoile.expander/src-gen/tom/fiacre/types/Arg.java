
package fiacre.types;


public abstract class Arg extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected Arg() {}



  /**
   * Returns true if the term is rooted by the symbol ValArg
   *
   * @return true if the term is rooted by the symbol ValArg
   */
  public boolean isValArg() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol RefArg
   *
   * @return true if the term is rooted by the symbol RefArg
   */
  public boolean isRefArg() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ArrayRefArg
   *
   * @return true if the term is rooted by the symbol ArrayRefArg
   */
  public boolean isArrayRefArg() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot arg
   *
   * @return the subterm corresponding to the slot arg
   */
  public String getarg() {
    throw new UnsupportedOperationException("This Arg has no arg");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot arg
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot arg is replaced by _arg
   */
  public Arg setarg(String _arg) {
    throw new UnsupportedOperationException("This Arg has no arg");
  }

  /**
   * Returns the subterm corresponding to the slot size
   *
   * @return the subterm corresponding to the slot size
   */
  public fiacre.types.Exp getsize() {
    throw new UnsupportedOperationException("This Arg has no size");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot size
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot size is replaced by _arg
   */
  public Arg setsize(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Arg has no size");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.Arg from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.Arg fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.Arg from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.Arg fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.Arg from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.Arg fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.Arg
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.Arg fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.Arg tmp;
    java.util.ArrayList<fiacre.types.Arg> results = new java.util.ArrayList<fiacre.types.Arg>();

    tmp = fiacre.types.arg.ValArg.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.arg.RefArg.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.arg.ArrayRefArg.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a Arg");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("Arg").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.Arg", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Arg from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Arg fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.Arg from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Arg fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.Arg reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
