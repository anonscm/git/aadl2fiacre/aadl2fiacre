
package fiacre.types;


public abstract class LTL extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected LTL() {}



  /**
   * Returns true if the term is rooted by the symbol LtlObs
   *
   * @return true if the term is rooted by the symbol LtlObs
   */
  public boolean isLtlObs() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol LtlAtom
   *
   * @return true if the term is rooted by the symbol LtlAtom
   */
  public boolean isLtlAtom() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol LtlAll
   *
   * @return true if the term is rooted by the symbol LtlAll
   */
  public boolean isLtlAll() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol LtlEx
   *
   * @return true if the term is rooted by the symbol LtlEx
   */
  public boolean isLtlEx() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol LtlUnary
   *
   * @return true if the term is rooted by the symbol LtlUnary
   */
  public boolean isLtlUnary() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol LtlBinary
   *
   * @return true if the term is rooted by the symbol LtlBinary
   */
  public boolean isLtlBinary() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol LtlPar
   *
   * @return true if the term is rooted by the symbol LtlPar
   */
  public boolean isLtlPar() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot name
   *
   * @return the subterm corresponding to the slot name
   */
  public String getname() {
    throw new UnsupportedOperationException("This LTL has no name");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot name
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot name is replaced by _arg
   */
  public LTL setname(String _arg) {
    throw new UnsupportedOperationException("This LTL has no name");
  }

  /**
   * Returns the subterm corresponding to the slot l2
   *
   * @return the subterm corresponding to the slot l2
   */
  public fiacre.types.LTL getl2() {
    throw new UnsupportedOperationException("This LTL has no l2");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot l2
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot l2 is replaced by _arg
   */
  public LTL setl2(fiacre.types.LTL _arg) {
    throw new UnsupportedOperationException("This LTL has no l2");
  }

  /**
   * Returns the subterm corresponding to the slot begin
   *
   * @return the subterm corresponding to the slot begin
   */
  public fiacre.types.Exp getbegin() {
    throw new UnsupportedOperationException("This LTL has no begin");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot begin
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot begin is replaced by _arg
   */
  public LTL setbegin(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This LTL has no begin");
  }

  /**
   * Returns the subterm corresponding to the slot end
   *
   * @return the subterm corresponding to the slot end
   */
  public fiacre.types.Exp getend() {
    throw new UnsupportedOperationException("This LTL has no end");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot end
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot end is replaced by _arg
   */
  public LTL setend(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This LTL has no end");
  }

  /**
   * Returns the subterm corresponding to the slot l1
   *
   * @return the subterm corresponding to the slot l1
   */
  public fiacre.types.LTL getl1() {
    throw new UnsupportedOperationException("This LTL has no l1");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot l1
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot l1 is replaced by _arg
   */
  public LTL setl1(fiacre.types.LTL _arg) {
    throw new UnsupportedOperationException("This LTL has no l1");
  }

  /**
   * Returns the subterm corresponding to the slot ind
   *
   * @return the subterm corresponding to the slot ind
   */
  public String getind() {
    throw new UnsupportedOperationException("This LTL has no ind");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot ind
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot ind is replaced by _arg
   */
  public LTL setind(String _arg) {
    throw new UnsupportedOperationException("This LTL has no ind");
  }

  /**
   * Returns the subterm corresponding to the slot l
   *
   * @return the subterm corresponding to the slot l
   */
  public fiacre.types.LTL getl() {
    throw new UnsupportedOperationException("This LTL has no l");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot l
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot l is replaced by _arg
   */
  public LTL setl(fiacre.types.LTL _arg) {
    throw new UnsupportedOperationException("This LTL has no l");
  }

  /**
   * Returns the subterm corresponding to the slot obs
   *
   * @return the subterm corresponding to the slot obs
   */
  public fiacre.types.Observable getobs() {
    throw new UnsupportedOperationException("This LTL has no obs");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot obs
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot obs is replaced by _arg
   */
  public LTL setobs(fiacre.types.Observable _arg) {
    throw new UnsupportedOperationException("This LTL has no obs");
  }

  /**
   * Returns the subterm corresponding to the slot op
   *
   * @return the subterm corresponding to the slot op
   */
  public String getop() {
    throw new UnsupportedOperationException("This LTL has no op");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot op
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot op is replaced by _arg
   */
  public LTL setop(String _arg) {
    throw new UnsupportedOperationException("This LTL has no op");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.LTL from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.LTL fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.LTL from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.LTL fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.LTL from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.LTL fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.LTL
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.LTL fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.LTL tmp;
    java.util.ArrayList<fiacre.types.LTL> results = new java.util.ArrayList<fiacre.types.LTL>();

    tmp = fiacre.types.ltl.LtlObs.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.ltl.LtlAtom.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.ltl.LtlAll.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.ltl.LtlEx.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.ltl.LtlUnary.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.ltl.LtlBinary.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.ltl.LtlPar.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a LTL");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("LTL").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.LTL", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.LTL from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.LTL fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.LTL from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.LTL fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.LTL reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
