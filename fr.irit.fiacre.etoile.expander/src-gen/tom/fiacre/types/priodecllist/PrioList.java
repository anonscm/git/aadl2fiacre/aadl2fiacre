
package fiacre.types.priodecllist;



public abstract class PrioList extends fiacre.types.PrioDeclList implements java.util.Collection<fiacre.types.PriorDecl>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.priodecllist.ConsPrioList) {
      fiacre.types.PrioDeclList tl = this.getTailPrioList();
      if (tl instanceof PrioList) {
        return 1+((PrioList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.PrioDeclList fromArray(fiacre.types.PriorDecl[] array) {
    fiacre.types.PrioDeclList res = fiacre.types.priodecllist.EmptyPrioList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.priodecllist.ConsPrioList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.PrioDeclList reverse() {
    if(this instanceof fiacre.types.priodecllist.ConsPrioList) {
      fiacre.types.PrioDeclList cur = this;
      fiacre.types.PrioDeclList rev = fiacre.types.priodecllist.EmptyPrioList.make();
      while(cur instanceof fiacre.types.priodecllist.ConsPrioList) {
        rev = fiacre.types.priodecllist.ConsPrioList.make(cur.getHeadPrioList(),rev);
        cur = cur.getTailPrioList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.PrioDeclList append(fiacre.types.PriorDecl element) {
    if(this instanceof fiacre.types.priodecllist.ConsPrioList) {
      fiacre.types.PrioDeclList tl = this.getTailPrioList();
      if (tl instanceof PrioList) {
        return fiacre.types.priodecllist.ConsPrioList.make(this.getHeadPrioList(),((PrioList)tl).append(element));
      } else {

        return fiacre.types.priodecllist.ConsPrioList.make(this.getHeadPrioList(),fiacre.types.priodecllist.ConsPrioList.make(element,tl));

      }
    } else {
      return fiacre.types.priodecllist.ConsPrioList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("PrioList(");
    if(this instanceof fiacre.types.priodecllist.ConsPrioList) {
      fiacre.types.PrioDeclList cur = this;
      while(cur instanceof fiacre.types.priodecllist.ConsPrioList) {
        fiacre.types.PriorDecl elem = cur.getHeadPrioList();
        cur = cur.getTailPrioList();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.priodecllist.ConsPrioList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.priodecllist.EmptyPrioList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.priodecllist.ConsPrioList) {
      fiacre.types.PrioDeclList tail = this.getTailPrioList();
      res = atermFactory.makeList(getHeadPrioList().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.PrioDeclList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PrioDeclList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("PrioList".equals(appl.getName())) {
        fiacre.types.PrioDeclList res = fiacre.types.priodecllist.EmptyPrioList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.PriorDecl elem = fiacre.types.PriorDecl.fromTerm(array[i],atConv);
          res = fiacre.types.priodecllist.ConsPrioList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.PrioDeclList res = fiacre.types.priodecllist.EmptyPrioList.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.PriorDecl elem = fiacre.types.PriorDecl.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.priodecllist.ConsPrioList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.PrioDeclList contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.PrioDeclList contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.PrioDeclList cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.priodecllist.ConsPrioList) {
      while(cur instanceof fiacre.types.priodecllist.ConsPrioList) {
        if( o.equals(cur.getHeadPrioList()) ) {
          return true;
        }
        cur = cur.getTailPrioList();
      }
      if(!(cur instanceof fiacre.types.priodecllist.EmptyPrioList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyPrioList() ; }

  public java.util.Iterator<fiacre.types.PriorDecl> iterator() {
    return new java.util.Iterator<fiacre.types.PriorDecl>() {
      fiacre.types.PrioDeclList list = PrioList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyPrioList();
      }

      public fiacre.types.PriorDecl next() {
        if(list.isEmptyPrioList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsPrioList()) {
          fiacre.types.PriorDecl head = list.getHeadPrioList();
          list = list.getTailPrioList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.PriorDecl)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.PriorDecl o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.PriorDecl> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.priodecllist.ConsPrioList) {
      fiacre.types.PrioDeclList cur = this;
      while(cur instanceof fiacre.types.priodecllist.ConsPrioList) {
        fiacre.types.PriorDecl elem = cur.getHeadPrioList();
        array[i] = elem;
        cur = cur.getTailPrioList();
        i++;
      }
      if(!(cur instanceof fiacre.types.priodecllist.EmptyPrioList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.priodecllist.ConsPrioList) {
      fiacre.types.PrioDeclList cur = this;
      while(cur instanceof fiacre.types.priodecllist.ConsPrioList) {
        fiacre.types.PriorDecl elem = cur.getHeadPrioList();
        array[i] = (T)elem;
        cur = cur.getTailPrioList();
        i++;
      }
      if(!(cur instanceof fiacre.types.priodecllist.EmptyPrioList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.PriorDecl> getCollection() {
    return new CollectionPrioList(this);
  }

  public java.util.Collection<fiacre.types.PriorDecl> getCollectionPrioList() {
    return new CollectionPrioList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionPrioList implements java.util.Collection<fiacre.types.PriorDecl> {
    private PrioList list;

    public PrioList getPrioDeclList() {
      return list;
    }

    public CollectionPrioList(PrioList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.PriorDecl> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.PriorDecl> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getPrioDeclList().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getPrioDeclList().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getPrioDeclList().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getPrioDeclList().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.PriorDecl> iterator() {
    return getPrioDeclList().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getPrioDeclList().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getPrioDeclList().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getPrioDeclList().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getPrioDeclList().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.PriorDecl o) {
      list = (PrioList)fiacre.types.priodecllist.ConsPrioList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (PrioList)fiacre.types.priodecllist.EmptyPrioList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyPrioList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
