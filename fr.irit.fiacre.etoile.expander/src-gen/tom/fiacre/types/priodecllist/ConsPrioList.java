
package fiacre.types.priodecllist;



public final class ConsPrioList extends fiacre.types.priodecllist.PrioList implements tom.library.sl.Visitable  {
  
  private static String symbolName = "ConsPrioList";


  private ConsPrioList() {}
  private int hashCode;
  private static ConsPrioList gomProto = new ConsPrioList();
    private fiacre.types.PriorDecl _HeadPrioList;
  private fiacre.types.PrioDeclList _TailPrioList;

  /**
   * Constructor that builds a term rooted by ConsPrioList
   *
   * @return a term rooted by ConsPrioList
   */

  public static ConsPrioList make(fiacre.types.PriorDecl _HeadPrioList, fiacre.types.PrioDeclList _TailPrioList) {

    // use the proto as a model
    gomProto.initHashCode( _HeadPrioList,  _TailPrioList);
    return (ConsPrioList) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadPrioList
   * @param _TailPrioList
   * @param hashCode hashCode of ConsPrioList
   */
  private void init(fiacre.types.PriorDecl _HeadPrioList, fiacre.types.PrioDeclList _TailPrioList, int hashCode) {
    this._HeadPrioList = _HeadPrioList;
    this._TailPrioList = _TailPrioList;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadPrioList
   * @param _TailPrioList
   */
  private void initHashCode(fiacre.types.PriorDecl _HeadPrioList, fiacre.types.PrioDeclList _TailPrioList) {
    this._HeadPrioList = _HeadPrioList;
    this._TailPrioList = _TailPrioList;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "ConsPrioList";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 2;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    ConsPrioList clone = new ConsPrioList();
    clone.init( _HeadPrioList,  _TailPrioList, hashCode);
    return clone;
  }
  

  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    ConsPrioList tco = (ConsPrioList) ao;
    int _HeadPrioListCmp = (this._HeadPrioList).compareToLPO(tco._HeadPrioList);
    if(_HeadPrioListCmp != 0) {
      return _HeadPrioListCmp;
    }

    int _TailPrioListCmp = (this._TailPrioList).compareToLPO(tco._TailPrioList);
    if(_TailPrioListCmp != 0) {
      return _TailPrioListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    ConsPrioList tco = (ConsPrioList) ao;
    int _HeadPrioListCmp = (this._HeadPrioList).compareTo(tco._HeadPrioList);
    if(_HeadPrioListCmp != 0) {
      return _HeadPrioListCmp;
    }

    int _TailPrioListCmp = (this._TailPrioList).compareTo(tco._TailPrioList);
    if(_TailPrioListCmp != 0) {
      return _TailPrioListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a ConsPrioList and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof ConsPrioList) {

      ConsPrioList peer = (ConsPrioList) obj;
      return _HeadPrioList==peer._HeadPrioList && _TailPrioList==peer._TailPrioList && true;
    }
    return false;
  }


   //PrioDeclList interface
  /**
   * Returns true if the term is rooted by the symbol ConsPrioList
   *
   * @return true, because this is rooted by ConsPrioList
   */
  @Override
  public boolean isConsPrioList() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.PriorDecl
   *
   * @return the attribute fiacre.types.PriorDecl
   */
  @Override
  public fiacre.types.PriorDecl getHeadPrioList() {
    return _HeadPrioList;
  }

  /**
   * Sets and returns the attribute fiacre.types.PrioDeclList
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PriorDecl which just has been set
   */
  @Override
  public fiacre.types.PrioDeclList setHeadPrioList(fiacre.types.PriorDecl set_arg) {
    return make(set_arg, _TailPrioList);
  }
  
  /**
   * Returns the attribute fiacre.types.PrioDeclList
   *
   * @return the attribute fiacre.types.PrioDeclList
   */
  @Override
  public fiacre.types.PrioDeclList getTailPrioList() {
    return _TailPrioList;
  }

  /**
   * Sets and returns the attribute fiacre.types.PrioDeclList
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PrioDeclList which just has been set
   */
  @Override
  public fiacre.types.PrioDeclList setTailPrioList(fiacre.types.PrioDeclList set_arg) {
    return make(_HeadPrioList, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getHeadPrioList().toATerm(), getTailPrioList().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.PrioDeclList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PrioDeclList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.PriorDecl.fromTerm(appl.getArgument(0),atConv), fiacre.types.PrioDeclList.fromTerm(appl.getArgument(1),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 2;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadPrioList;
      case 1: return _TailPrioList;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.PriorDecl) v, _TailPrioList);
      case 1: return make(_HeadPrioList, (fiacre.types.PrioDeclList) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 2
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 2  && childs[0] instanceof fiacre.types.PriorDecl && childs[1] instanceof fiacre.types.PrioDeclList) {
      return make((fiacre.types.PriorDecl) childs[0], (fiacre.types.PrioDeclList) childs[1]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _HeadPrioList,  _TailPrioList };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-1859890693<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_HeadPrioList.hashCode() << 8);
    a += (_TailPrioList.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
