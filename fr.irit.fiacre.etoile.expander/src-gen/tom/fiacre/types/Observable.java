
package fiacre.types;


public abstract class Observable extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected Observable() {}



  /**
   * Returns true if the term is rooted by the symbol ObsItem
   *
   * @return true if the term is rooted by the symbol ObsItem
   */
  public boolean isObsItem() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ObsUnary
   *
   * @return true if the term is rooted by the symbol ObsUnary
   */
  public boolean isObsUnary() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ObsBinary
   *
   * @return true if the term is rooted by the symbol ObsBinary
   */
  public boolean isObsBinary() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ObsPar
   *
   * @return true if the term is rooted by the symbol ObsPar
   */
  public boolean isObsPar() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot i
   *
   * @return the subterm corresponding to the slot i
   */
  public fiacre.types.Item geti() {
    throw new UnsupportedOperationException("This Observable has no i");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot i
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot i is replaced by _arg
   */
  public Observable seti(fiacre.types.Item _arg) {
    throw new UnsupportedOperationException("This Observable has no i");
  }

  /**
   * Returns the subterm corresponding to the slot o
   *
   * @return the subterm corresponding to the slot o
   */
  public fiacre.types.Observable geto() {
    throw new UnsupportedOperationException("This Observable has no o");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot o
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot o is replaced by _arg
   */
  public Observable seto(fiacre.types.Observable _arg) {
    throw new UnsupportedOperationException("This Observable has no o");
  }

  /**
   * Returns the subterm corresponding to the slot o1
   *
   * @return the subterm corresponding to the slot o1
   */
  public fiacre.types.Observable geto1() {
    throw new UnsupportedOperationException("This Observable has no o1");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot o1
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot o1 is replaced by _arg
   */
  public Observable seto1(fiacre.types.Observable _arg) {
    throw new UnsupportedOperationException("This Observable has no o1");
  }

  /**
   * Returns the subterm corresponding to the slot o2
   *
   * @return the subterm corresponding to the slot o2
   */
  public fiacre.types.Observable geto2() {
    throw new UnsupportedOperationException("This Observable has no o2");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot o2
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot o2 is replaced by _arg
   */
  public Observable seto2(fiacre.types.Observable _arg) {
    throw new UnsupportedOperationException("This Observable has no o2");
  }

  /**
   * Returns the subterm corresponding to the slot obs
   *
   * @return the subterm corresponding to the slot obs
   */
  public fiacre.types.Observable getobs() {
    throw new UnsupportedOperationException("This Observable has no obs");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot obs
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot obs is replaced by _arg
   */
  public Observable setobs(fiacre.types.Observable _arg) {
    throw new UnsupportedOperationException("This Observable has no obs");
  }

  /**
   * Returns the subterm corresponding to the slot p
   *
   * @return the subterm corresponding to the slot p
   */
  public fiacre.types.OPath getp() {
    throw new UnsupportedOperationException("This Observable has no p");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot p
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot p is replaced by _arg
   */
  public Observable setp(fiacre.types.OPath _arg) {
    throw new UnsupportedOperationException("This Observable has no p");
  }

  /**
   * Returns the subterm corresponding to the slot op
   *
   * @return the subterm corresponding to the slot op
   */
  public String getop() {
    throw new UnsupportedOperationException("This Observable has no op");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot op
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot op is replaced by _arg
   */
  public Observable setop(String _arg) {
    throw new UnsupportedOperationException("This Observable has no op");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.Observable from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.Observable fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.Observable from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.Observable fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.Observable from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.Observable fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.Observable
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.Observable fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.Observable tmp;
    java.util.ArrayList<fiacre.types.Observable> results = new java.util.ArrayList<fiacre.types.Observable>();

    tmp = fiacre.types.observable.ObsItem.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.observable.ObsUnary.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.observable.ObsBinary.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.observable.ObsPar.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a Observable");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("Observable").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.Observable", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Observable from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Observable fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.Observable from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Observable fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.Observable reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
