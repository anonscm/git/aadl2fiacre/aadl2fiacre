
package fiacre.types.typelist;



public final class ConsType_list extends fiacre.types.typelist.Type_list implements tom.library.sl.Visitable  {
  
  private static String symbolName = "ConsType_list";


  private ConsType_list() {}
  private int hashCode;
  private static ConsType_list gomProto = new ConsType_list();
    private fiacre.types.Type _HeadType_list;
  private fiacre.types.TypeList _TailType_list;

  /**
   * Constructor that builds a term rooted by ConsType_list
   *
   * @return a term rooted by ConsType_list
   */

  public static ConsType_list make(fiacre.types.Type _HeadType_list, fiacre.types.TypeList _TailType_list) {

    // use the proto as a model
    gomProto.initHashCode( _HeadType_list,  _TailType_list);
    return (ConsType_list) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadType_list
   * @param _TailType_list
   * @param hashCode hashCode of ConsType_list
   */
  private void init(fiacre.types.Type _HeadType_list, fiacre.types.TypeList _TailType_list, int hashCode) {
    this._HeadType_list = _HeadType_list;
    this._TailType_list = _TailType_list;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadType_list
   * @param _TailType_list
   */
  private void initHashCode(fiacre.types.Type _HeadType_list, fiacre.types.TypeList _TailType_list) {
    this._HeadType_list = _HeadType_list;
    this._TailType_list = _TailType_list;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "ConsType_list";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 2;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    ConsType_list clone = new ConsType_list();
    clone.init( _HeadType_list,  _TailType_list, hashCode);
    return clone;
  }
  

  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    ConsType_list tco = (ConsType_list) ao;
    int _HeadType_listCmp = (this._HeadType_list).compareToLPO(tco._HeadType_list);
    if(_HeadType_listCmp != 0) {
      return _HeadType_listCmp;
    }

    int _TailType_listCmp = (this._TailType_list).compareToLPO(tco._TailType_list);
    if(_TailType_listCmp != 0) {
      return _TailType_listCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    ConsType_list tco = (ConsType_list) ao;
    int _HeadType_listCmp = (this._HeadType_list).compareTo(tco._HeadType_list);
    if(_HeadType_listCmp != 0) {
      return _HeadType_listCmp;
    }

    int _TailType_listCmp = (this._TailType_list).compareTo(tco._TailType_list);
    if(_TailType_listCmp != 0) {
      return _TailType_listCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a ConsType_list and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof ConsType_list) {

      ConsType_list peer = (ConsType_list) obj;
      return _HeadType_list==peer._HeadType_list && _TailType_list==peer._TailType_list && true;
    }
    return false;
  }


   //TypeList interface
  /**
   * Returns true if the term is rooted by the symbol ConsType_list
   *
   * @return true, because this is rooted by ConsType_list
   */
  @Override
  public boolean isConsType_list() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.Type
   *
   * @return the attribute fiacre.types.Type
   */
  @Override
  public fiacre.types.Type getHeadType_list() {
    return _HeadType_list;
  }

  /**
   * Sets and returns the attribute fiacre.types.TypeList
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Type which just has been set
   */
  @Override
  public fiacre.types.TypeList setHeadType_list(fiacre.types.Type set_arg) {
    return make(set_arg, _TailType_list);
  }
  
  /**
   * Returns the attribute fiacre.types.TypeList
   *
   * @return the attribute fiacre.types.TypeList
   */
  @Override
  public fiacre.types.TypeList getTailType_list() {
    return _TailType_list;
  }

  /**
   * Sets and returns the attribute fiacre.types.TypeList
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.TypeList which just has been set
   */
  @Override
  public fiacre.types.TypeList setTailType_list(fiacre.types.TypeList set_arg) {
    return make(_HeadType_list, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getHeadType_list().toATerm(), getTailType_list().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.TypeList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.TypeList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.Type.fromTerm(appl.getArgument(0),atConv), fiacre.types.TypeList.fromTerm(appl.getArgument(1),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 2;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadType_list;
      case 1: return _TailType_list;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.Type) v, _TailType_list);
      case 1: return make(_HeadType_list, (fiacre.types.TypeList) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 2
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 2  && childs[0] instanceof fiacre.types.Type && childs[1] instanceof fiacre.types.TypeList) {
      return make((fiacre.types.Type) childs[0], (fiacre.types.TypeList) childs[1]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _HeadType_list,  _TailType_list };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-1508397498<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_HeadType_list.hashCode() << 8);
    a += (_TailType_list.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
