
package fiacre.types.declaration;



public final class ComponentDecl extends fiacre.types.Declaration implements tom.library.sl.Visitable  {
  
  private static String symbolName = "ComponentDecl";


  private ComponentDecl() {}
  private int hashCode;
  private static ComponentDecl gomProto = new ComponentDecl();
    private String _name;
  private fiacre.types.StringList _genericParams;
  private fiacre.types.PortDecls _ports;
  private fiacre.types.ParamDecls _params;
  private fiacre.types.VarDecls _vars;
  private fiacre.types.LPDList _lpd;
  private fiacre.types.PrioDeclList _prios;
  private fiacre.types.Init _init;
  private fiacre.types.Composition _bodyCmp;

  /**
   * Constructor that builds a term rooted by ComponentDecl
   *
   * @return a term rooted by ComponentDecl
   */

  public static ComponentDecl make(String _name, fiacre.types.StringList _genericParams, fiacre.types.PortDecls _ports, fiacre.types.ParamDecls _params, fiacre.types.VarDecls _vars, fiacre.types.LPDList _lpd, fiacre.types.PrioDeclList _prios, fiacre.types.Init _init, fiacre.types.Composition _bodyCmp) {

    // use the proto as a model
    gomProto.initHashCode( _name,  _genericParams,  _ports,  _params,  _vars,  _lpd,  _prios,  _init,  _bodyCmp);
    return (ComponentDecl) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _name
   * @param _genericParams
   * @param _ports
   * @param _params
   * @param _vars
   * @param _lpd
   * @param _prios
   * @param _init
   * @param _bodyCmp
   * @param hashCode hashCode of ComponentDecl
   */
  private void init(String _name, fiacre.types.StringList _genericParams, fiacre.types.PortDecls _ports, fiacre.types.ParamDecls _params, fiacre.types.VarDecls _vars, fiacre.types.LPDList _lpd, fiacre.types.PrioDeclList _prios, fiacre.types.Init _init, fiacre.types.Composition _bodyCmp, int hashCode) {
    this._name = _name.intern();
    this._genericParams = _genericParams;
    this._ports = _ports;
    this._params = _params;
    this._vars = _vars;
    this._lpd = _lpd;
    this._prios = _prios;
    this._init = _init;
    this._bodyCmp = _bodyCmp;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _name
   * @param _genericParams
   * @param _ports
   * @param _params
   * @param _vars
   * @param _lpd
   * @param _prios
   * @param _init
   * @param _bodyCmp
   */
  private void initHashCode(String _name, fiacre.types.StringList _genericParams, fiacre.types.PortDecls _ports, fiacre.types.ParamDecls _params, fiacre.types.VarDecls _vars, fiacre.types.LPDList _lpd, fiacre.types.PrioDeclList _prios, fiacre.types.Init _init, fiacre.types.Composition _bodyCmp) {
    this._name = _name.intern();
    this._genericParams = _genericParams;
    this._ports = _ports;
    this._params = _params;
    this._vars = _vars;
    this._lpd = _lpd;
    this._prios = _prios;
    this._init = _init;
    this._bodyCmp = _bodyCmp;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "ComponentDecl";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 9;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    ComponentDecl clone = new ComponentDecl();
    clone.init( _name,  _genericParams,  _ports,  _params,  _vars,  _lpd,  _prios,  _init,  _bodyCmp, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("ComponentDecl(");
    buffer.append('"');
            for (int i = 0; i < _name.length(); i++) {
              char c = _name.charAt(i);
              switch (c) {
                case '\n':
                  buffer.append('\\');
                  buffer.append('n');
                  break;
                case '\t':
                  buffer.append('\\');
                  buffer.append('t');
                  break;
                case '\b':
                  buffer.append('\\');
                  buffer.append('b');
                  break;
                case '\r':
                  buffer.append('\\');
                  buffer.append('r');
                  break;
                case '\f':
                  buffer.append('\\');
                  buffer.append('f');
                  break;
                case '\\':
                  buffer.append('\\');
                  buffer.append('\\');
                  break;
                case '\'':
                  buffer.append('\\');
                  buffer.append('\'');
                  break;
                case '\"':
                  buffer.append('\\');
                  buffer.append('\"');
                  break;
                case '!':
                case '@':
                case '#':
                case '$':
                case '%':
                case '^':
                case '&':
                case '*':
                case '(':
                case ')':
                case '-':
                case '_':
                case '+':
                case '=':
                case '|':
                case '~':
                case '{':
                case '}':
                case '[':
                case ']':
                case ';':
                case ':':
                case '<':
                case '>':
                case ',':
                case '.':
                case '?':
                case ' ':
                case '/':
                  buffer.append(c);
                  break;

                default:
                  if (java.lang.Character.isLetterOrDigit(c)) {
                    buffer.append(c);
                  } else {
                    buffer.append('\\');
                    buffer.append((char) ('0' + c / 64));
                    c = (char) (c % 64);
                    buffer.append((char) ('0' + c / 8));
                    c = (char) (c % 8);
                    buffer.append((char) ('0' + c));
                  }
              }
            }
            buffer.append('"');
buffer.append(",");
    _genericParams.toStringBuilder(buffer);
buffer.append(",");
    _ports.toStringBuilder(buffer);
buffer.append(",");
    _params.toStringBuilder(buffer);
buffer.append(",");
    _vars.toStringBuilder(buffer);
buffer.append(",");
    _lpd.toStringBuilder(buffer);
buffer.append(",");
    _prios.toStringBuilder(buffer);
buffer.append(",");
    _init.toStringBuilder(buffer);
buffer.append(",");
    _bodyCmp.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    ComponentDecl tco = (ComponentDecl) ao;
    int _nameCmp = (this._name).compareTo(tco._name);
    if(_nameCmp != 0) {
      return _nameCmp;
    }


    int _genericParamsCmp = (this._genericParams).compareToLPO(tco._genericParams);
    if(_genericParamsCmp != 0) {
      return _genericParamsCmp;
    }

    int _portsCmp = (this._ports).compareToLPO(tco._ports);
    if(_portsCmp != 0) {
      return _portsCmp;
    }

    int _paramsCmp = (this._params).compareToLPO(tco._params);
    if(_paramsCmp != 0) {
      return _paramsCmp;
    }

    int _varsCmp = (this._vars).compareToLPO(tco._vars);
    if(_varsCmp != 0) {
      return _varsCmp;
    }

    int _lpdCmp = (this._lpd).compareToLPO(tco._lpd);
    if(_lpdCmp != 0) {
      return _lpdCmp;
    }

    int _priosCmp = (this._prios).compareToLPO(tco._prios);
    if(_priosCmp != 0) {
      return _priosCmp;
    }

    int _initCmp = (this._init).compareToLPO(tco._init);
    if(_initCmp != 0) {
      return _initCmp;
    }

    int _bodyCmpCmp = (this._bodyCmp).compareToLPO(tco._bodyCmp);
    if(_bodyCmpCmp != 0) {
      return _bodyCmpCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    ComponentDecl tco = (ComponentDecl) ao;
    int _nameCmp = (this._name).compareTo(tco._name);
    if(_nameCmp != 0) {
      return _nameCmp;
    }


    int _genericParamsCmp = (this._genericParams).compareTo(tco._genericParams);
    if(_genericParamsCmp != 0) {
      return _genericParamsCmp;
    }

    int _portsCmp = (this._ports).compareTo(tco._ports);
    if(_portsCmp != 0) {
      return _portsCmp;
    }

    int _paramsCmp = (this._params).compareTo(tco._params);
    if(_paramsCmp != 0) {
      return _paramsCmp;
    }

    int _varsCmp = (this._vars).compareTo(tco._vars);
    if(_varsCmp != 0) {
      return _varsCmp;
    }

    int _lpdCmp = (this._lpd).compareTo(tco._lpd);
    if(_lpdCmp != 0) {
      return _lpdCmp;
    }

    int _priosCmp = (this._prios).compareTo(tco._prios);
    if(_priosCmp != 0) {
      return _priosCmp;
    }

    int _initCmp = (this._init).compareTo(tco._init);
    if(_initCmp != 0) {
      return _initCmp;
    }

    int _bodyCmpCmp = (this._bodyCmp).compareTo(tco._bodyCmp);
    if(_bodyCmpCmp != 0) {
      return _bodyCmpCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a ComponentDecl and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof ComponentDecl) {

      ComponentDecl peer = (ComponentDecl) obj;
      return _name==peer._name && _genericParams==peer._genericParams && _ports==peer._ports && _params==peer._params && _vars==peer._vars && _lpd==peer._lpd && _prios==peer._prios && _init==peer._init && _bodyCmp==peer._bodyCmp && true;
    }
    return false;
  }


   //Declaration interface
  /**
   * Returns true if the term is rooted by the symbol ComponentDecl
   *
   * @return true, because this is rooted by ComponentDecl
   */
  @Override
  public boolean isComponentDecl() {
    return true;
  }
  
  /**
   * Returns the attribute String
   *
   * @return the attribute String
   */
  @Override
  public String getname() {
    return _name;
  }

  /**
   * Sets and returns the attribute fiacre.types.Declaration
   *
   * @param set_arg the argument to set
   * @return the attribute String which just has been set
   */
  @Override
  public fiacre.types.Declaration setname(String set_arg) {
    return make(set_arg, _genericParams, _ports, _params, _vars, _lpd, _prios, _init, _bodyCmp);
  }
  
  /**
   * Returns the attribute fiacre.types.StringList
   *
   * @return the attribute fiacre.types.StringList
   */
  @Override
  public fiacre.types.StringList getgenericParams() {
    return _genericParams;
  }

  /**
   * Sets and returns the attribute fiacre.types.Declaration
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.StringList which just has been set
   */
  @Override
  public fiacre.types.Declaration setgenericParams(fiacre.types.StringList set_arg) {
    return make(_name, set_arg, _ports, _params, _vars, _lpd, _prios, _init, _bodyCmp);
  }
  
  /**
   * Returns the attribute fiacre.types.PortDecls
   *
   * @return the attribute fiacre.types.PortDecls
   */
  @Override
  public fiacre.types.PortDecls getports() {
    return _ports;
  }

  /**
   * Sets and returns the attribute fiacre.types.Declaration
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PortDecls which just has been set
   */
  @Override
  public fiacre.types.Declaration setports(fiacre.types.PortDecls set_arg) {
    return make(_name, _genericParams, set_arg, _params, _vars, _lpd, _prios, _init, _bodyCmp);
  }
  
  /**
   * Returns the attribute fiacre.types.ParamDecls
   *
   * @return the attribute fiacre.types.ParamDecls
   */
  @Override
  public fiacre.types.ParamDecls getparams() {
    return _params;
  }

  /**
   * Sets and returns the attribute fiacre.types.Declaration
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.ParamDecls which just has been set
   */
  @Override
  public fiacre.types.Declaration setparams(fiacre.types.ParamDecls set_arg) {
    return make(_name, _genericParams, _ports, set_arg, _vars, _lpd, _prios, _init, _bodyCmp);
  }
  
  /**
   * Returns the attribute fiacre.types.VarDecls
   *
   * @return the attribute fiacre.types.VarDecls
   */
  @Override
  public fiacre.types.VarDecls getvars() {
    return _vars;
  }

  /**
   * Sets and returns the attribute fiacre.types.Declaration
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.VarDecls which just has been set
   */
  @Override
  public fiacre.types.Declaration setvars(fiacre.types.VarDecls set_arg) {
    return make(_name, _genericParams, _ports, _params, set_arg, _lpd, _prios, _init, _bodyCmp);
  }
  
  /**
   * Returns the attribute fiacre.types.LPDList
   *
   * @return the attribute fiacre.types.LPDList
   */
  @Override
  public fiacre.types.LPDList getlpd() {
    return _lpd;
  }

  /**
   * Sets and returns the attribute fiacre.types.Declaration
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.LPDList which just has been set
   */
  @Override
  public fiacre.types.Declaration setlpd(fiacre.types.LPDList set_arg) {
    return make(_name, _genericParams, _ports, _params, _vars, set_arg, _prios, _init, _bodyCmp);
  }
  
  /**
   * Returns the attribute fiacre.types.PrioDeclList
   *
   * @return the attribute fiacre.types.PrioDeclList
   */
  @Override
  public fiacre.types.PrioDeclList getprios() {
    return _prios;
  }

  /**
   * Sets and returns the attribute fiacre.types.Declaration
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PrioDeclList which just has been set
   */
  @Override
  public fiacre.types.Declaration setprios(fiacre.types.PrioDeclList set_arg) {
    return make(_name, _genericParams, _ports, _params, _vars, _lpd, set_arg, _init, _bodyCmp);
  }
  
  /**
   * Returns the attribute fiacre.types.Init
   *
   * @return the attribute fiacre.types.Init
   */
  @Override
  public fiacre.types.Init getinit() {
    return _init;
  }

  /**
   * Sets and returns the attribute fiacre.types.Declaration
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Init which just has been set
   */
  @Override
  public fiacre.types.Declaration setinit(fiacre.types.Init set_arg) {
    return make(_name, _genericParams, _ports, _params, _vars, _lpd, _prios, set_arg, _bodyCmp);
  }
  
  /**
   * Returns the attribute fiacre.types.Composition
   *
   * @return the attribute fiacre.types.Composition
   */
  @Override
  public fiacre.types.Composition getbodyCmp() {
    return _bodyCmp;
  }

  /**
   * Sets and returns the attribute fiacre.types.Declaration
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Composition which just has been set
   */
  @Override
  public fiacre.types.Declaration setbodyCmp(fiacre.types.Composition set_arg) {
    return make(_name, _genericParams, _ports, _params, _vars, _lpd, _prios, _init, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {(aterm.ATerm) atermFactory.makeAppl(atermFactory.makeAFun(getname() ,0 , true)), getgenericParams().toATerm(), getports().toATerm(), getparams().toATerm(), getvars().toATerm(), getlpd().toATerm(), getprios().toATerm(), getinit().toATerm(), getbodyCmp().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Declaration from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Declaration fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
convertATermToString(appl.getArgument(0), atConv), fiacre.types.StringList.fromTerm(appl.getArgument(1),atConv), fiacre.types.PortDecls.fromTerm(appl.getArgument(2),atConv), fiacre.types.ParamDecls.fromTerm(appl.getArgument(3),atConv), fiacre.types.VarDecls.fromTerm(appl.getArgument(4),atConv), fiacre.types.LPDList.fromTerm(appl.getArgument(5),atConv), fiacre.types.PrioDeclList.fromTerm(appl.getArgument(6),atConv), fiacre.types.Init.fromTerm(appl.getArgument(7),atConv), fiacre.types.Composition.fromTerm(appl.getArgument(8),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 9;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return new tom.library.sl.VisitableBuiltin<String>(_name);
      case 1: return _genericParams;
      case 2: return _ports;
      case 3: return _params;
      case 4: return _vars;
      case 5: return _lpd;
      case 6: return _prios;
      case 7: return _init;
      case 8: return _bodyCmp;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make(getname(), _genericParams, _ports, _params, _vars, _lpd, _prios, _init, _bodyCmp);
      case 1: return make(getname(), (fiacre.types.StringList) v, _ports, _params, _vars, _lpd, _prios, _init, _bodyCmp);
      case 2: return make(getname(), _genericParams, (fiacre.types.PortDecls) v, _params, _vars, _lpd, _prios, _init, _bodyCmp);
      case 3: return make(getname(), _genericParams, _ports, (fiacre.types.ParamDecls) v, _vars, _lpd, _prios, _init, _bodyCmp);
      case 4: return make(getname(), _genericParams, _ports, _params, (fiacre.types.VarDecls) v, _lpd, _prios, _init, _bodyCmp);
      case 5: return make(getname(), _genericParams, _ports, _params, _vars, (fiacre.types.LPDList) v, _prios, _init, _bodyCmp);
      case 6: return make(getname(), _genericParams, _ports, _params, _vars, _lpd, (fiacre.types.PrioDeclList) v, _init, _bodyCmp);
      case 7: return make(getname(), _genericParams, _ports, _params, _vars, _lpd, _prios, (fiacre.types.Init) v, _bodyCmp);
      case 8: return make(getname(), _genericParams, _ports, _params, _vars, _lpd, _prios, _init, (fiacre.types.Composition) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 9
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 9  && childs[0] instanceof tom.library.sl.VisitableBuiltin && childs[1] instanceof fiacre.types.StringList && childs[2] instanceof fiacre.types.PortDecls && childs[3] instanceof fiacre.types.ParamDecls && childs[4] instanceof fiacre.types.VarDecls && childs[5] instanceof fiacre.types.LPDList && childs[6] instanceof fiacre.types.PrioDeclList && childs[7] instanceof fiacre.types.Init && childs[8] instanceof fiacre.types.Composition) {
      return make(((tom.library.sl.VisitableBuiltin<String>)childs[0]).getBuiltin(), (fiacre.types.StringList) childs[1], (fiacre.types.PortDecls) childs[2], (fiacre.types.ParamDecls) childs[3], (fiacre.types.VarDecls) childs[4], (fiacre.types.LPDList) childs[5], (fiacre.types.PrioDeclList) childs[6], (fiacre.types.Init) childs[7], (fiacre.types.Composition) childs[8]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  new tom.library.sl.VisitableBuiltin<String>(_name),  _genericParams,  _ports,  _params,  _vars,  _lpd,  _prios,  _init,  _bodyCmp };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-1098569102<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    c += (shared.HashFunctions.stringHashFunction(_name, 8));
    b += (_genericParams.hashCode() << 24);
    b += (_ports.hashCode() << 16);
    b += (_params.hashCode() << 8);
    b += (_vars.hashCode());
    a += (_lpd.hashCode() << 24);
    a += (_prios.hashCode() << 16);
    a += (_init.hashCode() << 8);
    a += (_bodyCmp.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
