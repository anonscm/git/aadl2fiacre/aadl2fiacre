
package fiacre.types.geninsts;



public abstract class GenInstList extends fiacre.types.GenInsts implements java.util.Collection<fiacre.types.GenInst>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.geninsts.ConsGenInstList) {
      fiacre.types.GenInsts tl = this.getTailGenInstList();
      if (tl instanceof GenInstList) {
        return 1+((GenInstList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.GenInsts fromArray(fiacre.types.GenInst[] array) {
    fiacre.types.GenInsts res = fiacre.types.geninsts.EmptyGenInstList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.geninsts.ConsGenInstList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.GenInsts reverse() {
    if(this instanceof fiacre.types.geninsts.ConsGenInstList) {
      fiacre.types.GenInsts cur = this;
      fiacre.types.GenInsts rev = fiacre.types.geninsts.EmptyGenInstList.make();
      while(cur instanceof fiacre.types.geninsts.ConsGenInstList) {
        rev = fiacre.types.geninsts.ConsGenInstList.make(cur.getHeadGenInstList(),rev);
        cur = cur.getTailGenInstList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.GenInsts append(fiacre.types.GenInst element) {
    if(this instanceof fiacre.types.geninsts.ConsGenInstList) {
      fiacre.types.GenInsts tl = this.getTailGenInstList();
      if (tl instanceof GenInstList) {
        return fiacre.types.geninsts.ConsGenInstList.make(this.getHeadGenInstList(),((GenInstList)tl).append(element));
      } else {

        return fiacre.types.geninsts.ConsGenInstList.make(this.getHeadGenInstList(),fiacre.types.geninsts.ConsGenInstList.make(element,tl));

      }
    } else {
      return fiacre.types.geninsts.ConsGenInstList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("GenInstList(");
    if(this instanceof fiacre.types.geninsts.ConsGenInstList) {
      fiacre.types.GenInsts cur = this;
      while(cur instanceof fiacre.types.geninsts.ConsGenInstList) {
        fiacre.types.GenInst elem = cur.getHeadGenInstList();
        cur = cur.getTailGenInstList();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.geninsts.ConsGenInstList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.geninsts.EmptyGenInstList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.geninsts.ConsGenInstList) {
      fiacre.types.GenInsts tail = this.getTailGenInstList();
      res = atermFactory.makeList(getHeadGenInstList().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.GenInsts from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.GenInsts fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("GenInstList".equals(appl.getName())) {
        fiacre.types.GenInsts res = fiacre.types.geninsts.EmptyGenInstList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.GenInst elem = fiacre.types.GenInst.fromTerm(array[i],atConv);
          res = fiacre.types.geninsts.ConsGenInstList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.GenInsts res = fiacre.types.geninsts.EmptyGenInstList.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.GenInst elem = fiacre.types.GenInst.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.geninsts.ConsGenInstList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.GenInsts contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.GenInsts contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.GenInsts cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.geninsts.ConsGenInstList) {
      while(cur instanceof fiacre.types.geninsts.ConsGenInstList) {
        if( o.equals(cur.getHeadGenInstList()) ) {
          return true;
        }
        cur = cur.getTailGenInstList();
      }
      if(!(cur instanceof fiacre.types.geninsts.EmptyGenInstList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyGenInstList() ; }

  public java.util.Iterator<fiacre.types.GenInst> iterator() {
    return new java.util.Iterator<fiacre.types.GenInst>() {
      fiacre.types.GenInsts list = GenInstList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyGenInstList();
      }

      public fiacre.types.GenInst next() {
        if(list.isEmptyGenInstList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsGenInstList()) {
          fiacre.types.GenInst head = list.getHeadGenInstList();
          list = list.getTailGenInstList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.GenInst)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.GenInst o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.GenInst> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.geninsts.ConsGenInstList) {
      fiacre.types.GenInsts cur = this;
      while(cur instanceof fiacre.types.geninsts.ConsGenInstList) {
        fiacre.types.GenInst elem = cur.getHeadGenInstList();
        array[i] = elem;
        cur = cur.getTailGenInstList();
        i++;
      }
      if(!(cur instanceof fiacre.types.geninsts.EmptyGenInstList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.geninsts.ConsGenInstList) {
      fiacre.types.GenInsts cur = this;
      while(cur instanceof fiacre.types.geninsts.ConsGenInstList) {
        fiacre.types.GenInst elem = cur.getHeadGenInstList();
        array[i] = (T)elem;
        cur = cur.getTailGenInstList();
        i++;
      }
      if(!(cur instanceof fiacre.types.geninsts.EmptyGenInstList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.GenInst> getCollection() {
    return new CollectionGenInstList(this);
  }

  public java.util.Collection<fiacre.types.GenInst> getCollectionGenInstList() {
    return new CollectionGenInstList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionGenInstList implements java.util.Collection<fiacre.types.GenInst> {
    private GenInstList list;

    public GenInstList getGenInsts() {
      return list;
    }

    public CollectionGenInstList(GenInstList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.GenInst> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.GenInst> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getGenInsts().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getGenInsts().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getGenInsts().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getGenInsts().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.GenInst> iterator() {
    return getGenInsts().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getGenInsts().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getGenInsts().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getGenInsts().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getGenInsts().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.GenInst o) {
      list = (GenInstList)fiacre.types.geninsts.ConsGenInstList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (GenInstList)fiacre.types.geninsts.EmptyGenInstList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyGenInstList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
