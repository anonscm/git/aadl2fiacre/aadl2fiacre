
package fiacre.types.geninsts;



public final class ConsGenInstList extends fiacre.types.geninsts.GenInstList implements tom.library.sl.Visitable  {
  
  private static String symbolName = "ConsGenInstList";


  private ConsGenInstList() {}
  private int hashCode;
  private static ConsGenInstList gomProto = new ConsGenInstList();
    private fiacre.types.GenInst _HeadGenInstList;
  private fiacre.types.GenInsts _TailGenInstList;

  /**
   * Constructor that builds a term rooted by ConsGenInstList
   *
   * @return a term rooted by ConsGenInstList
   */

  public static ConsGenInstList make(fiacre.types.GenInst _HeadGenInstList, fiacre.types.GenInsts _TailGenInstList) {

    // use the proto as a model
    gomProto.initHashCode( _HeadGenInstList,  _TailGenInstList);
    return (ConsGenInstList) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadGenInstList
   * @param _TailGenInstList
   * @param hashCode hashCode of ConsGenInstList
   */
  private void init(fiacre.types.GenInst _HeadGenInstList, fiacre.types.GenInsts _TailGenInstList, int hashCode) {
    this._HeadGenInstList = _HeadGenInstList;
    this._TailGenInstList = _TailGenInstList;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadGenInstList
   * @param _TailGenInstList
   */
  private void initHashCode(fiacre.types.GenInst _HeadGenInstList, fiacre.types.GenInsts _TailGenInstList) {
    this._HeadGenInstList = _HeadGenInstList;
    this._TailGenInstList = _TailGenInstList;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "ConsGenInstList";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 2;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    ConsGenInstList clone = new ConsGenInstList();
    clone.init( _HeadGenInstList,  _TailGenInstList, hashCode);
    return clone;
  }
  

  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    ConsGenInstList tco = (ConsGenInstList) ao;
    int _HeadGenInstListCmp = (this._HeadGenInstList).compareToLPO(tco._HeadGenInstList);
    if(_HeadGenInstListCmp != 0) {
      return _HeadGenInstListCmp;
    }

    int _TailGenInstListCmp = (this._TailGenInstList).compareToLPO(tco._TailGenInstList);
    if(_TailGenInstListCmp != 0) {
      return _TailGenInstListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    ConsGenInstList tco = (ConsGenInstList) ao;
    int _HeadGenInstListCmp = (this._HeadGenInstList).compareTo(tco._HeadGenInstList);
    if(_HeadGenInstListCmp != 0) {
      return _HeadGenInstListCmp;
    }

    int _TailGenInstListCmp = (this._TailGenInstList).compareTo(tco._TailGenInstList);
    if(_TailGenInstListCmp != 0) {
      return _TailGenInstListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a ConsGenInstList and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof ConsGenInstList) {

      ConsGenInstList peer = (ConsGenInstList) obj;
      return _HeadGenInstList==peer._HeadGenInstList && _TailGenInstList==peer._TailGenInstList && true;
    }
    return false;
  }


   //GenInsts interface
  /**
   * Returns true if the term is rooted by the symbol ConsGenInstList
   *
   * @return true, because this is rooted by ConsGenInstList
   */
  @Override
  public boolean isConsGenInstList() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.GenInst
   *
   * @return the attribute fiacre.types.GenInst
   */
  @Override
  public fiacre.types.GenInst getHeadGenInstList() {
    return _HeadGenInstList;
  }

  /**
   * Sets and returns the attribute fiacre.types.GenInsts
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.GenInst which just has been set
   */
  @Override
  public fiacre.types.GenInsts setHeadGenInstList(fiacre.types.GenInst set_arg) {
    return make(set_arg, _TailGenInstList);
  }
  
  /**
   * Returns the attribute fiacre.types.GenInsts
   *
   * @return the attribute fiacre.types.GenInsts
   */
  @Override
  public fiacre.types.GenInsts getTailGenInstList() {
    return _TailGenInstList;
  }

  /**
   * Sets and returns the attribute fiacre.types.GenInsts
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.GenInsts which just has been set
   */
  @Override
  public fiacre.types.GenInsts setTailGenInstList(fiacre.types.GenInsts set_arg) {
    return make(_HeadGenInstList, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getHeadGenInstList().toATerm(), getTailGenInstList().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.GenInsts from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.GenInsts fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.GenInst.fromTerm(appl.getArgument(0),atConv), fiacre.types.GenInsts.fromTerm(appl.getArgument(1),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 2;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadGenInstList;
      case 1: return _TailGenInstList;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.GenInst) v, _TailGenInstList);
      case 1: return make(_HeadGenInstList, (fiacre.types.GenInsts) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 2
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 2  && childs[0] instanceof fiacre.types.GenInst && childs[1] instanceof fiacre.types.GenInsts) {
      return make((fiacre.types.GenInst) childs[0], (fiacre.types.GenInsts) childs[1]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _HeadGenInstList,  _TailGenInstList };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-826310165<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_HeadGenInstList.hashCode() << 8);
    a += (_TailGenInstList.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
