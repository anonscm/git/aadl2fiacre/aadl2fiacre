
package fiacre.types.conditionalstatementlist;



public abstract class CondStList extends fiacre.types.ConditionalStatementList implements java.util.Collection<fiacre.types.ConditionalStatement>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
      fiacre.types.ConditionalStatementList tl = this.getTailCondStList();
      if (tl instanceof CondStList) {
        return 1+((CondStList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.ConditionalStatementList fromArray(fiacre.types.ConditionalStatement[] array) {
    fiacre.types.ConditionalStatementList res = fiacre.types.conditionalstatementlist.EmptyCondStList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.conditionalstatementlist.ConsCondStList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.ConditionalStatementList reverse() {
    if(this instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
      fiacre.types.ConditionalStatementList cur = this;
      fiacre.types.ConditionalStatementList rev = fiacre.types.conditionalstatementlist.EmptyCondStList.make();
      while(cur instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
        rev = fiacre.types.conditionalstatementlist.ConsCondStList.make(cur.getHeadCondStList(),rev);
        cur = cur.getTailCondStList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.ConditionalStatementList append(fiacre.types.ConditionalStatement element) {
    if(this instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
      fiacre.types.ConditionalStatementList tl = this.getTailCondStList();
      if (tl instanceof CondStList) {
        return fiacre.types.conditionalstatementlist.ConsCondStList.make(this.getHeadCondStList(),((CondStList)tl).append(element));
      } else {

        return fiacre.types.conditionalstatementlist.ConsCondStList.make(this.getHeadCondStList(),fiacre.types.conditionalstatementlist.ConsCondStList.make(element,tl));

      }
    } else {
      return fiacre.types.conditionalstatementlist.ConsCondStList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("CondStList(");
    if(this instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
      fiacre.types.ConditionalStatementList cur = this;
      while(cur instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
        fiacre.types.ConditionalStatement elem = cur.getHeadCondStList();
        cur = cur.getTailCondStList();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.conditionalstatementlist.EmptyCondStList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
      fiacre.types.ConditionalStatementList tail = this.getTailCondStList();
      res = atermFactory.makeList(getHeadCondStList().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.ConditionalStatementList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.ConditionalStatementList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("CondStList".equals(appl.getName())) {
        fiacre.types.ConditionalStatementList res = fiacre.types.conditionalstatementlist.EmptyCondStList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.ConditionalStatement elem = fiacre.types.ConditionalStatement.fromTerm(array[i],atConv);
          res = fiacre.types.conditionalstatementlist.ConsCondStList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.ConditionalStatementList res = fiacre.types.conditionalstatementlist.EmptyCondStList.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.ConditionalStatement elem = fiacre.types.ConditionalStatement.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.conditionalstatementlist.ConsCondStList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.ConditionalStatementList contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.ConditionalStatementList contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.ConditionalStatementList cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
      while(cur instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
        if( o.equals(cur.getHeadCondStList()) ) {
          return true;
        }
        cur = cur.getTailCondStList();
      }
      if(!(cur instanceof fiacre.types.conditionalstatementlist.EmptyCondStList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyCondStList() ; }

  public java.util.Iterator<fiacre.types.ConditionalStatement> iterator() {
    return new java.util.Iterator<fiacre.types.ConditionalStatement>() {
      fiacre.types.ConditionalStatementList list = CondStList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyCondStList();
      }

      public fiacre.types.ConditionalStatement next() {
        if(list.isEmptyCondStList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsCondStList()) {
          fiacre.types.ConditionalStatement head = list.getHeadCondStList();
          list = list.getTailCondStList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.ConditionalStatement)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.ConditionalStatement o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.ConditionalStatement> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
      fiacre.types.ConditionalStatementList cur = this;
      while(cur instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
        fiacre.types.ConditionalStatement elem = cur.getHeadCondStList();
        array[i] = elem;
        cur = cur.getTailCondStList();
        i++;
      }
      if(!(cur instanceof fiacre.types.conditionalstatementlist.EmptyCondStList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
      fiacre.types.ConditionalStatementList cur = this;
      while(cur instanceof fiacre.types.conditionalstatementlist.ConsCondStList) {
        fiacre.types.ConditionalStatement elem = cur.getHeadCondStList();
        array[i] = (T)elem;
        cur = cur.getTailCondStList();
        i++;
      }
      if(!(cur instanceof fiacre.types.conditionalstatementlist.EmptyCondStList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.ConditionalStatement> getCollection() {
    return new CollectionCondStList(this);
  }

  public java.util.Collection<fiacre.types.ConditionalStatement> getCollectionCondStList() {
    return new CollectionCondStList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionCondStList implements java.util.Collection<fiacre.types.ConditionalStatement> {
    private CondStList list;

    public CondStList getConditionalStatementList() {
      return list;
    }

    public CollectionCondStList(CondStList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.ConditionalStatement> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.ConditionalStatement> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getConditionalStatementList().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getConditionalStatementList().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getConditionalStatementList().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getConditionalStatementList().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.ConditionalStatement> iterator() {
    return getConditionalStatementList().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getConditionalStatementList().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getConditionalStatementList().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getConditionalStatementList().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getConditionalStatementList().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.ConditionalStatement o) {
      list = (CondStList)fiacre.types.conditionalstatementlist.ConsCondStList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (CondStList)fiacre.types.conditionalstatementlist.EmptyCondStList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyCondStList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
