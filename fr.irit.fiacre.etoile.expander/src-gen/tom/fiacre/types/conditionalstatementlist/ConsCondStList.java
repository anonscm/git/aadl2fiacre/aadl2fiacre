
package fiacre.types.conditionalstatementlist;



public final class ConsCondStList extends fiacre.types.conditionalstatementlist.CondStList implements tom.library.sl.Visitable  {
  
  private static String symbolName = "ConsCondStList";


  private ConsCondStList() {}
  private int hashCode;
  private static ConsCondStList gomProto = new ConsCondStList();
    private fiacre.types.ConditionalStatement _HeadCondStList;
  private fiacre.types.ConditionalStatementList _TailCondStList;

  /**
   * Constructor that builds a term rooted by ConsCondStList
   *
   * @return a term rooted by ConsCondStList
   */

  public static ConsCondStList make(fiacre.types.ConditionalStatement _HeadCondStList, fiacre.types.ConditionalStatementList _TailCondStList) {

    // use the proto as a model
    gomProto.initHashCode( _HeadCondStList,  _TailCondStList);
    return (ConsCondStList) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadCondStList
   * @param _TailCondStList
   * @param hashCode hashCode of ConsCondStList
   */
  private void init(fiacre.types.ConditionalStatement _HeadCondStList, fiacre.types.ConditionalStatementList _TailCondStList, int hashCode) {
    this._HeadCondStList = _HeadCondStList;
    this._TailCondStList = _TailCondStList;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadCondStList
   * @param _TailCondStList
   */
  private void initHashCode(fiacre.types.ConditionalStatement _HeadCondStList, fiacre.types.ConditionalStatementList _TailCondStList) {
    this._HeadCondStList = _HeadCondStList;
    this._TailCondStList = _TailCondStList;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "ConsCondStList";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 2;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    ConsCondStList clone = new ConsCondStList();
    clone.init( _HeadCondStList,  _TailCondStList, hashCode);
    return clone;
  }
  

  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    ConsCondStList tco = (ConsCondStList) ao;
    int _HeadCondStListCmp = (this._HeadCondStList).compareToLPO(tco._HeadCondStList);
    if(_HeadCondStListCmp != 0) {
      return _HeadCondStListCmp;
    }

    int _TailCondStListCmp = (this._TailCondStList).compareToLPO(tco._TailCondStList);
    if(_TailCondStListCmp != 0) {
      return _TailCondStListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    ConsCondStList tco = (ConsCondStList) ao;
    int _HeadCondStListCmp = (this._HeadCondStList).compareTo(tco._HeadCondStList);
    if(_HeadCondStListCmp != 0) {
      return _HeadCondStListCmp;
    }

    int _TailCondStListCmp = (this._TailCondStList).compareTo(tco._TailCondStList);
    if(_TailCondStListCmp != 0) {
      return _TailCondStListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a ConsCondStList and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof ConsCondStList) {

      ConsCondStList peer = (ConsCondStList) obj;
      return _HeadCondStList==peer._HeadCondStList && _TailCondStList==peer._TailCondStList && true;
    }
    return false;
  }


   //ConditionalStatementList interface
  /**
   * Returns true if the term is rooted by the symbol ConsCondStList
   *
   * @return true, because this is rooted by ConsCondStList
   */
  @Override
  public boolean isConsCondStList() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.ConditionalStatement
   *
   * @return the attribute fiacre.types.ConditionalStatement
   */
  @Override
  public fiacre.types.ConditionalStatement getHeadCondStList() {
    return _HeadCondStList;
  }

  /**
   * Sets and returns the attribute fiacre.types.ConditionalStatementList
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.ConditionalStatement which just has been set
   */
  @Override
  public fiacre.types.ConditionalStatementList setHeadCondStList(fiacre.types.ConditionalStatement set_arg) {
    return make(set_arg, _TailCondStList);
  }
  
  /**
   * Returns the attribute fiacre.types.ConditionalStatementList
   *
   * @return the attribute fiacre.types.ConditionalStatementList
   */
  @Override
  public fiacre.types.ConditionalStatementList getTailCondStList() {
    return _TailCondStList;
  }

  /**
   * Sets and returns the attribute fiacre.types.ConditionalStatementList
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.ConditionalStatementList which just has been set
   */
  @Override
  public fiacre.types.ConditionalStatementList setTailCondStList(fiacre.types.ConditionalStatementList set_arg) {
    return make(_HeadCondStList, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getHeadCondStList().toATerm(), getTailCondStList().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.ConditionalStatementList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.ConditionalStatementList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.ConditionalStatement.fromTerm(appl.getArgument(0),atConv), fiacre.types.ConditionalStatementList.fromTerm(appl.getArgument(1),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 2;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadCondStList;
      case 1: return _TailCondStList;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.ConditionalStatement) v, _TailCondStList);
      case 1: return make(_HeadCondStList, (fiacre.types.ConditionalStatementList) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 2
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 2  && childs[0] instanceof fiacre.types.ConditionalStatement && childs[1] instanceof fiacre.types.ConditionalStatementList) {
      return make((fiacre.types.ConditionalStatement) childs[0], (fiacre.types.ConditionalStatementList) childs[1]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _HeadCondStList,  _TailCondStList };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (568383682<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_HeadCondStList.hashCode() << 8);
    a += (_TailCondStList.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
