
package fiacre.types;


public abstract class Statement extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected Statement() {}



  /**
   * Returns true if the term is rooted by the symbol StNull
   *
   * @return true if the term is rooted by the symbol StNull
   */
  public boolean isStNull() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StTag
   *
   * @return true if the term is rooted by the symbol StTag
   */
  public boolean isStTag() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StCond
   *
   * @return true if the term is rooted by the symbol StCond
   */
  public boolean isStCond() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StSelect
   *
   * @return true if the term is rooted by the symbol StSelect
   */
  public boolean isStSelect() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StSelectIndexe
   *
   * @return true if the term is rooted by the symbol StSelectIndexe
   */
  public boolean isStSelectIndexe() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StSequence
   *
   * @return true if the term is rooted by the symbol StSequence
   */
  public boolean isStSequence() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StTo
   *
   * @return true if the term is rooted by the symbol StTo
   */
  public boolean isStTo() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StWhile
   *
   * @return true if the term is rooted by the symbol StWhile
   */
  public boolean isStWhile() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StAssign
   *
   * @return true if the term is rooted by the symbol StAssign
   */
  public boolean isStAssign() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StAny
   *
   * @return true if the term is rooted by the symbol StAny
   */
  public boolean isStAny() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StSignal
   *
   * @return true if the term is rooted by the symbol StSignal
   */
  public boolean isStSignal() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StInput
   *
   * @return true if the term is rooted by the symbol StInput
   */
  public boolean isStInput() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StOutput
   *
   * @return true if the term is rooted by the symbol StOutput
   */
  public boolean isStOutput() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StCase
   *
   * @return true if the term is rooted by the symbol StCase
   */
  public boolean isStCase() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StForEach
   *
   * @return true if the term is rooted by the symbol StForEach
   */
  public boolean isStForEach() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StLoop
   *
   * @return true if the term is rooted by the symbol StLoop
   */
  public boolean isStLoop() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StOn
   *
   * @return true if the term is rooted by the symbol StOn
   */
  public boolean isStOn() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StWait
   *
   * @return true if the term is rooted by the symbol StWait
   */
  public boolean isStWait() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot ost
   *
   * @return the subterm corresponding to the slot ost
   */
  public fiacre.types.Optional_Statement getost() {
    throw new UnsupportedOperationException("This Statement has no ost");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot ost
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot ost is replaced by _arg
   */
  public Statement setost(fiacre.types.Optional_Statement _arg) {
    throw new UnsupportedOperationException("This Statement has no ost");
  }

  /**
   * Returns the subterm corresponding to the slot e
   *
   * @return the subterm corresponding to the slot e
   */
  public fiacre.types.Exp gete() {
    throw new UnsupportedOperationException("This Statement has no e");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot e
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot e is replaced by _arg
   */
  public Statement sete(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Statement has no e");
  }

  /**
   * Returns the subterm corresponding to the slot st
   *
   * @return the subterm corresponding to the slot st
   */
  public fiacre.types.Statement getst() {
    throw new UnsupportedOperationException("This Statement has no st");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot st
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot st is replaced by _arg
   */
  public Statement setst(fiacre.types.Statement _arg) {
    throw new UnsupportedOperationException("This Statement has no st");
  }

  /**
   * Returns the subterm corresponding to the slot stl
   *
   * @return the subterm corresponding to the slot stl
   */
  public fiacre.types.PStatementList getstl() {
    throw new UnsupportedOperationException("This Statement has no stl");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot stl
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot stl is replaced by _arg
   */
  public Statement setstl(fiacre.types.PStatementList _arg) {
    throw new UnsupportedOperationException("This Statement has no stl");
  }

  /**
   * Returns the subterm corresponding to the slot type
   *
   * @return the subterm corresponding to the slot type
   */
  public fiacre.types.Type gettype() {
    throw new UnsupportedOperationException("This Statement has no type");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot type
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot type is replaced by _arg
   */
  public Statement settype(fiacre.types.Type _arg) {
    throw new UnsupportedOperationException("This Statement has no type");
  }

  /**
   * Returns the subterm corresponding to the slot s1
   *
   * @return the subterm corresponding to the slot s1
   */
  public fiacre.types.Statement gets1() {
    throw new UnsupportedOperationException("This Statement has no s1");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot s1
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot s1 is replaced by _arg
   */
  public Statement sets1(fiacre.types.Statement _arg) {
    throw new UnsupportedOperationException("This Statement has no s1");
  }

  /**
   * Returns the subterm corresponding to the slot cond
   *
   * @return the subterm corresponding to the slot cond
   */
  public fiacre.types.Exp getcond() {
    throw new UnsupportedOperationException("This Statement has no cond");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot cond
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot cond is replaced by _arg
   */
  public Statement setcond(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Statement has no cond");
  }

  /**
   * Returns the subterm corresponding to the slot types
   *
   * @return the subterm corresponding to the slot types
   */
  public fiacre.types.TypeList gettypes() {
    throw new UnsupportedOperationException("This Statement has no types");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot types
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot types is replaced by _arg
   */
  public Statement settypes(fiacre.types.TypeList _arg) {
    throw new UnsupportedOperationException("This Statement has no types");
  }

  /**
   * Returns the subterm corresponding to the slot cel
   *
   * @return the subterm corresponding to the slot cel
   */
  public fiacre.types.CaseElementList getcel() {
    throw new UnsupportedOperationException("This Statement has no cel");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot cel
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot cel is replaced by _arg
   */
  public Statement setcel(fiacre.types.CaseElementList _arg) {
    throw new UnsupportedOperationException("This Statement has no cel");
  }

  /**
   * Returns the subterm corresponding to the slot p
   *
   * @return the subterm corresponding to the slot p
   */
  public fiacre.types.ExpList getp() {
    throw new UnsupportedOperationException("This Statement has no p");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot p
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot p is replaced by _arg
   */
  public Statement setp(fiacre.types.ExpList _arg) {
    throw new UnsupportedOperationException("This Statement has no p");
  }

  /**
   * Returns the subterm corresponding to the slot fin
   *
   * @return the subterm corresponding to the slot fin
   */
  public fiacre.types.Exp getfin() {
    throw new UnsupportedOperationException("This Statement has no fin");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot fin
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot fin is replaced by _arg
   */
  public Statement setfin(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Statement has no fin");
  }

  /**
   * Returns the subterm corresponding to the slot state
   *
   * @return the subterm corresponding to the slot state
   */
  public String getstate() {
    throw new UnsupportedOperationException("This Statement has no state");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot state
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot state is replaced by _arg
   */
  public Statement setstate(String _arg) {
    throw new UnsupportedOperationException("This Statement has no state");
  }

  /**
   * Returns the subterm corresponding to the slot var
   *
   * @return the subterm corresponding to the slot var
   */
  public String getvar() {
    throw new UnsupportedOperationException("This Statement has no var");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot var
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot var is replaced by _arg
   */
  public Statement setvar(String _arg) {
    throw new UnsupportedOperationException("This Statement has no var");
  }

  /**
   * Returns the subterm corresponding to the slot s2
   *
   * @return the subterm corresponding to the slot s2
   */
  public fiacre.types.Statement gets2() {
    throw new UnsupportedOperationException("This Statement has no s2");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot s2
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot s2 is replaced by _arg
   */
  public Statement sets2(fiacre.types.Statement _arg) {
    throw new UnsupportedOperationException("This Statement has no s2");
  }

  /**
   * Returns the subterm corresponding to the slot s
   *
   * @return the subterm corresponding to the slot s
   */
  public String gets() {
    throw new UnsupportedOperationException("This Statement has no s");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot s
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot s is replaced by _arg
   */
  public Statement sets(String _arg) {
    throw new UnsupportedOperationException("This Statement has no s");
  }

  /**
   * Returns the subterm corresponding to the slot chan
   *
   * @return the subterm corresponding to the slot chan
   */
  public fiacre.types.Channel getchan() {
    throw new UnsupportedOperationException("This Statement has no chan");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot chan
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot chan is replaced by _arg
   */
  public Statement setchan(fiacre.types.Channel _arg) {
    throw new UnsupportedOperationException("This Statement has no chan");
  }

  /**
   * Returns the subterm corresponding to the slot oe
   *
   * @return the subterm corresponding to the slot oe
   */
  public fiacre.types.Optional_Exp getoe() {
    throw new UnsupportedOperationException("This Statement has no oe");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot oe
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot oe is replaced by _arg
   */
  public Statement setoe(fiacre.types.Optional_Exp _arg) {
    throw new UnsupportedOperationException("This Statement has no oe");
  }

  /**
   * Returns the subterm corresponding to the slot condst
   *
   * @return the subterm corresponding to the slot condst
   */
  public fiacre.types.ConditionalStatementList getcondst() {
    throw new UnsupportedOperationException("This Statement has no condst");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot condst
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot condst is replaced by _arg
   */
  public Statement setcondst(fiacre.types.ConditionalStatementList _arg) {
    throw new UnsupportedOperationException("This Statement has no condst");
  }

  /**
   * Returns the subterm corresponding to the slot name
   *
   * @return the subterm corresponding to the slot name
   */
  public String getname() {
    throw new UnsupportedOperationException("This Statement has no name");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot name
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot name is replaced by _arg
   */
  public Statement setname(String _arg) {
    throw new UnsupportedOperationException("This Statement has no name");
  }

  /**
   * Returns the subterm corresponding to the slot elist
   *
   * @return the subterm corresponding to the slot elist
   */
  public fiacre.types.ExpList getelist() {
    throw new UnsupportedOperationException("This Statement has no elist");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot elist
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot elist is replaced by _arg
   */
  public Statement setelist(fiacre.types.ExpList _arg) {
    throw new UnsupportedOperationException("This Statement has no elist");
  }

  /**
   * Returns the subterm corresponding to the slot f
   *
   * @return the subterm corresponding to the slot f
   */
  public fiacre.types.Bound getf() {
    throw new UnsupportedOperationException("This Statement has no f");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot f
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot f is replaced by _arg
   */
  public Statement setf(fiacre.types.Bound _arg) {
    throw new UnsupportedOperationException("This Statement has no f");
  }

  /**
   * Returns the subterm corresponding to the slot po
   *
   * @return the subterm corresponding to the slot po
   */
  public fiacre.types.Exp getpo() {
    throw new UnsupportedOperationException("This Statement has no po");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot po
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot po is replaced by _arg
   */
  public Statement setpo(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Statement has no po");
  }

  /**
   * Returns the subterm corresponding to the slot d
   *
   * @return the subterm corresponding to the slot d
   */
  public fiacre.types.Bound getd() {
    throw new UnsupportedOperationException("This Statement has no d");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot d
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot d is replaced by _arg
   */
  public Statement setd(fiacre.types.Bound _arg) {
    throw new UnsupportedOperationException("This Statement has no d");
  }

  /**
   * Returns the subterm corresponding to the slot st1
   *
   * @return the subterm corresponding to the slot st1
   */
  public fiacre.types.Statement getst1() {
    throw new UnsupportedOperationException("This Statement has no st1");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot st1
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot st1 is replaced by _arg
   */
  public Statement setst1(fiacre.types.Statement _arg) {
    throw new UnsupportedOperationException("This Statement has no st1");
  }

  /**
   * Returns the subterm corresponding to the slot deb
   *
   * @return the subterm corresponding to the slot deb
   */
  public fiacre.types.Exp getdeb() {
    throw new UnsupportedOperationException("This Statement has no deb");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot deb
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot deb is replaced by _arg
   */
  public Statement setdeb(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Statement has no deb");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.Statement from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.Statement fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.Statement from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.Statement fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.Statement from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.Statement fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.Statement
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.Statement fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.Statement tmp;
    java.util.ArrayList<fiacre.types.Statement> results = new java.util.ArrayList<fiacre.types.Statement>();

    tmp = fiacre.types.statement.StNull.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StTag.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StCond.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StSelect.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StSelectIndexe.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StSequence.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StTo.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StWhile.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StAssign.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StAny.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StSignal.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StInput.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StOutput.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StCase.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StForEach.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StLoop.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StOn.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statement.StWait.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a Statement");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("Statement").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.Statement", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Statement from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Statement fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.Statement from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Statement fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.Statement reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
