
package fiacre.types;


public abstract class Declaration extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected Declaration() {}



  /**
   * Returns true if the term is rooted by the symbol TypeDecl
   *
   * @return true if the term is rooted by the symbol TypeDecl
   */
  public boolean isTypeDecl() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ChannelDecl
   *
   * @return true if the term is rooted by the symbol ChannelDecl
   */
  public boolean isChannelDecl() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ConstantDecl
   *
   * @return true if the term is rooted by the symbol ConstantDecl
   */
  public boolean isConstantDecl() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ProcessDecl
   *
   * @return true if the term is rooted by the symbol ProcessDecl
   */
  public boolean isProcessDecl() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ExtendedProcessDecl
   *
   * @return true if the term is rooted by the symbol ExtendedProcessDecl
   */
  public boolean isExtendedProcessDecl() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ComponentDecl
   *
   * @return true if the term is rooted by the symbol ComponentDecl
   */
  public boolean isComponentDecl() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol Main
   *
   * @return true if the term is rooted by the symbol Main
   */
  public boolean isMain() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropertyDecl
   *
   * @return true if the term is rooted by the symbol PropertyDecl
   */
  public boolean isPropertyDecl() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol Assert
   *
   * @return true if the term is rooted by the symbol Assert
   */
  public boolean isAssert() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot params
   *
   * @return the subterm corresponding to the slot params
   */
  public fiacre.types.ParamDecls getparams() {
    throw new UnsupportedOperationException("This Declaration has no params");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot params
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot params is replaced by _arg
   */
  public Declaration setparams(fiacre.types.ParamDecls _arg) {
    throw new UnsupportedOperationException("This Declaration has no params");
  }

  /**
   * Returns the subterm corresponding to the slot states
   *
   * @return the subterm corresponding to the slot states
   */
  public fiacre.types.StringList getstates() {
    throw new UnsupportedOperationException("This Declaration has no states");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot states
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot states is replaced by _arg
   */
  public Declaration setstates(fiacre.types.StringList _arg) {
    throw new UnsupportedOperationException("This Declaration has no states");
  }

  /**
   * Returns the subterm corresponding to the slot bodyC
   *
   * @return the subterm corresponding to the slot bodyC
   */
  public fiacre.types.Channel getbodyC() {
    throw new UnsupportedOperationException("This Declaration has no bodyC");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot bodyC
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot bodyC is replaced by _arg
   */
  public Declaration setbodyC(fiacre.types.Channel _arg) {
    throw new UnsupportedOperationException("This Declaration has no bodyC");
  }

  /**
   * Returns the subterm corresponding to the slot iff
   *
   * @return the subterm corresponding to the slot iff
   */
  public String getiff() {
    throw new UnsupportedOperationException("This Declaration has no iff");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot iff
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot iff is replaced by _arg
   */
  public Declaration setiff(String _arg) {
    throw new UnsupportedOperationException("This Declaration has no iff");
  }

  /**
   * Returns the subterm corresponding to the slot lpd
   *
   * @return the subterm corresponding to the slot lpd
   */
  public fiacre.types.LPDList getlpd() {
    throw new UnsupportedOperationException("This Declaration has no lpd");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot lpd
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot lpd is replaced by _arg
   */
  public Declaration setlpd(fiacre.types.LPDList _arg) {
    throw new UnsupportedOperationException("This Declaration has no lpd");
  }

  /**
   * Returns the subterm corresponding to the slot name
   *
   * @return the subterm corresponding to the slot name
   */
  public String getname() {
    throw new UnsupportedOperationException("This Declaration has no name");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot name
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot name is replaced by _arg
   */
  public Declaration setname(String _arg) {
    throw new UnsupportedOperationException("This Declaration has no name");
  }

  /**
   * Returns the subterm corresponding to the slot genericParams
   *
   * @return the subterm corresponding to the slot genericParams
   */
  public fiacre.types.StringList getgenericParams() {
    throw new UnsupportedOperationException("This Declaration has no genericParams");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot genericParams
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot genericParams is replaced by _arg
   */
  public Declaration setgenericParams(fiacre.types.StringList _arg) {
    throw new UnsupportedOperationException("This Declaration has no genericParams");
  }

  /**
   * Returns the subterm corresponding to the slot bodyT
   *
   * @return the subterm corresponding to the slot bodyT
   */
  public fiacre.types.Type getbodyT() {
    throw new UnsupportedOperationException("This Declaration has no bodyT");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot bodyT
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot bodyT is replaced by _arg
   */
  public Declaration setbodyT(fiacre.types.Type _arg) {
    throw new UnsupportedOperationException("This Declaration has no bodyT");
  }

  /**
   * Returns the subterm corresponding to the slot bodyCmp
   *
   * @return the subterm corresponding to the slot bodyCmp
   */
  public fiacre.types.Composition getbodyCmp() {
    throw new UnsupportedOperationException("This Declaration has no bodyCmp");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot bodyCmp
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot bodyCmp is replaced by _arg
   */
  public Declaration setbodyCmp(fiacre.types.Composition _arg) {
    throw new UnsupportedOperationException("This Declaration has no bodyCmp");
  }

  /**
   * Returns the subterm corresponding to the slot def
   *
   * @return the subterm corresponding to the slot def
   */
  public fiacre.types.Property getdef() {
    throw new UnsupportedOperationException("This Declaration has no def");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot def
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot def is replaced by _arg
   */
  public Declaration setdef(fiacre.types.Property _arg) {
    throw new UnsupportedOperationException("This Declaration has no def");
  }

  /**
   * Returns the subterm corresponding to the slot prios
   *
   * @return the subterm corresponding to the slot prios
   */
  public fiacre.types.PrioDeclList getprios() {
    throw new UnsupportedOperationException("This Declaration has no prios");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot prios
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot prios is replaced by _arg
   */
  public Declaration setprios(fiacre.types.PrioDeclList _arg) {
    throw new UnsupportedOperationException("This Declaration has no prios");
  }

  /**
   * Returns the subterm corresponding to the slot type
   *
   * @return the subterm corresponding to the slot type
   */
  public fiacre.types.Type gettype() {
    throw new UnsupportedOperationException("This Declaration has no type");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot type
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot type is replaced by _arg
   */
  public Declaration settype(fiacre.types.Type _arg) {
    throw new UnsupportedOperationException("This Declaration has no type");
  }

  /**
   * Returns the subterm corresponding to the slot trans
   *
   * @return the subterm corresponding to the slot trans
   */
  public fiacre.types.Transitions gettrans() {
    throw new UnsupportedOperationException("This Declaration has no trans");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot trans
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot trans is replaced by _arg
   */
  public Declaration settrans(fiacre.types.Transitions _arg) {
    throw new UnsupportedOperationException("This Declaration has no trans");
  }

  /**
   * Returns the subterm corresponding to the slot main
   *
   * @return the subterm corresponding to the slot main
   */
  public String getmain() {
    throw new UnsupportedOperationException("This Declaration has no main");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot main
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot main is replaced by _arg
   */
  public Declaration setmain(String _arg) {
    throw new UnsupportedOperationException("This Declaration has no main");
  }

  /**
   * Returns the subterm corresponding to the slot bodyE
   *
   * @return the subterm corresponding to the slot bodyE
   */
  public fiacre.types.Exp getbodyE() {
    throw new UnsupportedOperationException("This Declaration has no bodyE");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot bodyE
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot bodyE is replaced by _arg
   */
  public Declaration setbodyE(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Declaration has no bodyE");
  }

  /**
   * Returns the subterm corresponding to the slot init
   *
   * @return the subterm corresponding to the slot init
   */
  public fiacre.types.Init getinit() {
    throw new UnsupportedOperationException("This Declaration has no init");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot init
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot init is replaced by _arg
   */
  public Declaration setinit(fiacre.types.Init _arg) {
    throw new UnsupportedOperationException("This Declaration has no init");
  }

  /**
   * Returns the subterm corresponding to the slot ift
   *
   * @return the subterm corresponding to the slot ift
   */
  public String getift() {
    throw new UnsupportedOperationException("This Declaration has no ift");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot ift
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot ift is replaced by _arg
   */
  public Declaration setift(String _arg) {
    throw new UnsupportedOperationException("This Declaration has no ift");
  }

  /**
   * Returns the subterm corresponding to the slot ports
   *
   * @return the subterm corresponding to the slot ports
   */
  public fiacre.types.PortDecls getports() {
    throw new UnsupportedOperationException("This Declaration has no ports");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot ports
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot ports is replaced by _arg
   */
  public Declaration setports(fiacre.types.PortDecls _arg) {
    throw new UnsupportedOperationException("This Declaration has no ports");
  }

  /**
   * Returns the subterm corresponding to the slot vars
   *
   * @return the subterm corresponding to the slot vars
   */
  public fiacre.types.VarDecls getvars() {
    throw new UnsupportedOperationException("This Declaration has no vars");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot vars
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot vars is replaced by _arg
   */
  public Declaration setvars(fiacre.types.VarDecls _arg) {
    throw new UnsupportedOperationException("This Declaration has no vars");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.Declaration from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.Declaration fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.Declaration from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.Declaration fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.Declaration from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.Declaration fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.Declaration
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.Declaration fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.Declaration tmp;
    java.util.ArrayList<fiacre.types.Declaration> results = new java.util.ArrayList<fiacre.types.Declaration>();

    tmp = fiacre.types.declaration.TypeDecl.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.declaration.ChannelDecl.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.declaration.ConstantDecl.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.declaration.ProcessDecl.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.declaration.ExtendedProcessDecl.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.declaration.ComponentDecl.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.declaration.Main.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.declaration.PropertyDecl.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.declaration.Assert.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a Declaration");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("Declaration").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.Declaration", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Declaration from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Declaration fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.Declaration from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Declaration fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.Declaration reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
