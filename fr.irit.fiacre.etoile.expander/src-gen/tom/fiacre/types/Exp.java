
package fiacre.types;


public abstract class Exp extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected Exp() {}



  /**
   * Returns true if the term is rooted by the symbol ParenExp
   *
   * @return true if the term is rooted by the symbol ParenExp
   */
  public boolean isParenExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol CondExp
   *
   * @return true if the term is rooted by the symbol CondExp
   */
  public boolean isCondExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol Unop
   *
   * @return true if the term is rooted by the symbol Unop
   */
  public boolean isUnop() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol Binop
   *
   * @return true if the term is rooted by the symbol Binop
   */
  public boolean isBinop() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol Infix
   *
   * @return true if the term is rooted by the symbol Infix
   */
  public boolean isInfix() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol RecordExp
   *
   * @return true if the term is rooted by the symbol RecordExp
   */
  public boolean isRecordExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol QueueExp
   *
   * @return true if the term is rooted by the symbol QueueExp
   */
  public boolean isQueueExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ArrayExp
   *
   * @return true if the term is rooted by the symbol ArrayExp
   */
  public boolean isArrayExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol RefExp
   *
   * @return true if the term is rooted by the symbol RefExp
   */
  public boolean isRefExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ConstrExp
   *
   * @return true if the term is rooted by the symbol ConstrExp
   */
  public boolean isConstrExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol IntExp
   *
   * @return true if the term is rooted by the symbol IntExp
   */
  public boolean isIntExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol BoolExp
   *
   * @return true if the term is rooted by the symbol BoolExp
   */
  public boolean isBoolExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol IdentExp
   *
   * @return true if the term is rooted by the symbol IdentExp
   */
  public boolean isIdentExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol AnyExp
   *
   * @return true if the term is rooted by the symbol AnyExp
   */
  public boolean isAnyExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ArrayAccessExp
   *
   * @return true if the term is rooted by the symbol ArrayAccessExp
   */
  public boolean isArrayAccessExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol RecordAccessExp
   *
   * @return true if the term is rooted by the symbol RecordAccessExp
   */
  public boolean isRecordAccessExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol InitListExp
   *
   * @return true if the term is rooted by the symbol InitListExp
   */
  public boolean isInitListExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol MultExp
   *
   * @return true if the term is rooted by the symbol MultExp
   */
  public boolean isMultExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ChanProjExp
   *
   * @return true if the term is rooted by the symbol ChanProjExp
   */
  public boolean isChanProjExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol AllExp
   *
   * @return true if the term is rooted by the symbol AllExp
   */
  public boolean isAllExp() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ExistsExp
   *
   * @return true if the term is rooted by the symbol ExistsExp
   */
  public boolean isExistsExp() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot st1
   *
   * @return the subterm corresponding to the slot st1
   */
  public fiacre.types.Exp getst1() {
    throw new UnsupportedOperationException("This Exp has no st1");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot st1
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot st1 is replaced by _arg
   */
  public Exp setst1(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Exp has no st1");
  }

  /**
   * Returns the subterm corresponding to the slot e
   *
   * @return the subterm corresponding to the slot e
   */
  public fiacre.types.Exp gete() {
    throw new UnsupportedOperationException("This Exp has no e");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot e
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot e is replaced by _arg
   */
  public Exp sete(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Exp has no e");
  }

  /**
   * Returns the subterm corresponding to the slot ref
   *
   * @return the subterm corresponding to the slot ref
   */
  public String getref() {
    throw new UnsupportedOperationException("This Exp has no ref");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot ref
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot ref is replaced by _arg
   */
  public Exp setref(String _arg) {
    throw new UnsupportedOperationException("This Exp has no ref");
  }

  /**
   * Returns the subterm corresponding to the slot record
   *
   * @return the subterm corresponding to the slot record
   */
  public fiacre.types.Exp getrecord() {
    throw new UnsupportedOperationException("This Exp has no record");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot record
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot record is replaced by _arg
   */
  public Exp setrecord(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Exp has no record");
  }

  /**
   * Returns the subterm corresponding to the slot type
   *
   * @return the subterm corresponding to the slot type
   */
  public fiacre.types.Type gettype() {
    throw new UnsupportedOperationException("This Exp has no type");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot type
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot type is replaced by _arg
   */
  public Exp settype(fiacre.types.Type _arg) {
    throw new UnsupportedOperationException("This Exp has no type");
  }

  /**
   * Returns the subterm corresponding to the slot t
   *
   * @return the subterm corresponding to the slot t
   */
  public fiacre.types.Type gett() {
    throw new UnsupportedOperationException("This Exp has no t");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot t
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot t is replaced by _arg
   */
  public Exp sett(fiacre.types.Type _arg) {
    throw new UnsupportedOperationException("This Exp has no t");
  }

  /**
   * Returns the subterm corresponding to the slot kind
   *
   * @return the subterm corresponding to the slot kind
   */
  public fiacre.types.Kind getkind() {
    throw new UnsupportedOperationException("This Exp has no kind");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot kind
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot kind is replaced by _arg
   */
  public Exp setkind(fiacre.types.Kind _arg) {
    throw new UnsupportedOperationException("This Exp has no kind");
  }

  /**
   * Returns the subterm corresponding to the slot e1
   *
   * @return the subterm corresponding to the slot e1
   */
  public fiacre.types.Exp gete1() {
    throw new UnsupportedOperationException("This Exp has no e1");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot e1
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot e1 is replaced by _arg
   */
  public Exp sete1(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Exp has no e1");
  }

  /**
   * Returns the subterm corresponding to the slot b
   *
   * @return the subterm corresponding to the slot b
   */
  public boolean getb() {
    throw new UnsupportedOperationException("This Exp has no b");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot b
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot b is replaced by _arg
   */
  public Exp setb(boolean _arg) {
    throw new UnsupportedOperationException("This Exp has no b");
  }

  /**
   * Returns the subterm corresponding to the slot i
   *
   * @return the subterm corresponding to the slot i
   */
  public String geti() {
    throw new UnsupportedOperationException("This Exp has no i");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot i
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot i is replaced by _arg
   */
  public Exp seti(String _arg) {
    throw new UnsupportedOperationException("This Exp has no i");
  }

  /**
   * Returns the subterm corresponding to the slot queue
   *
   * @return the subterm corresponding to the slot queue
   */
  public fiacre.types.ExpList getqueue() {
    throw new UnsupportedOperationException("This Exp has no queue");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot queue
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot queue is replaced by _arg
   */
  public Exp setqueue(fiacre.types.ExpList _arg) {
    throw new UnsupportedOperationException("This Exp has no queue");
  }

  /**
   * Returns the subterm corresponding to the slot iop
   *
   * @return the subterm corresponding to the slot iop
   */
  public fiacre.types.Infix getiop() {
    throw new UnsupportedOperationException("This Exp has no iop");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot iop
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot iop is replaced by _arg
   */
  public Exp setiop(fiacre.types.Infix _arg) {
    throw new UnsupportedOperationException("This Exp has no iop");
  }

  /**
   * Returns the subterm corresponding to the slot bop
   *
   * @return the subterm corresponding to the slot bop
   */
  public fiacre.types.Binop getbop() {
    throw new UnsupportedOperationException("This Exp has no bop");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot bop
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot bop is replaced by _arg
   */
  public Exp setbop(fiacre.types.Binop _arg) {
    throw new UnsupportedOperationException("This Exp has no bop");
  }

  /**
   * Returns the subterm corresponding to the slot chan
   *
   * @return the subterm corresponding to the slot chan
   */
  public fiacre.types.Exp getchan() {
    throw new UnsupportedOperationException("This Exp has no chan");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot chan
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot chan is replaced by _arg
   */
  public Exp setchan(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Exp has no chan");
  }

  /**
   * Returns the subterm corresponding to the slot id
   *
   * @return the subterm corresponding to the slot id
   */
  public String getid() {
    throw new UnsupportedOperationException("This Exp has no id");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot id
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot id is replaced by _arg
   */
  public Exp setid(String _arg) {
    throw new UnsupportedOperationException("This Exp has no id");
  }

  /**
   * Returns the subterm corresponding to the slot st2
   *
   * @return the subterm corresponding to the slot st2
   */
  public fiacre.types.Exp getst2() {
    throw new UnsupportedOperationException("This Exp has no st2");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot st2
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot st2 is replaced by _arg
   */
  public Exp setst2(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Exp has no st2");
  }

  /**
   * Returns the subterm corresponding to the slot var
   *
   * @return the subterm corresponding to the slot var
   */
  public String getvar() {
    throw new UnsupportedOperationException("This Exp has no var");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot var
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot var is replaced by _arg
   */
  public Exp setvar(String _arg) {
    throw new UnsupportedOperationException("This Exp has no var");
  }

  /**
   * Returns the subterm corresponding to the slot affs
   *
   * @return the subterm corresponding to the slot affs
   */
  public fiacre.types.AffectationList getaffs() {
    throw new UnsupportedOperationException("This Exp has no affs");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot affs
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot affs is replaced by _arg
   */
  public Exp setaffs(fiacre.types.AffectationList _arg) {
    throw new UnsupportedOperationException("This Exp has no affs");
  }

  /**
   * Returns the subterm corresponding to the slot fields
   *
   * @return the subterm corresponding to the slot fields
   */
  public fiacre.types.ExpList getfields() {
    throw new UnsupportedOperationException("This Exp has no fields");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot fields
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot fields is replaced by _arg
   */
  public Exp setfields(fiacre.types.ExpList _arg) {
    throw new UnsupportedOperationException("This Exp has no fields");
  }

  /**
   * Returns the subterm corresponding to the slot n
   *
   * @return the subterm corresponding to the slot n
   */
  public fiacre.types.Exp getn() {
    throw new UnsupportedOperationException("This Exp has no n");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot n
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot n is replaced by _arg
   */
  public Exp setn(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Exp has no n");
  }

  /**
   * Returns the subterm corresponding to the slot begin
   *
   * @return the subterm corresponding to the slot begin
   */
  public fiacre.types.Exp getbegin() {
    throw new UnsupportedOperationException("This Exp has no begin");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot begin
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot begin is replaced by _arg
   */
  public Exp setbegin(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Exp has no begin");
  }

  /**
   * Returns the subterm corresponding to the slot end
   *
   * @return the subterm corresponding to the slot end
   */
  public fiacre.types.Exp getend() {
    throw new UnsupportedOperationException("This Exp has no end");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot end
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot end is replaced by _arg
   */
  public Exp setend(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Exp has no end");
  }

  /**
   * Returns the subterm corresponding to the slot el
   *
   * @return the subterm corresponding to the slot el
   */
  public fiacre.types.ExpList getel() {
    throw new UnsupportedOperationException("This Exp has no el");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot el
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot el is replaced by _arg
   */
  public Exp setel(fiacre.types.ExpList _arg) {
    throw new UnsupportedOperationException("This Exp has no el");
  }

  /**
   * Returns the subterm corresponding to the slot uop
   *
   * @return the subterm corresponding to the slot uop
   */
  public fiacre.types.Unop getuop() {
    throw new UnsupportedOperationException("This Exp has no uop");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot uop
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot uop is replaced by _arg
   */
  public Exp setuop(fiacre.types.Unop _arg) {
    throw new UnsupportedOperationException("This Exp has no uop");
  }

  /**
   * Returns the subterm corresponding to the slot array
   *
   * @return the subterm corresponding to the slot array
   */
  public fiacre.types.Exp getarray() {
    throw new UnsupportedOperationException("This Exp has no array");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot array
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot array is replaced by _arg
   */
  public Exp setarray(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Exp has no array");
  }

  /**
   * Returns the subterm corresponding to the slot consrt
   *
   * @return the subterm corresponding to the slot consrt
   */
  public String getconsrt() {
    throw new UnsupportedOperationException("This Exp has no consrt");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot consrt
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot consrt is replaced by _arg
   */
  public Exp setconsrt(String _arg) {
    throw new UnsupportedOperationException("This Exp has no consrt");
  }

  /**
   * Returns the subterm corresponding to the slot e2
   *
   * @return the subterm corresponding to the slot e2
   */
  public fiacre.types.Exp gete2() {
    throw new UnsupportedOperationException("This Exp has no e2");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot e2
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot e2 is replaced by _arg
   */
  public Exp sete2(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Exp has no e2");
  }

  /**
   * Returns the subterm corresponding to the slot test
   *
   * @return the subterm corresponding to the slot test
   */
  public fiacre.types.Exp gettest() {
    throw new UnsupportedOperationException("This Exp has no test");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot test
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot test is replaced by _arg
   */
  public Exp settest(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Exp has no test");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.Exp from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.Exp fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.Exp from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.Exp fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.Exp from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.Exp fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.Exp
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.Exp fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.Exp tmp;
    java.util.ArrayList<fiacre.types.Exp> results = new java.util.ArrayList<fiacre.types.Exp>();

    tmp = fiacre.types.exp.ParenExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.CondExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.Unop.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.Binop.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.Infix.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.RecordExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.QueueExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.ArrayExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.RefExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.ConstrExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.IntExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.BoolExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.IdentExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.AnyExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.ArrayAccessExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.RecordAccessExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.InitListExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.MultExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.ChanProjExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.AllExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.exp.ExistsExp.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a Exp");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("Exp").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.Exp", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Exp from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Exp fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.Exp from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Exp fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.Exp reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
