
package fiacre.types;


public abstract class Infix extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected Infix() {}



  /**
   * Returns true if the term is rooted by the symbol AND
   *
   * @return true if the term is rooted by the symbol AND
   */
  public boolean isAND() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol OR
   *
   * @return true if the term is rooted by the symbol OR
   */
  public boolean isOR() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol LE
   *
   * @return true if the term is rooted by the symbol LE
   */
  public boolean isLE() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol LT
   *
   * @return true if the term is rooted by the symbol LT
   */
  public boolean isLT() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol GT
   *
   * @return true if the term is rooted by the symbol GT
   */
  public boolean isGT() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol GE
   *
   * @return true if the term is rooted by the symbol GE
   */
  public boolean isGE() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol EQ
   *
   * @return true if the term is rooted by the symbol EQ
   */
  public boolean isEQ() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol NE
   *
   * @return true if the term is rooted by the symbol NE
   */
  public boolean isNE() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ADD
   *
   * @return true if the term is rooted by the symbol ADD
   */
  public boolean isADD() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol SUB
   *
   * @return true if the term is rooted by the symbol SUB
   */
  public boolean isSUB() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol MOD
   *
   * @return true if the term is rooted by the symbol MOD
   */
  public boolean isMOD() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol MUL
   *
   * @return true if the term is rooted by the symbol MUL
   */
  public boolean isMUL() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol DIV
   *
   * @return true if the term is rooted by the symbol DIV
   */
  public boolean isDIV() {
    return false;
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.Infix from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.Infix fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.Infix from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.Infix fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.Infix from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.Infix fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.Infix
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.Infix fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.Infix tmp;
    java.util.ArrayList<fiacre.types.Infix> results = new java.util.ArrayList<fiacre.types.Infix>();

    tmp = fiacre.types.infix.AND.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.infix.OR.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.infix.LE.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.infix.LT.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.infix.GT.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.infix.GE.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.infix.EQ.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.infix.NE.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.infix.ADD.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.infix.SUB.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.infix.MOD.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.infix.MUL.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.infix.DIV.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a Infix");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("Infix").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.Infix", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Infix from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Infix fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.Infix from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Infix fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.Infix reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
