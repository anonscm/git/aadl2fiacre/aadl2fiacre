
package fiacre.types;


public abstract class CaseElementList extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected CaseElementList() {}



  /**
   * Returns true if the term is rooted by the symbol ConsCaseEL
   *
   * @return true if the term is rooted by the symbol ConsCaseEL
   */
  public boolean isConsCaseEL() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol EmptyCaseEL
   *
   * @return true if the term is rooted by the symbol EmptyCaseEL
   */
  public boolean isEmptyCaseEL() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot TailCaseEL
   *
   * @return the subterm corresponding to the slot TailCaseEL
   */
  public fiacre.types.CaseElementList getTailCaseEL() {
    throw new UnsupportedOperationException("This CaseElementList has no TailCaseEL");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot TailCaseEL
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot TailCaseEL is replaced by _arg
   */
  public CaseElementList setTailCaseEL(fiacre.types.CaseElementList _arg) {
    throw new UnsupportedOperationException("This CaseElementList has no TailCaseEL");
  }

  /**
   * Returns the subterm corresponding to the slot HeadCaseEL
   *
   * @return the subterm corresponding to the slot HeadCaseEL
   */
  public fiacre.types.CaseElement getHeadCaseEL() {
    throw new UnsupportedOperationException("This CaseElementList has no HeadCaseEL");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot HeadCaseEL
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot HeadCaseEL is replaced by _arg
   */
  public CaseElementList setHeadCaseEL(fiacre.types.CaseElement _arg) {
    throw new UnsupportedOperationException("This CaseElementList has no HeadCaseEL");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.CaseElementList from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.CaseElementList fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.CaseElementList from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.CaseElementList fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.CaseElementList from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.CaseElementList fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.CaseElementList
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.CaseElementList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.CaseElementList tmp;
    java.util.ArrayList<fiacre.types.CaseElementList> results = new java.util.ArrayList<fiacre.types.CaseElementList>();

    tmp = fiacre.types.caseelementlist.ConsCaseEL.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.caseelementlist.EmptyCaseEL.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.caseelementlist.CaseEL.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a CaseElementList");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("CaseElementList").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.CaseElementList", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.CaseElementList from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.CaseElementList fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.CaseElementList from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.CaseElementList fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.CaseElementList reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
  /**
   * Returns a Collection extracted from the term
   *
   * @return the collection
   * @throws UnsupportedOperationException if the term is not a list
   */
  public java.util.Collection<fiacre.types.CaseElement> getCollectionCaseEL() {
    throw new UnsupportedOperationException("This CaseElementList cannot be converted into a Collection");
  }
          
}
