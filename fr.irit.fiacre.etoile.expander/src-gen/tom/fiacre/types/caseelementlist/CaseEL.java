
package fiacre.types.caseelementlist;



public abstract class CaseEL extends fiacre.types.CaseElementList implements java.util.Collection<fiacre.types.CaseElement>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.caseelementlist.ConsCaseEL) {
      fiacre.types.CaseElementList tl = this.getTailCaseEL();
      if (tl instanceof CaseEL) {
        return 1+((CaseEL)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.CaseElementList fromArray(fiacre.types.CaseElement[] array) {
    fiacre.types.CaseElementList res = fiacre.types.caseelementlist.EmptyCaseEL.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.caseelementlist.ConsCaseEL.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.CaseElementList reverse() {
    if(this instanceof fiacre.types.caseelementlist.ConsCaseEL) {
      fiacre.types.CaseElementList cur = this;
      fiacre.types.CaseElementList rev = fiacre.types.caseelementlist.EmptyCaseEL.make();
      while(cur instanceof fiacre.types.caseelementlist.ConsCaseEL) {
        rev = fiacre.types.caseelementlist.ConsCaseEL.make(cur.getHeadCaseEL(),rev);
        cur = cur.getTailCaseEL();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.CaseElementList append(fiacre.types.CaseElement element) {
    if(this instanceof fiacre.types.caseelementlist.ConsCaseEL) {
      fiacre.types.CaseElementList tl = this.getTailCaseEL();
      if (tl instanceof CaseEL) {
        return fiacre.types.caseelementlist.ConsCaseEL.make(this.getHeadCaseEL(),((CaseEL)tl).append(element));
      } else {

        return fiacre.types.caseelementlist.ConsCaseEL.make(this.getHeadCaseEL(),fiacre.types.caseelementlist.ConsCaseEL.make(element,tl));

      }
    } else {
      return fiacre.types.caseelementlist.ConsCaseEL.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("CaseEL(");
    if(this instanceof fiacre.types.caseelementlist.ConsCaseEL) {
      fiacre.types.CaseElementList cur = this;
      while(cur instanceof fiacre.types.caseelementlist.ConsCaseEL) {
        fiacre.types.CaseElement elem = cur.getHeadCaseEL();
        cur = cur.getTailCaseEL();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.caseelementlist.ConsCaseEL) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.caseelementlist.EmptyCaseEL)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.caseelementlist.ConsCaseEL) {
      fiacre.types.CaseElementList tail = this.getTailCaseEL();
      res = atermFactory.makeList(getHeadCaseEL().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.CaseElementList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.CaseElementList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("CaseEL".equals(appl.getName())) {
        fiacre.types.CaseElementList res = fiacre.types.caseelementlist.EmptyCaseEL.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.CaseElement elem = fiacre.types.CaseElement.fromTerm(array[i],atConv);
          res = fiacre.types.caseelementlist.ConsCaseEL.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.CaseElementList res = fiacre.types.caseelementlist.EmptyCaseEL.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.CaseElement elem = fiacre.types.CaseElement.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.caseelementlist.ConsCaseEL.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.CaseElementList contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.CaseElementList contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.CaseElementList cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.caseelementlist.ConsCaseEL) {
      while(cur instanceof fiacre.types.caseelementlist.ConsCaseEL) {
        if( o.equals(cur.getHeadCaseEL()) ) {
          return true;
        }
        cur = cur.getTailCaseEL();
      }
      if(!(cur instanceof fiacre.types.caseelementlist.EmptyCaseEL)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyCaseEL() ; }

  public java.util.Iterator<fiacre.types.CaseElement> iterator() {
    return new java.util.Iterator<fiacre.types.CaseElement>() {
      fiacre.types.CaseElementList list = CaseEL.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyCaseEL();
      }

      public fiacre.types.CaseElement next() {
        if(list.isEmptyCaseEL()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsCaseEL()) {
          fiacre.types.CaseElement head = list.getHeadCaseEL();
          list = list.getTailCaseEL();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.CaseElement)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.CaseElement o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.CaseElement> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.caseelementlist.ConsCaseEL) {
      fiacre.types.CaseElementList cur = this;
      while(cur instanceof fiacre.types.caseelementlist.ConsCaseEL) {
        fiacre.types.CaseElement elem = cur.getHeadCaseEL();
        array[i] = elem;
        cur = cur.getTailCaseEL();
        i++;
      }
      if(!(cur instanceof fiacre.types.caseelementlist.EmptyCaseEL)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.caseelementlist.ConsCaseEL) {
      fiacre.types.CaseElementList cur = this;
      while(cur instanceof fiacre.types.caseelementlist.ConsCaseEL) {
        fiacre.types.CaseElement elem = cur.getHeadCaseEL();
        array[i] = (T)elem;
        cur = cur.getTailCaseEL();
        i++;
      }
      if(!(cur instanceof fiacre.types.caseelementlist.EmptyCaseEL)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.CaseElement> getCollection() {
    return new CollectionCaseEL(this);
  }

  public java.util.Collection<fiacre.types.CaseElement> getCollectionCaseEL() {
    return new CollectionCaseEL(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionCaseEL implements java.util.Collection<fiacre.types.CaseElement> {
    private CaseEL list;

    public CaseEL getCaseElementList() {
      return list;
    }

    public CollectionCaseEL(CaseEL list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.CaseElement> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.CaseElement> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getCaseElementList().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getCaseElementList().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getCaseElementList().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getCaseElementList().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.CaseElement> iterator() {
    return getCaseElementList().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getCaseElementList().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getCaseElementList().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getCaseElementList().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getCaseElementList().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.CaseElement o) {
      list = (CaseEL)fiacre.types.caseelementlist.ConsCaseEL.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (CaseEL)fiacre.types.caseelementlist.EmptyCaseEL.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyCaseEL();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
