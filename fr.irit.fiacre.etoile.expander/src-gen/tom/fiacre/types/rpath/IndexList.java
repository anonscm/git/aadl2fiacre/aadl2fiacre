
package fiacre.types.rpath;



public abstract class IndexList extends fiacre.types.RPath implements java.util.Collection<fiacre.types.CIndex>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.rpath.ConsIndexList) {
      fiacre.types.RPath tl = this.getTailIndexList();
      if (tl instanceof IndexList) {
        return 1+((IndexList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.RPath fromArray(fiacre.types.CIndex[] array) {
    fiacre.types.RPath res = fiacre.types.rpath.EmptyIndexList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.rpath.ConsIndexList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.RPath reverse() {
    if(this instanceof fiacre.types.rpath.ConsIndexList) {
      fiacre.types.RPath cur = this;
      fiacre.types.RPath rev = fiacre.types.rpath.EmptyIndexList.make();
      while(cur instanceof fiacre.types.rpath.ConsIndexList) {
        rev = fiacre.types.rpath.ConsIndexList.make(cur.getHeadIndexList(),rev);
        cur = cur.getTailIndexList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.RPath append(fiacre.types.CIndex element) {
    if(this instanceof fiacre.types.rpath.ConsIndexList) {
      fiacre.types.RPath tl = this.getTailIndexList();
      if (tl instanceof IndexList) {
        return fiacre.types.rpath.ConsIndexList.make(this.getHeadIndexList(),((IndexList)tl).append(element));
      } else {

        return fiacre.types.rpath.ConsIndexList.make(this.getHeadIndexList(),fiacre.types.rpath.ConsIndexList.make(element,tl));

      }
    } else {
      return fiacre.types.rpath.ConsIndexList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("IndexList(");
    if(this instanceof fiacre.types.rpath.ConsIndexList) {
      fiacre.types.RPath cur = this;
      while(cur instanceof fiacre.types.rpath.ConsIndexList) {
        fiacre.types.CIndex elem = cur.getHeadIndexList();
        cur = cur.getTailIndexList();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.rpath.ConsIndexList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.rpath.EmptyIndexList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.rpath.ConsIndexList) {
      fiacre.types.RPath tail = this.getTailIndexList();
      res = atermFactory.makeList(getHeadIndexList().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.RPath from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.RPath fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("IndexList".equals(appl.getName())) {
        fiacre.types.RPath res = fiacre.types.rpath.EmptyIndexList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.CIndex elem = fiacre.types.CIndex.fromTerm(array[i],atConv);
          res = fiacre.types.rpath.ConsIndexList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.RPath res = fiacre.types.rpath.EmptyIndexList.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.CIndex elem = fiacre.types.CIndex.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.rpath.ConsIndexList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.RPath contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.RPath contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.RPath cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.rpath.ConsIndexList) {
      while(cur instanceof fiacre.types.rpath.ConsIndexList) {
        if( o.equals(cur.getHeadIndexList()) ) {
          return true;
        }
        cur = cur.getTailIndexList();
      }
      if(!(cur instanceof fiacre.types.rpath.EmptyIndexList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyIndexList() ; }

  public java.util.Iterator<fiacre.types.CIndex> iterator() {
    return new java.util.Iterator<fiacre.types.CIndex>() {
      fiacre.types.RPath list = IndexList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyIndexList();
      }

      public fiacre.types.CIndex next() {
        if(list.isEmptyIndexList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsIndexList()) {
          fiacre.types.CIndex head = list.getHeadIndexList();
          list = list.getTailIndexList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.CIndex)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.CIndex o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.CIndex> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.rpath.ConsIndexList) {
      fiacre.types.RPath cur = this;
      while(cur instanceof fiacre.types.rpath.ConsIndexList) {
        fiacre.types.CIndex elem = cur.getHeadIndexList();
        array[i] = elem;
        cur = cur.getTailIndexList();
        i++;
      }
      if(!(cur instanceof fiacre.types.rpath.EmptyIndexList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.rpath.ConsIndexList) {
      fiacre.types.RPath cur = this;
      while(cur instanceof fiacre.types.rpath.ConsIndexList) {
        fiacre.types.CIndex elem = cur.getHeadIndexList();
        array[i] = (T)elem;
        cur = cur.getTailIndexList();
        i++;
      }
      if(!(cur instanceof fiacre.types.rpath.EmptyIndexList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.CIndex> getCollection() {
    return new CollectionIndexList(this);
  }

  public java.util.Collection<fiacre.types.CIndex> getCollectionIndexList() {
    return new CollectionIndexList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionIndexList implements java.util.Collection<fiacre.types.CIndex> {
    private IndexList list;

    public IndexList getRPath() {
      return list;
    }

    public CollectionIndexList(IndexList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.CIndex> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.CIndex> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getRPath().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getRPath().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getRPath().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getRPath().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.CIndex> iterator() {
    return getRPath().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getRPath().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getRPath().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getRPath().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getRPath().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.CIndex o) {
      list = (IndexList)fiacre.types.rpath.ConsIndexList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (IndexList)fiacre.types.rpath.EmptyIndexList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyIndexList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
