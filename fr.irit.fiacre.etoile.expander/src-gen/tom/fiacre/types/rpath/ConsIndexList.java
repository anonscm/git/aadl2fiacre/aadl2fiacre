
package fiacre.types.rpath;



public final class ConsIndexList extends fiacre.types.rpath.IndexList implements tom.library.sl.Visitable  {
  
  private static String symbolName = "ConsIndexList";


  private ConsIndexList() {}
  private int hashCode;
  private static ConsIndexList gomProto = new ConsIndexList();
    private fiacre.types.CIndex _HeadIndexList;
  private fiacre.types.RPath _TailIndexList;

  /**
   * Constructor that builds a term rooted by ConsIndexList
   *
   * @return a term rooted by ConsIndexList
   */

  public static ConsIndexList make(fiacre.types.CIndex _HeadIndexList, fiacre.types.RPath _TailIndexList) {

    // use the proto as a model
    gomProto.initHashCode( _HeadIndexList,  _TailIndexList);
    return (ConsIndexList) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadIndexList
   * @param _TailIndexList
   * @param hashCode hashCode of ConsIndexList
   */
  private void init(fiacre.types.CIndex _HeadIndexList, fiacre.types.RPath _TailIndexList, int hashCode) {
    this._HeadIndexList = _HeadIndexList;
    this._TailIndexList = _TailIndexList;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadIndexList
   * @param _TailIndexList
   */
  private void initHashCode(fiacre.types.CIndex _HeadIndexList, fiacre.types.RPath _TailIndexList) {
    this._HeadIndexList = _HeadIndexList;
    this._TailIndexList = _TailIndexList;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "ConsIndexList";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 2;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    ConsIndexList clone = new ConsIndexList();
    clone.init( _HeadIndexList,  _TailIndexList, hashCode);
    return clone;
  }
  

  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    ConsIndexList tco = (ConsIndexList) ao;
    int _HeadIndexListCmp = (this._HeadIndexList).compareToLPO(tco._HeadIndexList);
    if(_HeadIndexListCmp != 0) {
      return _HeadIndexListCmp;
    }

    int _TailIndexListCmp = (this._TailIndexList).compareToLPO(tco._TailIndexList);
    if(_TailIndexListCmp != 0) {
      return _TailIndexListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    ConsIndexList tco = (ConsIndexList) ao;
    int _HeadIndexListCmp = (this._HeadIndexList).compareTo(tco._HeadIndexList);
    if(_HeadIndexListCmp != 0) {
      return _HeadIndexListCmp;
    }

    int _TailIndexListCmp = (this._TailIndexList).compareTo(tco._TailIndexList);
    if(_TailIndexListCmp != 0) {
      return _TailIndexListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a ConsIndexList and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof ConsIndexList) {

      ConsIndexList peer = (ConsIndexList) obj;
      return _HeadIndexList==peer._HeadIndexList && _TailIndexList==peer._TailIndexList && true;
    }
    return false;
  }


   //RPath interface
  /**
   * Returns true if the term is rooted by the symbol ConsIndexList
   *
   * @return true, because this is rooted by ConsIndexList
   */
  @Override
  public boolean isConsIndexList() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.CIndex
   *
   * @return the attribute fiacre.types.CIndex
   */
  @Override
  public fiacre.types.CIndex getHeadIndexList() {
    return _HeadIndexList;
  }

  /**
   * Sets and returns the attribute fiacre.types.RPath
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.CIndex which just has been set
   */
  @Override
  public fiacre.types.RPath setHeadIndexList(fiacre.types.CIndex set_arg) {
    return make(set_arg, _TailIndexList);
  }
  
  /**
   * Returns the attribute fiacre.types.RPath
   *
   * @return the attribute fiacre.types.RPath
   */
  @Override
  public fiacre.types.RPath getTailIndexList() {
    return _TailIndexList;
  }

  /**
   * Sets and returns the attribute fiacre.types.RPath
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.RPath which just has been set
   */
  @Override
  public fiacre.types.RPath setTailIndexList(fiacre.types.RPath set_arg) {
    return make(_HeadIndexList, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getHeadIndexList().toATerm(), getTailIndexList().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.RPath from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.RPath fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.CIndex.fromTerm(appl.getArgument(0),atConv), fiacre.types.RPath.fromTerm(appl.getArgument(1),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 2;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadIndexList;
      case 1: return _TailIndexList;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.CIndex) v, _TailIndexList);
      case 1: return make(_HeadIndexList, (fiacre.types.RPath) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 2
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 2  && childs[0] instanceof fiacre.types.CIndex && childs[1] instanceof fiacre.types.RPath) {
      return make((fiacre.types.CIndex) childs[0], (fiacre.types.RPath) childs[1]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _HeadIndexList,  _TailIndexList };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (106603189<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_HeadIndexList.hashCode() << 8);
    a += (_TailIndexList.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
