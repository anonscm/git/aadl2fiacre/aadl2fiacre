
package fiacre.types.paramdecls;



public abstract class ListParamDecl extends fiacre.types.ParamDecls implements java.util.Collection<fiacre.types.ParamDecl>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.paramdecls.ConsListParamDecl) {
      fiacre.types.ParamDecls tl = this.getTailListParamDecl();
      if (tl instanceof ListParamDecl) {
        return 1+((ListParamDecl)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.ParamDecls fromArray(fiacre.types.ParamDecl[] array) {
    fiacre.types.ParamDecls res = fiacre.types.paramdecls.EmptyListParamDecl.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.paramdecls.ConsListParamDecl.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.ParamDecls reverse() {
    if(this instanceof fiacre.types.paramdecls.ConsListParamDecl) {
      fiacre.types.ParamDecls cur = this;
      fiacre.types.ParamDecls rev = fiacre.types.paramdecls.EmptyListParamDecl.make();
      while(cur instanceof fiacre.types.paramdecls.ConsListParamDecl) {
        rev = fiacre.types.paramdecls.ConsListParamDecl.make(cur.getHeadListParamDecl(),rev);
        cur = cur.getTailListParamDecl();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.ParamDecls append(fiacre.types.ParamDecl element) {
    if(this instanceof fiacre.types.paramdecls.ConsListParamDecl) {
      fiacre.types.ParamDecls tl = this.getTailListParamDecl();
      if (tl instanceof ListParamDecl) {
        return fiacre.types.paramdecls.ConsListParamDecl.make(this.getHeadListParamDecl(),((ListParamDecl)tl).append(element));
      } else {

        return fiacre.types.paramdecls.ConsListParamDecl.make(this.getHeadListParamDecl(),fiacre.types.paramdecls.ConsListParamDecl.make(element,tl));

      }
    } else {
      return fiacre.types.paramdecls.ConsListParamDecl.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("ListParamDecl(");
    if(this instanceof fiacre.types.paramdecls.ConsListParamDecl) {
      fiacre.types.ParamDecls cur = this;
      while(cur instanceof fiacre.types.paramdecls.ConsListParamDecl) {
        fiacre.types.ParamDecl elem = cur.getHeadListParamDecl();
        cur = cur.getTailListParamDecl();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.paramdecls.ConsListParamDecl) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.paramdecls.EmptyListParamDecl)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.paramdecls.ConsListParamDecl) {
      fiacre.types.ParamDecls tail = this.getTailListParamDecl();
      res = atermFactory.makeList(getHeadListParamDecl().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.ParamDecls from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.ParamDecls fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("ListParamDecl".equals(appl.getName())) {
        fiacre.types.ParamDecls res = fiacre.types.paramdecls.EmptyListParamDecl.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.ParamDecl elem = fiacre.types.ParamDecl.fromTerm(array[i],atConv);
          res = fiacre.types.paramdecls.ConsListParamDecl.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.ParamDecls res = fiacre.types.paramdecls.EmptyListParamDecl.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.ParamDecl elem = fiacre.types.ParamDecl.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.paramdecls.ConsListParamDecl.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.ParamDecls contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.ParamDecls contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.ParamDecls cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.paramdecls.ConsListParamDecl) {
      while(cur instanceof fiacre.types.paramdecls.ConsListParamDecl) {
        if( o.equals(cur.getHeadListParamDecl()) ) {
          return true;
        }
        cur = cur.getTailListParamDecl();
      }
      if(!(cur instanceof fiacre.types.paramdecls.EmptyListParamDecl)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyListParamDecl() ; }

  public java.util.Iterator<fiacre.types.ParamDecl> iterator() {
    return new java.util.Iterator<fiacre.types.ParamDecl>() {
      fiacre.types.ParamDecls list = ListParamDecl.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyListParamDecl();
      }

      public fiacre.types.ParamDecl next() {
        if(list.isEmptyListParamDecl()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsListParamDecl()) {
          fiacre.types.ParamDecl head = list.getHeadListParamDecl();
          list = list.getTailListParamDecl();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.ParamDecl)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.ParamDecl o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.ParamDecl> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.paramdecls.ConsListParamDecl) {
      fiacre.types.ParamDecls cur = this;
      while(cur instanceof fiacre.types.paramdecls.ConsListParamDecl) {
        fiacre.types.ParamDecl elem = cur.getHeadListParamDecl();
        array[i] = elem;
        cur = cur.getTailListParamDecl();
        i++;
      }
      if(!(cur instanceof fiacre.types.paramdecls.EmptyListParamDecl)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.paramdecls.ConsListParamDecl) {
      fiacre.types.ParamDecls cur = this;
      while(cur instanceof fiacre.types.paramdecls.ConsListParamDecl) {
        fiacre.types.ParamDecl elem = cur.getHeadListParamDecl();
        array[i] = (T)elem;
        cur = cur.getTailListParamDecl();
        i++;
      }
      if(!(cur instanceof fiacre.types.paramdecls.EmptyListParamDecl)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.ParamDecl> getCollection() {
    return new CollectionListParamDecl(this);
  }

  public java.util.Collection<fiacre.types.ParamDecl> getCollectionListParamDecl() {
    return new CollectionListParamDecl(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionListParamDecl implements java.util.Collection<fiacre.types.ParamDecl> {
    private ListParamDecl list;

    public ListParamDecl getParamDecls() {
      return list;
    }

    public CollectionListParamDecl(ListParamDecl list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.ParamDecl> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.ParamDecl> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getParamDecls().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getParamDecls().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getParamDecls().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getParamDecls().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.ParamDecl> iterator() {
    return getParamDecls().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getParamDecls().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getParamDecls().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getParamDecls().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getParamDecls().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.ParamDecl o) {
      list = (ListParamDecl)fiacre.types.paramdecls.ConsListParamDecl.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (ListParamDecl)fiacre.types.paramdecls.EmptyListParamDecl.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyListParamDecl();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
