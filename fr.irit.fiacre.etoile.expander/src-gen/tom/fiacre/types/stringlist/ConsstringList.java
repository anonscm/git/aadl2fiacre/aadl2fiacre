
package fiacre.types.stringlist;



public final class ConsstringList extends fiacre.types.stringlist.stringList implements tom.library.sl.Visitable  {
  
  private static String symbolName = "ConsstringList";


  private ConsstringList() {}
  private int hashCode;
  private static ConsstringList gomProto = new ConsstringList();
    private String _HeadstringList;
  private fiacre.types.StringList _TailstringList;

  /**
   * Constructor that builds a term rooted by ConsstringList
   *
   * @return a term rooted by ConsstringList
   */

  public static ConsstringList make(String _HeadstringList, fiacre.types.StringList _TailstringList) {

    // use the proto as a model
    gomProto.initHashCode( _HeadstringList,  _TailstringList);
    return (ConsstringList) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadstringList
   * @param _TailstringList
   * @param hashCode hashCode of ConsstringList
   */
  private void init(String _HeadstringList, fiacre.types.StringList _TailstringList, int hashCode) {
    this._HeadstringList = _HeadstringList.intern();
    this._TailstringList = _TailstringList;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadstringList
   * @param _TailstringList
   */
  private void initHashCode(String _HeadstringList, fiacre.types.StringList _TailstringList) {
    this._HeadstringList = _HeadstringList.intern();
    this._TailstringList = _TailstringList;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "ConsstringList";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 2;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    ConsstringList clone = new ConsstringList();
    clone.init( _HeadstringList,  _TailstringList, hashCode);
    return clone;
  }
  

  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    ConsstringList tco = (ConsstringList) ao;
    int _HeadstringListCmp = (this._HeadstringList).compareTo(tco._HeadstringList);
    if(_HeadstringListCmp != 0) {
      return _HeadstringListCmp;
    }


    int _TailstringListCmp = (this._TailstringList).compareToLPO(tco._TailstringList);
    if(_TailstringListCmp != 0) {
      return _TailstringListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    ConsstringList tco = (ConsstringList) ao;
    int _HeadstringListCmp = (this._HeadstringList).compareTo(tco._HeadstringList);
    if(_HeadstringListCmp != 0) {
      return _HeadstringListCmp;
    }


    int _TailstringListCmp = (this._TailstringList).compareTo(tco._TailstringList);
    if(_TailstringListCmp != 0) {
      return _TailstringListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a ConsstringList and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof ConsstringList) {

      ConsstringList peer = (ConsstringList) obj;
      return _HeadstringList==peer._HeadstringList && _TailstringList==peer._TailstringList && true;
    }
    return false;
  }


   //StringList interface
  /**
   * Returns true if the term is rooted by the symbol ConsstringList
   *
   * @return true, because this is rooted by ConsstringList
   */
  @Override
  public boolean isConsstringList() {
    return true;
  }
  
  /**
   * Returns the attribute String
   *
   * @return the attribute String
   */
  @Override
  public String getHeadstringList() {
    return _HeadstringList;
  }

  /**
   * Sets and returns the attribute fiacre.types.StringList
   *
   * @param set_arg the argument to set
   * @return the attribute String which just has been set
   */
  @Override
  public fiacre.types.StringList setHeadstringList(String set_arg) {
    return make(set_arg, _TailstringList);
  }
  
  /**
   * Returns the attribute fiacre.types.StringList
   *
   * @return the attribute fiacre.types.StringList
   */
  @Override
  public fiacre.types.StringList getTailstringList() {
    return _TailstringList;
  }

  /**
   * Sets and returns the attribute fiacre.types.StringList
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.StringList which just has been set
   */
  @Override
  public fiacre.types.StringList setTailstringList(fiacre.types.StringList set_arg) {
    return make(_HeadstringList, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {(aterm.ATerm) atermFactory.makeAppl(atermFactory.makeAFun(getHeadstringList() ,0 , true)), getTailstringList().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.StringList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.StringList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
convertATermToString(appl.getArgument(0), atConv), fiacre.types.StringList.fromTerm(appl.getArgument(1),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 2;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return new tom.library.sl.VisitableBuiltin<String>(_HeadstringList);
      case 1: return _TailstringList;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make(getHeadstringList(), _TailstringList);
      case 1: return make(getHeadstringList(), (fiacre.types.StringList) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 2
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 2  && childs[0] instanceof tom.library.sl.VisitableBuiltin && childs[1] instanceof fiacre.types.StringList) {
      return make(((tom.library.sl.VisitableBuiltin<String>)childs[0]).getBuiltin(), (fiacre.types.StringList) childs[1]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  new tom.library.sl.VisitableBuiltin<String>(_HeadstringList),  _TailstringList };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (801801820<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (shared.HashFunctions.stringHashFunction(_HeadstringList, 1) << 8);
    a += (_TailstringList.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
