
package fiacre.types.stringlist;



public abstract class stringList extends fiacre.types.StringList implements java.util.Collection<String>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.stringlist.ConsstringList) {
      fiacre.types.StringList tl = this.getTailstringList();
      if (tl instanceof stringList) {
        return 1+((stringList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.StringList fromArray(String[] array) {
    fiacre.types.StringList res = fiacre.types.stringlist.EmptystringList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.stringlist.ConsstringList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.StringList reverse() {
    if(this instanceof fiacre.types.stringlist.ConsstringList) {
      fiacre.types.StringList cur = this;
      fiacre.types.StringList rev = fiacre.types.stringlist.EmptystringList.make();
      while(cur instanceof fiacre.types.stringlist.ConsstringList) {
        rev = fiacre.types.stringlist.ConsstringList.make(cur.getHeadstringList(),rev);
        cur = cur.getTailstringList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.StringList append(String element) {
    if(this instanceof fiacre.types.stringlist.ConsstringList) {
      fiacre.types.StringList tl = this.getTailstringList();
      if (tl instanceof stringList) {
        return fiacre.types.stringlist.ConsstringList.make(this.getHeadstringList(),((stringList)tl).append(element));
      } else {

        return fiacre.types.stringlist.ConsstringList.make(this.getHeadstringList(),fiacre.types.stringlist.ConsstringList.make(element,tl));

      }
    } else {
      return fiacre.types.stringlist.ConsstringList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("stringList(");
    if(this instanceof fiacre.types.stringlist.ConsstringList) {
      fiacre.types.StringList cur = this;
      while(cur instanceof fiacre.types.stringlist.ConsstringList) {
        String elem = cur.getHeadstringList();
        cur = cur.getTailstringList();
        buffer.append('"');
            for (int i = 0; i < elem.length(); i++) {
              char c = elem.charAt(i);
              switch (c) {
                case '\n':
                  buffer.append('\\');
                  buffer.append('n');
                  break;
                case '\t':
                  buffer.append('\\');
                  buffer.append('t');
                  break;
                case '\b':
                  buffer.append('\\');
                  buffer.append('b');
                  break;
                case '\r':
                  buffer.append('\\');
                  buffer.append('r');
                  break;
                case '\f':
                  buffer.append('\\');
                  buffer.append('f');
                  break;
                case '\\':
                  buffer.append('\\');
                  buffer.append('\\');
                  break;
                case '\'':
                  buffer.append('\\');
                  buffer.append('\'');
                  break;
                case '\"':
                  buffer.append('\\');
                  buffer.append('\"');
                  break;
                case '!':
                case '@':
                case '#':
                case '$':
                case '%':
                case '^':
                case '&':
                case '*':
                case '(':
                case ')':
                case '-':
                case '_':
                case '+':
                case '=':
                case '|':
                case '~':
                case '{':
                case '}':
                case '[':
                case ']':
                case ';':
                case ':':
                case '<':
                case '>':
                case ',':
                case '.':
                case '?':
                case ' ':
                case '/':
                  buffer.append(c);
                  break;

                default:
                  if (java.lang.Character.isLetterOrDigit(c)) {
                    buffer.append(c);
                  } else {
                    buffer.append('\\');
                    buffer.append((char) ('0' + c / 64));
                    c = (char) (c % 64);
                    buffer.append((char) ('0' + c / 8));
                    c = (char) (c % 8);
                    buffer.append((char) ('0' + c));
                  }
              }
            }
            buffer.append('"');

        if(cur instanceof fiacre.types.stringlist.ConsstringList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.stringlist.EmptystringList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.stringlist.ConsstringList) {
      fiacre.types.StringList tail = this.getTailstringList();
      res = atermFactory.makeList((aterm.ATerm) atermFactory.makeAppl(atermFactory.makeAFun(getHeadstringList() ,0 , true)),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.StringList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.StringList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("stringList".equals(appl.getName())) {
        fiacre.types.StringList res = fiacre.types.stringlist.EmptystringList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          String elem = convertATermToString(array[i], atConv);
          res = fiacre.types.stringlist.ConsstringList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.StringList res = fiacre.types.stringlist.EmptystringList.make();
      try {
        while(!list.isEmpty()) {
          String elem = convertATermToString(list.getFirst(), atConv);
          res = fiacre.types.stringlist.ConsstringList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.StringList contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.StringList contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.StringList cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.stringlist.ConsstringList) {
      while(cur instanceof fiacre.types.stringlist.ConsstringList) {
        if( o.equals(cur.getHeadstringList()) ) {
          return true;
        }
        cur = cur.getTailstringList();
      }
      if(!(cur instanceof fiacre.types.stringlist.EmptystringList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptystringList() ; }

  public java.util.Iterator<String> iterator() {
    return new java.util.Iterator<String>() {
      fiacre.types.StringList list = stringList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptystringList();
      }

      public String next() {
        if(list.isEmptystringList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsstringList()) {
          String head = list.getHeadstringList();
          list = list.getTailstringList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (String)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(String o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends String> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.stringlist.ConsstringList) {
      fiacre.types.StringList cur = this;
      while(cur instanceof fiacre.types.stringlist.ConsstringList) {
        String elem = cur.getHeadstringList();
        array[i] = elem;
        cur = cur.getTailstringList();
        i++;
      }
      if(!(cur instanceof fiacre.types.stringlist.EmptystringList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.stringlist.ConsstringList) {
      fiacre.types.StringList cur = this;
      while(cur instanceof fiacre.types.stringlist.ConsstringList) {
        String elem = cur.getHeadstringList();
        array[i] = (T)elem;
        cur = cur.getTailstringList();
        i++;
      }
      if(!(cur instanceof fiacre.types.stringlist.EmptystringList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<String> getCollection() {
    return new CollectionstringList(this);
  }

  public java.util.Collection<String> getCollectionstringList() {
    return new CollectionstringList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionstringList implements java.util.Collection<String> {
    private stringList list;

    public stringList getStringList() {
      return list;
    }

    public CollectionstringList(stringList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends String> c) {
    boolean modified = false;
    java.util.Iterator<? extends String> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getStringList().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getStringList().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getStringList().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getStringList().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<String> iterator() {
    return getStringList().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getStringList().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getStringList().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getStringList().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getStringList().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(String o) {
      list = (stringList)fiacre.types.stringlist.ConsstringList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (stringList)fiacre.types.stringlist.EmptystringList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptystringList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
