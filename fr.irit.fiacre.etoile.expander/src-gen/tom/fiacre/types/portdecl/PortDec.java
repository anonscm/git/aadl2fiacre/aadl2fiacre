
package fiacre.types.portdecl;



public final class PortDec extends fiacre.types.PortDecl implements tom.library.sl.Visitable  {
  
  private static String symbolName = "PortDec";


  private PortDec() {}
  private int hashCode;
  private static PortDec gomProto = new PortDec();
    private fiacre.types.StringList _lp;
  private fiacre.types.Type _type;
  private fiacre.types.PortAttrList _pal;
  private fiacre.types.Channel _chan;

  /**
   * Constructor that builds a term rooted by PortDec
   *
   * @return a term rooted by PortDec
   */

  public static PortDec make(fiacre.types.StringList _lp, fiacre.types.Type _type, fiacre.types.PortAttrList _pal, fiacre.types.Channel _chan) {

    // use the proto as a model
    gomProto.initHashCode( _lp,  _type,  _pal,  _chan);
    return (PortDec) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _lp
   * @param _type
   * @param _pal
   * @param _chan
   * @param hashCode hashCode of PortDec
   */
  private void init(fiacre.types.StringList _lp, fiacre.types.Type _type, fiacre.types.PortAttrList _pal, fiacre.types.Channel _chan, int hashCode) {
    this._lp = _lp;
    this._type = _type;
    this._pal = _pal;
    this._chan = _chan;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _lp
   * @param _type
   * @param _pal
   * @param _chan
   */
  private void initHashCode(fiacre.types.StringList _lp, fiacre.types.Type _type, fiacre.types.PortAttrList _pal, fiacre.types.Channel _chan) {
    this._lp = _lp;
    this._type = _type;
    this._pal = _pal;
    this._chan = _chan;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "PortDec";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 4;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    PortDec clone = new PortDec();
    clone.init( _lp,  _type,  _pal,  _chan, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("PortDec(");
    _lp.toStringBuilder(buffer);
buffer.append(",");
    _type.toStringBuilder(buffer);
buffer.append(",");
    _pal.toStringBuilder(buffer);
buffer.append(",");
    _chan.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    PortDec tco = (PortDec) ao;
    int _lpCmp = (this._lp).compareToLPO(tco._lp);
    if(_lpCmp != 0) {
      return _lpCmp;
    }

    int _typeCmp = (this._type).compareToLPO(tco._type);
    if(_typeCmp != 0) {
      return _typeCmp;
    }

    int _palCmp = (this._pal).compareToLPO(tco._pal);
    if(_palCmp != 0) {
      return _palCmp;
    }

    int _chanCmp = (this._chan).compareToLPO(tco._chan);
    if(_chanCmp != 0) {
      return _chanCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    PortDec tco = (PortDec) ao;
    int _lpCmp = (this._lp).compareTo(tco._lp);
    if(_lpCmp != 0) {
      return _lpCmp;
    }

    int _typeCmp = (this._type).compareTo(tco._type);
    if(_typeCmp != 0) {
      return _typeCmp;
    }

    int _palCmp = (this._pal).compareTo(tco._pal);
    if(_palCmp != 0) {
      return _palCmp;
    }

    int _chanCmp = (this._chan).compareTo(tco._chan);
    if(_chanCmp != 0) {
      return _chanCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a PortDec and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof PortDec) {

      PortDec peer = (PortDec) obj;
      return _lp==peer._lp && _type==peer._type && _pal==peer._pal && _chan==peer._chan && true;
    }
    return false;
  }


   //PortDecl interface
  /**
   * Returns true if the term is rooted by the symbol PortDec
   *
   * @return true, because this is rooted by PortDec
   */
  @Override
  public boolean isPortDec() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.StringList
   *
   * @return the attribute fiacre.types.StringList
   */
  @Override
  public fiacre.types.StringList getlp() {
    return _lp;
  }

  /**
   * Sets and returns the attribute fiacre.types.PortDecl
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.StringList which just has been set
   */
  @Override
  public fiacre.types.PortDecl setlp(fiacre.types.StringList set_arg) {
    return make(set_arg, _type, _pal, _chan);
  }
  
  /**
   * Returns the attribute fiacre.types.Type
   *
   * @return the attribute fiacre.types.Type
   */
  @Override
  public fiacre.types.Type gettype() {
    return _type;
  }

  /**
   * Sets and returns the attribute fiacre.types.PortDecl
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Type which just has been set
   */
  @Override
  public fiacre.types.PortDecl settype(fiacre.types.Type set_arg) {
    return make(_lp, set_arg, _pal, _chan);
  }
  
  /**
   * Returns the attribute fiacre.types.PortAttrList
   *
   * @return the attribute fiacre.types.PortAttrList
   */
  @Override
  public fiacre.types.PortAttrList getpal() {
    return _pal;
  }

  /**
   * Sets and returns the attribute fiacre.types.PortDecl
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PortAttrList which just has been set
   */
  @Override
  public fiacre.types.PortDecl setpal(fiacre.types.PortAttrList set_arg) {
    return make(_lp, _type, set_arg, _chan);
  }
  
  /**
   * Returns the attribute fiacre.types.Channel
   *
   * @return the attribute fiacre.types.Channel
   */
  @Override
  public fiacre.types.Channel getchan() {
    return _chan;
  }

  /**
   * Sets and returns the attribute fiacre.types.PortDecl
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Channel which just has been set
   */
  @Override
  public fiacre.types.PortDecl setchan(fiacre.types.Channel set_arg) {
    return make(_lp, _type, _pal, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getlp().toATerm(), gettype().toATerm(), getpal().toATerm(), getchan().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.PortDecl from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PortDecl fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.StringList.fromTerm(appl.getArgument(0),atConv), fiacre.types.Type.fromTerm(appl.getArgument(1),atConv), fiacre.types.PortAttrList.fromTerm(appl.getArgument(2),atConv), fiacre.types.Channel.fromTerm(appl.getArgument(3),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 4;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _lp;
      case 1: return _type;
      case 2: return _pal;
      case 3: return _chan;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.StringList) v, _type, _pal, _chan);
      case 1: return make(_lp, (fiacre.types.Type) v, _pal, _chan);
      case 2: return make(_lp, _type, (fiacre.types.PortAttrList) v, _chan);
      case 3: return make(_lp, _type, _pal, (fiacre.types.Channel) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 4
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 4  && childs[0] instanceof fiacre.types.StringList && childs[1] instanceof fiacre.types.Type && childs[2] instanceof fiacre.types.PortAttrList && childs[3] instanceof fiacre.types.Channel) {
      return make((fiacre.types.StringList) childs[0], (fiacre.types.Type) childs[1], (fiacre.types.PortAttrList) childs[2], (fiacre.types.Channel) childs[3]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _lp,  _type,  _pal,  _chan };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-1815356055<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_lp.hashCode() << 24);
    a += (_type.hashCode() << 16);
    a += (_pal.hashCode() << 8);
    a += (_chan.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
