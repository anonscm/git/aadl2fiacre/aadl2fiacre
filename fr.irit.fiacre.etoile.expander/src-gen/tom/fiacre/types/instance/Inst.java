
package fiacre.types.instance;



public final class Inst extends fiacre.types.Instance implements tom.library.sl.Visitable  {
  
  private static String symbolName = "Inst";


  private Inst() {}
  private int hashCode;
  private static Inst gomProto = new Inst();
    private String _s;
  private fiacre.types.GenInsts _genericParamsEffec;
  private fiacre.types.ExpList _strl;
  private fiacre.types.ExpList _el;

  /**
   * Constructor that builds a term rooted by Inst
   *
   * @return a term rooted by Inst
   */

  public static Inst make(String _s, fiacre.types.GenInsts _genericParamsEffec, fiacre.types.ExpList _strl, fiacre.types.ExpList _el) {

    // use the proto as a model
    gomProto.initHashCode( _s,  _genericParamsEffec,  _strl,  _el);
    return (Inst) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _s
   * @param _genericParamsEffec
   * @param _strl
   * @param _el
   * @param hashCode hashCode of Inst
   */
  private void init(String _s, fiacre.types.GenInsts _genericParamsEffec, fiacre.types.ExpList _strl, fiacre.types.ExpList _el, int hashCode) {
    this._s = _s.intern();
    this._genericParamsEffec = _genericParamsEffec;
    this._strl = _strl;
    this._el = _el;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _s
   * @param _genericParamsEffec
   * @param _strl
   * @param _el
   */
  private void initHashCode(String _s, fiacre.types.GenInsts _genericParamsEffec, fiacre.types.ExpList _strl, fiacre.types.ExpList _el) {
    this._s = _s.intern();
    this._genericParamsEffec = _genericParamsEffec;
    this._strl = _strl;
    this._el = _el;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "Inst";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 4;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    Inst clone = new Inst();
    clone.init( _s,  _genericParamsEffec,  _strl,  _el, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("Inst(");
    buffer.append('"');
            for (int i = 0; i < _s.length(); i++) {
              char c = _s.charAt(i);
              switch (c) {
                case '\n':
                  buffer.append('\\');
                  buffer.append('n');
                  break;
                case '\t':
                  buffer.append('\\');
                  buffer.append('t');
                  break;
                case '\b':
                  buffer.append('\\');
                  buffer.append('b');
                  break;
                case '\r':
                  buffer.append('\\');
                  buffer.append('r');
                  break;
                case '\f':
                  buffer.append('\\');
                  buffer.append('f');
                  break;
                case '\\':
                  buffer.append('\\');
                  buffer.append('\\');
                  break;
                case '\'':
                  buffer.append('\\');
                  buffer.append('\'');
                  break;
                case '\"':
                  buffer.append('\\');
                  buffer.append('\"');
                  break;
                case '!':
                case '@':
                case '#':
                case '$':
                case '%':
                case '^':
                case '&':
                case '*':
                case '(':
                case ')':
                case '-':
                case '_':
                case '+':
                case '=':
                case '|':
                case '~':
                case '{':
                case '}':
                case '[':
                case ']':
                case ';':
                case ':':
                case '<':
                case '>':
                case ',':
                case '.':
                case '?':
                case ' ':
                case '/':
                  buffer.append(c);
                  break;

                default:
                  if (java.lang.Character.isLetterOrDigit(c)) {
                    buffer.append(c);
                  } else {
                    buffer.append('\\');
                    buffer.append((char) ('0' + c / 64));
                    c = (char) (c % 64);
                    buffer.append((char) ('0' + c / 8));
                    c = (char) (c % 8);
                    buffer.append((char) ('0' + c));
                  }
              }
            }
            buffer.append('"');
buffer.append(",");
    _genericParamsEffec.toStringBuilder(buffer);
buffer.append(",");
    _strl.toStringBuilder(buffer);
buffer.append(",");
    _el.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    Inst tco = (Inst) ao;
    int _sCmp = (this._s).compareTo(tco._s);
    if(_sCmp != 0) {
      return _sCmp;
    }


    int _genericParamsEffecCmp = (this._genericParamsEffec).compareToLPO(tco._genericParamsEffec);
    if(_genericParamsEffecCmp != 0) {
      return _genericParamsEffecCmp;
    }

    int _strlCmp = (this._strl).compareToLPO(tco._strl);
    if(_strlCmp != 0) {
      return _strlCmp;
    }

    int _elCmp = (this._el).compareToLPO(tco._el);
    if(_elCmp != 0) {
      return _elCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    Inst tco = (Inst) ao;
    int _sCmp = (this._s).compareTo(tco._s);
    if(_sCmp != 0) {
      return _sCmp;
    }


    int _genericParamsEffecCmp = (this._genericParamsEffec).compareTo(tco._genericParamsEffec);
    if(_genericParamsEffecCmp != 0) {
      return _genericParamsEffecCmp;
    }

    int _strlCmp = (this._strl).compareTo(tco._strl);
    if(_strlCmp != 0) {
      return _strlCmp;
    }

    int _elCmp = (this._el).compareTo(tco._el);
    if(_elCmp != 0) {
      return _elCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a Inst and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof Inst) {

      Inst peer = (Inst) obj;
      return _s==peer._s && _genericParamsEffec==peer._genericParamsEffec && _strl==peer._strl && _el==peer._el && true;
    }
    return false;
  }


   //Instance interface
  /**
   * Returns true if the term is rooted by the symbol Inst
   *
   * @return true, because this is rooted by Inst
   */
  @Override
  public boolean isInst() {
    return true;
  }
  
  /**
   * Returns the attribute String
   *
   * @return the attribute String
   */
  @Override
  public String gets() {
    return _s;
  }

  /**
   * Sets and returns the attribute fiacre.types.Instance
   *
   * @param set_arg the argument to set
   * @return the attribute String which just has been set
   */
  @Override
  public fiacre.types.Instance sets(String set_arg) {
    return make(set_arg, _genericParamsEffec, _strl, _el);
  }
  
  /**
   * Returns the attribute fiacre.types.GenInsts
   *
   * @return the attribute fiacre.types.GenInsts
   */
  @Override
  public fiacre.types.GenInsts getgenericParamsEffec() {
    return _genericParamsEffec;
  }

  /**
   * Sets and returns the attribute fiacre.types.Instance
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.GenInsts which just has been set
   */
  @Override
  public fiacre.types.Instance setgenericParamsEffec(fiacre.types.GenInsts set_arg) {
    return make(_s, set_arg, _strl, _el);
  }
  
  /**
   * Returns the attribute fiacre.types.ExpList
   *
   * @return the attribute fiacre.types.ExpList
   */
  @Override
  public fiacre.types.ExpList getstrl() {
    return _strl;
  }

  /**
   * Sets and returns the attribute fiacre.types.Instance
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.ExpList which just has been set
   */
  @Override
  public fiacre.types.Instance setstrl(fiacre.types.ExpList set_arg) {
    return make(_s, _genericParamsEffec, set_arg, _el);
  }
  
  /**
   * Returns the attribute fiacre.types.ExpList
   *
   * @return the attribute fiacre.types.ExpList
   */
  @Override
  public fiacre.types.ExpList getel() {
    return _el;
  }

  /**
   * Sets and returns the attribute fiacre.types.Instance
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.ExpList which just has been set
   */
  @Override
  public fiacre.types.Instance setel(fiacre.types.ExpList set_arg) {
    return make(_s, _genericParamsEffec, _strl, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {(aterm.ATerm) atermFactory.makeAppl(atermFactory.makeAFun(gets() ,0 , true)), getgenericParamsEffec().toATerm(), getstrl().toATerm(), getel().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Instance from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Instance fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
convertATermToString(appl.getArgument(0), atConv), fiacre.types.GenInsts.fromTerm(appl.getArgument(1),atConv), fiacre.types.ExpList.fromTerm(appl.getArgument(2),atConv), fiacre.types.ExpList.fromTerm(appl.getArgument(3),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 4;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return new tom.library.sl.VisitableBuiltin<String>(_s);
      case 1: return _genericParamsEffec;
      case 2: return _strl;
      case 3: return _el;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make(gets(), _genericParamsEffec, _strl, _el);
      case 1: return make(gets(), (fiacre.types.GenInsts) v, _strl, _el);
      case 2: return make(gets(), _genericParamsEffec, (fiacre.types.ExpList) v, _el);
      case 3: return make(gets(), _genericParamsEffec, _strl, (fiacre.types.ExpList) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 4
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 4  && childs[0] instanceof tom.library.sl.VisitableBuiltin && childs[1] instanceof fiacre.types.GenInsts && childs[2] instanceof fiacre.types.ExpList && childs[3] instanceof fiacre.types.ExpList) {
      return make(((tom.library.sl.VisitableBuiltin<String>)childs[0]).getBuiltin(), (fiacre.types.GenInsts) childs[1], (fiacre.types.ExpList) childs[2], (fiacre.types.ExpList) childs[3]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  new tom.library.sl.VisitableBuiltin<String>(_s),  _genericParamsEffec,  _strl,  _el };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-138990988<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (shared.HashFunctions.stringHashFunction(_s, 3) << 24);
    a += (_genericParamsEffec.hashCode() << 16);
    a += (_strl.hashCode() << 8);
    a += (_el.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
