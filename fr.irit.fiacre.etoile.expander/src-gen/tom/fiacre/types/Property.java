
package fiacre.types;


public abstract class Property extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected Property() {}



  /**
   * Returns true if the term is rooted by the symbol PropLTL
   *
   * @return true if the term is rooted by the symbol PropLTL
   */
  public boolean isPropLTL() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropO
   *
   * @return true if the term is rooted by the symbol PropO
   */
  public boolean isPropO() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropOx
   *
   * @return true if the term is rooted by the symbol PropOx
   */
  public boolean isPropOx() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropOxOx
   *
   * @return true if the term is rooted by the symbol PropOxOx
   */
  public boolean isPropOxOx() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropxOx
   *
   * @return true if the term is rooted by the symbol PropxOx
   */
  public boolean isPropxOx() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropOxOxOx
   *
   * @return true if the term is rooted by the symbol PropOxOxOx
   */
  public boolean isPropOxOxOx() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropOxOxOd
   *
   * @return true if the term is rooted by the symbol PropOxOxOd
   */
  public boolean isPropOxOxOd() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropxOxOxOx
   *
   * @return true if the term is rooted by the symbol PropxOxOxOx
   */
  public boolean isPropxOxOxOx() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropxOxOx
   *
   * @return true if the term is rooted by the symbol PropxOxOx
   */
  public boolean isPropxOxOx() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropOxOxOI
   *
   * @return true if the term is rooted by the symbol PropOxOxOI
   */
  public boolean isPropOxOxOI() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropOxOd
   *
   * @return true if the term is rooted by the symbol PropOxOd
   */
  public boolean isPropOxOd() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropxOxOI
   *
   * @return true if the term is rooted by the symbol PropxOxOI
   */
  public boolean isPropxOxOI() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropxOxOxOI
   *
   * @return true if the term is rooted by the symbol PropxOxOxOI
   */
  public boolean isPropxOxOxOI() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropBin
   *
   * @return true if the term is rooted by the symbol PropBin
   */
  public boolean isPropBin() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropNot
   *
   * @return true if the term is rooted by the symbol PropNot
   */
  public boolean isPropNot() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropPar
   *
   * @return true if the term is rooted by the symbol PropPar
   */
  public boolean isPropPar() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropAll
   *
   * @return true if the term is rooted by the symbol PropAll
   */
  public boolean isPropAll() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PropEx
   *
   * @return true if the term is rooted by the symbol PropEx
   */
  public boolean isPropEx() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot op2
   *
   * @return the subterm corresponding to the slot op2
   */
  public String getop2() {
    throw new UnsupportedOperationException("This Property has no op2");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot op2
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot op2 is replaced by _arg
   */
  public Property setop2(String _arg) {
    throw new UnsupportedOperationException("This Property has no op2");
  }

  /**
   * Returns the subterm corresponding to the slot arg1
   *
   * @return the subterm corresponding to the slot arg1
   */
  public fiacre.types.Property getarg1() {
    throw new UnsupportedOperationException("This Property has no arg1");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot arg1
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot arg1 is replaced by _arg
   */
  public Property setarg1(fiacre.types.Property _arg) {
    throw new UnsupportedOperationException("This Property has no arg1");
  }

  /**
   * Returns the subterm corresponding to the slot op1
   *
   * @return the subterm corresponding to the slot op1
   */
  public String getop1() {
    throw new UnsupportedOperationException("This Property has no op1");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot op1
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot op1 is replaced by _arg
   */
  public Property setop1(String _arg) {
    throw new UnsupportedOperationException("This Property has no op1");
  }

  /**
   * Returns the subterm corresponding to the slot begin
   *
   * @return the subterm corresponding to the slot begin
   */
  public fiacre.types.Exp getbegin() {
    throw new UnsupportedOperationException("This Property has no begin");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot begin
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot begin is replaced by _arg
   */
  public Property setbegin(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Property has no begin");
  }

  /**
   * Returns the subterm corresponding to the slot end
   *
   * @return the subterm corresponding to the slot end
   */
  public fiacre.types.Exp getend() {
    throw new UnsupportedOperationException("This Property has no end");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot end
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot end is replaced by _arg
   */
  public Property setend(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Property has no end");
  }

  /**
   * Returns the subterm corresponding to the slot ind
   *
   * @return the subterm corresponding to the slot ind
   */
  public String getind() {
    throw new UnsupportedOperationException("This Property has no ind");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot ind
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot ind is replaced by _arg
   */
  public Property setind(String _arg) {
    throw new UnsupportedOperationException("This Property has no ind");
  }

  /**
   * Returns the subterm corresponding to the slot op3
   *
   * @return the subterm corresponding to the slot op3
   */
  public String getop3() {
    throw new UnsupportedOperationException("This Property has no op3");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot op3
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot op3 is replaced by _arg
   */
  public Property setop3(String _arg) {
    throw new UnsupportedOperationException("This Property has no op3");
  }

  /**
   * Returns the subterm corresponding to the slot obs
   *
   * @return the subterm corresponding to the slot obs
   */
  public fiacre.types.Observable getobs() {
    throw new UnsupportedOperationException("This Property has no obs");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot obs
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot obs is replaced by _arg
   */
  public Property setobs(fiacre.types.Observable _arg) {
    throw new UnsupportedOperationException("This Property has no obs");
  }

  /**
   * Returns the subterm corresponding to the slot op
   *
   * @return the subterm corresponding to the slot op
   */
  public String getop() {
    throw new UnsupportedOperationException("This Property has no op");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot op
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot op is replaced by _arg
   */
  public Property setop(String _arg) {
    throw new UnsupportedOperationException("This Property has no op");
  }

  /**
   * Returns the subterm corresponding to the slot f
   *
   * @return the subterm corresponding to the slot f
   */
  public fiacre.types.LTL getf() {
    throw new UnsupportedOperationException("This Property has no f");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot f
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot f is replaced by _arg
   */
  public Property setf(fiacre.types.LTL _arg) {
    throw new UnsupportedOperationException("This Property has no f");
  }

  /**
   * Returns the subterm corresponding to the slot arg2
   *
   * @return the subterm corresponding to the slot arg2
   */
  public fiacre.types.Property getarg2() {
    throw new UnsupportedOperationException("This Property has no arg2");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot arg2
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot arg2 is replaced by _arg
   */
  public Property setarg2(fiacre.types.Property _arg) {
    throw new UnsupportedOperationException("This Property has no arg2");
  }

  /**
   * Returns the subterm corresponding to the slot max
   *
   * @return the subterm corresponding to the slot max
   */
  public fiacre.types.Bound getmax() {
    throw new UnsupportedOperationException("This Property has no max");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot max
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot max is replaced by _arg
   */
  public Property setmax(fiacre.types.Bound _arg) {
    throw new UnsupportedOperationException("This Property has no max");
  }

  /**
   * Returns the subterm corresponding to the slot obs2
   *
   * @return the subterm corresponding to the slot obs2
   */
  public fiacre.types.Observable getobs2() {
    throw new UnsupportedOperationException("This Property has no obs2");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot obs2
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot obs2 is replaced by _arg
   */
  public Property setobs2(fiacre.types.Observable _arg) {
    throw new UnsupportedOperationException("This Property has no obs2");
  }

  /**
   * Returns the subterm corresponding to the slot obs3
   *
   * @return the subterm corresponding to the slot obs3
   */
  public fiacre.types.Observable getobs3() {
    throw new UnsupportedOperationException("This Property has no obs3");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot obs3
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot obs3 is replaced by _arg
   */
  public Property setobs3(fiacre.types.Observable _arg) {
    throw new UnsupportedOperationException("This Property has no obs3");
  }

  /**
   * Returns the subterm corresponding to the slot min
   *
   * @return the subterm corresponding to the slot min
   */
  public fiacre.types.Bound getmin() {
    throw new UnsupportedOperationException("This Property has no min");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot min
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot min is replaced by _arg
   */
  public Property setmin(fiacre.types.Bound _arg) {
    throw new UnsupportedOperationException("This Property has no min");
  }

  /**
   * Returns the subterm corresponding to the slot obs4
   *
   * @return the subterm corresponding to the slot obs4
   */
  public fiacre.types.Observable getobs4() {
    throw new UnsupportedOperationException("This Property has no obs4");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot obs4
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot obs4 is replaced by _arg
   */
  public Property setobs4(fiacre.types.Observable _arg) {
    throw new UnsupportedOperationException("This Property has no obs4");
  }

  /**
   * Returns the subterm corresponding to the slot dur
   *
   * @return the subterm corresponding to the slot dur
   */
  public fiacre.types.Duration getdur() {
    throw new UnsupportedOperationException("This Property has no dur");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot dur
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot dur is replaced by _arg
   */
  public Property setdur(fiacre.types.Duration _arg) {
    throw new UnsupportedOperationException("This Property has no dur");
  }

  /**
   * Returns the subterm corresponding to the slot p
   *
   * @return the subterm corresponding to the slot p
   */
  public fiacre.types.Property getp() {
    throw new UnsupportedOperationException("This Property has no p");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot p
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot p is replaced by _arg
   */
  public Property setp(fiacre.types.Property _arg) {
    throw new UnsupportedOperationException("This Property has no p");
  }

  /**
   * Returns the subterm corresponding to the slot obs1
   *
   * @return the subterm corresponding to the slot obs1
   */
  public fiacre.types.Observable getobs1() {
    throw new UnsupportedOperationException("This Property has no obs1");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot obs1
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot obs1 is replaced by _arg
   */
  public Property setobs1(fiacre.types.Observable _arg) {
    throw new UnsupportedOperationException("This Property has no obs1");
  }

  /**
   * Returns the subterm corresponding to the slot arg
   *
   * @return the subterm corresponding to the slot arg
   */
  public fiacre.types.Property getarg() {
    throw new UnsupportedOperationException("This Property has no arg");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot arg
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot arg is replaced by _arg
   */
  public Property setarg(fiacre.types.Property _arg) {
    throw new UnsupportedOperationException("This Property has no arg");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.Property from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.Property fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.Property from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.Property fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.Property from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.Property fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.Property
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.Property fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.Property tmp;
    java.util.ArrayList<fiacre.types.Property> results = new java.util.ArrayList<fiacre.types.Property>();

    tmp = fiacre.types.property.PropLTL.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropO.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropOx.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropOxOx.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropxOx.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropOxOxOx.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropOxOxOd.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropxOxOxOx.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropxOxOx.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropOxOxOI.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropOxOd.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropxOxOI.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropxOxOxOI.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropBin.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropNot.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropPar.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropAll.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.property.PropEx.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a Property");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("Property").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.Property", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Property from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Property fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.Property from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Property fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.Property reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
