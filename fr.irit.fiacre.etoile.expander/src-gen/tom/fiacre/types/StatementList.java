
package fiacre.types;


public abstract class StatementList extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected StatementList() {}



  /**
   * Returns true if the term is rooted by the symbol ConsStmList
   *
   * @return true if the term is rooted by the symbol ConsStmList
   */
  public boolean isConsStmList() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol EmptyStmList
   *
   * @return true if the term is rooted by the symbol EmptyStmList
   */
  public boolean isEmptyStmList() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot TailStmList
   *
   * @return the subterm corresponding to the slot TailStmList
   */
  public fiacre.types.StatementList getTailStmList() {
    throw new UnsupportedOperationException("This StatementList has no TailStmList");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot TailStmList
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot TailStmList is replaced by _arg
   */
  public StatementList setTailStmList(fiacre.types.StatementList _arg) {
    throw new UnsupportedOperationException("This StatementList has no TailStmList");
  }

  /**
   * Returns the subterm corresponding to the slot HeadStmList
   *
   * @return the subterm corresponding to the slot HeadStmList
   */
  public fiacre.types.Statement getHeadStmList() {
    throw new UnsupportedOperationException("This StatementList has no HeadStmList");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot HeadStmList
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot HeadStmList is replaced by _arg
   */
  public StatementList setHeadStmList(fiacre.types.Statement _arg) {
    throw new UnsupportedOperationException("This StatementList has no HeadStmList");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.StatementList from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.StatementList fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.StatementList from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.StatementList fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.StatementList from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.StatementList fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.StatementList
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.StatementList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.StatementList tmp;
    java.util.ArrayList<fiacre.types.StatementList> results = new java.util.ArrayList<fiacre.types.StatementList>();

    tmp = fiacre.types.statementlist.ConsStmList.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statementlist.EmptyStmList.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.statementlist.StmList.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a StatementList");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("StatementList").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.StatementList", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.StatementList from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.StatementList fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.StatementList from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.StatementList fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.StatementList reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
  /**
   * Returns a Collection extracted from the term
   *
   * @return the collection
   * @throws UnsupportedOperationException if the term is not a list
   */
  public java.util.Collection<fiacre.types.Statement> getCollectionStmList() {
    throw new UnsupportedOperationException("This StatementList cannot be converted into a Collection");
  }
          
}
