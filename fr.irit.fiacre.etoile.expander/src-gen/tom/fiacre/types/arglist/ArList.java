
package fiacre.types.arglist;



public abstract class ArList extends fiacre.types.ArgList implements java.util.Collection<fiacre.types.Arg>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.arglist.ConsArList) {
      fiacre.types.ArgList tl = this.getTailArList();
      if (tl instanceof ArList) {
        return 1+((ArList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.ArgList fromArray(fiacre.types.Arg[] array) {
    fiacre.types.ArgList res = fiacre.types.arglist.EmptyArList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.arglist.ConsArList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.ArgList reverse() {
    if(this instanceof fiacre.types.arglist.ConsArList) {
      fiacre.types.ArgList cur = this;
      fiacre.types.ArgList rev = fiacre.types.arglist.EmptyArList.make();
      while(cur instanceof fiacre.types.arglist.ConsArList) {
        rev = fiacre.types.arglist.ConsArList.make(cur.getHeadArList(),rev);
        cur = cur.getTailArList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.ArgList append(fiacre.types.Arg element) {
    if(this instanceof fiacre.types.arglist.ConsArList) {
      fiacre.types.ArgList tl = this.getTailArList();
      if (tl instanceof ArList) {
        return fiacre.types.arglist.ConsArList.make(this.getHeadArList(),((ArList)tl).append(element));
      } else {

        return fiacre.types.arglist.ConsArList.make(this.getHeadArList(),fiacre.types.arglist.ConsArList.make(element,tl));

      }
    } else {
      return fiacre.types.arglist.ConsArList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("ArList(");
    if(this instanceof fiacre.types.arglist.ConsArList) {
      fiacre.types.ArgList cur = this;
      while(cur instanceof fiacre.types.arglist.ConsArList) {
        fiacre.types.Arg elem = cur.getHeadArList();
        cur = cur.getTailArList();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.arglist.ConsArList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.arglist.EmptyArList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.arglist.ConsArList) {
      fiacre.types.ArgList tail = this.getTailArList();
      res = atermFactory.makeList(getHeadArList().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.ArgList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.ArgList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("ArList".equals(appl.getName())) {
        fiacre.types.ArgList res = fiacre.types.arglist.EmptyArList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.Arg elem = fiacre.types.Arg.fromTerm(array[i],atConv);
          res = fiacre.types.arglist.ConsArList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.ArgList res = fiacre.types.arglist.EmptyArList.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.Arg elem = fiacre.types.Arg.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.arglist.ConsArList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.ArgList contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.ArgList contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.ArgList cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.arglist.ConsArList) {
      while(cur instanceof fiacre.types.arglist.ConsArList) {
        if( o.equals(cur.getHeadArList()) ) {
          return true;
        }
        cur = cur.getTailArList();
      }
      if(!(cur instanceof fiacre.types.arglist.EmptyArList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyArList() ; }

  public java.util.Iterator<fiacre.types.Arg> iterator() {
    return new java.util.Iterator<fiacre.types.Arg>() {
      fiacre.types.ArgList list = ArList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyArList();
      }

      public fiacre.types.Arg next() {
        if(list.isEmptyArList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsArList()) {
          fiacre.types.Arg head = list.getHeadArList();
          list = list.getTailArList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.Arg)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.Arg o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.Arg> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.arglist.ConsArList) {
      fiacre.types.ArgList cur = this;
      while(cur instanceof fiacre.types.arglist.ConsArList) {
        fiacre.types.Arg elem = cur.getHeadArList();
        array[i] = elem;
        cur = cur.getTailArList();
        i++;
      }
      if(!(cur instanceof fiacre.types.arglist.EmptyArList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.arglist.ConsArList) {
      fiacre.types.ArgList cur = this;
      while(cur instanceof fiacre.types.arglist.ConsArList) {
        fiacre.types.Arg elem = cur.getHeadArList();
        array[i] = (T)elem;
        cur = cur.getTailArList();
        i++;
      }
      if(!(cur instanceof fiacre.types.arglist.EmptyArList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.Arg> getCollection() {
    return new CollectionArList(this);
  }

  public java.util.Collection<fiacre.types.Arg> getCollectionArList() {
    return new CollectionArList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionArList implements java.util.Collection<fiacre.types.Arg> {
    private ArList list;

    public ArList getArgList() {
      return list;
    }

    public CollectionArList(ArList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.Arg> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.Arg> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getArgList().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getArgList().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getArgList().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getArgList().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.Arg> iterator() {
    return getArgList().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getArgList().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getArgList().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getArgList().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getArgList().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.Arg o) {
      list = (ArList)fiacre.types.arglist.ConsArList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (ArList)fiacre.types.arglist.EmptyArList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyArList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
