
package fiacre.types;


public abstract class FieldsDecl extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected FieldsDecl() {}



  /**
   * Returns true if the term is rooted by the symbol field
   *
   * @return true if the term is rooted by the symbol field
   */
  public boolean isfield() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot names
   *
   * @return the subterm corresponding to the slot names
   */
  public fiacre.types.StringList getnames() {
    throw new UnsupportedOperationException("This FieldsDecl has no names");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot names
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot names is replaced by _arg
   */
  public FieldsDecl setnames(fiacre.types.StringList _arg) {
    throw new UnsupportedOperationException("This FieldsDecl has no names");
  }

  /**
   * Returns the subterm corresponding to the slot type
   *
   * @return the subterm corresponding to the slot type
   */
  public fiacre.types.Type gettype() {
    throw new UnsupportedOperationException("This FieldsDecl has no type");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot type
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot type is replaced by _arg
   */
  public FieldsDecl settype(fiacre.types.Type _arg) {
    throw new UnsupportedOperationException("This FieldsDecl has no type");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.FieldsDecl from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.FieldsDecl fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.FieldsDecl from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.FieldsDecl fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.FieldsDecl from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.FieldsDecl fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.FieldsDecl
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.FieldsDecl fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.FieldsDecl tmp;
    java.util.ArrayList<fiacre.types.FieldsDecl> results = new java.util.ArrayList<fiacre.types.FieldsDecl>();

    tmp = fiacre.types.fieldsdecl.field.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a FieldsDecl");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("FieldsDecl").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.FieldsDecl", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.FieldsDecl from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.FieldsDecl fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.FieldsDecl from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.FieldsDecl fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.FieldsDecl reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
