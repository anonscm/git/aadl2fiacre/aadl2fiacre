
package fiacre.types.lpdlist;



public abstract class lpdList extends fiacre.types.LPDList implements java.util.Collection<fiacre.types.LocPortDecl>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.lpdlist.ConslpdList) {
      fiacre.types.LPDList tl = this.getTaillpdList();
      if (tl instanceof lpdList) {
        return 1+((lpdList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.LPDList fromArray(fiacre.types.LocPortDecl[] array) {
    fiacre.types.LPDList res = fiacre.types.lpdlist.EmptylpdList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.lpdlist.ConslpdList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.LPDList reverse() {
    if(this instanceof fiacre.types.lpdlist.ConslpdList) {
      fiacre.types.LPDList cur = this;
      fiacre.types.LPDList rev = fiacre.types.lpdlist.EmptylpdList.make();
      while(cur instanceof fiacre.types.lpdlist.ConslpdList) {
        rev = fiacre.types.lpdlist.ConslpdList.make(cur.getHeadlpdList(),rev);
        cur = cur.getTaillpdList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.LPDList append(fiacre.types.LocPortDecl element) {
    if(this instanceof fiacre.types.lpdlist.ConslpdList) {
      fiacre.types.LPDList tl = this.getTaillpdList();
      if (tl instanceof lpdList) {
        return fiacre.types.lpdlist.ConslpdList.make(this.getHeadlpdList(),((lpdList)tl).append(element));
      } else {

        return fiacre.types.lpdlist.ConslpdList.make(this.getHeadlpdList(),fiacre.types.lpdlist.ConslpdList.make(element,tl));

      }
    } else {
      return fiacre.types.lpdlist.ConslpdList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("lpdList(");
    if(this instanceof fiacre.types.lpdlist.ConslpdList) {
      fiacre.types.LPDList cur = this;
      while(cur instanceof fiacre.types.lpdlist.ConslpdList) {
        fiacre.types.LocPortDecl elem = cur.getHeadlpdList();
        cur = cur.getTaillpdList();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.lpdlist.ConslpdList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.lpdlist.EmptylpdList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.lpdlist.ConslpdList) {
      fiacre.types.LPDList tail = this.getTaillpdList();
      res = atermFactory.makeList(getHeadlpdList().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.LPDList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.LPDList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("lpdList".equals(appl.getName())) {
        fiacre.types.LPDList res = fiacre.types.lpdlist.EmptylpdList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.LocPortDecl elem = fiacre.types.LocPortDecl.fromTerm(array[i],atConv);
          res = fiacre.types.lpdlist.ConslpdList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.LPDList res = fiacre.types.lpdlist.EmptylpdList.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.LocPortDecl elem = fiacre.types.LocPortDecl.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.lpdlist.ConslpdList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.LPDList contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.LPDList contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.LPDList cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.lpdlist.ConslpdList) {
      while(cur instanceof fiacre.types.lpdlist.ConslpdList) {
        if( o.equals(cur.getHeadlpdList()) ) {
          return true;
        }
        cur = cur.getTaillpdList();
      }
      if(!(cur instanceof fiacre.types.lpdlist.EmptylpdList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptylpdList() ; }

  public java.util.Iterator<fiacre.types.LocPortDecl> iterator() {
    return new java.util.Iterator<fiacre.types.LocPortDecl>() {
      fiacre.types.LPDList list = lpdList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptylpdList();
      }

      public fiacre.types.LocPortDecl next() {
        if(list.isEmptylpdList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConslpdList()) {
          fiacre.types.LocPortDecl head = list.getHeadlpdList();
          list = list.getTaillpdList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.LocPortDecl)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.LocPortDecl o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.LocPortDecl> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.lpdlist.ConslpdList) {
      fiacre.types.LPDList cur = this;
      while(cur instanceof fiacre.types.lpdlist.ConslpdList) {
        fiacre.types.LocPortDecl elem = cur.getHeadlpdList();
        array[i] = elem;
        cur = cur.getTaillpdList();
        i++;
      }
      if(!(cur instanceof fiacre.types.lpdlist.EmptylpdList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.lpdlist.ConslpdList) {
      fiacre.types.LPDList cur = this;
      while(cur instanceof fiacre.types.lpdlist.ConslpdList) {
        fiacre.types.LocPortDecl elem = cur.getHeadlpdList();
        array[i] = (T)elem;
        cur = cur.getTaillpdList();
        i++;
      }
      if(!(cur instanceof fiacre.types.lpdlist.EmptylpdList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.LocPortDecl> getCollection() {
    return new CollectionlpdList(this);
  }

  public java.util.Collection<fiacre.types.LocPortDecl> getCollectionlpdList() {
    return new CollectionlpdList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionlpdList implements java.util.Collection<fiacre.types.LocPortDecl> {
    private lpdList list;

    public lpdList getLPDList() {
      return list;
    }

    public CollectionlpdList(lpdList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.LocPortDecl> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.LocPortDecl> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getLPDList().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getLPDList().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getLPDList().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getLPDList().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.LocPortDecl> iterator() {
    return getLPDList().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getLPDList().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getLPDList().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getLPDList().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getLPDList().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.LocPortDecl o) {
      list = (lpdList)fiacre.types.lpdlist.ConslpdList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (lpdList)fiacre.types.lpdlist.EmptylpdList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptylpdList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
