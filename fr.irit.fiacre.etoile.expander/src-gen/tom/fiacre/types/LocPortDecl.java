
package fiacre.types;


public abstract class LocPortDecl extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected LocPortDecl() {}



  /**
   * Returns true if the term is rooted by the symbol LocPortDec
   *
   * @return true if the term is rooted by the symbol LocPortDec
   */
  public boolean isLocPortDec() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot pdl
   *
   * @return the subterm corresponding to the slot pdl
   */
  public fiacre.types.PortDecls getpdl() {
    throw new UnsupportedOperationException("This LocPortDecl has no pdl");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot pdl
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot pdl is replaced by _arg
   */
  public LocPortDecl setpdl(fiacre.types.PortDecls _arg) {
    throw new UnsupportedOperationException("This LocPortDecl has no pdl");
  }

  /**
   * Returns the subterm corresponding to the slot b2
   *
   * @return the subterm corresponding to the slot b2
   */
  public fiacre.types.Bound getb2() {
    throw new UnsupportedOperationException("This LocPortDecl has no b2");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot b2
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot b2 is replaced by _arg
   */
  public LocPortDecl setb2(fiacre.types.Bound _arg) {
    throw new UnsupportedOperationException("This LocPortDecl has no b2");
  }

  /**
   * Returns the subterm corresponding to the slot b1
   *
   * @return the subterm corresponding to the slot b1
   */
  public fiacre.types.Bound getb1() {
    throw new UnsupportedOperationException("This LocPortDecl has no b1");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot b1
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot b1 is replaced by _arg
   */
  public LocPortDecl setb1(fiacre.types.Bound _arg) {
    throw new UnsupportedOperationException("This LocPortDecl has no b1");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.LocPortDecl from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.LocPortDecl fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.LocPortDecl from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.LocPortDecl fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.LocPortDecl from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.LocPortDecl fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.LocPortDecl
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.LocPortDecl fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.LocPortDecl tmp;
    java.util.ArrayList<fiacre.types.LocPortDecl> results = new java.util.ArrayList<fiacre.types.LocPortDecl>();

    tmp = fiacre.types.locportdecl.LocPortDec.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a LocPortDecl");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("LocPortDecl").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.LocPortDecl", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.LocPortDecl from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.LocPortDecl fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.LocPortDecl from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.LocPortDecl fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.LocPortDecl reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
