
package fiacre.types.locportdecl;



public final class LocPortDec extends fiacre.types.LocPortDecl implements tom.library.sl.Visitable  {
  
  private static String symbolName = "LocPortDec";


  private LocPortDec() {}
  private int hashCode;
  private static LocPortDec gomProto = new LocPortDec();
    private fiacre.types.PortDecls _pdl;
  private fiacre.types.Bound _b1;
  private fiacre.types.Bound _b2;

  /**
   * Constructor that builds a term rooted by LocPortDec
   *
   * @return a term rooted by LocPortDec
   */

  public static LocPortDec make(fiacre.types.PortDecls _pdl, fiacre.types.Bound _b1, fiacre.types.Bound _b2) {

    // use the proto as a model
    gomProto.initHashCode( _pdl,  _b1,  _b2);
    return (LocPortDec) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _pdl
   * @param _b1
   * @param _b2
   * @param hashCode hashCode of LocPortDec
   */
  private void init(fiacre.types.PortDecls _pdl, fiacre.types.Bound _b1, fiacre.types.Bound _b2, int hashCode) {
    this._pdl = _pdl;
    this._b1 = _b1;
    this._b2 = _b2;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _pdl
   * @param _b1
   * @param _b2
   */
  private void initHashCode(fiacre.types.PortDecls _pdl, fiacre.types.Bound _b1, fiacre.types.Bound _b2) {
    this._pdl = _pdl;
    this._b1 = _b1;
    this._b2 = _b2;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "LocPortDec";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 3;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    LocPortDec clone = new LocPortDec();
    clone.init( _pdl,  _b1,  _b2, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("LocPortDec(");
    _pdl.toStringBuilder(buffer);
buffer.append(",");
    _b1.toStringBuilder(buffer);
buffer.append(",");
    _b2.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    LocPortDec tco = (LocPortDec) ao;
    int _pdlCmp = (this._pdl).compareToLPO(tco._pdl);
    if(_pdlCmp != 0) {
      return _pdlCmp;
    }

    int _b1Cmp = (this._b1).compareToLPO(tco._b1);
    if(_b1Cmp != 0) {
      return _b1Cmp;
    }

    int _b2Cmp = (this._b2).compareToLPO(tco._b2);
    if(_b2Cmp != 0) {
      return _b2Cmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    LocPortDec tco = (LocPortDec) ao;
    int _pdlCmp = (this._pdl).compareTo(tco._pdl);
    if(_pdlCmp != 0) {
      return _pdlCmp;
    }

    int _b1Cmp = (this._b1).compareTo(tco._b1);
    if(_b1Cmp != 0) {
      return _b1Cmp;
    }

    int _b2Cmp = (this._b2).compareTo(tco._b2);
    if(_b2Cmp != 0) {
      return _b2Cmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a LocPortDec and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof LocPortDec) {

      LocPortDec peer = (LocPortDec) obj;
      return _pdl==peer._pdl && _b1==peer._b1 && _b2==peer._b2 && true;
    }
    return false;
  }


   //LocPortDecl interface
  /**
   * Returns true if the term is rooted by the symbol LocPortDec
   *
   * @return true, because this is rooted by LocPortDec
   */
  @Override
  public boolean isLocPortDec() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.PortDecls
   *
   * @return the attribute fiacre.types.PortDecls
   */
  @Override
  public fiacre.types.PortDecls getpdl() {
    return _pdl;
  }

  /**
   * Sets and returns the attribute fiacre.types.LocPortDecl
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PortDecls which just has been set
   */
  @Override
  public fiacre.types.LocPortDecl setpdl(fiacre.types.PortDecls set_arg) {
    return make(set_arg, _b1, _b2);
  }
  
  /**
   * Returns the attribute fiacre.types.Bound
   *
   * @return the attribute fiacre.types.Bound
   */
  @Override
  public fiacre.types.Bound getb1() {
    return _b1;
  }

  /**
   * Sets and returns the attribute fiacre.types.LocPortDecl
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Bound which just has been set
   */
  @Override
  public fiacre.types.LocPortDecl setb1(fiacre.types.Bound set_arg) {
    return make(_pdl, set_arg, _b2);
  }
  
  /**
   * Returns the attribute fiacre.types.Bound
   *
   * @return the attribute fiacre.types.Bound
   */
  @Override
  public fiacre.types.Bound getb2() {
    return _b2;
  }

  /**
   * Sets and returns the attribute fiacre.types.LocPortDecl
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Bound which just has been set
   */
  @Override
  public fiacre.types.LocPortDecl setb2(fiacre.types.Bound set_arg) {
    return make(_pdl, _b1, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getpdl().toATerm(), getb1().toATerm(), getb2().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.LocPortDecl from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.LocPortDecl fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.PortDecls.fromTerm(appl.getArgument(0),atConv), fiacre.types.Bound.fromTerm(appl.getArgument(1),atConv), fiacre.types.Bound.fromTerm(appl.getArgument(2),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 3;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _pdl;
      case 1: return _b1;
      case 2: return _b2;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.PortDecls) v, _b1, _b2);
      case 1: return make(_pdl, (fiacre.types.Bound) v, _b2);
      case 2: return make(_pdl, _b1, (fiacre.types.Bound) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 3
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 3  && childs[0] instanceof fiacre.types.PortDecls && childs[1] instanceof fiacre.types.Bound && childs[2] instanceof fiacre.types.Bound) {
      return make((fiacre.types.PortDecls) childs[0], (fiacre.types.Bound) childs[1], (fiacre.types.Bound) childs[2]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _pdl,  _b1,  _b2 };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (1193860890<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_pdl.hashCode() << 16);
    a += (_b1.hashCode() << 8);
    a += (_b2.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
