
package fiacre.types.portdecls;



public abstract class ListPortDecl extends fiacre.types.PortDecls implements java.util.Collection<fiacre.types.PortDecl>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.portdecls.ConsListPortDecl) {
      fiacre.types.PortDecls tl = this.getTailListPortDecl();
      if (tl instanceof ListPortDecl) {
        return 1+((ListPortDecl)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.PortDecls fromArray(fiacre.types.PortDecl[] array) {
    fiacre.types.PortDecls res = fiacre.types.portdecls.EmptyListPortDecl.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.portdecls.ConsListPortDecl.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.PortDecls reverse() {
    if(this instanceof fiacre.types.portdecls.ConsListPortDecl) {
      fiacre.types.PortDecls cur = this;
      fiacre.types.PortDecls rev = fiacre.types.portdecls.EmptyListPortDecl.make();
      while(cur instanceof fiacre.types.portdecls.ConsListPortDecl) {
        rev = fiacre.types.portdecls.ConsListPortDecl.make(cur.getHeadListPortDecl(),rev);
        cur = cur.getTailListPortDecl();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.PortDecls append(fiacre.types.PortDecl element) {
    if(this instanceof fiacre.types.portdecls.ConsListPortDecl) {
      fiacre.types.PortDecls tl = this.getTailListPortDecl();
      if (tl instanceof ListPortDecl) {
        return fiacre.types.portdecls.ConsListPortDecl.make(this.getHeadListPortDecl(),((ListPortDecl)tl).append(element));
      } else {

        return fiacre.types.portdecls.ConsListPortDecl.make(this.getHeadListPortDecl(),fiacre.types.portdecls.ConsListPortDecl.make(element,tl));

      }
    } else {
      return fiacre.types.portdecls.ConsListPortDecl.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("ListPortDecl(");
    if(this instanceof fiacre.types.portdecls.ConsListPortDecl) {
      fiacre.types.PortDecls cur = this;
      while(cur instanceof fiacre.types.portdecls.ConsListPortDecl) {
        fiacre.types.PortDecl elem = cur.getHeadListPortDecl();
        cur = cur.getTailListPortDecl();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.portdecls.ConsListPortDecl) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.portdecls.EmptyListPortDecl)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.portdecls.ConsListPortDecl) {
      fiacre.types.PortDecls tail = this.getTailListPortDecl();
      res = atermFactory.makeList(getHeadListPortDecl().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.PortDecls from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PortDecls fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("ListPortDecl".equals(appl.getName())) {
        fiacre.types.PortDecls res = fiacre.types.portdecls.EmptyListPortDecl.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.PortDecl elem = fiacre.types.PortDecl.fromTerm(array[i],atConv);
          res = fiacre.types.portdecls.ConsListPortDecl.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.PortDecls res = fiacre.types.portdecls.EmptyListPortDecl.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.PortDecl elem = fiacre.types.PortDecl.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.portdecls.ConsListPortDecl.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.PortDecls contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.PortDecls contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.PortDecls cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.portdecls.ConsListPortDecl) {
      while(cur instanceof fiacre.types.portdecls.ConsListPortDecl) {
        if( o.equals(cur.getHeadListPortDecl()) ) {
          return true;
        }
        cur = cur.getTailListPortDecl();
      }
      if(!(cur instanceof fiacre.types.portdecls.EmptyListPortDecl)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyListPortDecl() ; }

  public java.util.Iterator<fiacre.types.PortDecl> iterator() {
    return new java.util.Iterator<fiacre.types.PortDecl>() {
      fiacre.types.PortDecls list = ListPortDecl.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyListPortDecl();
      }

      public fiacre.types.PortDecl next() {
        if(list.isEmptyListPortDecl()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsListPortDecl()) {
          fiacre.types.PortDecl head = list.getHeadListPortDecl();
          list = list.getTailListPortDecl();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.PortDecl)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.PortDecl o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.PortDecl> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.portdecls.ConsListPortDecl) {
      fiacre.types.PortDecls cur = this;
      while(cur instanceof fiacre.types.portdecls.ConsListPortDecl) {
        fiacre.types.PortDecl elem = cur.getHeadListPortDecl();
        array[i] = elem;
        cur = cur.getTailListPortDecl();
        i++;
      }
      if(!(cur instanceof fiacre.types.portdecls.EmptyListPortDecl)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.portdecls.ConsListPortDecl) {
      fiacre.types.PortDecls cur = this;
      while(cur instanceof fiacre.types.portdecls.ConsListPortDecl) {
        fiacre.types.PortDecl elem = cur.getHeadListPortDecl();
        array[i] = (T)elem;
        cur = cur.getTailListPortDecl();
        i++;
      }
      if(!(cur instanceof fiacre.types.portdecls.EmptyListPortDecl)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.PortDecl> getCollection() {
    return new CollectionListPortDecl(this);
  }

  public java.util.Collection<fiacre.types.PortDecl> getCollectionListPortDecl() {
    return new CollectionListPortDecl(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionListPortDecl implements java.util.Collection<fiacre.types.PortDecl> {
    private ListPortDecl list;

    public ListPortDecl getPortDecls() {
      return list;
    }

    public CollectionListPortDecl(ListPortDecl list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.PortDecl> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.PortDecl> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getPortDecls().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getPortDecls().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getPortDecls().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getPortDecls().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.PortDecl> iterator() {
    return getPortDecls().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getPortDecls().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getPortDecls().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getPortDecls().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getPortDecls().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.PortDecl o) {
      list = (ListPortDecl)fiacre.types.portdecls.ConsListPortDecl.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (ListPortDecl)fiacre.types.portdecls.EmptyListPortDecl.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyListPortDecl();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
