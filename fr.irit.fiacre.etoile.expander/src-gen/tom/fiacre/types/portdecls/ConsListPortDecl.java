
package fiacre.types.portdecls;



public final class ConsListPortDecl extends fiacre.types.portdecls.ListPortDecl implements tom.library.sl.Visitable  {
  
  private static String symbolName = "ConsListPortDecl";


  private ConsListPortDecl() {}
  private int hashCode;
  private static ConsListPortDecl gomProto = new ConsListPortDecl();
    private fiacre.types.PortDecl _HeadListPortDecl;
  private fiacre.types.PortDecls _TailListPortDecl;

  /**
   * Constructor that builds a term rooted by ConsListPortDecl
   *
   * @return a term rooted by ConsListPortDecl
   */

  public static ConsListPortDecl make(fiacre.types.PortDecl _HeadListPortDecl, fiacre.types.PortDecls _TailListPortDecl) {

    // use the proto as a model
    gomProto.initHashCode( _HeadListPortDecl,  _TailListPortDecl);
    return (ConsListPortDecl) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadListPortDecl
   * @param _TailListPortDecl
   * @param hashCode hashCode of ConsListPortDecl
   */
  private void init(fiacre.types.PortDecl _HeadListPortDecl, fiacre.types.PortDecls _TailListPortDecl, int hashCode) {
    this._HeadListPortDecl = _HeadListPortDecl;
    this._TailListPortDecl = _TailListPortDecl;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadListPortDecl
   * @param _TailListPortDecl
   */
  private void initHashCode(fiacre.types.PortDecl _HeadListPortDecl, fiacre.types.PortDecls _TailListPortDecl) {
    this._HeadListPortDecl = _HeadListPortDecl;
    this._TailListPortDecl = _TailListPortDecl;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "ConsListPortDecl";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 2;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    ConsListPortDecl clone = new ConsListPortDecl();
    clone.init( _HeadListPortDecl,  _TailListPortDecl, hashCode);
    return clone;
  }
  

  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    ConsListPortDecl tco = (ConsListPortDecl) ao;
    int _HeadListPortDeclCmp = (this._HeadListPortDecl).compareToLPO(tco._HeadListPortDecl);
    if(_HeadListPortDeclCmp != 0) {
      return _HeadListPortDeclCmp;
    }

    int _TailListPortDeclCmp = (this._TailListPortDecl).compareToLPO(tco._TailListPortDecl);
    if(_TailListPortDeclCmp != 0) {
      return _TailListPortDeclCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    ConsListPortDecl tco = (ConsListPortDecl) ao;
    int _HeadListPortDeclCmp = (this._HeadListPortDecl).compareTo(tco._HeadListPortDecl);
    if(_HeadListPortDeclCmp != 0) {
      return _HeadListPortDeclCmp;
    }

    int _TailListPortDeclCmp = (this._TailListPortDecl).compareTo(tco._TailListPortDecl);
    if(_TailListPortDeclCmp != 0) {
      return _TailListPortDeclCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a ConsListPortDecl and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof ConsListPortDecl) {

      ConsListPortDecl peer = (ConsListPortDecl) obj;
      return _HeadListPortDecl==peer._HeadListPortDecl && _TailListPortDecl==peer._TailListPortDecl && true;
    }
    return false;
  }


   //PortDecls interface
  /**
   * Returns true if the term is rooted by the symbol ConsListPortDecl
   *
   * @return true, because this is rooted by ConsListPortDecl
   */
  @Override
  public boolean isConsListPortDecl() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.PortDecl
   *
   * @return the attribute fiacre.types.PortDecl
   */
  @Override
  public fiacre.types.PortDecl getHeadListPortDecl() {
    return _HeadListPortDecl;
  }

  /**
   * Sets and returns the attribute fiacre.types.PortDecls
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PortDecl which just has been set
   */
  @Override
  public fiacre.types.PortDecls setHeadListPortDecl(fiacre.types.PortDecl set_arg) {
    return make(set_arg, _TailListPortDecl);
  }
  
  /**
   * Returns the attribute fiacre.types.PortDecls
   *
   * @return the attribute fiacre.types.PortDecls
   */
  @Override
  public fiacre.types.PortDecls getTailListPortDecl() {
    return _TailListPortDecl;
  }

  /**
   * Sets and returns the attribute fiacre.types.PortDecls
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PortDecls which just has been set
   */
  @Override
  public fiacre.types.PortDecls setTailListPortDecl(fiacre.types.PortDecls set_arg) {
    return make(_HeadListPortDecl, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getHeadListPortDecl().toATerm(), getTailListPortDecl().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.PortDecls from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PortDecls fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.PortDecl.fromTerm(appl.getArgument(0),atConv), fiacre.types.PortDecls.fromTerm(appl.getArgument(1),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 2;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadListPortDecl;
      case 1: return _TailListPortDecl;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.PortDecl) v, _TailListPortDecl);
      case 1: return make(_HeadListPortDecl, (fiacre.types.PortDecls) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 2
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 2  && childs[0] instanceof fiacre.types.PortDecl && childs[1] instanceof fiacre.types.PortDecls) {
      return make((fiacre.types.PortDecl) childs[0], (fiacre.types.PortDecls) childs[1]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _HeadListPortDecl,  _TailListPortDecl };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (1502249500<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_HeadListPortDecl.hashCode() << 8);
    a += (_TailListPortDecl.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
