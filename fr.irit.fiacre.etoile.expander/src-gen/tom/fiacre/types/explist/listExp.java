
package fiacre.types.explist;



public abstract class listExp extends fiacre.types.ExpList implements java.util.Collection<fiacre.types.Exp>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.explist.ConslistExp) {
      fiacre.types.ExpList tl = this.getTaillistExp();
      if (tl instanceof listExp) {
        return 1+((listExp)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.ExpList fromArray(fiacre.types.Exp[] array) {
    fiacre.types.ExpList res = fiacre.types.explist.EmptylistExp.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.explist.ConslistExp.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.ExpList reverse() {
    if(this instanceof fiacre.types.explist.ConslistExp) {
      fiacre.types.ExpList cur = this;
      fiacre.types.ExpList rev = fiacre.types.explist.EmptylistExp.make();
      while(cur instanceof fiacre.types.explist.ConslistExp) {
        rev = fiacre.types.explist.ConslistExp.make(cur.getHeadlistExp(),rev);
        cur = cur.getTaillistExp();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.ExpList append(fiacre.types.Exp element) {
    if(this instanceof fiacre.types.explist.ConslistExp) {
      fiacre.types.ExpList tl = this.getTaillistExp();
      if (tl instanceof listExp) {
        return fiacre.types.explist.ConslistExp.make(this.getHeadlistExp(),((listExp)tl).append(element));
      } else {

        return fiacre.types.explist.ConslistExp.make(this.getHeadlistExp(),fiacre.types.explist.ConslistExp.make(element,tl));

      }
    } else {
      return fiacre.types.explist.ConslistExp.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("listExp(");
    if(this instanceof fiacre.types.explist.ConslistExp) {
      fiacre.types.ExpList cur = this;
      while(cur instanceof fiacre.types.explist.ConslistExp) {
        fiacre.types.Exp elem = cur.getHeadlistExp();
        cur = cur.getTaillistExp();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.explist.ConslistExp) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.explist.EmptylistExp)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.explist.ConslistExp) {
      fiacre.types.ExpList tail = this.getTaillistExp();
      res = atermFactory.makeList(getHeadlistExp().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.ExpList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.ExpList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("listExp".equals(appl.getName())) {
        fiacre.types.ExpList res = fiacre.types.explist.EmptylistExp.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.Exp elem = fiacre.types.Exp.fromTerm(array[i],atConv);
          res = fiacre.types.explist.ConslistExp.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.ExpList res = fiacre.types.explist.EmptylistExp.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.Exp elem = fiacre.types.Exp.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.explist.ConslistExp.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.ExpList contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.ExpList contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.ExpList cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.explist.ConslistExp) {
      while(cur instanceof fiacre.types.explist.ConslistExp) {
        if( o.equals(cur.getHeadlistExp()) ) {
          return true;
        }
        cur = cur.getTaillistExp();
      }
      if(!(cur instanceof fiacre.types.explist.EmptylistExp)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptylistExp() ; }

  public java.util.Iterator<fiacre.types.Exp> iterator() {
    return new java.util.Iterator<fiacre.types.Exp>() {
      fiacre.types.ExpList list = listExp.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptylistExp();
      }

      public fiacre.types.Exp next() {
        if(list.isEmptylistExp()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConslistExp()) {
          fiacre.types.Exp head = list.getHeadlistExp();
          list = list.getTaillistExp();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.Exp)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.Exp o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.Exp> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.explist.ConslistExp) {
      fiacre.types.ExpList cur = this;
      while(cur instanceof fiacre.types.explist.ConslistExp) {
        fiacre.types.Exp elem = cur.getHeadlistExp();
        array[i] = elem;
        cur = cur.getTaillistExp();
        i++;
      }
      if(!(cur instanceof fiacre.types.explist.EmptylistExp)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.explist.ConslistExp) {
      fiacre.types.ExpList cur = this;
      while(cur instanceof fiacre.types.explist.ConslistExp) {
        fiacre.types.Exp elem = cur.getHeadlistExp();
        array[i] = (T)elem;
        cur = cur.getTaillistExp();
        i++;
      }
      if(!(cur instanceof fiacre.types.explist.EmptylistExp)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.Exp> getCollection() {
    return new CollectionlistExp(this);
  }

  public java.util.Collection<fiacre.types.Exp> getCollectionlistExp() {
    return new CollectionlistExp(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionlistExp implements java.util.Collection<fiacre.types.Exp> {
    private listExp list;

    public listExp getExpList() {
      return list;
    }

    public CollectionlistExp(listExp list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.Exp> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.Exp> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getExpList().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getExpList().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getExpList().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getExpList().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.Exp> iterator() {
    return getExpList().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getExpList().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getExpList().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getExpList().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getExpList().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.Exp o) {
      list = (listExp)fiacre.types.explist.ConslistExp.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (listExp)fiacre.types.explist.EmptylistExp.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptylistExp();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
