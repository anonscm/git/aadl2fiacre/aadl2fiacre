
package fiacre.types.pstatementlist;



public final class ConsPStmList extends fiacre.types.pstatementlist.PStmList implements tom.library.sl.Visitable  {
  
  private static String symbolName = "ConsPStmList";


  private ConsPStmList() {}
  private int hashCode;
  private static ConsPStmList gomProto = new ConsPStmList();
    private fiacre.types.StatementList _HeadPStmList;
  private fiacre.types.PStatementList _TailPStmList;

  /**
   * Constructor that builds a term rooted by ConsPStmList
   *
   * @return a term rooted by ConsPStmList
   */

  public static ConsPStmList make(fiacre.types.StatementList _HeadPStmList, fiacre.types.PStatementList _TailPStmList) {

    // use the proto as a model
    gomProto.initHashCode( _HeadPStmList,  _TailPStmList);
    return (ConsPStmList) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadPStmList
   * @param _TailPStmList
   * @param hashCode hashCode of ConsPStmList
   */
  private void init(fiacre.types.StatementList _HeadPStmList, fiacre.types.PStatementList _TailPStmList, int hashCode) {
    this._HeadPStmList = _HeadPStmList;
    this._TailPStmList = _TailPStmList;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _HeadPStmList
   * @param _TailPStmList
   */
  private void initHashCode(fiacre.types.StatementList _HeadPStmList, fiacre.types.PStatementList _TailPStmList) {
    this._HeadPStmList = _HeadPStmList;
    this._TailPStmList = _TailPStmList;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "ConsPStmList";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 2;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    ConsPStmList clone = new ConsPStmList();
    clone.init( _HeadPStmList,  _TailPStmList, hashCode);
    return clone;
  }
  

  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    ConsPStmList tco = (ConsPStmList) ao;
    int _HeadPStmListCmp = (this._HeadPStmList).compareToLPO(tco._HeadPStmList);
    if(_HeadPStmListCmp != 0) {
      return _HeadPStmListCmp;
    }

    int _TailPStmListCmp = (this._TailPStmList).compareToLPO(tco._TailPStmList);
    if(_TailPStmListCmp != 0) {
      return _TailPStmListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    ConsPStmList tco = (ConsPStmList) ao;
    int _HeadPStmListCmp = (this._HeadPStmList).compareTo(tco._HeadPStmList);
    if(_HeadPStmListCmp != 0) {
      return _HeadPStmListCmp;
    }

    int _TailPStmListCmp = (this._TailPStmList).compareTo(tco._TailPStmList);
    if(_TailPStmListCmp != 0) {
      return _TailPStmListCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a ConsPStmList and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof ConsPStmList) {

      ConsPStmList peer = (ConsPStmList) obj;
      return _HeadPStmList==peer._HeadPStmList && _TailPStmList==peer._TailPStmList && true;
    }
    return false;
  }


   //PStatementList interface
  /**
   * Returns true if the term is rooted by the symbol ConsPStmList
   *
   * @return true, because this is rooted by ConsPStmList
   */
  @Override
  public boolean isConsPStmList() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.StatementList
   *
   * @return the attribute fiacre.types.StatementList
   */
  @Override
  public fiacre.types.StatementList getHeadPStmList() {
    return _HeadPStmList;
  }

  /**
   * Sets and returns the attribute fiacre.types.PStatementList
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.StatementList which just has been set
   */
  @Override
  public fiacre.types.PStatementList setHeadPStmList(fiacre.types.StatementList set_arg) {
    return make(set_arg, _TailPStmList);
  }
  
  /**
   * Returns the attribute fiacre.types.PStatementList
   *
   * @return the attribute fiacre.types.PStatementList
   */
  @Override
  public fiacre.types.PStatementList getTailPStmList() {
    return _TailPStmList;
  }

  /**
   * Sets and returns the attribute fiacre.types.PStatementList
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PStatementList which just has been set
   */
  @Override
  public fiacre.types.PStatementList setTailPStmList(fiacre.types.PStatementList set_arg) {
    return make(_HeadPStmList, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getHeadPStmList().toATerm(), getTailPStmList().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.PStatementList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PStatementList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.StatementList.fromTerm(appl.getArgument(0),atConv), fiacre.types.PStatementList.fromTerm(appl.getArgument(1),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 2;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadPStmList;
      case 1: return _TailPStmList;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.StatementList) v, _TailPStmList);
      case 1: return make(_HeadPStmList, (fiacre.types.PStatementList) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 2
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 2  && childs[0] instanceof fiacre.types.StatementList && childs[1] instanceof fiacre.types.PStatementList) {
      return make((fiacre.types.StatementList) childs[0], (fiacre.types.PStatementList) childs[1]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _HeadPStmList,  _TailPStmList };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-351369284<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_HeadPStmList.hashCode() << 8);
    a += (_TailPStmList.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
