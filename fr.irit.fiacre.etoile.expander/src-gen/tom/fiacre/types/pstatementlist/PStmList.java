
package fiacre.types.pstatementlist;



public abstract class PStmList extends fiacre.types.PStatementList implements java.util.Collection<fiacre.types.StatementList>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.pstatementlist.ConsPStmList) {
      fiacre.types.PStatementList tl = this.getTailPStmList();
      if (tl instanceof PStmList) {
        return 1+((PStmList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.PStatementList fromArray(fiacre.types.StatementList[] array) {
    fiacre.types.PStatementList res = fiacre.types.pstatementlist.EmptyPStmList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.pstatementlist.ConsPStmList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.PStatementList reverse() {
    if(this instanceof fiacre.types.pstatementlist.ConsPStmList) {
      fiacre.types.PStatementList cur = this;
      fiacre.types.PStatementList rev = fiacre.types.pstatementlist.EmptyPStmList.make();
      while(cur instanceof fiacre.types.pstatementlist.ConsPStmList) {
        rev = fiacre.types.pstatementlist.ConsPStmList.make(cur.getHeadPStmList(),rev);
        cur = cur.getTailPStmList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.PStatementList append(fiacre.types.StatementList element) {
    if(this instanceof fiacre.types.pstatementlist.ConsPStmList) {
      fiacre.types.PStatementList tl = this.getTailPStmList();
      if (tl instanceof PStmList) {
        return fiacre.types.pstatementlist.ConsPStmList.make(this.getHeadPStmList(),((PStmList)tl).append(element));
      } else {

        return fiacre.types.pstatementlist.ConsPStmList.make(this.getHeadPStmList(),fiacre.types.pstatementlist.ConsPStmList.make(element,tl));

      }
    } else {
      return fiacre.types.pstatementlist.ConsPStmList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("PStmList(");
    if(this instanceof fiacre.types.pstatementlist.ConsPStmList) {
      fiacre.types.PStatementList cur = this;
      while(cur instanceof fiacre.types.pstatementlist.ConsPStmList) {
        fiacre.types.StatementList elem = cur.getHeadPStmList();
        cur = cur.getTailPStmList();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.pstatementlist.ConsPStmList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.pstatementlist.EmptyPStmList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.pstatementlist.ConsPStmList) {
      fiacre.types.PStatementList tail = this.getTailPStmList();
      res = atermFactory.makeList(getHeadPStmList().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.PStatementList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.PStatementList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("PStmList".equals(appl.getName())) {
        fiacre.types.PStatementList res = fiacre.types.pstatementlist.EmptyPStmList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.StatementList elem = fiacre.types.StatementList.fromTerm(array[i],atConv);
          res = fiacre.types.pstatementlist.ConsPStmList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.PStatementList res = fiacre.types.pstatementlist.EmptyPStmList.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.StatementList elem = fiacre.types.StatementList.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.pstatementlist.ConsPStmList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.PStatementList contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.PStatementList contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.PStatementList cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.pstatementlist.ConsPStmList) {
      while(cur instanceof fiacre.types.pstatementlist.ConsPStmList) {
        if( o.equals(cur.getHeadPStmList()) ) {
          return true;
        }
        cur = cur.getTailPStmList();
      }
      if(!(cur instanceof fiacre.types.pstatementlist.EmptyPStmList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyPStmList() ; }

  public java.util.Iterator<fiacre.types.StatementList> iterator() {
    return new java.util.Iterator<fiacre.types.StatementList>() {
      fiacre.types.PStatementList list = PStmList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyPStmList();
      }

      public fiacre.types.StatementList next() {
        if(list.isEmptyPStmList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsPStmList()) {
          fiacre.types.StatementList head = list.getHeadPStmList();
          list = list.getTailPStmList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.StatementList)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.StatementList o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.StatementList> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.pstatementlist.ConsPStmList) {
      fiacre.types.PStatementList cur = this;
      while(cur instanceof fiacre.types.pstatementlist.ConsPStmList) {
        fiacre.types.StatementList elem = cur.getHeadPStmList();
        array[i] = elem;
        cur = cur.getTailPStmList();
        i++;
      }
      if(!(cur instanceof fiacre.types.pstatementlist.EmptyPStmList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.pstatementlist.ConsPStmList) {
      fiacre.types.PStatementList cur = this;
      while(cur instanceof fiacre.types.pstatementlist.ConsPStmList) {
        fiacre.types.StatementList elem = cur.getHeadPStmList();
        array[i] = (T)elem;
        cur = cur.getTailPStmList();
        i++;
      }
      if(!(cur instanceof fiacre.types.pstatementlist.EmptyPStmList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.StatementList> getCollection() {
    return new CollectionPStmList(this);
  }

  public java.util.Collection<fiacre.types.StatementList> getCollectionPStmList() {
    return new CollectionPStmList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionPStmList implements java.util.Collection<fiacre.types.StatementList> {
    private PStmList list;

    public PStmList getPStatementList() {
      return list;
    }

    public CollectionPStmList(PStmList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.StatementList> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.StatementList> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getPStatementList().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getPStatementList().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getPStatementList().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getPStatementList().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.StatementList> iterator() {
    return getPStatementList().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getPStatementList().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getPStatementList().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getPStatementList().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getPStatementList().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.StatementList o) {
      list = (PStmList)fiacre.types.pstatementlist.ConsPStmList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (PStmList)fiacre.types.pstatementlist.EmptyPStmList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyPStmList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
