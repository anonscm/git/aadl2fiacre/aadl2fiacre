
package fiacre.types.vardecl;



public final class VarDec extends fiacre.types.VarDecl implements tom.library.sl.Visitable  {
  
  private static String symbolName = "VarDec";


  private VarDec() {}
  private int hashCode;
  private static VarDec gomProto = new VarDec();
    private fiacre.types.StringList _lsl;
  private fiacre.types.Type _type;
  private fiacre.types.Optional_Exp _oe;

  /**
   * Constructor that builds a term rooted by VarDec
   *
   * @return a term rooted by VarDec
   */

  public static VarDec make(fiacre.types.StringList _lsl, fiacre.types.Type _type, fiacre.types.Optional_Exp _oe) {

    // use the proto as a model
    gomProto.initHashCode( _lsl,  _type,  _oe);
    return (VarDec) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _lsl
   * @param _type
   * @param _oe
   * @param hashCode hashCode of VarDec
   */
  private void init(fiacre.types.StringList _lsl, fiacre.types.Type _type, fiacre.types.Optional_Exp _oe, int hashCode) {
    this._lsl = _lsl;
    this._type = _type;
    this._oe = _oe;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _lsl
   * @param _type
   * @param _oe
   */
  private void initHashCode(fiacre.types.StringList _lsl, fiacre.types.Type _type, fiacre.types.Optional_Exp _oe) {
    this._lsl = _lsl;
    this._type = _type;
    this._oe = _oe;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "VarDec";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 3;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    VarDec clone = new VarDec();
    clone.init( _lsl,  _type,  _oe, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("VarDec(");
    _lsl.toStringBuilder(buffer);
buffer.append(",");
    _type.toStringBuilder(buffer);
buffer.append(",");
    _oe.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    VarDec tco = (VarDec) ao;
    int _lslCmp = (this._lsl).compareToLPO(tco._lsl);
    if(_lslCmp != 0) {
      return _lslCmp;
    }

    int _typeCmp = (this._type).compareToLPO(tco._type);
    if(_typeCmp != 0) {
      return _typeCmp;
    }

    int _oeCmp = (this._oe).compareToLPO(tco._oe);
    if(_oeCmp != 0) {
      return _oeCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    VarDec tco = (VarDec) ao;
    int _lslCmp = (this._lsl).compareTo(tco._lsl);
    if(_lslCmp != 0) {
      return _lslCmp;
    }

    int _typeCmp = (this._type).compareTo(tco._type);
    if(_typeCmp != 0) {
      return _typeCmp;
    }

    int _oeCmp = (this._oe).compareTo(tco._oe);
    if(_oeCmp != 0) {
      return _oeCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a VarDec and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof VarDec) {

      VarDec peer = (VarDec) obj;
      return _lsl==peer._lsl && _type==peer._type && _oe==peer._oe && true;
    }
    return false;
  }


   //VarDecl interface
  /**
   * Returns true if the term is rooted by the symbol VarDec
   *
   * @return true, because this is rooted by VarDec
   */
  @Override
  public boolean isVarDec() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.StringList
   *
   * @return the attribute fiacre.types.StringList
   */
  @Override
  public fiacre.types.StringList getlsl() {
    return _lsl;
  }

  /**
   * Sets and returns the attribute fiacre.types.VarDecl
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.StringList which just has been set
   */
  @Override
  public fiacre.types.VarDecl setlsl(fiacre.types.StringList set_arg) {
    return make(set_arg, _type, _oe);
  }
  
  /**
   * Returns the attribute fiacre.types.Type
   *
   * @return the attribute fiacre.types.Type
   */
  @Override
  public fiacre.types.Type gettype() {
    return _type;
  }

  /**
   * Sets and returns the attribute fiacre.types.VarDecl
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Type which just has been set
   */
  @Override
  public fiacre.types.VarDecl settype(fiacre.types.Type set_arg) {
    return make(_lsl, set_arg, _oe);
  }
  
  /**
   * Returns the attribute fiacre.types.Optional_Exp
   *
   * @return the attribute fiacre.types.Optional_Exp
   */
  @Override
  public fiacre.types.Optional_Exp getoe() {
    return _oe;
  }

  /**
   * Sets and returns the attribute fiacre.types.VarDecl
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Optional_Exp which just has been set
   */
  @Override
  public fiacre.types.VarDecl setoe(fiacre.types.Optional_Exp set_arg) {
    return make(_lsl, _type, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getlsl().toATerm(), gettype().toATerm(), getoe().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.VarDecl from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.VarDecl fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.StringList.fromTerm(appl.getArgument(0),atConv), fiacre.types.Type.fromTerm(appl.getArgument(1),atConv), fiacre.types.Optional_Exp.fromTerm(appl.getArgument(2),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 3;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _lsl;
      case 1: return _type;
      case 2: return _oe;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.StringList) v, _type, _oe);
      case 1: return make(_lsl, (fiacre.types.Type) v, _oe);
      case 2: return make(_lsl, _type, (fiacre.types.Optional_Exp) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 3
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 3  && childs[0] instanceof fiacre.types.StringList && childs[1] instanceof fiacre.types.Type && childs[2] instanceof fiacre.types.Optional_Exp) {
      return make((fiacre.types.StringList) childs[0], (fiacre.types.Type) childs[1], (fiacre.types.Optional_Exp) childs[2]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _lsl,  _type,  _oe };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-746187446<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_lsl.hashCode() << 16);
    a += (_type.hashCode() << 8);
    a += (_oe.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
