
package fiacre.types.statement;



public final class StOutput extends fiacre.types.Statement implements tom.library.sl.Visitable  {
  
  private static String symbolName = "StOutput";


  private StOutput() {}
  private int hashCode;
  private static StOutput gomProto = new StOutput();
    private fiacre.types.Exp _po;
  private fiacre.types.Channel _chan;
  private fiacre.types.ExpList _elist;

  /**
   * Constructor that builds a term rooted by StOutput
   *
   * @return a term rooted by StOutput
   */

  public static StOutput make(fiacre.types.Exp _po, fiacre.types.Channel _chan, fiacre.types.ExpList _elist) {

    // use the proto as a model
    gomProto.initHashCode( _po,  _chan,  _elist);
    return (StOutput) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _po
   * @param _chan
   * @param _elist
   * @param hashCode hashCode of StOutput
   */
  private void init(fiacre.types.Exp _po, fiacre.types.Channel _chan, fiacre.types.ExpList _elist, int hashCode) {
    this._po = _po;
    this._chan = _chan;
    this._elist = _elist;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _po
   * @param _chan
   * @param _elist
   */
  private void initHashCode(fiacre.types.Exp _po, fiacre.types.Channel _chan, fiacre.types.ExpList _elist) {
    this._po = _po;
    this._chan = _chan;
    this._elist = _elist;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "StOutput";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 3;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    StOutput clone = new StOutput();
    clone.init( _po,  _chan,  _elist, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("StOutput(");
    _po.toStringBuilder(buffer);
buffer.append(",");
    _chan.toStringBuilder(buffer);
buffer.append(",");
    _elist.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    StOutput tco = (StOutput) ao;
    int _poCmp = (this._po).compareToLPO(tco._po);
    if(_poCmp != 0) {
      return _poCmp;
    }

    int _chanCmp = (this._chan).compareToLPO(tco._chan);
    if(_chanCmp != 0) {
      return _chanCmp;
    }

    int _elistCmp = (this._elist).compareToLPO(tco._elist);
    if(_elistCmp != 0) {
      return _elistCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    StOutput tco = (StOutput) ao;
    int _poCmp = (this._po).compareTo(tco._po);
    if(_poCmp != 0) {
      return _poCmp;
    }

    int _chanCmp = (this._chan).compareTo(tco._chan);
    if(_chanCmp != 0) {
      return _chanCmp;
    }

    int _elistCmp = (this._elist).compareTo(tco._elist);
    if(_elistCmp != 0) {
      return _elistCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a StOutput and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof StOutput) {

      StOutput peer = (StOutput) obj;
      return _po==peer._po && _chan==peer._chan && _elist==peer._elist && true;
    }
    return false;
  }


   //Statement interface
  /**
   * Returns true if the term is rooted by the symbol StOutput
   *
   * @return true, because this is rooted by StOutput
   */
  @Override
  public boolean isStOutput() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp getpo() {
    return _po;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.Statement setpo(fiacre.types.Exp set_arg) {
    return make(set_arg, _chan, _elist);
  }
  
  /**
   * Returns the attribute fiacre.types.Channel
   *
   * @return the attribute fiacre.types.Channel
   */
  @Override
  public fiacre.types.Channel getchan() {
    return _chan;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Channel which just has been set
   */
  @Override
  public fiacre.types.Statement setchan(fiacre.types.Channel set_arg) {
    return make(_po, set_arg, _elist);
  }
  
  /**
   * Returns the attribute fiacre.types.ExpList
   *
   * @return the attribute fiacre.types.ExpList
   */
  @Override
  public fiacre.types.ExpList getelist() {
    return _elist;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.ExpList which just has been set
   */
  @Override
  public fiacre.types.Statement setelist(fiacre.types.ExpList set_arg) {
    return make(_po, _chan, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getpo().toATerm(), getchan().toATerm(), getelist().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Statement from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Statement fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.Exp.fromTerm(appl.getArgument(0),atConv), fiacre.types.Channel.fromTerm(appl.getArgument(1),atConv), fiacre.types.ExpList.fromTerm(appl.getArgument(2),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 3;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _po;
      case 1: return _chan;
      case 2: return _elist;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.Exp) v, _chan, _elist);
      case 1: return make(_po, (fiacre.types.Channel) v, _elist);
      case 2: return make(_po, _chan, (fiacre.types.ExpList) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 3
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 3  && childs[0] instanceof fiacre.types.Exp && childs[1] instanceof fiacre.types.Channel && childs[2] instanceof fiacre.types.ExpList) {
      return make((fiacre.types.Exp) childs[0], (fiacre.types.Channel) childs[1], (fiacre.types.ExpList) childs[2]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _po,  _chan,  _elist };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-1767030646<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_po.hashCode() << 16);
    a += (_chan.hashCode() << 8);
    a += (_elist.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
