
package fiacre.types.statement;



public final class StCase extends fiacre.types.Statement implements tom.library.sl.Visitable  {
  
  private static String symbolName = "StCase";


  private StCase() {}
  private int hashCode;
  private static StCase gomProto = new StCase();
    private fiacre.types.Exp _e;
  private fiacre.types.CaseElementList _cel;

  /**
   * Constructor that builds a term rooted by StCase
   *
   * @return a term rooted by StCase
   */

  public static StCase make(fiacre.types.Exp _e, fiacre.types.CaseElementList _cel) {

    // use the proto as a model
    gomProto.initHashCode( _e,  _cel);
    return (StCase) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _e
   * @param _cel
   * @param hashCode hashCode of StCase
   */
  private void init(fiacre.types.Exp _e, fiacre.types.CaseElementList _cel, int hashCode) {
    this._e = _e;
    this._cel = _cel;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _e
   * @param _cel
   */
  private void initHashCode(fiacre.types.Exp _e, fiacre.types.CaseElementList _cel) {
    this._e = _e;
    this._cel = _cel;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "StCase";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 2;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    StCase clone = new StCase();
    clone.init( _e,  _cel, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("StCase(");
    _e.toStringBuilder(buffer);
buffer.append(",");
    _cel.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    StCase tco = (StCase) ao;
    int _eCmp = (this._e).compareToLPO(tco._e);
    if(_eCmp != 0) {
      return _eCmp;
    }

    int _celCmp = (this._cel).compareToLPO(tco._cel);
    if(_celCmp != 0) {
      return _celCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    StCase tco = (StCase) ao;
    int _eCmp = (this._e).compareTo(tco._e);
    if(_eCmp != 0) {
      return _eCmp;
    }

    int _celCmp = (this._cel).compareTo(tco._cel);
    if(_celCmp != 0) {
      return _celCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a StCase and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof StCase) {

      StCase peer = (StCase) obj;
      return _e==peer._e && _cel==peer._cel && true;
    }
    return false;
  }


   //Statement interface
  /**
   * Returns true if the term is rooted by the symbol StCase
   *
   * @return true, because this is rooted by StCase
   */
  @Override
  public boolean isStCase() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp gete() {
    return _e;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.Statement sete(fiacre.types.Exp set_arg) {
    return make(set_arg, _cel);
  }
  
  /**
   * Returns the attribute fiacre.types.CaseElementList
   *
   * @return the attribute fiacre.types.CaseElementList
   */
  @Override
  public fiacre.types.CaseElementList getcel() {
    return _cel;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.CaseElementList which just has been set
   */
  @Override
  public fiacre.types.Statement setcel(fiacre.types.CaseElementList set_arg) {
    return make(_e, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {gete().toATerm(), getcel().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Statement from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Statement fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.Exp.fromTerm(appl.getArgument(0),atConv), fiacre.types.CaseElementList.fromTerm(appl.getArgument(1),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 2;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _e;
      case 1: return _cel;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.Exp) v, _cel);
      case 1: return make(_e, (fiacre.types.CaseElementList) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 2
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 2  && childs[0] instanceof fiacre.types.Exp && childs[1] instanceof fiacre.types.CaseElementList) {
      return make((fiacre.types.Exp) childs[0], (fiacre.types.CaseElementList) childs[1]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _e,  _cel };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-1921958893<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_e.hashCode() << 8);
    a += (_cel.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
