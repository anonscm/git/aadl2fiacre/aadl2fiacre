
package fiacre.types.statement;



public final class StCond extends fiacre.types.Statement implements tom.library.sl.Visitable  {
  
  private static String symbolName = "StCond";


  private StCond() {}
  private int hashCode;
  private static StCond gomProto = new StCond();
    private fiacre.types.Exp _cond;
  private fiacre.types.Statement _st1;
  private fiacre.types.ConditionalStatementList _condst;
  private fiacre.types.Optional_Statement _ost;

  /**
   * Constructor that builds a term rooted by StCond
   *
   * @return a term rooted by StCond
   */

  public static StCond make(fiacre.types.Exp _cond, fiacre.types.Statement _st1, fiacre.types.ConditionalStatementList _condst, fiacre.types.Optional_Statement _ost) {

    // use the proto as a model
    gomProto.initHashCode( _cond,  _st1,  _condst,  _ost);
    return (StCond) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _cond
   * @param _st1
   * @param _condst
   * @param _ost
   * @param hashCode hashCode of StCond
   */
  private void init(fiacre.types.Exp _cond, fiacre.types.Statement _st1, fiacre.types.ConditionalStatementList _condst, fiacre.types.Optional_Statement _ost, int hashCode) {
    this._cond = _cond;
    this._st1 = _st1;
    this._condst = _condst;
    this._ost = _ost;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _cond
   * @param _st1
   * @param _condst
   * @param _ost
   */
  private void initHashCode(fiacre.types.Exp _cond, fiacre.types.Statement _st1, fiacre.types.ConditionalStatementList _condst, fiacre.types.Optional_Statement _ost) {
    this._cond = _cond;
    this._st1 = _st1;
    this._condst = _condst;
    this._ost = _ost;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "StCond";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 4;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    StCond clone = new StCond();
    clone.init( _cond,  _st1,  _condst,  _ost, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("StCond(");
    _cond.toStringBuilder(buffer);
buffer.append(",");
    _st1.toStringBuilder(buffer);
buffer.append(",");
    _condst.toStringBuilder(buffer);
buffer.append(",");
    _ost.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    StCond tco = (StCond) ao;
    int _condCmp = (this._cond).compareToLPO(tco._cond);
    if(_condCmp != 0) {
      return _condCmp;
    }

    int _st1Cmp = (this._st1).compareToLPO(tco._st1);
    if(_st1Cmp != 0) {
      return _st1Cmp;
    }

    int _condstCmp = (this._condst).compareToLPO(tco._condst);
    if(_condstCmp != 0) {
      return _condstCmp;
    }

    int _ostCmp = (this._ost).compareToLPO(tco._ost);
    if(_ostCmp != 0) {
      return _ostCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    StCond tco = (StCond) ao;
    int _condCmp = (this._cond).compareTo(tco._cond);
    if(_condCmp != 0) {
      return _condCmp;
    }

    int _st1Cmp = (this._st1).compareTo(tco._st1);
    if(_st1Cmp != 0) {
      return _st1Cmp;
    }

    int _condstCmp = (this._condst).compareTo(tco._condst);
    if(_condstCmp != 0) {
      return _condstCmp;
    }

    int _ostCmp = (this._ost).compareTo(tco._ost);
    if(_ostCmp != 0) {
      return _ostCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a StCond and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof StCond) {

      StCond peer = (StCond) obj;
      return _cond==peer._cond && _st1==peer._st1 && _condst==peer._condst && _ost==peer._ost && true;
    }
    return false;
  }


   //Statement interface
  /**
   * Returns true if the term is rooted by the symbol StCond
   *
   * @return true, because this is rooted by StCond
   */
  @Override
  public boolean isStCond() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp getcond() {
    return _cond;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.Statement setcond(fiacre.types.Exp set_arg) {
    return make(set_arg, _st1, _condst, _ost);
  }
  
  /**
   * Returns the attribute fiacre.types.Statement
   *
   * @return the attribute fiacre.types.Statement
   */
  @Override
  public fiacre.types.Statement getst1() {
    return _st1;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Statement which just has been set
   */
  @Override
  public fiacre.types.Statement setst1(fiacre.types.Statement set_arg) {
    return make(_cond, set_arg, _condst, _ost);
  }
  
  /**
   * Returns the attribute fiacre.types.ConditionalStatementList
   *
   * @return the attribute fiacre.types.ConditionalStatementList
   */
  @Override
  public fiacre.types.ConditionalStatementList getcondst() {
    return _condst;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.ConditionalStatementList which just has been set
   */
  @Override
  public fiacre.types.Statement setcondst(fiacre.types.ConditionalStatementList set_arg) {
    return make(_cond, _st1, set_arg, _ost);
  }
  
  /**
   * Returns the attribute fiacre.types.Optional_Statement
   *
   * @return the attribute fiacre.types.Optional_Statement
   */
  @Override
  public fiacre.types.Optional_Statement getost() {
    return _ost;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Optional_Statement which just has been set
   */
  @Override
  public fiacre.types.Statement setost(fiacre.types.Optional_Statement set_arg) {
    return make(_cond, _st1, _condst, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getcond().toATerm(), getst1().toATerm(), getcondst().toATerm(), getost().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Statement from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Statement fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.Exp.fromTerm(appl.getArgument(0),atConv), fiacre.types.Statement.fromTerm(appl.getArgument(1),atConv), fiacre.types.ConditionalStatementList.fromTerm(appl.getArgument(2),atConv), fiacre.types.Optional_Statement.fromTerm(appl.getArgument(3),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 4;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _cond;
      case 1: return _st1;
      case 2: return _condst;
      case 3: return _ost;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.Exp) v, _st1, _condst, _ost);
      case 1: return make(_cond, (fiacre.types.Statement) v, _condst, _ost);
      case 2: return make(_cond, _st1, (fiacre.types.ConditionalStatementList) v, _ost);
      case 3: return make(_cond, _st1, _condst, (fiacre.types.Optional_Statement) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 4
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 4  && childs[0] instanceof fiacre.types.Exp && childs[1] instanceof fiacre.types.Statement && childs[2] instanceof fiacre.types.ConditionalStatementList && childs[3] instanceof fiacre.types.Optional_Statement) {
      return make((fiacre.types.Exp) childs[0], (fiacre.types.Statement) childs[1], (fiacre.types.ConditionalStatementList) childs[2], (fiacre.types.Optional_Statement) childs[3]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _cond,  _st1,  _condst,  _ost };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-934133010<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_cond.hashCode() << 24);
    a += (_st1.hashCode() << 16);
    a += (_condst.hashCode() << 8);
    a += (_ost.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
