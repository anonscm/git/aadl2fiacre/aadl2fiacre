
package fiacre.types.statement;



public final class StWhile extends fiacre.types.Statement implements tom.library.sl.Visitable  {
  
  private static String symbolName = "StWhile";


  private StWhile() {}
  private int hashCode;
  private static StWhile gomProto = new StWhile();
    private fiacre.types.Exp _cond;
  private fiacre.types.Statement _st;

  /**
   * Constructor that builds a term rooted by StWhile
   *
   * @return a term rooted by StWhile
   */

  public static StWhile make(fiacre.types.Exp _cond, fiacre.types.Statement _st) {

    // use the proto as a model
    gomProto.initHashCode( _cond,  _st);
    return (StWhile) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _cond
   * @param _st
   * @param hashCode hashCode of StWhile
   */
  private void init(fiacre.types.Exp _cond, fiacre.types.Statement _st, int hashCode) {
    this._cond = _cond;
    this._st = _st;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _cond
   * @param _st
   */
  private void initHashCode(fiacre.types.Exp _cond, fiacre.types.Statement _st) {
    this._cond = _cond;
    this._st = _st;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "StWhile";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 2;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    StWhile clone = new StWhile();
    clone.init( _cond,  _st, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("StWhile(");
    _cond.toStringBuilder(buffer);
buffer.append(",");
    _st.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    StWhile tco = (StWhile) ao;
    int _condCmp = (this._cond).compareToLPO(tco._cond);
    if(_condCmp != 0) {
      return _condCmp;
    }

    int _stCmp = (this._st).compareToLPO(tco._st);
    if(_stCmp != 0) {
      return _stCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    StWhile tco = (StWhile) ao;
    int _condCmp = (this._cond).compareTo(tco._cond);
    if(_condCmp != 0) {
      return _condCmp;
    }

    int _stCmp = (this._st).compareTo(tco._st);
    if(_stCmp != 0) {
      return _stCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a StWhile and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof StWhile) {

      StWhile peer = (StWhile) obj;
      return _cond==peer._cond && _st==peer._st && true;
    }
    return false;
  }


   //Statement interface
  /**
   * Returns true if the term is rooted by the symbol StWhile
   *
   * @return true, because this is rooted by StWhile
   */
  @Override
  public boolean isStWhile() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp getcond() {
    return _cond;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.Statement setcond(fiacre.types.Exp set_arg) {
    return make(set_arg, _st);
  }
  
  /**
   * Returns the attribute fiacre.types.Statement
   *
   * @return the attribute fiacre.types.Statement
   */
  @Override
  public fiacre.types.Statement getst() {
    return _st;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Statement which just has been set
   */
  @Override
  public fiacre.types.Statement setst(fiacre.types.Statement set_arg) {
    return make(_cond, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getcond().toATerm(), getst().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Statement from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Statement fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.Exp.fromTerm(appl.getArgument(0),atConv), fiacre.types.Statement.fromTerm(appl.getArgument(1),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 2;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _cond;
      case 1: return _st;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.Exp) v, _st);
      case 1: return make(_cond, (fiacre.types.Statement) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 2
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 2  && childs[0] instanceof fiacre.types.Exp && childs[1] instanceof fiacre.types.Statement) {
      return make((fiacre.types.Exp) childs[0], (fiacre.types.Statement) childs[1]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _cond,  _st };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-764879895<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_cond.hashCode() << 8);
    a += (_st.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
