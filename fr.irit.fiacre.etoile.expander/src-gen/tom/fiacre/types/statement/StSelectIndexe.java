
package fiacre.types.statement;



public final class StSelectIndexe extends fiacre.types.Statement implements tom.library.sl.Visitable  {
  
  private static String symbolName = "StSelectIndexe";


  private StSelectIndexe() {}
  private int hashCode;
  private static StSelectIndexe gomProto = new StSelectIndexe();
    private String _var;
  private fiacre.types.Exp _deb;
  private fiacre.types.Exp _fin;
  private fiacre.types.PStatementList _stl;

  /**
   * Constructor that builds a term rooted by StSelectIndexe
   *
   * @return a term rooted by StSelectIndexe
   */

  public static StSelectIndexe make(String _var, fiacre.types.Exp _deb, fiacre.types.Exp _fin, fiacre.types.PStatementList _stl) {

    // use the proto as a model
    gomProto.initHashCode( _var,  _deb,  _fin,  _stl);
    return (StSelectIndexe) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _var
   * @param _deb
   * @param _fin
   * @param _stl
   * @param hashCode hashCode of StSelectIndexe
   */
  private void init(String _var, fiacre.types.Exp _deb, fiacre.types.Exp _fin, fiacre.types.PStatementList _stl, int hashCode) {
    this._var = _var.intern();
    this._deb = _deb;
    this._fin = _fin;
    this._stl = _stl;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _var
   * @param _deb
   * @param _fin
   * @param _stl
   */
  private void initHashCode(String _var, fiacre.types.Exp _deb, fiacre.types.Exp _fin, fiacre.types.PStatementList _stl) {
    this._var = _var.intern();
    this._deb = _deb;
    this._fin = _fin;
    this._stl = _stl;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "StSelectIndexe";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 4;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    StSelectIndexe clone = new StSelectIndexe();
    clone.init( _var,  _deb,  _fin,  _stl, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("StSelectIndexe(");
    buffer.append('"');
            for (int i = 0; i < _var.length(); i++) {
              char c = _var.charAt(i);
              switch (c) {
                case '\n':
                  buffer.append('\\');
                  buffer.append('n');
                  break;
                case '\t':
                  buffer.append('\\');
                  buffer.append('t');
                  break;
                case '\b':
                  buffer.append('\\');
                  buffer.append('b');
                  break;
                case '\r':
                  buffer.append('\\');
                  buffer.append('r');
                  break;
                case '\f':
                  buffer.append('\\');
                  buffer.append('f');
                  break;
                case '\\':
                  buffer.append('\\');
                  buffer.append('\\');
                  break;
                case '\'':
                  buffer.append('\\');
                  buffer.append('\'');
                  break;
                case '\"':
                  buffer.append('\\');
                  buffer.append('\"');
                  break;
                case '!':
                case '@':
                case '#':
                case '$':
                case '%':
                case '^':
                case '&':
                case '*':
                case '(':
                case ')':
                case '-':
                case '_':
                case '+':
                case '=':
                case '|':
                case '~':
                case '{':
                case '}':
                case '[':
                case ']':
                case ';':
                case ':':
                case '<':
                case '>':
                case ',':
                case '.':
                case '?':
                case ' ':
                case '/':
                  buffer.append(c);
                  break;

                default:
                  if (java.lang.Character.isLetterOrDigit(c)) {
                    buffer.append(c);
                  } else {
                    buffer.append('\\');
                    buffer.append((char) ('0' + c / 64));
                    c = (char) (c % 64);
                    buffer.append((char) ('0' + c / 8));
                    c = (char) (c % 8);
                    buffer.append((char) ('0' + c));
                  }
              }
            }
            buffer.append('"');
buffer.append(",");
    _deb.toStringBuilder(buffer);
buffer.append(",");
    _fin.toStringBuilder(buffer);
buffer.append(",");
    _stl.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    StSelectIndexe tco = (StSelectIndexe) ao;
    int _varCmp = (this._var).compareTo(tco._var);
    if(_varCmp != 0) {
      return _varCmp;
    }


    int _debCmp = (this._deb).compareToLPO(tco._deb);
    if(_debCmp != 0) {
      return _debCmp;
    }

    int _finCmp = (this._fin).compareToLPO(tco._fin);
    if(_finCmp != 0) {
      return _finCmp;
    }

    int _stlCmp = (this._stl).compareToLPO(tco._stl);
    if(_stlCmp != 0) {
      return _stlCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    StSelectIndexe tco = (StSelectIndexe) ao;
    int _varCmp = (this._var).compareTo(tco._var);
    if(_varCmp != 0) {
      return _varCmp;
    }


    int _debCmp = (this._deb).compareTo(tco._deb);
    if(_debCmp != 0) {
      return _debCmp;
    }

    int _finCmp = (this._fin).compareTo(tco._fin);
    if(_finCmp != 0) {
      return _finCmp;
    }

    int _stlCmp = (this._stl).compareTo(tco._stl);
    if(_stlCmp != 0) {
      return _stlCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a StSelectIndexe and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof StSelectIndexe) {

      StSelectIndexe peer = (StSelectIndexe) obj;
      return _var==peer._var && _deb==peer._deb && _fin==peer._fin && _stl==peer._stl && true;
    }
    return false;
  }


   //Statement interface
  /**
   * Returns true if the term is rooted by the symbol StSelectIndexe
   *
   * @return true, because this is rooted by StSelectIndexe
   */
  @Override
  public boolean isStSelectIndexe() {
    return true;
  }
  
  /**
   * Returns the attribute String
   *
   * @return the attribute String
   */
  @Override
  public String getvar() {
    return _var;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute String which just has been set
   */
  @Override
  public fiacre.types.Statement setvar(String set_arg) {
    return make(set_arg, _deb, _fin, _stl);
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp getdeb() {
    return _deb;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.Statement setdeb(fiacre.types.Exp set_arg) {
    return make(_var, set_arg, _fin, _stl);
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp getfin() {
    return _fin;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.Statement setfin(fiacre.types.Exp set_arg) {
    return make(_var, _deb, set_arg, _stl);
  }
  
  /**
   * Returns the attribute fiacre.types.PStatementList
   *
   * @return the attribute fiacre.types.PStatementList
   */
  @Override
  public fiacre.types.PStatementList getstl() {
    return _stl;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.PStatementList which just has been set
   */
  @Override
  public fiacre.types.Statement setstl(fiacre.types.PStatementList set_arg) {
    return make(_var, _deb, _fin, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {(aterm.ATerm) atermFactory.makeAppl(atermFactory.makeAFun(getvar() ,0 , true)), getdeb().toATerm(), getfin().toATerm(), getstl().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Statement from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Statement fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
convertATermToString(appl.getArgument(0), atConv), fiacre.types.Exp.fromTerm(appl.getArgument(1),atConv), fiacre.types.Exp.fromTerm(appl.getArgument(2),atConv), fiacre.types.PStatementList.fromTerm(appl.getArgument(3),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 4;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return new tom.library.sl.VisitableBuiltin<String>(_var);
      case 1: return _deb;
      case 2: return _fin;
      case 3: return _stl;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make(getvar(), _deb, _fin, _stl);
      case 1: return make(getvar(), (fiacre.types.Exp) v, _fin, _stl);
      case 2: return make(getvar(), _deb, (fiacre.types.Exp) v, _stl);
      case 3: return make(getvar(), _deb, _fin, (fiacre.types.PStatementList) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 4
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 4  && childs[0] instanceof tom.library.sl.VisitableBuiltin && childs[1] instanceof fiacre.types.Exp && childs[2] instanceof fiacre.types.Exp && childs[3] instanceof fiacre.types.PStatementList) {
      return make(((tom.library.sl.VisitableBuiltin<String>)childs[0]).getBuiltin(), (fiacre.types.Exp) childs[1], (fiacre.types.Exp) childs[2], (fiacre.types.PStatementList) childs[3]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  new tom.library.sl.VisitableBuiltin<String>(_var),  _deb,  _fin,  _stl };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-1848870996<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (shared.HashFunctions.stringHashFunction(_var, 3) << 24);
    a += (_deb.hashCode() << 16);
    a += (_fin.hashCode() << 8);
    a += (_stl.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
