
package fiacre.types.statement;



public final class StAny extends fiacre.types.Statement implements tom.library.sl.Visitable  {
  
  private static String symbolName = "StAny";


  private StAny() {}
  private int hashCode;
  private static StAny gomProto = new StAny();
    private fiacre.types.ExpList _p;
  private fiacre.types.TypeList _types;
  private fiacre.types.Optional_Exp _oe;

  /**
   * Constructor that builds a term rooted by StAny
   *
   * @return a term rooted by StAny
   */

  public static StAny make(fiacre.types.ExpList _p, fiacre.types.TypeList _types, fiacre.types.Optional_Exp _oe) {

    // use the proto as a model
    gomProto.initHashCode( _p,  _types,  _oe);
    return (StAny) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _p
   * @param _types
   * @param _oe
   * @param hashCode hashCode of StAny
   */
  private void init(fiacre.types.ExpList _p, fiacre.types.TypeList _types, fiacre.types.Optional_Exp _oe, int hashCode) {
    this._p = _p;
    this._types = _types;
    this._oe = _oe;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _p
   * @param _types
   * @param _oe
   */
  private void initHashCode(fiacre.types.ExpList _p, fiacre.types.TypeList _types, fiacre.types.Optional_Exp _oe) {
    this._p = _p;
    this._types = _types;
    this._oe = _oe;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "StAny";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 3;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    StAny clone = new StAny();
    clone.init( _p,  _types,  _oe, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("StAny(");
    _p.toStringBuilder(buffer);
buffer.append(",");
    _types.toStringBuilder(buffer);
buffer.append(",");
    _oe.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    StAny tco = (StAny) ao;
    int _pCmp = (this._p).compareToLPO(tco._p);
    if(_pCmp != 0) {
      return _pCmp;
    }

    int _typesCmp = (this._types).compareToLPO(tco._types);
    if(_typesCmp != 0) {
      return _typesCmp;
    }

    int _oeCmp = (this._oe).compareToLPO(tco._oe);
    if(_oeCmp != 0) {
      return _oeCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    StAny tco = (StAny) ao;
    int _pCmp = (this._p).compareTo(tco._p);
    if(_pCmp != 0) {
      return _pCmp;
    }

    int _typesCmp = (this._types).compareTo(tco._types);
    if(_typesCmp != 0) {
      return _typesCmp;
    }

    int _oeCmp = (this._oe).compareTo(tco._oe);
    if(_oeCmp != 0) {
      return _oeCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a StAny and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof StAny) {

      StAny peer = (StAny) obj;
      return _p==peer._p && _types==peer._types && _oe==peer._oe && true;
    }
    return false;
  }


   //Statement interface
  /**
   * Returns true if the term is rooted by the symbol StAny
   *
   * @return true, because this is rooted by StAny
   */
  @Override
  public boolean isStAny() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.ExpList
   *
   * @return the attribute fiacre.types.ExpList
   */
  @Override
  public fiacre.types.ExpList getp() {
    return _p;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.ExpList which just has been set
   */
  @Override
  public fiacre.types.Statement setp(fiacre.types.ExpList set_arg) {
    return make(set_arg, _types, _oe);
  }
  
  /**
   * Returns the attribute fiacre.types.TypeList
   *
   * @return the attribute fiacre.types.TypeList
   */
  @Override
  public fiacre.types.TypeList gettypes() {
    return _types;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.TypeList which just has been set
   */
  @Override
  public fiacre.types.Statement settypes(fiacre.types.TypeList set_arg) {
    return make(_p, set_arg, _oe);
  }
  
  /**
   * Returns the attribute fiacre.types.Optional_Exp
   *
   * @return the attribute fiacre.types.Optional_Exp
   */
  @Override
  public fiacre.types.Optional_Exp getoe() {
    return _oe;
  }

  /**
   * Sets and returns the attribute fiacre.types.Statement
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Optional_Exp which just has been set
   */
  @Override
  public fiacre.types.Statement setoe(fiacre.types.Optional_Exp set_arg) {
    return make(_p, _types, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getp().toATerm(), gettypes().toATerm(), getoe().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Statement from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Statement fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.ExpList.fromTerm(appl.getArgument(0),atConv), fiacre.types.TypeList.fromTerm(appl.getArgument(1),atConv), fiacre.types.Optional_Exp.fromTerm(appl.getArgument(2),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 3;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _p;
      case 1: return _types;
      case 2: return _oe;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.ExpList) v, _types, _oe);
      case 1: return make(_p, (fiacre.types.TypeList) v, _oe);
      case 2: return make(_p, _types, (fiacre.types.Optional_Exp) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 3
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 3  && childs[0] instanceof fiacre.types.ExpList && childs[1] instanceof fiacre.types.TypeList && childs[2] instanceof fiacre.types.Optional_Exp) {
      return make((fiacre.types.ExpList) childs[0], (fiacre.types.TypeList) childs[1], (fiacre.types.Optional_Exp) childs[2]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _p,  _types,  _oe };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (-1704618897<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_p.hashCode() << 16);
    a += (_types.hashCode() << 8);
    a += (_oe.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
