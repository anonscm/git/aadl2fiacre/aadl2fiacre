
package fiacre.types.ltl;



public final class LtlAll extends fiacre.types.LTL implements tom.library.sl.Visitable  {
  
  private static String symbolName = "LtlAll";


  private LtlAll() {}
  private int hashCode;
  private static LtlAll gomProto = new LtlAll();
    private String _ind;
  private fiacre.types.Exp _begin;
  private fiacre.types.Exp _end;
  private fiacre.types.LTL _l;

  /**
   * Constructor that builds a term rooted by LtlAll
   *
   * @return a term rooted by LtlAll
   */

  public static LtlAll make(String _ind, fiacre.types.Exp _begin, fiacre.types.Exp _end, fiacre.types.LTL _l) {

    // use the proto as a model
    gomProto.initHashCode( _ind,  _begin,  _end,  _l);
    return (LtlAll) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _ind
   * @param _begin
   * @param _end
   * @param _l
   * @param hashCode hashCode of LtlAll
   */
  private void init(String _ind, fiacre.types.Exp _begin, fiacre.types.Exp _end, fiacre.types.LTL _l, int hashCode) {
    this._ind = _ind.intern();
    this._begin = _begin;
    this._end = _end;
    this._l = _l;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _ind
   * @param _begin
   * @param _end
   * @param _l
   */
  private void initHashCode(String _ind, fiacre.types.Exp _begin, fiacre.types.Exp _end, fiacre.types.LTL _l) {
    this._ind = _ind.intern();
    this._begin = _begin;
    this._end = _end;
    this._l = _l;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "LtlAll";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 4;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    LtlAll clone = new LtlAll();
    clone.init( _ind,  _begin,  _end,  _l, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("LtlAll(");
    buffer.append('"');
            for (int i = 0; i < _ind.length(); i++) {
              char c = _ind.charAt(i);
              switch (c) {
                case '\n':
                  buffer.append('\\');
                  buffer.append('n');
                  break;
                case '\t':
                  buffer.append('\\');
                  buffer.append('t');
                  break;
                case '\b':
                  buffer.append('\\');
                  buffer.append('b');
                  break;
                case '\r':
                  buffer.append('\\');
                  buffer.append('r');
                  break;
                case '\f':
                  buffer.append('\\');
                  buffer.append('f');
                  break;
                case '\\':
                  buffer.append('\\');
                  buffer.append('\\');
                  break;
                case '\'':
                  buffer.append('\\');
                  buffer.append('\'');
                  break;
                case '\"':
                  buffer.append('\\');
                  buffer.append('\"');
                  break;
                case '!':
                case '@':
                case '#':
                case '$':
                case '%':
                case '^':
                case '&':
                case '*':
                case '(':
                case ')':
                case '-':
                case '_':
                case '+':
                case '=':
                case '|':
                case '~':
                case '{':
                case '}':
                case '[':
                case ']':
                case ';':
                case ':':
                case '<':
                case '>':
                case ',':
                case '.':
                case '?':
                case ' ':
                case '/':
                  buffer.append(c);
                  break;

                default:
                  if (java.lang.Character.isLetterOrDigit(c)) {
                    buffer.append(c);
                  } else {
                    buffer.append('\\');
                    buffer.append((char) ('0' + c / 64));
                    c = (char) (c % 64);
                    buffer.append((char) ('0' + c / 8));
                    c = (char) (c % 8);
                    buffer.append((char) ('0' + c));
                  }
              }
            }
            buffer.append('"');
buffer.append(",");
    _begin.toStringBuilder(buffer);
buffer.append(",");
    _end.toStringBuilder(buffer);
buffer.append(",");
    _l.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    LtlAll tco = (LtlAll) ao;
    int _indCmp = (this._ind).compareTo(tco._ind);
    if(_indCmp != 0) {
      return _indCmp;
    }


    int _beginCmp = (this._begin).compareToLPO(tco._begin);
    if(_beginCmp != 0) {
      return _beginCmp;
    }

    int _endCmp = (this._end).compareToLPO(tco._end);
    if(_endCmp != 0) {
      return _endCmp;
    }

    int _lCmp = (this._l).compareToLPO(tco._l);
    if(_lCmp != 0) {
      return _lCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    LtlAll tco = (LtlAll) ao;
    int _indCmp = (this._ind).compareTo(tco._ind);
    if(_indCmp != 0) {
      return _indCmp;
    }


    int _beginCmp = (this._begin).compareTo(tco._begin);
    if(_beginCmp != 0) {
      return _beginCmp;
    }

    int _endCmp = (this._end).compareTo(tco._end);
    if(_endCmp != 0) {
      return _endCmp;
    }

    int _lCmp = (this._l).compareTo(tco._l);
    if(_lCmp != 0) {
      return _lCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a LtlAll and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof LtlAll) {

      LtlAll peer = (LtlAll) obj;
      return _ind==peer._ind && _begin==peer._begin && _end==peer._end && _l==peer._l && true;
    }
    return false;
  }


   //LTL interface
  /**
   * Returns true if the term is rooted by the symbol LtlAll
   *
   * @return true, because this is rooted by LtlAll
   */
  @Override
  public boolean isLtlAll() {
    return true;
  }
  
  /**
   * Returns the attribute String
   *
   * @return the attribute String
   */
  @Override
  public String getind() {
    return _ind;
  }

  /**
   * Sets and returns the attribute fiacre.types.LTL
   *
   * @param set_arg the argument to set
   * @return the attribute String which just has been set
   */
  @Override
  public fiacre.types.LTL setind(String set_arg) {
    return make(set_arg, _begin, _end, _l);
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp getbegin() {
    return _begin;
  }

  /**
   * Sets and returns the attribute fiacre.types.LTL
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.LTL setbegin(fiacre.types.Exp set_arg) {
    return make(_ind, set_arg, _end, _l);
  }
  
  /**
   * Returns the attribute fiacre.types.Exp
   *
   * @return the attribute fiacre.types.Exp
   */
  @Override
  public fiacre.types.Exp getend() {
    return _end;
  }

  /**
   * Sets and returns the attribute fiacre.types.LTL
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Exp which just has been set
   */
  @Override
  public fiacre.types.LTL setend(fiacre.types.Exp set_arg) {
    return make(_ind, _begin, set_arg, _l);
  }
  
  /**
   * Returns the attribute fiacre.types.LTL
   *
   * @return the attribute fiacre.types.LTL
   */
  @Override
  public fiacre.types.LTL getl() {
    return _l;
  }

  /**
   * Sets and returns the attribute fiacre.types.LTL
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.LTL which just has been set
   */
  @Override
  public fiacre.types.LTL setl(fiacre.types.LTL set_arg) {
    return make(_ind, _begin, _end, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {(aterm.ATerm) atermFactory.makeAppl(atermFactory.makeAFun(getind() ,0 , true)), getbegin().toATerm(), getend().toATerm(), getl().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.LTL from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.LTL fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
convertATermToString(appl.getArgument(0), atConv), fiacre.types.Exp.fromTerm(appl.getArgument(1),atConv), fiacre.types.Exp.fromTerm(appl.getArgument(2),atConv), fiacre.types.LTL.fromTerm(appl.getArgument(3),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 4;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return new tom.library.sl.VisitableBuiltin<String>(_ind);
      case 1: return _begin;
      case 2: return _end;
      case 3: return _l;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make(getind(), _begin, _end, _l);
      case 1: return make(getind(), (fiacre.types.Exp) v, _end, _l);
      case 2: return make(getind(), _begin, (fiacre.types.Exp) v, _l);
      case 3: return make(getind(), _begin, _end, (fiacre.types.LTL) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 4
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 4  && childs[0] instanceof tom.library.sl.VisitableBuiltin && childs[1] instanceof fiacre.types.Exp && childs[2] instanceof fiacre.types.Exp && childs[3] instanceof fiacre.types.LTL) {
      return make(((tom.library.sl.VisitableBuiltin<String>)childs[0]).getBuiltin(), (fiacre.types.Exp) childs[1], (fiacre.types.Exp) childs[2], (fiacre.types.LTL) childs[3]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  new tom.library.sl.VisitableBuiltin<String>(_ind),  _begin,  _end,  _l };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (2033205176<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (shared.HashFunctions.stringHashFunction(_ind, 3) << 24);
    a += (_begin.hashCode() << 16);
    a += (_end.hashCode() << 8);
    a += (_l.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
