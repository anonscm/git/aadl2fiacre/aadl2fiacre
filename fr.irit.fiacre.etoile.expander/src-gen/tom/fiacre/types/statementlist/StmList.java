
package fiacre.types.statementlist;



public abstract class StmList extends fiacre.types.StatementList implements java.util.Collection<fiacre.types.Statement>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.statementlist.ConsStmList) {
      fiacre.types.StatementList tl = this.getTailStmList();
      if (tl instanceof StmList) {
        return 1+((StmList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.StatementList fromArray(fiacre.types.Statement[] array) {
    fiacre.types.StatementList res = fiacre.types.statementlist.EmptyStmList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.statementlist.ConsStmList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.StatementList reverse() {
    if(this instanceof fiacre.types.statementlist.ConsStmList) {
      fiacre.types.StatementList cur = this;
      fiacre.types.StatementList rev = fiacre.types.statementlist.EmptyStmList.make();
      while(cur instanceof fiacre.types.statementlist.ConsStmList) {
        rev = fiacre.types.statementlist.ConsStmList.make(cur.getHeadStmList(),rev);
        cur = cur.getTailStmList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.StatementList append(fiacre.types.Statement element) {
    if(this instanceof fiacre.types.statementlist.ConsStmList) {
      fiacre.types.StatementList tl = this.getTailStmList();
      if (tl instanceof StmList) {
        return fiacre.types.statementlist.ConsStmList.make(this.getHeadStmList(),((StmList)tl).append(element));
      } else {

        return fiacre.types.statementlist.ConsStmList.make(this.getHeadStmList(),fiacre.types.statementlist.ConsStmList.make(element,tl));

      }
    } else {
      return fiacre.types.statementlist.ConsStmList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("StmList(");
    if(this instanceof fiacre.types.statementlist.ConsStmList) {
      fiacre.types.StatementList cur = this;
      while(cur instanceof fiacre.types.statementlist.ConsStmList) {
        fiacre.types.Statement elem = cur.getHeadStmList();
        cur = cur.getTailStmList();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.statementlist.ConsStmList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.statementlist.EmptyStmList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.statementlist.ConsStmList) {
      fiacre.types.StatementList tail = this.getTailStmList();
      res = atermFactory.makeList(getHeadStmList().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.StatementList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.StatementList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("StmList".equals(appl.getName())) {
        fiacre.types.StatementList res = fiacre.types.statementlist.EmptyStmList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.Statement elem = fiacre.types.Statement.fromTerm(array[i],atConv);
          res = fiacre.types.statementlist.ConsStmList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.StatementList res = fiacre.types.statementlist.EmptyStmList.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.Statement elem = fiacre.types.Statement.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.statementlist.ConsStmList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.StatementList contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.StatementList contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.StatementList cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.statementlist.ConsStmList) {
      while(cur instanceof fiacre.types.statementlist.ConsStmList) {
        if( o.equals(cur.getHeadStmList()) ) {
          return true;
        }
        cur = cur.getTailStmList();
      }
      if(!(cur instanceof fiacre.types.statementlist.EmptyStmList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyStmList() ; }

  public java.util.Iterator<fiacre.types.Statement> iterator() {
    return new java.util.Iterator<fiacre.types.Statement>() {
      fiacre.types.StatementList list = StmList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyStmList();
      }

      public fiacre.types.Statement next() {
        if(list.isEmptyStmList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsStmList()) {
          fiacre.types.Statement head = list.getHeadStmList();
          list = list.getTailStmList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.Statement)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.Statement o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.Statement> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.statementlist.ConsStmList) {
      fiacre.types.StatementList cur = this;
      while(cur instanceof fiacre.types.statementlist.ConsStmList) {
        fiacre.types.Statement elem = cur.getHeadStmList();
        array[i] = elem;
        cur = cur.getTailStmList();
        i++;
      }
      if(!(cur instanceof fiacre.types.statementlist.EmptyStmList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.statementlist.ConsStmList) {
      fiacre.types.StatementList cur = this;
      while(cur instanceof fiacre.types.statementlist.ConsStmList) {
        fiacre.types.Statement elem = cur.getHeadStmList();
        array[i] = (T)elem;
        cur = cur.getTailStmList();
        i++;
      }
      if(!(cur instanceof fiacre.types.statementlist.EmptyStmList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.Statement> getCollection() {
    return new CollectionStmList(this);
  }

  public java.util.Collection<fiacre.types.Statement> getCollectionStmList() {
    return new CollectionStmList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionStmList implements java.util.Collection<fiacre.types.Statement> {
    private StmList list;

    public StmList getStatementList() {
      return list;
    }

    public CollectionStmList(StmList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.Statement> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.Statement> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getStatementList().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getStatementList().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getStatementList().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getStatementList().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.Statement> iterator() {
    return getStatementList().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getStatementList().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getStatementList().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getStatementList().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getStatementList().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.Statement o) {
      list = (StmList)fiacre.types.statementlist.ConsStmList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (StmList)fiacre.types.statementlist.EmptyStmList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyStmList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
