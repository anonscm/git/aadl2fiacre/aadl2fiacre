
package fiacre.types;


public abstract class AffectationList extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected AffectationList() {}



  /**
   * Returns true if the term is rooted by the symbol ConsAffList
   *
   * @return true if the term is rooted by the symbol ConsAffList
   */
  public boolean isConsAffList() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol EmptyAffList
   *
   * @return true if the term is rooted by the symbol EmptyAffList
   */
  public boolean isEmptyAffList() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot HeadAffList
   *
   * @return the subterm corresponding to the slot HeadAffList
   */
  public fiacre.types.Affectation getHeadAffList() {
    throw new UnsupportedOperationException("This AffectationList has no HeadAffList");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot HeadAffList
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot HeadAffList is replaced by _arg
   */
  public AffectationList setHeadAffList(fiacre.types.Affectation _arg) {
    throw new UnsupportedOperationException("This AffectationList has no HeadAffList");
  }

  /**
   * Returns the subterm corresponding to the slot TailAffList
   *
   * @return the subterm corresponding to the slot TailAffList
   */
  public fiacre.types.AffectationList getTailAffList() {
    throw new UnsupportedOperationException("This AffectationList has no TailAffList");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot TailAffList
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot TailAffList is replaced by _arg
   */
  public AffectationList setTailAffList(fiacre.types.AffectationList _arg) {
    throw new UnsupportedOperationException("This AffectationList has no TailAffList");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.AffectationList from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.AffectationList fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.AffectationList from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.AffectationList fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.AffectationList from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.AffectationList fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.AffectationList
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.AffectationList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.AffectationList tmp;
    java.util.ArrayList<fiacre.types.AffectationList> results = new java.util.ArrayList<fiacre.types.AffectationList>();

    tmp = fiacre.types.affectationlist.ConsAffList.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.affectationlist.EmptyAffList.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.affectationlist.AffList.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a AffectationList");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("AffectationList").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.AffectationList", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.AffectationList from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.AffectationList fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.AffectationList from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.AffectationList fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.AffectationList reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
  /**
   * Returns a Collection extracted from the term
   *
   * @return the collection
   * @throws UnsupportedOperationException if the term is not a list
   */
  public java.util.Collection<fiacre.types.Affectation> getCollectionAffList() {
    throw new UnsupportedOperationException("This AffectationList cannot be converted into a Collection");
  }
          
}
