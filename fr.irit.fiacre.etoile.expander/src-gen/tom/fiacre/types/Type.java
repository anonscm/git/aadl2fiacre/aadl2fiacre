
package fiacre.types;


public abstract class Type extends fiacre.FiacreAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected Type() {}



  /**
   * Returns true if the term is rooted by the symbol BoolType
   *
   * @return true if the term is rooted by the symbol BoolType
   */
  public boolean isBoolType() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol NatType
   *
   * @return true if the term is rooted by the symbol NatType
   */
  public boolean isNatType() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol IntType
   *
   * @return true if the term is rooted by the symbol IntType
   */
  public boolean isIntType() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol NamedType
   *
   * @return true if the term is rooted by the symbol NamedType
   */
  public boolean isNamedType() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol IntervalType
   *
   * @return true if the term is rooted by the symbol IntervalType
   */
  public boolean isIntervalType() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol UnionType
   *
   * @return true if the term is rooted by the symbol UnionType
   */
  public boolean isUnionType() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol RecordType
   *
   * @return true if the term is rooted by the symbol RecordType
   */
  public boolean isRecordType() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ArrayType
   *
   * @return true if the term is rooted by the symbol ArrayType
   */
  public boolean isArrayType() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol QueueType
   *
   * @return true if the term is rooted by the symbol QueueType
   */
  public boolean isQueueType() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol AnyArray
   *
   * @return true if the term is rooted by the symbol AnyArray
   */
  public boolean isAnyArray() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol AnyQueue
   *
   * @return true if the term is rooted by the symbol AnyQueue
   */
  public boolean isAnyQueue() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol BotType
   *
   * @return true if the term is rooted by the symbol BotType
   */
  public boolean isBotType() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol TopType
   *
   * @return true if the term is rooted by the symbol TopType
   */
  public boolean isTopType() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PortType
   *
   * @return true if the term is rooted by the symbol PortType
   */
  public boolean isPortType() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot queue
   *
   * @return the subterm corresponding to the slot queue
   */
  public fiacre.types.Exp getqueue() {
    throw new UnsupportedOperationException("This Type has no queue");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot queue
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot queue is replaced by _arg
   */
  public Type setqueue(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Type has no queue");
  }

  /**
   * Returns the subterm corresponding to the slot array
   *
   * @return the subterm corresponding to the slot array
   */
  public fiacre.types.Exp getarray() {
    throw new UnsupportedOperationException("This Type has no array");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot array
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot array is replaced by _arg
   */
  public Type setarray(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Type has no array");
  }

  /**
   * Returns the subterm corresponding to the slot fdl
   *
   * @return the subterm corresponding to the slot fdl
   */
  public fiacre.types.FieldsDeclList getfdl() {
    throw new UnsupportedOperationException("This Type has no fdl");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot fdl
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot fdl is replaced by _arg
   */
  public Type setfdl(fiacre.types.FieldsDeclList _arg) {
    throw new UnsupportedOperationException("This Type has no fdl");
  }

  /**
   * Returns the subterm corresponding to the slot min
   *
   * @return the subterm corresponding to the slot min
   */
  public fiacre.types.Exp getmin() {
    throw new UnsupportedOperationException("This Type has no min");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot min
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot min is replaced by _arg
   */
  public Type setmin(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Type has no min");
  }

  /**
   * Returns the subterm corresponding to the slot max
   *
   * @return the subterm corresponding to the slot max
   */
  public fiacre.types.Exp getmax() {
    throw new UnsupportedOperationException("This Type has no max");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot max
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot max is replaced by _arg
   */
  public Type setmax(fiacre.types.Exp _arg) {
    throw new UnsupportedOperationException("This Type has no max");
  }

  /**
   * Returns the subterm corresponding to the slot abrev
   *
   * @return the subterm corresponding to the slot abrev
   */
  public String getabrev() {
    throw new UnsupportedOperationException("This Type has no abrev");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot abrev
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot abrev is replaced by _arg
   */
  public Type setabrev(String _arg) {
    throw new UnsupportedOperationException("This Type has no abrev");
  }

  /**
   * Returns the subterm corresponding to the slot type
   *
   * @return the subterm corresponding to the slot type
   */
  public fiacre.types.Type gettype() {
    throw new UnsupportedOperationException("This Type has no type");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot type
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot type is replaced by _arg
   */
  public Type settype(fiacre.types.Type _arg) {
    throw new UnsupportedOperationException("This Type has no type");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fiacre.types.Type from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fiacre.types.Type fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fiacre.types.Type from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fiacre.types.Type fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fiacre.types.Type from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fiacre.types.Type fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fiacre.types.Type
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fiacre.types.Type fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fiacre.types.Type tmp;
    java.util.ArrayList<fiacre.types.Type> results = new java.util.ArrayList<fiacre.types.Type>();

    tmp = fiacre.types.type.BoolType.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.type.NatType.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.type.IntType.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.type.NamedType.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.type.IntervalType.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.type.UnionType.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.type.RecordType.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.type.ArrayType.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.type.QueueType.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.type.AnyArray.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.type.AnyQueue.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.type.BotType.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.type.TopType.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fiacre.types.type.PortType.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a Type");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("Type").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fiacre.types.Type", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.Type from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Type fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fiacre.types.Type from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.Type fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fiacre.types.Type reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }
  
}
