
package fiacre.types.affectationlist;



public abstract class AffList extends fiacre.types.AffectationList implements java.util.Collection<fiacre.types.Affectation>  {


  /**
   * Returns the number of arguments of the variadic operator
   *
   * @return the number of arguments of the variadic operator
   */
  @Override
  public int length() {
    if(this instanceof fiacre.types.affectationlist.ConsAffList) {
      fiacre.types.AffectationList tl = this.getTailAffList();
      if (tl instanceof AffList) {
        return 1+((AffList)tl).length();
      } else {
        return 2;
      }
    } else {
      return 0;
    }
  }

  public static fiacre.types.AffectationList fromArray(fiacre.types.Affectation[] array) {
    fiacre.types.AffectationList res = fiacre.types.affectationlist.EmptyAffList.make();
    for(int i = array.length; i>0;) {
      res = fiacre.types.affectationlist.ConsAffList.make(array[--i],res);
    }
    return res;
  }

  /**
   * Inverses the term if it is a list
   *
   * @return the inverted term if it is a list, otherwise the term itself
   */
  @Override
  public fiacre.types.AffectationList reverse() {
    if(this instanceof fiacre.types.affectationlist.ConsAffList) {
      fiacre.types.AffectationList cur = this;
      fiacre.types.AffectationList rev = fiacre.types.affectationlist.EmptyAffList.make();
      while(cur instanceof fiacre.types.affectationlist.ConsAffList) {
        rev = fiacre.types.affectationlist.ConsAffList.make(cur.getHeadAffList(),rev);
        cur = cur.getTailAffList();
      }

      return rev;
    } else {
      return this;
    }
  }

  /**
   * Appends an element
   *
   * @param element element which has to be added
   * @return the term with the added element
   */
  public fiacre.types.AffectationList append(fiacre.types.Affectation element) {
    if(this instanceof fiacre.types.affectationlist.ConsAffList) {
      fiacre.types.AffectationList tl = this.getTailAffList();
      if (tl instanceof AffList) {
        return fiacre.types.affectationlist.ConsAffList.make(this.getHeadAffList(),((AffList)tl).append(element));
      } else {

        return fiacre.types.affectationlist.ConsAffList.make(this.getHeadAffList(),fiacre.types.affectationlist.ConsAffList.make(element,tl));

      }
    } else {
      return fiacre.types.affectationlist.ConsAffList.make(element,this);
    }
  }

  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("AffList(");
    if(this instanceof fiacre.types.affectationlist.ConsAffList) {
      fiacre.types.AffectationList cur = this;
      while(cur instanceof fiacre.types.affectationlist.ConsAffList) {
        fiacre.types.Affectation elem = cur.getHeadAffList();
        cur = cur.getTailAffList();
        elem.toStringBuilder(buffer);

        if(cur instanceof fiacre.types.affectationlist.ConsAffList) {
          buffer.append(",");
        }
      }
      if(!(cur instanceof fiacre.types.affectationlist.EmptyAffList)) {
        buffer.append(",");
        cur.toStringBuilder(buffer);
      }
    }
    buffer.append(")");
  }

  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  public aterm.ATerm toATerm() {
    aterm.ATerm res = atermFactory.makeList();
    if(this instanceof fiacre.types.affectationlist.ConsAffList) {
      fiacre.types.AffectationList tail = this.getTailAffList();
      res = atermFactory.makeList(getHeadAffList().toATerm(),(aterm.ATermList)tail.toATerm());
    }
    return res;
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.AffectationList from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.AffectationList fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if("AffList".equals(appl.getName())) {
        fiacre.types.AffectationList res = fiacre.types.affectationlist.EmptyAffList.make();

        aterm.ATerm array[] = appl.getArgumentArray();
        for(int i = array.length-1; i>=0; --i) {
          fiacre.types.Affectation elem = fiacre.types.Affectation.fromTerm(array[i],atConv);
          res = fiacre.types.affectationlist.ConsAffList.make(elem,res);
        }
        return res;
      }
    }

    if(trm instanceof aterm.ATermList) {
      aterm.ATermList list = (aterm.ATermList) trm;
      fiacre.types.AffectationList res = fiacre.types.affectationlist.EmptyAffList.make();
      try {
        while(!list.isEmpty()) {
          fiacre.types.Affectation elem = fiacre.types.Affectation.fromTerm(list.getFirst(),atConv);
          res = fiacre.types.affectationlist.ConsAffList.make(elem,res);
          list = list.getNext();
        }
      } catch(IllegalArgumentException e) {
        // returns null when the fromATerm call failed
        return null;
      }
      return res.reverse();
    }

    return null;
  }

  /*
   * Checks if the Collection contains all elements of the parameter Collection
   *
   * @param c the Collection of elements to check
   * @return true if the Collection contains all elements of the parameter, otherwise false
   */
  public boolean containsAll(java.util.Collection c) {
    java.util.Iterator it = c.iterator();
    while(it.hasNext()) {
      if(!this.contains(it.next())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if fiacre.types.AffectationList contains a specified object
   *
   * @param o object whose presence is tested
   * @return true if fiacre.types.AffectationList contains the object, otherwise false
   */
  public boolean contains(Object o) {
    fiacre.types.AffectationList cur = this;
    if(o==null) { return false; }
    if(cur instanceof fiacre.types.affectationlist.ConsAffList) {
      while(cur instanceof fiacre.types.affectationlist.ConsAffList) {
        if( o.equals(cur.getHeadAffList()) ) {
          return true;
        }
        cur = cur.getTailAffList();
      }
      if(!(cur instanceof fiacre.types.affectationlist.EmptyAffList)) {
        if( o.equals(cur) ) {
          return true;
        }
      }
    }
    return false;
  }

  //public boolean equals(Object o) { return this == o; }

  //public int hashCode() { return hashCode(); }

  /**
   * Checks the emptiness
   *
   * @return true if empty, otherwise false
   */
  public boolean isEmpty() { return isEmptyAffList() ; }

  public java.util.Iterator<fiacre.types.Affectation> iterator() {
    return new java.util.Iterator<fiacre.types.Affectation>() {
      fiacre.types.AffectationList list = AffList.this;

      public boolean hasNext() {
        return list!=null && !list.isEmptyAffList();
      }

      public fiacre.types.Affectation next() {
        if(list.isEmptyAffList()) {
          throw new java.util.NoSuchElementException();
        }
        if(list.isConsAffList()) {
          fiacre.types.Affectation head = list.getHeadAffList();
          list = list.getTailAffList();
          return head;
        } else {
          // we are in this case only if domain=codomain
          // thus, the cast is safe
          Object res = list;
          list = null;
          return (fiacre.types.Affectation)res;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException("Not yet implemented");
      }
    };

  }

  public boolean add(fiacre.types.Affectation o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean addAll(java.util.Collection<? extends fiacre.types.Affectation> c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean remove(Object o) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public void clear() {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean removeAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  public boolean retainAll(java.util.Collection c) {
    throw new UnsupportedOperationException("This object "+this.getClass().getName()+" is not mutable");
  }

  /**
   * Returns the size of the collection
   *
   * @return the size of the collection
   */
  public int size() { return length(); }

  /**
   * Returns an array containing the elements of the collection
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    int size = this.length();
    Object[] array = new Object[size];
    int i=0;
    if(this instanceof fiacre.types.affectationlist.ConsAffList) {
      fiacre.types.AffectationList cur = this;
      while(cur instanceof fiacre.types.affectationlist.ConsAffList) {
        fiacre.types.Affectation elem = cur.getHeadAffList();
        array[i] = elem;
        cur = cur.getTailAffList();
        i++;
      }
      if(!(cur instanceof fiacre.types.affectationlist.EmptyAffList)) {
        array[i] = cur;
      }
    }
    return array;
  }

  @SuppressWarnings("unchecked")
  public <T> T[] toArray(T[] array) {
    int size = this.length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    if(this instanceof fiacre.types.affectationlist.ConsAffList) {
      fiacre.types.AffectationList cur = this;
      while(cur instanceof fiacre.types.affectationlist.ConsAffList) {
        fiacre.types.Affectation elem = cur.getHeadAffList();
        array[i] = (T)elem;
        cur = cur.getTailAffList();
        i++;
      }
      if(!(cur instanceof fiacre.types.affectationlist.EmptyAffList)) {
        array[i] = (T)cur;
      }
    }
    return array;
  }

  /*
   * to get a Collection for an immutable list
   */
  public java.util.Collection<fiacre.types.Affectation> getCollection() {
    return new CollectionAffList(this);
  }

  public java.util.Collection<fiacre.types.Affectation> getCollectionAffList() {
    return new CollectionAffList(this);
  }

  /************************************************************
   * private static class
   ************************************************************/
  private static class CollectionAffList implements java.util.Collection<fiacre.types.Affectation> {
    private AffList list;

    public AffList getAffectationList() {
      return list;
    }

    public CollectionAffList(AffList list) {
      this.list = list;
    }

    /**
     * generic
     */
  public boolean addAll(java.util.Collection<? extends fiacre.types.Affectation> c) {
    boolean modified = false;
    java.util.Iterator<? extends fiacre.types.Affectation> it = c.iterator();
    while(it.hasNext()) {
      modified = modified || add(it.next());
    }
    return modified;
  }

  /**
   * Checks if the collection contains an element
   *
   * @param o element whose presence has to be checked
   * @return true if the element is found, otherwise false
   */
  public boolean contains(Object o) {
    return getAffectationList().contains(o);
  }

  /**
   * Checks if the collection contains elements given as parameter
   *
   * @param c elements whose presence has to be checked
   * @return true all the elements are found, otherwise false
   */
  public boolean containsAll(java.util.Collection<?> c) {
    return getAffectationList().containsAll(c);
  }

  /**
   * Checks if an object is equal
   *
   * @param o object which is compared
   * @return true if objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    return getAffectationList().equals(o);
  }

  /**
   * Returns the hashCode
   *
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    return getAffectationList().hashCode();
  }

  /**
   * Returns an iterator over the elements in the collection
   *
   * @return an iterator over the elements in the collection
   */
  public java.util.Iterator<fiacre.types.Affectation> iterator() {
    return getAffectationList().iterator();
  }

  /**
   * Return the size of the collection
   *
   * @return the size of the collection
   */
  public int size() {
    return getAffectationList().size();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @return an array of elements
   */
  public Object[] toArray() {
    return getAffectationList().toArray();
  }

  /**
   * Returns an array containing all of the elements in this collection.
   *
   * @param array array which will contain the result
   * @return an array of elements
   */
  public <T> T[] toArray(T[] array) {
    return getAffectationList().toArray(array);
  }

/*
  public <T> T[] toArray(T[] array) {
    int size = getAffectationList().length();
    if (array.length < size) {
      array = (T[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
    } else if (array.length > size) {
      array[size] = null;
    }
    int i=0;
    for(java.util.Iterator it=iterator() ; it.hasNext() ; i++) {
        array[i] = (T)it.next();
    }
    return array;
  }
*/
    /**
     * Collection
     */

    /**
     * Adds an element to the collection
     *
     * @param o element to add to the collection
     * @return true if it is a success
     */
    public boolean add(fiacre.types.Affectation o) {
      list = (AffList)fiacre.types.affectationlist.ConsAffList.make(o,list);
      return true;
    }

    /**
     * Removes all of the elements from this collection
     */
    public void clear() {
      list = (AffList)fiacre.types.affectationlist.EmptyAffList.make();
    }

    /**
     * Tests the emptiness of the collection
     *
     * @return true if the collection is empty
     */
    public boolean isEmpty() {
      return list.isEmptyAffList();
    }

    public boolean remove(Object o) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean removeAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

    public boolean retainAll(java.util.Collection<?> c) {
      throw new UnsupportedOperationException("Not yet implemented");
    }

  }


}
