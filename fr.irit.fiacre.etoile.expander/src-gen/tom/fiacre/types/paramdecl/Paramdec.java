
package fiacre.types.paramdecl;



public final class Paramdec extends fiacre.types.ParamDecl implements tom.library.sl.Visitable  {
  
  private static String symbolName = "Paramdec";


  private Paramdec() {}
  private int hashCode;
  private static Paramdec gomProto = new Paramdec();
    private fiacre.types.ArgList _lal;
  private fiacre.types.VarAttrList _val;
  private fiacre.types.Type _type;

  /**
   * Constructor that builds a term rooted by Paramdec
   *
   * @return a term rooted by Paramdec
   */

  public static Paramdec make(fiacre.types.ArgList _lal, fiacre.types.VarAttrList _val, fiacre.types.Type _type) {

    // use the proto as a model
    gomProto.initHashCode( _lal,  _val,  _type);
    return (Paramdec) factory.build(gomProto);

  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _lal
   * @param _val
   * @param _type
   * @param hashCode hashCode of Paramdec
   */
  private void init(fiacre.types.ArgList _lal, fiacre.types.VarAttrList _val, fiacre.types.Type _type, int hashCode) {
    this._lal = _lal;
    this._val = _val;
    this._type = _type;

    this.hashCode = hashCode;
  }

  /**
   * Initializes attributes and hashcode of the class
   *
   * @param  _lal
   * @param _val
   * @param _type
   */
  private void initHashCode(fiacre.types.ArgList _lal, fiacre.types.VarAttrList _val, fiacre.types.Type _type) {
    this._lal = _lal;
    this._val = _val;
    this._type = _type;

    this.hashCode = hashFunction();
  }

  /* name and arity */

  /**
   * Returns the name of the symbol
   *
   * @return the name of the symbol
   */
  @Override
  public String symbolName() {
    return "Paramdec";
  }

  /**
   * Returns the arity of the symbol
   *
   * @return the arity of the symbol
   */
  private int getArity() {
    return 3;
  }

  /**
   * Copy the object and returns the copy
   *
   * @return a clone of the SharedObject
   */
  public shared.SharedObject duplicate() {
    Paramdec clone = new Paramdec();
    clone.init( _lal,  _val,  _type, hashCode);
    return clone;
  }
  
  /**
   * Appends a string representation of this term to the buffer given as argument.
   *
   * @param buffer the buffer to which a string represention of this term is appended.
   */
  @Override
  public void toStringBuilder(java.lang.StringBuilder buffer) {
    buffer.append("Paramdec(");
    _lal.toStringBuilder(buffer);
buffer.append(",");
    _val.toStringBuilder(buffer);
buffer.append(",");
    _type.toStringBuilder(buffer);

    buffer.append(")");
  }


  /**
   * Compares two terms. This functions implements a total lexicographic path ordering.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareToLPO(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* compare the symbols */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* compare the childs */
    Paramdec tco = (Paramdec) ao;
    int _lalCmp = (this._lal).compareToLPO(tco._lal);
    if(_lalCmp != 0) {
      return _lalCmp;
    }

    int _valCmp = (this._val).compareToLPO(tco._val);
    if(_valCmp != 0) {
      return _valCmp;
    }

    int _typeCmp = (this._type).compareToLPO(tco._type);
    if(_typeCmp != 0) {
      return _typeCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 /**
   * Compares two terms. This functions implements a total order.
   *
   * @param o object to which this term is compared
   * @return a negative integer, zero, or a positive integer as this
   *         term is less than, equal to, or greater than the argument
   * @throws ClassCastException in case of invalid arguments
   * @throws RuntimeException if unable to compare childs
   */
  @Override
  public int compareTo(Object o) {
    /*
     * We do not want to compare with any object, only members of the module
     * In case of invalid argument, throw a ClassCastException, as the java api
     * asks for it
     */
    fiacre.FiacreAbstractType ao = (fiacre.FiacreAbstractType) o;
    /* return 0 for equality */
    if (ao == this) { return 0; }
    /* use the hash values to discriminate */

    if(hashCode != ao.hashCode()) { return (hashCode < ao.hashCode())?-1:1; }

    /* If not, compare the symbols : back to the normal order */
    int symbCmp = this.symbolName().compareTo(ao.symbolName());
    if (symbCmp != 0) { return symbCmp; }
    /* last resort: compare the childs */
    Paramdec tco = (Paramdec) ao;
    int _lalCmp = (this._lal).compareTo(tco._lal);
    if(_lalCmp != 0) {
      return _lalCmp;
    }

    int _valCmp = (this._val).compareTo(tco._val);
    if(_valCmp != 0) {
      return _valCmp;
    }

    int _typeCmp = (this._type).compareTo(tco._type);
    if(_typeCmp != 0) {
      return _typeCmp;
    }

    throw new RuntimeException("Unable to compare");
  }

 //shared.SharedObject
  /**
   * Returns hashCode
   *
   * @return hashCode
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  /**
   * Checks if a SharedObject is equivalent to the current object
   *
   * @param obj SharedObject to test
   * @return true if obj is a Paramdec and its members are equal, else false
   */
  public final boolean equivalent(shared.SharedObject obj) {
    if(obj instanceof Paramdec) {

      Paramdec peer = (Paramdec) obj;
      return _lal==peer._lal && _val==peer._val && _type==peer._type && true;
    }
    return false;
  }


   //ParamDecl interface
  /**
   * Returns true if the term is rooted by the symbol Paramdec
   *
   * @return true, because this is rooted by Paramdec
   */
  @Override
  public boolean isParamdec() {
    return true;
  }
  
  /**
   * Returns the attribute fiacre.types.ArgList
   *
   * @return the attribute fiacre.types.ArgList
   */
  @Override
  public fiacre.types.ArgList getlal() {
    return _lal;
  }

  /**
   * Sets and returns the attribute fiacre.types.ParamDecl
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.ArgList which just has been set
   */
  @Override
  public fiacre.types.ParamDecl setlal(fiacre.types.ArgList set_arg) {
    return make(set_arg, _val, _type);
  }
  
  /**
   * Returns the attribute fiacre.types.VarAttrList
   *
   * @return the attribute fiacre.types.VarAttrList
   */
  @Override
  public fiacre.types.VarAttrList getval() {
    return _val;
  }

  /**
   * Sets and returns the attribute fiacre.types.ParamDecl
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.VarAttrList which just has been set
   */
  @Override
  public fiacre.types.ParamDecl setval(fiacre.types.VarAttrList set_arg) {
    return make(_lal, set_arg, _type);
  }
  
  /**
   * Returns the attribute fiacre.types.Type
   *
   * @return the attribute fiacre.types.Type
   */
  @Override
  public fiacre.types.Type gettype() {
    return _type;
  }

  /**
   * Sets and returns the attribute fiacre.types.ParamDecl
   *
   * @param set_arg the argument to set
   * @return the attribute fiacre.types.Type which just has been set
   */
  @Override
  public fiacre.types.ParamDecl settype(fiacre.types.Type set_arg) {
    return make(_lal, _val, set_arg);
  }
  
  /* AbstractType */
  /**
   * Returns an ATerm representation of this term.
   *
   * @return an ATerm representation of this term.
   */
  @Override
  public aterm.ATerm toATerm() {
    aterm.ATerm res = super.toATerm();
    if(res != null) {
      // the super class has produced an ATerm (may be a variadic operator)
      return res;
    }
    return atermFactory.makeAppl(
      atermFactory.makeAFun(symbolName(),getArity(),false),
      new aterm.ATerm[] {getlal().toATerm(), getval().toATerm(), gettype().toATerm()});
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fiacre.types.ParamDecl from it
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fiacre.types.ParamDecl fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    trm = atConv.convert(trm);
    if(trm instanceof aterm.ATermAppl) {
      aterm.ATermAppl appl = (aterm.ATermAppl) trm;
      if(symbolName.equals(appl.getName()) && !appl.getAFun().isQuoted()) {
        return make(
fiacre.types.ArgList.fromTerm(appl.getArgument(0),atConv), fiacre.types.VarAttrList.fromTerm(appl.getArgument(1),atConv), fiacre.types.Type.fromTerm(appl.getArgument(2),atConv)
        );
      }
    }
    return null;
  }

  /* Visitable */
  /**
   * Returns the number of childs of the term
   *
   * @return the number of childs of the term
   */
  public int getChildCount() {
    return 3;
  }

  /**
   * Returns the child at the specified index
   *
   * @param index index of the child to return; must be
             nonnegative and less than the childCount
   * @return the child at the specified index
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _lal;
      case 1: return _val;
      case 2: return _type;

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set the child at the specified index
   *
   * @param index index of the child to set; must be
             nonnegative and less than the childCount
   * @param v child to set at the specified index
   * @return the child which was just set
   * @throws IndexOutOfBoundsException if the index out of range
   */
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable v) {
    switch(index) {
      case 0: return make((fiacre.types.ArgList) v, _val, _type);
      case 1: return make(_lal, (fiacre.types.VarAttrList) v, _type);
      case 2: return make(_lal, _val, (fiacre.types.Type) v);

      default: throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Set children to the term
   *
   * @param childs array of children to set
   * @return an array of children which just were set
   * @throws IndexOutOfBoundsException if length of "childs" is different than 3
   */
  @SuppressWarnings("unchecked")
  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] childs) {
    if (childs.length == 3  && childs[0] instanceof fiacre.types.ArgList && childs[1] instanceof fiacre.types.VarAttrList && childs[2] instanceof fiacre.types.Type) {
      return make((fiacre.types.ArgList) childs[0], (fiacre.types.VarAttrList) childs[1], (fiacre.types.Type) childs[2]);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Returns the whole children of the term
   *
   * @return the children of the term
   */
  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[] {  _lal,  _val,  _type };
  }

    /**
     * Compute a hashcode for this term.
     * (for internal use)
     *
     * @return a hash value
     */
  protected int hashFunction() {
    int a, b, c;
    /* Set up the internal state */
    a = 0x9e3779b9; /* the golden ratio; an arbitrary value */
    b = (1511256880<<8);
    c = getArity();
    /* -------------------------------------- handle most of the key */
    /* ------------------------------------ handle the last 11 bytes */
    a += (_lal.hashCode() << 16);
    a += (_val.hashCode() << 8);
    a += (_type.hashCode());

    a -= b; a -= c; a ^= (c >> 13);
    b -= c; b -= a; b ^= (a << 8);
    c -= a; c -= b; c ^= (b >> 13);
    a -= b; a -= c; a ^= (c >> 12);
    b -= c; b -= a; b ^= (a << 16);
    c -= a; c -= b; c ^= (b >> 5);
    a -= b; a -= c; a ^= (c >> 3);
    b -= c; b -= a; b ^= (a << 10);
    c -= a; c -= b; c ^= (b >> 15);
    /* ------------------------------------------- report the result */
    return c;
  }

}
