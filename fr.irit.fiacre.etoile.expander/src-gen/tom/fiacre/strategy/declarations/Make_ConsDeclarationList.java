
package fiacre.strategy.declarations;

public class Make_ConsDeclarationList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadDeclarationList;
  private tom.library.sl.Strategy _TailDeclarationList;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadDeclarationList;
      case 1: return _TailDeclarationList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadDeclarationList = (tom.library.sl.Strategy) child; return this;
      case 1: _TailDeclarationList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadDeclarationList, _TailDeclarationList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadDeclarationList = (tom.library.sl.Strategy)children[0];
    this._TailDeclarationList = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsDeclarationList(tom.library.sl.Strategy _HeadDeclarationList, tom.library.sl.Strategy _TailDeclarationList) {
    this._HeadDeclarationList = _HeadDeclarationList;
    this._TailDeclarationList = _TailDeclarationList;

  }

  /**
    * Builds a new ConsDeclarationList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadDeclarationList = _HeadDeclarationList.visit(any,i);
    if (! (tmp_HeadDeclarationList instanceof fiacre.types.Declaration)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Declaration new_HeadDeclarationList = (fiacre.types.Declaration) tmp_HeadDeclarationList;

    Object tmp_TailDeclarationList = _TailDeclarationList.visit(any,i);
    if (! (tmp_TailDeclarationList instanceof fiacre.types.Declarations)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Declarations new_TailDeclarationList = (fiacre.types.Declarations) tmp_TailDeclarationList;

    return (T) fiacre.types.declarations.ConsDeclarationList.make( new_HeadDeclarationList,  new_TailDeclarationList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadDeclarationList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Declaration)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Declaration new_HeadDeclarationList = (fiacre.types.Declaration) getEnvironment().getSubject();

    (_TailDeclarationList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Declarations)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Declarations new_TailDeclarationList = (fiacre.types.Declarations) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.declarations.ConsDeclarationList.make( new_HeadDeclarationList,  new_TailDeclarationList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
