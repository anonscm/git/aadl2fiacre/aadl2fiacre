
package fiacre.strategy.portdecls;

public class Make_ConsListPortDecl implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadListPortDecl;
  private tom.library.sl.Strategy _TailListPortDecl;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadListPortDecl;
      case 1: return _TailListPortDecl;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadListPortDecl = (tom.library.sl.Strategy) child; return this;
      case 1: _TailListPortDecl = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadListPortDecl, _TailListPortDecl};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadListPortDecl = (tom.library.sl.Strategy)children[0];
    this._TailListPortDecl = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsListPortDecl(tom.library.sl.Strategy _HeadListPortDecl, tom.library.sl.Strategy _TailListPortDecl) {
    this._HeadListPortDecl = _HeadListPortDecl;
    this._TailListPortDecl = _TailListPortDecl;

  }

  /**
    * Builds a new ConsListPortDecl
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadListPortDecl = _HeadListPortDecl.visit(any,i);
    if (! (tmp_HeadListPortDecl instanceof fiacre.types.PortDecl)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PortDecl new_HeadListPortDecl = (fiacre.types.PortDecl) tmp_HeadListPortDecl;

    Object tmp_TailListPortDecl = _TailListPortDecl.visit(any,i);
    if (! (tmp_TailListPortDecl instanceof fiacre.types.PortDecls)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PortDecls new_TailListPortDecl = (fiacre.types.PortDecls) tmp_TailListPortDecl;

    return (T) fiacre.types.portdecls.ConsListPortDecl.make( new_HeadListPortDecl,  new_TailListPortDecl);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadListPortDecl).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PortDecl)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PortDecl new_HeadListPortDecl = (fiacre.types.PortDecl) getEnvironment().getSubject();

    (_TailListPortDecl).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PortDecls)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PortDecls new_TailListPortDecl = (fiacre.types.PortDecls) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.portdecls.ConsListPortDecl.make( new_HeadListPortDecl,  new_TailListPortDecl));
    return tom.library.sl.Environment.SUCCESS;
  }
}
