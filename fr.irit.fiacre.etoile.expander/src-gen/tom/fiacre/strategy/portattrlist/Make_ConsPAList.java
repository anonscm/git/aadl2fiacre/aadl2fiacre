
package fiacre.strategy.portattrlist;

public class Make_ConsPAList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadPAList;
  private tom.library.sl.Strategy _TailPAList;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadPAList;
      case 1: return _TailPAList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadPAList = (tom.library.sl.Strategy) child; return this;
      case 1: _TailPAList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadPAList, _TailPAList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadPAList = (tom.library.sl.Strategy)children[0];
    this._TailPAList = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsPAList(tom.library.sl.Strategy _HeadPAList, tom.library.sl.Strategy _TailPAList) {
    this._HeadPAList = _HeadPAList;
    this._TailPAList = _TailPAList;

  }

  /**
    * Builds a new ConsPAList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadPAList = _HeadPAList.visit(any,i);
    if (! (tmp_HeadPAList instanceof fiacre.types.PortAttr)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PortAttr new_HeadPAList = (fiacre.types.PortAttr) tmp_HeadPAList;

    Object tmp_TailPAList = _TailPAList.visit(any,i);
    if (! (tmp_TailPAList instanceof fiacre.types.PortAttrList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PortAttrList new_TailPAList = (fiacre.types.PortAttrList) tmp_TailPAList;

    return (T) fiacre.types.portattrlist.ConsPAList.make( new_HeadPAList,  new_TailPAList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadPAList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PortAttr)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PortAttr new_HeadPAList = (fiacre.types.PortAttr) getEnvironment().getSubject();

    (_TailPAList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PortAttrList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PortAttrList new_TailPAList = (fiacre.types.PortAttrList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.portattrlist.ConsPAList.make( new_HeadPAList,  new_TailPAList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
