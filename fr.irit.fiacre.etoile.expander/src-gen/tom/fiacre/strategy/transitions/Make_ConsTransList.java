
package fiacre.strategy.transitions;

public class Make_ConsTransList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadTransList;
  private tom.library.sl.Strategy _TailTransList;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadTransList;
      case 1: return _TailTransList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadTransList = (tom.library.sl.Strategy) child; return this;
      case 1: _TailTransList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadTransList, _TailTransList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadTransList = (tom.library.sl.Strategy)children[0];
    this._TailTransList = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsTransList(tom.library.sl.Strategy _HeadTransList, tom.library.sl.Strategy _TailTransList) {
    this._HeadTransList = _HeadTransList;
    this._TailTransList = _TailTransList;

  }

  /**
    * Builds a new ConsTransList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadTransList = _HeadTransList.visit(any,i);
    if (! (tmp_HeadTransList instanceof fiacre.types.Transition)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Transition new_HeadTransList = (fiacre.types.Transition) tmp_HeadTransList;

    Object tmp_TailTransList = _TailTransList.visit(any,i);
    if (! (tmp_TailTransList instanceof fiacre.types.Transitions)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Transitions new_TailTransList = (fiacre.types.Transitions) tmp_TailTransList;

    return (T) fiacre.types.transitions.ConsTransList.make( new_HeadTransList,  new_TailTransList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadTransList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Transition)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Transition new_HeadTransList = (fiacre.types.Transition) getEnvironment().getSubject();

    (_TailTransList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Transitions)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Transitions new_TailTransList = (fiacre.types.Transitions) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.transitions.ConsTransList.make( new_HeadTransList,  new_TailTransList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
