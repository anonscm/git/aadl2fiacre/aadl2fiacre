
package fiacre.strategy.property;

public class Make_PropBin implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _arg1;
  private String _op;
  private tom.library.sl.Strategy _arg2;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _arg1;
      case 1: return _arg2;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _arg1 = (tom.library.sl.Strategy) child; return this;
      case 1: _arg2 = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_arg1, _arg2};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._arg1 = (tom.library.sl.Strategy)children[0];
    this._arg2 = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_PropBin(tom.library.sl.Strategy _arg1, String _op, tom.library.sl.Strategy _arg2) {
    this._arg1 = _arg1;
    this._op = _op;
    this._arg2 = _arg2;

  }

  /**
    * Builds a new PropBin
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_arg1 = _arg1.visit(any,i);
    if (! (tmp_arg1 instanceof fiacre.types.Property)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Property new_arg1 = (fiacre.types.Property) tmp_arg1;

    Object tmp_arg2 = _arg2.visit(any,i);
    if (! (tmp_arg2 instanceof fiacre.types.Property)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Property new_arg2 = (fiacre.types.Property) tmp_arg2;

    return (T) fiacre.types.property.PropBin.make( new_arg1,  _op,  new_arg2);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_arg1).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Property)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Property new_arg1 = (fiacre.types.Property) getEnvironment().getSubject();

    (_arg2).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Property)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Property new_arg2 = (fiacre.types.Property) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.property.PropBin.make( new_arg1,  _op,  new_arg2));
    return tom.library.sl.Environment.SUCCESS;
  }
}
