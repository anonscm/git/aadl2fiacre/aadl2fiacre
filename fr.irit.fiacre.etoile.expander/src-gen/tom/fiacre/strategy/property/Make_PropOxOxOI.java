
package fiacre.strategy.property;

public class Make_PropOxOxOI implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private String _op1;
  private tom.library.sl.Strategy _obs1;
  private String _op2;
  private tom.library.sl.Strategy _obs2;
  private String _op3;
  private tom.library.sl.Strategy _min;
  private tom.library.sl.Strategy _max;


  public int getChildCount() {
    return 4;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _obs1;
      case 1: return _obs2;
      case 2: return _min;
      case 3: return _max;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _obs1 = (tom.library.sl.Strategy) child; return this;
      case 1: _obs2 = (tom.library.sl.Strategy) child; return this;
      case 2: _min = (tom.library.sl.Strategy) child; return this;
      case 3: _max = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_obs1, _obs2, _min, _max};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._obs1 = (tom.library.sl.Strategy)children[0];
    this._obs2 = (tom.library.sl.Strategy)children[1];
    this._min = (tom.library.sl.Strategy)children[2];
    this._max = (tom.library.sl.Strategy)children[3];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_PropOxOxOI(String _op1, tom.library.sl.Strategy _obs1, String _op2, tom.library.sl.Strategy _obs2, String _op3, tom.library.sl.Strategy _min, tom.library.sl.Strategy _max) {
    this._op1 = _op1;
    this._obs1 = _obs1;
    this._op2 = _op2;
    this._obs2 = _obs2;
    this._op3 = _op3;
    this._min = _min;
    this._max = _max;

  }

  /**
    * Builds a new PropOxOxOI
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_obs1 = _obs1.visit(any,i);
    if (! (tmp_obs1 instanceof fiacre.types.Observable)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Observable new_obs1 = (fiacre.types.Observable) tmp_obs1;

    Object tmp_obs2 = _obs2.visit(any,i);
    if (! (tmp_obs2 instanceof fiacre.types.Observable)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Observable new_obs2 = (fiacre.types.Observable) tmp_obs2;

    Object tmp_min = _min.visit(any,i);
    if (! (tmp_min instanceof fiacre.types.Bound)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Bound new_min = (fiacre.types.Bound) tmp_min;

    Object tmp_max = _max.visit(any,i);
    if (! (tmp_max instanceof fiacre.types.Bound)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Bound new_max = (fiacre.types.Bound) tmp_max;

    return (T) fiacre.types.property.PropOxOxOI.make( _op1,  new_obs1,  _op2,  new_obs2,  _op3,  new_min,  new_max);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_obs1).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Observable)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Observable new_obs1 = (fiacre.types.Observable) getEnvironment().getSubject();

    (_obs2).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Observable)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Observable new_obs2 = (fiacre.types.Observable) getEnvironment().getSubject();

    (_min).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Bound)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Bound new_min = (fiacre.types.Bound) getEnvironment().getSubject();

    (_max).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Bound)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Bound new_max = (fiacre.types.Bound) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.property.PropOxOxOI.make( _op1,  new_obs1,  _op2,  new_obs2,  _op3,  new_min,  new_max));
    return tom.library.sl.Environment.SUCCESS;
  }
}
