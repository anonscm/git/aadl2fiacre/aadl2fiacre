
package fiacre.strategy.property;

public class Make_PropAll implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private String _ind;
  private tom.library.sl.Strategy _begin;
  private tom.library.sl.Strategy _end;
  private tom.library.sl.Strategy _p;


  public int getChildCount() {
    return 3;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _begin;
      case 1: return _end;
      case 2: return _p;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _begin = (tom.library.sl.Strategy) child; return this;
      case 1: _end = (tom.library.sl.Strategy) child; return this;
      case 2: _p = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_begin, _end, _p};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._begin = (tom.library.sl.Strategy)children[0];
    this._end = (tom.library.sl.Strategy)children[1];
    this._p = (tom.library.sl.Strategy)children[2];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_PropAll(String _ind, tom.library.sl.Strategy _begin, tom.library.sl.Strategy _end, tom.library.sl.Strategy _p) {
    this._ind = _ind;
    this._begin = _begin;
    this._end = _end;
    this._p = _p;

  }

  /**
    * Builds a new PropAll
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_begin = _begin.visit(any,i);
    if (! (tmp_begin instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_begin = (fiacre.types.Exp) tmp_begin;

    Object tmp_end = _end.visit(any,i);
    if (! (tmp_end instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_end = (fiacre.types.Exp) tmp_end;

    Object tmp_p = _p.visit(any,i);
    if (! (tmp_p instanceof fiacre.types.Property)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Property new_p = (fiacre.types.Property) tmp_p;

    return (T) fiacre.types.property.PropAll.make( _ind,  new_begin,  new_end,  new_p);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_begin).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_begin = (fiacre.types.Exp) getEnvironment().getSubject();

    (_end).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_end = (fiacre.types.Exp) getEnvironment().getSubject();

    (_p).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Property)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Property new_p = (fiacre.types.Property) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.property.PropAll.make( _ind,  new_begin,  new_end,  new_p));
    return tom.library.sl.Environment.SUCCESS;
  }
}
