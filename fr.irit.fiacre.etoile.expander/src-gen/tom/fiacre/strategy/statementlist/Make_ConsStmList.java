
package fiacre.strategy.statementlist;

public class Make_ConsStmList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadStmList;
  private tom.library.sl.Strategy _TailStmList;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadStmList;
      case 1: return _TailStmList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadStmList = (tom.library.sl.Strategy) child; return this;
      case 1: _TailStmList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadStmList, _TailStmList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadStmList = (tom.library.sl.Strategy)children[0];
    this._TailStmList = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsStmList(tom.library.sl.Strategy _HeadStmList, tom.library.sl.Strategy _TailStmList) {
    this._HeadStmList = _HeadStmList;
    this._TailStmList = _TailStmList;

  }

  /**
    * Builds a new ConsStmList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadStmList = _HeadStmList.visit(any,i);
    if (! (tmp_HeadStmList instanceof fiacre.types.Statement)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Statement new_HeadStmList = (fiacre.types.Statement) tmp_HeadStmList;

    Object tmp_TailStmList = _TailStmList.visit(any,i);
    if (! (tmp_TailStmList instanceof fiacre.types.StatementList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.StatementList new_TailStmList = (fiacre.types.StatementList) tmp_TailStmList;

    return (T) fiacre.types.statementlist.ConsStmList.make( new_HeadStmList,  new_TailStmList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadStmList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Statement)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Statement new_HeadStmList = (fiacre.types.Statement) getEnvironment().getSubject();

    (_TailStmList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.StatementList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.StatementList new_TailStmList = (fiacre.types.StatementList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.statementlist.ConsStmList.make( new_HeadStmList,  new_TailStmList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
