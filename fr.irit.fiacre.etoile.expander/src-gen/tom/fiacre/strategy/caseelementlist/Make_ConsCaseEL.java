
package fiacre.strategy.caseelementlist;

public class Make_ConsCaseEL implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadCaseEL;
  private tom.library.sl.Strategy _TailCaseEL;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadCaseEL;
      case 1: return _TailCaseEL;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadCaseEL = (tom.library.sl.Strategy) child; return this;
      case 1: _TailCaseEL = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadCaseEL, _TailCaseEL};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadCaseEL = (tom.library.sl.Strategy)children[0];
    this._TailCaseEL = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsCaseEL(tom.library.sl.Strategy _HeadCaseEL, tom.library.sl.Strategy _TailCaseEL) {
    this._HeadCaseEL = _HeadCaseEL;
    this._TailCaseEL = _TailCaseEL;

  }

  /**
    * Builds a new ConsCaseEL
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadCaseEL = _HeadCaseEL.visit(any,i);
    if (! (tmp_HeadCaseEL instanceof fiacre.types.CaseElement)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.CaseElement new_HeadCaseEL = (fiacre.types.CaseElement) tmp_HeadCaseEL;

    Object tmp_TailCaseEL = _TailCaseEL.visit(any,i);
    if (! (tmp_TailCaseEL instanceof fiacre.types.CaseElementList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.CaseElementList new_TailCaseEL = (fiacre.types.CaseElementList) tmp_TailCaseEL;

    return (T) fiacre.types.caseelementlist.ConsCaseEL.make( new_HeadCaseEL,  new_TailCaseEL);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadCaseEL).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.CaseElement)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.CaseElement new_HeadCaseEL = (fiacre.types.CaseElement) getEnvironment().getSubject();

    (_TailCaseEL).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.CaseElementList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.CaseElementList new_TailCaseEL = (fiacre.types.CaseElementList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.caseelementlist.ConsCaseEL.make( new_HeadCaseEL,  new_TailCaseEL));
    return tom.library.sl.Environment.SUCCESS;
  }
}
