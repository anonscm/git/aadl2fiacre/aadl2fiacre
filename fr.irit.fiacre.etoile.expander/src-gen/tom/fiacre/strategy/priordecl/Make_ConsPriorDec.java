
package fiacre.strategy.priordecl;

public class Make_ConsPriorDec implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadPriorDec;
  private tom.library.sl.Strategy _TailPriorDec;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadPriorDec;
      case 1: return _TailPriorDec;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadPriorDec = (tom.library.sl.Strategy) child; return this;
      case 1: _TailPriorDec = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadPriorDec, _TailPriorDec};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadPriorDec = (tom.library.sl.Strategy)children[0];
    this._TailPriorDec = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsPriorDec(tom.library.sl.Strategy _HeadPriorDec, tom.library.sl.Strategy _TailPriorDec) {
    this._HeadPriorDec = _HeadPriorDec;
    this._TailPriorDec = _TailPriorDec;

  }

  /**
    * Builds a new ConsPriorDec
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadPriorDec = _HeadPriorDec.visit(any,i);
    if (! (tmp_HeadPriorDec instanceof fiacre.types.ExpList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.ExpList new_HeadPriorDec = (fiacre.types.ExpList) tmp_HeadPriorDec;

    Object tmp_TailPriorDec = _TailPriorDec.visit(any,i);
    if (! (tmp_TailPriorDec instanceof fiacre.types.PriorDecl)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PriorDecl new_TailPriorDec = (fiacre.types.PriorDecl) tmp_TailPriorDec;

    return (T) fiacre.types.priordecl.ConsPriorDec.make( new_HeadPriorDec,  new_TailPriorDec);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadPriorDec).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.ExpList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.ExpList new_HeadPriorDec = (fiacre.types.ExpList) getEnvironment().getSubject();

    (_TailPriorDec).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PriorDecl)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PriorDecl new_TailPriorDec = (fiacre.types.PriorDecl) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.priordecl.ConsPriorDec.make( new_HeadPriorDec,  new_TailPriorDec));
    return tom.library.sl.Environment.SUCCESS;
  }
}
