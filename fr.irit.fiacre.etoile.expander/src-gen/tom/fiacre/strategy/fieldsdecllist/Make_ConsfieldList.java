
package fiacre.strategy.fieldsdecllist;

public class Make_ConsfieldList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadfieldList;
  private tom.library.sl.Strategy _TailfieldList;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadfieldList;
      case 1: return _TailfieldList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadfieldList = (tom.library.sl.Strategy) child; return this;
      case 1: _TailfieldList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadfieldList, _TailfieldList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadfieldList = (tom.library.sl.Strategy)children[0];
    this._TailfieldList = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsfieldList(tom.library.sl.Strategy _HeadfieldList, tom.library.sl.Strategy _TailfieldList) {
    this._HeadfieldList = _HeadfieldList;
    this._TailfieldList = _TailfieldList;

  }

  /**
    * Builds a new ConsfieldList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadfieldList = _HeadfieldList.visit(any,i);
    if (! (tmp_HeadfieldList instanceof fiacre.types.FieldsDecl)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.FieldsDecl new_HeadfieldList = (fiacre.types.FieldsDecl) tmp_HeadfieldList;

    Object tmp_TailfieldList = _TailfieldList.visit(any,i);
    if (! (tmp_TailfieldList instanceof fiacre.types.FieldsDeclList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.FieldsDeclList new_TailfieldList = (fiacre.types.FieldsDeclList) tmp_TailfieldList;

    return (T) fiacre.types.fieldsdecllist.ConsfieldList.make( new_HeadfieldList,  new_TailfieldList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadfieldList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.FieldsDecl)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.FieldsDecl new_HeadfieldList = (fiacre.types.FieldsDecl) getEnvironment().getSubject();

    (_TailfieldList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.FieldsDeclList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.FieldsDeclList new_TailfieldList = (fiacre.types.FieldsDeclList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.fieldsdecllist.ConsfieldList.make( new_HeadfieldList,  new_TailfieldList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
