
package fiacre.strategy.varattrlist;

public class Make_ConsVAList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadVAList;
  private tom.library.sl.Strategy _TailVAList;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadVAList;
      case 1: return _TailVAList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadVAList = (tom.library.sl.Strategy) child; return this;
      case 1: _TailVAList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadVAList, _TailVAList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadVAList = (tom.library.sl.Strategy)children[0];
    this._TailVAList = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsVAList(tom.library.sl.Strategy _HeadVAList, tom.library.sl.Strategy _TailVAList) {
    this._HeadVAList = _HeadVAList;
    this._TailVAList = _TailVAList;

  }

  /**
    * Builds a new ConsVAList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadVAList = _HeadVAList.visit(any,i);
    if (! (tmp_HeadVAList instanceof fiacre.types.VarAttr)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.VarAttr new_HeadVAList = (fiacre.types.VarAttr) tmp_HeadVAList;

    Object tmp_TailVAList = _TailVAList.visit(any,i);
    if (! (tmp_TailVAList instanceof fiacre.types.VarAttrList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.VarAttrList new_TailVAList = (fiacre.types.VarAttrList) tmp_TailVAList;

    return (T) fiacre.types.varattrlist.ConsVAList.make( new_HeadVAList,  new_TailVAList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadVAList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.VarAttr)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.VarAttr new_HeadVAList = (fiacre.types.VarAttr) getEnvironment().getSubject();

    (_TailVAList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.VarAttrList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.VarAttrList new_TailVAList = (fiacre.types.VarAttrList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.varattrlist.ConsVAList.make( new_HeadVAList,  new_TailVAList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
