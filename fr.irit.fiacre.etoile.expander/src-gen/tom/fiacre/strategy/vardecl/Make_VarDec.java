
package fiacre.strategy.vardecl;

public class Make_VarDec implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _lsl;
  private tom.library.sl.Strategy _type;
  private tom.library.sl.Strategy _oe;


  public int getChildCount() {
    return 3;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _lsl;
      case 1: return _type;
      case 2: return _oe;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _lsl = (tom.library.sl.Strategy) child; return this;
      case 1: _type = (tom.library.sl.Strategy) child; return this;
      case 2: _oe = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_lsl, _type, _oe};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._lsl = (tom.library.sl.Strategy)children[0];
    this._type = (tom.library.sl.Strategy)children[1];
    this._oe = (tom.library.sl.Strategy)children[2];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_VarDec(tom.library.sl.Strategy _lsl, tom.library.sl.Strategy _type, tom.library.sl.Strategy _oe) {
    this._lsl = _lsl;
    this._type = _type;
    this._oe = _oe;

  }

  /**
    * Builds a new VarDec
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_lsl = _lsl.visit(any,i);
    if (! (tmp_lsl instanceof fiacre.types.StringList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.StringList new_lsl = (fiacre.types.StringList) tmp_lsl;

    Object tmp_type = _type.visit(any,i);
    if (! (tmp_type instanceof fiacre.types.Type)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Type new_type = (fiacre.types.Type) tmp_type;

    Object tmp_oe = _oe.visit(any,i);
    if (! (tmp_oe instanceof fiacre.types.Optional_Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Optional_Exp new_oe = (fiacre.types.Optional_Exp) tmp_oe;

    return (T) fiacre.types.vardecl.VarDec.make( new_lsl,  new_type,  new_oe);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_lsl).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.StringList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.StringList new_lsl = (fiacre.types.StringList) getEnvironment().getSubject();

    (_type).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Type)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Type new_type = (fiacre.types.Type) getEnvironment().getSubject();

    (_oe).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Optional_Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Optional_Exp new_oe = (fiacre.types.Optional_Exp) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.vardecl.VarDec.make( new_lsl,  new_type,  new_oe));
    return tom.library.sl.Environment.SUCCESS;
  }
}
