
package fiacre.strategy.statement;

public class Make_StOutput implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _po;
  private tom.library.sl.Strategy _chan;
  private tom.library.sl.Strategy _elist;


  public int getChildCount() {
    return 3;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _po;
      case 1: return _chan;
      case 2: return _elist;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _po = (tom.library.sl.Strategy) child; return this;
      case 1: _chan = (tom.library.sl.Strategy) child; return this;
      case 2: _elist = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_po, _chan, _elist};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._po = (tom.library.sl.Strategy)children[0];
    this._chan = (tom.library.sl.Strategy)children[1];
    this._elist = (tom.library.sl.Strategy)children[2];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_StOutput(tom.library.sl.Strategy _po, tom.library.sl.Strategy _chan, tom.library.sl.Strategy _elist) {
    this._po = _po;
    this._chan = _chan;
    this._elist = _elist;

  }

  /**
    * Builds a new StOutput
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_po = _po.visit(any,i);
    if (! (tmp_po instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_po = (fiacre.types.Exp) tmp_po;

    Object tmp_chan = _chan.visit(any,i);
    if (! (tmp_chan instanceof fiacre.types.Channel)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Channel new_chan = (fiacre.types.Channel) tmp_chan;

    Object tmp_elist = _elist.visit(any,i);
    if (! (tmp_elist instanceof fiacre.types.ExpList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.ExpList new_elist = (fiacre.types.ExpList) tmp_elist;

    return (T) fiacre.types.statement.StOutput.make( new_po,  new_chan,  new_elist);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_po).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_po = (fiacre.types.Exp) getEnvironment().getSubject();

    (_chan).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Channel)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Channel new_chan = (fiacre.types.Channel) getEnvironment().getSubject();

    (_elist).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.ExpList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.ExpList new_elist = (fiacre.types.ExpList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.statement.StOutput.make( new_po,  new_chan,  new_elist));
    return tom.library.sl.Environment.SUCCESS;
  }
}
