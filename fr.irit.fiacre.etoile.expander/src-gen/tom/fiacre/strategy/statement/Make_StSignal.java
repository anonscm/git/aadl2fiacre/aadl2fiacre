
package fiacre.strategy.statement;

public class Make_StSignal implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _po;
  private tom.library.sl.Strategy _chan;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _po;
      case 1: return _chan;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _po = (tom.library.sl.Strategy) child; return this;
      case 1: _chan = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_po, _chan};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._po = (tom.library.sl.Strategy)children[0];
    this._chan = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_StSignal(tom.library.sl.Strategy _po, tom.library.sl.Strategy _chan) {
    this._po = _po;
    this._chan = _chan;

  }

  /**
    * Builds a new StSignal
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_po = _po.visit(any,i);
    if (! (tmp_po instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_po = (fiacre.types.Exp) tmp_po;

    Object tmp_chan = _chan.visit(any,i);
    if (! (tmp_chan instanceof fiacre.types.Channel)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Channel new_chan = (fiacre.types.Channel) tmp_chan;

    return (T) fiacre.types.statement.StSignal.make( new_po,  new_chan);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_po).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_po = (fiacre.types.Exp) getEnvironment().getSubject();

    (_chan).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Channel)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Channel new_chan = (fiacre.types.Channel) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.statement.StSignal.make( new_po,  new_chan));
    return tom.library.sl.Environment.SUCCESS;
  }
}
