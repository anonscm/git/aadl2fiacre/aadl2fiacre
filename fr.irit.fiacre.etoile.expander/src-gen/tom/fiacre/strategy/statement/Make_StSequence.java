
package fiacre.strategy.statement;

public class Make_StSequence implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _s1;
  private tom.library.sl.Strategy _s2;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _s1;
      case 1: return _s2;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _s1 = (tom.library.sl.Strategy) child; return this;
      case 1: _s2 = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_s1, _s2};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._s1 = (tom.library.sl.Strategy)children[0];
    this._s2 = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_StSequence(tom.library.sl.Strategy _s1, tom.library.sl.Strategy _s2) {
    this._s1 = _s1;
    this._s2 = _s2;

  }

  /**
    * Builds a new StSequence
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_s1 = _s1.visit(any,i);
    if (! (tmp_s1 instanceof fiacre.types.Statement)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Statement new_s1 = (fiacre.types.Statement) tmp_s1;

    Object tmp_s2 = _s2.visit(any,i);
    if (! (tmp_s2 instanceof fiacre.types.Statement)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Statement new_s2 = (fiacre.types.Statement) tmp_s2;

    return (T) fiacre.types.statement.StSequence.make( new_s1,  new_s2);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_s1).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Statement)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Statement new_s1 = (fiacre.types.Statement) getEnvironment().getSubject();

    (_s2).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Statement)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Statement new_s2 = (fiacre.types.Statement) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.statement.StSequence.make( new_s1,  new_s2));
    return tom.library.sl.Environment.SUCCESS;
  }
}
