
package fiacre.strategy.statement;

public class Make_StAny implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _p;
  private tom.library.sl.Strategy _types;
  private tom.library.sl.Strategy _oe;


  public int getChildCount() {
    return 3;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _p;
      case 1: return _types;
      case 2: return _oe;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _p = (tom.library.sl.Strategy) child; return this;
      case 1: _types = (tom.library.sl.Strategy) child; return this;
      case 2: _oe = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_p, _types, _oe};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._p = (tom.library.sl.Strategy)children[0];
    this._types = (tom.library.sl.Strategy)children[1];
    this._oe = (tom.library.sl.Strategy)children[2];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_StAny(tom.library.sl.Strategy _p, tom.library.sl.Strategy _types, tom.library.sl.Strategy _oe) {
    this._p = _p;
    this._types = _types;
    this._oe = _oe;

  }

  /**
    * Builds a new StAny
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_p = _p.visit(any,i);
    if (! (tmp_p instanceof fiacre.types.ExpList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.ExpList new_p = (fiacre.types.ExpList) tmp_p;

    Object tmp_types = _types.visit(any,i);
    if (! (tmp_types instanceof fiacre.types.TypeList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.TypeList new_types = (fiacre.types.TypeList) tmp_types;

    Object tmp_oe = _oe.visit(any,i);
    if (! (tmp_oe instanceof fiacre.types.Optional_Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Optional_Exp new_oe = (fiacre.types.Optional_Exp) tmp_oe;

    return (T) fiacre.types.statement.StAny.make( new_p,  new_types,  new_oe);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_p).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.ExpList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.ExpList new_p = (fiacre.types.ExpList) getEnvironment().getSubject();

    (_types).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.TypeList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.TypeList new_types = (fiacre.types.TypeList) getEnvironment().getSubject();

    (_oe).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Optional_Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Optional_Exp new_oe = (fiacre.types.Optional_Exp) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.statement.StAny.make( new_p,  new_types,  new_oe));
    return tom.library.sl.Environment.SUCCESS;
  }
}
