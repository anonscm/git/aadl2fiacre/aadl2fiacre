
package fiacre.strategy.statement;

public class Make_StSelectIndexe implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private String _var;
  private tom.library.sl.Strategy _deb;
  private tom.library.sl.Strategy _fin;
  private tom.library.sl.Strategy _stl;


  public int getChildCount() {
    return 3;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _deb;
      case 1: return _fin;
      case 2: return _stl;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _deb = (tom.library.sl.Strategy) child; return this;
      case 1: _fin = (tom.library.sl.Strategy) child; return this;
      case 2: _stl = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_deb, _fin, _stl};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._deb = (tom.library.sl.Strategy)children[0];
    this._fin = (tom.library.sl.Strategy)children[1];
    this._stl = (tom.library.sl.Strategy)children[2];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_StSelectIndexe(String _var, tom.library.sl.Strategy _deb, tom.library.sl.Strategy _fin, tom.library.sl.Strategy _stl) {
    this._var = _var;
    this._deb = _deb;
    this._fin = _fin;
    this._stl = _stl;

  }

  /**
    * Builds a new StSelectIndexe
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_deb = _deb.visit(any,i);
    if (! (tmp_deb instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_deb = (fiacre.types.Exp) tmp_deb;

    Object tmp_fin = _fin.visit(any,i);
    if (! (tmp_fin instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_fin = (fiacre.types.Exp) tmp_fin;

    Object tmp_stl = _stl.visit(any,i);
    if (! (tmp_stl instanceof fiacre.types.PStatementList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PStatementList new_stl = (fiacre.types.PStatementList) tmp_stl;

    return (T) fiacre.types.statement.StSelectIndexe.make( _var,  new_deb,  new_fin,  new_stl);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_deb).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_deb = (fiacre.types.Exp) getEnvironment().getSubject();

    (_fin).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_fin = (fiacre.types.Exp) getEnvironment().getSubject();

    (_stl).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PStatementList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PStatementList new_stl = (fiacre.types.PStatementList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.statement.StSelectIndexe.make( _var,  new_deb,  new_fin,  new_stl));
    return tom.library.sl.Environment.SUCCESS;
  }
}
