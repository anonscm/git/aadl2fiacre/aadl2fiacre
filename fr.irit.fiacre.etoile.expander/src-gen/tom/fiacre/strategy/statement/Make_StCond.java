
package fiacre.strategy.statement;

public class Make_StCond implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _cond;
  private tom.library.sl.Strategy _st1;
  private tom.library.sl.Strategy _condst;
  private tom.library.sl.Strategy _ost;


  public int getChildCount() {
    return 4;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _cond;
      case 1: return _st1;
      case 2: return _condst;
      case 3: return _ost;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _cond = (tom.library.sl.Strategy) child; return this;
      case 1: _st1 = (tom.library.sl.Strategy) child; return this;
      case 2: _condst = (tom.library.sl.Strategy) child; return this;
      case 3: _ost = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_cond, _st1, _condst, _ost};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._cond = (tom.library.sl.Strategy)children[0];
    this._st1 = (tom.library.sl.Strategy)children[1];
    this._condst = (tom.library.sl.Strategy)children[2];
    this._ost = (tom.library.sl.Strategy)children[3];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_StCond(tom.library.sl.Strategy _cond, tom.library.sl.Strategy _st1, tom.library.sl.Strategy _condst, tom.library.sl.Strategy _ost) {
    this._cond = _cond;
    this._st1 = _st1;
    this._condst = _condst;
    this._ost = _ost;

  }

  /**
    * Builds a new StCond
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_cond = _cond.visit(any,i);
    if (! (tmp_cond instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_cond = (fiacre.types.Exp) tmp_cond;

    Object tmp_st1 = _st1.visit(any,i);
    if (! (tmp_st1 instanceof fiacre.types.Statement)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Statement new_st1 = (fiacre.types.Statement) tmp_st1;

    Object tmp_condst = _condst.visit(any,i);
    if (! (tmp_condst instanceof fiacre.types.ConditionalStatementList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.ConditionalStatementList new_condst = (fiacre.types.ConditionalStatementList) tmp_condst;

    Object tmp_ost = _ost.visit(any,i);
    if (! (tmp_ost instanceof fiacre.types.Optional_Statement)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Optional_Statement new_ost = (fiacre.types.Optional_Statement) tmp_ost;

    return (T) fiacre.types.statement.StCond.make( new_cond,  new_st1,  new_condst,  new_ost);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_cond).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_cond = (fiacre.types.Exp) getEnvironment().getSubject();

    (_st1).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Statement)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Statement new_st1 = (fiacre.types.Statement) getEnvironment().getSubject();

    (_condst).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.ConditionalStatementList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.ConditionalStatementList new_condst = (fiacre.types.ConditionalStatementList) getEnvironment().getSubject();

    (_ost).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Optional_Statement)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Optional_Statement new_ost = (fiacre.types.Optional_Statement) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.statement.StCond.make( new_cond,  new_st1,  new_condst,  new_ost));
    return tom.library.sl.Environment.SUCCESS;
  }
}
