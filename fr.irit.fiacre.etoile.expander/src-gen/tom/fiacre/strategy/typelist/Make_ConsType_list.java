
package fiacre.strategy.typelist;

public class Make_ConsType_list implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadType_list;
  private tom.library.sl.Strategy _TailType_list;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadType_list;
      case 1: return _TailType_list;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadType_list = (tom.library.sl.Strategy) child; return this;
      case 1: _TailType_list = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadType_list, _TailType_list};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadType_list = (tom.library.sl.Strategy)children[0];
    this._TailType_list = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsType_list(tom.library.sl.Strategy _HeadType_list, tom.library.sl.Strategy _TailType_list) {
    this._HeadType_list = _HeadType_list;
    this._TailType_list = _TailType_list;

  }

  /**
    * Builds a new ConsType_list
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadType_list = _HeadType_list.visit(any,i);
    if (! (tmp_HeadType_list instanceof fiacre.types.Type)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Type new_HeadType_list = (fiacre.types.Type) tmp_HeadType_list;

    Object tmp_TailType_list = _TailType_list.visit(any,i);
    if (! (tmp_TailType_list instanceof fiacre.types.TypeList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.TypeList new_TailType_list = (fiacre.types.TypeList) tmp_TailType_list;

    return (T) fiacre.types.typelist.ConsType_list.make( new_HeadType_list,  new_TailType_list);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadType_list).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Type)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Type new_HeadType_list = (fiacre.types.Type) getEnvironment().getSubject();

    (_TailType_list).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.TypeList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.TypeList new_TailType_list = (fiacre.types.TypeList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.typelist.ConsType_list.make( new_HeadType_list,  new_TailType_list));
    return tom.library.sl.Environment.SUCCESS;
  }
}
