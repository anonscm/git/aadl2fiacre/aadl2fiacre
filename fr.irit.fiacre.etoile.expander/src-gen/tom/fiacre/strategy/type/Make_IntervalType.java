
package fiacre.strategy.type;

public class Make_IntervalType implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _min;
  private tom.library.sl.Strategy _max;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _min;
      case 1: return _max;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _min = (tom.library.sl.Strategy) child; return this;
      case 1: _max = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_min, _max};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._min = (tom.library.sl.Strategy)children[0];
    this._max = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_IntervalType(tom.library.sl.Strategy _min, tom.library.sl.Strategy _max) {
    this._min = _min;
    this._max = _max;

  }

  /**
    * Builds a new IntervalType
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_min = _min.visit(any,i);
    if (! (tmp_min instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_min = (fiacre.types.Exp) tmp_min;

    Object tmp_max = _max.visit(any,i);
    if (! (tmp_max instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_max = (fiacre.types.Exp) tmp_max;

    return (T) fiacre.types.type.IntervalType.make( new_min,  new_max);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_min).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_min = (fiacre.types.Exp) getEnvironment().getSubject();

    (_max).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_max = (fiacre.types.Exp) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.type.IntervalType.make( new_min,  new_max));
    return tom.library.sl.Environment.SUCCESS;
  }
}
