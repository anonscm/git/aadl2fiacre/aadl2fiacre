
package fiacre.strategy.conditionalstatementlist;

public class Make_ConsCondStList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadCondStList;
  private tom.library.sl.Strategy _TailCondStList;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadCondStList;
      case 1: return _TailCondStList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadCondStList = (tom.library.sl.Strategy) child; return this;
      case 1: _TailCondStList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadCondStList, _TailCondStList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadCondStList = (tom.library.sl.Strategy)children[0];
    this._TailCondStList = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsCondStList(tom.library.sl.Strategy _HeadCondStList, tom.library.sl.Strategy _TailCondStList) {
    this._HeadCondStList = _HeadCondStList;
    this._TailCondStList = _TailCondStList;

  }

  /**
    * Builds a new ConsCondStList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadCondStList = _HeadCondStList.visit(any,i);
    if (! (tmp_HeadCondStList instanceof fiacre.types.ConditionalStatement)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.ConditionalStatement new_HeadCondStList = (fiacre.types.ConditionalStatement) tmp_HeadCondStList;

    Object tmp_TailCondStList = _TailCondStList.visit(any,i);
    if (! (tmp_TailCondStList instanceof fiacre.types.ConditionalStatementList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.ConditionalStatementList new_TailCondStList = (fiacre.types.ConditionalStatementList) tmp_TailCondStList;

    return (T) fiacre.types.conditionalstatementlist.ConsCondStList.make( new_HeadCondStList,  new_TailCondStList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadCondStList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.ConditionalStatement)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.ConditionalStatement new_HeadCondStList = (fiacre.types.ConditionalStatement) getEnvironment().getSubject();

    (_TailCondStList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.ConditionalStatementList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.ConditionalStatementList new_TailCondStList = (fiacre.types.ConditionalStatementList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.conditionalstatementlist.ConsCondStList.make( new_HeadCondStList,  new_TailCondStList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
