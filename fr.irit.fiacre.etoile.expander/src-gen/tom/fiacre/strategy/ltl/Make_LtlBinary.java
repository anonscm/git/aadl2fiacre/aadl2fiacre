
package fiacre.strategy.ltl;

public class Make_LtlBinary implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _l1;
  private String _op;
  private tom.library.sl.Strategy _l2;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _l1;
      case 1: return _l2;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _l1 = (tom.library.sl.Strategy) child; return this;
      case 1: _l2 = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_l1, _l2};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._l1 = (tom.library.sl.Strategy)children[0];
    this._l2 = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_LtlBinary(tom.library.sl.Strategy _l1, String _op, tom.library.sl.Strategy _l2) {
    this._l1 = _l1;
    this._op = _op;
    this._l2 = _l2;

  }

  /**
    * Builds a new LtlBinary
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_l1 = _l1.visit(any,i);
    if (! (tmp_l1 instanceof fiacre.types.LTL)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.LTL new_l1 = (fiacre.types.LTL) tmp_l1;

    Object tmp_l2 = _l2.visit(any,i);
    if (! (tmp_l2 instanceof fiacre.types.LTL)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.LTL new_l2 = (fiacre.types.LTL) tmp_l2;

    return (T) fiacre.types.ltl.LtlBinary.make( new_l1,  _op,  new_l2);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_l1).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.LTL)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.LTL new_l1 = (fiacre.types.LTL) getEnvironment().getSubject();

    (_l2).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.LTL)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.LTL new_l2 = (fiacre.types.LTL) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.ltl.LtlBinary.make( new_l1,  _op,  new_l2));
    return tom.library.sl.Environment.SUCCESS;
  }
}
