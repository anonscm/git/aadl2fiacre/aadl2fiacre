
package fiacre.strategy.ltl;

public class Make_LtlEx implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private String _ind;
  private tom.library.sl.Strategy _begin;
  private tom.library.sl.Strategy _end;
  private tom.library.sl.Strategy _l;


  public int getChildCount() {
    return 3;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _begin;
      case 1: return _end;
      case 2: return _l;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _begin = (tom.library.sl.Strategy) child; return this;
      case 1: _end = (tom.library.sl.Strategy) child; return this;
      case 2: _l = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_begin, _end, _l};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._begin = (tom.library.sl.Strategy)children[0];
    this._end = (tom.library.sl.Strategy)children[1];
    this._l = (tom.library.sl.Strategy)children[2];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_LtlEx(String _ind, tom.library.sl.Strategy _begin, tom.library.sl.Strategy _end, tom.library.sl.Strategy _l) {
    this._ind = _ind;
    this._begin = _begin;
    this._end = _end;
    this._l = _l;

  }

  /**
    * Builds a new LtlEx
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_begin = _begin.visit(any,i);
    if (! (tmp_begin instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_begin = (fiacre.types.Exp) tmp_begin;

    Object tmp_end = _end.visit(any,i);
    if (! (tmp_end instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_end = (fiacre.types.Exp) tmp_end;

    Object tmp_l = _l.visit(any,i);
    if (! (tmp_l instanceof fiacre.types.LTL)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.LTL new_l = (fiacre.types.LTL) tmp_l;

    return (T) fiacre.types.ltl.LtlEx.make( _ind,  new_begin,  new_end,  new_l);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_begin).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_begin = (fiacre.types.Exp) getEnvironment().getSubject();

    (_end).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_end = (fiacre.types.Exp) getEnvironment().getSubject();

    (_l).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.LTL)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.LTL new_l = (fiacre.types.LTL) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.ltl.LtlEx.make( _ind,  new_begin,  new_end,  new_l));
    return tom.library.sl.Environment.SUCCESS;
  }
}
