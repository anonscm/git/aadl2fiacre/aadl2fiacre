
package fiacre.strategy.program_fiacre;

public class Make_Prog implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _decl;
  private tom.library.sl.Strategy _main;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _decl;
      case 1: return _main;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _decl = (tom.library.sl.Strategy) child; return this;
      case 1: _main = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_decl, _main};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._decl = (tom.library.sl.Strategy)children[0];
    this._main = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_Prog(tom.library.sl.Strategy _decl, tom.library.sl.Strategy _main) {
    this._decl = _decl;
    this._main = _main;

  }

  /**
    * Builds a new Prog
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_decl = _decl.visit(any,i);
    if (! (tmp_decl instanceof fiacre.types.Declarations)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Declarations new_decl = (fiacre.types.Declarations) tmp_decl;

    Object tmp_main = _main.visit(any,i);
    if (! (tmp_main instanceof fiacre.types.Optional_Decl)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Optional_Decl new_main = (fiacre.types.Optional_Decl) tmp_main;

    return (T) fiacre.types.program_fiacre.Prog.make( new_decl,  new_main);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_decl).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Declarations)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Declarations new_decl = (fiacre.types.Declarations) getEnvironment().getSubject();

    (_main).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Optional_Decl)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Optional_Decl new_main = (fiacre.types.Optional_Decl) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.program_fiacre.Prog.make( new_decl,  new_main));
    return tom.library.sl.Environment.SUCCESS;
  }
}
