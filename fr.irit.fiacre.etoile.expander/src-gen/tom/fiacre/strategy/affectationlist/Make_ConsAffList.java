
package fiacre.strategy.affectationlist;

public class Make_ConsAffList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadAffList;
  private tom.library.sl.Strategy _TailAffList;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadAffList;
      case 1: return _TailAffList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadAffList = (tom.library.sl.Strategy) child; return this;
      case 1: _TailAffList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadAffList, _TailAffList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadAffList = (tom.library.sl.Strategy)children[0];
    this._TailAffList = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsAffList(tom.library.sl.Strategy _HeadAffList, tom.library.sl.Strategy _TailAffList) {
    this._HeadAffList = _HeadAffList;
    this._TailAffList = _TailAffList;

  }

  /**
    * Builds a new ConsAffList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadAffList = _HeadAffList.visit(any,i);
    if (! (tmp_HeadAffList instanceof fiacre.types.Affectation)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Affectation new_HeadAffList = (fiacre.types.Affectation) tmp_HeadAffList;

    Object tmp_TailAffList = _TailAffList.visit(any,i);
    if (! (tmp_TailAffList instanceof fiacre.types.AffectationList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.AffectationList new_TailAffList = (fiacre.types.AffectationList) tmp_TailAffList;

    return (T) fiacre.types.affectationlist.ConsAffList.make( new_HeadAffList,  new_TailAffList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadAffList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Affectation)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Affectation new_HeadAffList = (fiacre.types.Affectation) getEnvironment().getSubject();

    (_TailAffList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.AffectationList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.AffectationList new_TailAffList = (fiacre.types.AffectationList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.affectationlist.ConsAffList.make( new_HeadAffList,  new_TailAffList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
