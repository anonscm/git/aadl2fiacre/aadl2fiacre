
package fiacre.strategy.pstatementlist;

public class Make_ConsPStmList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadPStmList;
  private tom.library.sl.Strategy _TailPStmList;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadPStmList;
      case 1: return _TailPStmList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadPStmList = (tom.library.sl.Strategy) child; return this;
      case 1: _TailPStmList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadPStmList, _TailPStmList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadPStmList = (tom.library.sl.Strategy)children[0];
    this._TailPStmList = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsPStmList(tom.library.sl.Strategy _HeadPStmList, tom.library.sl.Strategy _TailPStmList) {
    this._HeadPStmList = _HeadPStmList;
    this._TailPStmList = _TailPStmList;

  }

  /**
    * Builds a new ConsPStmList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadPStmList = _HeadPStmList.visit(any,i);
    if (! (tmp_HeadPStmList instanceof fiacre.types.StatementList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.StatementList new_HeadPStmList = (fiacre.types.StatementList) tmp_HeadPStmList;

    Object tmp_TailPStmList = _TailPStmList.visit(any,i);
    if (! (tmp_TailPStmList instanceof fiacre.types.PStatementList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PStatementList new_TailPStmList = (fiacre.types.PStatementList) tmp_TailPStmList;

    return (T) fiacre.types.pstatementlist.ConsPStmList.make( new_HeadPStmList,  new_TailPStmList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadPStmList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.StatementList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.StatementList new_HeadPStmList = (fiacre.types.StatementList) getEnvironment().getSubject();

    (_TailPStmList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PStatementList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PStatementList new_TailPStmList = (fiacre.types.PStatementList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.pstatementlist.ConsPStmList.make( new_HeadPStmList,  new_TailPStmList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
