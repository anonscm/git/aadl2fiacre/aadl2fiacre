
package fiacre.strategy.arglist;

public class Make_ConsArList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadArList;
  private tom.library.sl.Strategy _TailArList;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadArList;
      case 1: return _TailArList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadArList = (tom.library.sl.Strategy) child; return this;
      case 1: _TailArList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadArList, _TailArList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadArList = (tom.library.sl.Strategy)children[0];
    this._TailArList = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsArList(tom.library.sl.Strategy _HeadArList, tom.library.sl.Strategy _TailArList) {
    this._HeadArList = _HeadArList;
    this._TailArList = _TailArList;

  }

  /**
    * Builds a new ConsArList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadArList = _HeadArList.visit(any,i);
    if (! (tmp_HeadArList instanceof fiacre.types.Arg)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Arg new_HeadArList = (fiacre.types.Arg) tmp_HeadArList;

    Object tmp_TailArList = _TailArList.visit(any,i);
    if (! (tmp_TailArList instanceof fiacre.types.ArgList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.ArgList new_TailArList = (fiacre.types.ArgList) tmp_TailArList;

    return (T) fiacre.types.arglist.ConsArList.make( new_HeadArList,  new_TailArList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadArList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Arg)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Arg new_HeadArList = (fiacre.types.Arg) getEnvironment().getSubject();

    (_TailArList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.ArgList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.ArgList new_TailArList = (fiacre.types.ArgList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.arglist.ConsArList.make( new_HeadArList,  new_TailArList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
