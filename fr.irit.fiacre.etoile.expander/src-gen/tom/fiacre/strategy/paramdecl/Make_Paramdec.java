
package fiacre.strategy.paramdecl;

public class Make_Paramdec implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _lal;
  private tom.library.sl.Strategy _val;
  private tom.library.sl.Strategy _type;


  public int getChildCount() {
    return 3;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _lal;
      case 1: return _val;
      case 2: return _type;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _lal = (tom.library.sl.Strategy) child; return this;
      case 1: _val = (tom.library.sl.Strategy) child; return this;
      case 2: _type = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_lal, _val, _type};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._lal = (tom.library.sl.Strategy)children[0];
    this._val = (tom.library.sl.Strategy)children[1];
    this._type = (tom.library.sl.Strategy)children[2];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_Paramdec(tom.library.sl.Strategy _lal, tom.library.sl.Strategy _val, tom.library.sl.Strategy _type) {
    this._lal = _lal;
    this._val = _val;
    this._type = _type;

  }

  /**
    * Builds a new Paramdec
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_lal = _lal.visit(any,i);
    if (! (tmp_lal instanceof fiacre.types.ArgList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.ArgList new_lal = (fiacre.types.ArgList) tmp_lal;

    Object tmp_val = _val.visit(any,i);
    if (! (tmp_val instanceof fiacre.types.VarAttrList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.VarAttrList new_val = (fiacre.types.VarAttrList) tmp_val;

    Object tmp_type = _type.visit(any,i);
    if (! (tmp_type instanceof fiacre.types.Type)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Type new_type = (fiacre.types.Type) tmp_type;

    return (T) fiacre.types.paramdecl.Paramdec.make( new_lal,  new_val,  new_type);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_lal).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.ArgList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.ArgList new_lal = (fiacre.types.ArgList) getEnvironment().getSubject();

    (_val).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.VarAttrList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.VarAttrList new_val = (fiacre.types.VarAttrList) getEnvironment().getSubject();

    (_type).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Type)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Type new_type = (fiacre.types.Type) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.paramdecl.Paramdec.make( new_lal,  new_val,  new_type));
    return tom.library.sl.Environment.SUCCESS;
  }
}
