
package fiacre.strategy.paramdecls;

public class Make_ConsListParamDecl implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadListParamDecl;
  private tom.library.sl.Strategy _TailListParamDecl;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadListParamDecl;
      case 1: return _TailListParamDecl;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadListParamDecl = (tom.library.sl.Strategy) child; return this;
      case 1: _TailListParamDecl = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadListParamDecl, _TailListParamDecl};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadListParamDecl = (tom.library.sl.Strategy)children[0];
    this._TailListParamDecl = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsListParamDecl(tom.library.sl.Strategy _HeadListParamDecl, tom.library.sl.Strategy _TailListParamDecl) {
    this._HeadListParamDecl = _HeadListParamDecl;
    this._TailListParamDecl = _TailListParamDecl;

  }

  /**
    * Builds a new ConsListParamDecl
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadListParamDecl = _HeadListParamDecl.visit(any,i);
    if (! (tmp_HeadListParamDecl instanceof fiacre.types.ParamDecl)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.ParamDecl new_HeadListParamDecl = (fiacre.types.ParamDecl) tmp_HeadListParamDecl;

    Object tmp_TailListParamDecl = _TailListParamDecl.visit(any,i);
    if (! (tmp_TailListParamDecl instanceof fiacre.types.ParamDecls)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.ParamDecls new_TailListParamDecl = (fiacre.types.ParamDecls) tmp_TailListParamDecl;

    return (T) fiacre.types.paramdecls.ConsListParamDecl.make( new_HeadListParamDecl,  new_TailListParamDecl);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadListParamDecl).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.ParamDecl)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.ParamDecl new_HeadListParamDecl = (fiacre.types.ParamDecl) getEnvironment().getSubject();

    (_TailListParamDecl).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.ParamDecls)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.ParamDecls new_TailListParamDecl = (fiacre.types.ParamDecls) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.paramdecls.ConsListParamDecl.make( new_HeadListParamDecl,  new_TailListParamDecl));
    return tom.library.sl.Environment.SUCCESS;
  }
}
