
package fiacre.strategy.vardecls;

public class Make_ConsListVarDecl implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadListVarDecl;
  private tom.library.sl.Strategy _TailListVarDecl;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadListVarDecl;
      case 1: return _TailListVarDecl;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadListVarDecl = (tom.library.sl.Strategy) child; return this;
      case 1: _TailListVarDecl = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadListVarDecl, _TailListVarDecl};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadListVarDecl = (tom.library.sl.Strategy)children[0];
    this._TailListVarDecl = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsListVarDecl(tom.library.sl.Strategy _HeadListVarDecl, tom.library.sl.Strategy _TailListVarDecl) {
    this._HeadListVarDecl = _HeadListVarDecl;
    this._TailListVarDecl = _TailListVarDecl;

  }

  /**
    * Builds a new ConsListVarDecl
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadListVarDecl = _HeadListVarDecl.visit(any,i);
    if (! (tmp_HeadListVarDecl instanceof fiacre.types.VarDecl)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.VarDecl new_HeadListVarDecl = (fiacre.types.VarDecl) tmp_HeadListVarDecl;

    Object tmp_TailListVarDecl = _TailListVarDecl.visit(any,i);
    if (! (tmp_TailListVarDecl instanceof fiacre.types.VarDecls)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.VarDecls new_TailListVarDecl = (fiacre.types.VarDecls) tmp_TailListVarDecl;

    return (T) fiacre.types.vardecls.ConsListVarDecl.make( new_HeadListVarDecl,  new_TailListVarDecl);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadListVarDecl).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.VarDecl)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.VarDecl new_HeadListVarDecl = (fiacre.types.VarDecl) getEnvironment().getSubject();

    (_TailListVarDecl).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.VarDecls)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.VarDecls new_TailListVarDecl = (fiacre.types.VarDecls) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.vardecls.ConsListVarDecl.make( new_HeadListVarDecl,  new_TailListVarDecl));
    return tom.library.sl.Environment.SUCCESS;
  }
}
