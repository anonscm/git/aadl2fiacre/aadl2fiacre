
package fiacre.strategy.portcomplist;

public class Make_ConsPCList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadPCList;
  private tom.library.sl.Strategy _TailPCList;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadPCList;
      case 1: return _TailPCList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadPCList = (tom.library.sl.Strategy) child; return this;
      case 1: _TailPCList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadPCList, _TailPCList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadPCList = (tom.library.sl.Strategy)children[0];
    this._TailPCList = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsPCList(tom.library.sl.Strategy _HeadPCList, tom.library.sl.Strategy _TailPCList) {
    this._HeadPCList = _HeadPCList;
    this._TailPCList = _TailPCList;

  }

  /**
    * Builds a new ConsPCList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadPCList = _HeadPCList.visit(any,i);
    if (! (tmp_HeadPCList instanceof fiacre.types.PortComposition)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PortComposition new_HeadPCList = (fiacre.types.PortComposition) tmp_HeadPCList;

    Object tmp_TailPCList = _TailPCList.visit(any,i);
    if (! (tmp_TailPCList instanceof fiacre.types.PortCompList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PortCompList new_TailPCList = (fiacre.types.PortCompList) tmp_TailPCList;

    return (T) fiacre.types.portcomplist.ConsPCList.make( new_HeadPCList,  new_TailPCList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadPCList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PortComposition)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PortComposition new_HeadPCList = (fiacre.types.PortComposition) getEnvironment().getSubject();

    (_TailPCList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PortCompList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PortCompList new_TailPCList = (fiacre.types.PortCompList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.portcomplist.ConsPCList.make( new_HeadPCList,  new_TailPCList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
