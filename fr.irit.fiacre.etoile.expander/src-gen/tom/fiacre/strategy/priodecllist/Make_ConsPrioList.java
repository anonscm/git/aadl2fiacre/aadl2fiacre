
package fiacre.strategy.priodecllist;

public class Make_ConsPrioList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadPrioList;
  private tom.library.sl.Strategy _TailPrioList;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadPrioList;
      case 1: return _TailPrioList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadPrioList = (tom.library.sl.Strategy) child; return this;
      case 1: _TailPrioList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadPrioList, _TailPrioList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadPrioList = (tom.library.sl.Strategy)children[0];
    this._TailPrioList = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsPrioList(tom.library.sl.Strategy _HeadPrioList, tom.library.sl.Strategy _TailPrioList) {
    this._HeadPrioList = _HeadPrioList;
    this._TailPrioList = _TailPrioList;

  }

  /**
    * Builds a new ConsPrioList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadPrioList = _HeadPrioList.visit(any,i);
    if (! (tmp_HeadPrioList instanceof fiacre.types.PriorDecl)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PriorDecl new_HeadPrioList = (fiacre.types.PriorDecl) tmp_HeadPrioList;

    Object tmp_TailPrioList = _TailPrioList.visit(any,i);
    if (! (tmp_TailPrioList instanceof fiacre.types.PrioDeclList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PrioDeclList new_TailPrioList = (fiacre.types.PrioDeclList) tmp_TailPrioList;

    return (T) fiacre.types.priodecllist.ConsPrioList.make( new_HeadPrioList,  new_TailPrioList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadPrioList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PriorDecl)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PriorDecl new_HeadPrioList = (fiacre.types.PriorDecl) getEnvironment().getSubject();

    (_TailPrioList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PrioDeclList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PrioDeclList new_TailPrioList = (fiacre.types.PrioDeclList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.priodecllist.ConsPrioList.make( new_HeadPrioList,  new_TailPrioList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
