
package fiacre.strategy.portcomposition;

public class Make_PortComp implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _ps;
  private tom.library.sl.Strategy _comp;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _ps;
      case 1: return _comp;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _ps = (tom.library.sl.Strategy) child; return this;
      case 1: _comp = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_ps, _comp};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._ps = (tom.library.sl.Strategy)children[0];
    this._comp = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_PortComp(tom.library.sl.Strategy _ps, tom.library.sl.Strategy _comp) {
    this._ps = _ps;
    this._comp = _comp;

  }

  /**
    * Builds a new PortComp
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_ps = _ps.visit(any,i);
    if (! (tmp_ps instanceof fiacre.types.PortSet)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PortSet new_ps = (fiacre.types.PortSet) tmp_ps;

    Object tmp_comp = _comp.visit(any,i);
    if (! (tmp_comp instanceof fiacre.types.Composition)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Composition new_comp = (fiacre.types.Composition) tmp_comp;

    return (T) fiacre.types.portcomposition.PortComp.make( new_ps,  new_comp);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_ps).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PortSet)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PortSet new_ps = (fiacre.types.PortSet) getEnvironment().getSubject();

    (_comp).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Composition)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Composition new_comp = (fiacre.types.Composition) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.portcomposition.PortComp.make( new_ps,  new_comp));
    return tom.library.sl.Environment.SUCCESS;
  }
}
