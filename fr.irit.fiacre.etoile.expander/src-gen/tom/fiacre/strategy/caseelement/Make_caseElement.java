
package fiacre.strategy.caseelement;

public class Make_caseElement implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private boolean _b;
  private tom.library.sl.Strategy _p;
  private tom.library.sl.Strategy _st;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _p;
      case 1: return _st;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _p = (tom.library.sl.Strategy) child; return this;
      case 1: _st = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_p, _st};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._p = (tom.library.sl.Strategy)children[0];
    this._st = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_caseElement(boolean _b, tom.library.sl.Strategy _p, tom.library.sl.Strategy _st) {
    this._b = _b;
    this._p = _p;
    this._st = _st;

  }

  /**
    * Builds a new caseElement
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_p = _p.visit(any,i);
    if (! (tmp_p instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_p = (fiacre.types.Exp) tmp_p;

    Object tmp_st = _st.visit(any,i);
    if (! (tmp_st instanceof fiacre.types.Statement)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Statement new_st = (fiacre.types.Statement) tmp_st;

    return (T) fiacre.types.caseelement.caseElement.make( _b,  new_p,  new_st);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_p).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_p = (fiacre.types.Exp) getEnvironment().getSubject();

    (_st).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Statement)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Statement new_st = (fiacre.types.Statement) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.caseelement.caseElement.make( _b,  new_p,  new_st));
    return tom.library.sl.Environment.SUCCESS;
  }
}
