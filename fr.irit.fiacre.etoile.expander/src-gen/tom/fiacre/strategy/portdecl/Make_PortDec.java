
package fiacre.strategy.portdecl;

public class Make_PortDec implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _lp;
  private tom.library.sl.Strategy _type;
  private tom.library.sl.Strategy _pal;
  private tom.library.sl.Strategy _chan;


  public int getChildCount() {
    return 4;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _lp;
      case 1: return _type;
      case 2: return _pal;
      case 3: return _chan;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _lp = (tom.library.sl.Strategy) child; return this;
      case 1: _type = (tom.library.sl.Strategy) child; return this;
      case 2: _pal = (tom.library.sl.Strategy) child; return this;
      case 3: _chan = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_lp, _type, _pal, _chan};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._lp = (tom.library.sl.Strategy)children[0];
    this._type = (tom.library.sl.Strategy)children[1];
    this._pal = (tom.library.sl.Strategy)children[2];
    this._chan = (tom.library.sl.Strategy)children[3];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_PortDec(tom.library.sl.Strategy _lp, tom.library.sl.Strategy _type, tom.library.sl.Strategy _pal, tom.library.sl.Strategy _chan) {
    this._lp = _lp;
    this._type = _type;
    this._pal = _pal;
    this._chan = _chan;

  }

  /**
    * Builds a new PortDec
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_lp = _lp.visit(any,i);
    if (! (tmp_lp instanceof fiacre.types.StringList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.StringList new_lp = (fiacre.types.StringList) tmp_lp;

    Object tmp_type = _type.visit(any,i);
    if (! (tmp_type instanceof fiacre.types.Type)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Type new_type = (fiacre.types.Type) tmp_type;

    Object tmp_pal = _pal.visit(any,i);
    if (! (tmp_pal instanceof fiacre.types.PortAttrList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PortAttrList new_pal = (fiacre.types.PortAttrList) tmp_pal;

    Object tmp_chan = _chan.visit(any,i);
    if (! (tmp_chan instanceof fiacre.types.Channel)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Channel new_chan = (fiacre.types.Channel) tmp_chan;

    return (T) fiacre.types.portdecl.PortDec.make( new_lp,  new_type,  new_pal,  new_chan);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_lp).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.StringList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.StringList new_lp = (fiacre.types.StringList) getEnvironment().getSubject();

    (_type).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Type)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Type new_type = (fiacre.types.Type) getEnvironment().getSubject();

    (_pal).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PortAttrList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PortAttrList new_pal = (fiacre.types.PortAttrList) getEnvironment().getSubject();

    (_chan).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Channel)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Channel new_chan = (fiacre.types.Channel) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.portdecl.PortDec.make( new_lp,  new_type,  new_pal,  new_chan));
    return tom.library.sl.Environment.SUCCESS;
  }
}
