
package fiacre.strategy.lpdlist;

public class Make_ConslpdList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _HeadlpdList;
  private tom.library.sl.Strategy _TaillpdList;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _HeadlpdList;
      case 1: return _TaillpdList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _HeadlpdList = (tom.library.sl.Strategy) child; return this;
      case 1: _TaillpdList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_HeadlpdList, _TaillpdList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._HeadlpdList = (tom.library.sl.Strategy)children[0];
    this._TaillpdList = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConslpdList(tom.library.sl.Strategy _HeadlpdList, tom.library.sl.Strategy _TaillpdList) {
    this._HeadlpdList = _HeadlpdList;
    this._TaillpdList = _TaillpdList;

  }

  /**
    * Builds a new ConslpdList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_HeadlpdList = _HeadlpdList.visit(any,i);
    if (! (tmp_HeadlpdList instanceof fiacre.types.LocPortDecl)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.LocPortDecl new_HeadlpdList = (fiacre.types.LocPortDecl) tmp_HeadlpdList;

    Object tmp_TaillpdList = _TaillpdList.visit(any,i);
    if (! (tmp_TaillpdList instanceof fiacre.types.LPDList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.LPDList new_TaillpdList = (fiacre.types.LPDList) tmp_TaillpdList;

    return (T) fiacre.types.lpdlist.ConslpdList.make( new_HeadlpdList,  new_TaillpdList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_HeadlpdList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.LocPortDecl)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.LocPortDecl new_HeadlpdList = (fiacre.types.LocPortDecl) getEnvironment().getSubject();

    (_TaillpdList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.LPDList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.LPDList new_TaillpdList = (fiacre.types.LPDList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.lpdlist.ConslpdList.make( new_HeadlpdList,  new_TaillpdList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
