
package fiacre.strategy.exp;

public class Make_RecordAccessExp implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _record;
  private String _id;
  private tom.library.sl.Strategy _type;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _record;
      case 1: return _type;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _record = (tom.library.sl.Strategy) child; return this;
      case 1: _type = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_record, _type};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._record = (tom.library.sl.Strategy)children[0];
    this._type = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_RecordAccessExp(tom.library.sl.Strategy _record, String _id, tom.library.sl.Strategy _type) {
    this._record = _record;
    this._id = _id;
    this._type = _type;

  }

  /**
    * Builds a new RecordAccessExp
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_record = _record.visit(any,i);
    if (! (tmp_record instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_record = (fiacre.types.Exp) tmp_record;

    Object tmp_type = _type.visit(any,i);
    if (! (tmp_type instanceof fiacre.types.Type)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Type new_type = (fiacre.types.Type) tmp_type;

    return (T) fiacre.types.exp.RecordAccessExp.make( new_record,  _id,  new_type);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_record).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_record = (fiacre.types.Exp) getEnvironment().getSubject();

    (_type).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Type)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Type new_type = (fiacre.types.Type) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.exp.RecordAccessExp.make( new_record,  _id,  new_type));
    return tom.library.sl.Environment.SUCCESS;
  }
}
