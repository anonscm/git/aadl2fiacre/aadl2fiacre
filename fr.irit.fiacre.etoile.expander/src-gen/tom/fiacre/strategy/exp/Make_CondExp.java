
package fiacre.strategy.exp;

public class Make_CondExp implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _test;
  private tom.library.sl.Strategy _st1;
  private tom.library.sl.Strategy _st2;
  private tom.library.sl.Strategy _type;


  public int getChildCount() {
    return 4;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _test;
      case 1: return _st1;
      case 2: return _st2;
      case 3: return _type;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _test = (tom.library.sl.Strategy) child; return this;
      case 1: _st1 = (tom.library.sl.Strategy) child; return this;
      case 2: _st2 = (tom.library.sl.Strategy) child; return this;
      case 3: _type = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_test, _st1, _st2, _type};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._test = (tom.library.sl.Strategy)children[0];
    this._st1 = (tom.library.sl.Strategy)children[1];
    this._st2 = (tom.library.sl.Strategy)children[2];
    this._type = (tom.library.sl.Strategy)children[3];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_CondExp(tom.library.sl.Strategy _test, tom.library.sl.Strategy _st1, tom.library.sl.Strategy _st2, tom.library.sl.Strategy _type) {
    this._test = _test;
    this._st1 = _st1;
    this._st2 = _st2;
    this._type = _type;

  }

  /**
    * Builds a new CondExp
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_test = _test.visit(any,i);
    if (! (tmp_test instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_test = (fiacre.types.Exp) tmp_test;

    Object tmp_st1 = _st1.visit(any,i);
    if (! (tmp_st1 instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_st1 = (fiacre.types.Exp) tmp_st1;

    Object tmp_st2 = _st2.visit(any,i);
    if (! (tmp_st2 instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_st2 = (fiacre.types.Exp) tmp_st2;

    Object tmp_type = _type.visit(any,i);
    if (! (tmp_type instanceof fiacre.types.Type)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Type new_type = (fiacre.types.Type) tmp_type;

    return (T) fiacre.types.exp.CondExp.make( new_test,  new_st1,  new_st2,  new_type);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_test).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_test = (fiacre.types.Exp) getEnvironment().getSubject();

    (_st1).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_st1 = (fiacre.types.Exp) getEnvironment().getSubject();

    (_st2).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_st2 = (fiacre.types.Exp) getEnvironment().getSubject();

    (_type).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Type)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Type new_type = (fiacre.types.Type) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.exp.CondExp.make( new_test,  new_st1,  new_st2,  new_type));
    return tom.library.sl.Environment.SUCCESS;
  }
}
