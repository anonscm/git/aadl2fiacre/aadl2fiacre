
package fiacre.strategy.exp;

public class Make_AllExp implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private String _i;
  private tom.library.sl.Strategy _begin;
  private tom.library.sl.Strategy _end;
  private tom.library.sl.Strategy _e;


  public int getChildCount() {
    return 3;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _begin;
      case 1: return _end;
      case 2: return _e;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _begin = (tom.library.sl.Strategy) child; return this;
      case 1: _end = (tom.library.sl.Strategy) child; return this;
      case 2: _e = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_begin, _end, _e};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._begin = (tom.library.sl.Strategy)children[0];
    this._end = (tom.library.sl.Strategy)children[1];
    this._e = (tom.library.sl.Strategy)children[2];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_AllExp(String _i, tom.library.sl.Strategy _begin, tom.library.sl.Strategy _end, tom.library.sl.Strategy _e) {
    this._i = _i;
    this._begin = _begin;
    this._end = _end;
    this._e = _e;

  }

  /**
    * Builds a new AllExp
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_begin = _begin.visit(any,i);
    if (! (tmp_begin instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_begin = (fiacre.types.Exp) tmp_begin;

    Object tmp_end = _end.visit(any,i);
    if (! (tmp_end instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_end = (fiacre.types.Exp) tmp_end;

    Object tmp_e = _e.visit(any,i);
    if (! (tmp_e instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_e = (fiacre.types.Exp) tmp_e;

    return (T) fiacre.types.exp.AllExp.make( _i,  new_begin,  new_end,  new_e);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_begin).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_begin = (fiacre.types.Exp) getEnvironment().getSubject();

    (_end).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_end = (fiacre.types.Exp) getEnvironment().getSubject();

    (_e).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_e = (fiacre.types.Exp) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.exp.AllExp.make( _i,  new_begin,  new_end,  new_e));
    return tom.library.sl.Environment.SUCCESS;
  }
}
