
package fiacre.strategy.exp;

public class Make_Infix implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _iop;
  private tom.library.sl.Strategy _type;
  private tom.library.sl.Strategy _e1;
  private tom.library.sl.Strategy _e2;


  public int getChildCount() {
    return 4;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _iop;
      case 1: return _type;
      case 2: return _e1;
      case 3: return _e2;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _iop = (tom.library.sl.Strategy) child; return this;
      case 1: _type = (tom.library.sl.Strategy) child; return this;
      case 2: _e1 = (tom.library.sl.Strategy) child; return this;
      case 3: _e2 = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_iop, _type, _e1, _e2};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._iop = (tom.library.sl.Strategy)children[0];
    this._type = (tom.library.sl.Strategy)children[1];
    this._e1 = (tom.library.sl.Strategy)children[2];
    this._e2 = (tom.library.sl.Strategy)children[3];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_Infix(tom.library.sl.Strategy _iop, tom.library.sl.Strategy _type, tom.library.sl.Strategy _e1, tom.library.sl.Strategy _e2) {
    this._iop = _iop;
    this._type = _type;
    this._e1 = _e1;
    this._e2 = _e2;

  }

  /**
    * Builds a new Infix
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_iop = _iop.visit(any,i);
    if (! (tmp_iop instanceof fiacre.types.Infix)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Infix new_iop = (fiacre.types.Infix) tmp_iop;

    Object tmp_type = _type.visit(any,i);
    if (! (tmp_type instanceof fiacre.types.Type)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Type new_type = (fiacre.types.Type) tmp_type;

    Object tmp_e1 = _e1.visit(any,i);
    if (! (tmp_e1 instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_e1 = (fiacre.types.Exp) tmp_e1;

    Object tmp_e2 = _e2.visit(any,i);
    if (! (tmp_e2 instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_e2 = (fiacre.types.Exp) tmp_e2;

    return (T) fiacre.types.exp.Infix.make( new_iop,  new_type,  new_e1,  new_e2);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_iop).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Infix)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Infix new_iop = (fiacre.types.Infix) getEnvironment().getSubject();

    (_type).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Type)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Type new_type = (fiacre.types.Type) getEnvironment().getSubject();

    (_e1).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_e1 = (fiacre.types.Exp) getEnvironment().getSubject();

    (_e2).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_e2 = (fiacre.types.Exp) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.exp.Infix.make( new_iop,  new_type,  new_e1,  new_e2));
    return tom.library.sl.Environment.SUCCESS;
  }
}
