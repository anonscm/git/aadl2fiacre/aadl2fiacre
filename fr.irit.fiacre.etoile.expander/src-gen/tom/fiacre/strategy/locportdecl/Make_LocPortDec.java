
package fiacre.strategy.locportdecl;

public class Make_LocPortDec implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _pdl;
  private tom.library.sl.Strategy _b1;
  private tom.library.sl.Strategy _b2;


  public int getChildCount() {
    return 3;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _pdl;
      case 1: return _b1;
      case 2: return _b2;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _pdl = (tom.library.sl.Strategy) child; return this;
      case 1: _b1 = (tom.library.sl.Strategy) child; return this;
      case 2: _b2 = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_pdl, _b1, _b2};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._pdl = (tom.library.sl.Strategy)children[0];
    this._b1 = (tom.library.sl.Strategy)children[1];
    this._b2 = (tom.library.sl.Strategy)children[2];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_LocPortDec(tom.library.sl.Strategy _pdl, tom.library.sl.Strategy _b1, tom.library.sl.Strategy _b2) {
    this._pdl = _pdl;
    this._b1 = _b1;
    this._b2 = _b2;

  }

  /**
    * Builds a new LocPortDec
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_pdl = _pdl.visit(any,i);
    if (! (tmp_pdl instanceof fiacre.types.PortDecls)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PortDecls new_pdl = (fiacre.types.PortDecls) tmp_pdl;

    Object tmp_b1 = _b1.visit(any,i);
    if (! (tmp_b1 instanceof fiacre.types.Bound)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Bound new_b1 = (fiacre.types.Bound) tmp_b1;

    Object tmp_b2 = _b2.visit(any,i);
    if (! (tmp_b2 instanceof fiacre.types.Bound)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Bound new_b2 = (fiacre.types.Bound) tmp_b2;

    return (T) fiacre.types.locportdecl.LocPortDec.make( new_pdl,  new_b1,  new_b2);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_pdl).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PortDecls)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PortDecls new_pdl = (fiacre.types.PortDecls) getEnvironment().getSubject();

    (_b1).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Bound)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Bound new_b1 = (fiacre.types.Bound) getEnvironment().getSubject();

    (_b2).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Bound)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Bound new_b2 = (fiacre.types.Bound) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.locportdecl.LocPortDec.make( new_pdl,  new_b1,  new_b2));
    return tom.library.sl.Environment.SUCCESS;
  }
}
