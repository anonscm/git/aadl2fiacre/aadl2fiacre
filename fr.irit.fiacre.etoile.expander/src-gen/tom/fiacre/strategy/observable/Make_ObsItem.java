
package fiacre.strategy.observable;

public class Make_ObsItem implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private tom.library.sl.Strategy _p;
  private tom.library.sl.Strategy _i;


  public int getChildCount() {
    return 2;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _p;
      case 1: return _i;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _p = (tom.library.sl.Strategy) child; return this;
      case 1: _i = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_p, _i};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._p = (tom.library.sl.Strategy)children[0];
    this._i = (tom.library.sl.Strategy)children[1];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ObsItem(tom.library.sl.Strategy _p, tom.library.sl.Strategy _i) {
    this._p = _p;
    this._i = _i;

  }

  /**
    * Builds a new ObsItem
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_p = _p.visit(any,i);
    if (! (tmp_p instanceof fiacre.types.OPath)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.OPath new_p = (fiacre.types.OPath) tmp_p;

    Object tmp_i = _i.visit(any,i);
    if (! (tmp_i instanceof fiacre.types.Item)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Item new_i = (fiacre.types.Item) tmp_i;

    return (T) fiacre.types.observable.ObsItem.make( new_p,  new_i);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_p).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.OPath)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.OPath new_p = (fiacre.types.OPath) getEnvironment().getSubject();

    (_i).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Item)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Item new_i = (fiacre.types.Item) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.observable.ObsItem.make( new_p,  new_i));
    return tom.library.sl.Environment.SUCCESS;
  }
}
