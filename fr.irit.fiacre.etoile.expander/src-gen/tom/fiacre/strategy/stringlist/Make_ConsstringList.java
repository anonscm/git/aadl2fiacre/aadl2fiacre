
package fiacre.strategy.stringlist;

public class Make_ConsstringList implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private String _HeadstringList;
  private tom.library.sl.Strategy _TailstringList;


  public int getChildCount() {
    return 1;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _TailstringList;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _TailstringList = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_TailstringList};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._TailstringList = (tom.library.sl.Strategy)children[0];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ConsstringList(String _HeadstringList, tom.library.sl.Strategy _TailstringList) {
    this._HeadstringList = _HeadstringList;
    this._TailstringList = _TailstringList;

  }

  /**
    * Builds a new ConsstringList
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_TailstringList = _TailstringList.visit(any,i);
    if (! (tmp_TailstringList instanceof fiacre.types.StringList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.StringList new_TailstringList = (fiacre.types.StringList) tmp_TailstringList;

    return (T) fiacre.types.stringlist.ConsstringList.make( _HeadstringList,  new_TailstringList);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_TailstringList).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.StringList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.StringList new_TailstringList = (fiacre.types.StringList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.stringlist.ConsstringList.make( _HeadstringList,  new_TailstringList));
    return tom.library.sl.Environment.SUCCESS;
  }
}
