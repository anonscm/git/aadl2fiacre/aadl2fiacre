
package fiacre.strategy.declaration;

public class Make_ChannelDecl implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private String _name;
  private tom.library.sl.Strategy _bodyC;


  public int getChildCount() {
    return 1;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _bodyC;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _bodyC = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_bodyC};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._bodyC = (tom.library.sl.Strategy)children[0];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ChannelDecl(String _name, tom.library.sl.Strategy _bodyC) {
    this._name = _name;
    this._bodyC = _bodyC;

  }

  /**
    * Builds a new ChannelDecl
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_bodyC = _bodyC.visit(any,i);
    if (! (tmp_bodyC instanceof fiacre.types.Channel)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Channel new_bodyC = (fiacre.types.Channel) tmp_bodyC;

    return (T) fiacre.types.declaration.ChannelDecl.make( _name,  new_bodyC);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_bodyC).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Channel)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Channel new_bodyC = (fiacre.types.Channel) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.declaration.ChannelDecl.make( _name,  new_bodyC));
    return tom.library.sl.Environment.SUCCESS;
  }
}
