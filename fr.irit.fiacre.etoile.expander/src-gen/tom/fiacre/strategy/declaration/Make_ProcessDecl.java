
package fiacre.strategy.declaration;

public class Make_ProcessDecl implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private String _name;
  private tom.library.sl.Strategy _genericParams;
  private tom.library.sl.Strategy _ports;
  private tom.library.sl.Strategy _params;
  private tom.library.sl.Strategy _states;
  private tom.library.sl.Strategy _vars;
  private tom.library.sl.Strategy _init;
  private tom.library.sl.Strategy _trans;


  public int getChildCount() {
    return 7;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _genericParams;
      case 1: return _ports;
      case 2: return _params;
      case 3: return _states;
      case 4: return _vars;
      case 5: return _init;
      case 6: return _trans;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _genericParams = (tom.library.sl.Strategy) child; return this;
      case 1: _ports = (tom.library.sl.Strategy) child; return this;
      case 2: _params = (tom.library.sl.Strategy) child; return this;
      case 3: _states = (tom.library.sl.Strategy) child; return this;
      case 4: _vars = (tom.library.sl.Strategy) child; return this;
      case 5: _init = (tom.library.sl.Strategy) child; return this;
      case 6: _trans = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_genericParams, _ports, _params, _states, _vars, _init, _trans};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._genericParams = (tom.library.sl.Strategy)children[0];
    this._ports = (tom.library.sl.Strategy)children[1];
    this._params = (tom.library.sl.Strategy)children[2];
    this._states = (tom.library.sl.Strategy)children[3];
    this._vars = (tom.library.sl.Strategy)children[4];
    this._init = (tom.library.sl.Strategy)children[5];
    this._trans = (tom.library.sl.Strategy)children[6];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_ProcessDecl(String _name, tom.library.sl.Strategy _genericParams, tom.library.sl.Strategy _ports, tom.library.sl.Strategy _params, tom.library.sl.Strategy _states, tom.library.sl.Strategy _vars, tom.library.sl.Strategy _init, tom.library.sl.Strategy _trans) {
    this._name = _name;
    this._genericParams = _genericParams;
    this._ports = _ports;
    this._params = _params;
    this._states = _states;
    this._vars = _vars;
    this._init = _init;
    this._trans = _trans;

  }

  /**
    * Builds a new ProcessDecl
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_genericParams = _genericParams.visit(any,i);
    if (! (tmp_genericParams instanceof fiacre.types.StringList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.StringList new_genericParams = (fiacre.types.StringList) tmp_genericParams;

    Object tmp_ports = _ports.visit(any,i);
    if (! (tmp_ports instanceof fiacre.types.PortDecls)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PortDecls new_ports = (fiacre.types.PortDecls) tmp_ports;

    Object tmp_params = _params.visit(any,i);
    if (! (tmp_params instanceof fiacre.types.ParamDecls)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.ParamDecls new_params = (fiacre.types.ParamDecls) tmp_params;

    Object tmp_states = _states.visit(any,i);
    if (! (tmp_states instanceof fiacre.types.StringList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.StringList new_states = (fiacre.types.StringList) tmp_states;

    Object tmp_vars = _vars.visit(any,i);
    if (! (tmp_vars instanceof fiacre.types.VarDecls)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.VarDecls new_vars = (fiacre.types.VarDecls) tmp_vars;

    Object tmp_init = _init.visit(any,i);
    if (! (tmp_init instanceof fiacre.types.Init)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Init new_init = (fiacre.types.Init) tmp_init;

    Object tmp_trans = _trans.visit(any,i);
    if (! (tmp_trans instanceof fiacre.types.Transitions)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Transitions new_trans = (fiacre.types.Transitions) tmp_trans;

    return (T) fiacre.types.declaration.ProcessDecl.make( _name,  new_genericParams,  new_ports,  new_params,  new_states,  new_vars,  new_init,  new_trans);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_genericParams).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.StringList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.StringList new_genericParams = (fiacre.types.StringList) getEnvironment().getSubject();

    (_ports).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PortDecls)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PortDecls new_ports = (fiacre.types.PortDecls) getEnvironment().getSubject();

    (_params).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.ParamDecls)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.ParamDecls new_params = (fiacre.types.ParamDecls) getEnvironment().getSubject();

    (_states).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.StringList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.StringList new_states = (fiacre.types.StringList) getEnvironment().getSubject();

    (_vars).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.VarDecls)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.VarDecls new_vars = (fiacre.types.VarDecls) getEnvironment().getSubject();

    (_init).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Init)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Init new_init = (fiacre.types.Init) getEnvironment().getSubject();

    (_trans).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Transitions)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Transitions new_trans = (fiacre.types.Transitions) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.declaration.ProcessDecl.make( _name,  new_genericParams,  new_ports,  new_params,  new_states,  new_vars,  new_init,  new_trans));
    return tom.library.sl.Environment.SUCCESS;
  }
}
