
package fiacre.strategy.parcomposition;

public class Make_parCompoB implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private String _id;
  private tom.library.sl.Strategy _deb;
  private tom.library.sl.Strategy _fin;
  private tom.library.sl.Strategy _ps;
  private tom.library.sl.Strategy _pcl;


  public int getChildCount() {
    return 4;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _deb;
      case 1: return _fin;
      case 2: return _ps;
      case 3: return _pcl;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _deb = (tom.library.sl.Strategy) child; return this;
      case 1: _fin = (tom.library.sl.Strategy) child; return this;
      case 2: _ps = (tom.library.sl.Strategy) child; return this;
      case 3: _pcl = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_deb, _fin, _ps, _pcl};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._deb = (tom.library.sl.Strategy)children[0];
    this._fin = (tom.library.sl.Strategy)children[1];
    this._ps = (tom.library.sl.Strategy)children[2];
    this._pcl = (tom.library.sl.Strategy)children[3];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_parCompoB(String _id, tom.library.sl.Strategy _deb, tom.library.sl.Strategy _fin, tom.library.sl.Strategy _ps, tom.library.sl.Strategy _pcl) {
    this._id = _id;
    this._deb = _deb;
    this._fin = _fin;
    this._ps = _ps;
    this._pcl = _pcl;

  }

  /**
    * Builds a new parCompoB
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_deb = _deb.visit(any,i);
    if (! (tmp_deb instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_deb = (fiacre.types.Exp) tmp_deb;

    Object tmp_fin = _fin.visit(any,i);
    if (! (tmp_fin instanceof fiacre.types.Exp)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.Exp new_fin = (fiacre.types.Exp) tmp_fin;

    Object tmp_ps = _ps.visit(any,i);
    if (! (tmp_ps instanceof fiacre.types.PortSet)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PortSet new_ps = (fiacre.types.PortSet) tmp_ps;

    Object tmp_pcl = _pcl.visit(any,i);
    if (! (tmp_pcl instanceof fiacre.types.PortCompList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.PortCompList new_pcl = (fiacre.types.PortCompList) tmp_pcl;

    return (T) fiacre.types.parcomposition.parCompoB.make( _id,  new_deb,  new_fin,  new_ps,  new_pcl);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_deb).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_deb = (fiacre.types.Exp) getEnvironment().getSubject();

    (_fin).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.Exp)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.Exp new_fin = (fiacre.types.Exp) getEnvironment().getSubject();

    (_ps).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PortSet)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PortSet new_ps = (fiacre.types.PortSet) getEnvironment().getSubject();

    (_pcl).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.PortCompList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.PortCompList new_pcl = (fiacre.types.PortCompList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.parcomposition.parCompoB.make( _id,  new_deb,  new_fin,  new_ps,  new_pcl));
    return tom.library.sl.Environment.SUCCESS;
  }
}
