
package fiacre.strategy.instance;

public class Make_Inst implements tom.library.sl.Strategy {

  protected tom.library.sl.Environment environment;
  public void setEnvironment(tom.library.sl.Environment env) {
    this.environment = env;
  }

  public tom.library.sl.Environment getEnvironment() {
    if(environment!=null) {
      return environment;
    } else {
      throw new RuntimeException("environment not initialized");
    }
  }

  private String _s;
  private tom.library.sl.Strategy _genericParamsEffec;
  private tom.library.sl.Strategy _strl;
  private tom.library.sl.Strategy _el;


  public int getChildCount() {
    return 3;
  }
  public tom.library.sl.Visitable getChildAt(int index) {
    switch(index) {
      case 0: return _genericParamsEffec;
      case 1: return _strl;
      case 2: return _el;

      default: throw new IndexOutOfBoundsException();
    }
  }
  public tom.library.sl.Visitable setChildAt(int index, tom.library.sl.Visitable child) {
    switch(index) {
      case 0: _genericParamsEffec = (tom.library.sl.Strategy) child; return this;
      case 1: _strl = (tom.library.sl.Strategy) child; return this;
      case 2: _el = (tom.library.sl.Strategy) child; return this;

      default: throw new IndexOutOfBoundsException();
    }
  }

  public tom.library.sl.Visitable[] getChildren() {
    return new tom.library.sl.Visitable[]{_genericParamsEffec, _strl, _el};
  }

  public tom.library.sl.Visitable setChildren(tom.library.sl.Visitable[] children) {
        this._genericParamsEffec = (tom.library.sl.Strategy)children[0];
    this._strl = (tom.library.sl.Strategy)children[1];
    this._el = (tom.library.sl.Strategy)children[2];

    return this;
  }

  @SuppressWarnings("unchecked")
  public <T extends tom.library.sl.Visitable> T visit(tom.library.sl.Environment envt) throws tom.library.sl.VisitFailure {
    return (T) visit(envt,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visit(T any) throws tom.library.sl.VisitFailure{
    return visit(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public <T extends tom.library.sl.Visitable> T visitLight(T any) throws tom.library.sl.VisitFailure {
    return visitLight(any,tom.library.sl.VisitableIntrospector.getInstance());
  }

  public Object visit(tom.library.sl.Environment envt, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,envt);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return environment.getSubject();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T visit(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {
    tom.library.sl.AbstractStrategy.init(this,new tom.library.sl.Environment());
    getEnvironment().setRoot(any);
    int status = visit(i);
    if(status == tom.library.sl.Environment.SUCCESS) {
      return (T) getEnvironment().getRoot();
    } else {
      throw new tom.library.sl.VisitFailure();
    }
  }

  public Make_Inst(String _s, tom.library.sl.Strategy _genericParamsEffec, tom.library.sl.Strategy _strl, tom.library.sl.Strategy _el) {
    this._s = _s;
    this._genericParamsEffec = _genericParamsEffec;
    this._strl = _strl;
    this._el = _el;

  }

  /**
    * Builds a new Inst
    * If one of the sub-strategies application fails, throw a VisitFailure
    */

  @SuppressWarnings("unchecked")
  public <T> T visitLight(T any, tom.library.sl.Introspector i) throws tom.library.sl.VisitFailure {

    Object tmp_genericParamsEffec = _genericParamsEffec.visit(any,i);
    if (! (tmp_genericParamsEffec instanceof fiacre.types.GenInsts)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.GenInsts new_genericParamsEffec = (fiacre.types.GenInsts) tmp_genericParamsEffec;

    Object tmp_strl = _strl.visit(any,i);
    if (! (tmp_strl instanceof fiacre.types.ExpList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.ExpList new_strl = (fiacre.types.ExpList) tmp_strl;

    Object tmp_el = _el.visit(any,i);
    if (! (tmp_el instanceof fiacre.types.ExpList)) {
      throw new tom.library.sl.VisitFailure();
    }
    fiacre.types.ExpList new_el = (fiacre.types.ExpList) tmp_el;

    return (T) fiacre.types.instance.Inst.make( _s,  new_genericParamsEffec,  new_strl,  new_el);
  }

  public int visit(tom.library.sl.Introspector i) {

    (_genericParamsEffec).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.GenInsts)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.GenInsts new_genericParamsEffec = (fiacre.types.GenInsts) getEnvironment().getSubject();

    (_strl).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.ExpList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.ExpList new_strl = (fiacre.types.ExpList) getEnvironment().getSubject();

    (_el).visit(i);
    if (! (getEnvironment().getSubject() instanceof fiacre.types.ExpList)) {
      return tom.library.sl.Environment.FAILURE;
    }
    fiacre.types.ExpList new_el = (fiacre.types.ExpList) getEnvironment().getSubject();

    getEnvironment().setSubject(fiacre.types.instance.Inst.make( _s,  new_genericParamsEffec,  new_strl,  new_el));
    return tom.library.sl.Environment.SUCCESS;
  }
}
