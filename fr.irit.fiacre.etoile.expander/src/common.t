/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Fonctions et stratégies communes à plusieurs modules.
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.*;
import fiacre.*;
import fiacre.types.*;
import java.lang.Integer;
import java.util.*;

public class common {
	%typeterm Hashtable {
		implement 	{Hashtable}
		is_sort(t)	{t instanceof Hashtable}
	}
	%include { fiacre/Fiacre.tom }
	%include { sl.tom }

	/******************************************************/
	/* sauvegarde toutes les constantes                   */
	/******************************************************/
	%strategy CollectCte(tabCte:Hashtable) extends Identity()
	{
		visit Declaration
		{
			ConstantDecl(name, type, bodyE) ->
			{
				Integer taille = eval(`bodyE,tabCte); 
				if(taille != null)
				{
					tabCte.put(`name,taille);
				}
				return `ConstantDecl(name, type, bodyE);
			}
		}
	}

	/***********************************************************/
	/* evalue une exp                                           */
	/***********************************************************/
	public static Integer eval(Exp exp, Hashtable<String,Integer> tabCte)
	{
		%match (exp)
		{
			IdentExp(var, _, _) -> { return tabCte.get((String)`var);}
			IntExp(i, _) -> {return new Integer(`i);}
			ParenExp(e) -> { return eval(`e,tabCte);}
			Infix(op, _,e1,e2) -> 
			{
				Integer int1 = eval(`e1,tabCte);
				Integer int2 = eval(`e2,tabCte);
				if((int1 != null) && (int2 != null))
				{
					%match(op)
					{
						ADD() -> {
							return int1 + int2;
						}
						SUB() -> {
							return int1 - int2;
						}
						MOD() -> {
							return int1 % int2;
						}
						MUL() -> {
							return int1 * int2;
						}
					}
				}
				return null;
			}
			Unop(PLUS(), _, e) ->
			{
				return eval(`e,tabCte);
			}
			Unop(MINUS(), _, e) ->
			{
				return -eval(`e,tabCte);
			}
		}
		return null;
	}
}
