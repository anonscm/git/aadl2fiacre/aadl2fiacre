/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * ajout des composants intermediaires aux paths d'accès aux observations
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.util.* ;

public class path {
	%include {fiacre/Fiacre.tom}
	%include {sl.tom}

	public static Program_Fiacre path (Program_Fiacre prg) throws Exception {
	       String main = null;
	       %match(prg) {
	         Prog(_, Decl(Main(m))) -> { main = `m; }
	       }
	       if (main == null) return prg;
	       try {
			return `TopDown (updatePath (prg)).visit (prg) ;
		} 
		catch (VisitFailure e) {
			return prg ;
		}
	}

	private static Declaration getDecl(Program_Fiacre prg, String root) {
		%match(prg) {
		  Prog(DeclarationList(l1*,dec@ComponentDecl(r,_,_,_,_,_,_,_,_),l2*), _) && root.equals(r) == true -> { return `dec; }
		  Prog(DeclarationList(l1*,dec@ProcessDecl(r,_,_,_,_,_,_,_),l2*), _) && root.equals(r) == true -> { return `dec; }
		  
		}
		return null;
	}

	private static String getName(Declaration d) {
		%match (d) {
		       ComponentDecl(n,_,_,_,_,_,_,_,_) -> { return `n; }
		       ProcessDecl(n,_,_,_,_,_,_,_) -> { return `n; }
		}
		return "";
	}

	private static boolean isExt(String s) {
		return s.startsWith("gextended");
	}

	private static RPath update(Program_Fiacre prg, Declaration d, RPath l) {
//System.err.println("decl:"+getName(d)+", path: "+l);
	  %match(d) {
	        ComponentDecl(_,_,_,_,_,_,_,_,ParComp(parCompo(_,PCList(PortComp(_,InstanceComp(Inst(s,_,_,_))))))) && (isExt(s) == true) -> { 
		  RPath nl = update(prg, getDecl(prg,`s), l);
		  return `ConsIndexList(Cnum("1"), nl);
		}
	  }
	  %match (l) {
	    IndexList() -> { return l; }
	    IndexList(_*) -> {
	      %match (d) {
	        ComponentDecl(_,_,_,_,_,_,_,_,bdy) -> {
		  return update(prg, `bdy, l);
		}
		ProcessDecl(_,_,_,_,_,_,_,_) -> { return l; }
	      }
	    }
	  }
	  return l;
	}

	private static PortComposition ieme(PortCompList l, int i) {
	  %match(l) {
	    PCList() -> { System.err.println("empty list"); System.exit(-1); }
	    PCList(pc,l1*) -> {
	      if (i == 1) return `pc;
	      if (i == 0) { System.err.println("null index"); System.exit(-1); }
	      return ieme(`l1, i-1);
	    }
	  }
	  return null;
	}

	private static RPath update(Program_Fiacre prg, Composition cmp, RPath l) {
//System.err.println("path: "+l+", compo: "+Pretty.prettyComposition(cmp));
	  %match(cmp) {
	        ParComp(parCompo(_,PCList(PortComp(_,InstanceComp(Inst(s,_,_,_)))))) && (isExt(s) == true) -> { 
		  RPath nl = update(prg, getDecl(prg,`s), l);
		  return `ConsIndexList(Cnum("1"), nl);
		}
		InstanceComp(Inst(s,_,_,_)) -> { 
		  return update(prg, getDecl(prg,`s),l);
		}		
	  }
	  %match(l) {
	    IndexList() -> { return l; }
	    IndexList(i@Cnum(ind),l1*) -> {
	      %match(cmp) {
	        ParComp(parCompo(_,PCList(il*))) -> { 
		  int num = Integer.parseInt(`ind);
		  PortComposition pc = ieme(`il, num);
		  %match (pc) {
		    PortComp(_, cmp1) -> {
		      RPath nl1 = update(prg, `cmp1, `l1);
		      return `ConsIndexList(i, nl1);
		    }
		  }
		}
		ParComp(parCompoB(_,_,_,_,PCList(il*))) -> {
		  System.err.println("parCompoB");
		  System.exit(-1);
		}
	      }
	    }
	  }
	  return l;
	}
	
	%strategy updatePath (Program_Fiacre prg) extends Identity () {
		visit Observable {
			ObsItem(OPathFrom (root, p), i) -> {
				Declaration r = getDecl(prg, "u"+`root);
				if (r == null) {
				   System.err.println("Unknown root in path:"+`root);
				   System.exit(-1);
				 }
				return `ObsItem(OPathFrom (root, update(prg,r,p)), i);
			}
		}
	}
}
