/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Autorise les expressions de la forme [all|exists] i in [N] . exp et [all|exists] i in (j .. k) . exp
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.util.* ;

public class allex {
	%typeterm Hashtable{
		implement 	{Hashtable}
		is_sort(t)	{ t instanceof Hashtable}
	}
	%include {fiacre/Fiacre.tom}
	%include {sl.tom}
	%op Strategy CollectCte (tabCte:Hashtable) {
		  make(tabCte) { new common.CollectCte (tabCte) }
	}

	static interface MkInfix<T> {
		  public T call(T x, T y);
	}

	static class AndExp implements MkInfix<Exp> {
	       public Exp call(Exp x, Exp y) { return `Infix (AND(),BotType (),x,y);}
	}
	static class OrExp implements MkInfix<Exp> {
	       public Exp call(Exp x, Exp y) { return `Infix (OR(),BotType (),x,y);}
	}
	static class AndProp implements MkInfix<Property> {
	       public Property call(Property x, Property y) { return `PropBin (x,"and",y);}
	}
	static class OrProp implements MkInfix<Property> {
	       public Property call(Property x, Property y) { return `PropBin (x,"or",y);}
	}
	static class AndLtl implements MkInfix<LTL> {
	       public LTL call(LTL x, LTL y) { return `LtlBinary (x,"and",y);}
	}
	static class OrLtl implements MkInfix<LTL> {
	       public LTL call(LTL x, LTL y) { return `LtlBinary (x,"or",y);}
	}

	private static AndExp andExp = new AndExp();
	private static OrExp orExp = new OrExp();
	private static AndProp andProp = new AndProp();
	private static OrProp orProp = new OrProp();
	private static AndLtl andLtl = new AndLtl();
	private static OrLtl orLtl = new OrLtl();

	public static Program_Fiacre allex (Program_Fiacre prg) throws Exception {
		try {
			Hashtable<String, Integer> tabCte = new Hashtable<String, Integer> () ;
			`TopDown (CollectCte (tabCte)).visit (prg);
			return `TopDown (elimAllExists (tabCte)).visit (prg) ;
		} 
		catch (VisitFailure e) {
			return prg ;
		}
	}

	%strategy elimAllExists (tabCte: Hashtable) extends Identity () {
		visit Exp {
			AllExp (id, begin, end, e) -> {
				return `replace (id, begin, end, e, andExp, tabCte) ;
			}
			ExistsExp (id, begin, end, e) -> {
				return `replace (id, begin, end, e, orExp, tabCte) ;
			}
		}
		visit Property {
			PropAll (id, begin, end, e) -> {
				return `replace (id, begin, end, e, andProp, tabCte) ;
			}
			PropEx (id, begin, end, e) -> {
				return `replace (id, begin, end, e, orProp, tabCte) ;
			}
		}
		visit LTL {
			LtlAll (id, begin, end, e) -> {
				return `replace (id, begin, end, e, andLtl, tabCte) ;
			}
			LtlEx (id, begin, end, e) -> {
				return `replace (id, begin, end, e, orLtl, tabCte) ;
			}
		}
	}

	private static <T extends tom.library.sl.Visitable> T replace (String id, Exp begin, Exp end, T e, MkInfix<T> op, Hashtable<String, Integer> tabCte) throws VisitFailure {
		Integer be = common.eval (begin, tabCte) ;
		Integer en = common.eval (end, tabCte) ;
		if (be == null || en == null) {
			System.err.println ("Impossible d'évaluer les expressions " + begin + " et " + end) ;
			System.exit (1) ;
		}
		if (be < 0 || en < be) {
			System.err.println ("Valeurs " + be + " et/ou " + en + " non valide(s).") ;
			System.exit (1) ;
		}
		T ne = `TopDown (replaceVar (id, be)).visit (e) ;
		for (int i = be + 1 ; i <= en ; ++i)
			ne = op.call(ne, `TopDown (replaceVar (id, i)).visit (e));
		return ne ;
	}

	%strategy replaceVar (var: String, i: int) extends Identity () {
		visit Exp {
			IdentExp (v, kind, type) -> {
				if (`v.equals (var))
					return `IntExp ("" + i, BotType ()) ;
			}
		}
		visit CIndex {
		        Cident (v) -> {
			       if (`v.equals(var)) return `Cnum("" + i);
			}
		}
	}
}
