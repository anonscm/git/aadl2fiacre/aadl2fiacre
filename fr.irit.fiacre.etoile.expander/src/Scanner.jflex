/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.irit.fiacre.etoile.expander ;

import java_cup.runtime.*;
import java.io.FileReader;
import java.io.File;
import java.io.FileNotFoundException;


%%
%class Scanner
%cup
//la classe sym est g�n�r�e par cup
%extends sym
%unicode
%line
%char
%column

%{
   StringBuffer string = new StringBuffer();
   static String yyfile = "";
   private Symbol symbol(int type) {
     return new Symbol(type, yyline, yycolumn);
   }
   private Symbol symbol(int type, Object value) {
     return new Symbol(type, yyline, yycolumn, value);
   }
%}

LineTerminator = \r|\n|\r\n
WhiteSpace = {LineTerminator} | [ \t\f]
InputCharacter = [^\r\n]

/* comments */
Comment = {TraditionalComment} | {EndOfLineComment} | {DocumentationComment}

TraditionalComment   = "/*" [^*] ~"*/" | "/*" "*"+ "/"
EndOfLineComment     = "//" {InputCharacter}* {LineTerminator}
DocumentationComment = "/**" {CommentContent} "*"+ "/"
CommentContent       = ( [^*] | \*+ [^/*] )*

underline = "_"
dot = "."
comma = ","
alpha = [A-Za-z]
digit=[0-9]
ident = {alpha}({alpha}|{digit}|{underline})* 
real = {digit}+({dot}){digit}+
%state LINENUMBER,FILENAME,ENDLINE
%%

<YYINITIAL> {WhiteSpace} { /* ignore */ }
<YYINITIAL> {Comment}   { /* ignore */ }
<YYINITIAL> ^"#"	{ yybegin(LINENUMBER); }
<LINENUMBER>	    " "*{digit}+ { yyline = Integer.parseInt(yytext().trim(),10);yybegin(FILENAME); }
<FILENAME>  " "*"\""[^\"]*"\"" {yyfile = yytext().trim(); yybegin(ENDLINE);}
<ENDLINE>   {InputCharacter}* {LineTerminator}		      {yybegin(YYINITIAL);}
<YYINITIAL> "init"	{ return symbol(sym.INIT); }
<YYINITIAL> "union"	{ return symbol(sym.UNION); }
<YYINITIAL> "record"	{ return symbol(sym.RECORD); }
<YYINITIAL> "queue"	{ return symbol(sym.QUEUE); }
<YYINITIAL> "const"	{ return symbol(sym.CONST); }
<YYINITIAL> "constr0"	{ return symbol(sym.CONSTR0); }
<YYINITIAL> "constr1"	{ return symbol(sym.CONSTR1); }
<YYINITIAL> "process"	{ return symbol(sym.PROCESS); }
<YYINITIAL> "component"	{ return symbol(sym.COMPONENT); }
<YYINITIAL> "property"	{ return symbol(sym.PROPERTY); }
<YYINITIAL> "assert"	{ return symbol(sym.ASSERT); }
<YYINITIAL> "deadlockfree"	{ return symbol(sym.DEADLOCKFREE); }
<YYINITIAL> "infinitelyoften"	{ return symbol(sym.INFINITELYOFTEN); }
<YYINITIAL> "mortal"	{ return symbol(sym.MORTAL); }
<YYINITIAL> "present"	{ return symbol(sym.PRESENT); }
<YYINITIAL> "after"	{ return symbol(sym.AFTER); }
<YYINITIAL> "before"	{ return symbol(sym.BEFORE); }
<YYINITIAL> "within"	{ return symbol(sym.WITHIN); }
<YYINITIAL> "lasting"	{ return symbol(sym.LASTING); }
<YYINITIAL> "between"	{ return symbol(sym.BETWEEN); }
<YYINITIAL> "until"	{ return symbol(sym.UNTIL); }
<YYINITIAL> "absent"	{ return symbol(sym.ABSENT); }
<YYINITIAL> "leadsto"	{ return symbol(sym.LEADSTO); }
<YYINITIAL> "precedes"	{ return symbol(sym.PRECEDES); }
<YYINITIAL> "always"	{ return symbol(sym.ALWAYS); }
<YYINITIAL> "release"	{ return symbol(sym.RELEASE); }
<YYINITIAL> "enter"	{ return symbol(sym.ENTER); }
<YYINITIAL> "leave"	{ return symbol(sym.LEAVE); }
<YYINITIAL> "state"	{ return symbol(sym.STATE); }
<YYINITIAL> "value"	{ return symbol(sym.VALUE); }
<YYINITIAL> "field"	{ return symbol(sym.FIELD); }
<YYINITIAL> "tag"	{ return symbol(sym.TAG); }
<YYINITIAL> "event"	{ return symbol(sym.EVENT); }
<YYINITIAL> "ltl"	{ return symbol(sym.LTL); }
<YYINITIAL> "var"	{ return symbol(sym.VAR); }
<YYINITIAL> "from"	{ return symbol(sym.FROM); }
<YYINITIAL> "any"	{ return symbol(sym.ANY); }
<YYINITIAL> "where"	{ return symbol(sym.WHERE); }
<YYINITIAL> "for"	{ return symbol(sym.FOR); }
<YYINITIAL> "if"	{ return symbol(sym.IF); }
<YYINITIAL> "of"	{ return symbol(sym.OF); }
<YYINITIAL> "then"	{ return symbol(sym.THEN); }
<YYINITIAL> "port"	{ return symbol(sym.PORT); }
<YYINITIAL> "sync"	{ return symbol(sym.SYNC); }
<YYINITIAL> "else"	{ return symbol(sym.ELSE); }
<YYINITIAL> "elsif"	{ return symbol(sym.ELSIF); }
<YYINITIAL> "select"	{ return symbol(sym.SELECT); }
<YYINITIAL> "unless"	{ return symbol(sym.UNLESS); }
<YYINITIAL> "case"      { return symbol(sym.CASE); }
<YYINITIAL> "end"	{ return symbol(sym.END); }
<YYINITIAL> "to"	{ return symbol(sym.TO); }
<YYINITIAL> "array"	{ return symbol(sym.ARRAY); }
<YYINITIAL> "bool"	{ return symbol(sym.T_BOOL); }
<YYINITIAL> "nat"	{ return symbol(sym.T_NAT); }
<YYINITIAL> "int"	{ return symbol(sym.T_INT); }
<YYINITIAL> "true"	{ return symbol(sym.TRUE); }
<YYINITIAL> "false"	{ return symbol(sym.FALSE); }
<YYINITIAL> "full"	{ return symbol(sym.FULL); }
<YYINITIAL> "first"	{ return symbol(sym.FIRST); }
<YYINITIAL> "length"	{ return symbol(sym.LENGTH); }
<YYINITIAL> "empty"	{ return symbol(sym.EMPTY); }
<YYINITIAL> "dequeue"	{ return symbol(sym.DEQUEUE); }
<YYINITIAL> "enqueue"	{ return symbol(sym.ENQUEUE); }
<YYINITIAL> "append"	{ return symbol(sym.APPEND); }
<YYINITIAL> "not"	{ return symbol(sym.NOT); }
<YYINITIAL> "in"	{ return symbol(sym.IN); }
<YYINITIAL> "out"	{ return symbol(sym.OUT); }
<YYINITIAL> "none"	{ return symbol(sym.NONE); }
<YYINITIAL> "states"	{ return symbol(sym.STATES); }
<YYINITIAL> "priority"	{ return symbol(sym.PRIORITY); }
<YYINITIAL> "read"	{ return symbol(sym.READ); }
<YYINITIAL> "write"	{ return symbol(sym.WRITE); }
<YYINITIAL> "null"  	{ return symbol(sym.NULL); }
<YYINITIAL> "while"	{ return symbol(sym.WHILE); }
<YYINITIAL> "foreach"	{ return symbol(sym.FOREACH); }
<YYINITIAL> "do"	{ return symbol(sym.DO); }
<YYINITIAL> "?"		{ return symbol(sym.RCVE); }
<YYINITIAL> "and"	{ return symbol(sym.AND); }
<YYINITIAL> "or"	{ return symbol(sym.OR); }
<YYINITIAL> "=>"	{ return symbol(sym.IMP); }
<YYINITIAL> "channel"   { return symbol(sym.CHANNEL); }
<YYINITIAL> "type"	{ return symbol(sym.TYPE); }
<YYINITIAL> "is"	{ return symbol(sym.IS); }
<YYINITIAL> "par"       { return symbol(sym.PAR); }
<YYINITIAL> "on"        { return symbol(sym.ON); }
<YYINITIAL> "wait"      { return symbol(sym.WAIT); }
<YYINITIAL> "loop"      { return symbol(sym.LOOP); }
<YYINITIAL> "all"       { return symbol(sym.ALL); }
<YYINITIAL> "exists"    { return symbol(sym.EXISTS); }
<YYINITIAL> "-true"     { return symbol(sym.IFTRUE); }
<YYINITIAL> "-false"    { return symbol(sym.IFFALSE); }
<YYINITIAL> "..."       { return symbol(sym.INFTY); }
<YYINITIAL> "&"         { return symbol(sym.AMP); }
<YYINITIAL> "|"		{ return symbol(sym.MID); }
<YYINITIAL> "||"	{ return symbol(sym.DBAR); }
<YYINITIAL> "+"		{ return symbol(sym.ADD); }
<YYINITIAL> "%"		{ return symbol(sym.MOD); }
<YYINITIAL> "-"		{ return symbol(sym.SUB); }
<YYINITIAL> "*"		{ return symbol(sym.MUL); }
<YYINITIAL> "@"		{ return symbol(sym.AT); }
<YYINITIAL> "#"		{ return symbol(sym.SHARP); }
<YYINITIAL> "^" 	{ return symbol(sym.CIRCUM); }
<YYINITIAL> "/"		{ return symbol(sym.DIV); }
<YYINITIAL> "$"		{ return symbol(sym.COERCE); }
<YYINITIAL> "<<"	{ return symbol(sym.BEGINPATTERN); }
<YYINITIAL> "<"		{ return symbol(sym.LT); }
<YYINITIAL> "<="	{ return symbol(sym.LE); }
<YYINITIAL> ">="	{ return symbol(sym.GE); }
<YYINITIAL> ">>"	{ return symbol(sym.ENDPATTERN); }
<YYINITIAL> ">"	        { return symbol(sym.GT); }
<YYINITIAL> "="		{ return symbol(sym.EQ); }
<YYINITIAL> "<>"	{ return symbol(sym.NE); }
<YYINITIAL> "!"		{ return symbol(sym.SEND); }
<YYINITIAL> ".."	{ return symbol(sym.TWODOT); }
<YYINITIAL> "."		{ return symbol(sym.DOT); }
<YYINITIAL> ":=" 	{ return symbol(sym.ASSIGN); }
<YYINITIAL> ":" 	{ return symbol(sym.COLON); }
<YYINITIAL> ","		{ return symbol(sym.COMMA); }
<YYINITIAL> ";"		{ return symbol(sym.SEMICOLON); }
<YYINITIAL> "[]"	{ return symbol(sym.ALT); }
<YYINITIAL> "()"	{ return symbol(sym.NEXT); }
<YYINITIAL> "["		{ return symbol(sym.LBRACK); }
<YYINITIAL> "]"		{ return symbol(sym.RBRACK); }
<YYINITIAL> "("		{ return symbol(sym.LPAREN); }
<YYINITIAL> ")"		{ return symbol(sym.RPAREN); }
<YYINITIAL> "{"		{ return symbol(sym.LBRACE); }
<YYINITIAL> "}"		{ return symbol(sym.RBRACE); }
<YYINITIAL> "{|"	{ return symbol(sym.LQBRACE); }
<YYINITIAL> "|}"	{ return symbol(sym.RQBRACE); }
<YYINITIAL> "->"        { return symbol(sym.ARROW); }
<YYINITIAL> "<|"	{ return symbol(sym.LTMID); }
<YYINITIAL> "|>"	{ return symbol(sym.MIDGT); }
<YYINITIAL> {ident}     { return symbol(sym.IDENT,  yytext()); }
<YYINITIAL> {digit}+    { return symbol(sym.NAT, yytext()); }
<YYINITIAL> {real}      { return symbol(sym.DEC, yytext()); }
<YYINITIAL> "\""[^\"]*"\"" { return symbol(sym.STRING, yytext()); }
<YYINITIAL> eof		{ }
/* error fallback */
<YYINITIAL> .|\n        { throw new Error("Illegal character <"+
                                        yytext()+">"); }
