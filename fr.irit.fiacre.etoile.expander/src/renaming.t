/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Renommage des identifiants : tous les identifiants sont préfixés par 'u'.
 * Lorsqu'une transformation génère un identifiant, elle le prefixe par 'g'.
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.util.* ;

public class renaming {
	%include {fiacre/Fiacre.tom}
	%include {sl.tom}

	public static Program_Fiacre renaming (Program_Fiacre prg) throws Exception {
		try {
			return `TopDown (rename ()).visit (prg) ;
		} 
		catch (VisitFailure e) {
			return prg ;
		}
	}

	%strategy rename () extends Identity () {
		visit Declaration {
			TypeDecl (name, bodyT) -> {
				return `TypeDecl ("u" + name, bodyT) ;
			}
			ChannelDecl (name, bodyC) -> {
				return `ChannelDecl ("u" + name, bodyC) ;
			}
			ConstantDecl (name, type, bodyE) -> {
				return `ConstantDecl ("u" + name, type, bodyE) ;
			}
			ProcessDecl (name, genericParams, ports, params, states, vars, init, trans) -> {
				return `ProcessDecl ("u" + name, genericParams, ports, params, states, vars, init, trans) ;
			}
			ExtendedProcessDecl (name, genericParams, ports, params, lpd, prios, states, vars, init, trans) -> {
				return `ExtendedProcessDecl ("u" + name, genericParams, ports, params, lpd, prios, states, vars, init, trans) ;
			}
			ComponentDecl (name, genericParams, ports, params, vars, lpd, prios, init, bodyCmp) -> {
				return `ComponentDecl ("u" + name, genericParams, ports, params, vars, lpd, prios, init, bodyCmp) ;
			}
			Main (main) -> {
				return `Main ("u" + main) ;
			}
		}
		visit Arg {
			ValArg (arg) -> {
				return `ValArg ("u" + arg) ;
			}
			RefArg (arg) -> {
				return `RefArg ("u" + arg) ;
			}
			ArrayRefArg (arg, size) -> {
				return `ArrayRefArg ("u" + arg, size) ;
			}
		}
		visit Type {
			NamedType (name) -> {
				return `NamedType ("u" + name) ;
			}
		}
		visit FieldsDecl {
			field (names, type) -> {
				return `field (names, type) ;
			}
		}
		visit StringList {
			stringList (head, tail*) -> {
				return `stringList ("u" + head, tail*) ;
			}
		}
		visit Channel {
			NamedChannel (abbrev) -> {
				return `NamedChannel ("u" + abbrev) ;
			}
		}
		visit Exp {
			RefExp (ref, type) -> {
				return `RefExp ("u" + ref, type) ;
			}
			ConstrExp (consrt, e, type) -> {
				return `ConstrExp ("u" + consrt, e, type) ;
			}
			IdentExp (var, kind, type) -> {
				return `IdentExp ("u" + var, kind, type) ;
			}
			RecordAccessExp (record, id, type) -> {
				return `RecordAccessExp (record, "u" + id, type) ;
			}
			InitListExp (var, n, e) -> {
				return `InitListExp ("u" + var, n, e) ;
			}
			AllExp (id, begin, end, e) -> {
				return `AllExp ("u" + id, begin, end, e) ;
			}
			ExistsExp (id, begin, end, e) -> {
				return `ExistsExp ("u" + id, begin, end, e) ;
			}
		}
		visit Affectation {
			Affect (name, val) -> {
				return `Affect ("u" + name, val) ;
			}
		}
		visit Statement {
			StSelectIndexe (var, deb, fin, stl) -> {
				return `StSelectIndexe ("u" + var, deb, fin, stl) ;
			}
			StTo (state) -> {
				return `StTo ("u" + state) ;
			}
			StForEach (s, type, st) -> {
				return `StForEach ("u" + s, type, st) ;
			}
		}
		visit Bound {
			Open (r) -> {
				return `Open (addU (r)) ;
			}
			Closed (r) -> {
				return `Closed (addU (r)) ;
			}
		}
		visit ParComposition {
			parCompoB (id, deb, fin, ps, pcl) -> {
				return  `parCompoB ("u" + id, deb, fin, ps, pcl) ;
			}
		}
		visit Instance {
			Inst (s, genericParamsEffec, strl, el) -> {
				return `Inst ("u" + s, genericParamsEffec, strl, el) ;
			}
		}
		visit Transition {
			Trans (l, s, st) -> {
				return `Trans (l, "u" + s, st) ;
			}
		}
		visit GenInst {
			ConstInst (cst) -> {
				return `ConstInst (addU (cst)) ;
			}
			Constr0Inst (cstr0) -> {
				return `Constr0Inst ("u" + cstr0) ;
			}
			Constr1Inst (cstr1) -> {
				return `Constr1Inst ("u" + cstr1) ;
			}
		}
	}

	private static String addU (String s) {
		try {
			Double.parseDouble (s) ;
			return s ;
		} catch (NumberFormatException e) {
			return "u" + s ;
		}
	}
}
