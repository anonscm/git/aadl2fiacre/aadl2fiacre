/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.*;
import fiacre.*;
import fiacre.types.*;
import java.lang.Integer;
import java.util.Hashtable;

import java_cup.runtime.*;
import fr.irit.fiacre.etoile.expander.Scanner ;

public class selecti {

%typeterm Hashtable{
    implement 	{Hashtable}
    is_sort(t)	{ t instanceof Hashtable}
  }

%include { fiacre/Fiacre.tom }
%include { sl.tom }
	%op Strategy CollectCte(tabCte:Hashtable) {
		  make(tabCte) { new common.CollectCte(tabCte) }
	}

        public static void report_error(String message, Object info) {
	    System.out.print(Scanner.yyfile+":");
            System.out.print(message);
            if ( !(info instanceof Symbol) ) return;
            Symbol symbol = (Symbol) info;
            if ( symbol.left < 0 || symbol.right < 0 ) return;
            System.out.println(" at line "+(symbol.left-1)+", column "+(symbol.right+1));
	    System.exit(1);
        }

	public static Program_Fiacre selecti (Program_Fiacre prg) throws Exception
	{
		try 
		{
			Hashtable<String,Integer> tabCte = new Hashtable<String,Integer>();	/* nom cte declare et initialise */
			
			/* sauvegarde toutes les ctes dans un hastable */
			Program_Fiacre pf = `TopDown(CollectCte(tabCte)).visit(prg);
			/* transforme le select indexe par un select simple */
			pf = `TopDown(elimSelectIndexe(tabCte)).visit(pf);

			return pf;
		} 
		catch(VisitFailure e) {}
	   	return prg;
	}

	/***************************************************/
	/* transfome un StSelectIndexe en StSelect	   */
	/***************************************************/
	%strategy elimSelectIndexe(tabCte:Hashtable) extends Identity()
	{
		visit Statement 
		{
			StSelectIndexe(var,deb,fin,lst) -> 
				{
					try
					{
						/* recuperation de la borne inferieur */
						Integer ideb = common.eval(`deb,tabCte);
						/* recuperation de la borne superieur */
						Integer ifin = common.eval(`fin,tabCte);
						if (ideb != null && ifin != null) {
							/*System.out.println("ideb: "+ideb+" ifin: "+ifin); */
							/* m-a-j de la variable de travail contenue dans les statements places dans le select indexe */
							PStatementList lstUpdate = dupPStatement(`lst, tabCte, `var, ideb, ifin);
							return `StSelect(TopDown(elimSelectIndexe(tabCte)).visit(lstUpdate));
						}
						/* impossible de d'evaluer la borne inferieur et/ou la borne superieur */
						System.out.println("Syntax error:SelectIndexe (invalid bounds for "+`var+"): "+Pretty.prettyExp(`deb)+","+Pretty.prettyExp(`fin));
						System.exit(-1);
/*						report_error("Syntax error (invalid bounds)", `var); */
					}
					catch (Exception ex)
					{
						System.out.println("Syntax error:SelectIndexe: "+Pretty.prettyStatement(`StSelectIndexe(var,deb,fin,lst)));
						System.exit(-1);
					}
					
				}
		}
   	}

	/***************************************************************************/
	/* duplique (deb-fin) fois une StatementList et modifie la var de travail  */
	/***************************************************************************/
	private static PStatementList dupPStatement(PStatementList s, Hashtable<String,Integer> tabCte, String var, int deb, int fin) throws Exception
	{
		%match(s) {
		  PStmList() -> { return s; }
		  PStmList(sl) -> {
		     return `PStmList(dupStatement(sl,tabCte,var,deb,fin));
		  }
		  _ -> {throw new Exception("Indexed select with unless");}
		}
		return s;
	}

	private static StatementList dupStatement(StatementList s, Hashtable<String,Integer> tabCte, String var, int deb, int fin) throws Exception
	{
		StatementList sUpdate = `TopDown(remplaceVarI(tabCte,(String)var,deb)).visit(s);
		if(fin == deb)
		{
			return sUpdate;
		}
		return appendStatementList(sUpdate, dupStatement(s,tabCte,var,deb+1,fin));
	}

	/******************************************************/
	/* remplace la var. de travail par sa valeur          */
	/******************************************************/
	%strategy remplaceVarI(tabCte:Hashtable, i:String, val:int) extends Identity()
	{
		visit Exp
		{
			/* cas d'une operation arithmetique  */
			Infix(iop, type, e1, e2) ->
			{
				/* evaluation */
				Integer ret = common.eval(`Infix(iop, type, e1, e2), tabCte);
				if (ret != null)
				{
					return `IntExp(String.valueOf(ret.intValue()), type);
				}
				return `Infix(iop, type, e1, e2);
			}
			
			/* cas de la variable de travail */
			IdentExp(var, kind, type) && i == var ->
			{
				return `IntExp(String.valueOf(val), type);
			}
		}
	}

	/******************************************************/
	/* concatene 2 StatementList                          */
	/******************************************************/
	private static StatementList appendStatementList(StatementList sl1, StatementList sl2)
	{
		%match(sl1,sl2) 
		{
			StmList(s1*),StmList(s2*) -> { return `StmList(s1,s2); }
		}
		return null;
	}
}

