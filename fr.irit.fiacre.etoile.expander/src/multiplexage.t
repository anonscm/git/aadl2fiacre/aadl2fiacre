/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Permet le multiplexage de ports, à savoir de passer plusieurs ports sous la forme p1 [] ... [] pn
 * à la place d'un seul port dans l'instanciation d'un processus/composant.
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.util.* ;

public class multiplexage {
	%include {fiacre/Fiacre.tom}
	%include {sl.tom}

	public static Program_Fiacre multiplexage (Program_Fiacre prg) throws Exception {
		try {
			program = prg ;
			prg = `TopDown (multiplexagePorts ()).visit (prg) ;
			%match (prg, newprocesses) {
				Prog (DeclarationList (l1*), main), DeclarationList (l2*) -> {
					return `Prog (DeclarationList (l2*, l1*), main) ;
				}
			}
			return null ;
		} 
		catch (VisitFailure e) {
			return prg ;
		}
	}

	%strategy multiplexagePorts () extends Identity () {
		visit Instance {
			inst@Inst (name, gen, params, el) -> {
				List<Multiplexed> multPorts = extractMultiplexedPorts (`params, 0) ;
				if (multPorts.size () > 0) {
					String newname = createProcess (`name, multPorts) ;
					ExpList newparams = generatePorts (`params) ;
					return `Inst (newname, gen, newparams, el) ;
				}
				return `inst ;
			}
		}
	}

	private static List<Multiplexed> extractMultiplexedPorts (ExpList el, int index) {
		%match (el) {
			ConslistExp (MultExp (el2), tl) -> {
				List<Multiplexed> res = extractMultiplexedPorts (`tl, index + 1) ;
				res.add (0, new Multiplexed (index, `el2)) ;
				return res ;
			}
			ConslistExp (_, tl) -> {
				return extractMultiplexedPorts (`tl, index + 1) ;
			}
		}
		return new LinkedList<Multiplexed> () ;
	}

	private static String createProcess (String name, List<Multiplexed> mparams) {
		%match (program) {
			Prog (decl, main) -> {
				Declaration process = findProcess (name, mparams, `decl) ;
				newprocesses = `ConsDeclarationList (process, newprocesses) ;
			}
		}
		return "gmultiplexage_" + name + "_" + processNb++ ;
	}

	private static Declaration findProcess (String name, List<Multiplexed> mparams, Declarations decl) {
		%match (decl) {
			ConsDeclarationList (ExtendedProcessDecl (nam, genericPorts, ports, params, lpd, prios, states, vars, init, trans), tl) -> {
				if (name.equals (`nam)) {
					index = 0 ;
					mp_ind = 0;
					PortDecls nports = generateFormalPorts (`ports, mparams) ;
					Transitions ntrans = updateSynchros (`trans, mparams) ;
					return `ExtendedProcessDecl ("gmultiplexage_" + name + "_" + processNb, genericPorts, nports, params, lpd, prios, states, vars, init, ntrans) ;
				}
				return findProcess (name, mparams, `tl) ;
			}
			ConsDeclarationList (_, tl) -> {
				return findProcess (name, mparams, `tl) ;
			}
		}
		System.err.println ("Failed to find process named " + name) ;
		System.exit (1) ;
		return null ;
	}

	private static PortDecls generateFormalPorts (PortDecls params, List<Multiplexed> mparams) {
		%match (params) {
			ConsListPortDecl (PortDec (ports, type, attr, chan), tl) -> {
				StringList nports = generateFormalPorts (`ports, mparams) ;
				return `ConsListPortDecl (PortDec (nports, type, attr, chan), generateFormalPorts (tl, mparams)) ;
			}
		}
		return params ;
	}

	private static StringList generateFormalPorts (StringList ports, List<Multiplexed> mparams) {
		if (mp_ind >= mparams.size()) return ports;
		%match (ports) {
			ConsstringList (port, tl) -> {
				Multiplexed m = mparams.get (mp_ind) ;
				if (m.index == index) {
					m.formal = `port ;
					m.formallist = generateFormalPorts (m.exp, `port, 0) ;
					index++;
					mp_ind++;
					StringList res = concStringList (m.formallist, generateFormalPorts (`tl, mparams)) ;
					return res ;
				}
				index++;
				return `ConsstringList (port, generateFormalPorts (tl, mparams)) ;
			}
		}
		return ports ;
	}

	private static StringList generateFormalPorts (ExpList el, String port, int idx) {
		%match (el) {
			ConslistExp (_, tl) -> {
				return `ConsstringList ("gmultiplexage_" + port + "_" + idx, generateFormalPorts (tl, port, idx + 1)) ;
			}
		}
		return `stringList () ;
	}

	private static ExpList generatePorts (ExpList params) {
		%match (params) {
			ConslistExp (MultExp (el2), tl) -> {
				ExpList res = generatePorts (`tl) ;
				%match (el2, res) {
					listExp (l1*), listExp (l2*) -> {
						return `listExp (l1*, l2*) ;
					}
				}
				return null ;
			}
			ConslistExp (hd, tl) -> {
				return `ConslistExp (hd, generatePorts (tl)) ;
			}
		}
		return params ;
	}

	private static Transitions updateSynchros (Transitions trans, List<Multiplexed> mparams) {
		if (mparams.isEmpty()) return trans;
		try {
			for (Multiplexed m : mparams) {
			    if (m.formal == null) continue; /* ????? */
				trans = `BottomUp (synchroToSelect (m.formal, m.formallist)).visit (trans) ;
			}
		} 
		catch (VisitFailure e) {
			return trans ;
		}
		return trans ;
	}

	%strategy synchroToSelect (var:String, lst:StringList) extends Identity () {
		visit Statement {
			sig@StSignal (IdentExp (var2, kind, type), chan) -> {
				if (var.equals (`var2))
					return `StSelect (PStmList (stringListToSignal (lst, kind, type, chan))) ;
				return `sig ;
			}
			inp@StInput (IdentExp (var2, kind, type), chan, p, oe) -> {
				if (var.equals (`var2))
					return `StSelect (PStmList (stringListToInput (lst, kind, type, chan, p, oe))) ;
				return `inp ;
			}
		}
	}

	private static StatementList stringListToSignal (StringList lst, Kind kind, Type type, Channel chan) {
		%match (lst) {
			ConsstringList (hd, tl) -> {
				return `ConsStmList (StSignal (IdentExp (hd, kind, type), chan), stringListToSignal (tl, kind, type, chan)) ;
			}
		}
		return `StmList () ;
	}

	private static StatementList stringListToInput (StringList lst, Kind kind, Type type, Channel chan, ExpList p, Optional_Exp oe) {
		%match (lst) {
			ConsstringList (hd, tl) -> {
				return `ConsStmList (StInput (IdentExp (hd, kind, type), chan, p, oe), stringListToInput (tl, kind, type, chan, p, oe)) ;
			}
		}
		return `StmList () ;
	}

	private static StringList concStringList (StringList l1, StringList l2) {
		%match (l1, l2) {
			stringList (ll1*), stringList (ll2*) -> {
				return `stringList (ll1*, ll2*) ;
			}
		}
		return null ;
	}

	private static class Multiplexed {
		public int index ;
		public ExpList exp ;
		public String formal ;
		public StringList formallist ;
		public Multiplexed (int idx, ExpList el) {
			index = idx ;
			exp = el ;
		}
	}

	private static int processNb = 0 ;

	private static int index ;

	private static int mp_ind = 0;

	private static Declarations newprocesses = `DeclarationList () ;

	private static Program_Fiacre program ;
}
