/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Remplace les wait par des synchronisations sur des ports locaux,
 * sauf dans les clauses unless des select.
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.util.* ;

public class elimWait {
	%include {fiacre/Fiacre.tom}
	%include {sl.tom}

	public static Program_Fiacre elimWait (Program_Fiacre prg) throws Exception {
		try {
			return `TopDown (elimWaitStrategy ()).visit (prg) ;
		} 
		catch (VisitFailure e) {
			return prg ;
		}
	}

	%strategy elimWaitStrategy () extends Identity () {
		visit Declaration {
			ExtendedProcessDecl (nam, genericParams, ports, params, lpd, prios, states, vars, init, trans) -> {
				ports = new LinkedList<Wait> () ;
				Transitions ntrans = `TopDown (elimWaitStmt ()).visit (`trans) ;
				ntrans = `TopDown (resetWaitsSelect ()).visit (ntrans) ;
				LPDList loc = `lpd ;
				Set<Wait> puniq = new HashSet<Wait> (ports) ;
				for (Wait w : puniq)
					loc = `ConslpdList (LocPortDec (ListPortDecl (PortDec (stringList (w.name), PortType (), PAList (), ProfileChannel (profile (Type_list ())))), w.left, w.right), loc) ;
				return `ExtendedProcessDecl (nam, genericParams, ports, params, loc, prios, states, vars, init, ntrans) ;
			}
		}
	}

	%strategy elimWaitStmt () extends Identity () {
		visit Statement {
			StWait (left, right) -> {
				String name = "gelimWait_" + boundToString (`left) + "_" + boundToString (`right) ;
				ports.add (new Wait (name, `left, `right)) ;
				return `StSignal (IdentExp (name, OpenKind (), BotType ()), ProfileChannel (profile (Type_list ()))) ;
			}
		}
	}

	%strategy resetWaitsSelect () extends Identity () {
		visit Statement {
			StSelect (ConsPStmList (l1, l2)) -> {
				return `StSelect (ConsPStmList (l1, TopDown (resetWaits ()).visit (l2))) ;
			}
		}
	}

	%strategy resetWaits () extends Identity () {
		visit Statement {
			s@StSignal (IdentExp (name, kind, type), _) -> {
				for (Iterator<Wait> it = ports.iterator () ; it.hasNext () ;) {
					Wait w = it.next () ;
					if (w.name.equals (`name)) {
						it.remove () ;
						return `StWait (w.left, w.right) ;
					}
				}
				return `s ;
			}
		}
	}

	private static String boundToString (Bound b) {
		%match (b) {
			Open (r) -> {
				return "o" + `r ;
			}
			Closed (r) -> {
				return "c" + `r ;
			}
		}
		return "" ;
	}

	static class Wait {
		public String name ;
		public Bound left ;
		public Bound right ;
		public Wait (String n, Bound l, Bound r) {
			name = n ;
			left = l ;
			right = r ;
		}
		public int hashCode () {
			return name.hashCode () ;
		}
		public boolean equals (Object o) {
			return o instanceof Wait && ((Wait)o).name.equals (name) ;
		}
	}

	private static List<Wait> ports ;
}
