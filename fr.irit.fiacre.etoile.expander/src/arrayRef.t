/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Permet de passer des tableaux de références en paramètre sous la forme &[N]v : T
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.*;
import fiacre.*;
import fiacre.types.*;
import java.lang.Integer;
import java.util.Hashtable;

public class arrayRef {

%typeterm Hashtable{
    implement 	{Hashtable}
    is_sort(t)	{ t instanceof Hashtable}
  }
%typeterm Integer{
    implement 	{Integer}
    is_sort(t)	{ t instanceof Integer}
  }

%include { fiacre/Fiacre.tom }
%include { sl.tom }
	%op Strategy CollectCte(tabCte:Hashtable) {
		  make(tabCte) { new common.CollectCte(tabCte) }
	}

	public static Program_Fiacre arrayRef (Program_Fiacre prg) throws Exception
	{
		try 
		{
			tabCte = new Hashtable<String,Integer>();
			Program_Fiacre p2 = `TopDown(CollectCte(tabCte)).visit(prg);
			p2 = `TopDown(NomArrayRef(tabCte)).visit(p2);
			return p2;		
		} 
		catch(VisitFailure e) {}
	   	return prg;
	}

	%strategy NomArrayRef(tabCte:Hashtable) extends Identity()
	{
		visit Declaration 
		{
			ProcessDecl (name, genP, ports, par, sta, vars, init, trans) ->
			{
				Hashtable<String, Integer> tabParamDec = new Hashtable<String, Integer> () ;
				ParamDecls paramsUpdate = `TopDown(updateParam(tabCte, tabParamDec)).visit(`par);
				Transitions transUpdate = `TopDown(updateTransition(tabParamDec)).visit(`trans);
				return `ProcessDecl (name, genP, ports, paramsUpdate, sta, vars, init, transUpdate);
			}

			ComponentDecl (name, gP, ports, par, var, lpd, prios, init, body) ->
			{
				Hashtable<String, Integer> tabParamDec = new Hashtable<String, Integer> () ;
				ParamDecls paramsUpdate = `TopDown(updateParam(tabCte, tabParamDec)).visit(`par);
				Composition bodyUpdate = `TopDown(updateBody(tabParamDec,tabCte)).visit(`body);			
				return `ComponentDecl (name, gP, ports, paramsUpdate, var, lpd, prios, init, bodyUpdate);
			}
		}
	}

	%strategy updateParam(tabCte:Hashtable, tabParamDec:Hashtable) extends Identity()
	{
		visit ParamDecl
		{		
			Paramdec(ArList(arl1*, ArrayRefArg(arg, size), arl2*), val, type) ->
			{
				Integer length = common.eval(`size,tabCte);
				if (length != null)
				{
					tabParamDec.put(`arg, common.eval(`size, tabCte));
					ArgList ar = `ArList(arl1*);
					while (length > 0)
					{
						length -= 1;
						ar = `ConsArList(RefArg("garrayRef_"+arg+"__"+length), ar);
					}
					%match (ar) {
						ArList(arl*) ->
						{
							ar = `ArList(arl*, arl2*);
						}
					}
					return `Paramdec(ar, val, type);
				}
			}
		}
	}
	
	%strategy updateBody(tabParamDec:Hashtable, tabCte:Hashtable) extends Identity()
	{
		visit Instance
		{
			Inst(s, te, pl, el) ->
			{
				return `Inst(s, te, pl, normExpList(el,tabParamDec,tabCte));
			}
		}
	}

	%strategy updateTransition(tabParamDec:Hashtable) extends Identity()
	{
		visit Statement
		{
			st@StCond(_, _, _, _) ->
			{
				return updateTrans(`st, tabParamDec);
			}
			st@StAssign(_, _) ->
			{
				return updateTrans(`st, tabParamDec);
			}
			st@StAny(_, _, _) ->
			{
				return updateTrans(`st, tabParamDec);
			}
			st@StInput(_, _, _, _) ->
			{
				return updateTrans(`st, tabParamDec);
			}
			st@StOutput(_, _, _) ->
			{
				return updateTrans(`st, tabParamDec);
			}
			st@StOn(_) ->
			{
				return updateTrans(`st, tabParamDec);
			}
			st@StWait(_, _) ->
			{
				return updateTrans(`st, tabParamDec);
			}
		}
	}

	private static Statement updateTrans(Statement stmt, Hashtable<String, Integer> tabParamDec)
	{
		containsIdentTab = "";
		Statement nstmt;
		try
		{
			nstmt = `TopDown(containsIdent(tabParamDec)).visit(stmt);
		}
		catch(VisitFailure e)
		{
			return stmt;
		}
		if (containsIdentTab.length() > 0)
		{
			Integer limit = tabParamDec.get(containsIdentTab);
			CaseElementList cel = `CaseEL();
			while (limit > 0)
			{
				limit -= 1;
				Statement st = stmt;
				try
				{
					st = `TopDown(replaceArrayAccess(containsIdentTab, containsIdentIndex, limit)).visit(stmt);
				}
				catch(VisitFailure e) {}
				cel = `ConsCaseEL(caseElement(true, IntExp(limit.toString(), BotType()), st), cel);
			}
			return `StCase(containsIdentIndex, cel);
		}
		return nstmt;
	}

	%strategy containsIdent(tabParamDec:Hashtable) extends Identity()
	{
		visit Exp
		{
			acc@ArrayAccessExp(IdentExp(arr, _, type), index, _) ->
			{
				if (!tabParamDec.containsKey (`arr))
					return `acc ;
				Integer idx = common.eval(`index, tabCte);
				if (idx != null)
					return `IdentExp("garrayRef_"+arr+"__"+idx, OpenKind(), type);
				containsIdentTab = `arr;
				containsIdentIndex = `index;
				return `acc;
			}
		}
	}

	%strategy replaceArrayAccess(array:String, oldindex:Exp, newindex:Integer) extends Identity()
	{
		visit Exp
		{
			acc@ArrayAccessExp(IdentExp(arr, _, type), index, _) ->
			{
				if (array.equals(`arr) && oldindex.equals(`index))
					return `IdentExp("garrayRef_"+array+"__"+newindex, OpenKind(), BotType());
				return `acc;
			}
		}
	}

	private static ExpList normExpList(ExpList el, Hashtable<String,Integer> tabParam, Hashtable<String,Integer> tabCte)
	{
		%match(el) 
		{
			listExp() ->
			{	
				return `listExp();
			}
			ConslistExp(ArrayExp(le@listExp(RefExp(_, _), _*), t),les) ->
			{	
				return `appendExpList(le,normExpList(les, tabParam, tabCte));
			}
			ConslistExp(hd, tl) ->
			{
				return `ConslistExp(hd, normExpList(tl, tabParam, tabCte));
			}
		}
		return el;
	}

	private static ExpList appendExpList(ExpList el1, ExpList el2)
	{
		%match(el1,el2) 
		{
			listExp(le1*),listExp(le2*) -> { return `listExp(le1,le2); }
		}
		return null;
	}

	private static Hashtable<String,Integer> tabCte;
	private static String containsIdentTab;
	private static Exp containsIdentIndex;
}

