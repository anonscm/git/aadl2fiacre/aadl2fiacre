/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Permet de paramétrer des processus/composants par des types/valeurs/constructeurs à la compilation.
 * Déclaration : process P<|N, T, C|>
 * Instantiation : P<|const 0, type T, constr0 C|>
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.*;
import fiacre.*;
import fiacre.types.*;
import java.lang.Integer;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Map;
import java.util.HashMap;

public class elimGen {
	%typeterm Hashtable{
		implement 	{Hashtable}
		is_sort(t)	{ t instanceof Hashtable}
	}

	%typeterm ArrayList{
		implement 	{ArrayList}
		is_sort(t)	{ t instanceof ArrayList}
	}

	%include { fiacre/Fiacre.tom }
	%include { sl.tom }

	static Strategy MyTopDown(Strategy s) {
		 return `mu(MuVar("x"), Try (Sequence(s, All(MuVar("x")))));
	}

	private static Map<String, Integer> NbInst = new HashMap<String,Integer>();
	static int nextInst(String k) {

	       	if (!NbInst.containsKey(k)) NbInst.put(k,1);
		int i = NbInst.get(k);
		NbInst.put(k,i+1);
		return i;
	}

	%strategy instanciate() extends Identity()
	{
		visit Program_Fiacre {
		      prg@_ -> {
		      	    Hashtable<String,ArrayList<ArrayList<Type>>> tabInstance = new Hashtable<String,ArrayList<ArrayList<Type>>>();

			    /* recuperation de la liste des instances a creer */
			    Program_Fiacre p2 = `MyTopDown(collectEff(tabInstance)).visit(`prg);
			    /* recuperation de la liste des instance generique */
			    Enumeration<String> enumInst = tabInstance.keys();
			    /* recuperation de l'instance a creer */
			    while (enumInst.hasMoreElements()) {
			    	/* recuperation de son id. */
				String id = enumInst.nextElement();
				/* recuperation de la liste des types effectifs */
				ArrayList<ArrayList<Type>> listTypeEff = tabInstance.get(id);
				for (int i=0; i < listTypeEff.size(); i++) {
					ArrayList<Type> tabTypeEff = listTypeEff.get(i);
					/* creation de l'instance associee */
					p2 = `BottomUp(creerInstance(id,tabTypeEff)).visit(p2);
					tabTypeEff.clear();
				}
			    }
		      	    return p2;	  
		      }
		}
	}

	public static Program_Fiacre elimGen (Program_Fiacre prg) throws Exception
	{
		Program_Fiacre p2 = `RepeatId(instanciate()).visit(prg);
		/* suppression des processus/composants generiques */
		p2 = `RepeatId(TopDown(suppressionAllGen())).visit(p2);
		return p2;
	}

	/*******************************************************************/
	/* collecte les types effectifs des instances a creer 		   */
	/*******************************************************************/
	%strategy collectEff(tab: Hashtable) extends Identity()
	{
		visit Declaration
		{
			d@ComponentDecl (_, stringList(_,_*), _, _, _, _, _, _, _) ->
			 { return `Fail().visit(`d); }
		}
		visit Instance 
		{
			Inst(name, genericParamsEffec@GenInstList(_,_*), strl, el) ->
			{
				ArrayList<GenInst> insts = new ArrayList<GenInst>();
				/* creer la liste des types effectifs */
				insts = creerlistGenInst(`genericParamsEffec, insts);
				ArrayList<ArrayList> listeEff = new ArrayList<ArrayList>();
				/* deja dans la table? */
				if (tab.containsKey(`name))
				{
					/* recuperation de la liste */
					listeEff = (ArrayList)tab.get((String)`name);	
				}
				/* ajout a la liste des nouveaux types effectifs */
				listeEff.add(insts);
				/* m-a-j de la hashtable contenant le nom du processus et la liste des types effectifs */
				tab.put(`name,listeEff);
				/* recuperation du num. d'instance pour renommer */
				int numInst =  listeEff.size();

				return `Inst("gelimGen_"+name+"_"+numInst, GenInstList(), strl, el);
			}
		}
	}

	/*******************************************************************/
	/* collecte les types effectifs des instances a creer 		   */
	/*******************************************************************/
	%strategy creerInstance(id:String, listType: ArrayList) extends Identity()
	{
		visit Declarations
		{
			ConsDeclarationList(proc@ExtendedProcessDecl (name, varGen, ports, params, lpd, prios,
						states, vars, init, trans),suite) && name == id ->
			{
				ArrayList<String> listVarGen = new ArrayList<String>();
				/* creer la liste des variables generiques */
				listVarGen = creerlistString(`varGen,listVarGen);
				/* associe la variable generique et son type effectif */
				Hashtable<String,GenInst> tab = creationHashtableVarInst(`name, listVarGen, listType);
				/* creation de l'instance du processus en tranformant la variables generique avec son type effectif */
				Declaration inst = `TopDown(replaceGen(tab)).visit(`ExtendedProcessDecl ("gelimGen_"+name+"_"+nextInst(id), 
							stringList(),ports,params,lpd,prios,states,vars,init,trans));
				return `ConsDeclarationList(inst, ConsDeclarationList(proc,suite));
			}
			ConsDeclarationList(comp@ComponentDecl (name, varGen, ports, params, vars, lpd, prios, init, bodyCmp),suite) && name == id ->
			{
				ArrayList<String> listVarGen = new ArrayList<String>();
				/* creation de la liste de variables generiques */
				listVarGen = creerlistString(`varGen,listVarGen);
				/* associe la variable generique avec son type effectif */
				Hashtable<String,GenInst> tab = creationHashtableVarInst(`name, listVarGen, listType);
				/* creation de l'instance du component en faisant le lien variables generiques avec son type effectifs */
				Declaration inst = `TopDown(replaceGen(tab)).visit(`ComponentDecl ("gelimGen_"+name+"_"+nextInst(id), 
							stringList(),ports,params,vars,lpd,prios,init,bodyCmp));
				return `ConsDeclarationList(inst,ConsDeclarationList(comp,suite));
			}
		}
	}

	/********************************/
	/* creer une liste de <String>  */
	/********************************/
	private static ArrayList<String> creerlistString(StringList sl, ArrayList<String> list)
	{
		%match(sl)
		{
			stringList(s) -> { list.add(`s); return list;}
			ConsstringList(s, csl) -> { list.add(`s); return creerlistString(`csl, list);}
		}
		return null;
	}

	/********************************/
	/* creer une liste de <Type>    */
	/********************************/
	private static ArrayList<Type> creerlistType(TypeList tl, ArrayList<Type> list)
	{
		%match(tl)
		{
			Type_list(t) -> { list.add(`t); return list;}
			ConsType_list(t, ctl) -> { list.add(`t); return creerlistType(`ctl, list);}
		}
		return null;
	}

	/********************************/
	/* creer une liste de <GenInst> */
	/********************************/
	private static ArrayList<GenInst> creerlistGenInst(GenInsts tl, ArrayList<GenInst> list)
	{
		%match(tl)
		{
			GenInstList(t) -> { list.add(`t); return list;}
			ConsGenInstList(t, ctl) -> { list.add(`t); return creerlistGenInst(`ctl, list);}
		}
		return null;
	}

	/*****************************************/
	/* substitution dans une liste de champs */
	/*****************************************/
	private static String substF(Hashtable ht, String n)
	{
		if (ht.containsKey(`n)) {
			GenInst maj = (GenInst)ht.get(`n);
			%match (maj) {
				ConstInst(c) -> {return `c; }
				TypeInst(t) -> {
					System.err.println("Illegal instance parameter:"+`t+" should be a field name\n");
					System.exit(1);
				}
				Constr0Inst(c) -> {
					System.err.println("Illegal instance parameter:"+`c+" should be a field name\n");
					System.exit(1);
				}
				Constr1Inst(c) -> {
					System.err.println("Illegal instance parameter:"+`c+" should be a field name\n");
					System.exit(1);
				}
			}
			return n;
		} else {
			return n;
		}
	}

	private static StringList substList(Hashtable ht, StringList tl)
	{
		%match(tl)
		{
			stringList() -> { return `stringList(); }
			stringList(n) -> { return `stringList(substF(ht,n)); }
			ConsstringList(n,l) -> { return `ConsstringList(substF(ht, n), substList(ht, l)); }
		}
		return null;
	}

	/*******************************************************************/
	/* on remplace les variables generiques avec leurs types effectifs */
	/* si necessaire                                                   */
	/*******************************************************************/
	%strategy replaceGen(ht: Hashtable) extends Identity()
	{
		visit Bound
		{
			b@Open(r) -> {
				if (ht.containsKey(`r)) {
					GenInst maj = (GenInst)ht.get(`r);
					%match (maj) {
						ConstInst(c) -> {return `Open(c);}
						TypeInst(t) -> {
							System.err.println("Illegal instance parameter:"+`t+" should be a constant\n");
							System.exit(1);
						}
						Constr0Inst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a field name\n");
							System.exit(1);
						}
						Constr1Inst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a field name\n");
							System.exit(1);
						}
					}
					return `b;
				} else {
					return `b;
				}
			}
			b@Closed(r) -> {
				if (ht.containsKey(`r)) {
					GenInst maj = (GenInst)ht.get(`r);
					%match (maj) {
						ConstInst(c) -> {return `Closed(c);}
						TypeInst(t) -> {
							System.err.println("Illegal instance parameter:"+`t+" should be a constant\n");
							System.exit(1);
						}
						Constr0Inst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a field name\n");
							System.exit(1);
						}
						Constr1Inst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a field name\n");
							System.exit(1);
						}
					}
					return `b;
				} else {
					return `b;
				}
			}
			b -> b
		}
		visit FieldsDecl
		{
			field(nl,t) -> { return `field(substList(ht, nl),t); }
		}
		visit Affectation
		{
			aff@Affect(n,v) -> {
				if (ht.containsKey(`n)) {
					GenInst maj = (GenInst)ht.get(`n);
					%match (maj) {
						ConstInst(c) -> {return `Affect(c,v);}
						TypeInst(t) -> {
							System.err.println("Illegal instance parameter:"+`t+" should be a field name\n");
							System.exit(1);
						}
						Constr0Inst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a field name\n");
							System.exit(1);
						}
						Constr1Inst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a field name\n");
							System.exit(1);
						}
					}
					return `aff;
				} else {
					return `aff;
				}
			}
		}
		visit GenInst
		{
			id@ConstInst(v) -> {
				if (ht.containsKey(`v)) {
					GenInst maj = (GenInst)ht.get(`v);
					%match (maj) {
						ConstInst(c) -> {return maj;}
						TypeInst(t) -> {
							System.err.println("Illegal instance parameter:"+`t+" should be a constant\n");
							System.exit(1);
						}
						Constr0Inst(c) -> { 
							System.err.println("Illegal instance parameter:"+`c+" should be a constant\n");
							System.exit(1);
						}
						Constr1Inst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a constant\n");
							System.exit(1);
						}

					}
					return `id;
				} else {
					return `id;
				}
			}
		}
		visit Exp
		{
			id@IdentExp(v,k,t) -> {
				if (ht.containsKey(`v)) {
					GenInst maj = (GenInst)ht.get(`v);
					%match (maj) {
						ConstInst(c) -> {return `IntExp(c, NatType());}
						TypeInst(t) -> {
							System.err.println("Illegal instance parameter:"+`t+" should be a constant\n");
							System.exit(1);
						}
						Constr0Inst(c) -> { return `IdentExp(c,ConstrKind(),t); }
						Constr1Inst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a constant\n");
							System.exit(1);
						}

					}
					return `id;
				} else {
					return `id;
				}
			}
			ctr@ConstrExp(k,e,t) -> {
				if (ht.containsKey(`k)) {
					GenInst maj = (GenInst)ht.get(`k);
					%match (maj) {
						Constr1Inst(c) -> { return `ConstrExp(c,e,t); }
						Constr0Inst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a constructor of arity 1\n");
							System.exit(1);
						}
						ConstInst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a constructor of arity 1\n");
							System.exit(1);
						}
						TypeInst(t) -> {
							System.err.println("Illegal instance parameter:"+`t+" should be a constructor of arity 1\n");
							System.exit(1);
						}

					}
					return `ctr;
				} else {
					return `ctr;
				}
			}
			rae@RecordAccessExp(r,id,t) -> {
				if (ht.containsKey(`id)) {
					GenInst maj = (GenInst)ht.get(`id);
					%match (maj) {
						ConstInst(c) -> { return `RecordAccessExp(r,c,t); }
						TypeInst(t) -> {
							System.err.println("Illegal instance parameter:"+`t+" should be a field name\n");
							System.exit(1);
						}
						Constr0Inst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a field name\n");
							System.exit(1);
						}
						Constr1Inst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a field name\n");
							System.exit(1);
						}
					}
					return `rae;
				} else {
					return `rae;
				}
			}
		}
		visit Type
		{
			NamedType(t) ->	{
				if (ht.containsKey(`t)) {
					GenInst maj = (GenInst)ht.get(`t);
					%match (maj) {
						TypeInst(t1) -> {return `t1;}
						ConstInst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a type\n");
							System.exit(1);
						}
						Constr0Inst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a type\n");
							System.exit(1);
						}
						Constr0Inst(c) -> {
							System.err.println("Illegal instance parameter:"+`c+" should be a type\n");
							System.exit(1);
						}
					}
					return `NamedType(t);
				} else {
					return `NamedType(t);
				}
			}
		}
	}


	/*******************************************************************/
	/* supprime les processus et composants generiques                 */
	/*******************************************************************/
	%strategy suppressionAllGen() extends Identity()
	{ 
		visit Declarations
		{
			DeclarationList(l1*,ExtendedProcessDecl (_, stringList(_,_*), _, _, _, _, _, _, _, _), l2*) ->
			{
				return `DeclarationList(l1,l2);
			}
			DeclarationList(l1*,ComponentDecl (_, stringList(_,_*), _, _, _, _, _, _, _),l2*) ->
			{	

				return `DeclarationList(l1,l2);
			}
		}
	}

	/********************************************************************/
	/* construit  une hashtable avec comme cle la var. gen. et son type */
	/* effectif							    */
	/********************************************************************/
	protected static Hashtable<String,GenInst> creationHashtableVarInst(String ipProgCourant, ArrayList<String> tabVarGen, ArrayList<GenInst> tabEff)
	{
		Hashtable<String,GenInst> Vgen_Eff = new Hashtable<String,GenInst>();

		/* retourne une exception si le nb de vars generiques est different du nb de types effectifs */
		if(tabEff.size() != tabVarGen.size())
		{
			System.err.println("NbVarGenTypeDifferent: "+ipProgCourant);
			System.exit(-1);
		}
		for(int i=0; i < tabVarGen.size(); i++)
		{
			/* construction de la hashtable contenant la variable generique en cle et son type effectif */
			Vgen_Eff.put(tabVarGen.get(i), tabEff.get(i));
		}
		return Vgen_Eff;
	}
}
