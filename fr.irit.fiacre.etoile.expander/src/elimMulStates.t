/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Élimination des états multiples :
 * Autorise l'écriture de plusieurs transitions à partir d'un état et les regroupe.
 * from s0 a; to s1
 * from s0 b; to s2
 * est autorisé et transformé en :
 * from s0 select a; to s1 [] b; to s2 end select
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.util.* ;

public class elimMulStates {
	%include {fiacre/Fiacre.tom}
	%include {sl.tom}

	public static Program_Fiacre elimMulStates (Program_Fiacre prg) throws Exception {
		try {
			return `RepeatId (TopDown (elimMultipleStates ())).visit (prg) ;
		} 
		catch (VisitFailure e) {
			return prg ;
		}
	}

	%strategy elimMultipleStates () extends Identity () {
		visit Transitions {
			TransList (tr1*, Trans (l1, s, StSelect (PStmList (StmList (st1*)))), tr2*, Trans (l2, s, st2), tr3*) -> {
				return `TransList (tr1*, Trans (l1+l2, s, StSelect (PStmList (StmList (st1*, st2)))), tr2*, tr3*) ;
			}
			TransList (tr1*, Trans (l1, s, st1), tr2*, Trans (l2, s, st2), tr3*) -> {
				return `TransList (tr1*, Trans (l1+l2, s, StSelect (PStmList (StmList (st1, st2)))), tr2*, tr3*) ;
			}
		}
	}
}
