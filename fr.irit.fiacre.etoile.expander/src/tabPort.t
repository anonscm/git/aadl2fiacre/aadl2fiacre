/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Permet de passer des tableaux de ports en paramètre des processus/composants.
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.*;
import fiacre.*;
import fiacre.types.*;
import java.lang.Integer;
import java.util.Hashtable;

public class tabPort {

%typeterm Hashtable{
    implement 	{Hashtable}
    is_sort(t)	{ t instanceof Hashtable}
  }

%include { fiacre/Fiacre.tom }
%include { sl.tom }
	%op Strategy CollectCte(tabCte:Hashtable) {
		  make(tabCte) { new common.CollectCte(tabCte) }
	}

	public static Program_Fiacre tabPort (Program_Fiacre prg) throws Exception
	{
		try 
		{
			Hashtable<String,Integer> tabCte = new Hashtable<String,Integer>();

			/* recuperation des constantes et normalisation de l'initialisation des tableaux */
			Program_Fiacre p2 = `TopDown(CollectCte(tabCte)).visit(prg);
			/* normalisation des tableaux de ports */
			p2 = `TopDown(NomTabPorts(tabCte)).visit(p2);
			return p2;		
		} 
		catch(VisitFailure e) {}
	   	return prg;
	}

	/*********************************************************************/
	/* Transforme les tableaux de ports en declaration de ports simple   */
	/*********************************************************************/
	%strategy NomTabPorts(tabCte:Hashtable) extends Identity()
	{
		visit Declaration 
		{
			ProcessDecl (name, genP, ports, par, sta, vars, init, trans) ->
			{
				Hashtable<String, Integer> tabPortDec = new Hashtable<String, Integer> () ;
				/* m-a-j du PortDecls */
				PortDecls portsUpdate = `TopDown(updatePort(tabCte, tabPortDec)).visit(`ports);
				/* m-a-j du Transitions pour les tableaux indexe par une variables */
				Transitions transUpdate = `TopDown(updateTransition(tabPortDec)).visit(`trans);
				return `ProcessDecl (name, genP, portsUpdate, par, sta, vars, init, transUpdate);
			}

			ComponentDecl (name, gP, ports, pa, var, lpd, prios, init, body) ->
			{
				Hashtable<String, Integer> tabPortDec = new Hashtable<String, Integer> () ;
				/* m-a-j du PortDecls */
				PortDecls portsUpdate = `TopDown(updatePort(tabCte, tabPortDec)).visit(`ports);
				/* m-a-j du LPDList, les ports en arguments */
				LPDList lpdUpdate = `TopDown(updatePort(tabCte, tabPortDec)).visit(`lpd);
				/* m-a-j du PrioDeclList, la priorite des ports */
				PrioDeclList priosUpdate = `TopDown(updatePrio(tabPortDec,tabCte)).visit(`prios);
				/* m-a-j du Composition, les intances et la par. si il y en a */
				Composition bodyUpdate = `TopDown(updateBody(tabPortDec,tabCte)).visit(`body);			
				return `ComponentDecl (name, gP, portsUpdate, pa, var, lpdUpdate, priosUpdate, init, bodyUpdate);
			}
		}
	}

	/******************************************************/
	/* Transforme les tableaux de ports dans les portDec  */
	/******************************************************/
	%strategy updatePort(tabCte:Hashtable, tabPortDec:Hashtable) extends Identity()
	{
		visit PortDecl
		{		
			PortDec(pl, ArrayType(taille, PortType()), pal, chan)  ->
				{
					/* evaluation */
					Integer length = common.eval(`taille,tabCte);
					if (length != null)
					{
						/* enregistre les tableaux de ports declarés */
						updateTabPort(`pl,tabPortDec,length);
						/* transformation du port en plusieur ident en fonction de la taille  */
						return `PortDec(normStringList(pl,length), PortType(), pal, chan);
					}
	        		}
		}
	}
	
	/******************************************************/
	/* Transforme les tableaux de ports dans les prioDecl */
	/******************************************************/
	%strategy updatePrio(tabPortDec:Hashtable, tabCte:Hashtable) extends Identity()
	{
		visit PriorDecl
		{
			PriorDec(el1, el2) ->
			{
				return `PriorDec(normExpList(el1,tabPortDec,tabCte), normExpList(el2,tabPortDec,tabCte));
			}
		}
	}	

	/******************************************************/
	/* Transforme les tableaux de ports dans les Compo    */
	/******************************************************/
	%strategy updateBody(tabPortDec:Hashtable, tabCte:Hashtable) extends Identity()
	{
		visit Instance
		{
			Inst(s, te, pl, el) ->
			{
				return `Inst(s, te, normExpList(pl,tabPortDec,tabCte), el);
			}
		}

		visit PortSet
		{
			AllPorts() -> { return ` AllPorts(); }
			SomePorts(pl) -> { return `SomePorts(normExpList(pl,tabPortDec,tabCte)); }
		}
	}

	/***************************************************************************/
	/* Transforme les signaux et les comunications dans le corps de processus  */
	/* lors d'une utilisation avec une var.                                    */
	/***************************************************************************/
	%strategy updateTransition(tabPortDec:Hashtable) extends Identity()
	{
		visit Statement
		{
			StSignal(ArrayAccessExp(id@IdentExp(tab, k, t), pos, type), chan) ->
			{
				/* recuperation de la taille du tableau declare */
				Integer limit = (Integer)tabPortDec.get((String)`tab);
				if ((limit != null) && containsIdent(`pos))
				{
					return `StCase(pos,
							/* ajout d'autant de cas en fonction de la taille du tableau */
							modifyCase(id,
								    limit-2,
			   					    ConsCaseEL(caseElement(true,
	    			     						IntExp (String.valueOf(limit.intValue()-1), BotType()),
				      						StSignal(IdentExp("gtabPort_"+tab+"__"+String.valueOf(limit.intValue()-1),OpenKind(), type), chan)),
				   						CaseEL()),
								    type,
							            chan)
							);
				}
				return `StSignal(IdentExp("gtabPort_"+tab+"__"+String.valueOf(common.eval(pos,tabPortDec)),OpenKind(), type), chan);
			}

                	StInput(ArrayAccessExp(id@IdentExp(tab, k, t), pos, type), chan, elist, oe) -> 
			{
				/* recuperation de la taille du tableau declare */
				Integer limit = (Integer)tabPortDec.get((String)`tab);
				if ((limit != null) && containsIdent(`pos))
				{
					return `StCase(pos,
							/* ajout d'autant de cas en fonction de la taille du tableau */
							modifyCase(id,
								    limit-2,
			   					    ConsCaseEL(caseElement(true,
	    			     						IntExp (String.valueOf(limit.intValue()-1), BotType()),
										StInput(IdentExp("gtabPort_"+tab+"__"+String.valueOf(limit.intValue()-1),OpenKind(), type), chan, elist, oe)),
										CaseEL()),
								    type,
							            chan,
								    elist,
								    oe)
							);
				}
				return `StInput(IdentExp("gtabPort_"+tab+"__"+String.valueOf(common.eval(pos,tabPortDec)),OpenKind(), type), chan, elist, oe);
			}

                	StOutput(ArrayAccessExp(id@IdentExp(tab, k, t), pos, type), chan, elist) -> 
			{
				/* recuperation de la taille du tableau declare */
				Integer limit = (Integer)tabPortDec.get((String)`tab);
				if ((limit != null) && containsIdent(`pos))
				{
					return `StCase(pos,
							/* ajout d'autant de cas en fonction de la taille du tableau */
							modifyCase(id,
								    limit-2,
			   					    ConsCaseEL(caseElement(true,
	    			     						IntExp (String.valueOf(limit.intValue()-1), BotType()),
										StOutput(IdentExp("gtabPort_"+tab+"__"+String.valueOf(limit.intValue()-1),OpenKind(), type), chan, elist)),
										CaseEL()),
								    type,
							            chan,
								    elist)
							);
				}
				return `StOutput(IdentExp("gtabPort_"+tab+"__"+String.valueOf(common.eval(pos,tabPortDec)),OpenKind(), type), chan, elist);
			}
		}
	}

	/******************************************************/
	/* Verifie la presence d'une var.                      */
	/******************************************************/
	private static boolean containsIdent(Exp exp)
	{
		%match (exp)
		{
			ParenExp(e) -> { return containsIdent(`e); }
           		CondExp(test, st1, st2, type) -> { return true; }
           		Unop(uop, type, e) -> { return containsIdent(`e); }
           		Binop(bop, type, e1, e2) -> { return containsIdent(`e1) || containsIdent(`e2); }
           		Infix(iop, type, e1, e2) -> { return containsIdent(`e1) || containsIdent(`e2); }
           		RefExp(ref, type) -> { return true; }
           		ConstrExp(consrt, e, type)  -> { return containsIdent(`e); }
           		IdentExp(var, kind, type) -> { return true; }
          		ArrayAccessExp(array, e, type) -> { return true; }
           		RecordAccessExp(record, id, type) -> { return true; }
		}
		return false;
	}

	/***************************************************   CaseElementList    *****************************************/
	
	/******************************************************/
	/* Permet de rajouter des cas pour un signal          */
	/******************************************************/
	private static CaseElementList modifyCase(Exp idTab, int limit, CaseElementList cel, Type type, Channel chan)
	{
		%match(cel) 
		{
			CaseEL() -> 
			{
				if (limit >= 0)
				{
					CaseElementList c = `ConsCaseEL(caseElement(true,
	    			     							IntExp (String.valueOf(limit), BotType()),
				      							StSignal(IdentExp("gtabPort_"+getId(idTab)+"__"+String.valueOf(limit),OpenKind(), type), chan)),
				   					CaseEL());
					return modifyCase(idTab, limit-1, appendCaseElementList(cel,c), type, chan);
				}
				return `CaseEL();
			}
			ConsCaseEL(cas,ccas) -> { return `ConsCaseEL(cas,modifyCase(idTab, limit, ccas, type, chan)); }
		}
		return null;
	}
	
	/******************************************************/
	/* Permet de rajouter des cas pour un reception       */
	/******************************************************/
	private static CaseElementList modifyCase(Exp idTab, int limit, CaseElementList cel, Type type, Channel chan, ExpList elist, Optional_Exp oe)
	{
		%match(cel) 
		{
			CaseEL() -> 
			{
				if (limit >= 0)
				{
					CaseElementList c = `ConsCaseEL(caseElement(true,
	    			     							IntExp (String.valueOf(limit), BotType()),
				      							StInput(IdentExp("gtabPort_"+getId(idTab)+"__"+String.valueOf(limit),OpenKind(), type), chan, elist, oe)),
				   					CaseEL());
					return modifyCase(idTab, limit-1, appendCaseElementList(cel,c), type, chan, elist, oe);
				}
				return `CaseEL();
			}
			ConsCaseEL(cas,ccas) -> { return `ConsCaseEL(cas,modifyCase(idTab, limit, ccas, type, chan, elist, oe)); }
		}
		return null;
	}
	
	/******************************************************/
	/* Permet de rajouter des cas pour un envoi           */
	/******************************************************/
	private static CaseElementList modifyCase(Exp idTab, int limit, CaseElementList cel, Type type, Channel chan, ExpList elist)
	{
		%match(cel) 
		{
			CaseEL() -> 
			{
				if (limit >= 0)
				{
					CaseElementList c = `ConsCaseEL(caseElement(true,
	    			     							IntExp (String.valueOf(limit), BotType()),
				      							StOutput(IdentExp("gtabPort_"+getId(idTab)+"__"+String.valueOf(limit),OpenKind(), type), chan, elist)),
				   					CaseEL());
					return modifyCase(idTab, limit-1, appendCaseElementList(cel,c), type, chan, elist);
				}
				return `CaseEL();
			}
			ConsCaseEL(cas,ccas) -> { return `ConsCaseEL(cas,modifyCase(idTab, limit, ccas, type, chan, elist)); }
		}
		return null;
	}

	/******************************************************/
	/* concatene 2 CaseElementList                        */
	/******************************************************/
	private static CaseElementList appendCaseElementList(CaseElementList cel1, CaseElementList cel2)
	{
		%match(cel1,cel2)
		{
			CaseEL(c1*),CaseEL(c2*) -> { return `CaseEL(c1,c2); }
		}
		return null;
	}

	/***************************************************   StringList    *****************************************/
	/******************************************************/
	/* suvegarde les id. des tableaux de ports            */
	/******************************************************/
	private static void updateTabPort(StringList sl, Hashtable<String,Integer> tabPortDec, int taille)
	{
		%match(sl) 
		{
			stringList(s) -> { tabPortDec.put((String)`s, new Integer(taille)); }
			ConsstringList(s,ls) -> 
				{ 
					tabPortDec.put((String)`s, new Integer(taille));
					updateTabPort(`ls, tabPortDec, taille);
				}
		}
	}

	/******************************************************/
	/* Normalise des stringList                           */
	/******************************************************/
	private static StringList normStringList(StringList sl, int taille)
	{
		%match(sl) 
		{
			stringList(s) -> { return dupPort(`s,taille-1); }
			ConsstringList(s,ls) -> { return appendStringList(dupPort(`s,taille-1),normStringList(`ls, taille)); }
		}
		return null;
	}

	/******************************************************/
	/* concatene 2 StringList                             */
	/******************************************************/
	private static StringList appendStringList(StringList sl1, StringList sl2)
	{
		%match(sl1,sl2) 
		{
			stringList(s1*),stringList(s2*) -> { return `stringList(s1,s2); }
		}
		return null;
	}

	/***********************************************************/
	/* cree une StringList a partir d'un string avec une limit */
	/* du nb de fois que sera cree l'id                        */
	/***********************************************************/
	private static StringList dupPort(String s, int taille)
	{
		if(taille == 0)
		{
			return `stringList("gtabPort_"+s+"__"+taille);
		}
		return `ConsstringList("gtabPort_"+s+"__"+taille, dupPort(s,taille-1));
	}

	/***************************************************   ExpList    *****************************************/
	/***********************************************************/
	/* normalise une ExpList                                   */
	/***********************************************************/
	private static ExpList normExpList(ExpList el, Hashtable<String,Integer> tabPort, Hashtable<String,Integer> tabCte)
	{
		%match(el) 
		{
			listExp() ->
				{	
					return `listExp();
				}
			/* un tableau utilise a la vole */
			ConslistExp(ArrayExp(le, t),les) ->
				{	
					/* les processus/composants attendent les elements du tableau par ordre d'indice decroissant
					 * la notation intuitive etant par ordre d'indice croissant, il faut inverser les elements du tableau */
					ExpList lst = `le;
					ExpList res = `listExp ();
					while(!lst.equals(`listExp()))
					{
						%match(lst)
						{
							ConslistExp(hd, tl) ->
							{
								res = `ConslistExp(hd, res);
								lst = `tl;
							}
						}
					}
					return `appendExpList(res,normExpList(les, tabPort, tabCte));
				}
			/* l'ident. d'un tableau */
			ConslistExp(IdentExp(v, k, t),le) -> 
				{
					Integer taille = tabPort.get((String)`v);
					if(taille != null)
					{
						return appendExpList(dupExp(`v,taille-1),normExpList(`le, tabPort, tabCte));
					}
					return `ConslistExp(IdentExp(v, k, t),normExpList(le, tabPort, tabCte));
			        }
			/* transformation d'un acces au tableau en une variable */
			ConslistExp(ArrayAccessExp(array, pos, type),le) -> 
				{
					Integer taille = common.eval(`pos,tabCte);
					if(taille != null)
					{
						return `ConslistExp(IdentExp("gtabPort_"+getId(array)+"__"+taille.toString(), OpenKind(), type),normExpList(le, tabPort, tabCte));
					}
					return `ConslistExp(ArrayAccessExp(array, pos, type),normExpList(le, tabPort, tabCte));
			        }
		}
		return el;
	}

	/***********************************************************/
	/* recupere l'id d'un ident                                */
	/***********************************************************/
	private static String getId(Exp e)
	{
		%match (e)
		{
			 IdentExp(var, _, _) -> { return `var; }
		}
		return null;
	}

	/***********************************************************/
	/* concatene 2 ExpList                                     */
	/***********************************************************/
	private static ExpList appendExpList(ExpList el1, ExpList el2)
	{
		%match(el1,el2) 
		{
			listExp(le1*),listExp(le2*) -> { return `listExp(le1,le2); }
		}
		return null;
	}

	/***********************************************************/
	/* cree une ExpList a partir d'une string                  */
	/***********************************************************/
	private static ExpList dupExp(String s, int taille)
	{
		if(taille == 0)
		{
			return `listExp(IdentExp("gtabPort_"+s+"__"+taille, OpenKind(), PortType()));
		}
		return `ConslistExp(IdentExp("gtabPort_"+s+"__"+taille, OpenKind(), PortType()), dupExp(s,taille-1));
	}
}

