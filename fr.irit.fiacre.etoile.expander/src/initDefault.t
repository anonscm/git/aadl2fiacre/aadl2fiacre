/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Permet d'initialiser des variables avec la valeur "default"
 * (false pour les booléens, 0 pour les entiers, ...).
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.util.* ;

public class initDefault {
	%include {fiacre/Fiacre.tom}
	%include {sl.tom}

	public static Program_Fiacre initDefault (Program_Fiacre prg) throws Exception {
		try {
			ast = prg ;
			return `TopDown (initDefaultStrategy ()).visit (prg) ;
		} 
		catch (VisitFailure e) {
			return prg ;
		}
	}

	%strategy initDefaultStrategy () extends Identity () {
		visit VarDecl {
			VarDec(lst, type, NoneExp()) -> {
				return `VarDec(lst, type, OptExp(defaultValue (type))) ;
			}
		}
	}

	private static Type findNamedType (String name) {
		typeFound = null ;
		try {
			`TopDown (findNamedStrategy (name)).visit (ast) ;
		}
		catch (VisitFailure e) {
		}
		if (typeFound == null)
			System.err.println ("Failed to find named type " + name) ;
		return typeFound ;
	}

	%strategy findNamedStrategy (String name) extends Identity () {
		visit Declaration {
			TypeDecl (n, type) -> {
				if (`n.equals (name))
					typeFound = `type ;
			}
		}
	}

	private static Exp defaultValue (Type t) {
		%match (t) {
			BoolType () -> {
				return `BoolExp(false) ;
			}
			IntType () || NatType () << t -> {
				return `IntExp("0", BotType()) ;
			}
			IntervalType (min, max) -> {
				return `min ;
			}
			ArrayType (exp, type2) -> {
				return `Infix(MUL(), BotType(), ArrayExp(listExp(defaultValue(type2)), type2), exp) ;
			}
			RecordType (fields) -> {
				return `RecordExp (recordDefault (t), BotType ()) ;
			}
			UnionType (ConsfieldList (field (ConsstringList (name, names), BotType ()), fields)) -> {
				return `IdentExp (name, OpenKind (), BotType ()) ;
			}
			UnionType (ConsfieldList (field (ConsstringList (name, names), type), fields)) -> {
				return `ConstrExp (name, defaultValue (type), type) ;
			}
			QueueType (_, type) -> {
				return `QueueExp (listExp (), type) ;
			}
			NamedType (name) -> {
				return defaultValue (findNamedType (`name)) ;
			}
		}
		System.err.println ("Impossible to find default value of type " + `t) ;
		return null ;
	}

	private static AffectationList recordDefault (Type t) {
		%match (t) {
			RecordType (fieldList ()) -> {
				return `AffList () ;
			}
			RecordType (ConsfieldList (field (stringList (), type), fields)) -> {
				return recordDefault (`RecordType (fields)) ;
			}
			RecordType (ConsfieldList (field (ConsstringList (name, names), type), fields)) -> {
				return `ConsAffList (Affect (name, defaultValue (type)), recordDefault (RecordType (ConsfieldList (field (names, type), fields)))) ;
			}
		}
		return null ;
	}

	private static Program_Fiacre ast ;

	private static Type typeFound ;
}
