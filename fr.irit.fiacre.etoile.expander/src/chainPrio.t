/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Chaînage des priorités :
 * Autorise l'écriture a > b > c > d et la remplace par a > b, b > c, c > d.
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.util.* ;

public class chainPrio {
	%include {fiacre/Fiacre.tom}
	%include {sl.tom}

	public static Program_Fiacre chainPrio (Program_Fiacre prg) throws Exception {
		try {
			return `TopDown (chainagePriorites ()).visit (prg) ;
		} 
		catch (VisitFailure e) {
			return prg ;
		}
	}

	%strategy chainagePriorites () extends Identity () {
		visit PrioDeclList {
			PrioList (PriorDec (p1, p2, p3, prios*), lst*) -> {
				return `PrioList (PriorDec (p1, p2), PriorDec (p2, p3, prios*), lst*) ;
			}
		}
	}
}
