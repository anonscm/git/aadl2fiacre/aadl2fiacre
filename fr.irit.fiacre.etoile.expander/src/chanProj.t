/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Permet de projeter un canal T1#...#Tn sur k indices sous la forme chan^{i, ..., j}.
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.util.* ;

public class chanProj {
	%typeterm Hashtable{
		implement 	{Hashtable}
		is_sort(t)	{ t instanceof Hashtable}
	}
	%include {fiacre/Fiacre.tom}
	%include {sl.tom}
	%op Strategy CollectCte (tabCte:Hashtable) {
		  make(tabCte) { new common.CollectCte (tabCte) }
	}

	public static Program_Fiacre chanProj (Program_Fiacre prg) throws Exception {
		try {
			program = prg ;
			`TopDown (CollectCte (tabCte)).visit (prg);
			prg = `TopDown (channelProjection ()).visit (prg) ;
			%match (prg, newprocesses) {
				Prog (DeclarationList (l1*), main), DeclarationList (l2*) -> {
					return `Prog (DeclarationList (l2*, l1*), main) ;
				}
			}
		} 
		catch (VisitFailure e) {
		}
		return prg ;
	}

	%strategy channelProjection () extends Identity () {
		visit Declaration {
			c@ComponentDecl (_, _, _, _, _, _, _, _, _) -> {
				return `TopDown (channelProjectionInst (c)).visit (`c) ;
			}
		}
	}

	%strategy channelProjectionInst (c : Declaration) extends Identity () {
		visit Instance {
			inst@Inst (name, gen, params, el) -> {
				List<Projection> projs = new LinkedList<Projection> () ;
				ExpList nparams = extractProjections (`params, 0, projs, c) ;
				if (projs.size () > 0)
					return `Inst (createProcCompo (name, projs), gen, nparams, el) ;
				return `inst ;
			}
		}
	}

	private static ExpList extractProjections (ExpList params, int index, List<Projection> projs, Declaration compo) {
		%match (params) {
			ConslistExp (ChanProjExp (chan, fields), tail) -> {
				projs.add (new Projection (index, `fields, findChannel (`chan, compo))) ;
				return `ConslistExp (chan, extractProjections (tail, index + 1, projs, compo)) ;
			}
			ConslistExp (head, tail) -> {
				return `ConslistExp (head, extractProjections (tail, index + 1, projs, compo)) ;
			}
		}
		return `listExp () ;
	}

	private static Channel findChannel (Exp e, Declaration compo) {
		try {
			%match (e) {
				IdentExp (var, kind, type) -> {
					channelFound = null ;
					`TopDown (findChannelStrategy (var)).visit (compo) ;
					if (channelFound != null)
						return channelFound ;
				}
			}
		} catch (VisitFailure exc) {
		}
		System.err.println ("Failed to find channel of " + e) ;
		System.exit (1) ;
		return null ;
	}

	%strategy findChannelStrategy (port : String) extends Identity () {
		visit PortDecl {
			PortDec (stringList (_*, p, _*), type, pal, chan) -> {
				if (`p.equals (port))
					channelFound = `chan ;
			}
		}
	}

	private static String createProcCompo (String name, List<Projection> mparams) {
		String newname = "gchanProj_" + name + "_" + processNb ;
		%match (program) {
			Prog (decl, main) -> {
				Declaration dec = findProcCompo (name, mparams, `decl) ;
				%match (dec) {
					ExtendedProcessDecl (name, genericPorts, ports, params, lpd, prios, states, vars, init, trans) -> {
						PortDecls nports = `ports ;
						for (Projection p : mparams) {
							nports = updatePort (nports, 0, p) ;
						}
						Transitions ntrans = `trans ;
						try {
							Hashtable<String, Projection> projs = matchPortsProjs (`ports, mparams) ;
							ntrans = `TopDown (channelActions (projs)).visit (`trans) ;
						} catch (VisitFailure e) {
						}
						dec = `ExtendedProcessDecl (name, genericPorts, nports, params, lpd, prios, states, vars, init, ntrans) ;
						processNb += 1 ;
					}
					ComponentDecl (name, genericParams, ports, params, vars, lpd, prios, init, bodyCmp) -> {
						PortDecls nports = `ports ;
						for (Projection p : mparams) {
							nports = updatePort (nports, 0, p) ;
						}
						dec = `ComponentDecl (name, genericParams, nports, params, vars, lpd, prios, init, bodyCmp) ;
						processNb += 1 ;
						try {
							dec = `TopDown (channelProjectionInst (dec)).visit (dec) ;
						} catch (VisitFailure e) {
						}
					}
				}
				newprocesses = `ConsDeclarationList (dec, newprocesses) ;
			}
		}
		return newname ;
	}

	private static Declaration findProcCompo (String name, List<Projection> mparams, Declarations decl) {
		%match (decl) {
			ConsDeclarationList (ExtendedProcessDecl (nam, genericPorts, ports, params, lpd, prios, states, vars, init, trans), tl) -> {
				if (name.equals (`nam))
					return `ExtendedProcessDecl ("gchanProj_" + name + "_" + processNb, genericPorts, ports, params, lpd, prios, states, vars, init, trans) ;
				return findProcCompo (name, mparams, `tl) ;
			}
			ConsDeclarationList (ComponentDecl (nam, genericParams, ports, params, vars, lpd, prios, init, bodyCmp), tl) -> {
				if (name.equals (`nam))
					return `ComponentDecl ("gchanProj_" + name + "_" + processNb, genericParams, ports, params, vars, lpd, prios, init, bodyCmp) ;
				return findProcCompo (name, mparams, `tl) ;
			}
			ConsDeclarationList (_, tl) -> {
				return findProcCompo (name, mparams, `tl) ;
			}
		}
		System.err.println ("Failed to find process or component named " + name) ;
		System.exit (1) ;
		return null ;
	}

	private static PortDecls updatePort (PortDecls ports, int index, Projection p) {
		%match (ports) {
			ConsListPortDecl (PortDec (lp, type, pal, chan), tail) -> {
				StringList olp = `lp ;
				StringList nlp = `stringList () ;
				while (index < p.index) {
					%match (olp) {
						ConsstringList (hd, tl) -> {
							nlp = `ConsstringList (hd, nlp) ;
							olp = `tl ;
							index += 1 ;
						}
					}
				}
				if (index == p.index) {
					%match (olp) {
						ConsstringList (hd, tl) -> {
							PortDecls res = `tail ;
							if (!`tl.equals (`stringList ()))
								res = `ConsListPortDecl (PortDec (tl, type, pal, chan), res) ;
							res = `ConsListPortDecl (PortDec (stringList (hd), type, pal, p.channel), res) ;
							if (!nlp.equals (`stringList ()))
								res = `ConsListPortDecl (PortDec (nlp, type, pal, chan), res) ;
							return res ;
						}
					}
				}
				return `ConsListPortDecl (PortDec (nlp, type, pal, chan), updatePort (tail, index, p)) ;
			}
		}
		return ports ;
	}

	private static Hashtable<String, Projection> matchPortsProjs (PortDecls ports, List<Projection> projs) {
		Hashtable<String, Projection> res = new Hashtable<String, Projection> () ;
		Projection proj = projs.get (0) ;
		int projidx = 0 ;
		int index = 0 ;
		%match (ports) {
			ConsListPortDecl (PortDec (ConsstringList (name, names), type, pal, chan), tail) -> {
				if (proj.index == index) {
					res.put (`name, proj) ;
					projidx += 1 ;
					if (projidx == projs.size ())
						return res ;
					proj = projs.get (projidx) ;
				}
				if (`names.equals (`stringList ()))
					ports = `tail ;
				else
					ports = `ConsListPortDecl (PortDec (names, type, pal, chan), tail) ;
				index += 1 ;
			}
		}
		return res ;
	}

	%strategy channelActions (mparams : Hashtable) extends Identity () {
		visit Statement {
			st@StInput (port@IdentExp (var, kind, type), chan, p, oe) -> {
				if (mparams.containsKey (`var))
					return `StInput (port, chan, updateActionPorts (p, (Projection)mparams.get (var)), oe) ;
				return `st ;
			}
			st@StOutput (port@IdentExp (var, kind, type), chan, p) -> {
				if (mparams.containsKey (`var))
					return `StOutput (port, chan, updateActionPorts (p, (Projection)mparams.get (var))) ;
				return `st ;
			}
		}
	}

	private static ExpList updateActionPorts (ExpList p, Projection proj) {
		ExpList res = `listExp () ;
		TypeList tl = null ;
		%match (proj.channel) {
			ProfileChannel (profile (t)) -> {
				tl = `t ;
			}
		}
		int index = 0 ;
		while (!tl.equals (`Type_list ())) {
			%match (tl) {
				ConsType_list (_, tail) -> {
					if (proj.fields.contains (index + 1))
						%match (p, res) {
							ConslistExp (e, ptl), listExp (r*) -> {
								res = `listExp (r*, e) ;
								p = `ptl ;
							}
						}
					else
						%match (res) {
							listExp (r*) -> {
								res = `listExp (r*, AnyExp ()) ;
							}
						}
					tl = `tail ;
					index += 1 ;
				}
			}
		}
		return res ;
	}

	private static class Projection {
		public int index ;
		public List<Integer> fields ;
		public Channel channel ;
		public Projection (int i, ExpList f, Channel c) {
			index = i ;
			channel = c ;
			fields = new LinkedList<Integer> () ;
			while (!f.equals (`listExp ())) {
				%match (f) {
					ConslistExp (exp, tail) -> {
						fields.add (common.eval (`exp, tabCte)) ;
						f = `tail ;
					}
				}
			}
		}
	}

	private static Channel channelFound ;

	private static int processNb = 0 ;

	private static Declarations newprocesses = `DeclarationList () ;

	private static Hashtable<String, Integer> tabCte = new Hashtable<String, Integer> () ;

	private static Program_Fiacre program ;
}
