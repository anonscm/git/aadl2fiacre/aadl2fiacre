/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Elimine les synchronisations multiples sur un seul état en rajoutant les états intermédiaires nécessaires :
 * from s0 a; b? to s1 devient from s0 a; to impl_s0 from impl_s0 b?; to s1
 * Ajoute un wait[0, 0] aux transitions n'ayant aucune synchronisation.
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.lang.Integer ;
import java.util.*  ;

public class seqSync {
	%typeterm List{
		implement 	{List}
		is_sort(t)	{t instanceof List}
	}

	%include {fiacre/Fiacre.tom}
	%include {sl.tom}

	private static int tlabcpt = 0;
	private static String tlab = "";
	private static String nextLab() {
		if (tlab.length() == 0) return tlab;
		tlabcpt++;
		return tlab+tlabcpt;
	}

	static class ResT {
		public ResT (Statement st, Transitions tr) {
			stmt = st ;
			trans = tr ;
		}
		public Statement stmt ;
		public Transitions trans ;
	}

	static class ResTList {
		public ResTList (StatementList st, Transitions tr) {
			stmt = st ;
			trans = tr ;
		}
		public StatementList stmt ;
		public Transitions trans ;
	}

	static class ResTCondList {
		public ResTCondList (ConditionalStatementList st, Transitions tr) {
			stmt = st ;
			trans = tr ;
		}
		public ConditionalStatementList stmt ;
		public Transitions trans ;
	}

	static class ResTCaseList {
		public ResTCaseList (CaseElementList st, Transitions tr) {
			stmt = st ;
			trans = tr ;
		}
		public CaseElementList stmt ;
		public Transitions trans ;
	}

	public static Program_Fiacre seqSync(Program_Fiacre prg) throws Exception {
		try  {
			Program_Fiacre pf = `TopDown(elimMultipleSynchro()).visit(prg) ;
			return pf ;
		} 
		catch(VisitFailure e) {
		}
	   	return prg ;
	}

	%strategy elimMultipleSynchro () extends Identity () {
		visit Declaration {
			ExtendedProcessDecl (name, genericParams, ports, params, lpd, prios, stat, vars, init, trans) -> {
				stateId = 0 ;
				states = `stat ;
				Transitions tr = elimSynchro (`trans) ;
				return `ExtendedProcessDecl (name, genericParams, ports, params, lpd, prios, states, vars, init, tr) ;
			}
		}
	}

	private static Transitions elimSynchro (Transitions trans) {
		%match (trans) {
			ConsTransList (Trans (lab, state, st), tl) -> {
				tlabcpt = 0;
				tlab = `lab;
				ResT res = elimSynchroSt (`state, `st, `NoneSt (), `NoneSt (), false) ;
				Transitions res2 = elimSynchro (`tl) ;
				return concTransList (`ConsTransList (Trans (lab, state, res.stmt), res.trans), res2) ;
			}
			TransList () -> {
				return `TransList () ;
			}
		}
		System.err.println ("Match failed.") ;
		return `TransList () ;
	}

	private static ResT elimSynchroSt (String s, Statement st, Optional_Statement after, Optional_Statement before, boolean synchro) {
		if (containsSynchro (st)) {
			%match (st) {
				StSignal (_, _) || StOutput (_, _, _) << st || StInput (_, _, _, _) << st || StWait (_, _) << st -> {
					if (synchro) {
						String state = newState () ;
						ResT res = elimSynchroSt (state, st, after, `NoneSt (), false) ;
						return new ResT (concStmt (before, `StTo (state)), `ConsTransList (Trans (nextLab(), state, res.stmt), res.trans)) ;
					} else {
						%match (after) {
							NoneSt () -> {
								return new ResT (concStmt (before, st), `TransList ()) ;
							}
							Statmt (stmt) -> {
								return elimSynchroSt (s, `stmt, `NoneSt (), `Statmt (concStmt (before, st)), true) ;
							}
						}
					}
				}
				StSequence (t@StTag(_), st2) -> {
					   ResT res = elimSynchroSt (s, `st2, after, before, synchro);
					   return new ResT(`StSequence(t, res.stmt), res.trans);
				}
				StSequence (st1, st2) -> {
					%match (after) {
						NoneSt () -> {
							return elimSynchroSt (s, `st1, `Statmt (st2), before, synchro) ;
						}
						Statmt (stmt) -> {
							return elimSynchroSt (s, `st1, `Statmt (concStmt (st2, after)), before, synchro) ;
						}
					}
				}
				StSelect (stl) -> {
					if (synchro && containsSynchro (`StSelect (stl))) {
						String state = newState () ;
						ResT res = elimSynchroSt (state, `StSelect (stl), after, `NoneSt (), false) ;
						return new ResT (concStmt (before, `StTo (state)), `ConsTransList (Trans (nextLab(),state, res.stmt), res.trans)) ;
					} else
						return elimSynchroPStmList (s, `stl, after, before, synchro) ;
				}
				StCond (cond, st1, condst, ost) -> {
					ResT res1 = elimSynchroSt (s, `st1, after, before, synchro) ;
					ResTCondList res2 = elimSynchroCondStList (s, `condst, after, before, synchro) ;
					%match (ost) {
						NoneSt () -> {
							%match (after) {
								Statmt (stm) -> {
									ResT res3 = elimSynchroSt (s, `stm, `NoneSt (), before, synchro) ;
									return new ResT (`StCond (cond, res1.stmt, res2.stmt, Statmt (res3.stmt)), concTransList (res1.trans, concTransList (res2.trans, res3.trans))) ;
								}
							}
							return null ;
						}
						Statmt (stm) -> {
							ResT res3 = elimSynchroSt (s, `stm, after, before, synchro) ;
							return new ResT (`StCond (cond, res1.stmt, res2.stmt, Statmt (res3.stmt)), concTransList (res1.trans, concTransList (res2.trans, res3.trans))) ;
						}
					}
				}
				StCase (e, cel) -> {
					ResTCaseList res = elimSynchroCaseList (s, `cel, after, before, synchro) ;
					return new ResT (`StCase (e, res.stmt), res.trans) ;
				}
				StForEach (_, _, _) -> {
					System.err.println ("seqSync:124: Warning: foreach not implemented.") ;
				}
			}
		} else if (!synchro) {
			try {
				st = `BottomUp (addWait00 ()).visit (st) ;
			} catch (VisitFailure e) {
			}
		}
		%match (after) {
			NoneSt () -> {
				return new ResT (concStmt (before, st), `TransList ()) ;
			}
			Statmt (stmt) -> {
				return elimSynchroSt (s, `stmt, `NoneSt (), `Statmt (concStmt (before, st)), synchro) ;
			}
		}
		return null ;
	}

	%strategy addWait00 () extends Identity () {
		visit Statement {
			StTo (s) -> {
				return `StSequence (StWait (Closed ("0"), Closed ("0")), StTo (s)) ;
			}
		}
	}

	private static boolean containsSynchro (Statement st) {
		%match (st) {
			StSignal (_, _) || StOutput (_, _, _) << st || StInput (_, _, _, _) << st || StWait (_, _) << st -> {
				return true ;
			}
			StSequence (st1, st2) -> {
				return containsSynchro (`st1) || containsSynchro (`st2) ;
			}
			StSelect (stl) -> {
				return containsSynchro (`stl) ;
			}
			StCond (_, st1, condst, ost) -> {
				return containsSynchro (`st1) || containsSynchro (`condst) || containsSynchro (`ost) ;
			}
			StCase (_, cel) -> {
				return containsSynchro (`cel) ;
			}
		}
		return false ;
	}

	private static boolean containsSynchro (PStatementList st) {
		%match (st) {
			ConsPStmList (stmt, tl) -> {
				return containsSynchro (`stmt) || containsSynchro (`tl) ;
			}
		}
		return false ;
	}

	private static boolean containsSynchro (StatementList st) {
		%match (st) {
			ConsStmList (stmt, tl) -> {
				return containsSynchro (`stmt) || containsSynchro (`tl) ;
			}
		}
		return false ;
	}

	private static boolean containsSynchro (ConditionalStatementList st) {
		%match (st) {
			ConsCondStList (ConditionalStmt (_, stmt), tl) -> {
				return containsSynchro (`stmt) || containsSynchro (`tl) ;
			}
		}
		return false ;
	}

	private static boolean containsSynchro (Optional_Statement st) {
		%match (st) {
			Statmt (stmt) -> {
				return containsSynchro (`stmt) ;
			}
		}
		return false ;
	}

	private static boolean containsSynchro (CaseElementList st) {
		%match (st) {
			ConsCaseEL (caseElement (_, _, stmt), tl) -> {
				return containsSynchro (`stmt) || containsSynchro (`tl) ;
			}
		}
		return false ;
	}

	private static ResT elimSynchroPStmList (String s, PStatementList st, Optional_Statement after, Optional_Statement before, boolean synchro) {
		%match (st) {
			PStmList (stm) -> {
				ResTList res = elimSynchroStmList (s, `stm, after, before, synchro) ;
				return new ResT (`StSelect (PStmList (res.stmt)), res.trans) ;
			}
			ConsPStmList (stm, tl) -> {
				ResTList res = elimSynchroStmList (s, `stm, after, before, synchro) ;
				ResT res2 = elimSynchroPStmList (s, `tl, after, before, synchro) ;
				%match (res2.stmt) {
					StSelect (PStmList (lst*)) -> {
						return new ResT (`StSelect (PStmList (res.stmt, lst*)), concTransList (res.trans, res2.trans)) ;
					}
				}
				return null ;
			}
		}
		return new ResT (`StSelect (PStmList ()), `TransList ()) ;
	}

	private static ResTList elimSynchroStmList (String s, StatementList st, Optional_Statement after, Optional_Statement before, boolean synchro) {
		%match (st) {
			StmList (stm) -> {
				ResT res = elimSynchroSt (s, `stm, after, before, synchro) ;
				return new ResTList (`StmList (res.stmt), res.trans) ;
			}
			ConsStmList (hd, tl) -> {
				ResT res = elimSynchroSt (s, `hd, after, before, synchro) ;
				ResTList rec = elimSynchroStmList (s, `tl, after, before, synchro) ;
				return new ResTList (`ConsStmList (res.stmt, rec.stmt), concTransList (res.trans, rec.trans)) ;
			}
		}
		return new ResTList (`StmList (), `TransList ()) ;
	}

	private static ResTCondList elimSynchroCondStList (String s, ConditionalStatementList st, Optional_Statement after, Optional_Statement before, boolean synchro) {
		%match (st) {
			CondStList (ConditionalStmt (exp, stm)) -> {
				ResT res = elimSynchroSt (s, `stm, after, before, synchro) ;
				return new ResTCondList (`CondStList (ConditionalStmt (exp, res.stmt)), res.trans) ;
			}
			ConsCondStList (ConditionalStmt (exp, stm), tl) -> {
				ResTCondList res = elimSynchroCondStList (s, `tl, after, before, synchro) ;
				ResT res2 = elimSynchroSt (s, `stm, after, before, synchro) ;
				return new ResTCondList (`ConsCondStList (ConditionalStmt (exp, res2.stmt), res.stmt), concTransList (res2.trans, res.trans)) ;
			}
		}
		return new ResTCondList (`CondStList (), `TransList ()) ;
	}

	private static ResTCaseList elimSynchroCaseList (String s, CaseElementList cel, Optional_Statement after, Optional_Statement before, boolean synchro) {
		%match (cel) {
			ConsCaseEL (caseElement (b, p, stm), tl) -> {
				ResT res = elimSynchroSt (s, `stm, after, before, synchro) ;
				ResTCaseList res2 = elimSynchroCaseList (s, `tl, after, before, synchro) ;
				%match (res2.stmt) {
					CaseEL (stmts*) -> {
						res2.stmt = `CaseEL (caseElement (b, p, res.stmt), stmts*) ;
						res2.trans = concTransList (res.trans, res2.trans) ;
						return res2 ;
					}
				}
				return null ;
			}
		}
		return new ResTCaseList (`CaseEL (), `TransList ()) ;
	}

	private static Statement concStmt (Statement st, Optional_Statement ost) {
		%match (ost) {
			NoneSt () -> {
				return st ;
			}
			Statmt (stmt) -> {
				return `StSequence (st, stmt) ;
			}
		}
		return `StNull () ;
	}

	private static Statement concStmt (Optional_Statement ost, Statement st) {
		%match (ost) {
			NoneSt () -> {
				return st ;
			}
			Statmt (stmt) -> {
				return `StSequence (stmt, st) ;
			}
		}
		return `StNull () ;
	}

	private static Transitions concTransList (Transitions tr1, Transitions tr2) {
		%match {
			TransList (l1*) << tr1 && TransList (l2*) << tr2 -> {
				return `TransList (l1*, l2*) ;
			}
		}
		return `TransList () ;
	}

	private static String newState () {
		String state = "gseqSync_" + stateId ;
		stateId += 1 ;
		states = `ConsstringList (state, states) ;
		return state ;
	}

	private static int stateId ;

	private static StringList states ;
}
