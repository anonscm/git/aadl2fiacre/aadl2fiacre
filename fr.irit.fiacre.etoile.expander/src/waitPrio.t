/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Remplace WAIT et SORTED_WAIT dans les déclarations de priorité par
 * la liste des ports gelimWait_.* remontées par lors de l'élimination des processus étendus.
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.util.* ;
import java.util.regex.* ;

public class waitPrio {
	%include {fiacre/Fiacre.tom}
	%include {sl.tom}

	public static Program_Fiacre waitPrio (Program_Fiacre prg) throws Exception {
		try {
			return `TopDown (elimWaitPrio ()).visit (prg) ;
		} 
		catch (VisitFailure e) {
			return prg ;
		}
	}

	%strategy elimWaitPrio () extends Identity () {
		visit Declaration {
			ComponentDecl (name, genericParams, ports, params, vars, lpd, prios, init, bodyCmp) -> {
				PortDecls p = `ports ;
				waits = new LinkedList<String> () ;
				`TopDown (extractWaits ()).visit (`lpd) ;
				PrioDeclList nprios = `TopDown (replaceWAIT ()).visit (`prios) ;
				nprios = `TopDown (replaceSORTED_WAIT ()).visit (nprios) ;
				return `ComponentDecl (name, genericParams, ports, params, vars, lpd, nprios, init, bodyCmp) ;
			}
		}
	}

	%strategy extractWaits () extends Identity () {
		visit PortDecl {
			PortDec (strl, _, _, _) -> {
				Pattern pattern = Pattern.compile (".*gelimWait_.*") ;
				StringList lst = `strl ;
				while (lst != `stringList ()) {
					%match (lst) {
						ConsstringList (str, tl2) -> {
							if (pattern.matcher (`str).matches ())
								waits.add (`str) ;
							lst = `tl2 ;
						}
					}
				}
			}
		}
	}

	%strategy replaceWAIT () extends Identity () {
		visit ExpList {
			listExp (lst1*, IdentExp ("uWAIT", kind, type), lst2*) -> {
				ExpList res = `listExp (lst2*) ;
				for (String w : waits)
					res = `ConslistExp (IdentExp (w, kind, type), res) ;
				%match (res) {
					listExp (l*) -> {
						res = `listExp (lst1*, l*) ;
					}
				}
				return res ;
			}
		}
	}

	%strategy replaceSORTED_WAIT () extends Identity () {
		visit PriorDecl {
			PriorDec (lst1*, listExp (IdentExp ("uSORTED_WAIT", kind, type)), lst2*) -> {
				PriorDecl res = `PriorDec (lst2*) ;
				for (String w : waits)
					res = `ConsPriorDec (listExp (IdentExp (w, kind, type)), res) ;
				%match (res) {
					PriorDec (l*) -> {
						res = `PriorDec (lst1*, l*) ;
					}
				}
				return res ;
			}
		}
	}

	static List<String> waits ;
}
