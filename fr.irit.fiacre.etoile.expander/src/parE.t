/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.*;
import fiacre.*;
import fiacre.types.*;
import java.lang.Integer;
import java.util.Hashtable;

public class parE {

%typeterm Hashtable{
    implement 	{Hashtable}
    is_sort(t)	{ t instanceof Hashtable}
  }

%include { fiacre/Fiacre.tom }
%include { sl.tom }
	%op Strategy CollectCte(tabCte:Hashtable) {
		  make(tabCte) { new common.CollectCte(tabCte) }
	}

	public static Program_Fiacre parE (Program_Fiacre prg) throws Exception
	{
		try 
		{
			Hashtable<String,Integer> tabCte = new Hashtable<String,Integer>();	/* nom cte declare et initialise */

			/* sauvegarde des ctes */
			Program_Fiacre pf = `TopDown(CollectCte(tabCte)).visit(prg);
			/* transforme le par evolue en un par simple */
			pf = `TopDown(elimParEvo(tabCte)).visit(pf);

			return pf;
		} 
		catch(VisitFailure e) {}
	   	return prg;
	}

	/***************************************************/
	/* transforme parCompoB en parCompo                */
	/***************************************************/
	%strategy elimParEvo(tabCte:Hashtable) extends Identity()
	{
		visit ParComposition
		{
			parCompoB (id, deb, fin, ps, pcl) ->
			{
				try
				{
					/* recuperation de la borne inf. */
					Integer ideb = common.eval(`deb,tabCte);
					/* recuperation de la borne sup. */
					Integer ifin = common.eval(`fin,tabCte);
					if((ideb != null) && (ifin != null))
					{
						/* m-a-j des ports declares avant le "in" */
						PortSet psUpdate = `TopDown(majVar(id,ideb)).visit(`ps);
						/* duplication des elements contenus dans le par evolue */
						PortCompList pclUpdate = dupPortCompList(`pcl,`id,ideb,ifin);
						return `parCompo(psUpdate, TopDown(elimParEvo(tabCte)).visit(pclUpdate));
					}
					System.out.println("parEvo: "+Pretty.prettyParComposition(`parCompoB (id, deb, fin, ps, pcl)));
					System.exit(-1);
				}
				catch(Exception ex)
				{
					System.out.println("parEvo: "+Pretty.prettyParComposition(`parCompoB (id, deb, fin, ps, pcl)));
					System.exit(-1);
				}
			}
		}
	}

	/***************************************************/
	/* transforme la var. de travail en sa valeur      */
	/***************************************************/
	%strategy majVar(i:String, val:int) extends Identity()
	{
		visit Exp
		{
			IdentExp(var, _, type) && (var == i) ->
			{
				return `IntExp(String.valueOf(val), type);
			}
		}
	}

	/***************************************************/
	/* duplique un PortCompList (ifin-ideb) fois       */
	/***************************************************/
	private static PortCompList dupPortCompList(PortCompList pcl, String id, int ideb, int ifin) throws Exception
	{
		PortCompList pclUpdate = `TopDown(majVar(id,ideb)).visit(pcl);
		if(ideb == ifin)
		{
			return pclUpdate; 
		}
		return appendPortCompList(pclUpdate,dupPortCompList(pcl,id,ideb+1,ifin));
	}
	
	/******************************************************/
	/* concatene 2 PortCompList                           */
	/******************************************************/
	private static PortCompList appendPortCompList(PortCompList pcl1, PortCompList pcl2)
	{
		%match(pcl1,pcl2) 
		{
			PCList(pl1*),PCList(pl2*) -> { return `PCList(pl1,pl2); }
		}
		return null;
	}
}
