/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Permet de faire remonter des ports vérifiant une expression régulière
 * d'un composant dans le composant englobant.
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.util.* ;
import java.util.regex.* ;

public class extractPorts {
	%include {fiacre/Fiacre.tom}
	%include {sl.tom}

	public static Program_Fiacre extractPorts (Program_Fiacre prg, String pattern) throws Exception {
		try {
			extractPorts.pattern = Pattern.compile (pattern) ;
			prg = `TopDown (extractPortsStrategy ()).visit (prg) ;
			return `TopDown (insertPortsStrategy ()).visit (prg) ;
		} 
		catch (VisitFailure e) {
			return prg ;
		}
	}

	%strategy extractPortsStrategy () extends Identity () {
		visit Declaration {
			ComponentDecl (name, genericParams, ports, params, vars, lpd, prios, init, bodyCmp) -> {
				tmp = new Extracted (`lpdList (), `PrioList ()) ;
				LPDList nlpd = extractLocPorts (`lpd) ;
				PrioDeclList nprios = extractPrios (`prios) ;
				PortDecls nports = addFormalParams (`ports, tmp.ports) ;
				tmp.params = nports ;
				extracted.put (`name, tmp) ;
				return `ComponentDecl (name, genericParams, nports, params, vars, nlpd, nprios, init, bodyCmp) ;
			}
		}
	}

	private static LPDList extractLocPorts (LPDList lpd) {
		LPDList nlpd = `lpdList () ;
		while (lpd != `lpdList ()) {
			%match (lpd) {
				ConslpdList (LocPortDec (pdl, b1, b2), lptl) -> {
					PortDecls decls = `pdl ;
					PortDecls exdecls = `ListPortDecl () ;
					PortDecls npdl = `ListPortDecl () ;
					while (decls != `ListPortDecl ()) {
						%match (decls) {
							ConsListPortDecl (PortDec (strl, type, pal, chan), tl) -> {
								StringList lst = `strl ;
								StringList nstrl = `stringList () ;
								while (lst != `stringList ()) {
									%match (lst) {
										ConsstringList (str, tl2) -> {
											if (pattern.matcher (`str).matches ())
												exdecls = `ConsListPortDecl (PortDec (stringList (str), type, pal, chan), exdecls) ;
											else
												nstrl = `ConsstringList (str, nstrl) ;
											lst = `tl2 ;
										}
									}
								}
								decls = `tl ;
								if (nstrl != `stringList ())
									npdl = `ConsListPortDecl (PortDec (nstrl, type, pal, chan), npdl) ;
							}
						}
					}
					if (exdecls != `ListPortDecl ()) {
						tmp.ports = `ConslpdList (LocPortDec (exdecls, b1, b2), tmp.ports) ;
					}
					if (npdl != `ListPortDecl ())
						nlpd = `ConslpdList (LocPortDec (npdl, b1, b2), nlpd) ;
					lpd = `lptl ;
				}
			}
		}
		return nlpd ;
	}

	private static PrioDeclList extractPrios (PrioDeclList prios) {
		%match (prios) {
			PrioList (l1*, p@PriorDec (_*, listExp (_*, IdentExp (var, _, _), _*), _*), l2*) -> {
				if (pattern.matcher (`var).matches ()) {
					tmp.prios = `ConsPrioList (p, tmp.prios) ;
					return extractPrios (`PrioList (l1*, l2*)) ;
				}
			}
		}
		return prios ;
	}

	private static PortDecls addFormalParams (PortDecls ports, LPDList lpd) {
		PortDecls res = ports ;
		%match (lpd) {
			ConslpdList (LocPortDec (decls, b1, b2), tl) -> {
				%match (decls, res) {
					ListPortDecl (l1*), ListPortDecl (l2*) -> {
						res = `ListPortDecl (l1*, l2*) ;
					}
				}
				return addFormalParams (res, `tl) ;
			}
		}
		return res ;
	}

	%strategy insertPortsStrategy () extends Identity () {
		visit Declaration {
			ComponentDecl (name, genericParams, ports, params, vars, lpd, prios, init, bodyCmp) -> {
				tmp.ports = `lpd ;
				tmp.prios = `prios ;
				nbInst = 0 ;
				Composition cmp = `TopDown (instances ()).visit (`bodyCmp) ;
				LPDList nlpd = tmp.ports ;
				PrioDeclList nprios = tmp.prios ;
				return `ComponentDecl (name, genericParams, ports, params, vars, nlpd, nprios, init, cmp) ;
			}
		}
	}

	%strategy instances () extends Identity () {
		visit Instance {
			Inst (s, genericParamsEffec, strl, el) -> {
				Extracted ex = extracted.get (`s) ;
				if (ex != null) {
					LPDList ports = tmp.ports ;
					LPDList ports2 = `TopDown (addPortPrefix ("gextractPorts_" + s + nbInst + "_")).visit (ex.ports) ;
					ExpList nstrl = addParams (`strl, ports2, `s) ;
					PrioDeclList prios = tmp.prios ;
					PrioDeclList prios2 = `TopDown (addPrioPrefix ("gextractPorts_" + s + nbInst + "_", s, nstrl)).visit (ex.prios) ;
					%match (ports, ports2, prios, prios2) {
						lpdList (l1*), lpdList (l2*), PrioList (p1*), PrioList (p2*) -> {
							tmp.ports = `lpdList (l1*, l2*) ;
							tmp.prios = `PrioList (p1*, p2*) ;
						}
					}
					nbInst += 1 ;
					return `Inst (s, genericParamsEffec, nstrl, el) ;
				}
			}
		}
	}

	%strategy addPortPrefix (String prefix) extends Identity () {
		visit PortDecl {
			PortDec (stringList (name), type, pal, chan) -> {
				return `PortDec (stringList (prefix + name), type, pal, chan) ;
			}
		}
	}

	%strategy addPrioPrefix (String prefix, String process, ExpList params) extends Identity () {
		visit Exp {
			IdentExp (name, kind, type) -> {
				if (pattern.matcher (`name).matches ()) {
					return `IdentExp (prefix + name, kind, type) ;
				}
				return `IdentExp (getParamName (name, process, params), kind, type) ;
			}
		}
	}

	private static String getParamName (String name, String process, ExpList params) {
		Extracted ex = extracted.get (process) ;
		return getAtIndex (params, indexOf (name, ex.params)) ;
	}

	private static int indexOf (String name, PortDecls decls) {
		%match (decls) {
			ListPortDecl (PortDec (stringList (n), _, _, _), tl2*) -> {
				if (name.equals (`n))
					return 0 ;
				return indexOf (name, `ListPortDecl (tl2*)) + 1 ;
			}
			ListPortDecl (PortDec (ConsstringList (n, tl), t, p, c), tl2*) -> {
				if (name.equals (`n))
					return 0 ;
				return indexOf (name, `ListPortDecl (PortDec (tl, t, p, c), tl2*)) + 1 ;
			}
			ConsListPortDecl (_, tl) -> {
				return indexOf (name, `tl) + 1 ;
			}
		}
		System.err.println ("Impossible de remonter les ports wait.") ;
		System.exit (1) ;
		return 0 ;
	}

	private static String getAtIndex (ExpList exp, int idx) {
		if (idx == 0) {
			%match (exp) {
				listExp (IdentExp (name, _, _), _*) -> {
					return `name ;
				}
			}
			System.err.println ("Impossible de remonter les ports wait.") ;
			System.exit (1) ;
			return null ;
		}
		%match (exp) {
			ConslistExp (_, tl) -> {
				return getAtIndex (`tl, idx - 1) ;
			}
		}
		System.err.println ("Impossible de remonter les ports wait.") ;
		System.exit (1) ;
		return null ;
	}

	private static ExpList addParams (ExpList el, LPDList lpd, String name) {
		ExpList res = el ;
		%match (lpd) {
			ConslpdList (LocPortDec (decls, b1, b2), tl) -> {
				PortDecls d = `decls ;
				while (d != `ListPortDecl ()) {
					%match (d) {
						ConsListPortDecl (PortDec (lp, type, pal, chan), tl2) -> {
							d = `tl2 ;
							StringList strl = `lp ;
							while (strl != `stringList ()) {
								%match (strl) {
									ConsstringList (str, tl3) -> {
										res = `ConslistExp (IdentExp (str, OpenKind (), BotType ()), res) ;
										strl = `tl3 ;
									}
								}
							}
							d = `tl2 ;
						}
					}
				}
				return addParams (res, `tl, name) ;
			}
		}
		return res ;
	}

	private static class Extracted {
		public LPDList ports ;
		public PrioDeclList prios ;
		public PortDecls params ;
		public Extracted (LPDList po, PrioDeclList pr) {
			ports = po ;
			prios = pr;
		}
	}

	private static Pattern pattern ;

	private static HashMap<String, Extracted> extracted = new HashMap<String, Extracted> () ;

	private static Extracted tmp ;

	private static int nbInst ;
}
