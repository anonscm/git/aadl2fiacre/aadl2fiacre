/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.irit.fiacre.etoile.expander ;

import fiacre.*;
import fiacre.types.*;
import java_cup.runtime.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.File;
import java.net.URL;

public class Main  {


    public static void transform(Program_Fiacre pf2) {
	try
	{
	    pf2 = renaming.renaming(pf2);
	    pf2 = elimGen.elimGen(pf2);
	    pf2 = selecti.selecti(pf2);
	    pf2 = parE.parE(pf2);
	    pf2 = elimMulStates.elimMulStates(pf2);
	    pf2 = seqSync.seqSync(pf2);
	    pf2 = elimWait.elimWait(pf2);
	    pf2 = initDefault.initDefault(pf2);
	    pf2 = InitEvo.initEvo(pf2);
	    pf2 = initList.initList(pf2);
	    pf2 = multiplexage.multiplexage(pf2);
	    pf2 = chanProj.chanProj(pf2);
	    pf2 = extendedProcess.extendedProcess(pf2);
	    pf2 = extractPorts.extractPorts(pf2, "gelimWait_.*");
	    pf2 = waitPrio.waitPrio(pf2);
	    pf2 = chainPrio.chainPrio(pf2);
	    pf2 = allex.allex(pf2);
	    pf2 = tabPort.tabPort(pf2);
	    pf2 = arrayRef.arrayRef(pf2);
	    pf2 = path.path(pf2);
	    //System.out.println (pf2) ;
	    System.out.println (Pretty.prettyProgram_Fiacre(pf2));
	}
	catch (ArrayIndexOutOfBoundsException aioobe)
	    {
		System.out.println ("Pb d'argument: [commande] [nom_fichier]");
	    }
	catch (Exception e)
	    {
		e.printStackTrace();
	    }
	
    }

    public static void main(String args[]) {

        Program_Fiacre pf1 ;
        
	try
	{
            if (args[0].equals("print")) {
                pf1 = parser.parse_file(args[1]);
                System.out.println (Pretty.prettyProgram_Fiacre(pf1));
            } 
	    else if (args[0].equals("Test")) {
                pf1 = parser.parse_file(args[1]);
		transform(pf1);
            }
            else
                System.out.println ("Commande inconnue: "+args[0]);
	}
	catch (FileNotFoundException fis)
	{
		System.out.println ("Fichier inconnu: "+args[1]);
	}
	catch (ArrayIndexOutOfBoundsException aioobe)
	{
		System.out.println ("Pb d'argument: [commande] [nom_fichier]");
	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
    }
}

