/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/* 
On veut que notre parser cr�e un terme tom � partir d'un fichier
fiacre. On a donc inclu du code tom dans ce parser. Les actions
associ�es � chaque r�gle font appel aux constructeurs de termes tom
(nom du constructeur d�fini dans la grammaire gom pr�c�d� par une back-quote.
De plus on importe les types Java cr�es par gom.

Le r�sultat de la compilation de ce fichier par cup est un fichier
tom. Le code tom employ� ici n'est pas interpr�t� par cup, ce dernier
se contente de le recopier dans le code qu'il produit
*/

package fr.irit.fiacre.etoile.expander ;


import java_cup.runtime.*;
import fr.irit.fiacre.etoile.expander.Scanner ;
//import des classes java g�n�r�es par Gom
import fiacre.*;
import fiacre.types.*;


//Le code de cette partie est recopi� directemment dans la classe parser g�n�r�e par CUP
parser code {:
       
        // M�thode statique qui retourne le terme tom correspondant au fichier pass�
        // en param�tre. 
	public static  Program_Fiacre parse_file(String file) throws Exception {
	    Scanner.yyfile = file;
            parser p = new parser(new Scanner(new java.io.FileInputStream(file)));
            Symbol o =  p.parse();
            return (Program_Fiacre) o.value ;
                
	}

        // Report d'erreur du scanner, non test�
        public void report_error(String message, Object info) {
	    System.out.print(Scanner.yyfile+":");
            System.out.print(message);
            if ( !(info instanceof Symbol) ) return;
            Symbol symbol = (Symbol) info;
            if ( symbol.left < 0 || symbol.right < 0 ) return;
            System.out.println(" at line "+(symbol.left-1)+", column "+(symbol.right+1));
	    System.exit(1);
        }

        public void report_fatal_error(String message, Object info) {
	    System.out.print(Scanner.yyfile+":");
            System.out.print(message);
            if ( !(info instanceof Symbol) ) return;
            Symbol symbol = (Symbol) info;
            if ( symbol.left < 0 || symbol.right < 0 ) return;
            System.out.println(" at line "+(symbol.left-1)+", column "+(symbol.right+1));
	    System.exit(1);
        }
:}


// Directive tom permetant d'inclure la grammaire fiacre
action code {: %include { fiacre/Fiacre.tom } :}

terminal  BEGINPATTERN, ENDPATTERN, UNION, RECORD, QUEUE, PROCESS, INFTY, NOT, VAR, ANY, 
WHERE, OF, THEN, ELSE, SELECT, UNLESS, END, ARRAY, T_BOOL, T_NAT, T_INT, 
TRUE, FALSE, FULL, FIRST, LENGTH, EMPTY, DEQUEUE, ENQUEUE, APPEND,
IN, OUT, NONE, PRIORITY, PORT, READ, WRITE, NULL, DO, RCVE, AND, 		
OR, CHANNEL, TYPE, AMP, IS, MID, ADD, MOD, SUB, MUL, SHARP, AT, DIV, COERCE,
LT, LE, GE, GT, EQ, NE, SEND, TWODOT, DOT, COLON, COMMA, ALT, CIRCUM, LBRACK, 
RBRACK, LPAREN, RPAREN, LBRACE, RBRACE, COMPONENT, DBAR,
CONST, FIELD, CONSTR0, CONSTR1, LQBRACE, RQBRACE, FROM, IF, ELSIF, TO, STATES, WHILE, 
ASSIGN, SEMICOLON, ARROW, SYNC, PAR, INIT, CASE, FOREACH, LOOP, 
ON, WAIT, LTMID, MIDGT, ALL, EXISTS, PROPERTY, ASSERT, 
LTL, DEADLOCKFREE, INFINITELYOFTEN, MORTAL, PRESENT, BEFORE, AFTER, BETWEEN, UNTIL,
ABSENT, LEADSTO, PRECEDES, ALWAYS, IMP, NEXT, RELEASE, ENTER, LEAVE, STATE, VALUE, TAG, EVENT, WITHIN, LASTING, FOR, IFTRUE, IFFALSE;



terminal String NAT;
terminal String DEC;
terminal String IDENT;
terminal String STRING;

//Les types de ces non terminaux sont d�clar�s dans la grammaire Gom 
non terminal Program_Fiacre start_fiacre ;
non terminal Declarations declarations ;
non terminal Declaration constantDecl ;
non terminal Declaration extendedProcessDecl ;
non terminal Declaration componentDecl ;
non terminal PortDecls portDeclarations ;
non terminal PortDecls portDecls ;
non terminal PortDecl portDecl ;
non terminal PortAttrList portAttrs ;
non terminal ParamDecls varDeclarations ;
non terminal ParamDecls varDecls ;
non terminal ParamDecl varDecl ;
non terminal ArgList varamps ;
non terminal Arg varamp ;
non terminal VarAttrList varAttrs ;
non terminal VarDecls lvarDeclarations ;
non terminal VarDecls lvarDecls ;
non terminal VarDecl lvarDecl ;
non terminal Optional_Exp initval ;
non terminal PrioDeclList priorityDeclarations ;
non terminal PrioDeclList prioDecls ;
non terminal PriorDecl prioDecl ;
non terminal PriorDecl prioDecll ;
non terminal Bound leftb ;
non terminal Bound rightb ;
non terminal Declaration typeDecl ;
non terminal FieldsDeclList fields ;
non terminal FieldsDecl field ;
non terminal FieldsDeclList ufields ;
non terminal FieldsDecl ufield ;
non terminal Type ntype ;
non terminal Declaration channelDecl ;
non terminal Channel channel ;
non terminal TypeList types ;
non terminal Init prelude ;
non terminal Transitions transitions ;
non terminal Transition transition ;
non terminal Statement statement ;
non terminal PStatementList statements ;
non terminal StatementList astatement ;
non terminal CaseElementList matches ;
non terminal CaseElement match ;
non terminal Exp pattern ;
non terminal Exp atompatt ;
non terminal Optional_Statement elses ;
non terminal ConditionalStatementList elsifs ;
non terminal Optional_Exp wheree ;
non terminal AffectationList vfields ;
non terminal Affectation vfield ;
non terminal Exp accessexp ;
non terminal Exp accesspatt ;
non terminal ExpList patterns ;
non terminal ExpList exps ;
non terminal Exp exp ;
non terminal Exp atomexp ;
non terminal ExpList multiplex ;
non terminal StringList idents ;
non terminal StringList gidents ;
non terminal StringList identsComma ;
non terminal Exp literal ;
non terminal ParComposition parcomposition ;
non terminal PortCompList parblocks ;
non terminal PortComposition parblock ;
non terminal PortSet portset ;
non terminal Composition composition ;
non terminal LPDList locportDeclarations ;
non terminal LPDList locportDecls ;
non terminal LocPortDecl locportDecl ;
non terminal ExpList portargs ;
non terminal ExpList arguments ;
non terminal ExpList expamps ;
non terminal Exp expamp ;
non terminal Instance instance ;
non terminal StringList states ;
non terminal Statement bodySelect;

/**********************************************************************************************/
/*                             ajout de la genericite                                         */
/**********************************************************************************************/
non terminal StringList genDeclarations;
non terminal GenInsts instEff;
non terminal GenInsts insts;
non terminal GenInst inst;

/**********************************************************************************************/
/*                             ajout des tableaux de ports                                    */
/**********************************************************************************************/
non terminal ExpList expBar;


/**********************************************************************************************/
/*                             ajout des proprietes                                    */
/**********************************************************************************************/
non terminal Declaration propertyDecl;
non terminal Declaration assertion;
non terminal Declarations properties;
non terminal Property property;
non terminal Property atomic_property;
non terminal LTL ltl;
non terminal Observable observable;
non terminal Observable atomic_observable;
non terminal OPath path;
non terminal RPath rpath;
non terminal Item item;
non terminal Duration duration;

precedence right NOT, COERCE, ARROW;
precedence left RCVE, COLON;
precedence left OR;
precedence right IMP;
precedence left AND ;
precedence nonassoc UNTIL, RELEASE;
precedence left EQ, NE;
precedence left LT, LE, GT, GE ;
precedence left ADD, SUB;
precedence left MUL, DIV, MOD ;
precedence right IDENT;
precedence nonassoc FULL, EMPTY, ENQUEUE, APPEND, FIRST, LENGTH, DEQUEUE;
precedence nonassoc IF, THEN, ELSE, ELSIF, SELECT;
precedence nonassoc DOT;
precedence nonassoc LBRACK, LBRACE, LQBRACE, LPAREN;
precedence nonassoc RBRACK, RBRACE, RQBRACE, RPAREN;
precedence right MID, DBAR, ALT, SEMICOLON, COMMA, CIRCUM;

start with start_fiacre;

start_fiacre ::= declarations:decl
              {: RESULT= `Prog(decl,NoneDecl()); :}
        | declarations:decl IDENT:id properties:pl
              {: RESULT= `Prog(DeclarationList(decl*, pl*), Decl(Main(id))); :};

declarations ::= {: RESULT= `DeclarationList(); :}
               | typeDecl:tp declarations:decl
                 {: RESULT= `ConsDeclarationList(tp, decl); :}
               | channelDecl:ch declarations:decl
                 {: RESULT= `ConsDeclarationList(ch, decl); :}
               | constantDecl:ct declarations:decl
                 {: RESULT= `ConsDeclarationList(ct, decl); :}
               | extendedProcessDecl:pr declarations:decl
                 {: RESULT= `ConsDeclarationList(pr, decl); :}
               | componentDecl:ch declarations:decl
                 {: RESULT= `ConsDeclarationList(ch, decl); :}
;

properties ::=
	         {: RESULT= `DeclarationList(); :}
	       | propertyDecl:cp properties:decl
                 {: RESULT= `ConsDeclarationList(cp, decl); :}
	       | assertion:ass properties:decl
                 {: RESULT= `ConsDeclarationList(ass, decl); :};

constantDecl ::= CONST IDENT:id COLON ntype:nt IS exp:e
                 {: RESULT= `ConstantDecl(id, nt, e); :};

extendedProcessDecl ::= PROCESS IDENT: id genDeclarations:gd portDeclarations:pd varDeclarations:vd IS locportDeclarations:lpd priorityDeclarations:prd STATES states:idts lvarDeclarations:lvd prelude:p transitions:tr
                 {: RESULT = `ExtendedProcessDecl(id, gd, pd, vd, lpd, prd, idts, lvd, p, tr); :};

states::= IDENT:id			{: RESULT= `stringList(id); :}
    | IDENT:id COMMA states:ids		{: RESULT= `ConsstringList(id, ids); :} ;

componentDecl ::= COMPONENT IDENT:id genDeclarations:gd portDeclarations:pd varDeclarations:vd IS lvarDeclarations:lvd locportDeclarations:lpd priorityDeclarations:prd prelude:p composition:cp
                 {: RESULT= `ComponentDecl(id, gd, pd, vd, lvd, lpd, prd, p, cp); :};

portDeclarations::=				
         {: RESULT= `ListPortDecl(); :}
     | LBRACK portDecls:pds RBRACK			
         {: RESULT= pds; :};

portDecls::= portDecl:pd
        {: RESULT= `ListPortDecl(pd); :}
     | portDecl:pd COMMA portDecls:pds 		
        {: RESULT= `ConsListPortDecl(pd, pds); :};

portDecl::= identsComma:idc COLON portAttrs:pa channel:c		{: RESULT= `PortDec(idc,PortType(),pa,c); :}
     | identsComma:idc COLON channel:c					{: RESULT= `PortDec(idc,PortType(),PAList(),c); :}
     | identsComma:idc COLON ARRAY exp:e OF portAttrs:pa channel:c	{: RESULT= `PortDec(idc,ArrayType(e,PortType()),pa,c); :};

portAttrs::= IN	 			{: RESULT=`PAList(IN()); :}
     | OUT 				{: RESULT=`PAList(OUT()); :}
     | IN OUT				{: RESULT=`ConsPAList(IN(),PAList(OUT())); :};

varDeclarations::=				{: RESULT=`ListParamDecl(); :}
     | LPAREN varDecls:vds RPAREN		{: RESULT=vds; :};

varDecls::= varDecl:vd				{: RESULT=`ListParamDecl(vd); :}
     | varDecl:vd COMMA varDecls:vds 		{: RESULT=`ConsListParamDecl(vd,vds); :};

varDecl::= varamps:vaps COLON varAttrs:vas ntype:t	{: RESULT= `Paramdec(vaps,vas,t); :};

varamps::= varamp:va       			{: RESULT=`ArList(va); :}
    | varamp:va COMMA varamps:vas		{: RESULT=`ConsArList(va,vas); :};

varamp::= IDENT:id					
        {: RESULT=`ValArg(id); :}
    | AMP IDENT:id					
        {: RESULT=`RefArg(id); :}
    | AMP LBRACK exp:e RBRACK IDENT:id
        {: RESULT=`ArrayRefArg(id,e); :};

varAttrs::=					{: RESULT= `VAList(); :}
     | READ varAttrs:vats			{: RESULT= `ConsVAList(READ(),vats); :}
     | WRITE varAttrs:vats             		{: RESULT= `ConsVAList(WRITE(),vats); :};

lvarDeclarations::=				{: RESULT= `ListVarDecl(); :}
     | VAR lvarDecls:lvd			{: RESULT= `lvd; :};

lvarDecls::= lvarDecl:lvd			{: RESULT= `ListVarDecl(lvd); :}
     | lvarDecl:lvd COMMA lvarDecls:lvds	{: RESULT= `ConsListVarDecl(lvd,lvds); :} ;

lvarDecl::= identsComma:idc COLON ntype:t initval:iv 	
       {: RESULT= `VarDec(idc, t, iv); :} ;

identsComma::= IDENT:id	 			
          {: RESULT= `stringList(id); :}
    | IDENT:id COMMA identsComma:idc		
          {: RESULT= `ConsstringList(id, idc); :} ;

initval::=				        {: RESULT= `NoneExp(); :}
     | ASSIGN exp:e				{: RESULT= `OptExp(e); :} ;

priorityDeclarations::= 		        {: RESULT= `PrioList(); :}
     | PRIORITY prioDecls:prd 			{: RESULT= prd ; :} ;

prioDecls::= prioDecl:prd	 		{: RESULT= `PrioList(prd); :}
     | prioDecl:prd COMMA prioDecls:prds	{: RESULT= `ConsPrioList(prd,prds); :} ;

prioDecl::= expBar:id1 GT expBar:id2 prioDecll:lst
        {: RESULT = `ConsPriorDec(id1,ConsPriorDec(id2,lst)); :}
     | expBar:id {: RESULT = `PriorDec(id); :} ;

prioDecll::= GT expBar:id prioDecll:lst		{: RESULT = `ConsPriorDec(id, lst); :}
    | {: RESULT = `PriorDec(); :} ;

expBar::= accessexp:id	 			
         {: RESULT= `listExp(id); :}
    | accessexp:id MID expBar:ids		
         {: RESULT= `ConslistExp(id, ids); :} ;

locportDeclarations::= 				{: RESULT= `lpdList(); :}
     | PORT locportDecls:lpds	 		{: RESULT= lpds; :} ;

locportDecls::= portDecls:pds		      		
       {: RESULT= `lpdList(LocPortDec(pds,Closed("0.0"),Infinite())); :}
     | locportDecl:lpd				      		
       {: RESULT= `lpdList(lpd); :}
     | locportDecl:lpd COMMA locportDecls:lpds 	   		
       {: RESULT= `ConslpdList(lpd,lpds); :} ;	

locportDecl::= portDecls:pds IN leftb:l COMMA rightb:r 	
       {: RESULT= `LocPortDec(pds, l, r); :} ;

leftb::= RBRACK DEC:d		{: RESULT= `Open((String) d); :}
    | LBRACK DEC:d		{: RESULT= `Closed((String) d); :}
    | RBRACK NAT:n		{: RESULT= `Open((String) n); :}
    | LBRACK NAT:n		{: RESULT= `Closed((String) n); :}
    | RBRACK IDENT:n		{: RESULT= `Open((String) n); :}
    | LBRACK IDENT:n		{: RESULT= `Closed((String) n); :} ;

rightb::= DEC:d RBRACK		{: RESULT= `Closed((String) d); :}
    | DEC:d LBRACK		{: RESULT= `Open((String) d); :}
    | NAT:n RBRACK		{: RESULT= `Closed((String) n); :}
    | NAT:n LBRACK		{: RESULT= `Open ((String) n); :}
    | IDENT:n RBRACK		{: RESULT= `Closed((String) n); :}
    | IDENT:n LBRACK		{: RESULT= `Open ((String) n); :}
    | INFTY LBRACK              {: RESULT= `Infinite(); :} ;

duration::= NAT:d		{: RESULT= `duration((String) d); :};

prelude::= 					{: RESULT= `NoneInit(); :}
    | INIT statement:st				{: RESULT= `Initial (st); :} ;

typeDecl::= TYPE IDENT:id IS ntype:t 		
        {: RESULT= `TypeDecl(id, t); :} ;

gidents::= CONST IDENT:id			{: RESULT= `stringList(id); :}
    | TYPE IDENT:id				{: RESULT= `stringList(id); :}
    | FIELD IDENT:id				{: RESULT= `stringList(id); :}
    | CONSTR0 IDENT:id				{: RESULT= `stringList(id); :}
    | CONSTR1 IDENT:id				{: RESULT= `stringList(id); :}
    | CONST IDENT:id COMMA gidents:ids		{: RESULT= `ConsstringList(id, ids); :}
    | TYPE IDENT:id COMMA gidents:ids		{: RESULT= `ConsstringList(id, ids); :}
    | FIELD IDENT:id COMMA gidents:ids		{: RESULT= `ConsstringList(id, ids); :}
    | CONSTR0 IDENT:id COMMA gidents:ids	{: RESULT= `ConsstringList(id, ids); :}
    | CONSTR1 IDENT:id COMMA gidents:ids	{: RESULT= `ConsstringList(id, ids); :} ;

idents::= IDENT:id			{: RESULT= `stringList(id); :}
    | IDENT:id COMMA idents:ids		{: RESULT= `ConsstringList(id, ids); :} ;

fields::= field:fd		    			{: RESULT= `fieldList(fd); :} 
    | field:fd COMMA fields:fds     			{: RESULT= `ConsfieldList(fd,fds); :} ; 

field::= idents:ids COLON ntype:t			{: RESULT= `field(ids,t); :} ;

ufields::= ufield:uf		    			{: RESULT= `fieldList(uf); :} 
    | ufield:uf MID ufields:ufs     			{: RESULT= `ConsfieldList(uf,ufs); :} ; 

ufield::= idents:ids	                  	{: RESULT= `field(ids,BotType()); :}
    | idents:ids OF ntype:t	   	        {: RESULT= `field(ids,t); :} ;

ntype::= T_BOOL 				{: RESULT= `BoolType(); :}
    | T_NAT         				{: RESULT= `NatType(); :}
    | T_INT 	    				{: RESULT= `IntType(); :}
    | IDENT:id 					{: RESULT= `NamedType(id); :}
    | LBRACK exp:exp1 TWODOT exp:exp2 RBRACK	{: RESULT= `IntervalType (exp1,exp2); :}
    | UNION ufields:ufs END             	{: RESULT= `UnionType(ufs); :}
    | UNION ufields:ufs END UNION           	{: RESULT= `UnionType(ufs); :}
    | RECORD fields:fds END          		{: RESULT= `RecordType(fds); :}
    | RECORD fields:fds END RECORD         	{: RESULT= `RecordType(fds); :}
    | QUEUE exp:e OF ntype:t	     		{: RESULT= `QueueType(e,t); :}
    | ARRAY exp:e OF ntype:t	     		{: RESULT= `ArrayType(e,t); :};

channelDecl ::= CHANNEL IDENT:id IS channel:c		
        {: RESULT=`ChannelDecl(id,c); :} ;

/* IDENT case omitted (would be ambiguous).
   channel names parsed as type names, typer will find out the exact kinds.
*/
channel::= NONE					{: RESULT= `ProfileChannel(profile(Type_list())); :}
    | types:ts					{: RESULT= `ProfileChannel(profile(ts)); :} ;

types::= ntype:t 				{: RESULT= `Type_list(t); :}
    | ntype:t SHARP types:ts			{: RESULT= `ConsType_list(t,ts); :} ;

/* transitions */

transitions::= transition:tr 			{: RESULT= `TransList(tr); :}
    | transition:tr transitions:trs 		{: RESULT= `ConsTransList(tr,trs); :} ;

transition::=
      FROM IDENT:id statement:st        	{: RESULT= `Trans("", id ,st); :};

/*    | IDENT:l COLON FROM IDENT:id statement:st {: RESULT= `Trans(l,id ,st); :} ; */

statement::=
      NULL                              	
        {: RESULT= `StNull(); :}
    | AT IDENT:id {: RESULT = `StTag(id); :}
    | patterns:p ASSIGN exps:e 			
        {: RESULT= `StAssign(p,e); :}
    | patterns:p ASSIGN ANY wheree:w 		
        {: RESULT= `StAny(p, Type_list(), w); :}
    | IF exp:e THEN statement:st elsifs:ef elses:es 	
        {: RESULT= `StCond(e, st, ef, es); :}
    | SELECT bodySelect:bs END 			
        {: RESULT= bs; :}
    | SELECT bodySelect:bs END SELECT 				
        {: RESULT= bs; :}		
    | WHILE exp:e DO statement:st END 		
        {: RESULT= `StWhile(e,st); :}
    | WHILE exp:e DO statement:st END WHILE 		
        {: RESULT= `StWhile(e,st); :} 		
    | FOREACH IDENT:id DO statement:st END            
        {: RESULT= `StForEach(id , BotType(),st); :}
    | FOREACH IDENT:id DO statement:st END FOREACH    
        {: RESULT= `StForEach( id , BotType(),st); :}
    | TO IDENT:id 					
        {: RESULT= `StTo(id); :}
    | statement:statement1 SEMICOLON statement:statement2 		
        {: RESULT= `StSequence(statement1,statement2); :}
    | accesspatt:ap					
        {: RESULT= `StSignal(ap, ProfileChannel(profile(Type_list()))); :}
    | accesspatt:ap RCVE patterns:p wheree:w 		
        {: RESULT= `StInput(ap, ProfileChannel(profile(Type_list())), p, w); :}
    | accesspatt:ap SEND exps:e          			
        {: RESULT= `StOutput(ap, ProfileChannel(profile(Type_list())), e); :}
    | CASE exp:e OF matches:m END                   
        {: RESULT= `StCase(e,m); :}
    | CASE exp:e OF matches:m END CASE            
        {: RESULT= `StCase(e,m); :}
    | LOOP
        {: RESULT= `StLoop(); :} 
    | ON exp:e
        {: RESULT= `StOn(e); :} 
    | WAIT leftb:b1 COMMA rightb:b2
        {: RESULT= `StWait(b1,b2); :} ;

bodySelect::=  
      statements:st				
	{: RESULT= `StSelect(st); :}
    | statements:st MID IDENT:var IN LBRACK exp:deb TWODOT exp:fin RBRACK 
        {: RESULT= `StSelectIndexe(var,deb,fin,st); :}
    | statements:st MID IDENT:var IN LBRACK exp:lim RBRACK
        {: RESULT= `StSelectIndexe(var,IntExp ("0",BotType()),Infix(SUB(), BotType(), lim, IntExp ("1",BotType())),st); :};

statements::= astatement:st	 		{: RESULT= `PStmList(st); :}
    | astatement:st UNLESS statements:sts	{: RESULT= `ConsPStmList(st,sts); :}; 
astatement::= statement:st 			{: RESULT= `StmList(st); :}
    | statement:st ALT astatement:sts		{: RESULT= `ConsStmList(st,sts); :} ; 

elses::= END					{: RESULT= `NoneSt(); :}
    | END IF					{: RESULT= `NoneSt(); :}
    | ELSE statement:st END			{: RESULT= `Statmt(st); :}
    | ELSE statement:st END IF			{: RESULT= `Statmt(st); :} ;

elsifs::= 					
      {: RESULT= `CondStList(); :}
    | ELSIF exp:e THEN statement:st elsifs:efs		
      {: RESULT= `ConsCondStList(ConditionalStmt(e,st),efs); :} ;

wheree::= 					{: RESULT= `NoneExp(); :}
    | WHERE exp:e				{: RESULT= `OptExp(e); :} ;

matches::= match:m
        {: RESULT= `CaseEL(m); :}
    | match:m MID matches:ms      
        {: RESULT= `ConsCaseEL(m, ms); :} ;

match::= pattern:p ARROW statement:st                  
    {: RESULT= `caseElement(true,p,st); :} ;

atompatt::= literal:l                           {: RESULT= l; :}
    | ANY                                       {: RESULT= `AnyExp(); :}
    | IDENT:id					
     {: RESULT= `IdentExp(id, OpenKind(), BotType()); :}
    | LPAREN pattern:p RPAREN                     {: RESULT= `ParenExp(p); :} ;

accesspatt::= atompatt:at
      {: RESULT= at; :}
    | accesspatt:ac LBRACK exp:e RBRACK 		
      {: RESULT= `ArrayAccessExp(ac,e,BotType()); :} 
    | accesspatt:ac DOT IDENT:id 			
      {: RESULT= `RecordAccessExp(ac,id,BotType()); :} ;

/* adding SUB,ADD rather than in atomexp avoids shift/reduce conflicts for exp */
pattern::= accesspatt:ac                        {: RESULT= ac; :}
    | SUB NAT:n                                 {: RESULT= `IntExp ("-"+n, BotType()); :}
    | ADD NAT:n                                 {: RESULT= `IntExp (n, BotType()); :}
    | IDENT:id atompatt:at                      {: RESULT= `ConstrExp(id,at,BotType()); :}; 

patterns::= pattern:p	 			{: RESULT= `listExp(p); :}
    | pattern:p COMMA patterns:ps 		{: RESULT= `ConslistExp(p,ps); :} ;

composition::=
      PAR parcomposition:pc END			
        {: RESULT= `ParComp(pc); :}
    | PAR parcomposition:pc END PAR		
        {: RESULT= `ParComp(pc); :} ;		

instance::= IDENT:id portargs:pas arguments:as		
        {: RESULT= `Inst(id,GenInstList(),pas,as); :}
    | IDENT:id instEff:ie portargs:pas arguments:as		
        {: RESULT= `Inst(id,ie,pas,as); :} ;

parcomposition::= parblocks:pa			
       {: RESULT= `parCompo(SomePorts(listExp()), pa); :}
    | SYNC portset:ps IN parblocks:pa MID IDENT:id IN LBRACK exp:d TWODOT exp:l RBRACK 
       {: RESULT= `parCompoB(id,d,l,ps, pa); :}
    | SYNC portset:ps IN parblocks:pa MID IDENT:id IN LBRACK exp:lim RBRACK 
       {: RESULT= `parCompoB(id,IntExp ("0", BotType()),Infix(SUB(), BotType(), lim, IntExp ("1",BotType())),ps, pa); :}
    | SYNC portset:ps IN parblocks:pa			
       {: RESULT= `parCompo(ps, pa); :} ;
    
parblocks::= parblock:pa			{: RESULT= `PCList(pa); :}
    | parblock:pa DBAR parblocks:pas		{: RESULT= `ConsPCList(pa,pas); :} ;

parblock::= portset:ps ARROW composition:c	 	
     {: RESULT= `PortComp(ps,c); :}
    | ARROW composition:c
     {: RESULT= `PortComp(SomePorts(listExp()),c); :}
    | portset:ps ARROW instance:i			
     {: RESULT= `PortComp(ps,InstanceComp(i)); :}
    | ARROW instance:i	    	 		 	
     {: RESULT= `PortComp(SomePorts(listExp()),InstanceComp(i)); :} ;

portset::=					{: RESULT= `SomePorts(listExp()); :}
    | MUL					{: RESULT= `AllPorts(); :}
    | expamps:aes				{: RESULT= `SomePorts(aes); :} ;

portargs::=					{: RESULT= `listExp(); :}
    | LBRACK expamps:aes RBRACK			{: RESULT= aes; :} ;

arguments::=					{: RESULT= `listExp(); :}
    | LPAREN expamps:ex RPAREN			{: RESULT= ex; :} ;

expamps::= expamp:exam				{: RESULT= `listExp(exam); :}
    | expamp:exam COMMA expamps:exams		{: RESULT= `ConslistExp(exam,exams); :} ;

expamp::= exp:e					{: RESULT= e; :}
    | AMP IDENT:id				{: RESULT= `RefExp(id,BotType()); :} ;

exps::= exp:e					{: RESULT= `listExp(e); :}
    | exp:e COMMA exps:es			{: RESULT= `ConslistExp(e,es); :} ;

exp::=  accessexp:ae 				
      {: RESULT= ae; :}
    | LBRACE vfields:vf RBRACE			
      {: RESULT= `RecordExp(vf,BotType()); :}
    | LBRACK expamps:es RBRACK			
      {: RESULT= `ArrayExp (es, BotType()); :}
    | LQBRACE RQBRACE				
      {: RESULT= `QueueExp (listExp(),BotType()); :}
    | LQBRACE exps:es RQBRACE			
      {: RESULT= `QueueExp (es, BotType()); :}
    | ENQUEUE LPAREN exp:exp1 COMMA exp:exp2 RPAREN 	
      {: RESULT= `Binop(ENQUEUE(), BotType(),exp1,exp2); :}
    | APPEND LPAREN exp:exp1 COMMA exp:exp2 RPAREN 	
      {: RESULT= `Binop(APPEND(), BotType(),exp1,exp2); :}
    | FIRST atomexp:ae         			
      {: RESULT= `Unop(FIRST(),BotType(),ae); :}
    | LENGTH atomexp:ae
      {: RESULT= `Unop(LENGTH(),BotType(),ae); :}
    | ADD atomexp:ae           			
      {: RESULT= `Unop(PLUS(), BotType(),ae); :}
    | SUB atomexp:ae           			
      {: RESULT= `Unop(MINUS(), BotType(),ae); :}
    | COERCE atomexp:ae           			
      {: RESULT= `Unop(COERCE(), BotType(),ae); :}
    | NOT atomexp:ae 				
      {: RESULT= `Unop(NOT(), BotType(),ae); :}
    | FULL atomexp:ae          			
      {: RESULT= `Unop(FULL(), BotType(),ae); :}
    | DEQUEUE atomexp:ae       			
      {: RESULT= `Unop(DEQUEUE(), BotType(), ae); :}
    | EMPTY atomexp:ae 				
      {: RESULT= `Unop(EMPTY(), BotType(), ae); :}
    | IDENT:id atomexp:ae                             
      {: RESULT= `ConstrExp (id, ae, BotType()); :}
    | exp:exp1 RCVE exp:exp2 COLON exp:exp3			
      {: RESULT= `CondExp(exp1,exp2,exp3, BotType()); :}
    | exp:exp1 AND exp:exp2 				
      {: RESULT= `Infix(AND(), BotType(),exp1,exp2); :}
    | exp:exp1 OR exp:exp2 				
      {: RESULT= `Infix(OR(), BotType(),exp1,exp2); :}
    | exp:exp1 LE exp:exp2 				
      {: RESULT= `Infix(LE(), BotType(),exp1,exp2); :}
    | exp:exp1 LT exp:exp2 				
      {: RESULT= `Infix(LT(), BotType(),exp1,exp2); :}
    | exp:exp1 GT exp:exp2 				
      {: RESULT= `Infix(GT(), BotType(),exp1,exp2); :}
    | exp:exp1 GE exp:exp2 				
      {: RESULT= `Infix(GE(), BotType(),exp1,exp2); :}
    | exp:exp1 EQ exp:exp2 				
      {: RESULT= `Infix(EQ(), BotType(),exp1,exp2); :}
    | exp:exp1 NE exp:exp2 				
      {: RESULT= `Infix(NE(), BotType(),exp1,exp2); :}
    | exp:exp1 ADD exp:exp2 				
      {: RESULT= `Infix(ADD(), BotType(),exp1,exp2); :}
    | exp:exp1 SUB exp:exp2 				
      {: RESULT= `Infix(SUB(), BotType(),exp1,exp2); :}
    | exp:exp1 MOD exp:exp2 				
      {: RESULT= `Infix(MOD(), BotType(),exp1,exp2); :}
    | exp:exp1 DIV exp:exp2 				
      {: RESULT= `Infix(DIV(), BotType(),exp1,exp2); :}
    | exp:exp1 MUL exp:exp2 				
      {: RESULT= `Infix(MUL(), BotType(),exp1,exp2); :}
    | LBRACK exp:e MID IDENT:i IN LBRACK exp:n RBRACK RBRACK
      {: RESULT= `InitListExp(i, n, e); :}
    | exp:chan CIRCUM exp:field
      {: RESULT= `ChanProjExp(chan, listExp(field)); :}
    | exp:chan CIRCUM LBRACE exps:fields RBRACE
      {: RESULT= `ChanProjExp(chan, fields); :}
    | ALL IDENT:i IN LBRACK exp:lim RBRACK DOT exp:e
      {: RESULT= `AllExp(i, IntExp ("0",BotType ()), Infix (SUB (), BotType (), lim, IntExp ("1", BotType ())), e); :}
    | ALL IDENT:i IN LBRACK exp:begin TWODOT exp:end RBRACK DOT exp:e
      {: RESULT= `AllExp(i, begin, end, e); :}
    | EXISTS IDENT:i IN LBRACK exp:lim RBRACK DOT exp:e
      {: RESULT= `ExistsExp(i, IntExp ("0",BotType ()), Infix (SUB (), BotType (), lim, IntExp ("1", BotType ())), e); :}
    | EXISTS IDENT:i IN LBRACK exp:begin TWODOT exp:end RBRACK DOT exp:e
      {: RESULT= `ExistsExp(i, begin, end, e); :}
    | IDENT:i ALT multiplex:el
      {: RESULT= `MultExp(ConslistExp(IdentExp(i, OpenKind(), BotType()), el)); :} ;

multiplex::= IDENT:i ALT multiplex:el
      {: RESULT= `ConslistExp(IdentExp(i, OpenKind(), BotType()), el); :}
    | IDENT:i
      {: RESULT= `listExp(IdentExp(i, OpenKind(), BotType())); :} ;

literal::= NAT:n				{: RESULT= `IntExp(n, BotType()); :}
    | TRUE 					{: RESULT= `BoolExp(true); :}
    | FALSE 					{: RESULT= `BoolExp(false); :} ;

atomexp::= literal:l                                
      {: RESULT=l; :}
    | IDENT:id                                     
      {: RESULT=`IdentExp (id, OpenKind(),BotType()); :}
    | LPAREN exp:e RPAREN                         
      {: RESULT= `ParenExp(e); :} ;

accessexp::= atomexp:ae 				
      {: RESULT= ae; :}
    | accessexp:ae LBRACK exp:e RBRACK
      {: RESULT= `ArrayAccessExp (ae,e,BotType()); :}
    | accessexp:ae DOT IDENT:id 			
      {: RESULT= `RecordAccessExp (ae,id,BotType()); :} ;

vfields::= vfield:vf					{: RESULT= `AffList(vf); :}
    | vfield:vf COMMA vfields:vfs			{: RESULT= `ConsAffList(vf, vfs); :} ;

vfield::= IDENT:id EQ exp:e				{: RESULT= `Affect(id,e); :} ;

/***********************************************************************************************/
/*                                       ajout de la genericite                                */
/***********************************************************************************************/
genDeclarations::=					{: RESULT= `stringList(); :}
    | LTMID gidents:gds MIDGT				{: RESULT= gds; :};

instEff::= LTMID insts:te MIDGT 			{: RESULT= `te; :};

inst::=
      TYPE ntype:t 					{: RESULT= `TypeInst(t); :}
    | CONST NAT:n 					{: RESULT= `ConstInst(n); :}
    | CONST IDENT:n 					{: RESULT= `ConstInst(n); :}
// I think that a FIELD construct is missing
    | CONSTR0 IDENT:n 					{: RESULT= `Constr0Inst(n); :}
    | CONSTR1 IDENT:n 					{: RESULT= `Constr1Inst(n); :}
;

insts::= 
      inst:i	 					{: RESULT= `GenInstList(i); :}
    | inst:i COMMA insts:ts				{: RESULT= `ConsGenInstList(i,ts); :}
;

/***********************************************************************************************/
/*                                       ajout des proprietes                                */
/***********************************************************************************************/

propertyDecl ::=
      PROPERTY IDENT:n IS property:p {: RESULT= `PropertyDecl(n,p); :};

assertion ::=
      ASSERT IDENT:n {: RESULT= `Assert(n, "", ""); :}
    | ASSERT IDENT:n IFTRUE STRING:ift {: RESULT= `Assert(n, ift, ""); :}
    | ASSERT IDENT:n IFFALSE STRING:iff {: RESULT= `Assert(n, "", iff); :}
    | ASSERT IDENT:n IFTRUE STRING:ift IFFALSE STRING:iff {: RESULT= `Assert(n, ift, iff); :};

atomic_property ::=
      LTL ltl:l  {: RESULT= `PropLTL(l); :}
    | DEADLOCKFREE {: RESULT= `PropO("deadlockfree"); :}
    | INFINITELYOFTEN observable:o {: RESULT= `PropOx("infinitelyoften",o); :}
    | MORTAL observable:o {: RESULT= `PropOx("mortal",o); :}
    | PRESENT observable:o {: RESULT= `PropOx("present",o); :}
    | PRESENT observable:o LASTING duration:d {: RESULT= `PropOxOd("present",o,"lasting",d); :}
    | PRESENT observable:o1 AFTER observable:o2 {: RESULT= `PropOxOx("present",o1,"after", o2); :}
    | PRESENT observable:o1 AFTER observable:o2 WITHIN leftb:lb COMMA rightb:rb {: RESULT= `PropOxOxOI("present",o1,"after", o2, "within", lb, rb); :}
    | PRESENT observable:o1 BEFORE observable:o2 {: RESULT= `PropOxOx("present",o1,"before", o2); :}
    | PRESENT observable:o1 BEFORE observable:o2 WITHIN leftb:lb COMMA rightb:rb {: RESULT= `PropOxOxOI("present",o1,"before", o2, "within", lb, rb); :}
    | PRESENT observable:o1 BETWEEN atomic_observable:o2 AND atomic_observable:o3 {: RESULT= `PropOxOxOx("present",o1,"between", o2, "and", o3); :}
    | PRESENT observable:o1 AFTER observable:o2 UNTIL observable:o3 {: RESULT= `PropOxOxOx("present",o1,"after", o2, "until", o3); :}
    | ABSENT observable:o {: RESULT= `PropOx("absent",o); :}
    | ABSENT observable:o1 AFTER observable:o2 {: RESULT= `PropOxOx("absent",o1,"after", o2); :}
    | ABSENT observable:o1 AFTER observable:o2 WITHIN leftb:lb COMMA rightb:rb {: RESULT= `PropOxOxOI("absent",o1,"after", o2,"within",lb,rb); :}
    | ABSENT observable:o1 BEFORE observable:o2 {: RESULT= `PropOxOx("absent",o1,"before", o2); :}
    | ABSENT observable:o1 BEFORE observable:o2 FOR duration:d {: RESULT= `PropOxOxOd("absent",o1,"before", o2, "for",d); :}
    | ABSENT observable:o1 BETWEEN atomic_observable:o2 AND atomic_observable:o3 {: RESULT= `PropOxOxOx("absent",o1,"between", o2, "and", o3); :}
    | ABSENT observable:o1 AFTER observable:o2 UNTIL observable:o3 {: RESULT= `PropOxOxOx("absent",o1,"after", o2, "until", o3); :}
    | observable:o1 LEADSTO observable:o2 {: RESULT= `PropxOx(o1,"leadsto",o2); :}
    | observable:o1 LEADSTO observable:o2 WITHIN leftb:lb COMMA rightb:rb {: RESULT= `PropxOxOI(o1,"leadsto",o2, "within", lb,rb); :}
    | observable:o1 LEADSTO observable:o2 BEFORE observable:o3 WITHIN leftb:lb COMMA rightb:rb {: RESULT= `PropxOxOxOI(o1,"leadsto",o2, "before", o3, "within", lb,rb); :}
    | observable:o1 LEADSTO observable:o2 AFTER observable:o3 WITHIN leftb:lb COMMA rightb:rb  {: RESULT= `PropxOxOxOI(o1,"leadsto",o2, "after",o3, "within", lb,rb); :}
    | observable:o1 LEADSTO observable:o2 BEFORE observable:o3 {: RESULT= `PropxOxOx(o1,"leadsto",o2,"before",o3); :}
    | observable:o1 LEADSTO observable:o2 AFTER observable:o3 {: RESULT= `PropxOxOx(o1,"leadsto",o2,"after",o3); :}
    | observable:o1 LEADSTO observable:o2 BETWEEN atomic_observable:o3 AND atomic_observable:o4 {: RESULT= `PropxOxOxOx(o1,"leadsto",o2,"between",o3,"and",o4); :}
    | observable:o1 LEADSTO observable:o2 AFTER observable:o3 UNTIL observable:o4 {: RESULT= `PropxOxOxOx(o1,"leadsto",o2,"after",o3,"until",o4); :}
    | ALWAYS observable:o {: RESULT= `PropOx("always",o); :}
    | ALWAYS observable:o1 AFTER observable:o2 {: RESULT= `PropOxOx("always",o1,"after", o2); :}
    | ALWAYS observable:o1 BEFORE observable:o2 {: RESULT= `PropOxOx("always",o1,"before", o2); :}
    | ALWAYS observable:o1 BETWEEN atomic_observable:o2 AND atomic_observable:o3 {: RESULT= `PropOxOxOx("always",o1,"between", o2, "and", o3); :}
    | ALWAYS observable:o1 AFTER observable:o2 UNTIL observable:o3 {: RESULT= `PropOxOxOx("always",o1,"after",o2,"until",o3); :}
    | observable:o1 PRECEDES observable:o2 {: RESULT= `PropxOx(o1,"precedes",o2); :}
    | observable:o1 PRECEDES observable:o2 BEFORE observable:o3 {: RESULT= `PropxOxOx(o1,"precedes",o2,"before",o3); :}
    | observable:o1 PRECEDES observable:o2 AFTER observable:o3 {: RESULT= `PropxOxOx(o1,"precedes",o2,"after",o3); :}
    | observable:o1 PRECEDES observable:o2 BETWEEN atomic_observable:o3 AND atomic_observable:o4 {: RESULT= `PropxOxOxOx(o1,"precedes",o2,"between",o3,"and",o4); :}
    | observable:o1 PRECEDES observable:o2 AFTER observable:o3 UNTIL observable:o4 {: RESULT= `PropxOxOxOx(o1,"precedes",o2,"after",o3,"until",o4); :}
;

property ::=
      BEGINPATTERN atomic_property:a ENDPATTERN {: RESULT= a; :}
    | property:p1 AND property:p2 {: RESULT= `PropBin(p1,"and",p2); :}
    | property:p1 OR property:p2 {: RESULT= `PropBin(p1,"or",p2); :}
    | property:p1 IMP property:p2 {: RESULT= `PropBin(p1,"=>",p2); :}
    | NOT property:p {: RESULT= `PropNot(p); :}
    | LPAREN property:p RPAREN {: RESULT= `PropPar(p); :}
    | ALL IDENT:i IN LBRACK exp:lim RBRACK DOT property:p
      {: RESULT= `PropAll(i, IntExp ("1",BotType ()), lim, p); :}
    | ALL IDENT:i IN LBRACK exp:begin TWODOT exp:end RBRACK DOT property:p
      {: RESULT= `PropAll(i, begin, end, p); :}
    | EXISTS IDENT:i IN LBRACK exp:lim RBRACK DOT property:p
      {: RESULT= `PropEx(i, IntExp ("1",BotType ()), lim, p); :}
    | EXISTS IDENT:i IN LBRACK exp:begin TWODOT exp:end RBRACK DOT property:p
      {: RESULT= `PropEx(i, begin, end, p); :}
;

ltl ::=
      atomic_observable:o  {: RESULT= `LtlObs(o); :}
    | IDENT:id {: RESULT= `LtlAtom(id); :}
    | ALT ltl:l  {: RESULT= `LtlUnary("[]",l); :}
    | NE ltl:l  {: RESULT= `LtlUnary("<>",l); :}
    | NEXT ltl:l  {: RESULT= `LtlUnary("()",l); :}
    | NOT ltl:l  {: RESULT= `LtlUnary("not",l); :}
    | ltl:l1 AND ltl:l2 {: RESULT= `LtlBinary(l1,"and",l2); :}
    | ltl:l1 OR ltl:l2 {: RESULT= `LtlBinary(l1,"or",l2); :}
    | ltl:l1 IMP ltl:l2 {: RESULT= `LtlBinary(l1,"=>",l2); :}
    | ltl:l1 UNTIL ltl:l2 {: RESULT= `LtlBinary(l1,"until",l2); :}
    | ltl:l1 RELEASE ltl:l2 {: RESULT= `LtlBinary(l1,"release",l2); :}
    | LPAREN ltl:l RPAREN {: RESULT= `LtlPar(l); :}
    | ALL IDENT:i IN LBRACK exp:lim RBRACK DOT ltl:l
      {: RESULT= `LtlAll(i, IntExp ("1",BotType ()), lim, l); :}
    | ALL IDENT:i IN LBRACK exp:begin TWODOT exp:end RBRACK DOT ltl:l
      {: RESULT= `LtlAll(i, begin, end, l); :}
    | EXISTS IDENT:i IN LBRACK exp:lim RBRACK DOT ltl:l
      {: RESULT= `LtlEx(i, IntExp ("1",BotType ()), lim, l); :}
    | EXISTS IDENT:i IN LBRACK exp:begin TWODOT exp:end RBRACK DOT ltl:l
      {: RESULT= `LtlEx(i, begin, end, l); :}
;

atomic_observable ::=
      path:p item:i {: RESULT= `ObsItem(p,i); :}
    | ENTER observable:o  {: RESULT= `ObsUnary("enter", o); :}
    | LEAVE observable:o  {: RESULT= `ObsUnary("leave", o); :}
;

observable ::=
      atomic_observable:a {: RESULT= a; :}
    | NOT observable:o  {: RESULT= `ObsUnary("not", o); :}
    | observable:o1 AND observable:o2 {: RESULT= `ObsBinary(o1,"and",o2); :}
    | observable:o1 OR observable:o2 {: RESULT= `ObsBinary(o1,"or",o2); :}
    | observable:o1 IMP observable:o2 {: RESULT= `ObsBinary(o1,"=>",o2); :}
    | LPAREN observable:o RPAREN {: RESULT= `ObsPar(o); :}
;

item ::=
      STATE IDENT:i {: RESULT= `IState(i); :}
    | VALUE exp:e {: RESULT= `IValue(e); :}
    | TAG IDENT:i {: RESULT= `ITag(i); :}
    | EVENT IDENT:i {: RESULT= `IEvent(i); :}
;

path ::=
       IDENT:i DIV rpath:p  {: RESULT= `OPathFrom(i,p); :}
;

rpath ::=
        {: RESULT= `IndexList(); :}
    | IDENT:i DIV rpath:p {: RESULT= `ConsIndexList(Cident(i), p); :}
    | NAT:i DIV rpath:p  {: RESULT= `ConsIndexList(Cnum(i), p); :}
;
