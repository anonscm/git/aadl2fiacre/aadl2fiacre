/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Elimine les processus "étendus" (qui contiennent des ports locaux et des priorités)
 * et les remplace par un processus classique et un composant.
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.util.* ;

public class extendedProcess {
	%include {fiacre/Fiacre.tom}
	%include {sl.tom}

	public static Program_Fiacre extendedProcess (Program_Fiacre prg) throws Exception {
		try {
			processes = `DeclarationList () ;
			prg = `TopDown (elimExtProc ()).visit (prg) ;
			%match (prg, processes) {
				Prog (DeclarationList (decls*), main), DeclarationList (procs*) -> {
					return `Prog (DeclarationList (procs*, decls*), main) ;
				}
			}
		} 
		catch (VisitFailure e) {
		}
		return prg ;
	}

	%strategy elimExtProc () extends Identity () {
		visit Declaration {
			eproc@ExtendedProcessDecl (name, genericParams, ports, params, lpd, prios, states, vars, init, trans) -> {
				if (`lpd != `lpdList () || `prios != `PrioList ()) {
					Declaration proc = `ProcessDecl ("gextendedProcess_" + name, genericParams, genProcessPortsDecls (ports, lpd), params, states, vars, init, trans) ;
					processes = `ConsDeclarationList (proc, processes) ;
					return `ComponentDecl (name, genericParams, ports, params, ListVarDecl (), lpd, prios, NoneInit (), ParComp (parCompo (AllPorts (), PCList (PortComp (SomePorts (listExp ()), InstanceComp (Inst ("gextendedProcess_" + name, GenInstList (), genProcessPorts (ports, lpd), genProcessParams (params)))))))) ;
				} else {
					return `ProcessDecl (name, genericParams, ports, params, states, vars, init, trans) ;
				}
			}
		}
	}

	private static PortDecls genProcessPortsDecls (PortDecls decls, LPDList loc) {
		%match (loc) {
			ConslpdList (LocPortDec (decls, _, _), tl) -> {
				return concPortDecls (`decls, genProcessPortsDecls (decls, `tl)) ;
			}
		}
		return decls ;
	}

	private static ExpList genProcessPorts (PortDecls decls, LPDList loc) {
		%match (loc) {
			ConslpdList (LocPortDec (hd, _, _), tl) -> {
				return concExpList (portDeclsToExpList (`hd), genProcessPorts (decls, `tl)) ;
			}
		}
		return portDeclsToExpList (decls) ;
	}

	private static ExpList genProcessParams (ParamDecls params) {
		%match (params) {
			ConsListParamDecl (Paramdec (lal, val, type), tl) -> {
				return concExpList (argListToExplist (`lal), genProcessParams (`tl)) ;
			}
		}
		return `listExp () ;
	}

	private static ExpList portDeclsToExpList (PortDecls decls) {
		%match (decls) {
			ConsListPortDecl (PortDec (ports, _, _, _), tl) -> {
				return concExpList (stringListToExplist (`ports), portDeclsToExpList (`tl)) ;
			}
		}
		return `listExp () ;
	}

	private static ExpList argListToExplist (ArgList args) {
		%match (args) {
			ConsArList (ValArg (arg), tl) -> {
				return `ConslistExp (IdentExp (arg, OpenKind (), BotType ()), argListToExplist (tl)) ;
			}
			ConsArList (RefArg (arg), tl) -> {
				return `ConslistExp (RefExp (arg, BotType ()), argListToExplist (tl)) ;
			}
		}
		return `listExp () ;
	}

	private static ExpList stringListToExplist (StringList lst) {
		%match (lst) {
			ConsstringList (hd, tl) -> {
				return `ConslistExp (IdentExp (hd, OpenKind (), BotType ()), stringListToExplist (tl)) ;
			}
		}
		return `listExp () ;
	}

	private static ExpList concExpList (ExpList e1, ExpList e2) {
		%match (e1, e2) {
			listExp (l1*), listExp (l2*) -> {
				return `listExp (l1*, l2*) ;
			}
		}
		return `listExp () ;
	}

	private static PortDecls concPortDecls (PortDecls d1, PortDecls d2) {
		%match (d1, d2) {
			ListPortDecl (l1*), ListPortDecl (l2*) -> {
				return `ListPortDecl (l1*, l2*) ;
			}
		}
		return `ListPortDecl () ;
	}

	private static Declarations processes = `DeclarationList () ;
}
