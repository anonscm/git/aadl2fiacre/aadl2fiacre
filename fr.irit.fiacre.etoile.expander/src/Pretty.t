/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
package fr.irit.fiacre.etoile.expander ;

import fiacre.*;
import fiacre.types.*;

public class Pretty {
 %include { fiacre/Fiacre.tom }
      
	public static String prettyProgram_Fiacre (Program_Fiacre prg){
         %match(prg){
           Prog(d, NoneDecl()) -> { return prettyDeclarations(`d, null) ;}
           Prog(d, Decl(Main(str))) -> { return prettyDeclarations(`d, `str) + "\n";}
         }
        return "";
       }

      public static String prettyDeclarations(Declarations decs, String main){
        StringBuffer s = new StringBuffer();
        %match(decs){
          DeclarationList(_*, dec@ConstantDecl(_,_,_), _*) -> 
            { s.append(prettyDeclaration(`dec)).append("\n");}
        }
        %match(decs){
          DeclarationList(_*, dec@TypeDecl(_,_), _*) -> 
            { s.append(prettyDeclaration(`dec)).append("\n");}
        }
        %match(decs){
          DeclarationList(_*, dec@ChannelDecl(_,_), _*) -> 
            { s.append(prettyDeclaration(`dec)).append("\n");}
        }
        %match(decs){
          DeclarationList(_*, dec@ProcessDecl(_,_,_,_,_,_,_,_), _*) -> 
            { s.append(prettyDeclaration(`dec)).append("\n");}
        }
        %match(decs){
          DeclarationList(_*, dec@ComponentDecl(_,_,_,_,_,_,_,_,_), _*) -> 
            { s.append(prettyDeclaration(`dec)).append("\n");}
        }
	if (main != null) s.append("\n"+main+"\n");
        %match(decs){
          DeclarationList(_*, dec@PropertyDecl(_,_), _*) -> 
            { s.append(prettyDeclaration(`dec)).append("\n");}
        }
        %match(decs){
          DeclarationList(_*, dec@Assert(_,_,_), _*) -> 
            { s.append(prettyDeclaration(`dec)).append("\n");}
        }
        return s.toString();
      }

	/* ajout de la genericite */
      public static String prettyDeclaration(Declaration dec){
        %match(dec){
          TypeDecl (name, bodyT) -> { return "type " + `name + " is " + prettyType(`bodyT) ; }
          ChannelDecl (name, bodyC) -> 
                { return "channel " + `name + " is " + prettyChannel(`bodyC) ; }
          ConstantDecl (name, type, bodyE) -> 
                {  return "const " + `name + " : " + prettyType(`type) 
                          + " is " + prettyExp(`bodyE) ; }
	  ProcessDecl (name, lvarGen, ports, params, states, vars, init, trans) -> 
                { return "process " + `name + prettyVarGen(`lvarGen, ", ") + prettyPortDecls(`ports)
                         + prettyParamDecls(`params) + "\n is states \n" 
                         + prettyStringList(`states, ", ") + prettyVarDecls(`vars)
                         + prettyInit(`init) + prettyTransitions(`trans) ; } 
          ComponentDecl (name, lvarGen, ports, params, vars, lpd, prios, init, bodyCmp) -> 
                { return "component " + `name + prettyVarGen(`lvarGen, ", ") + prettyPortDecls(`ports) 
                         + prettyParamDecls(`params) + " is\n" + prettyVarDecls(`vars)
                         + prettyLPDList(`lpd) + prettyPrioDeclList(`prios)
                         + prettyInit(`init) + prettyComposition(`bodyCmp) ;}
	  PropertyDecl(name,p) ->
	        { return "property " + `name + " is\n  " + prettyProperty(`p); }
	  Assert(name, "", "") ->
	        { return "assert " + `name; }
	  Assert(name, ift, "") ->
	        { return "assert " + `name + " -true " + `ift; }
	  Assert(name, "", iff) ->
	        { return "assert " + `name + " -false " + `iff; }
	  Assert(name, ift, iff) ->
	        { return "assert " + `name + " -true " + `ift + " -false " + `iff; }
        }
        return "";
      }

      public static String prettyVarGen(StringList sl, String sep){
        %match(sl){
          stringList(head) -> {return "<| "+`head + " |> ";}
          ConsstringList(head, tail) -> {return "<| "+`head + sep + prettyStringList(`tail, sep)+ " |> " ;}
	}
	return "";
     }

      public static String prettyType(Type t){
        %match(t){
          PortType() -> { return "";}
	  BoolType() -> {return "bool" ;}
          NatType() -> {return "nat" ;}
          IntType() -> {return "int" ;}
          NamedType(abrev) -> {return `abrev ;}
          IntervalType(min, max) -> {return prettyExp(`min) + " .. " + prettyExp(`max) ;}
          UnionType(fdl) -> 
               {return "union \n" + prettyFieldsDeclList(`fdl, "of ", " |") + " end union \n" ;}
          RecordType(fdl) -> 
               {return "record \n" + prettyFieldsDeclList(`fdl, ": ", " ,") + " end record \n" ;}
          ArrayType(array, type) -> 
               { return "array " + prettyExp(`array) + " of " +  prettyType(`type) ;}
          QueueType(queue, type) -> 
               { return "queue " + prettyExp(`queue) + " of " +  prettyType(`type) ;}
          AnyArray() -> {return "";}
          AnyQueue() -> {return "";}
          BotType() -> {return "";}
          TopType() -> {return "";}
        }
        return "";
      }

      public static String prettyFieldsDeclList(FieldsDeclList fdl , String typeSep, 
                                                String elementSep){
        %match(fdl){
	  fieldList() -> { return "";}
          fieldList(fd) -> { return prettyFieldsDecl(`fd, typeSep) + "\n";}
          ConsfieldList(fd, sfdl) -> { return prettyFieldsDecl(`fd, typeSep) + elementSep + "\n" 
                                       + prettyFieldsDeclList(`sfdl, typeSep, elementSep) ; }
        }
        return "";
      }

      public static String prettyFieldsDecl(FieldsDecl fd, String typeSep){
        %match(fd){
          field(names, BotType()) -> {return prettyStringList(`names, ", ") ; }
          field(names, type) -> {return prettyStringList(`names, ", ") + typeSep 
                                        + prettyType(`type) ; }
        }
        return "";
      }

      public static String prettyStringList(StringList s, String sep){
        %match(s){
          stringList(head) -> {return `head + " ";}
          ConsstringList(head, tail) -> {return `head + sep + prettyStringList(`tail, sep) ;}
        }
        return "";
      }

      public static String prettyChannel(Channel c){
        %match(c){
          NamedChannel(abrev) -> {return `abrev ;} /*jamais construit   */
          ProfileChannel(profile(Type_list())) -> {return "none" ;}
          ProfileChannel(profile(tl)) -> {return  prettyTypeList(`tl);}
        }
        return "";
      }


      public static String prettyTypeList(TypeList t){
        %match(t){
          Type_list(type) -> {return prettyType(`type) ;}
          ConsType_list(t1,ts) -> {return prettyType(`t1) + "#" + prettyTypeList(`ts) ;}
        }
        return "";
      }

      public static String prettyExp(Exp exp){
        %match(exp){
          ParenExp(e) -> {return "(" + prettyExp(`e) + ")"  ;}
          CondExp(test, st1, st2, type) -> {return prettyExp(`test) + " ? " + 
                          prettyExp(`st1) + " : " + prettyExp(`st2)  ;}
          Unop(uop, type, e) -> {return  prettyUnop(`uop) + " " + prettyExp(`e) ;}
          Binop(bop, type, e1, e2) -> {return prettyBinop(`bop) + "(" + prettyExp(`e1) +
                                              ", " + prettyExp(`e2) + ")"  ;}
          Infix(iop, type, e1, e2)-> {return  prettyExp(`e1) + prettyInfix(`iop) 
                                              + prettyExp(`e2)  ;}
          RecordExp(affs, type) -> {return "{" + prettyAffectationList(`affs) + "}" ;}
          QueueExp(queue, type) -> {return "{|" + prettyExpList(`queue) + "|}"  ;}
          ArrayExp(el, t) -> {return  "[" + prettyExpList(`el) + "]" ;}
          RefExp(ref, type) -> {return  "&" + `ref ;}
          ConstrExp(constr, e, typed) -> {return `constr + " (" + prettyExp(`e) +")" ;}
          IntExp(i, type) -> {return  `i ;}
          BoolExp(b) -> {return "" + `b  ;}
          IdentExp(var, kind, type) -> {return `var  ;}
          AnyExp() -> {return  "any" ;}
          ArrayAccessExp(array, e, type) -> {return prettyExp(`array) + "[" + 
                                                    prettyExp(`e)+"]"  ;}
          RecordAccessExp(record, id, type) -> {return prettyExp(`record) + "." + `id ;}
        }
        return "";
      }

      public static String prettyUnop(Unop u){
        %match(u){
          MINUS() -> { return "-" ;}
          PLUS() -> { return "+" ;}
          NOT() -> { return "not" ;}
          COERCE() -> { return "$" ;}
          FULL()  -> { return "full " ;}
          EMPTY()  -> { return "empty " ;}
          DEQUEUE() -> { return "dequeue " ;}
          FIRST() -> { return "first " ;}
	  LENGTH() -> { return "length " ;}
        }
        return "";
      }

      public static String prettyBinop(Binop b){
        %match(b){
          ENQUEUE() -> {return "enqueue";} 
          APPEND() -> {return "append";}
        }
        return "";
      }

      public static String prettyInfix(Infix t){
        %match(t){
          AND() -> {return " and " ;}
          OR() -> {return " or " ;}
          LE()  -> {return " <= " ;}
          LT()  -> {return " < " ;}
          GT()  -> {return " > " ;}
          GE()  -> {return " >= " ;}
          EQ()  -> {return " = " ;}
          NE() -> {return " <> " ;}
          ADD()  -> {return " + " ;}
          SUB()  -> {return " - " ;}
          MOD()  -> {return " % " ;}
          MUL()  -> {return " * " ;}
          DIV() -> {return " / " ;}
        }
        return "";
      }

      public static String prettyAffectationList(AffectationList afl){
        %match(afl){
          AffList(e) -> {return prettyAffectation(`e);}
          ConsAffList(e,list) -> {return prettyAffectation(`e)+ ", " + prettyAffectationList(`list);}
        }
        return "";
      }

      public static String prettyAffectation(Affectation a){
        %match(a){
          Affect(name, val) -> {return `name + " = " + prettyExp(`val); }
        }
        return "";
      }

      public static String prettyExpList(ExpList el){
        %match(el){
          listExp() -> {return "";}
          listExp(e) -> {return prettyExp(`e) ;}
          ConslistExp(e,list) -> {return prettyExp(`e) + ", " + prettyExpList(`list) ;}
        }
        return "";
      }

      public static String prettyStatement(Statement fsst){
        %match(fsst){
          StNull() -> { return  "null"  ;}
	  StTag(id) -> { return "#" + `id; }
          StCond(cond, st1, condst, ost)  -> { return "if " + prettyExp(`cond) + " then \n" +
                prettyStatement(`st1) + "\n" + prettyConditionalStatementList(`condst) + 
                prettyOptional_Statement(`ost) +"\n"  ;}
          StSelect(stl) -> { return  "\n select \n" + prettyPStatementList(`stl) + "\n end select \n"  ;}
	  StSelectIndexe(var,deb,fin,stl) -> { return  "\n select " + `var +" of (" + prettyExp(`deb) + " .. " + prettyExp(`fin) + ")\n" + prettyPStatementList(`stl) + "\n end select \n"  ;}
          StSequence(s1, s2) -> { return prettyStatement(`s1) + ";\n" + prettyStatement(`s2)  ;}
          StTo(state) -> { return "to " + `state  ;}
          StWhile(cond, st) -> { return  "while " + prettyExp(`cond) + " do \n" +
                       prettyStatement(`st) + "\n end while \n" ;}
          StAssign(p, elist) -> { return prettyExpList(`p) + " := " + prettyExpList(`elist) ;}
          StAny(p, types, NoneExp()) -> { return prettyExpList(`p)+ " := any " ;}
          StAny(p, types, OptExp(e)) -> { return prettyExpList(`p)+ " := any where " +
                                          prettyExp(`e);}
          StSignal(po, chan) -> { return  prettyExp(`po)  ;}
	  StInput(po, chan, p, NoneExp()) -> { return prettyExp(`po) +"?"+ prettyExpList(`p) ;}
          StInput(po, chan, p, OptExp(e)) -> { return prettyExp(`po) +"?" + prettyExpList(`p) 
                                              + " where " + prettyExp(`e);}
          StOutput(po, chan, elist) -> { return prettyExp(`po) + "!"+ prettyExpList(`elist)   ;}
          StCase(e, cel) -> { return "\n case " + prettyExp(`e) + " of\n" + 
                                     prettyCaseElementList(`cel) + "\n end case\n" ;}
          StForEach(s, type, st) -> { return  "foreach " + `s + " do " + prettyStatement(`st)
                                             + " end foreach\n"  ;}
	  StLoop() -> {return "loop\n";}
	  StOn(e) -> {return "on "+prettyExp(`e);}
	  StWait(b1,b2) -> {return "wait "+prettyBound(`b1, "left") + ", " + prettyBound(`b2, "right") ;}
        }
        return "";
      }

      public static String prettyOptional_Statement(Optional_Statement os){
        %match(os){
          NoneSt() -> {return " end if \n";}
          Statmt(st) -> {return " else \n " + prettyStatement(`st) +  " end if \n" ;}
        }
        return "";
      }

      public static String prettyPStatementList(PStatementList sl){
        %match(sl){
          PStmList() -> {return "";}
          PStmList(st) -> {return prettyStatementList(`st) ;}
          ConsPStmList(e,list) -> {return prettyStatementList(`e) + "\n  unless \n " 
                                         + prettyPStatementList(`list) ;}
        }
        return "";
      }

      public static String prettyStatementList(StatementList sl){
        %match(sl){
          StmList() -> {return "";}
          StmList(st) -> {return prettyStatement(`st) ;}
          ConsStmList(e,list) -> {return prettyStatement(`e) + "\n  [] \n " 
                                         + prettyStatementList(`list) ;}
        }
        return "";
      }

      public static String prettyConditionalStatementList(ConditionalStatementList cstl){
        %match(cstl){
          CondStList() -> { return "" ;}
          ConsCondStList(ConditionalStmt(e,st),efs) -> { return "elsif " + prettyExp(`e)
              + " then \n" + prettyStatement(`st) + " \n" + prettyConditionalStatementList(`efs) ;}
        }
        return "";
      }

      public static String prettyCaseElement(CaseElement ce){
        %match(ce){
          caseElement(b, p, st) -> { return prettyExp(`p) + " -> " +prettyStatement(`st) ;}
        }
        return "";
      }

      public static String prettyCaseElementList(CaseElementList cel){
        %match(cel){
          CaseEL(m) -> { return prettyCaseElement(`m) ;}
          ConsCaseEL(m, ms) -> { return prettyCaseElement(`m) + " \n | \n " + 
                       prettyCaseElementList(`ms) ;}
        }
        return "";
      }


      public static String prettyComposition(Composition c){
        %match(c){
          ParComp(parc) -> {return "par "+ prettyParComposition(`parc) + " \n end par \n";}
          InstanceComp(i) -> {return  prettyInstance(`i) ;}
        }
        return "";
      }

      public static String prettyParComposition(ParComposition pc){
        %match(pc){
          parCompo(ps, pcl) -> { return "\n "+prettyPortSet(`ps) + " in " + prettyPortCompList(`pcl)  ;}
          parCompo(SomePorts(listExp()), pcl) -> { return  "\n "+prettyPortCompList(`pcl) ;}
	  parCompoB(id,d,l,ps, pa) -> { return ` id+" of ("+prettyExp(`d)+".."+prettyExp(`l)+") in \n "+prettyPortCompList(`pa) ;}
        }
        return "";
      }

      
      public static String prettyPortComposition(PortComposition portcomp){
        %match(portcomp){
          PortComp(ps, comp) -> {return prettyPortSet(`ps) + " -> " + prettyComposition(`comp) ;}
          PortComp(SomePorts(listExp()), comp) -> {return  prettyComposition(`comp)  ;} 
        }
        return "";
      }

      /* ajout du tab. de ports */
      public static String prettyInstance(Instance i){
        %match(i){
	  Inst(s, GenInstList(), pl, le) -> { return `s +" "+ prettyInstPortsList(`pl) + " " 
                                        + prettyInstExpList(`le) + "\n";}
	  Inst(s, lve, pl, le) -> { return `s + " <|" + prettyGenInsts(`lve) +"|> " + prettyInstPortsList(`pl) + " " 
                                        + prettyInstExpList(`le) + "\n";}
        }
        return "";
      }  
      
      public static String prettyPortCompList(PortCompList pcl){
        %match(pcl){
          PCList(pc) -> {return prettyPortComposition(`pc) ;}
          ConsPCList(pc,pcs) -> {return prettyPortComposition(`pc) + "\n  || \n"
                                  + prettyPortCompList(`pcs) ;}  
        }
        return "";
      }
 
      public static String prettyPortSet(PortSet ps){
        %match(ps){
          AllPorts() -> { return "*";}
          SomePorts(pl) -> { return prettyExpList(`pl) ;}
        }
        return "";
      }

      public static String prettyPortAttrList(PortAttrList pal){
        %match(pal){
          PAList() -> {return "" ;}
          ConsPAList(IN(), p) -> {return "in " +  prettyPortAttrList(`p) ;}
          ConsPAList(OUT(), p) -> {return "out " +  prettyPortAttrList(`p) ;}
        }
        return "";
      }  

      public static String prettyParamDecl(ParamDecl p){
        %match(p){
          Paramdec(lal, val, type) -> {return prettyArgList(`lal) + " : "+ 
                        prettyVarAttrList(`val) + " " + prettyType(`type)     ;}
        }
        return "";
      }

      public static String prettyArgList(ArgList al){
        %match(al){
          ArList(ValArg(id)) -> { return `id ;}
          ArList(RefArg(id)) -> { return "&" + `id ;}
          ConsArList(ValArg(id), list) -> { return `id + ", " + prettyArgList(`list);}
          ConsArList(RefArg(id), list) -> { return "&"+ `id + ", " + prettyArgList(`list);}
        }
        return "";
      }

      public static String prettyVarAttrList(VarAttrList t){
        %match(t){
          VAList() -> {return "" ;}
          ConsVAList(READ(), val) -> {return "read " +  prettyVarAttrList(`val) ;}
          ConsVAList(WRITE(), val) -> {return "write " +  prettyVarAttrList(`val) ;}
        }
        return "";
      }

      
      public static String prettyVarDecl(VarDecl vd){
        %match(vd){
          VarDec(lsl, type, NoneExp()) -> {return prettyStringList(`lsl, ", ") + " : " + 
                                prettyType(`type) ;}
          VarDec(lsl, type, OptExp(e)) -> {return prettyStringList(`lsl, ", ") + " : " + 
                                prettyType(`type) + " := " + prettyExp(`e) ;}
        }
        return "";
      }

      public static String prettyPriorDecl(PriorDecl prio){
        %match(prio){
          PriorDec(pl1, pl2) -> {return  prettyExpList(`pl1, " | ") + " > " 
                                       +  prettyExpList(`pl2, " | ")  + "\n";}
        }
        return "";
      }

      public static String prettyExpList(ExpList pl, String sep){
        %match(pl){
          listExp(head) -> {return prettyExp(`head) + " ";}
          ConslistExp(head, tail) -> {return prettyExp(`head) + sep + prettyExpList(`tail, sep) ;}
        }
        return "";
      }

      public static String prettyPrioDeclList(PrioDeclList prios){
        %match(prios){
          PrioList() -> {return "" ;}
          ConsPrioList(e,l) -> {return "\n priority \n" + prettySubPrioDeclList(`prios) ;}
        }
        return "";
      }

       public static String prettySubPrioDeclList(PrioDeclList prios){
        %match(prios){
          PrioList(prio) -> {return prettyPriorDecl(`prio) ;}
          ConsPrioList(e,l) -> {return prettyPriorDecl(`e) + ", \n"+ prettySubPrioDeclList(`l) ;}
        }
        return "";
      }

      public static String prettyLocPortDecl(LocPortDecl lpd){
        %match(lpd){
          LocPortDec(pdl, b1, b2) -> { return prettyLocPortDecls(`pdl) + " in " 
              + prettyBound(`b1, "left") + ", " + prettyBound(`b2, "right") ;}
        }
        return "";
      }

      public static String prettyBound(Bound b, String s){
        %match(b,s){
          Open(n),"left" -> { return "] " + `n ; }
          Open(n),"right" -> { return `n + "[ "; }
          Closed(n),"left" -> {return "[ " + `n ;}
          Closed(n),"right" -> {return `n  + "] " ;}
          Infinite(),"right" -> {return "...[" ;}
        }
        return "";
      }

      public static String prettyLPDList(LPDList lpdl){
        %match(lpdl){
          lpdList() ->  {return "" ;}
          ConslpdList(lpd,lpds) -> {return "port \n"+ prettyLPDList2(`lpdl) ;}
        }
        return "";
      }

      public static String prettyLPDList2(LPDList lpdl){
        %match(lpdl){
          lpdList(lpd) ->  {return prettyLocPortDecl(`lpd) + "\n" ;}
          ConslpdList(lpd,lpds) -> {return prettyLocPortDecl(`lpd) + ", \n" 
                                   + prettyLPDList2(`lpds) ;}
        }
        return "";
      }

      public static String prettyTransition(Transition t){
        %match(t){
          Trans(_, id ,st) -> {return "from " + `id + " " + prettyStatement(`st);}
        }
        return "";
      }
      
      public static String prettyParamDecls(ParamDecls params){
        %match(params){
          ListParamDecl() -> {return "";}
          ConsListParamDecl(pd, lpds) -> {return "(" + prettySubParamDecls(`params) + ")";}
        }
        return "";
      }

      public static String prettySubParamDecls(ParamDecls pds){
        %match(pds){
          ListParamDecl(pd) -> {return prettyParamDecl(`pd) ;}
          ConsListParamDecl(pd, lpds) -> {return  prettyParamDecl(`pd) + ", " +
                               prettySubParamDecls(`lpds) ;}
        }
        return "";
      }

      public static String prettyVarDecls(VarDecls vds){
        %match(vds){
          ListVarDecl() -> {return "";}
          ConsListVarDecl(v,lv) -> {return "\n var \n" + prettySubVarDecls(`vds) ;}
        }
        return "";
      }

      public static String prettySubVarDecls(VarDecls vds){
        %match(vds){
          ListVarDecl(v) -> {return prettyVarDecl(`v) + "\n" ;}
          ConsListVarDecl(v,lv) -> {return prettyVarDecl(`v) + ", \n " + prettySubVarDecls(`lv) ;}
        }
        return "";
      }

      public static String prettyInit(Init i){
        %match(i){
          Initial(st) -> {return "init \n" + prettyStatement(`st) + "\n  ";}
          NoneInit() -> {return ""   ;}
        }
        return "";
      }

      public static String prettyTransitions(Transitions tl){
        %match(tl){
          TransList(t) -> {return prettyTransition(`t) + "\n" ;}
          ConsTransList(t,lt) -> {return prettyTransition(`t) + "\n"  + prettyTransitions(`lt);}
        }
        return "";
      }

      public static String prettyPortDecl(PortDecl pd){
        %match(pd){
          PortDec(sl, type, pal, chan) -> {return prettyStringList(`sl,",") + " : " + prettyType(`type) + " " +
                                     prettyPortAttrList(`pal) + " " + prettyChannel(`chan) ;}
        }
        return "";
      }

      public static String prettyPortDecls(PortDecls pds){
        %match(pds){
          ListPortDecl() -> {return "";}
          ListPortDecl(pd) -> {return "["+prettyPortDecl(`pd)+"]" ;}
          ConsListPortDecl(pd, lpds) -> {return  "["+prettyPortDecl(`pd) + ", " +
                               prettyLocPortDecls(`lpds)+"]" ;}
        }
        return "";
      }
      
       public static String prettyLocPortDecls(PortDecls pds){
        %match(pds){
          ListPortDecl() -> {return "";}
          ListPortDecl(pd) -> {return prettyPortDecl(`pd) ;}
          ConsListPortDecl(pd, lpds) -> {return  prettyPortDecl(`pd) + ", " +
                               prettyLocPortDecls(`lpds) ;}
        }
        return "";
      }

      public static String prettyInstPortsList(ExpList el){
        %match(el){
          listExp(e) -> {return "["+prettyExp(`e)+"]" ;}
          ConslistExp(e,list) -> {return "["+prettyExp(`e) + ", " + prettyExpListInst(`list)+"]" ;}
        }
        return "";
      }

      public static String prettyGenInst(GenInst gi){
        %match(gi){
          TypeInst(t) -> { return "type "+ `t;}
          ConstInst(c) -> { return "const "+ `c;}
          Constr0Inst(c) -> { return "constr0 "+ `c;}
          Constr1Inst(c) -> { return "constr1 "+ `c;}
        }
        return "";
      }

	public static String prettyGenInsts(GenInsts l){
        %match(l){
          GenInstList() -> {return "";}
          GenInstList(e) -> {return prettyGenInst(`e) ;}
          ConsGenInstList(e,list) -> {return prettyGenInst(`e) + ", " + prettyGenInsts(`list) ;}
        }
        return "";
      }

       public static String prettyInstExpList(ExpList el){
        %match(el){
          listExp(e) -> {return "("+prettyExp(`e)+")" ;}
          ConslistExp(e,list) -> {return "("+prettyExp(`e) + ", " + prettyExpList(`list)+")" ;}
        }
        return "";
      }

	public static String prettyExpListInst(ExpList el){
        %match(el){
          listExp() -> {return "";}
          listExp(e) -> {return prettyExp(`e) ;}
          ConslistExp(e,list) -> {return prettyExp(`e) + ", " + prettyExpListInst(`list) ;}
        }
        return "";
      }

      public static String prettyItem(Item l) {
      	     %match(l) {
	       IState(i) -> { return "state " + "u"+`i; }
	       ITag(i) -> { return "tag " + `i; }
	       IEvent(i) -> { return "event " + "u" + `i; }
	       IValue(e) -> { return "value " + prettyExp(`e); }
	     }
	     return "";
      }

      public static String prettyOPath(OPath p) {
      	     %match(p) {
	       OPathFrom(r,IndexList()) -> { return "u"+`r; }
	       OPathFrom(r,p1) -> { return "u"+`r+"/"+prettyRPath(`p1); }
	     }
	     return "";
      }

      public static String prettyIndex(CIndex c) {
      	     %match(c) {
	       Cident(i) -> { return `i; }
	       Cnum(i) -> { return `i; }
	     }
	     return "";
      }

      public static String prettyRPath(RPath p) {
      	     %match(p) {
	       IndexList() -> {return "";}
	       IndexList(i) -> { return prettyIndex(`i); }
	       IndexList(i,l*) -> { return prettyIndex(`i)+"/"+prettyRPath(`l); }
	     }
	     return "";
      }

      public static String prettyObservable(Observable o) {
      	     %match(o) {
     	       ObsItem(p, i) -> { return prettyOPath(`p) + "/" + prettyItem(`i); }
	       ObsUnary(op, obs) -> { return `op + " " + prettyObservable(`obs); }
	       ObsBinary(o1, op, o2) -> { return prettyObservable(`o1) + " " + `op + " " + prettyObservable(`o2); }
	       ObsPar(o1) -> { return "(" + prettyObservable(`o1) +")"; }
	     }
	     return "";
      }

      public static String prettyLTL(LTL l) {
      	    %match(l) {
	       LtlObs(obs) -> { return prettyObservable(`obs); }
	       LtlAtom(id) -> { return `id; }
	       LtlUnary(op, l1) -> { return `op + " " + prettyLTL(`l1); }
	       LtlBinary(l1, op, l2) -> { return prettyLTL(`l1) + " " + `op + " " + prettyLTL(`l2); }
	       LtlPar(l1) -> { return "(" + prettyLTL(`l1) + ")"; }
	    }
	    return "";
      }

      public static String prettyDuration(Duration d) {
      	    %match(d) {
	    	      duration(d1) -> { return `d1; }
	    }
	    return "";
      }

      public static String prettyProperty(Property p) {
            %match(p) {
	      PropLTL (f) -> { return "ltl " + prettyLTL(`f); }
              PropO(op) -> { return `op; }
	      PropOx (op, obs) -> { return `op + " " + prettyObservable(`obs); }
	      PropOxOx(op1, obs1, op2, obs2) -> 
	        { return `op1 + " " + prettyObservable(`obs1) + " " + `op2 + " " + prettyObservable(`obs2); }
	      PropxOx(obs1, op, obs2) ->
	        { return prettyObservable(`obs1) + " " + `op + " " + prettyObservable(`obs2); }
	      PropOxOxOx(op1, obs1, op2, obs2, op3, obs3) ->
	        { return `op1 + " " + prettyObservable(`obs1) + " " + `op2 + " " + prettyObservable(`obs2) + " " + `op3 + " " + prettyObservable(`obs3); }
	      PropxOxOxOx(obs1, op1, obs2, op2, obs3, op3, obs4) ->
	        { return prettyObservable(`obs1) + " " + `op1 + " " + prettyObservable(`obs2) + " " + `op2 + " " + prettyObservable(`obs3) + " " + `op3 + " " + prettyObservable(`obs4); }
	      PropxOxOx(obs1, op1, obs2, op2, obs3) ->
	        { return prettyObservable(`obs1) + " " + `op1 + " " + prettyObservable(`obs2) + " " + `op2 + " " + prettyObservable(`obs3); }
	      PropBin(arg1, op, arg2) ->
	        { return prettyProperty(`arg1) + " " + `op + " " + prettyProperty(`arg2); }
	      PropOxOd(op1, obs1, op2, dur) ->
	        { return `op1 + " " + prettyObservable(`obs1) + " " + `op2 + " " + prettyDuration(`dur); }
	      PropxOxOI(obs1, op1, obs2, op2, min,max) ->
	        { return prettyObservable(`obs1) + " " + `op1 + " " + prettyObservable(`obs2) + " " + `op2 + " " + prettyBound(`min, "left") +"," + prettyBound(`max, "right"); }
	      PropOxOxOd(op1, obs1, op2, obs2, op3, dur) ->
	        { return `op1 + " " + prettyObservable(`obs1) + " " + `op2 + " " + prettyObservable(`obs2) + " " + `op3 + " " + prettyDuration(`dur); }
	      PropOxOxOI(op1, obs1, op2, obs2, op3, min,max) ->
	        { return `op1 + " " + prettyObservable(`obs1) + " " + `op2 + " " + prettyObservable(`obs2) + " " + `op3 + " " + prettyBound(`min, "left") + "," + prettyBound(`max,"right"); }
	      PropxOxOxOI(obs1, op1, obs2, op2, obs3, op3, min,max) ->
	        { return prettyObservable(`obs1) + " " + `op1 + " " + prettyObservable(`obs2) + " " + `op2 + " " + prettyObservable(`obs3) + " " + `op3 + " " + prettyBound(`min, "left") + "," + prettyBound(`max, "right"); }
	      PropNot(arg) -> { return "not " + prettyProperty(`arg); }
	      PropPar(arg) -> { return "(" + prettyProperty(`arg) + ")"; }
	    }
	    return "";
      }
}
