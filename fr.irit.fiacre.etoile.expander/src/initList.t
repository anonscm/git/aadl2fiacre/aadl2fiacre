/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Permet d'initialiser les tableaux en intention.
 * Ainsi, [i : 5 | i + 1] sera remplacé par [1, 2, 3, 4, 5].
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.* ;
import fiacre.* ;
import fiacre.types.* ;
import java.util.* ;

public class initList {
	%typeterm Hashtable{
		implement 	{Hashtable}
		is_sort(t)	{ t instanceof Hashtable}
	}
	%include {fiacre/Fiacre.tom}
	%include {sl.tom}
	%op Strategy CollectCte (tabCte:Hashtable) {
		  make(tabCte) { new common.CollectCte (tabCte) }
	}

	public static Program_Fiacre initList (Program_Fiacre prg) throws Exception {
		try {
			Hashtable<String,Integer> tabCte = new Hashtable<String,Integer>();
			`TopDown (CollectCte (tabCte)).visit (prg) ;
			return `TopDown (initialisationList (tabCte)).visit (prg) ;
		} 
		catch (VisitFailure e) {
			return prg ;
		}
	}

	%strategy initialisationList (tabCte : Hashtable) extends Identity () {
		visit Exp {
			InitListExp(var, n, e) -> {
				Integer N = common.eval (`n, tabCte) ;
				if (N == null) {
					System.err.println ("initList: impossible d'évaluer la taille du tableau " + `n) ;
					System.exit (1) ;
				}
				ExpList lst = `listExp () ;
				for (int i = N - 1 ; i >= 0 ; --i)
					lst = `ConslistExp (TopDown (replaceVar (var, i)).visit (e), lst) ;
				return `ArrayExp (lst, BotType ()) ;
			}
		}
	}

	%strategy replaceVar (var: String, i: int) extends Identity () {
		visit Exp {
			IdentExp (v, kind, type) -> {
				if (`v.equals (var))
					return `IntExp ("" + i, BotType ()) ;
			}
		}
	}
}
