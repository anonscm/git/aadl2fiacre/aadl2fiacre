/* Copyright (C) 2018 UPS/IRIT
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* Original authors: Jean-Paul Bodeveix, Mamoun Filali, Regis Spadotti, Guillaume Verdier (UPS/IRIT)
* Modified by: Marc Pantel (INPT/IRIT)
* 
*/
/*
 * Permet d'initialiser les N éléments d'un tableau avec une valeur v
 * sous la forme [N] * v ou v * [N]
 */
package fr.irit.fiacre.etoile.expander ;

import tom.library.sl.*;
import fiacre.*;
import fiacre.types.*;
import java.lang.Integer;
import java.util.Hashtable;

public class InitEvo {

%typeterm Hashtable{
    implement 	{Hashtable}
    is_sort(t)	{ t instanceof Hashtable}
  }

%include { fiacre/Fiacre.tom }
%include { sl.tom }
	%op Strategy CollectCte(tabCte:Hashtable) {
		  make(tabCte) { new common.CollectCte(tabCte) }
	}

	public static Program_Fiacre initEvo (Program_Fiacre prg) throws Exception
	{
		try 
		{
			Hashtable<String,Integer> tabCte = new Hashtable<String,Integer>();	/* nom cte declare et initialise */

			/* sauvegarde des ctes */
			Program_Fiacre pf = `TopDown(CollectCte(tabCte)).visit(prg);
			/* m-a-j l'initialisation des tableaux evolues */
			pf = `TopDown(normaliseDecl(tabCte)).visit(pf);

			return pf;
		} 
		catch(VisitFailure e) {}
	   	return prg;
	}

	/***************************************************/
	/* normalise le Optional_Exp			   */
	/***************************************************/
	%strategy normaliseDecl(tabCte:Hashtable) extends Identity()
	{
		visit Exp {
			/* cas [t]*cte */
			Infix(MUL(), type, tab@ArrayExp(el, tA), eg) ->
			{
				/* evaluation de la cte */
				Integer fois = common.eval(`eg,tabCte);
				if((fois == null))
				{
					System.out.println("pbInitTableau: "+Pretty.prettyExp(`Infix(MUL(), type, tab, eg)));
					System.exit(-1);
				}
				ExpList elUpdate = dupExp(`el, fois);
				return `ArrayExp(elUpdate, tA);
			}

			/* cas cte*[t] */
			Infix(MUL(), type, ed, tab@ArrayExp(el, tA)) ->
			{
				/* evaluation de la cte */
				Integer fois = common.eval(`ed,tabCte);
				if((fois == null))
				{
					System.out.println("pbInitTableau: "+Pretty.prettyExp(`Infix(MUL(), type, ed, tab)));
					System.exit(-1);
				}
				ExpList elUpdate = dupExp(`el, fois);
				return `ArrayExp(elUpdate, tA);
			}
		}
	}
	
	/*************************************/
	/* inidique si l'exp est autorise    */
	/*************************************/
	private static boolean expOk(Exp exp)
	{
		%match (exp)
		{
			IntExp(_, _) -> { return true;}
			IdentExp(_, _, _) -> { return true;}
		}
		return false;
	}

	/**************************************************/
	/* duplique une expression nbfois		  */
	/**************************************************/
	private static ExpList dupExp(ExpList el, int nbFois)
	{
		if(nbFois == 1)
		{
			return el;
		}
		return appendExpList(el, dupExp(el, nbFois-1));
	}

	/***********************************************************/
	/* concatene 2 ExpList                                     */
	/***********************************************************/
	private static ExpList appendExpList(ExpList el1, ExpList el2)
	{
		%match(el1,el2) 
		{
			listExp(le1*),listExp(le2*) -> { return `listExp(le1,le2); }
		}
		return null;
	}
}
