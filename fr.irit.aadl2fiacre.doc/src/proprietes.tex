%!TEX root = ./transformation.tex
%==============================================================================%
               \section{Génération des propriétés}
%==============================================================================%

Cette section présente les schémas de génération des propriétés
automatiquement vérifiées sur un modèle AADL. Trois familles de
propriétés ont été étudiées: les propriétés portant sur la sémantique
du langage AADL dont la vérification est une condition nécessaire de
la correction de la traduction, les propriétés devant être satisfaites
par tout modèle AADL et des propriétés dont la validité est indiquée à
titre d'information sur le modèle. Ces propriétés sont ici spécifiées en
\textit{Acceleo} et peuvent être intégrées au modèle Fiacre* contenant la description comportementale du source AADL.

\subsection{Propriétés portant sur la sémantique du langage AADL}

Ces propriétés constituent un sous-ensemble d'une sémantique axiomatique du langage AADL. Elles sont générées pour démontrer la possibilité de vérifier (partiellement) la correction de la traduction du modèle considéré. D'autres techniques (basées sur des assistants de preuve) seraient nécessaires pour garantir la correction du traducteur lui-même, garantissant ainsi la correction de la traduction de tout modèle. Nous avons considéré les propriétés suivantes:

\begin{itemize}
  \item[$\oplus$] \textit{Un thread périodique est activé périodiquement}. La propriété
  suivante est automatique générée pour chaque thread périodique. Elle
  se base sur l'automate d'état associé à chaque thread et exprime que
  chaque entrée dans l'état suivant l'activation est suivi par une
  nouvelle entrée au bout d'une durée égale à la période.

\textit{Règles de génération}

\begin{rtfiacre}
[for (t : ComponentInstance | thl->select(p | p.isPeriodic() and p.hasThreadDeadline())) 
    separator('\n')]
  property ['PR_PER_'.concat(t.uid())/] is 
     enter [s.id()/]/[thl->indexOf(t)*2/]/value (st = pdl_rdy) 
     leadsto enter [s.id()/]/[thl->indexOf(t)*2/]/value (st = pdl_rdy) 
     within [ '[' /] [t.getThreadPeriod()/], [t.getThreadPeriod()/] [ ']' /]
  assert ['PR_PER_'.concat(t.uid())/] -true "SUCCESS: [ t.uid() /] is dispatched periodically" 
                                  -false "ERROR: [ t.uid() /] is not dispatched periodically"
[/for]

[for (t : ComponentInstance | thl->select(p | p.isPeriodic() and not p.hasThreadDeadline())) 
    separator('\n')]
  property ['PR_PER_'.concat(t.uid())/] is 
     enter [s.id()/]/[thl->indexOf(t)*2/]/value (st = p_rdy) 
     leadsto enter [s.id()/]/[thl->indexOf(t)*2/]/value (st = p_rdy) 
     within [ '[' /] [t.getThreadPeriod()/], [t.getThreadPeriod()/] [ ']' /]
  assert ['PR_PER_'.concat(t.uid())/] -true "SUCCESS: [ t.uid() /] is dispatched periodically" 
                                    -false "ERROR: [ t.uid() /] is not dispatched periodically"
[/for]
\end{rtfiacre}

\textit{Exemple de génération}
\begin{rtfiacre}
property PR_PER_aadl_pmin_t is 
  enter aadl_buffers_alarm_i_Instance/2/value (st = p_rdy) 
     leadsto enter aadl_buffers_alarm_i_Instance/2/value (st = p_rdy) 
  within [ 5000, 5000 ]

assert PR_PER_aadl_pmin_t 
  -true "SUCCESS: aadl_pmin_t is dispatched periodically" 
  -false "ERROR: aadl_pmin_t is not dispatched periodically"
\end{rtfiacre}

\item[$\oplus$] \textit{Un thread sporadique est immédiatement activé si un
    événement est présent sur l'un de ses ports d'entrée et que la
    condition d'activation associée est vérifiée}. La
  propriété suivante est automatiquement générée pour chaque thread
  sporadique AADL. Lorsque la file d'un des ports d'entrée est non
  vide, le thread entre immédiatement (temporellement) dans l'état
  \textit{ready}. La condition d'activation est ici ignorée.

\textit{Règles de génération}

\begin{rtfiacre}
[for (t : ComponentInstance | thl->select(p | p.isSporadic())) separator('\n')]
 [let pl: OrderedSet(FeatureInstance) = t.getIncomingPortFeatureInstances()]
  [for (p: FeatureInstance | pl->select(feature.oclIsKindOf(EventDataPort)))]
  property ['PR_SPO_IMM_DISP_'.concat(p.feature.name).concat('_').concat(t.uid())/] is 
     [s.id()/]/[3+thl->size()*2/]/[pl->indexOf(p)/]/value (not(empty(q))) 
    leadsto enter [s.id()/]/[thl->indexOf(t)*2/]/value (st = sp_rdy) within ['[0,0]'/]

  assert ['PR_SPO_IMM_DISP_'.concat(p.feature.name).concat('_').concat(t.uid())/] 
    -true "SUCCESS: [ t.uid() /] is immediately dispatched if a queue is not empty" 
    -false "WARNING: [ t.uid() /] is not dispatched in zero time but a queue is not empty"
  [/for]

  [for (p: FeatureInstance | pl->select(feature.oclIsKindOf(EventPort)))]
  property ['PR_SPO_IMM_DISP_'.concat(p.feature.name).concat('_').concat(t.uid())/] is 
     [s.id()/]/[3+thl->size()*2/]/[pl->indexOf(p)/]/value (evt > 0) 
    leadsto enter [s.id()/]/[thl->indexOf(t)*2/]/value (st = sp_rdy) within ['[0,0]'/]

  assert ['PR_SPO_IMM_DISP_'.concat(p.feature.name).concat('_').concat(t.uid())/] 
    -true "SUCCESS: [ t.uid() /] is immediately dispatched if a queue is not empty" 
    -false "WARNING: [ t.uid() /] is not dispatched in zero time but a queue is not empty"
  [/for]
 [/let]
[/for]
\end{rtfiacre}

\textit{Exemple de génération}
\begin{rtfiacre}
property PR_SPO_IMM_DISP_alarm_aadl_man_t is 
  aadl_buffers_alarm_i_Instance/9/1/value (not(empty(q))) leadsto 
  enter aadl_buffers_alarm_i_Instance/4/value (st = sp_rdy) within [0,0]

assert PR_SPO_IMM_DISP_alarm_aadl_man_t 
  -true "SUCCESS: aadl_man_t is immediately dispatched if a queue is not empty" 
  -false "WARNING: aadl_man_t is not dispatched in zero time but a queue is not empty"
\end{rtfiacre}

\item[$\oplus$] \textit{Si l'ordonnanceur est non préemptif à priorité
    fixe, le plus prioritaire de deux threads en attente du processeur
    sera servi en premier, s'ils ne sont pas reliés par une connexion
    immédiate}.

\textit{Règle de génération}
\begin{rtfiacre}
[for (t1 : ComponentInstance | thl) separator('\n')]
  [for (t2 : ComponentInstance | thl->select(th: ComponentInstance | 
    not s.hasImmediateConnection(t1,th) and 
    th.getPriorityWithDefaultValue() < t1.getPriorityWithDefaultValue())) separator('\n')]
  property ['PR_SCHED_'.concat(t1.uid()).concat('_').concat(t2.uid())/] is 
    ltl ['[]'/] (
	  (([s.id()/]/event proc_[t1.uid()/]_d and 
              (not [s.id()/]/event proc_[t2.uid()/]_e until [s.id()/]/event proc_[t2.uid()/]_d)) or
	   ([s.id()/]/event proc_[t2.uid()/]_d and 
              (not [s.id()/]/event proc_[t1.uid()/]_e until [s.id()/]/event proc_[t1.uid()/]_d))) 
	  => (not [s.id()/]/event proc_[t2.uid()/]_e until [s.id()/]/event proc_[t1.uid()/]_c))

  assert ['PR_SCHED_'.concat(t1.uid()).concat('_').concat(t2.uid())/] 
    -true "SUCCESS: [ t1.uid() /] has priority over [t2.uid()/]" 
    -false "ERROR: [ t1.uid() /] should have priority over [t2.uid()/]"
  [/for]
[/for]
\end{rtfiacre}

\textit{Exemple de génération}

\begin{rtfiacre}
property PR_SCHED_aadl_pmin_t_aadl_man_t is 
  ltl [] (
	  ((aadl_buffers_alarm_i_Instance/event proc_aadl_pmin_t_d and 
            (not aadl_buffers_alarm_i_Instance/event proc_aadl_man_t_e until 
             aadl_buffers_alarm_i_Instance/event proc_aadl_man_t_d)) or
           (aadl_buffers_alarm_i_Instance/event proc_aadl_man_t_d and 
            (not aadl_buffers_alarm_i_Instance/event proc_aadl_pmin_t_e until 
              aadl_buffers_alarm_i_Instance/event proc_aadl_pmin_t_d))) 
          => (not aadl_buffers_alarm_i_Instance/event proc_aadl_man_t_e until 
                 aadl_buffers_alarm_i_Instance/event proc_aadl_pmin_t_c))

assert PR_SCHED_aadl_pmin_t_aadl_man_t 
  -true "SUCCESS: aadl_pmin_t has priority over aadl_man_t" 
  -false "ERROR: aadl_pmin_t should have priority over aadl_man_t"
\end{rtfiacre}

  \item[$\oplus$] \textit{Lorsque deux threads périodiques sont simultanément (du
    point de vue temporel) activés et qu'il existe une connexion de
    donnée du premier vers le deuxième, le deuxième n'obtient le
    processeur qu'après complétion du premier}. Cette propriété
  temporisée n'est pas exprimable à l'aide des patrons retenus et
  nécessiterait l'écriture d'un observateur. Une spécification MTL de
  cette propriété pourrait être la suivante:

\[
\bigwedge_{T_i\leadsto T_j}\Box (((d_i \land \diamond_{[0,0]} d_j) \vee
(d_j \land \diamond_{[0,0]} d_i))
\Rightarrow \lnot e_j ~ \mathbf{U}~ c_i)
\]

Elle est toutefois exprimable dans le contexte de la traduction AADL
$\rightarrow$ RT-Fiacre en exploitant le fait que les événements
temporisés sont prioritaires et de durée minimale non nulle. Tester la
simultanéité temporelle de deux événements de temporisation nulle revient alors à tester
l'absence d'événements temporisés entre eux.

\textit{Règle de génération}

\begin{rtfiacre}
[for (t1 : ComponentInstance | thl) separator('\n')]
  [for (t2 : ComponentInstance | thl->select(th: ComponentInstance | 
     s.hasImmediateConnectionFrom(t1,th))) separator('\n')]
  property ['PR_SCHED_IMM_SYNC_'.concat(t1.uid()).concat('_').concat(t2.uid())/] is 
    ltl ['[]'/] (
	  (([s.id()/]/event proc_[t1.uid()/]_d and ((not timed_events) 
               until [s.id()/]/event proc_[t2.uid()/]_d)) or
	   ([s.id()/]/event proc_[t2.uid()/]_d and ((not timed_events) 
               until [s.id()/]/event proc_[t1.uid()/]_d)))
	  => (not [s.id()/]/event proc_[t2.uid()/]_e until [s.id()/]/event proc_[t1.uid()/]_c))

  assert ['PR_SCHED_IMM_SYNC_'.concat(t1.uid()).concat('_').concat(t2.uid())/] 
    -true "SUCCESS: when dispatch is synchronous, sender thread [ t1.uid() /] has priority 
                     over receiver thread [t2.uid()/]" 
    -false "ERROR: when dispatch is synchronous, sender thread [ t1.uid() /] should have 
                     priority over receiver thread [t2.uid()/]"
  [/for]
[/for]
\end{rtfiacre}


\textit{Exemple de génération}

\begin{rtfiacre}
property PR_SCHED_IMM_SYNC_aadl_ps_t_aadl_pr_t is 
  ltl [] (
    ((aadl_imm_imm_i_Instance/event proc_aadl_ps_t_d and ((not timed_events) 
               until aadl_imm_imm_i_Instance/event proc_aadl_pr_t_d)) or
    (aadl_imm_imm_i_Instance/event proc_aadl_pr_t_d and ((not timed_events) 
               until aadl_imm_imm_i_Instance/event proc_aadl_ps_t_d)))
  => (not aadl_imm_imm_i_Instance/event proc_aadl_pr_t_e 
              until aadl_imm_imm_i_Instance/event proc_aadl_ps_t_c))

assert PR_SCHED_IMM_SYNC_aadl_ps_t_aadl_pr_t 
    -true "SUCCESS: when dispatch is synchronous, sender thread aadl_ps_t has priority 
                   over receiver thread aadl_pr_t" 
    -false "ERROR: when dispatch is synchronous, sender thread aadl_ps_t should have 
                   priority over receiver thread aadl_pr_t"
\end{rtfiacre}

  \item[$\ominus$] Respect de l'automate d'état d'un thread AADL:
     chaque thread doit se synchroniser successivement sur les
     événements \textit{dispatch}, \textit{execute}, \textit{complete}
     et \textit{deadline}. Exprimer une telle propriété en LTL donne
     lieu à l'écriture d'une formule complexe. Un observateur serait
     plus adapté.

  \end{itemize}

\subsection{Propriétés génériques}
Ces propriétés doivent être vérifiées inconditionnellement par tout
modèle AADL.
  \begin{itemize}
    \item[$\oplus$] Respect du deadline: la complétion doit se
      produire avant le deadline décompté depuis l'activation du
      thread. Ces deux événements sont identifiés par un changement
      d'état du processus contrôleur.

\textit{Règles de génération} Les règles dépendent de la nature du
thread et de la présence ou non d'une échéance explicite inférieure à
la période. Le tableau suivant décrit les états associés aux deux événements:

\begin{center}
\begin{tabular}{|c|c|c|}
\hline
nature du thread  & activation & complétion \\
\hline
périodique avec deadline & pdl\_rdy & pdl\_wdl \\
périodique sans deadline & p\_rdy & p\_idle \\
sporadique avec deadline & spdl\_rdy & spdl\_wp \\
sporadique sans deadline & sp\_rdy & sp\_wp  \\
\hline
\end{tabular}
\end{center}

Nous ne donnons ici que la règle associée à la première configuration:
\begin{rtfiacre}
[for (t : ComponentInstance | thl->select(p | p.isPeriodic() and p.hasThreadDeadline())) 
    separator('\n')]
  property ['PR_DL_'.concat(t.uid())/] is [s.id()/]/[thl->indexOf(t)*2/]/value (st = pdl_rdy) 
    leadsto [s.id()/]/[thl->indexOf(t)*2/]/value (st = pdl_wdl) 
    within [ '[' /] 0, [t.getThreadDeadlineOrPeriod()/] [ ']' /]

  assert ['PR_DL_'.concat(t.uid())/] 
    -true "SUCCESS: [ t.uid() /] completes before deadline" 
    -false "ERROR: [ t.uid() /] does not complete before deadline"
[/for]
\end{rtfiacre}

\textit{Exemple de génération}

\begin{rtfiacre}
property PR_DL_aadl_pmin_t is aadl_buffers_alarm_i_Instance/2/value (st = p_rdy) 
  leadsto aadl_buffers_alarm_i_Instance/2/value (st = p_idle) within [ 0, 5000 ]

assert PR_DL_aadl_pmin_t 
  -true "SUCCESS: aadl_pmin_t completes before deadline" 
  -false "ERROR: aadl_pmin_t does not complete before deadline"
	
property PR_DL_aadl_man_t is aadl_buffers_alarm_i_Instance/6/value (st = sp_rdy) 
  leadsto aadl_buffers_alarm_i_Instance/6/value (st = sp_wp) within [ 0, 2000 ]

assert PR_DL_aadl_man_t 
  -true "SUCCESS: aadl_man_t completes before deadline" 
  -false "ERROR: aadl_man_t does not complete before deadline"
\end{rtfiacre}

    \item[$\oplus$]  Tout état est potentiellement accessible (absence
     de code mort). On vérifie en fait que l'inaccessibilité de l'état
     n'est pas satisfaite par le modèle.

\begin{rtfiacre}
[for (t : ComponentInstance | thl) separator('\n')]
  [for (st: State | t.getBehaviorAnnex().state)]
  	property ['PR_ST_'.concat(t.uid()).concat(st.name)/] is 
           absent [s.id()/]/[thl->indexOf(t)*2+1/]/state 
               [if (st.isInitialState())][st.toDispatchIdent()/]
               [elseif (st.isDispatchState())][st.toCompleteIdent()/]
               [else][st.toIdent()/][/if]

        assert ['PR_ST_'.concat(t.uid()).concat(st.name)/] 
          -true "WARNING: state [st.name()/] of thread [t.uid()/] is not reachable" 
          -false "SUCCESS: state [st.name()/] of thread [t.uid()/] is reachable"
  [/for]
[/for]
\end{rtfiacre}

\textit{Exemple de génération}
\begin{rtfiacre}
property PR_ST_aadl_pmin_ts0 is 
  absent aadl_buffers_alarm_i_Instance/3/state dispatch_state_aadl_s0

assert PR_ST_aadl_pmin_ts0 
  -true "WARNING: state s0 of thread aadl_pmin_t is not reachable" 
  -false "SUCCESS: state s0 of thread aadl_pmin_t is reachable"
\end{rtfiacre}

\end{itemize}

\subsection{Informations sur le modèle}
  \begin{itemize}
%   \item[$\oplus$] Absence de perte de données sur un port
% pas de débordement et (port non vide leadsto transition_prise) tous les cas

  \item[$\oplus$] Un thread sporadique est-il activé infiniment
    souvent? Cette propriété ne doit pas être nécessairement
    vérifiée. Il serait possible de l'ajouter comme annotation dans le
    modèle AADL. Ici, sa validité est indiquée en tant qu'information.

\textit{Règle de génération}

\begin{rtfiacre}
[for (t : ComponentInstance | thl->select(p | p.isSporadic())) separator('\n')]
  property ['PR_SPO_INF_'.concat(t.uid())/] is 
     infinitelyoften [s.id()/]/[thl->indexOf(t)*2/]/tag dispatch

  assert ['PR_SPO_INF_'.concat(t.uid())/] 
    -true "INFO: [ t.uid() /] is dispatched infinitely often"
    -false "INFO: [ t.uid() /] is not dispatched infinitely often"
[/for]
\end{rtfiacre}

\textit{Exemple de génération}

\begin{rtfiacre}
property PR_SPO_INF_aadl_man_t is 
  infinitelyoften aadl_buffers_alarm_i_Instance/6/tag dispatch
\end{rtfiacre}

   \item[$\ominus$] Les états internes des threads sont-ils toujours
     (c'est-à-dire dans toute exécution) tous accessibles?

\textit{Règle de génération}

\begin{rtfiacre}
[for (t : ComponentInstance | thl) separator('\n')]
  [for (st: State | t.getBehaviorAnnex().state)]
  	property ['PR_STR_'.concat(t.uid()).concat(st.name)/] is 
           present [s.id()/]/[thl->indexOf(t)*2+1/]/state
              [if (st.isInitialState())][st.toDispatchIdent()/]
              [elseif (st.isDispatchState())][st.toCompleteIdent()/]
              [else][st.toIdent()/][/if]

  	assert ['PR_STR_'.concat(t.uid()).concat(st.name)/] 
         -true "INFO: state [st.name()/] of thread [t.uid()/] is always reached" 
         -false "INFO: state [st.name()/] of thread [t.uid()/] may be unreachable"
  [/for]
[/for]
\end{rtfiacre}

\textit{Exemple de génération}

\begin{rtfiacre}
property PR_STR_aadl_pmin_ts0 is
  present aadl_buffers_alarm_i_Instance/3/state dispatch_state_aadl_s0

assert PR_STR_aadl_pmin_ts0 
  -true "INFO: state s0 of thread aadl_pmin_t is always reached" 
  -false "INFO: state s0 of thread aadl_pmin_t may be unreachable"
\end{rtfiacre}

    \item[$\oplus$]  Non débordement des buffers associés aux ports de
      type event ou event data:

\textit{Règle de génération}

\begin{rtfiacre}
[for (t : ComponentInstance | thl->select(p | p.isSporadic())) separator('\n')]
 [let pl: OrderedSet(FeatureInstance) = t.getIncomingPortFeatureInstances()]
  [for (p: FeatureInstance | pl->select(feature.oclIsKindOf(EventDataPort)))]
  property ['PR_SPO_NO_OVF_'.concat(p.feature.name).concat('_').concat(t.uid())/] is
	  absent [s.id()/]/[3+thl->size()*2/]/[pl->indexOf(p)/]/tag overflow

  assert ['PR_SPO_NO_OVF_'.concat(p.feature.name).concat('_').concat(t.uid())/] 
    -true "SUCCESS: port [p.name/] does not overflow in thread [t.uid() /]" 
    -false "WARNING: port [p.name/] overflows in thread [ t.uid() /]"
  [/for]

  [for (p: FeatureInstance | pl->select(feature.oclIsKindOf(EventPort)))]
  property ['PR_SPO_NO_OVF_'.concat(p.feature.name).concat('_').concat(t.uid())/] is 
     absent [s.id()/]/[3+thl->size()*2/]/[pl->indexOf(p)/]/tag overflow

  assert ['PR_SPO_NO_OVF_'.concat(p.feature.name).concat('_').concat(t.uid())/] 
    -true "SUCCESS: port [p.name/] does not overflow in thread [ t.uid() /]"
    -false "WARNING: port [p.name/] overflows in thread [ t.uid() /]"
  [/for]
 [/let]
[/for]
\end{rtfiacre}

\textit{Exemple de génération}
\begin{rtfiacre}
property PR_SPO_NO_OVF_alarm_aadl_man_t is
  absent aadl_buffers_alarm_i_Instance/9/1/tag overflow

assert PR_SPO_NO_OVF_alarm_aadl_man_t 
  -true "SUCCESS: port alarm does not overflow in thread aadl_man_t" 
  -false "WARNING: port alarm overflows in thread aadl_man_t"
\end{rtfiacre}

   \item[$\oplus$] Consommation de tout message reçu (de type event ou
      event data): en supposant le non débordement, on vérifie de plus
      que chaque file non vide induit une activation d'un thread sporadique.

\textit{Règles de génération}
\begin{rtfiacre}
[for (t : ComponentInstance | thl->select(p | p.isSporadic())) separator('\n')]
 [let pl: OrderedSet(FeatureInstance) = t.getIncomingPortFeatureInstances()]
  [for (p: FeatureInstance | pl->select(feature.oclIsKindOf(EventDataPort)))]
  property ['PR_SPO_NO_LOSS_'.concat(p.feature.name).concat('_').concat(t.uid())/] is
	  [s.id()/]/[3+thl->size()*2/]/[pl->indexOf(p)/]/value (not(empty(q))) 
          leadsto [s.id()/]/[3+thl->size()*2/]/[pl->indexOf(p)/]/tag consume

  assert ['PR_SPO_NO_LOSS_'.concat(p.feature.name).concat('_').concat(t.uid())/] 
    -true "SUCCESS: [ t.uid() /] consumes all messages received on port [p.name/]" 
    -false "WARNING: [ t.uid() /] does not consume all messages received on port [p.name/]"
  [/for]

  [for (p: FeatureInstance | pl->select(feature.oclIsKindOf(EventPort)))]
  property ['PR_SPO_NO_LOSS_'.concat(p.feature.name).concat('_').concat(t.uid())/] is 
     [s.id()/]/[3+thl->size()*2/]/[pl->indexOf(p)/]/value (evt > 0) 
     leadsto [s.id()/]/[3+thl->size()*2/]/[pl->indexOf(p)/]/tag consume

  assert ['PR_SPO_NO_LOSS_'.concat(p.feature.name).concat('_').concat(t.uid())/] 
   -true "SUCCESS: [ t.uid() /] consumes all messages received on port [p.name/]" 
   -false "WARNING: [ t.uid() /] does not consume all messages received on port [p.name/]"
  [/for]
 [/let]
[/for]
\end{rtfiacre}

\textit{Exemple de génération}

\begin{rtfiacre}
property PR_SPO_NO_LOSS_alarm_aadl_man_t is
  aadl_buffers_alarm_i_Instance/9/1/value (not(empty(q))) 
  leadsto aadl_buffers_alarm_i_Instance/9/1/tag consume

assert PR_SPO_NO_LOSS_alarm_aadl_man_t 
  -true "SUCCESS: aadl_man_t consumes all messages received on port alarm" 
  -false "WARNING: aadl_man_t does not consume all messages received on port alarm"
\end{rtfiacre}

    \item[$\oplus$] Respect du délai minimal d'inter-arrivée des
      événements d'un thread sporadique. Il y a une règle pour chaque
      port de type event ou event data. On ne donne ici que la règle
      associée aux ports de type event data.

\textit{Règle de génération}
\begin{rtfiacre}
[for (t : ComponentInstance | thl->select(p | p.isSporadic())) separator('\n')]
 [let pl: OrderedSet(FeatureInstance) = t.getIncomingPortFeatureInstances()]
  [for (p: FeatureInstance | pl->select(feature.oclIsKindOf(EventDataPort)))]
  property ['PR_SPO_INTER_'.concat(p.feature.name).concat('_').concat(t.uid())/] is
	  absent (([s.id()/]/[3+thl->size()*2/]/[pl->indexOf(p)/]/value (not(empty(q)))) and
                       ([s.id()/]/[thl->indexOf(t)*2/]/value 
                           (st <> [if (t.hasThreadDeadline())]spdl_idle[else]sp_idle[/if])))

  assert ['PR_SPO_INTER_'.concat(p.feature.name).concat('_').concat(t.uid())/] 
    -true "SUCCESS: incomming event on port [p.name/] while thread [ t.uid() /] is idle" 
    -false "WARNING: incomming event on port [p.name/] while thread [ t.uid() /] is not idle"
  [/for]
 [/let]
[/for]
\end{rtfiacre}

\textit{Exemple de génération}

\begin{rtfiacre}
	  property PR_SPO_INTER_alarm_aadl_man_t is
		  absent ((aadl_buffers_alarm_i_Instance/9/1/value (not(empty(q)))) and (aadl_buffers_alarm_i_Instance/6/value (st <> sp_idle)))
	  assert PR_SPO_INTER_alarm_aadl_man_t -true "SUCCESS: incomming event on port alarm while thread aadl_man_t is idle" -false "WARNING: incomming event on port alarm while thread aadl_man_t is not idle"
\end{rtfiacre}

   \item[$\ominus$] Respect du délai de prise en compte de messages

   \item[$\ominus$] Respect du délai de réaction sur un flot. Cette
     règle n'a pas été implantée en raison de la sémantique mal
     définie de la notion de flot en AADL et de la complexité d'une
     vérification précise. Elle nécessiterait de dater les messages,
     notion non supportée par les outils de vérification. Cette propriété pourrait
     être reformulée en: une émission via le port d'arrivée du flot se
     produit au bout d'un délai inférieur à celui associé au flot
     après une réception sur le port source du flot.
  \end{itemize}

