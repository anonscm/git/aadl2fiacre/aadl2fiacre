aadl_files = Dir['**/*.aadl']
rtfcr_files = Marshal.load(Marshal.dump(aadl_files))
rtfcr_files.map do |e|
  e.gsub!(/aadl/, 'rtfcr')
end

aadl_files.each_index do |i|
  aadl_lines = IO.readlines(aadl_files[i])
  rtfcr_lines = IO.readlines(rtfcr_files[i])
  files = [aadl_files[i], rtfcr_files[i]]
  smaller = aadl_lines.size < rtfcr_lines.size ? 0 : 1
  
  File.open(files[smaller], 'a') do |f|
    (aadl_lines.size - rtfcr_lines.size).abs.times do
      f.puts ''
    end
    f.puts '$$'
  end
end