%!TEX root = ./transformation.tex
%==============================================================================%
                        \section{Traduction AADL.Thread}
%==============================================================================%

                %---------------------------------------------%
                \subsection{Squelette général de la traduction}
                            \label{sec:thread_trad}
                %---------------------------------------------%

	Chaque \verb!thread! AADL est traduit en un \verb!process! RT-Fiacre.
	La structure du schéma de traduction d'un \verb!thread! AADL en
	\verb!process! RT-Fiacre est détaillée ci-après.
	Dans la partie déclaration de ports du processus RT-Fiacre (partie entre
	\verb![!\,\verb!]!) nous trouvons les ports prédéfinis qui assurent la
	correspondance avec le modèle d'exécution AADL:
	
	%--------------------------------------------------------------------------%
	\begin{itemize}

		\item
		\verb!d! pour \emph{dispatch}

		\item
		\verb!e! pour \emph{execute}

    	\item
		\verb!c! pour \emph{complete}
			
		\item
		\verb!dl! pour \emph{deadline}
			
	\end{itemize}
	%--------------------------------------------------------------------------%
                                                                               %
	Puis viendront s'ajouter les ports de type \verb!event! et \verb!event data!
	éventuellement définis. La partie déclaration de paramètres du processus
	(partie entre \verb!()!) permettra de définir les variables partagées
	associées aux ports de type \verb!event!, \verb!event data! et \verb!data!
	définis dans le thread AADL.
	Enfin les spécifications de l'annexe comportementale viendront compléter la
	définition du corps du \verb!process!.
%	(voir listing \ref{lst:thread/skeleton_thread_generation}).
	
	% \trad{thread/skeleton_thread_generation}
	% 	 {Squelette général de la transformation d'un thread en process}

	\traduction{thread/skeleton_thread_generation}

	Il faut toutefois noter que certains schémas de traduction
	d'un \verb!thread! \emph{diffèrent} selon la valeur de la propriété
	\verb!Dispatch_Protocol!.

             %---------------------------------------------------%
             \subsection{Déclaration de ports (thread > features)}
             %---------------------------------------------------%
	
	Dans le cas général, la déclaration des ports dans un \verb!thread! AADL
	se traduit relativement facilement en RT-Fiacre. Les règles de conversion
	en type RT-Fiacre des types associés aux ports 
	(\verb!data! et \verb!event data!) sont données à la section 
	\ref{sec:type_trad}.\\
	
	%--------------------------------------------------------------------------%
	\begin{itemize}\withbullet

		\item
		\underline{Port de type \verb!event! et \verb!event data! en \emph{sortie}
				(attribut \verb!out! ou \verb!in out!) :}\\
		Pour chaque port satisfaisant ces conditions on déclare un port
		RT-Fiacre associé au processus.
		
		%----------------------------------------------------------------------%
		\begin{itemize}
			
			\item port de type \verb!event! :
			\incltraduction{thread/port_decl_event}
			
			\item port de type \verb!event data! :
			\incltraduction{thread/port_decl_event_data}
			
		\end{itemize}
		%----------------------------------------------------------------------%
		
		\item
		\underline{Port de type \verb!event! et \verb!event data! en \emph{entrée} pour
				un thread \emph{sporadique} :}\\
		Les ports vérifiant cette condition seront \emph{multiplexés} sur le
		port prédéfini \verb!d! dont le type associé correspondra à un type
		\verb!union! dont les règles de construction sont détaillées dans la
		section	\ref{sec:union_gen}.
		
		\traduction{thread/spo_port_decl}
		
	\end{itemize}
	%--------------------------------------------------------------------------%
	
	Seuls les ports \emph{vérifiant} les conditions évoquées précédemment se
	retrouvent dans la partie déclaration de port du processus RT-Fiacre. Les
	autres ports seront modélisés par des variables partagées passées par
	référence au processus RT-Fiacre dont les règles de construction sont
	détaillées dans la section \ref{sec:arg_gen}.

          %---------------------------------------------------------%
          \subsection{Règle de traduction des types AADL / RT-Fiacre}
                             \label{sec:type_trad}
          %---------------------------------------------------------%

	Les règles de transformation des types sont données dans le tableau
	suivant :\\
	\begin{center}
		\newcolumntype{C}{>{\centering\arraybackslash}X}
		\begin{tabularx}{.75\textwidth}{|C|C|}
			\hline
			AADL & RT-Fiacre \\ \hline \hline
			\verb!behavior::integer! & \verb!int! \\ \hline
			\verb!behavior::boolean! & \verb!bool! \\ \hline
			\verb!Data! & \verb!record! \\ \hline
		\end{tabularx}
	\end{center}

        %--------------------------------------------------------------%
        \subsection{Génération du type union associé au port \texttt{d}}
                             \label{sec:union_gen}
        %--------------------------------------------------------------%

	Comme évoqué précédemment, dans le cadre d'un thread \emph{sporadique}
	les ports de type \verb!event! et \verb!event data! en entrée sont 
	multiplexés sur le port prédéfini \verb!d! (dispatch) du processus
	RT-Fiacre correspondant. Ainsi on ajoutera un constructeur au type
	\verb!union! pour \emph{chaque} port vérifiant cette condition.
	La construction \op[id]{p} désignera l'identificateur
	RT-Fiacre associé au port \verb!p!, de plus la construction \verb!type!
	désignera le type correspondant tel que défini à la section
	\ref{sec:type_trad}.
	
	\traduction{thread/spo_union_type}
	% {Schéma de génération du type union à partir des ports de type
	%  \texttt{event} et \texttt{event data} en entrée pour un thread
	%  sporadique.} 
	
	Une autre sorte constructeur sera éventuellement ajoutée dans le type \verb!union!
	comme décrit à la section \ref{sec:timeout_gen}.

         %-----------------------------------------------------------%
         \subsection{Déclaration des arguments du processus RT-Fiacre}
                              \label{sec:arg_gen}
         %-----------------------------------------------------------%

	\subsubsection*{Ports associés à un argument}

	Les arguments du processus RT-Fiacre permettent le passage par 
	\emph{référence} des variables partagées \emph{associées} aux ports de 
	communication pour gérer la nature asynchrone des communications AADL.\\
	On distingue plusieurs cas :\\
	
	%--------------------------------------------------------------------------%
	\begin{itemize}\withbullet
		
		\item \underline{Thread périodique ou sporadique :}\\
		Pour \emph{chaque port} déclaré dans le \verb!thread! on associe un
		argument au \verb!process! ainsi généré. Les ports de type
		\verb!data! en \emph{entrée} génèrent non pas un mais \emph{deux}
		arguments comme décrit ci-après.
		\traduction{thread/per_arg_decl}

		\item \underline{Cas particuliers (périodique ou sporadique):}\\
		Les ports \verb!data! en \emph{entrée} seront associés à \emph{deux}
		arguments. Le premier argument correspond au port \verb!data!
		(\op[id]{p}), le second argument modélise l'attribut \verb!'fresh!
		associé au port (\verb!fresh_!\op[id]{p}).\\
		
		\item \underline{Gestion \verb!data access! :}\\
		Il est possible en AADL de partager des données entre différents composants
		en utilisant les constructions \verb!provides data access! et
		\verb!requires data access!. Ce mécanisme est traduit en RT-Fiacre
		par l'ajout d'un argument avec passage par référence pour chacune de
		ces variables partagées.\\
		L'exemple suivant illustre cette traduction :
		\traduction{thread/data_access}
		
		
	\end{itemize}
	%--------------------------------------------------------------------------%

	\subsubsection*{Typage des arguments}
	
	Le typage des arguments associés aux ports AADL dépend de plusieurs 
	paramètres, lorsque cela n'est pas précisé les ports peuvent être
	considérés en \emph{entrée} ou en \emph{sortie}.\\
	
	%--------------------------------------------------------------------------%
	\begin{itemize}\withbullet
		
		\item \underline{Port \verb!event! :}
		\begin{itemize}
			\item Par défaut, lorsque la propriété \verb!Dequeue_Protocol! 
			\emph{n'est pas} associée au port l'argument a le type :
			\verb!0..1!.
			
			\item Si la propriété \verb!Dequeue_Protocol! est égal à
			\verb!OneItem! l'argument a le type : \verb!0..1!.
			
			\item Si la propriété \verb!Dequeue_Protocol! est égal à
			\verb!AllItems! l'argument a le type : \verb!0..x! où
			\verb!x! désigne la valeur entière de la propriété \verb!Queue_Size!
			dont la valeur est 1 par défaut.\\
		\end{itemize}
		
		\item \underline{Port \verb!event data! :}\\
		On appellera \verb!t! le type RT-Fiacre associé au port tel que défini
		à la section \ref{sec:type_trad}.
		\begin{itemize}
			
			\item Par défaut, lorsque la propriété \verb!Dequeue_Protocol! 
			      \emph{n'est pas} associée au port l'argument a le type :
			      \verb!queue 1 of t!.
			
			\item Si la propriété \verb!Dequeue_Protocol! est égal à
			\verb!OneItem! l'argument a le type : \verb!queue 1 of t!.
			
			\item Si la propriété \verb!Dequeue_Protocol! est égal à
			\verb!AllItems! l'argument a le type : \verb!queue x of t! où
			\verb!x! désigne la valeur entière de la propriété \verb!Queue_Size!
			dont la valeur entière est 1 par défaut.\\
		\end{itemize}
		
		\item \underline{Port \verb!data! en \emph{entrée} :}\\
		On appellera \verb!t! le type RT-Fiacre associé au port tel que défini
		à la section \ref{sec:type_trad}.
		\begin{itemize}
			\item Le type de l'argument associé au port : \verb!queue 1 of t!.

			\item Le type de l'argument associé à l'attribut \verb!'fresh! est
			      \verb!bool!.
		\end{itemize}
		
	\end{itemize}
	%--------------------------------------------------------------------------%

 %---------------------------------------------------------------------------%
         \subsection{Gestion de la propriété \texttt{Urgency} des ports
                            pour les threads sporadiques}                                                                                   
                            \label{sec:urgency_gen}
 %---------------------------------------------------------------------------%
	
	Lorsque \verb!t! est un thread \emph{sporadique} la propriété
	\verb!Urgency! des ports est modélisée par un processus auxiliaire.
	On appelle $S$ l'ensemble des ports en \emph{entrée} du thread
	\verb!t! dont le type est \verb!event! ou \verb!event data! ; ce sont
	ces mêmes ports que l'on a multiplexé sur le port \verb!d!
	(voir section \ref{sec:union_gen}). On partitionne ensuite l'ensemble $S$
	en une famille d'ensembles $S_i$ où l'indice $i$ désigne la valeur de la
	propriété \verb!Urgency! associée au port dont la valeur par défaut est 1.
	L'automate associé au processus RT-Fiacre comporte un unique état dont la
	seule action est une construction de la forme :
	\begin{alltt}
            select
                d?constr\(\sb{1}\) [] ...
            unless
                d?constr\(\sb{i}\) [] ...
            end; null
	\end{alltt}
	où les familles d'ensemble $S_i$ sont séparées par la clause \verb!unless!.
	Ainsi, \emph{chacun} des constructeurs du type \verb!union! associé au
	port \verb!d! se retrouve dans une branche du \verb!select ... end!.
	Enfin, les constructeurs du type \verb!union! obtenus à partir des
	constructions de la forme \verb!on timeout t! sont plus prioritaires que
	tout port. Un exemple exhaustif détaillant ce schéma de traduction est donné
	dans le listing suivant :
	% \ref{lst:thread/urgency_gen}.
	
	\traduction{thread/urgency_gen}
	     % {Association d'un processus auxiliaire pour la traduction de la
	     %  propriété \texttt{Urgency} associée aux ports}
	
	
%==============================================================================%