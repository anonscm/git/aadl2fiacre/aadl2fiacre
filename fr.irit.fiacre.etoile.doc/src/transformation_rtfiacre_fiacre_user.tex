\section{Documentation utilisateurs -- Présentation des transformations}

\subsection{Processus étendus}

Le but de cette transformation est d'autoriser la déclaration et l'utilisation de ports locaux au sein d'un processus (de manière similaire à ce que l'on fait dans un composant).

Un composant sera alors créé afin d'intégrer les ports locaux et les priorités.

\trans{locPorts}{Exemple de ports locaux}

\subsection{Généricité}

Cette transformation permet de paramétrer des processus et des composants par des types, des constructeurs et des valeurs constantes (entier, identificateur, ...) à la compilation.
Ces paramètres suivent directement le nom du processus/composant (avant les ports, donc) et sont entourés de \verb@<|@ et \verb@|>@.

Lors de la déclaration du processus/composant, on indique la liste des paramètres génériques.

Lors de l'instantiation, on donne les paramètres effectifs précédés de leur \og type \fg{} à savoir :
\begin{itemize}
	\item \verb@const@ pour les valeurs constantes
	\item \verb@type@ pour les types
	\item \verb@constr0@ pour les constructeurs ne prenant pas de paramètre
	\item \verb@constr1@ pour les constructeurs prenant un paramètre
\end{itemize}

Le processus/composant instancié est copié et ses paramètres génériques sont remplacés par leur valeur effective.

\lstinputlisting[language=rtfiacre]{code/generic.rtfcr}

Ce qui devient :

\lstinputlisting[language=fiacre]{code/generic.fcr}

\subsection{Regroupement des transitions multiples}

Cette transformation consiste à regrouper les transitions partant d'un même état en une seule transition, utilisant une instruction \verb@select@.

\trans{transMul}{Les instructions sont regroupées en une seule}

\subsection{Découpage des transitions contenant plusieurs synchronisations}

En Fiacre, il est interdit de mettre plusieurs synchronisations (envoi ou réception de signal) dans une même transition.

Afin de s'affranchir de cette limitation, cette transformation se charge de découper chaque transition contenant plusieurs synchronisations en plusieurs transitions, en ajoutant les états intermédiaires nécessaires.
Le découpage est effectué juste avant la seconde synchronisation rencontrée.

\trans{seqSync}{Exemple de découpage de transition}

Ou schématiquement :

\begin{figure}[h]
	\centering
	\begin{tikzpicture}
		\tikzstyle{rstyle} = [
			very thick, draw, drop shadow,
			ellipse,
			fill=black!5,
		]
		\tikzstyle{every node} = [font=\scriptsize]

		\node (s0) at (3.75, 0) [rstyle] {s0};
		\node (s1) at (8.75, 0) [rstyle] {s1};

		\node (tr1) at (6.25, .25) {a; b?0; c!1; q := \{||\}; d};

		\path [very thick] (s0) edge[->] (s1);

		\draw [-implies, double distance=2pt, very thick] (6.25, -.5) -- +(0, -.75);

		\node (s0bis) at (-.5, -2) [rstyle] {s0};
		\node (impls0) at (2.25, -2) [rstyle] {gseqSync\_s0};
		\node (impls1) at (6.25, -2) [rstyle] {gseqSync\_s1};
		\node (impls2) at (10.25, -2) [rstyle] {gseqSync\_s2};
		\node (s1bis) at (13, -2) [rstyle] {s1};

		\path [very thick] (s0bis) edge[->] node[above] {a} (impls0);
		\path [very thick] (impls0) edge[->] node[above] {b?0} (impls1);
		\path [very thick] (impls1) edge[->] node[above, text width=10cm, align=center] {c!1;\\q := \{||\}} (impls2);
		\path [very thick] (impls2) edge[->] node[above] {d} (s1bis);
	\end{tikzpicture}
\end{figure}

Afin d'éviter d'avoir des transitions sans synchronisation, cette transformation se charge aussi d'ajouter un \verb@wait [0, 0]@ si une transition ne contient aucune synchronisation.

\trans{seqSync2}{Ajout d'une synchronisation}

\subsection{Élimination des wait}

Cette transformation remplace les instructions \verb@wait@ par des synchronisations sur des ports locaux.

Deux \verb@wait@ ayant le même intervalle seront remplacés par des synchronisations sur le même port ; deux \verb@wait@ sur des intervalles différents deviendront des synchronisations sur des ports différents.

Toutefois, dans les \verb@unless@ des \verb@select@, les \verb@wait@ sont autorisés mais les synchronisations interdites, aussi les \verb@wait@ placés dans des \verb@unless@ sont laissés tels quels.

\trans{elimWait}{Exemple de transformation de wait en port}

\subsection{Extraction de ports}

Cette transformation permet de faire remonter certains ports (correspondants à une expression régulière) depuis un composant dans le composant englobant.

Les priorités associées aux ports remontés sont également remontées.

\trans{extractPorts}{Exemple d'extraction avec l'expression régulière test\_.*}

Cette transformation est utilisée pour remonter les ports correspondants aux waits (cf transformation précédente).

\subsection{Priorités des wait}

Cette transformation permet d'exprimer la priorité des ports issus des \verb@wait@ d'un processus dans le composant englobant.

Les deux transformations précédentes ont ajouté des ports locaux à certains composants.
Il est possible d'indiquer la priorité de ces ports à l'aide de deux expressions : \verb@WAIT@ et \verb@SORTED_WAIT@.

Dans les déclarations de priorités, \verb@WAIT@ sera remplacé par la liste des port locaux correspondant à des \verb@wait@ séparés par des \verb@|@ et \verb@SORTED_WAIT@ par la liste des ports locaux séparés par des \verb@>@.
\clearpage

\trans{waitPrio}{Exemple de déclaration de priorité sur les waits}

\subsection{Chaînage des priorités des ports}

Afin d'alléger les déclarations de priorité des ports, cette transformation autorise le chaînage dans les-dites déclarations, c'est-à-dire permet l'écriture de \verb@a > b > c > d@ au lieu de \verb@a > b, b > c, c > d@.

\subsection{Tableaux de ports}

Cette transformation permet de passer en paramètre à un processus un tableau de ports.

\trans{tabPort}{Exemple d'utilisation d'un tableau de ports}

\subsection{Tableaux de références}

Cette transformation permet de passer des tableaux de références en paramètre à des processus/composants :
\begin{rtfiacre}
process P[a : array 3 of in out none](&[3]v : nat) is
states s0
var x : nat
init to s0
from s0 select i of [3] p[i]; x := v[i] end; to s0

component C is
var x1, x2, x3 : nat
port p : array 3 of in out none
par * in -> P[p]([&x1, &x2, &x3])
end
\end{rtfiacre}

\subsection{Select indexé}

Un \verb@select@ indexé sur \verb@N@ génère un select à \verb@N@ branches, éventuellement paramétrées d'une valeur allant de \verb@0@ à \verb@N - 1@.

\trans{selecti}{Utilisation d'un select indexé sur des tableaux de ports et de références}

\subsection{Par évolué}

Le \verb@par@ évolué permet de générer plusieurs instanciations paramétrées par une valeur comprise dans un intervalle.

\trans{parE}{Utilisation du par évolué avec un tableau de ports}

\subsection{Multiplexage de ports}

Le but est d'autoriser le multiplexage de ports, c'est-à-dire de permettre de passer à un processus une liste de ports en lieu et place d'un.

\trans{multiplex}{Exemple de multiplexage de ports}

\subsection{Projection de canaux}

Soit un processus \verb@process P[p : T1]@ et un canal \verb@c : T1#T2@. Cette transformation permet de passer en paramètre à \verb@P@ la projection de \verb@c@ sur \verb@T1@ notée \verb@c^1@, ou \verb@c^{1}@.
Lors de synchronisations, les éléments non projetés du canal ont la valeur \verb@any@.

Plus généralement, on peut projeter un canal sur un ensemble d'indices sous la forme \verb@c^{i1, ..., in}@.

\trans{projChan}{Projection d'un canal sur 2 indices}

\subsection{Quantificateurs universel et existentiel}

Ceci ajoute les opérateurs \verb@all@ et \verb@exists@ dans les expressions afin d'exprimer qu'une propriété est vraie pour tout entier (resp. est vraie pour un entier) entre deux entiers \verb@i@ et \verb@j@, ou de \verb@0@ à \verb@N - 1@ :

\trans{allex}{Exemple d'utilisation des quantificateurs}

\subsection{Initialisation évoluée des tableaux}

Cette transformation permet d'initialiser tous les éléments d'un tableau avec la même valeur.

Ainsi, pour initialiser tous les éléments d'un tableau à 0, on peut écrire :
\begin{rtfiacre}
var t : array 4 of int := [0] * 4,
t2 : array 4 of int := 4 * [0]
\end{rtfiacre}

\subsection{Liste d'initialisation}

Le but est de permettre d'initialiser les éléments d'un tableau avec une expression dépendante de l'indice du tableau.

Ainsi, on peut écrire :
\begin{rtfiacre}
var t : array 10 of int := [i : 10 | i + 1]
\end{rtfiacre}
à la place de :
\begin{fiacre}
var t : array 10 of int := [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
\end{fiacre}

\subsection{Initialisation par défaut}

Cette transformation initialise automatiquement les variables avec une valeur par défaut :
\begin{itemize}
	\item pour les entiers, \verb@0@
	\item pour les booléens, \verb@false@
	\item pour les unions, le premier champ déclaré
	\item pour les queues, une queue vide
	\item pour les tableaux, chaque élément est initialisé à sa valeur par défaut
	\item pour les structures, chaque champs est initialisé à sa valeur par défaut
\end{itemize}
\clearpage

\trans{initDefault}{Exemple d'initialisation par défaut}
