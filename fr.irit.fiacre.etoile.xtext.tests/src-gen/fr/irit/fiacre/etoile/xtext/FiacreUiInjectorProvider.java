/*
 * generated by Xtext
 */
package fr.irit.fiacre.etoile.xtext;

import org.eclipse.xtext.junit4.IInjectorProvider;

import com.google.inject.Injector;

public class FiacreUiInjectorProvider implements IInjectorProvider {
	
	@Override
	public Injector getInjector() {
		return fr.irit.fiacre.etoile.xtext.ui.internal.FiacreActivator.getInstance().getInjector("fr.irit.fiacre.etoile.xtext.Fiacre");
	}
	
}
